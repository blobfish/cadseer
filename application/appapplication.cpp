/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <memory>

#include <QTimer>
#include <QMessageBox>
#include <QSettings>
#include <QDir>

#include "subprojects/libgit2pp/src/global.hpp" //for git start and shutdown.

#include "application/appapplication.h"
#include "application/appmainwindow.h"
#include "viewer/vwrspaceballqevent.h"
#include "viewer/vwrwidget.h"
#include "project/prjmessage.h"
#include "project/prjgitmanager.h" //needed for unique_ptr destructor call.
#include "project/prjproject.h"
#include "preferences/prfmanager.h"
#include "dialogs/dlgpreferences.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "menu/mnumanager.h"
#include "command/cmdmanager.h"
#include "lod/lodmanager.h"
#include "import/impmanager.h"
#include "dialogs/dlgproject.h"
#include "dialogs/dlgabout.h"
#ifdef DBUS_BUILD
#include "dbus/dbsmanager.h"
#endif

#ifdef SPNAV_PRESENT
#include <spnav.h>
#endif


using namespace app;

struct Application::Stow
{
  Application &application;
  msg::Node node;
  msg::Sift sift;
  lod::Manager lodManager;
  imp::Manager impManager;
  MainWindow mainWindow;
  std::unique_ptr<prj::Project> project; //build outside constructor call path
  bool spaceballPresent = false;
#ifdef DBUS_BUILD
  dbs::Manager dbsManager;
#endif
  
  Stow() = delete;
  Stow(Application &appIn)
  : application(appIn)
  , lodManager(application.arguments().at(0).toStdString())
  , impManager(appIn)
  {
    node.connect(msg::hub());
    sift.name = "app::Application";
    node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
    setupDispatcher();
    mainWindow.showMaximized();
  }
  
  void createNewProject(const std::string &directoryIn)
  {
    prj::Message pm; pm.directory = directoryIn;
    
    msg::Message preMessage(msg::Response | msg::Pre | msg::New | msg::Project, pm);
    node.send(preMessage);
    
    //directoryIn has been verified to exist before this call.
    project = std::make_unique<prj::Project>();
    project->setSaveDirectory(directoryIn);
    project->initializeNew();
    
    updateTitle();
    
    msg::Message postMessage(msg::Response | msg::Post | msg::New | msg::Project, pm);
    node.send(postMessage);
  }
  
  void openProject(const std::string &directoryIn)
  {
    prj::Message pm; pm.directory = directoryIn;
    
    msg::Message preMessage(msg::Response | msg::Pre | msg::Open | msg::Project);
    node.send(preMessage);
    
    assert(!project);
    //directoryIn has been verified to exist before this call.
    project = std::make_unique<prj::Project>();
    project->setSaveDirectory(directoryIn);
    project->open();
    
    updateTitle();
    
    msg::Message postMessage(msg::Response | msg::Post | msg::Open | msg::Project, pm);
    node.send(postMessage);
    
    node.sendBlocked(msg::Message(msg::Request | msg::Project | msg::Update | msg::Visual));
  }
  
  void closeProject()
  {
    //something here for modified project.
    if (!project) return;
    
    prj::Message pm; pm.directory = project->getSaveDirectory();
    
    msg::Message preMessage(msg::Response | msg::Pre | msg::Close | msg::Project, pm);
    node.send(preMessage);
    
    project.reset();
    
    msg::Message postMessage(msg::Response | msg::Post | msg::Close | msg::Project, pm);
    node.send(postMessage);
  }
  
  void updateTitle()
  {
    if (!project) return;
    std::filesystem::path path = project->getSaveDirectory();
    QString title = tr("CadSeer --") + QString::fromStdString((--path.end())->u8string()) + "--";
    mainWindow.setWindowTitle(title);
  }
  
  void newProjectRequestDispatched(const msg::Message &messageIn)
  {
    prj::Message pMessage = messageIn.getPRJ();
    
    std::filesystem::path path = pMessage.directory;
    if (!std::filesystem::create_directory(path))
    {
      QMessageBox::critical(&mainWindow, tr("Failed Building Directory: "), QString::fromStdString(path.string()));
      return;
    }
    
    if(project) closeProject();
    
    createNewProject(path.string());
  }
  
  void openProjectRequestDispatched(const msg::Message &messageIn)
  {
    prj::Message pMessage = messageIn.getPRJ();
    
    std::filesystem::path path = pMessage.directory;
    if (!std::filesystem::exists(path))
    {
      QMessageBox::critical(&mainWindow, tr("Failed Finding Directory: "), QString::fromStdString(path.string()));
      return;
    }
    
    if(project) closeProject();
    
    openProject(path.string());
  }
  
  void closeProjectRequestDispatched(const msg::Message&)
  {
    if (project) closeProject();
  }
  
  void ProjectDialogRequestDispatched(const msg::Message&)
  {
    dlg::Project dialog(&mainWindow);
    if (dialog.exec() == QDialog::Accepted)
    {
      dlg::Project::Result r = dialog.getResult();
      if (r == dlg::Project::Result::Open || r == dlg::Project::Result::Recent)
      {
        closeProject();
        openProject(dialog.getDirectory().string());
      }
      if (r == dlg::Project::Result::New)
      {
        closeProject();
        createNewProject(dialog.getDirectory().string());
      }
    }
  }
  
  void AboutDialogRequestDispatched(const msg::Message &)
  {
    auto dialog = std::make_unique<dlg::About>(&mainWindow);
    dialog->exec();
  }
  
  void setupDispatcher()
  {
    sift.insert
    (
      {
        std::make_pair
        (
          msg::Request | msg::New | msg::Project
          , std::bind(&Stow::newProjectRequestDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::Open | msg::Project
          , std::bind(&Stow::openProjectRequestDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::Close | msg::Project
          , std::bind(&Stow::closeProjectRequestDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::Project | msg::Dialog
          , std::bind(&Stow::ProjectDialogRequestDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::About | msg::Dialog
          , std::bind(&Stow::AboutDialogRequestDispatched, this, std::placeholders::_1)
        )
      }
    );
  }
};

Application::Application(int &argc, char **argv) :
  QApplication(argc, argv)
{
  qRegisterMetaType<msg::Message>("msg::Message");
  setOrganizationName("blobfish");
  setOrganizationDomain("blobfish.org"); //doesn't exist.
  setApplicationName("cadseer");
  git2::start();
  
  using namespace std::filesystem;
  //get config path and check for first run
  path homePath = path(QDir::homePath().toStdString());
  assert(exists(homePath));
  if (!exists(homePath))
  {
    bail(tr("No Home Path"));
    return;
  }
  
  configPath = homePath / path(".CadSeer");
  if (!exists(configPath))
  {
    firstRun = true;
    if (!std::filesystem::create_directory(configPath))
    {
      bail(tr("Failed To Create Config Directory"));
      return;
    }
  }
}

Application::~Application()
{
  git2::stop();
}

void Application::bail(const QString &message)
{
  QMessageBox::critical(0, tr("CadSeer"), message);
  QTimer::singleShot(0, this, SLOT(quit()));
}

void Application::appStartSlot()
{
  //some day pass the preferences file from user home directory to the manager constructor.
  if (!prf::manager().isOk())
  {
    bail(tr("Preferences Failed To Load"));
    return;
  }
  
  stow = std::make_unique<Stow>(*this);
  
  //menu manager has to be done after preferences
  mnu::manager().loadMenu(prf::manager().getMenuConfigPath());
  initializeSpaceball();
  cmd::manager(); //just to construct the singleton and get it ready for messages.
  //a project might be loaded before we launch the message queue.
  //if so then don't do the dialog.
  if (!stow->project)
  {
    if (firstRun)
    {
      firstRun = false;
      dlg::Preferences dialog(&prf::manager(), &stow->mainWindow);
      dialog.setModal(true);
      dialog.exec();
    }
    
    dlg::Project dialog(this->getMainWindow());
    if (dialog.exec() == QDialog::Accepted)
    {
      dlg::Project::Result r = dialog.getResult();
      if (r == dlg::Project::Result::Open || r == dlg::Project::Result::Recent)
      {
        stow->openProject(dialog.getDirectory().string());
      }
      if (r == dlg::Project::Result::New)
      {
        stow->createNewProject(dialog.getDirectory().string());
      }
    }
    else
    {
      this->quit();
    }
  }
  
  getMainWindow()->getViewer()->setFocus();
}

void Application::quittingSlot()
{
#ifdef SPNAV_PRESENT
  spnav_close();
#endif
  
  if (stow->project) stow->closeProject(); //sends out project closed message
  stow->node.sendBlocked(msg::Response | msg::Pre | msg::Application | msg::Close);
}

void Application::queuedMessage(const msg::Message &message)
{
  QMetaObject::invokeMethod(this, "messageSlot", Qt::QueuedConnection, Q_ARG(msg::Message, message));
}

void Application::messageSlot(const msg::Message &messageIn)
{
  //can't block, message might be open file or something we need to handle here.
  stow->node.send(messageIn);
}

bool Application::notify(QObject* receiver, QEvent* e)
{
  try
  {
    bool outDefault = QApplication::notify(receiver, e);
    if (e->type() == vwr::MotionEvent::Type)
    {
      vwr::MotionEvent *motionEvent = dynamic_cast<vwr::MotionEvent*>(e);
      if
      (
        (!motionEvent) ||
        (motionEvent->isHandled())
      )
        return true;
        
      //make a new event and post to parent.
      vwr::MotionEvent *newEvent = new vwr::MotionEvent(*motionEvent);
      QObject *theParent = receiver->parent();
      if (!theParent || theParent == &stow->mainWindow)
        postEvent(stow->mainWindow.getViewer(), newEvent);
      else
        postEvent(theParent, newEvent);
      return true;
    }
    else if(e->type() == vwr::ButtonEvent::Type)
    {
      vwr::ButtonEvent *buttonEvent = dynamic_cast<vwr::ButtonEvent*>(e);
      if
      (
        (!buttonEvent) ||
        (buttonEvent->isHandled())
      )
        return true;
        
      //make a new event and post to parent.
      vwr::ButtonEvent *newEvent = new vwr::ButtonEvent(*buttonEvent);
      QObject *theParent = receiver->parent();
      if (!theParent || theParent == &stow->mainWindow)
        postEvent(stow->mainWindow.getViewer(), newEvent);
      else
        postEvent(theParent, newEvent);
      return true;
    }
    else
      return outDefault;
  }
  catch(...)
  {
    std::cerr << "unhandled exception in Application::notify" << std::endl;
  }
  return false;
}

void Application::initializeSpaceball()
{
#ifdef SPNAV_PRESENT
    if (!mainWindow)
        return;

    vwr::registerEvents();

    if (spnav_open() == -1)
    {
      std::cout << "No spaceball found" << std::endl;
    }
    else
    {
      std::cout << "Spaceball found" << std::endl;
      spaceballPresent = true;
      QTimer *spaceballTimer = new QTimer(this);
      spaceballTimer->setInterval(1000/30); //30 times a second
      connect(spaceballTimer, &QTimer::timeout, this, &Application::spaceballPollSlot);
      spaceballTimer->start();
    }
#endif
}

void Application::spaceballPollSlot()
{
#ifdef SPNAV_PRESENT
  spnav_event navEvent;
  if (!spnav_poll_event(&navEvent))
    return;
  
  QWidget *currentWidget = qApp->focusWidget();
  if (!currentWidget)
    return;

  if (navEvent.type == SPNAV_EVENT_MOTION)
  {
    vwr::MotionEvent *qEvent = new vwr::MotionEvent();
    qEvent->setTranslations(navEvent.motion.x, navEvent.motion.y, navEvent.motion.z);
    qEvent->setRotations(navEvent.motion.rx, navEvent.motion.ry, navEvent.motion.rz);
    this->postEvent(currentWidget, qEvent);
    return;
  }

  if (navEvent.type == SPNAV_EVENT_BUTTON)
  {
    vwr::ButtonEvent *qEvent = new vwr::ButtonEvent();
    qEvent->setButtonNumber(navEvent.button.bnum);
    if (navEvent.button.press == 1)
      qEvent->setButtonStatus(vwr::BUTTON_PRESSED);
    else
      qEvent->setButtonStatus(vwr::BUTTON_RELEASED);
    this->postEvent(currentWidget, qEvent);
    return;
  }
#endif
}

prj::Project* Application::getProject()
{
  return stow->project.get();
}

MainWindow* Application::getMainWindow()
{
  return &stow->mainWindow;
}

const std::filesystem::path& Application::getApplicationDirectory()
{
  assert(std::filesystem::exists(configPath));
  return configPath;
}

QSettings& Application::getUserSettings()
{
  static QSettings *out = nullptr;
  if (!out)
  {
    std::filesystem::path path = getApplicationDirectory();
    path /= "QSettings.ini";
    out = new QSettings(QString::fromStdString(path.string()), QSettings::IniFormat, this);
  }
  return *out;
}

Application* app::instance()
{
  return static_cast<Application*>(qApp);
}

WaitCursor::WaitCursor()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
}

WaitCursor::~WaitCursor()
{
  QApplication::restoreOverrideCursor();
}

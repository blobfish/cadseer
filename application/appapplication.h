/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef APP_APPLICATION_H
#define APP_APPLICATION_H

#include <memory>
#include <filesystem>

#include <QApplication>

class QSettings;

namespace prj{class Project;}
namespace msg{struct Message;}

namespace app
{
class MainWindow;

class Application : public QApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc, char **argv);
    ~Application();
    bool notify(QObject * receiver, QEvent * e) override;
    void initializeSpaceball();
    prj::Project* getProject();
    MainWindow* getMainWindow();
    const std::filesystem::path& getApplicationDirectory();
    QSettings& getUserSettings();
    void queuedMessage(const msg::Message&); //queue message into qt event loop
public Q_SLOTS:
    void quittingSlot();
    void appStartSlot();
    void messageSlot(const msg::Message&); //for queued connection
    void spaceballPollSlot();
private:
    struct Stow;
    std::unique_ptr<Stow> stow;
    std::filesystem::path configPath;
    bool firstRun = false;
    void bail(const QString&);
};

Application* instance();

struct WaitCursor
{
  WaitCursor();
  ~WaitCursor();
};
}

#endif // APP_APPLICATION_H

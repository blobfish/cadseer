/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project/prjproject.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "feature/ftrinert.h"
#include "command/cmdanchor.h"

using namespace cmd;

Anchor::Anchor()
: Base("cmd::Anchor")
{
  isEdit = false;
  isFirstRun = true;
}

Anchor::~Anchor() = default;

std::string Anchor::getStatusMessage()
{
  return QObject::tr("Select geometry for anchor feature").toStdString();
}

void Anchor::activate()
{
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  sendDone();
}

void Anchor::deactivate()
{
  isActive = false;
}

bool Anchor::isValidSelection(const slc::Message &mIn)
{
  if (!slc::isObjectType(mIn.type)) return false;
  if (mIn.featureType != ftr::Type::Inert) return false;
  return true;
}

void Anchor::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  
  std::vector<slc::Message> tm; //target messages
  for (const auto &c : cs)
  {
    auto m = slc::EventHandler::containerToMessage(c);
    if (isValidSelection(m)) tm.push_back(m);
    else node->sendBlocked(msg::buildStatusMessage("Skipping non-inert feature", 2.0));
  }
  
  for (const auto &m : tm)
  {
    ftr::Inert::Feature *feature = dynamic_cast<ftr::Inert::Feature*>(project->findFeature(m.featureId));
    feature->updateAnchor();
  }
}

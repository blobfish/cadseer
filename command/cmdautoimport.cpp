/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <filesystem>
#include <unordered_set>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "project/prjmessage.h"
#include "preferences/preferencesXML.h"
#include "preferences/prfmanager.h"
#include "message/msgnode.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvautoimport.h"
#include "command/cmdautoimport.h"

using namespace cmd;

struct AutoImport::Stow
{
  cmd::AutoImport &command;
  cmv::AutoImport *view = nullptr;
  std::filesystem::path path;
  std::unordered_set<std::filesystem::path> files;
  
  Stow(cmd::AutoImport &cIn)
  : command(cIn)
  {
  }
  
  void initDirectory()
  {
    files.clear();
    if (!std::filesystem::exists(path)) return;
    for (auto const& dir_entry : std::filesystem::directory_iterator{path})
    {
      if (!dir_entry.is_regular_file()) continue;
      files.insert(dir_entry);
    }
  };
  
  void scan()
  {
    if (!std::filesystem::exists(path)) return;
    for (auto const& dir_entry : std::filesystem::directory_iterator{path})
    {
      if (!dir_entry.is_regular_file()) continue;
      if (files.count(dir_entry) != 0) continue;
      files.insert(dir_entry);
      view->fileAdded(QString::fromStdString(dir_entry.path().filename().u8string()));
      prj::Message mOut;
      mOut.directory = dir_entry.path().u8string();
      command.node->send(msg::Message(msg::Request | msg::Import, mOut));
      command.node->send(msg::Mask(msg::Request | msg::Project | msg::Update));
    }
    
  }
};

AutoImport::AutoImport()
: Base("cmd::AutoImport")
, stow(std::make_unique<Stow>(*this))
{
  isEdit = false;
  isFirstRun = true;
  shouldUpdate = false;
}

AutoImport::~AutoImport() = default;

std::string AutoImport::getStatusMessage()
{
  return QObject::tr("Watch Directory To Auto Import Files").toStdString();
}

void AutoImport::activate()
{
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    
    stow->path = std::filesystem::u8path(std::string(prf::manager().rootPtr->project().lastDirectory().get()));
    if (!std::filesystem::exists(stow->path)) stow->path = app::instance()->getApplicationDirectory();
    stow->initDirectory();
    
    viewBase = std::make_unique<cmv::AutoImport>(this);
    stow->view = static_cast<cmv::AutoImport*>(viewBase.get());
    stow->view->setPath(QString::fromStdString(stow->path));
    
  }
  if (viewBase)
  {
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void AutoImport::deactivate()
{
  if (viewBase)
  {
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  isActive = false;
}

void AutoImport::changeDirectory(const std::string &freshDirectory)
{
  //we send setPath to the view to ensure path edit in view is correct.
  //user can paste an incorrect path into it and we get here.
  auto fresh = std::filesystem::u8path(freshDirectory);
  if (!std::filesystem::exists(fresh))
  {
    node->send(msg::buildStatusMessage(QObject::tr("Directory doesn't exist").toStdString(), 2.0));
    stow->view->setPath(QString::fromStdString(stow->path));
    return;
  }
  if (!std::filesystem::is_directory(fresh))
  {
    node->send(msg::buildStatusMessage(QObject::tr("Not a directory").toStdString(), 2.0));
    stow->view->setPath(QString::fromStdString(stow->path));
    return;
  }
  stow->path = fresh;
  stow->initDirectory();
  stow->view->setPath(QString::fromStdString(stow->path));
  prf::manager().rootPtr->project().lastDirectory() = stow->path.u8string();
  prf::manager().saveConfig();
}

void AutoImport::scan()
{
  stow->scan();
}

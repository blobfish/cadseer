/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include "application/appapplication.h"
#include "application/appmainwindow.h"
#include "viewer/vwrwidget.h"
#include "project/prjproject.h"
#include "feature/ftrbase.h"
#include "feature/ftrpick.h"
#include "parameter/prmparameter.h"
#include "selection/slcmanager.h"
#include "selection/slceventhandler.h"
#include "selection/slcmessage.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "message/msgmessage.h"
#include "tools/featuretools.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvbase.h"
#include "command/cmdbase.h"

using namespace cmd;

Base::Base()
: node(std::make_unique<msg::Node>())
, sift(std::make_unique<msg::Sift>())
{
  node->connect(msg::hub());
  node->setHandler(std::bind(&msg::Sift::receive, sift.get(), std::placeholders::_1));
  sift->name = "cmd::Base"; //set derived class or use other constructor
  
  sift->insert
  (
    {
      std::make_pair
      (
        msg::Response | msg::Command | msg::View | msg::Update
        , std::bind(&Base::splitterDispatched, this, std::placeholders::_1)
      )
    }
  );
  
  application = app::instance(); assert(application);
  mainWindow = application->getMainWindow(); assert(mainWindow);
  project = application->getProject(); assert(project);
  selectionManager = mainWindow->getSelectionManager(); assert(selectionManager);
  viewer = mainWindow->getViewer(); assert(viewer);
  eventHandler = viewer->getSelectionEventHandler(); assert(eventHandler);
  
  isActive = false;
}

Base::Base(const std::string &siftNameIn)
: Base()
{
  sift->name = siftNameIn;
}

Base::~Base()
{
}

void Base::splitterDispatched(const msg::Message &mIn)
{
  if (!viewBase || !isActive)
    return;
  assert(mIn.isCMV());
  viewBase->setPaneWidth(mIn.getCMV().paneWidth);
}

void Base::sendDone()
{
  msg::Message mOut(msg::Mask(msg::Request | msg::Command | msg::Done));
  app::instance()->queuedMessage(mOut);
}

std::string Base::indexTag(std::string_view sView, std::size_t index)
{
  return std::string(sView) + std::to_string(index);
}

/*! @brief Clear the pick parameter with tag of feature.
 * 
 * @param target Feature that receives inputs
 * @param tag Identifying link between feature picks and project graph edges.
 * @param msgsIn Selection messages for feature inputs.
 * @details Clear the pick parameter with tag of feature.
 * Simple convenience function.
 */
void Base::clearPick(const ftr::Base &target, std::string_view tag)
{
  target.getParameter(tag)->setValue(ftr::Picks());
}

/*! @brief Make selection messages inputs for feature
 *
 * @param target Feature that receives inputs
 * @param tag Identifying link between feature picks and project graph edges.
 * @param msgsIn Selection messages for feature inputs.
 * @param extraTags Additional tags that are added to the graph edge in addition to 'tag'.
 * @details Make selection messages inputs for feature.
 * messages get converted to ftr::Pick. the tag gets an index
 * appended and that is assigned to both the pick and the project
 * graph edge. asserts if target feature has no parameter with 'tag'.
 * The parameter will reflect msgsIn. Meaning if msgIn is empty so will
 * the parameter.
 */
void Base::setPicks(const ftr::Base &target, std::string_view tag, const slc::Messages &msgsIn, const ftr::InputType &extraTags)
{
  ftr::Picks picks;
  for (const auto &m : msgsIn)
  {
    const ftr::Base *lf = project->findFeature(m.featureId); assert(lf);
    ftr::Pick out = tls::convertToPick(m, *lf, project->getShapeHistory());
    out.tag = indexTag(tag, picks.size());
    project->connect(lf->getId(), target.getId(), extraTags + out.tag);
    picks.push_back(out);
  }
  target.getParameter(tag)->setValue(picks);
}

/*! @brief Make selection messages inputs for feature
 *
 * @param target Feature that receives inputs
 * @param tag Identifying link between feature picks and project graph edges.
 * @param msgsIn Selection messages for feature inputs.
 * @param extraTags Additional tags that are added to the graph edge in addition to 'tag'.
 * @details Make selection messages inputs for feature.
 * messages get converted to ftr::Pick. the tag gets an index
 * appended and that is assigned to both the pick and the project
 * graph edge. asserts if target feature has no parameter with 'tag'.
 * The parameter will reflect msgsIn. Meaning if msgIn is empty so will
 * the parameter. Only difference with this function vs setPicks is this
 * call project->connectInsert that allows inserting a feature into 
 * the project graph. Useful for alter features in a 'make current' context.
 */
void Base::setPicksInsert(const ftr::Base &target, std::string_view tag, const slc::Messages &msgsIn, const ftr::InputType &extraTags)
{
  ftr::Picks picks;
  for (const auto &m : msgsIn)
  {
    const ftr::Base *lf = project->findFeature(m.featureId); assert(lf);
    ftr::Pick out = tls::convertToPick(m, *lf, project->getShapeHistory());
    out.tag = indexTag(tag, picks.size());
    project->connectInsert(lf->getId(), target.getId(), extraTags + out.tag);
    picks.push_back(out);
  }
  target.getParameter(tag)->setValue(picks);
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project/prjproject.h"
#include "annex/annseershape.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "feature/ftrbase.h"
#include "tools/occtools.h"
#include "command/cmdcompound.h"

using namespace cmd;

Compound::Compound()
: Base("cmd::Compound")
{
  isEdit = false;
  isFirstRun = true;
}

Compound::~Compound() = default;

std::string Compound::getStatusMessage()
{
  return QObject::tr("Select geometry for compound inert feature").toStdString();
}

void Compound::activate()
{
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  sendDone();
}

void Compound::deactivate()
{
  isActive = false;
}

void Compound::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  
  occt::ShapeVector shapes;
  for (const auto &c : cs)
  {
    auto m = slc::EventHandler::containerToMessage(c);
    const auto *f = project->findFeature(m.featureId); assert(f);
    if (!f->hasAnnex(ann::Type::SeerShape)) continue;
    const auto &ss = f->getAnnex<ann::SeerShape>(); if (ss.isNull()) continue;
    if (slc::isObjectType(m.type))
    {
      auto roots = ss.useGetNonCompoundChildren();
      shapes.insert(shapes.end(), roots.begin(), roots.end());
      node->send(msg::buildHideThreeD(m.featureId));
      node->send(msg::buildHideOverlay(m.featureId));
    }
    else if (slc::isShapeType(m.type))
    {
      shapes.push_back(ss.getOCCTShape(m.shapeId));
      node->send(msg::buildHideThreeD(m.featureId));
      node->send(msg::buildHideOverlay(m.featureId));
    }
  }
  if (shapes.empty())
  {
    node->send(msg::buildStatusMessage("No shapes to compound", 2.0));
    return;
  }
  
  auto c = static_cast<TopoDS_Compound>(occt::ShapeVectorCast(shapes));
  project->addOCCShape(c, QObject::tr("Compound").toStdString());
}

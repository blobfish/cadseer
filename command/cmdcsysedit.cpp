/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include <boost/signals2/shared_connection_block.hpp>

#include "globalutilities.h"
#include "library/lbrcsysdragger.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "application/appmessage.h"
#include "project/prjproject.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "selection/slcmessage.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrbase.h"
#include "annex/annseershape.h"
#include "annex/anncsysdragger.h"
#include "command/cmdcsysedit.h"

using namespace cmd;

CSysEdit::CSysEdit() : Base("cmd::CSysEdit")
{
  setupDispatcher();
}

CSysEdit::~CSysEdit()
{

}

std::string CSysEdit::getStatusMessage()
{
  if (type == Type::Origin)
    return "select point for new origin";
  if (type == Type::Vector)
  {
    std::string draggerName = translateDragger->getName();
    std::string axisLabel = "none";
    if (draggerName == "xTranslate")
      axisLabel = "X";
    else if (draggerName == "yTranslate")
      axisLabel = "Y";
    else if (draggerName == "zTranslate")
      axisLabel = "Z";
    
    std::ostringstream stream;
    stream << "Select 2 points or object for new " << 
      axisLabel << " vector direction";
      
    return stream.str();
  }
  return "unknown condition";
}

void CSysEdit::setupDispatcher()
{
  sift->insert
  (
    {
      std::make_pair
      (
        msg::Response | msg::Post | msg::Selection | msg::Add
        , std::bind(&CSysEdit::selectionAdditionDispatched, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Response | msg::Pre | msg::Selection | msg::Remove
        , std::bind(&CSysEdit::selectionSubtractionDispatched, this, std::placeholders::_1)
      )
    }
  );
}


void CSysEdit::selectionAdditionDispatched(const msg::Message &messageIn)
{
  if (!isActive)
    return;
  
  slc::Message sMessage = messageIn.getSLC();
  
  if (!slc::has(messages, sMessage))
    slc::add(messages, sMessage);
  
  analyzeSelections();
}

void CSysEdit::selectionSubtractionDispatched(const msg::Message &messageIn)
{
  if (!isActive)
    return;
  
  slc::Message sMessage = messageIn.getSLC();
  
  if (slc::has(messages, sMessage))
    slc::remove(messages, sMessage);
  
  analyzeSelections();
}

/* We don't have to worry about the csys parameter being linked to another csys parameter
 * or it being linked to an expression. In those cases the CSysDragger is hidden and we
 * can't get here.
 */
void CSysEdit::analyzeSelections()
{
  switch (type)
  {
    case Type::Origin: {analyzeOrigin(); break;}
    case Type::Vector: {analyzeVector(); break;}
    case Type::None: {std::cout << "Type error: in CSysEdit::analyzeSelections"; break;}
  };
}

void CSysEdit::analyzeOrigin()
{
  if (messages.empty()) return;
  if (messages.size() > 1) {std::cout << "Error in message size: CSysEdit::analyzeOrigin"; sendDone(); return;}
  
  osg::Matrixd derivedSystem = csysDragger->getMatrix(); //start from dragger system
  const ftr::Base* selectedFeature = project->findFeature(messages.front().featureId); assert(selectedFeature);
  
  //first derive a system from the selection.
  auto sType = messages.front().type;
  switch (sType)
  {
    case slc::Type::Object: case slc::Type::Feature:
    {
      if (auto freshSystem = tls::gleanSystem(messages); freshSystem)
      {
        derivedSystem = *freshSystem;
        break;
      }
      if (auto ops = selectedFeature->getParameters(prm::Tags::Origin); !ops.empty())
      {
        derivedSystem.setTrans(ops.front()->getVector());
        break;
      }
      break;
    }
    case slc::Type::Face: case slc::Type::Edge:
    {
      if (auto freshSystem = tls::gleanSystem(messages); freshSystem)
      {
        derivedSystem = *freshSystem;
        break;
      }
      break;
    }
    case slc::Type::StartPoint:
    case slc::Type::EndPoint:
    case slc::Type::MidPoint:
    case slc::Type::CenterPoint:
    case slc::Type::QuadrantPoint:
    case slc::Type::NearestPoint:
    case slc::Type::ScreenPoint:
    {
      derivedSystem.setTrans(messages.at(0).pointLocation);
      break;
    }
    default:
    {
      node->sendBlocked(msg::buildStatusMessage("Failed to find origin from selection", 2.0));
      sendDone();
      break;
    }
  };
  
  //now we should have the new system or maybe the original, doesn't matter.
  //This might be the current dragger that lives in the view and we have
  //to process that special as it isn't a ann::CSysDragger.
  boost::uuids::uuid id = gu::getId(csysDragger);
  if (id.is_nil()) //current system in view
  {
    csysDragger->setMatrix(derivedSystem);
  }
  else
  {
    ftr::Base *draggerFeature = dynamic_cast<ftr::Base*>(project->findFeature(id)); assert(draggerFeature);
    assert(draggerFeature->hasAnnex(ann::Type::CSysDragger));
    ann::CSysDragger &da = draggerFeature->getAnnex<ann::CSysDragger>(ann::Type::CSysDragger);
    da.draggerUpdate(derivedSystem, ann::CSysDragger::RespectLink);
  }
  
  sendDone();
}

void CSysEdit::analyzeVector()
{
  if (messages.empty()) return;
  if (messages.size() > 2) {std::cout << "Error in message size: CSysEdit::analyzeVector"; sendDone(); return;}
  
  auto gleanVector = [&](const slc::Message &mIn) -> std::optional<osg::Vec3d>
  {
    ftr::Base *f = project->findFeature(mIn.featureId); assert(f);
    switch (mIn.type)
    {
      case slc::Type::Object: case slc::Type::Feature:
      {
        auto params = f->getParameters(prm::Tags::Direction);
        if (!params.empty()) return params.front()->getVector();
        break;
      }
      case slc::Type::Edge: case slc::Type::Face:
      {
        const ann::SeerShape &seerShape = f->getAnnex<ann::SeerShape>(); assert(!seerShape.isNull());
        gp_Vec vector;
        bool results;
        std::tie(vector, results) = occt::gleanVector(seerShape.getOCCTShape(mIn.shapeId), gu::toOcc(mIn.pointLocation).XYZ());
        if (!results) return std::nullopt;
        osg::Vec3d direction = gu::toOsg(vector);
        if (!direction.valid() || direction.length() < Precision::Confusion()) return std::nullopt;
        return direction;
        break;
      }
      default:{break;} //default is std::nullopt;
    }
    
    return std::nullopt;
  };
  
  auto gleanPoint = [&](const slc::Message &mIn) -> std::optional<osg::Vec3d>
  {
    ftr::Base *f = project->findFeature(mIn.featureId); assert(f);
    switch (mIn.type)
    {
      case slc::Type::Object: case slc::Type::Feature:
      {
        auto origins = f->getParameters(prm::Tags::Origin);
        if (!origins.empty()) return origins.front()->getVector();
        auto systems = f->getParameters(prm::Tags::CSys);
        if (!systems.empty()) return systems.front()->getMatrix().getTrans();
        break;
      }
      case slc::Type::StartPoint:
      case slc::Type::EndPoint:
      case slc::Type::MidPoint:
      case slc::Type::CenterPoint:
      case slc::Type::QuadrantPoint:
      case slc::Type::NearestPoint:
      case slc::Type::ScreenPoint:
      {
        return mIn.pointLocation; //point location is from occt, so accurate.
        break;
      }
      default:{break;} //default is std::nullopt;
    }
    return std::nullopt;
  };
  
  if (messages.size() == 1)
  {
    auto gleaned = gleanVector(messages.front());
    if (gleaned)
    {
      updateToVector(*gleaned);
      sendDone();
      return;
    }
    return; //couldn't get a vector from 1 pick so continue on with command and try 2 picks.
  }
  
  auto p0 = gleanPoint(messages.at(0));
  auto p1 = gleanPoint(messages.at(1));
  
  if (!p0 || !p1)
  {
    node->sendBlocked(msg::buildStatusMessage("Invalid point", 2.0));
    sendDone();
    return;
  }
  
  //head to tail pick.
  osg::Vec3d toVector = *p0 - *p1;
  if (toVector.valid() && toVector.length() > Precision::Confusion())
  {
    toVector.normalize();
    updateToVector(toVector);
    sendDone();
    return;
  }
  node->sendBlocked(msg::buildStatusMessage("Failed To Get Vector", 2.0));
  sendDone();
}

void CSysEdit::updateToVector(const osg::Vec3d& toVector)
{
  osg::Vec3d freshX, freshY, freshZ;
  osg::Matrixd originalMatrix = csysDragger->getMatrix();
  if (translateDragger->getName() == "xTranslate")
  {
    if
    (
      (std::fabs(toVector * gu::getYVector(originalMatrix))) <
      (std::fabs(toVector * gu::getZVector(originalMatrix)))
    )
    {
      freshZ = toVector ^ gu::getYVector(originalMatrix);
      freshZ.normalize();
      freshY = freshZ ^ toVector;
      freshY.normalize();
    }
    else
    {
      freshY = gu::getZVector(originalMatrix) ^ toVector;
      freshY.normalize();
      freshZ = toVector ^ freshY;
      freshZ.normalize();
    }
    freshX = toVector;
  }
  else if (translateDragger->getName() == "yTranslate")
  {
    if
    (
      (std::fabs(toVector * gu::getXVector(originalMatrix))) <
      (std::fabs(toVector * gu::getZVector(originalMatrix)))
    )
    {
      freshZ = gu::getXVector(originalMatrix) ^ toVector;
      freshZ.normalize();
      freshX = toVector ^ freshZ;
      freshX.normalize();
    }
    else
    {
      freshX = toVector ^ gu::getZVector(originalMatrix);
      freshX.normalize();
      freshZ = freshX ^ toVector;
      freshZ.normalize();
    }
    freshY = toVector;
  }
  else if (translateDragger->getName() == "zTranslate")
  {
    if
    (
      (std::fabs(toVector * gu::getXVector(originalMatrix))) <
      (std::fabs(toVector * gu::getYVector(originalMatrix)))
    )
    {
      freshY = toVector ^ gu::getXVector(originalMatrix);
      freshY.normalize();
      freshX = freshY ^ toVector;
      freshX.normalize();
    }
    else
    {
      freshX = gu::getYVector(originalMatrix) ^ toVector;
      freshX.normalize();
      freshY = toVector ^ freshX;
      freshY.normalize();
    }
    freshZ = toVector;
  }
  
  osg::Matrixd fm = originalMatrix;
  fm(0,0) = freshX.x(); fm(0,1) = freshX.y(); fm(0,2) = freshX.z();
  fm(1,0) = freshY.x(); fm(1,1) = freshY.y(); fm(1,2) = freshY.z();
  fm(2,0) = freshZ.x(); fm(2,1) = freshZ.y(); fm(2,2) = freshZ.z();
  
  boost::uuids::uuid id = gu::getId(csysDragger);
  if (id.is_nil() || (!csysDragger->isLinked())) //current system
  {
    csysDragger->setMatrix(fm);
    return;
  }

  ftr::Base *feature = dynamic_cast<ftr::Base*>(project->findFeature(id));
  assert(feature);
  if (feature->hasAnnex(ann::Type::CSysDragger))
  {
    osg::Matrixd diffMatrix = osg::Matrixd::inverse(originalMatrix) * fm;
    ann::CSysDragger &da = feature->getAnnex<ann::CSysDragger>(ann::Type::CSysDragger);
    da.setCSys(da.parameter->getMatrix() * diffMatrix); //should move dragger also.
  }
}

void CSysEdit::activate()
{
  static const slc::Mask mask = slc::ObjectsEnabled | slc::AllPointsEnabled | slc::EndPointsSelectable | slc::FacesEnabled | slc::EdgesEnabled;
  
  if ((type == Type::Vector) && translateDragger)
    osgManipulator::setMaterialColor(translateDragger->getPickColor(), *translateDragger);
  if (type == Type::Origin)
    csysDragger->highlightOrigin();
  
  auto block = node->createBlocker();
  for (const auto &sMessage : messages)
  {
    msg::Message messageOut(msg::Message(msg::Request | msg::Selection | msg::Add, sMessage));
    node->send(messageOut);
  }
  app::Message am; am.toolbar = 0;
  node->send(msg::Message(msg::Request | msg::Toolbar | msg::Show, am));
  node->send(msg::buildSelectionMask(mask));
  isActive = true;
}

void CSysEdit::deactivate()
{
  if ((type == Type::Vector) && translateDragger)
    osgManipulator::setMaterialColor(translateDragger->getColor(), *translateDragger);
  if (type == Type::Origin)
    csysDragger->unHighlightOrigin();
  
  isActive = false;
}

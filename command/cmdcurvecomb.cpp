/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "tools/featuretools.h"
#include "project/prjproject.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmconstants.h"
#include "feature/ftrcurvecomb.h"
#include "command/cmdcurvecomb.h"

using namespace cmd;

CurveComb::CurveComb()
: Base("cmd::CurveComb")
{
  isEdit = false;
  isFirstRun = true;
}

CurveComb::~CurveComb() = default;

std::string CurveComb::getStatusMessage()
{
  return QObject::tr("Select geometry for curve comb feature").toStdString();
}

void CurveComb::activate()
{
  isActive = true;
  go();
  sendDone();
}

void CurveComb::deactivate()
{
  isActive = false;
}

void CurveComb::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  for (const auto &c : cs)
  {
    if (c.selectionType != slc::Type::Edge && !slc::isObjectType(c.selectionType)) continue;
    auto m = slc::EventHandler::containerToMessage(c);
    ftr::Base *bf = project->findFeature(m.featureId);
    if (!bf->hasAnnex(ann::Type::SeerShape)) continue;
    auto *comb = project->addFeature(std::make_unique<ftr::CurveComb::Feature>());
    setPicks(*comb, prm::Tags::Picks, {m});
    shouldUpdate = true;
    node->sendBlocked(msg::buildStatusMessage("CurveComb Added", 2.0));
  }
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

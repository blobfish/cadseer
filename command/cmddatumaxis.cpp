/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "globalutilities.h"
#include "application/appapplication.h"
#include "application/appmainwindow.h"
#include "viewer/vwrwidget.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "selection/slcmessage.h"
#include "project/prjproject.h"
#include "tools/featuretools.h"
#include "feature/ftrinputtype.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrdatumaxis.h"
#include "annex/anncsysdragger.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvdatumaxis.h"
#include "command/cmddatumaxis.h"

using boost::uuids::uuid;
using namespace cmd;

DatumAxis::DatumAxis()
: Base()
, leafManager()
{
  feature = new ftr::DatumAxis::Feature();
  project->addFeature(std::unique_ptr<ftr::DatumAxis::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

DatumAxis::DatumAxis(ftr::Base *fIn)
: Base()
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::DatumAxis::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::DatumAxis>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

DatumAxis::~DatumAxis() {}

std::string DatumAxis::getStatusMessage()
{
  return QObject::tr("Select geometry for datum axis").toStdString();
}

void DatumAxis::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void DatumAxis::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  }
  isActive = false;
}

void DatumAxis::go()
{
  assert(project);
  
  auto isAPoint = [](const slc::Message &mIn) -> bool
  {
    if (slc::isPointType(mIn.type)) return true;
    if (mIn.featureType == ftr::Type::DatumPoint) return true;
    return false;
  };
  
  auto setAxisType = [&](ftr::DatumAxis::AxisType tIn)
  { feature->getParameter(ftr::DatumAxis::Tags::AxisType)->setValue(static_cast<int>(tIn)); };
  
  slc::Messages msgs;
  for (const auto &c : eventHandler->getSelections()) msgs.push_back(slc::EventHandler::containerToMessage(c));
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  
  if (msgs.size() == 2)
  {
    int pointCount = 0;
    for (const auto &s : msgs) if (isAPoint(s)) pointCount++;
    switch (pointCount)
    {
      case 1: {setAxisType(ftr::DatumAxis::PointNormal); setToPointNormal(msgs); break;}
      case 2: {setAxisType(ftr::DatumAxis::Points); setToPoints(msgs); break;}
      default: {setAxisType(ftr::DatumAxis::Intersection); setToIntersection(msgs); break;}
    }
    feature->getParameter(prm::Tags::AutoSize)->setValue(true);
    return;
  }

  if (msgs.size() == 1)
  {
    if (msgs.front().type == slc::Type::Face || msgs.front().type == slc::Type::Edge)
    {
      setAxisType(ftr::DatumAxis::Geometry);
      setToGeometry(msgs);
      feature->getParameter(prm::Tags::AutoSize)->setValue(true);
      return;
    }
    if (slc::isObjectType(msgs.front().type))
    {
      setAxisType(ftr::DatumAxis::Linked);
      setToLinked(msgs);
      return;
    }
  }
  
  //create a constant type. and launch command view
  setAxisType(ftr::DatumAxis::Constant);
  setToConstant();
  const auto &sys = viewer->getPresentSystem();
  feature->getParameter(prm::Tags::CSys)->setValue(sys);
  feature->getAnnex<ann::CSysDragger>(ann::Type::CSysDragger).resetDragger();
  feature->getParameter(prm::Tags::Origin)->setValue(sys.getTrans());
  feature->getParameter(prm::Tags::Direction)->setValue(gu::getZVector(sys));
  feature->getParameter(prm::Tags::Size)->setValue(viewer->getDiagonalLength() / 4.0);
  
  localUpdate();
  node->sendBlocked(msg::buildStatusMessage("Constant datum added at current system z axis", 2.0));
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  viewBase = std::make_unique<cmv::DatumAxis>(this);
}

void DatumAxis::setToConstant()
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
}

void DatumAxis::setToParameters()
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
}

void DatumAxis::setToLinked(const slc::Messages &msIn)
{
  assert(isActive);
  
  project->clearAllInputs(feature->getId());
  clearPick(*feature, ftr::DatumAxis::Tags::Linked);
  if (msIn.empty()) return;
  //the following is unusual. we have a different tag for parameter versus
  //what is in the pick itself and on the project graph edge.
  const ftr::Base &parent = *project->findFeature(msIn.front().featureId);
  auto pick = tls::convertToPick(msIn.front(), parent, project->getShapeHistory());
  pick.tag = indexTag(ftr::InputType::linkCSys, 0);
  feature->getParameter(ftr::DatumAxis::Tags::Linked)->setValue(pick);
  project->connectInsert(parent.getId(), feature->getId(), {pick.tag});
}

void DatumAxis::setToPoints(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::DatumAxis::Tags::Points, msIn);
}

void DatumAxis::setToIntersection(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::DatumAxis::Tags::Intersection, msIn);
}

void DatumAxis::setToGeometry(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::DatumAxis::Tags::Geometry, msIn);
}

void DatumAxis::setToPointNormal(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::DatumAxis::Tags::PointNormal, msIn);
}

void DatumAxis::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

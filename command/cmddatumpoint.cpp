/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <TopoDS.hxx>
#include <BRepAdaptor_Surface.hxx>

#include "globalutilities.h"
#include "tools/featuretools.h"
#include "tools/occtools.h"
#include "project/prjproject.h"
#include "viewer/vwrwidget.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "annex/annseershape.h"
#include "feature/ftrdatumpoint.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvdatumpoint.h"
#include "command/cmddatumpoint.h"

using namespace cmd;

DatumPoint::DatumPoint()
: Base("cmd::DatumPoint")
, leafManager()
{
  feature = new ftr::DatumPoint::Feature();
  project->addFeature(std::unique_ptr<ftr::DatumPoint::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

DatumPoint::DatumPoint(ftr::Base *fIn)
: Base("cmd::DatumPoint")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::DatumPoint::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::DatumPoint>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

DatumPoint::~DatumPoint() = default;

std::string DatumPoint::getStatusMessage()
{
  return QObject::tr("Select geometry for datumpoint feature").toStdString();
}

void DatumPoint::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
    if (!*isEdit)
      node->sendBlocked(msg::buildSelectionFreeze(feature->getId()));
  }
  else
    sendDone();
}

void DatumPoint::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
    node->sendBlocked(msg::buildSelectionThaw(feature->getId()));
  }
  isActive = false;
}

void DatumPoint::setToConstant()
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  clearPick(*feature, prm::Tags::Picks);
}

void DatumPoint::setToAtPoint(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, prm::Tags::Picks, msIn);
}

void DatumPoint::setToEdgeParameter(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, prm::Tags::Picks, msIn);
  
  //assign u parameter.
  const auto &picks = feature->getParameter(prm::Tags::Picks)->getPicks();
  auto *edgeParameter = feature->getParameter(ftr::DatumPoint::Tags::parameterU); assert(edgeParameter);
  if (msIn.empty() || picks.empty()) return;
  switch (msIn.front().type)
  {
    case slc::Type::NearestPoint: {edgeParameter->setValue(picks.front().u); break;}
    case slc::Type::Edge: case slc::Type::Wire: {edgeParameter->setValue(0.5);}
    default: {break;}
  };
}

void DatumPoint::setToFaceParameters(const slc::Messages &msIn)
{
  assert(isActive);
  
  project->clearAllInputs(feature->getId());
  setPicks(*feature, prm::Tags::Picks, msIn);
  const auto &thePicks = feature->getParameter(prm::Tags::Picks)->getPicks();
  if (msIn.empty() || msIn.front().type != slc::Type::Face || thePicks.empty()) return;
  const ftr::Base *lf = project->findFeature(msIn.front().featureId); assert(lf);
  const auto &face = TopoDS::Face(lf->getAnnex<ann::SeerShape>(ann::Type::SeerShape).getOCCTShape(msIn.front().shapeId));
  auto [u, v] = occt::normalize(face, thePicks.front().u, thePicks.front().v);
  auto *pu = feature->getParameter(ftr::DatumPoint::Tags::parameterU); assert(pu); pu->setValue(u);
  auto *pv = feature->getParameter(ftr::DatumPoint::Tags::parameterV); assert(pv); pv->setValue(v);
}

void DatumPoint::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void DatumPoint::go()
{
  auto setPointType = [&](ftr::DatumPoint::PointType ptIn)
  {
    auto *typePrm = feature->getParameter(ftr::DatumPoint::Tags::pointType); assert(typePrm);
    typePrm->setValue(static_cast<int>(ptIn));
  };
  
  node->sendBlocked(msg::buildShowThreeD(feature->getId()));
  node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  
  int initialSize = static_cast<int>(viewer->getDiagonalLength() / 20.0);
  initialSize = std::max(1, initialSize);
  feature->getParameter(prm::Tags::Size)->setValue(static_cast<double>(initialSize));
  
  const slc::Containers &cs = eventHandler->getSelections();
  if (cs.size() == 1)
  {
    auto m = slc::EventHandler::containerToMessage(cs.front());
    auto t = cs.front().selectionType;
    
    switch (t)
    {
      case slc::Type::Face:
      {
        setPointType(ftr::DatumPoint::PointType::FaceParameters);
        setToFaceParameters({m});
        node->sendBlocked(msg::buildStatusMessage("Face Parameter DatumPoint Created", 2.0));
        break;
      }
      case slc::Type::NearestPoint:
      case slc::Type::Wire:
      case slc::Type::Edge:
      {
        setPointType(ftr::DatumPoint::PointType::EdgeParameter);
        setToEdgeParameter({m});
        node->sendBlocked(msg::buildStatusMessage("Edge Parameter DatumPoint Created", 2.0));
        break;
      }
      case slc::Type::StartPoint:
      case slc::Type::EndPoint:
      case slc::Type::MidPoint:
      case slc::Type::CenterPoint:
      case slc::Type::QuadrantPoint:
      {
        setPointType(ftr::DatumPoint::PointType::AtPoint);
        setToAtPoint({m});
        node->sendBlocked(msg::buildStatusMessage("At Point DatumPoint Created", 2.0));
        break;
      }
      default: {break;}
    }
  }
  else
  {
    feature->getParameter(prm::Tags::Origin)->setValue(viewer->getPresentSystem().getTrans());
    node->sendBlocked(msg::buildStatusMessage("Invalid Pre-Selection", 2.0));
    viewBase = std::make_unique<cmv::DatumPoint>(this);
  }
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
}

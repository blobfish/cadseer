/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "tools/featuretools.h"
#include "application/appmainwindow.h"
#include "project/prjproject.h"
#include "viewer/vwrwidget.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvdatumsystem.h"
#include "annex/annseershape.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrinputtype.h"
#include "feature/ftrdatumsystem.h"
#include "command/cmddatumsystem.h"

using namespace cmd;
using namespace ftr::DatumSystem;
using boost::uuids::uuid;

DatumSystem::DatumSystem()
: Base("cmd::DatumSystem")
, leafManager()
{
  feature = new ftr::DatumSystem::Feature();
  auto nf = std::make_shared<ftr::DatumSystem::Feature>();
  project->addFeature(std::unique_ptr<ftr::DatumSystem::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

DatumSystem::DatumSystem(ftr::Base *fIn)
: Base("cmd::DatumSystem")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::DatumSystem::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::DatumSystem>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

DatumSystem::~DatumSystem() = default;

std::string DatumSystem::getStatusMessage()
{
  return QObject::tr("Select geometry for DatumSystem feature").toStdString();
}

void DatumSystem::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void DatumSystem::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  }
  isActive = false;
}

void DatumSystem::setConstant()
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
}

void DatumSystem::setLinked(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  if (msIn.empty())
  {
    clearPick(*feature, ftr::DatumSystem::Tags::Linked);
    return;
  }
  const ftr::Base &parent = *project->findFeature(msIn.front().featureId);
  auto pick = tls::convertToPick(msIn.front(), parent, project->getShapeHistory());
  pick.tag = indexTag(ftr::InputType::linkCSys, 0);
  feature->getParameter(ftr::DatumSystem::Tags::Linked)->setValue(pick);
  project->connectInsert(parent.getId(), feature->getId(), {pick.tag});
}

void DatumSystem::set3Points(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::DatumSystem::Tags::Points, msIn);
}

void DatumSystem::setShapeInfer(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::DatumSystem::Tags::ShapeInfer, msIn);
  
  //conversion to pick will resolve shape history to vertices for end points.
  //we need to keep curve for frenet.
  auto *pp = feature->getParameter(ftr::DatumSystem::Tags::ShapeInfer); assert(pp);
  if (pp->getPicks().empty() || msIn.empty()) return;
  
  if (msIn.front().type == slc::Type::StartPoint || msIn.front().type == slc::Type::EndPoint)
  {
    auto pick = pp->getPicks().front();
    pick.shapeHistory = project->getShapeHistory().createDevolveHistory(msIn.front().shapeId);
    pp->setValue({pick});
  }
}

void DatumSystem::setDefine(const slc::Messages &originMsgs, const slc::Messages &axis0Msgs, const slc::Messages &axis1Msgs)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, prm::Tags::Origin, originMsgs);
  setPicks(*feature, ftr::DatumSystem::Tags::Axis0, axis0Msgs);
  setPicks(*feature, ftr::DatumSystem::Tags::Axis1, axis1Msgs);
}

void DatumSystem::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void DatumSystem::go()
{
  assert(project);
  
  slc::Messages targets;
  for (const auto &c : eventHandler->getSelections())
    targets.push_back(eventHandler->containerToMessage(c));
  
  node->sendBlocked(msg::buildShowThreeD(feature->getId()));
  node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  
  auto setSystemType = [&](ftr::DatumSystem::SystemType tIn)
  {
    auto *param = feature->getParameter(ftr::DatumSystem::Tags::SystemType);
    param->setValue(static_cast<int>(tIn));
  };
  
  auto goConstantSystem = [&]()
  {
    setSystemType(ftr::DatumSystem::Constant);
    setConstant();
    feature->getParameter(prm::Tags::CSys)->setValue(viewer->getPresentSystem());
    feature->getParameter(prm::Tags::Size)->setValue(viewer->getDiagonalLength() / 4.0);
    localUpdate();
    node->sendBlocked(msg::buildStatusMessage("invalid pre selection", 2.0));
    viewBase = std::make_unique<cmv::DatumSystem>(this);
  };
  
  if (targets.size() == 1)
  {
    if (slc::isObjectType(targets.front().type))
    {
      setSystemType(ftr::DatumSystem::Linked);
      setLinked(targets);
      node->sendBlocked(msg::buildStatusMessage("Linked System Created", 2.0));
    }
    else
    {
      setSystemType(ftr::DatumSystem::ShapeInfer);
      setShapeInfer(targets);
      feature->getParameter(prm::Tags::AutoSize)->setValue(true);
      node->sendBlocked(msg::buildStatusMessage("Shape Infer System Created", 2.0));
    }
  }
  else if (targets.size() == 3)
  {
    bool allPoints = true;
    for (const auto &m : targets)
    {
      if (!slc::isPointType(m.type))
      {
        allPoints = false;
        break;
      }
    }
    if (allPoints)
    {
      setSystemType(ftr::DatumSystem::Through3Points);
      set3Points(targets);
      feature->getParameter(prm::Tags::AutoSize)->setValue(true);
      node->sendBlocked(msg::buildStatusMessage("3 Point System Created", 2.0));
    }
    else
      goConstantSystem();
  }
  else
  {
    goConstantSystem();
  }
}

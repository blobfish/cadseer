/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project/prjproject.h"
#include "annex/annseershape.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "feature/ftrbase.h"
#include "tools/occtools.h"
#include "tools/tlsnameindexer.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvexplode.h"
#include "command/cmdexplode.h"

using namespace cmd;

Explode::Explode()
: Base("cmd::Explode")
{
  isEdit = false;
  isFirstRun = true;
}

Explode::~Explode() = default;

std::string Explode::getStatusMessage()
{
  return QObject::tr("Select geometry for explosion of feature").toStdString();
}

void Explode::activate()
{
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void Explode::deactivate()
{
  if (viewBase)
  {
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  isActive = false;
}

//explode down next level
void Explode::goExplode(const slc::Messages &msgIn)
{
  for (const auto &m : msgIn)
  {
    auto *f = project->findFeature(m.featureId); assert(f);
    if (!f->hasAnnex(ann::Type::SeerShape)) continue;
    const auto &ss = f->getAnnex<ann::SeerShape>();
    occt::ShapeVector subShapes;
    if (slc::isPointType(m.type)) continue;
    if (slc::isObjectType(m.type))
    {
      subShapes = ss.useGetNonCompoundChildren();
      //if feature compound is wrapping only 1 object, go lower.
      if (subShapes.size() == 1)
      {
        TopAbs_ShapeEnum type = subShapes.front().ShapeType();
        if (type > TopAbs_WIRE) continue; //explode only to edges
        type = static_cast<TopAbs_ShapeEnum>(static_cast<int>(type) + 1);
        subShapes = ss.useGetChildrenOfType(subShapes.front(), type);
      }
    }
    else
    {
      const auto &subShape = ss.getOCCTShape(m.shapeId);
      if (subShape.IsNull()) continue;
      TopAbs_ShapeEnum type = subShape.ShapeType();
      if (type > TopAbs_WIRE) continue; //explode only to edges
      type = static_cast<TopAbs_ShapeEnum>(static_cast<int>(type) + 1);
      subShapes = ss.useGetChildrenOfType(subShape, type);
    }
    if (subShapes.empty()) continue;
    std::ostringstream base;
    base << f->getName().toStdString() << "_";
    tls::NameIndexer ni(subShapes.size());
    for (const auto &s : subShapes)
    {
      std::ostringstream name;
      name << base.str() << occt::getShapeTypeString(s) << ni.buildSuffix();
      project->addOCCShape(s, name.str());
    }
  }
}

//explode all shapes and label index
void Explode::goExplodeAll(const slc::Messages &msgIn)
{
  for (const auto &m : msgIn)
  {
    auto *f = project->findFeature(m.featureId); assert(f);
    if (!f->hasAnnex(ann::Type::SeerShape)) continue;
    const auto &ss = f->getAnnex<ann::SeerShape>();
    occt::ShapeVector subShapes;
    if (slc::isPointType(m.type)) continue;
    TopoDS_Shape shapeToMap;
    if (slc::isObjectType(m.type)) shapeToMap = ss.getRootOCCTShape();
    else shapeToMap = ss.getOCCTShape(m.shapeId);
    auto shapes = occt::mapShapes(shapeToMap);
    
    tls::NameIndexer ni(shapes.size());
    std::ostringstream base; base << f->getName().toStdString() << "_";
    for (const auto &s : shapes)
    {
      std::ostringstream name;
      name << base.str() << occt::getShapeTypeString(s) << ni.buildSuffix();
      project->addOCCShape(s, name.str());
    }
  }
}

void Explode::goExplode(const slc::Messages &msIn, TopAbs_ShapeEnum type)
{
  for (const auto &m : msIn)
  {
    auto *f = project->findFeature(m.featureId); assert(f);
    if (!f->hasAnnex(ann::Type::SeerShape)) continue;
    const auto &ss = f->getAnnex<ann::SeerShape>();
    auto shapes = occt::mapShapes(ss.getRootOCCTShape());
    if (shapes.empty()) continue;
    std::ostringstream base;
    base << f->getName().toStdString() << "_";
    tls::NameIndexer ni(shapes.size());
    for (const auto &s : shapes)
    {
      if (s.ShapeType() != type) continue;
      std::ostringstream name;
      name << base.str() << occt::getShapeTypeString(s) << ni.buildSuffix();
      project->addOCCShape(s, name.str());
    }
  }
}

void Explode::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  
  std::vector<slc::Message> tm; //target messages
  for (const auto &c : cs) tm.push_back(slc::EventHandler::containerToMessage(c));
  if (!tm.empty())
  {
    goExplode(tm);
    node->sendBlocked(msg::buildStatusMessage("Pre-Selection Exploded", 2.0));
    node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
    return;
  }

  viewBase = std::make_unique<cmv::Explode>(this);
}

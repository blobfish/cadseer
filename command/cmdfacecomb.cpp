/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project/prjproject.h"
#include "annex/annseershape.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmconstants.h"
#include "feature/ftrfacecomb.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvfacecomb.h"
#include "command/cmdfacecomb.h"

using namespace cmd;

FaceComb::FaceComb()
: Base("cmd::FaceComb")
, leafManager()
{
  feature = new ftr::FaceComb::Feature();
  project->addFeature(std::unique_ptr<ftr::FaceComb::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

FaceComb::FaceComb(ftr::Base *fIn)
: Base("cmd::FaceComb")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::FaceComb::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::FaceComb>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

FaceComb::~FaceComb() = default;

std::string FaceComb::getStatusMessage()
{
  return QObject::tr("Select geometry for facecomb feature").toStdString();
}

void FaceComb::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
    if (!*isEdit)
      node->sendBlocked(msg::buildSelectionFreeze(feature->getId()));
  }
  else
    sendDone();
}

void FaceComb::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
    node->sendBlocked(msg::buildSelectionThaw(feature->getId()));
  }
  isActive = false;
}

bool FaceComb::isValidSelection(const slc::Message &mIn)
{
  //only use this from go for pre-selection screening. Don't use in set* functions called from commandView
  const ftr::Base *lf = project->findFeature(mIn.featureId);
  if (!lf->hasAnnex(ann::Type::SeerShape) || lf->getAnnex<ann::SeerShape>().isNull()) return false;
  auto t = mIn.type;
  if ((t != slc::Type::Object) && (t != slc::Type::Face)) return false;
  return true;
}

void FaceComb::setSelections(const slc::Messages &targets)
{
  assert(isActive);
  setPicks(*feature, prm::Tags::Picks, targets);
}

void FaceComb::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void FaceComb::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  
  std::vector<slc::Message> tm; //target messages
  for (const auto &c : cs)
  {
    auto m = slc::EventHandler::containerToMessage(c);
    if (isValidSelection(m)) tm.push_back(m);
  }
  
  if (!tm.empty())
  {
    setSelections(tm);
    node->sendBlocked(msg::buildStatusMessage("FaceComb Added", 2.0));
    node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
    return;
  }
  
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  node->sendBlocked(msg::buildShowThreeD(feature->getId()));
  node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  viewBase = std::make_unique<cmv::FaceComb>(this);
}

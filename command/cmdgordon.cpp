/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2025 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project/prjproject.h"
#include "annex/annseershape.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrinputtype.h"
#include "feature/ftrgordon.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvgordon.h"
#include "command/cmdgordon.h"

// https://link.springer.com/article/10.1007/s11786-019-00401-y

using namespace cmd;

Gordon::Gordon()
: Base("cmd::Gordon")
, leafManager()
{
  feature = new ftr::Gordon::Feature();
  project->addFeature(std::unique_ptr<ftr::Gordon::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  node->sendBlocked(msg::buildStatusMessage("Gordon Added", 2.0));
  isEdit = false;
  isFirstRun = true;
}

Gordon::Gordon(ftr::Base *fIn)
: Base("cmd::Gordon")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::Gordon::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::Gordon>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

Gordon::~Gordon() = default;

std::string Gordon::getStatusMessage()
{
  return QObject::tr("Select geometry for gordon feature").toStdString();
}

void Gordon::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    //no pick first support. Can't determine which selections are for which.
    isFirstRun = false;
    node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildHideOverlay(feature->getId()));
    viewBase = std::make_unique<cmv::Gordon>(this);
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
    if (!*isEdit)
      node->sendBlocked(msg::buildSelectionFreeze(feature->getId()));
  }
  else
    sendDone();
}

void Gordon::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
    node->sendBlocked(msg::buildSelectionThaw(feature->getId()));
  }
  isActive = false;
}

void Gordon::setSelections(const slc::Messages &uPicks, const slc::Messages &vPicks)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::Gordon::Tags::uPicks, uPicks);
  setPicks(*feature, ftr::Gordon::Tags::vPicks, vPicks);
}

void Gordon::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

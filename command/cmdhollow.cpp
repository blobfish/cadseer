/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <osg/Geometry>

#include "application/appmainwindow.h"
#include "project/prjproject.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmconstants.h"
#include "dialogs/dlgparameter.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvhollow.h"
#include "annex/annseershape.h"
#include "tools/featuretools.h"
#include "feature/ftrhollow.h"
#include "command/cmdhollow.h"

using boost::uuids::uuid;

using namespace cmd;

Hollow::Hollow()
: Base("cmd::Hollow")
, leafManager()
{
  feature = new ftr::Hollow::Feature();
  project->addFeature(std::unique_ptr<ftr::Hollow::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

Hollow::Hollow(ftr::Base *fIn)
: Base("cmd::Hollow")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::Hollow::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::Hollow>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

Hollow::~Hollow() = default;

std::string Hollow::getStatusMessage()
{
  return QObject::tr("Select faces for Hollow feature").toStdString();
}

void Hollow::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void Hollow::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    project->hideAlterParents(feature->getId());
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  }
  isActive = false;
}

bool Hollow::isValidSelection(const slc::Message &mIn)
{
  const ftr::Base *lf = project->findFeature(mIn.featureId);
  if (!lf->hasAnnex(ann::Type::SeerShape) || lf->getAnnex<ann::SeerShape>().isNull())
    return false;
  
  if (mIn.type != slc::Type::Face)
    return false;
  return true;
}

void Hollow::setSelections(const slc::Messages &targets)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicksInsert(*feature, prm::Tags::Picks, targets);
}

void Hollow::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void Hollow::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  if (!cs.empty())
  {
    slc::Messages targets;
    for (const auto &c : cs)
    {
      auto m = slc::EventHandler::containerToMessage(c);
      if (!isValidSelection(m))
        continue;
      targets.push_back(m);
    }
    if (!targets.empty())
    {
      setSelections(targets);
      node->sendBlocked(msg::buildHideThreeD(targets.front().featureId));
      node->sendBlocked(msg::buildHideOverlay(targets.front().featureId));
      dlg::Parameter *pDialog = new dlg::Parameter(feature->getParameter(prm::Tags::Offset), feature->getId());
      pDialog->show();
      pDialog->raise();
      pDialog->activateWindow();
      return;
    }
  }
  
  node->sendBlocked(msg::buildStatusMessage("invalid pre selection", 2.0));
  viewBase = std::make_unique<cmv::Hollow>(this);
}

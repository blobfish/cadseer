/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QFileDialog>
#include <QTimer>

#include "application/appmainwindow.h"
#include "application/appapplication.h"
#include "project/prjproject.h"
#include "project/prjmessage.h"
#include "preferences/preferencesXML.h"
#include "preferences/prfmanager.h"
#include "message/msgnode.h"
#include "command/cmdmanager.h"
#include "command/cmdimportfc.h"
#include "command/cmdimport.h"

using namespace cmd;

Import::Import() : Base(){}
Import::~Import() = default;

std::string Import::getStatusMessage()
{
  return QObject::tr("Select geometry for Import feature").toStdString();
}

void Import::activate()
{
  isActive = true;
  go();
  //don't use 'sendDone', because it is queued.
  //this causes 'WARNING: cmd::Manager stack depth: 2'. that is needed here.
  msg::Message mOut(msg::Mask(msg::Request | msg::Command | msg::Done));
  node->sendBlocked(mOut);
}

void Import::deactivate()
{
  isActive = false;
}

void Import::goDialog()
{
  QString supportedFiles = QObject::tr
  (
    "Supported Formats"
    " (*.brep *.brp"
    " *.FCStd"
    " *.step *.stp"
    " *.iges *.igs"
    " *.off"
    " *.ply"
    " *.stl"
    ")"
  );
  
  files = QFileDialog::getOpenFileNames
  (
    app::instance()->getMainWindow(),
    QObject::tr("Open File"),
    QString::fromStdString(prf::manager().rootPtr->project().lastDirectory().get()),
    supportedFiles
  );
  
  if (files.empty()) return;
  
  std::filesystem::path p = files.at(0).toStdString();
  prf::manager().rootPtr->project().lastDirectory() = p.remove_filename().string();
  prf::manager().saveConfig();
}

void Import::go()
{
  shouldUpdate = false;
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  
  goDialog();
  if (files.isEmpty()) return;
  
  app::WaitCursor wc; //show busy.
  assert(project);
  
  for (const auto &fn : files)
  {
    qApp->processEvents();
    std::filesystem::path currentFilePath = fn.toStdString(); //current path.
    if (!std::filesystem::exists(currentFilePath)) continue;
    
    //we have to use Qt to get to lower case then use the extension of std::filesystem::path. What a pain in the ass.
    std::string extension = std::filesystem::path(fn.toLower().toStdString()).extension().u8string();
    if (extension == ".fcstd")
    {
      auto launch = [=]()
      {
        auto fcCommand = std::make_shared<ImportFC>(currentFilePath);
        cmd::manager().addCommand(fcCommand);
      };
      QTimer::singleShot(0, launch);
    }
    else
    {
      prj::Message mOut;
      mOut.directory = currentFilePath.u8string();
      node->sendBlocked(msg::Message(msg::Request | msg::Import, mOut));
    }
    
    shouldUpdate = true;
  }
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2022 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>

#include "subprojects/libzippp/src/libzippp.h"
#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgnode.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvimportfc.h"
#include "command/cmdimportfc.h"

using namespace cmd;

ImportFC::ImportFC(const std::filesystem::path &pIn)
: Base("cmd::ImportFC")
, path(pIn)
{
  isEdit = false;
  isFirstRun = true;
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
}

ImportFC::~ImportFC()
{
  removeZipDirectory();
}

std::string ImportFC::getStatusMessage()
{
  return QObject::tr("Import Features From FreeCAD").toStdString();
}

void ImportFC::activate()
{
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void ImportFC::deactivate()
{
  if (viewBase)
  {
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  isActive = false;
}

bool ImportFC::removeZipDirectory()
{
  if (std::filesystem::exists(zipDirectory))
  {
    if (!std::filesystem::remove_all(zipDirectory))
    {
      node->sendBlocked(msg::buildStatusMessage("Couldn't remove zip directory", 2.0));
      return false;
    }
  }
  return true;
}

bool ImportFC::createZipDirectory()
{
  if (std::filesystem::exists(zipDirectory)) return false;
  if (!std::filesystem::create_directory(zipDirectory))
  {
    node->sendBlocked(msg::buildStatusMessage("Couldn't create zip directory", 2.0));
    return false;
  }
  return true;
}

void ImportFC::localUpdate()
{
  assert(isActive);
  // feature->updateModel(project->getPayload(feature->getId()));
  // feature->updateVisual();
  // feature->setModelDirty();
  // node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void ImportFC::go()
{
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  
  //unzip
  zipDirectory = std::filesystem::path(project->getSaveDirectory().string()) / ".scratch" / path.stem();
  if (!removeZipDirectory()) return;
  if (!createZipDirectory()) return;

  libzippp::ZipArchive zip(path.string());
  zip.open(libzippp::ZipArchive::ReadOnly);
  for(const auto &e : zip.getEntries())
  {
    if (!e.isFile()) continue;
    std::filesystem::path filePath = zipDirectory / e.getName();
    std::ofstream fileOut(filePath.string().c_str(), std::ios_base::out | std::ios_base::trunc);
    e.readContent(fileOut);
  }
  
  std::filesystem::path docPath = zipDirectory / "Document.xml";
  if (!std::filesystem::exists(docPath))
  {
    node->sendBlocked(msg::buildStatusMessage("No Document.xml", 2.0));
    return;
  }
  originalName = path.filename();
  path = docPath;
  viewBase = std::make_unique<cmv::ImportFC>(this);
}

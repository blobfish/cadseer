/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2022 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CMD_IMPORTFC_H
#define CMD_IMPORTFC_H

#include <filesystem>

#include "command/cmdbase.h"

namespace ftr{namespace ImportFC{class Feature;}}

namespace cmd
{
  class ImportFC : public Base
  {
  public:
    ImportFC(const std::filesystem::path&);
    ~ImportFC() override;
    
    std::string getCommandName() override{return "ImportFC";}
    std::string getStatusMessage() override;
    void activate() override;
    void deactivate() override;
    
    void localUpdate();
    const std::filesystem::path& getPath(){return path;}
    const std::filesystem::path& getZipDirectory(){return zipDirectory;}
    const std::filesystem::path& getOriginalName(){return originalName;}
  private:
    std::filesystem::path zipDirectory; //unzipped directory
    std::filesystem::path path; //freecad file in, converted to unzipped document.xml
    std::filesystem::path originalName; //name of the .FCStd file.
    
    bool removeZipDirectory();
    bool createZipDirectory();
    void go();
  };
}
#endif // CMD_IMPORTFC_H

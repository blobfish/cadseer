/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "tools/featuretools.h"
#include "project/prjproject.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmparameter.h"
#include "feature/ftrinstance.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvinstance.h"
#include "command/cmdinstance.h"

using namespace cmd;

Instance::Instance(int typeIn)
: Base("cmd::Instance")
, leafManager()
{
  feature = new ftr::Instance::Feature();
  project->addFeature(std::unique_ptr<ftr::Instance::Feature>(feature));
  assert(typeIn >= 0 && typeIn < 5);
  auto *typePrm = feature->getParameter(ftr::Instance::Tags::InstanceType);
  typePrm->setValue(typeIn);
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

Instance::Instance(ftr::Base *fIn)
: Base("cmd::Instance")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::Instance::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::Instance>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

Instance::~Instance() = default;

std::string Instance::getStatusMessage()
{
  return QObject::tr("Select geometry for instance feature").toStdString();
}

void Instance::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
    if (!*isEdit)
      node->sendBlocked(msg::buildSelectionFreeze(feature->getId()));
  }
  else
    sendDone();
}

void Instance::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
    node->sendBlocked(msg::buildSelectionThaw(feature->getId()));
  }
  isActive = false;
}

bool Instance::isValidSelection(const slc::Message &mIn)
{
  const ftr::Base *lf = project->findFeature(mIn.featureId);
  if (!lf->hasAnnex(ann::Type::SeerShape)) return false;
  return true;
}

void Instance::setLinear(const slc::Messages &source, const slc::Messages &csys)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::Instance::Tags::Source, source);
  setPicks(*feature, ftr::Instance::Tags::LinearCSys, csys);
}

void Instance::setPolar(const slc::Messages &source, const slc::Messages &axis)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::Instance::Tags::Source, source);
  setPicks(*feature, ftr::Instance::Tags::PolarAxis, axis);
}

void Instance::setMirror(const slc::Messages &source, const slc::Messages &plane)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::Instance::Tags::Source, source);
  setPicks(*feature, ftr::Instance::Tags::MirrorPlane, plane);
}

void Instance::setInterpolate(const slc::Messages &source, const slc::Messages &csys)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::Instance::Tags::Source, source);
  setPicks(*feature, ftr::Instance::Tags::InterpolateCSys, csys);
}

void Instance::setSpine(const slc::Messages &source, const slc::Messages &spines)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicks(*feature, ftr::Instance::Tags::Source, source);
  setPicks(*feature, ftr::Instance::Tags::SpineSpine, spines);
}

void Instance::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void Instance::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  
  //we are not going to try infer shapes as system/plane/axis here.
  std::vector<slc::Message> shapes;
  std::vector<slc::Message> datumSystems;
  std::vector<slc::Message> datumPlanes;
  std::vector<slc::Message> datumAxes;
  std::vector<slc::Message> sketches;
  const ftr::Base *inputFeature = nullptr;
  for (const auto &c : cs)
  {
    auto m = slc::EventHandler::containerToMessage(c);
    const auto *tempFeature = project->findFeature(m.featureId); assert(tempFeature);
    if (tempFeature->getType() == ftr::Type::DatumSystem) {datumSystems.push_back(m); continue;}
    if (tempFeature->getType() == ftr::Type::DatumPlane) {datumPlanes.push_back(m); continue;}
    if (tempFeature->getType() == ftr::Type::DatumAxis) {datumAxes.push_back(m); continue;}
    if (tempFeature->getType() == ftr::Type::Sketch) {sketches.push_back(m); continue;}
    if (tempFeature->hasAnnex(ann::Type::SeerShape))
    {
      shapes.push_back(m);
      if (!inputFeature)
      {
        inputFeature = tempFeature;
        feature->setColor(inputFeature->getColor());
      }
      continue;
    }
  }
  
  node->sendBlocked(msg::buildShowThreeD(feature->getId()));
  node->sendBlocked(msg::buildStatusMessage("Instance Added", 2.0));
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  
  auto instanceType = feature->getInstanceType(); //note: was set in constructor
  switch (instanceType)
  {
    case ftr::Instance::IType::Linear:
    {
      if (!datumSystems.empty()) setLinear(shapes, datumSystems);
      else if (shapes.size() > 1) setLinear({shapes.front()}, {shapes.back()});
      else setLinear(shapes, {});
      break;
    }
    case ftr::Instance::IType::Polar: {setPolar(shapes, datumAxes); break;} //needs datum axis
    case ftr::Instance::IType::Mirror:
    {
      if (!datumPlanes.empty()) setMirror(shapes, datumPlanes);
      else if (shapes.size() > 1) setMirror({shapes.front()}, {shapes.back()});
      else setMirror(shapes, {});
      break;
    }
    case ftr::Instance::IType::Interpolate:
    {
      if (!datumSystems.empty()) setInterpolate(shapes, datumSystems);
      else if (shapes.size() > 1) setInterpolate({shapes.front()}, {shapes.back()});
      else setInterpolate(shapes, {});
      break;
    }
    case ftr::Instance::IType::Spine:
    {
      if (!sketches.empty()) setSpine(shapes, sketches);
      else if (shapes.size() > 1) setSpine({shapes.front()}, {shapes.back()});
      else setSpine(shapes, {});
      break;
    }
  }
  
  if (shapes.empty())
  {
    node->sendBlocked(msg::buildHideOverlay(feature->getId()));
    viewBase = std::make_unique<cmv::Instance>(this);
  }
  else node->sendBlocked(msg::buildShowOverlay(feature->getId()));
}

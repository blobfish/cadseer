/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CMD_INSTANCE_H
#define CMD_INSTANCE_H

#include "command/cmdleafmanager.h"
#include "command/cmdbase.h"

namespace ftr{namespace Instance{class Feature;}}

namespace cmd
{
  class Instance : public Base
  {
  public:
    ftr::Instance::Feature *feature = nullptr;
    
    Instance(int);
    Instance(ftr::Base*);
    ~Instance() override;
    
    std::string getCommandName() override{return "Instance";}
    std::string getStatusMessage() override;
    void activate() override;
    void deactivate() override;
    
    void setLinear(const slc::Messages&, const slc::Messages&);
    void setPolar(const slc::Messages&, const slc::Messages&);
    void setMirror(const slc::Messages&, const slc::Messages&);
    void setInterpolate(const slc::Messages&, const slc::Messages&);
    void setSpine(const slc::Messages&, const slc::Messages&);
    void localUpdate();
  private:
    cmd::LeafManager leafManager;
    void go();
    bool isValidSelection(const slc::Message&);
  };
}
#endif // CMD_INSTANCE_H

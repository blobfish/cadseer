/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <osg/Geometry>

#include "tools/featuretools.h"
#include "message/msgnode.h"
#include "project/prjproject.h"
#include "selection/slceventhandler.h"
#include "feature/ftrline.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvline.h"
#include "command/cmdline.h"

using namespace cmd;

Line::Line() : Base("cmd::Line")
{
  feature = new ftr::Line::Feature();
  project->addFeature(std::unique_ptr<ftr::Line::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

Line::Line(ftr::Base *fIn)
: Base("cmd::Line")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::Line::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::Line>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

Line::~Line() = default;

std::string Line::getStatusMessage()
{
  return QObject::tr("Select geometry for line feature").toStdString();
}

void Line::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
    if (!*isEdit) node->sendBlocked(msg::buildSelectionFreeze(feature->getId()));
  }
  else sendDone();
}

void Line::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
    node->sendBlocked(msg::buildSelectionThaw(feature->getId()));
  }
  isActive = false;
}

void Line::setToExtrema(const slc::Messages &msIn)
{
  project->clearAllInputs(feature->getId());
  clearPick(*feature, ftr::Line::Tags::originPick);
  clearPick(*feature, ftr::Line::Tags::finalePick);
  if (msIn.size() > 0) setPicks(*feature, ftr::Line::Tags::originPick, {msIn.front()});
  if (msIn.size() > 1) setPicks(*feature, ftr::Line::Tags::finalePick, {msIn.at(1)});
}

void Line::setOriginDirectionLength(const slc::Messages &msIn)
{
  project->clearAllInputs(feature->getId());
  clearPick(*feature, ftr::Line::Tags::originPick);
  clearPick(*feature, ftr::Line::Tags::directionPick);
  if (msIn.size() > 0) setPicks(*feature, ftr::Line::Tags::originPick, {msIn.front()});
  if (msIn.size() > 1) setPicks(*feature, ftr::Line::Tags::directionPick, {msIn.at(1)});
}

void Line::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void Line::go()
{
  slc::Messages targets;
  for (const auto &c : eventHandler->getSelections()) targets.push_back(slc::EventHandler::containerToMessage(c));
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  if (targets.size() == 2)
  {
    setToExtrema(targets);
    node->sendBlocked(msg::buildStatusMessage("Extrema Line Added", 2.0));
    return;
  }
  node->sendBlocked(msg::buildStatusMessage("Invalid Line Selection", 2.0));
  node->sendBlocked(msg::buildShowThreeD(feature->getId()));
  node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  viewBase = std::make_unique<cmv::Line>(this);
}

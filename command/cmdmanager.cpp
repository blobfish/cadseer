/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <functional>

#include <osg/Geometry> //need this for containers.

#include "application/appapplication.h"
#include "application/appmainwindow.h"
#include "project/prjproject.h"
#include "feature/ftrbase.h"
#include "feature/ftrbox.h"
#include "feature/ftrcylinder.h"
#include "feature/ftroblong.h"
#include "feature/ftrtorus.h"
#include "feature/ftrsphere.h"
#include "feature/ftrcone.h"
#include "feature/ftrthread.h"
#include "feature/ftrprism.h"
#include "feature/ftrarc.h"
#include "feature/ftrhelix.h"
#include "command/cmdresetdragger.h"
#include "command/cmdcheckgeometry.h"
#include "command/cmdeditcolor.h"
#include "command/cmdfeaturerename.h"
#include "command/cmdblend.h"
#include "command/cmdextract.h"
#include "command/cmdreposition.h"
#include "command/cmddissolve.h"
#include "command/cmdsystemtoselection.h"
#include "command/cmdsquash.h"
#include "command/cmdstrip.h"
#include "command/cmdnest.h"
#include "command/cmddieset.h"
#include "command/cmdquote.h"
#include "command/cmdisolate.h"
#include "command/cmdmeasurelinear.h"
#include "command/cmdmeasureangular.h"
#include "command/cmdrefine.h"
#include "command/cmdinstance.h"
#include "command/cmdboolean.h"
#include "command/cmdoffset.h"
#include "command/cmdthicken.h"
#include "command/cmdsew.h"
#include "command/cmdtrim.h"
#include "command/cmdrevision.h"
#include "command/cmdremovefaces.h"
#include "command/cmddatumaxis.h"
#include "command/cmddatumplane.h"
#include "command/cmdsketch.h"
#include "command/cmdextrude.h"
#include "command/cmdrevolve.h"
#include "command/cmdsurfacemesh.h"
#include "command/cmdline.h"
#include "command/cmdtransitioncurve.h"
#include "command/cmdruled.h"
#include "command/cmdimageplane.h"
#include "command/cmdsweep.h"
#include "command/cmddraft.h"
#include "command/cmdchamfer.h"
#include "command/cmdhollow.h"
#include "command/cmdimport.h"
#include "command/cmdexport.h"
#include "command/cmdpreferences.h"
#include "command/cmdremove.h"
#include "command/cmdinfo.h"
#include "command/cmddirty.h"
#include "command/cmdfeaturedump.h"
#include "command/cmdshapetrackdump.h"
#include "command/cmdshapegraphdump.h"
#include "command/cmdtest.h"
#include "command/cmddatumsystem.h"
#include "command/cmdsurfaceremesh.h"
#include "command/cmdsurfacemeshfill.h"
#include "command/cmdprimitive.h"
#include "command/cmdmappcurve.h"
#include "command/cmduntrim.h"
#include "command/cmdface.h"
#include "command/cmdfill.h"
#include "command/cmdundercut.h"
#include "command/cmdmutate.h"
#include "command/cmdlawspine.h"
#include "command/cmdcurvecomb.h"
#include "command/cmddatumpoint.h"
#include "command/cmdskin.h"
#include "command/cmdexplode.h"
#include "command/cmdcompound.h"
#include "command/cmdautoimport.h"
#include "command/cmdshapemesh.h"
#include "command/cmdboundingbox.h"
#include "command/cmdmatch.h"
#include "command/cmdanchor.h"
#include "command/cmdfacecomb.h"
#include "command/cmdparametriccurve.h"
#include "command/cmdgordon.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "selection/slcmessage.h"
#include "selection/slccontainer.h"
#include "selection/slcdefinitions.h"
#include "selection/slceventhandler.h"
#include "preferences/preferencesXML.h"
#include "preferences/prfmanager.h"
#include "viewer/vwrmessage.h"
#include "viewer/vwrwidget.h"
#include "command/cmdmanager.h"

using namespace cmd;

Manager& cmd::manager()
{
  static Manager localManager;
  return localManager;
}

Manager::Manager()
{
  node = std::make_unique<msg::Node>();
  node->connect(msg::hub());
  sift = std::make_unique<msg::Sift>();
  sift->name = "cmd::Manager";
  node->setHandler(std::bind(&msg::Sift::receive, sift.get(), std::placeholders::_1));
  
  setupDispatcher();
  
  setupEditFunctionMap();
  
  selectionMask = slc::AllEnabled;
  app::instance()->queuedMessage(msg::Mask(msg::Response | msg::Command | msg::Inactive));
}

void Manager::setupDispatcher()
{
  using std::placeholders::_1;
  using std::make_shared;
  auto con = [&](const msg::Mask &mIn, std::function<void(const msg::Message&)> fIn) { sift->insert(mIn, fIn); };
  
  //beyond trivial, handler has dedicated functions.
  con(msg::Request | msg::Command | msg::Clear, std::bind(&Manager::clearCommandDispatched, this, _1));
  con(msg::Request | msg::Command | msg::Done, std::bind(&Manager::doneCommandDispatched, this, _1));
  con(msg::Response | msg::Selection | msg::SetMask, std::bind(&Manager::selectionMaskDispatched, this, _1));
  con(msg::Request | msg::Edit | msg::Feature | msg::Name, std::bind(&Manager::featureRenameDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Box, std::bind(&Manager::constructBoxDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Oblong, std::bind(&Manager::constructOblongDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Torus, std::bind(&Manager::constructTorusDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Cylinder, std::bind(&Manager::constructCylinderDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Sphere, std::bind(&Manager::constructSphereDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Cone, std::bind(&Manager::constructConeDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Prism, std::bind(&Manager::constructPrismDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Arc, std::bind(&Manager::constructArcDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Helix, std::bind(&Manager::constructHelixDispatched, this, _1));
  con(msg::Request | msg::View | msg::ThreeD | msg::Isolate, std::bind(&Manager::viewIsolateDispatched, this, _1));
  con(msg::Request | msg::View | msg::Overlay | msg::Isolate, std::bind(&Manager::viewIsolateDispatched, this, _1));
  con(msg::Request | msg::View | msg::ThreeD | msg::Overlay | msg::Isolate, std::bind(&Manager::viewIsolateDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Intersect, std::bind(&Manager::constructBooleanDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Subtract, std::bind(&Manager::constructBooleanDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Union, std::bind(&Manager::constructBooleanDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Section, std::bind(&Manager::constructBooleanDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Split, std::bind(&Manager::constructBooleanDispatched, this, _1));
  con(msg::Request | msg::Construct | msg::Thread, std::bind(&Manager::constructThreadDispatched, this, _1));
  con(msg::Request | msg::Edit | msg::Feature, std::bind(&Manager::editFeatureDispatched, this, _1));
  
  //trivial handlers just get an in place lambda.
  con(msg::Request | msg::Reset | msg::Dragger, [this](const msg::Message&){addCommand(make_shared<ResetDragger>());});
  con(msg::Request | msg::Feature | msg::Reposition, [this](const msg::Message&){addCommand(make_shared<Reposition>());});
  con(msg::Request | msg::Feature | msg::Dissolve, [this](const msg::Message&){addCommand(make_shared<Dissolve>());});
  con(msg::Request | msg::SystemToSelection, [this](const msg::Message&){addCommand(make_shared<SystemToSelection>());});
  con(msg::Request | msg::CheckGeometry, [this](const msg::Message&){addCommand(make_shared<CheckGeometry>());});
  con(msg::Request | msg::Edit | msg::Feature | msg::Color, [this](const msg::Message&){addCommand(make_shared<EditColor>());});
  con(msg::Request | msg::Construct | msg::Blend, [this](const msg::Message&){addCommand(make_shared<Blend>());});
  con(msg::Request | msg::Construct | msg::Extract, [this](const msg::Message&){addCommand(make_shared<Extract>());});
  con(msg::Request | msg::Construct | msg::Squash, [this](const msg::Message&){addCommand(make_shared<Squash>());});
  con(msg::Request | msg::Construct | msg::Strip, [this](const msg::Message&){addCommand(make_shared<Strip>());});
  con(msg::Request | msg::Construct | msg::Nest, [this](const msg::Message&){addCommand(make_shared<Nest>());});
  con(msg::Request | msg::Construct | msg::DieSet, [this](const msg::Message&){addCommand(make_shared<DieSet>());});
  con(msg::Request | msg::Construct | msg::Quote, [this](const msg::Message&){addCommand(make_shared<Quote>());});
  con(msg::Request | msg::Linear | msg::Measure, [this](const msg::Message&){addCommand(make_shared<MeasureLinear>());});
  con(msg::Request | msg::Measure | msg::Angular, [this](const msg::Message&){addCommand(make_shared<MeasureAngular>());});
  con(msg::Request | msg::Construct | msg::Refine, [this](const msg::Message&){addCommand(make_shared<Refine>());});
  con(msg::Request | msg::Construct | msg::Offset, [this](const msg::Message&){addCommand(make_shared<Offset>());});
  con(msg::Request | msg::Construct | msg::Thicken, [this](const msg::Message&){addCommand(make_shared<Thicken>());});
  con(msg::Request | msg::Construct | msg::Sew, [this](const msg::Message&){addCommand(make_shared<Sew>());});
  con(msg::Request | msg::Construct | msg::Trim, [this](const msg::Message&){addCommand(make_shared<Trim>());});
  con(msg::Request | msg::Construct | msg::RemoveFaces, [this](const msg::Message&){addCommand(make_shared<RemoveFaces>());});
  con(msg::Request | msg::Construct | msg::Datum | msg::Axis, [this](const msg::Message&){addCommand(make_shared<DatumAxis>());});
  con(msg::Request | msg::Construct | msg::Datum | msg::Plane, [this](const msg::Message&){addCommand(make_shared<DatumPlane>());});
  con(msg::Request | msg::Construct | msg::Sketch, [this](const msg::Message&){addCommand(make_shared<Sketch>());});
  con(msg::Request | msg::Construct | msg::Extrude, [this](const msg::Message&){addCommand(make_shared<Extrude>());});
  con(msg::Request | msg::Construct | msg::Revolve, [this](const msg::Message&){addCommand(make_shared<Revolve>());});
  con(msg::Request | msg::Construct | msg::SurfaceMesh, [this](const msg::Message&){addCommand(make_shared<SurfaceMesh>());});
  con(msg::Request | msg::Construct | msg::Line, [this](const msg::Message&){addCommand(make_shared<Line>());});
  con(msg::Request | msg::Construct | msg::TransitionCurve, [this](const msg::Message&){addCommand(make_shared<TransitionCurve>());});
  con(msg::Request | msg::Construct | msg::Ruled, [this](const msg::Message&){addCommand(make_shared<Ruled>());});
  con(msg::Request | msg::Construct | msg::ImagePlane, [this](const msg::Message&){addCommand(make_shared<ImagePlane>());});
  con(msg::Request | msg::Construct | msg::Sweep, [this](const msg::Message&){addCommand(make_shared<Sweep>());});
  con(msg::Request | msg::Construct | msg::Draft, [this](const msg::Message&){addCommand(make_shared<Draft>());});
  con(msg::Request | msg::Construct | msg::Chamfer, [this](const msg::Message&){addCommand(make_shared<Chamfer>());});
  con(msg::Request | msg::Construct | msg::Hollow, [this](const msg::Message&){addCommand(make_shared<Hollow>());});
  con(msg::Request | msg::Construct | msg::DatumSystem, [this](const msg::Message&){addCommand(make_shared<DatumSystem>());});
  con(msg::Request | msg::Construct | msg::SurfaceReMesh, [this](const msg::Message&){addCommand(make_shared<SurfaceReMesh>());});
  con(msg::Request | msg::Construct | msg::SurfaceMeshFill, [this](const msg::Message&){addCommand(make_shared<SurfaceMeshFill>());});
  con(msg::Request | msg::Construct | msg::MapPCurve, [this](const msg::Message&){addCommand(make_shared<MapPCurve>());});
  con(msg::Request | msg::Construct | msg::Untrim, [this](const msg::Message&){addCommand(make_shared<Untrim>());});
  con(msg::Request | msg::Construct | msg::Face, [this](const msg::Message&){addCommand(make_shared<Face>());});
  con(msg::Request | msg::Construct | msg::Fill, [this](const msg::Message&){addCommand(make_shared<Fill>());});
  con(msg::Request | msg::Construct | msg::UnderCut, [this](const msg::Message&){addCommand(make_shared<UnderCut>());});
  con(msg::Request | msg::Construct | msg::Mutate, [this](const msg::Message&){addCommand(make_shared<Mutate>());});
  con(msg::Request | msg::Construct | msg::LawSpine, [this](const msg::Message&){addCommand(make_shared<LawSpine>());});
  con(msg::Request | msg::Construct | msg::Curve | msg::Comb, [this](const msg::Message&){addCommand(make_shared<CurveComb>());});
  con(msg::Request | msg::Construct | msg::Face | msg::Comb, [this](const msg::Message&){addCommand(make_shared<FaceComb>());});
  con(msg::Request | msg::Construct | msg::Datum | msg::Point, [this](const msg::Message&){addCommand(make_shared<DatumPoint>());});
  con(msg::Request | msg::Construct | msg::Skin, [this](const msg::Message&){addCommand(make_shared<Skin>());});
  con(msg::Request | msg::Construct | msg::Shape | msg::SurfaceMesh, [this](const msg::Message&){addCommand(make_shared<ShapeMesh>());});
  con(msg::Request | msg::Construct | msg::BoundingBox, [this](const msg::Message&){addCommand(make_shared<BoundingBox>());});
  con(msg::Request | msg::Edit | msg::Feature | msg::Explode, [this](const msg::Message&){addCommand(make_shared<Explode>());});
  con(msg::Request | msg::Edit | msg::Feature | msg::Compound, [this](const msg::Message&){addCommand(make_shared<Compound>());});
  con(msg::Request | msg::Project | msg::Revision | msg::Dialog, [this](const msg::Message&){addCommand(make_shared<Revision>());});
  con(msg::Request | msg::Command | msg::Import, [this](const msg::Message&){addCommand(make_shared<Import>());});
  con(msg::Request | msg::Export, [this](const msg::Message&){addCommand(make_shared<Export>());});
  con(msg::Request | msg::Preferences, [this](const msg::Message&){addCommand(make_shared<Preferences>());});
  con(msg::Request | msg::Remove, [this](const msg::Message&){addCommand(make_shared<Remove>());});
  con(msg::Request | msg::Info, [this](const msg::Message&){addCommand(make_shared<Info>());});
  con(msg::Request | msg::Feature | msg::Model | msg::Dirty, [this](const msg::Message&){addCommand(make_shared<Dirty>());});
  con(msg::Request | msg::Feature | msg::Dump, [this](const msg::Message&){addCommand(make_shared<FeatureDump>());});
  con(msg::Request | msg::Shape | msg::Track | msg::Dump, [this](const msg::Message&){addCommand(make_shared<ShapeTrackDump>());});
  con(msg::Request | msg::Shape | msg::Graph | msg::Dump, [this](const msg::Message&){addCommand(make_shared<ShapeGraphDump>());});
  con(msg::Request | msg::Test, [this](const msg::Message&){addCommand(make_shared<Test>());});
  con(msg::Request | msg::Command | msg::Auto | msg::Import, [this](const msg::Message&){addCommand(make_shared<AutoImport>());});
  con(msg::Request | msg::Edit | msg::Feature | msg::Match, [this](const msg::Message&){addCommand(make_shared<Match>());});
  con(msg::Request | msg::Edit | msg::Anchor, [this](const msg::Message&){addCommand(make_shared<Anchor>());});
  con(msg::Request | msg::Construct | msg::InstanceLinear, [this](const msg::Message&){addCommand(make_shared<Instance>(0));});
  con(msg::Request | msg::Construct | msg::InstanceMirror, [this](const msg::Message&){addCommand(make_shared<Instance>(2));});
  con(msg::Request | msg::Construct | msg::InstancePolar, [this](const msg::Message&){addCommand(make_shared<Instance>(1));});
  con(msg::Request | msg::Construct | msg::Instance | msg::Interpolate, [this](const msg::Message&){addCommand(make_shared<Instance>(3));});
  con(msg::Request | msg::Construct | msg::Instance | msg::Spine, [this](const msg::Message&){addCommand(make_shared<Instance>(4));});
  con(msg::Request | msg::Construct | msg::Parameter | msg::Curve, [this](const msg::Message&){addCommand(make_shared<ParametricCurve>());});
  con(msg::Request | msg::Construct | msg::Gordon, [this](const msg::Message&){addCommand(make_shared<Gordon>());});
}

void Manager::doneCommandDispatched(const msg::Message&)
{
  doneSlot();
}

void Manager::clearCommandDispatched(const msg::Message &)
{
  while(!stack.empty())
    doneSlot();
}

void Manager::addCommand(BasePtr pointerIn)
{
  //preselection will only work if the command stack is empty.
  if (!stack.empty())
  {
    stack.top()->deactivate();
    clearSelection();
  }
  stack.push(pointerIn);
  activateTop();
}

void Manager::doneSlot()
{
  bool shouldCommandUpdate = false;
  //only active command should trigger it is done.
  if (!stack.empty())
  {
    shouldCommandUpdate = stack.top()->getShouldUpdate();
    stack.top()->deactivate();
    stack.pop();
  }
  clearSelection();
  node->send(msg::buildStatusMessage(""));
  
  if (prf::manager().rootPtr->dragger().triggerUpdateOnFinish() && shouldCommandUpdate)
  {
    node->sendBlocked(msg::Mask(msg::Request | msg::Project | msg::Update));
  }
  
  if (!stack.empty())
    activateTop();
  else
  {
    sendCommandMessage("Active command count: 0");
    node->send(msg::buildSelectionMask(selectionMask));
    node->sendBlocked(msg::Mask(msg::Response | msg::Command | msg::Inactive));
  }
}

void Manager::activateTop()
{
  node->sendBlocked(msg::buildStatusMessage(stack.top()->getStatusMessage()));
  
  std::ostringstream stream;
  stream << 
    "Active command count: " << stack.size() << std::endl <<
    "Command: " << stack.top()->getCommandName();
  sendCommandMessage(stream.str());
  
  node->sendBlocked(msg::Mask(msg::Response | msg::Command | msg::Active));
  stack.top()->activate();
}

void Manager::sendCommandMessage(const std::string& messageIn)
{
  vwr::Message statusVMessage;
  statusVMessage.text = messageIn;
  msg::Message statusMessage(msg::Request | msg::Command | msg::Text, statusVMessage);
  node->sendBlocked(statusMessage);
}

void Manager::clearSelection()
{
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
}

void Manager::selectionMaskDispatched(const msg::Message &messageIn)
{
  if (!stack.empty()) //only when no commands 
    return;
  
  slc::Message sMsg = messageIn.getSLC();
  selectionMask = sMsg.selectionMask;
}

void Manager::featureRenameDispatched(const msg::Message &mIn)
{
  auto featureRename = std::make_shared<FeatureRename>();
  featureRename->setFromMessage(mIn);
  addCommand(featureRename);
}

void Manager::viewIsolateDispatched(const msg::Message &mIn)
{
  auto i = std::make_shared<Isolate>();
  i->setFromMessage(mIn);
  addCommand(i);
}

void Manager::constructBooleanDispatched(const msg::Message &mIn)
{
  if ((mIn.mask & msg::Intersect) != 0)
    addCommand(std::make_shared<Boolean>(0));
  else if ((mIn.mask & msg::Subtract) != 0)
    addCommand(std::make_shared<Boolean>(1));
  else if ((mIn.mask & msg::Union) != 0)
    addCommand(std::make_shared<Boolean>(2));
  else if ((mIn.mask & msg::Section) != 0)
    addCommand(std::make_shared<Boolean>(3));
  else if ((mIn.mask & msg::Split) != 0)
    addCommand(std::make_shared<Boolean>(4));
}

void Manager::constructThreadDispatched(const msg::Message&)
{
  auto *nf = new ftr::Thread::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Thread::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructBoxDispatched(const msg::Message&)
{
  auto *nf = new ftr::Box::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Box::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructOblongDispatched(const msg::Message&)
{
  auto *nf = new ftr::Oblong::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Oblong::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructTorusDispatched(const msg::Message&)
{
  auto *nf = new ftr::Torus::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Torus::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructCylinderDispatched(const msg::Message&)
{
  auto *nf = new ftr::Cylinder::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Cylinder::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructSphereDispatched(const msg::Message&)
{
  auto *nf = new ftr::Sphere::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Sphere::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructConeDispatched(const msg::Message&)
{
  auto *nf = new ftr::Cone::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Cone::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructPrismDispatched(const msg::Message&)
{
  auto *nf = new ftr::Prism::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Prism::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructArcDispatched(const msg::Message&)
{
  auto *nf = new ftr::Arc::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Arc::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

void Manager::constructHelixDispatched(const msg::Message&)
{
  auto *nf = new ftr::Helix::Feature();
  app::instance()->getProject()->addFeature(std::unique_ptr<ftr::Helix::Feature>(nf));
  auto c = std::make_shared<Primitive>(nf, false);
  addCommand(c);
}

//editing commands and dispatching.
void Manager::editFeatureDispatched(const msg::Message&)
{
  app::Application *application = app::instance();
  const slc::Containers &selections = application->
    getMainWindow()->getViewer()->getSelectionEventHandler()->getSelections();
    
  //edit feature only works with 1 object pre-selection.
  if (selections.size() != 1)
  {
    node->sendBlocked(msg::buildStatusMessage("Select 1 object prior to edit feature command", 2.0));
    return;
  }
  
  if (selections.front().selectionType != slc::Type::Object)
  {
    node->sendBlocked(msg::buildStatusMessage("Wrong selection type for edit feature command", 2.0));
    return;
  }
  
  ftr::Base *feature = application->getProject()->findFeature(selections.front().featureId);
  
  auto it = editFunctionMap.find(feature->getType());
  if (it == editFunctionMap.end())
  {
    node->sendBlocked(msg::buildStatusMessage("Editing of feature type not implemented", 2.0));
    return;
  }
  
  addCommand(it->second(feature));
}

void Manager::setupEditFunctionMap()
{
  using std::make_shared;
  using std::make_pair;
  
  editFunctionMap.insert(make_pair(ftr::Type::Blend, [](ftr::Base *f){return make_shared<Blend>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Strip, [](ftr::Base *f){return make_shared<Strip>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Quote, [](ftr::Base *f){return make_shared<Quote>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Boolean, [](ftr::Base *f){return make_shared<Boolean>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Sketch, [](ftr::Base *f){return make_shared<Sketch>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Sweep, [](ftr::Base *f){return make_shared<Sweep>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Draft, [](ftr::Base *f){return make_shared<Draft>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Extract, [](ftr::Base *f){return make_shared<Extract>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Chamfer, [](ftr::Base *f){return make_shared<Chamfer>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Box, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Oblong, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Torus, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Cylinder, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Sphere, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Cone, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Thread, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Prism, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Arc, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Helix, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Inert, [](ftr::Base *f){return make_shared<Primitive>(f, true);}));
  editFunctionMap.insert(make_pair(ftr::Type::Hollow, [](ftr::Base *f){return make_shared<Hollow>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::DatumSystem, [](ftr::Base *f){return make_shared<DatumSystem>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::SurfaceMesh, [](ftr::Base *f){return make_shared<SurfaceMesh>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::DatumAxis, [](ftr::Base *f){return make_shared<DatumAxis>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::DatumPlane, [](ftr::Base *f){return make_shared<DatumPlane>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Extrude, [](ftr::Base *f){return make_shared<Extrude>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Revolve, [](ftr::Base *f){return make_shared<Revolve>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Trim, [](ftr::Base *f){return make_shared<Trim>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Offset, [](ftr::Base *f){return make_shared<Offset>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::RemoveFaces, [](ftr::Base *f){return make_shared<RemoveFaces>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Ruled, [](ftr::Base *f){return make_shared<Ruled>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Sew, [](ftr::Base *f){return make_shared<Sew>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Thicken, [](ftr::Base *f){return make_shared<Thicken>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::TransitionCurve, [](ftr::Base *f){return make_shared<TransitionCurve>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::MapPCurve, [](ftr::Base *f){return make_shared<MapPCurve>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Untrim, [](ftr::Base *f){return make_shared<Untrim>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Face, [](ftr::Base *f){return make_shared<Face>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Fill, [](ftr::Base *f){return make_shared<Fill>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::UnderCut, [](ftr::Base *f){return make_shared<UnderCut>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Mutate, [](ftr::Base *f){return make_shared<Mutate>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::LawSpine, [](ftr::Base *f){return make_shared<LawSpine>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::DatumPoint, [](ftr::Base *f){return make_shared<DatumPoint>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Skin, [](ftr::Base *f){return make_shared<Skin>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Instance, [](ftr::Base *f){return make_shared<Instance>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::ShapeMesh, [](ftr::Base *f){return make_shared<ShapeMesh>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::BoundingBox, [](ftr::Base *f){return make_shared<BoundingBox>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Line, [](ftr::Base *f){return make_shared<Line>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::FaceComb, [](ftr::Base *f){return make_shared<FaceComb>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::ParametricCurve, [](ftr::Base *f){return make_shared<ParametricCurve>(f);}));
  editFunctionMap.insert(make_pair(ftr::Type::Gordon, [](ftr::Base *f){return make_shared<Gordon>(f);}));
}

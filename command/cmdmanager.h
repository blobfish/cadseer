/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CMD_MANAGER_H
#define CMD_MANAGER_H

#include <stack>
#include <memory>
#include <functional>
#include <map>

#include "selection/slcdefinitions.h"
#include "feature/ftrtypes.h"
#include "command/cmdbase.h"

namespace msg{struct Message; struct Node; struct Sift;}
namespace ftr{class Base;}

namespace cmd
{
  class Manager
  {
  public:
    Manager();
    
    void addCommand(BasePtr);
  private:
    std::unique_ptr<msg::Node> node;
    std::unique_ptr<msg::Sift> sift;
    slc::Mask selectionMask;
    void cancelCommandDispatched(const msg::Message &);
    void doneCommandDispatched(const msg::Message &);
    void clearCommandDispatched(const msg::Message &);
    void setupDispatcher();
    void doneSlot();
    void activateTop();
    void sendCommandMessage(const std::string &messageIn);
    void selectionMaskDispatched(const msg::Message&);
    void clearSelection();
    
    std::stack<BasePtr> stack;
    
    void featureRenameDispatched(const msg::Message&);
    void editFeatureDispatched(const msg::Message&);
    void viewIsolateDispatched(const msg::Message&);
    void constructBooleanDispatched(const msg::Message&);
    void constructThreadDispatched(const msg::Message&);
    void constructBoxDispatched(const msg::Message&);
    void constructOblongDispatched(const msg::Message&);
    void constructTorusDispatched(const msg::Message&);
    void constructCylinderDispatched(const msg::Message&);
    void constructSphereDispatched(const msg::Message&);
    void constructConeDispatched(const msg::Message&);
    void constructPrismDispatched(const msg::Message&);
    void constructArcDispatched(const msg::Message&);
    void constructHelixDispatched(const msg::Message&);
    
    //editing functions
    typedef std::function<BasePtr (ftr::Base *)> EditFunction;
    typedef std::map<ftr::Type, EditFunction> EditFunctionMap;
    EditFunctionMap editFunctionMap;
    void setupEditFunctionMap();
  };
  
  Manager& manager();
}

#endif // CMD_MANAGER_H

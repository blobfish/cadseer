/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "globalutilities.h"
#include "project/prjproject.h"
#include "annex/anncsysdragger.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrbase.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvmatch.h"
#include "command/cmdmatch.h"

using namespace cmd;

Match::Match()
: Base("cmd::Match")
{
  isEdit = false;
  isFirstRun = true;
}

Match::~Match() = default;

std::string Match::getStatusMessage()
{
  return QObject::tr("Select features for matching").toStdString();
}

void Match::activate()
{
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void Match::deactivate()
{
  if (viewBase)
  {
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  isActive = false;
}

bool Match::isValidSelection(const slc::Message &mIn)
{
  if (slc::isObjectType(mIn.type)) return true;
  return false;
}

void Match::localUpdate()
{
  assert(isActive);
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void Match::setSource(const boost::uuids::uuid &idIn)
{
  source = project->findFeature(idIn); assert(source);
}

void Match::clearSource()
{
  source = nullptr;
}

void Match::addTarget(const boost::uuids::uuid &idIn)
{
  ftr::Base *aTarget = project->findFeature(idIn); assert(aTarget);
  targets.emplace_back(aTarget);
}

void Match::addTarget(const std::vector<boost::uuids::uuid> &idsIn)
{
  for (const auto &id : idsIn) addTarget(id);
}

void Match::clearTargets()
{
  targets.clear();
}

std::vector<std::string_view> Match::reconcileParameterTags()
{
  if (!source || targets.empty()) return std::vector<std::string_view>();
  
  std::vector<std::string_view> out;
  for (const auto *p : source->getParameters()) out.push_back(p->getTag());
  gu::uniquefy(out);
  
  for (auto *f : targets)
  {
    const auto &params = f->getParameters();
    std::vector<std::string_view> ct;
    for (const auto *p : params)
    {
      if (p->getValueType() == typeid(ftr::Picks)) continue;
      if (!p->isConstant()) continue; //skip expression linked.
      //what about skipping non-active?
      auto t = p->getTag();
      if (t == prm::Tags::CSysType) continue; //CSysLinked already skipped because picks.
      ct.push_back(p->getTag());
    }
    gu::uniquefy(ct);

    std::vector<std::string_view> i;
    std::set_intersection(out.begin(), out.end(), ct.begin(), ct.end(), std::back_inserter(i));
    out = i;
  }
  
  return out;
}

//first feature in vector is source and rest are targets.
void Match::goMatch(const std::vector<std::string_view> &tags)
{
  if (!source || targets.empty() || tags.empty()) return;
  for (auto *target : targets) for (const auto &tag : tags)
  {
    *target->getParameter(tag) = *source->getParameter(tag);
    if (tag == prm::Tags::CSys && target->hasAnnex(ann::Type::CSysDragger))
    {
      ann::CSysDragger &da = target->getAnnex<ann::CSysDragger>(ann::Type::CSysDragger);
      da.resetDragger();
    }
  }
}

void Match::goMatch(const std::vector<std::string> &tags)
{
  std::vector<std::string_view> forward;
  for (const auto &t : tags) forward.emplace_back(t);
  goMatch(forward);
}

void Match::go()
{
  const slc::Containers &cs = eventHandler->getSelections();
  
  std::vector<slc::Message> tm; //target messages
  for (const auto &c : cs)
  {
    auto m = slc::EventHandler::containerToMessage(c);
    if (isValidSelection(m)) tm.emplace_back(m);
  }
  
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  for (const auto &m : tm)
  {
    if (!source) setSource(m.featureId);
    else addTarget(m.featureId);
  }
  auto tags = reconcileParameterTags();
  
  if (source && !targets.empty() && !tags.empty())
  {
    goMatch(tags);
    node->sendBlocked(msg::buildStatusMessage("Match Executed", 2.0));
    return;
  }
  viewBase = std::make_unique<cmv::Match>(this);
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CMD_MATCH_H
#define CMD_MATCH_H

#include "command/cmdbase.h"

namespace cmd
{
  class Match : public Base
  {
  public:
    Match();
    ~Match() override;
    
    std::string getCommandName() override{return "Match";}
    std::string getStatusMessage() override;
    void activate() override;
    void deactivate() override;
    
    void localUpdate();
    
    void setSource(const boost::uuids::uuid&);
    void clearSource();
    void addTarget(const boost::uuids::uuid&);
    void addTarget(const std::vector<boost::uuids::uuid>&);
    void clearTargets();
    
    std::vector<std::string_view> reconcileParameterTags();
    void goMatch(const std::vector<std::string_view>&);
    void goMatch(const std::vector<std::string>&);
  private:
    void go();
    bool isValidSelection(const slc::Message&);
    ftr::Base *source = nullptr;
    std::vector<ftr::Base*> targets;
  };
}
#endif // CMD_MATCH_H

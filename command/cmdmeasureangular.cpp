/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project/prjproject.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvmeasureangular.h"
#include "command/cmdmeasureangular.h"

using namespace cmd;

MeasureAngular::MeasureAngular()
: Base("cmd::MeasureAngular")
{
  shouldUpdate = false;
  isFirstRun = true;
}

MeasureAngular::~MeasureAngular() = default;

std::string MeasureAngular::getStatusMessage()
{
  return QObject::tr("Select geometry for angular measure").toStdString();
}

void MeasureAngular::activate()
{
  auto goShow = [&]()
  {
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  };
  
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    const auto &containers = eventHandler->getSelections();
    slc::Messages msgs;
    for (const auto &c : containers) msgs.push_back(eventHandler->containerToMessage(c));
    node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
    viewBase = std::make_unique<cmv::MeasureAngular>(this);
    goShow();
    for (const auto &m : msgs) node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Add, m));
  }
  else goShow();
}

void MeasureAngular::deactivate()
{
  isActive = false;
  msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
  node->sendBlocked(out);
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2017  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QTextStream>

#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepExtrema_DistShapeShape.hxx>

#include <osg/AutoTransform>

#include "selection/slceventhandler.h"
#include "message/msgnode.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvmeasurelinear.h"
#include "command/cmdmeasurelinear.h"

using namespace cmd;

MeasureLinear::MeasureLinear()
: Base()
{
  shouldUpdate = false;
  isFirstRun = true;
}

MeasureLinear::~MeasureLinear(){}

std::string MeasureLinear::getStatusMessage()
{
  return QObject::tr("Select 2 objects for measure linear").toStdString();
}

void MeasureLinear::activate()
{
  auto goShow = [&]()
  {
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  };
  
  isActive = true;
  if (*isFirstRun)
  {
    isFirstRun = false;
    const auto &containers = eventHandler->getSelections();
    slc::Messages msgs;
    for (const auto &c : containers) msgs.push_back(eventHandler->containerToMessage(c));
    node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
    viewBase = std::make_unique<cmv::MeasureLinear>(this);
    goShow();
    for (const auto &m : msgs) node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Add, m));
  }
  else goShow();
  
}

void MeasureLinear::deactivate()
{
  isActive = false;
  msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
  node->sendBlocked(out);
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project/prjproject.h"
#include "message/msgnode.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrinputtype.h"
#include "feature/ftrparametriccurve.h"
#include "tools/featuretools.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvparametriccurve.h"
#include "command/cmdparametriccurve.h"

using namespace cmd;

ParametricCurve::ParametricCurve()
: Base("cmd::ParametricCurve")
, leafManager()
{
  feature = new ftr::ParametricCurve::Feature();
  project->addFeature(std::unique_ptr<ftr::ParametricCurve::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

ParametricCurve::ParametricCurve(ftr::Base *fIn)
: Base("cmd::ParametricCurve")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::ParametricCurve::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::ParametricCurve>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false;
}

ParametricCurve::~ParametricCurve() = default;

std::string ParametricCurve::getStatusMessage()
{
  return QObject::tr("Set Parameters For Parametric Curve Feature").toStdString();
}

void ParametricCurve::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
    viewBase = std::make_unique<cmv::ParametricCurve>(this);
    localUpdate();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
    if (!*isEdit)
      node->sendBlocked(msg::buildSelectionFreeze(feature->getId()));
  }
  else
    sendDone();
}

void ParametricCurve::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
    node->sendBlocked(msg::buildSelectionThaw(feature->getId()));
  }
  isActive = false;
}

void ParametricCurve::setToLinked(const slc::Messages &msIn)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  if (msIn.empty()) return;
  
  const ftr::Base &parent = *project->findFeature(msIn.front().featureId);
  auto pick = tls::convertToPick(msIn.front(), parent, project->getShapeHistory());
  pick.tag = indexTag(prm::Tags::CSysLinked, 0);
  feature->getParameter(prm::Tags::CSysLinked)->setValue({pick});
  project->connect(parent.getId(), feature->getId(), {pick.tag});
}

void ParametricCurve::setToConstant()
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
}

void ParametricCurve::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

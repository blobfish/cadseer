/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <TopoDS.hxx>

#include "tools/featuretools.h"
#include "project/prjproject.h"
#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "parameter/prmconstants.h"
#include "feature/ftrremovefaces.h"
#include "commandview/cmvmessage.h"
#include "commandview/cmvremovefaces.h"
#include "command/cmdremovefaces.h"

using boost::uuids::uuid;

using namespace cmd;

RemoveFaces::RemoveFaces()
: Base()
, leafManager()
{
  feature = new ftr::RemoveFaces::Feature();
  project->addFeature(std::unique_ptr<ftr::RemoveFaces::Feature>(feature));
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  isEdit = false;
  isFirstRun = true;
}

RemoveFaces::RemoveFaces(ftr::Base *fIn)
: Base("cmd::RemoveFaces")
, leafManager(fIn)
{
  feature = dynamic_cast<ftr::RemoveFaces::Feature*>(fIn);
  assert(feature);
  viewBase = std::make_unique<cmv::RemoveFaces>(this);
  node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
  isEdit = true;
  isFirstRun = false; //bypass go() in activate.
}

RemoveFaces::~RemoveFaces() = default;

std::string RemoveFaces::getStatusMessage()
{
  return QObject::tr("Select faces to remove").toStdString();
}

void RemoveFaces::activate()
{
  isActive = true;
  leafManager.rewind();
  if (*isFirstRun)
  {
    isFirstRun = false;
    go();
  }
  if (viewBase)
  {
    feature->setEditing();
    cmv::Message vm(viewBase.get(), viewBase->getPaneWidth());
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Show), vm);
    node->sendBlocked(out);
  }
  else
    sendDone();
}

void RemoveFaces::deactivate()
{
  if (viewBase)
  {
    feature->setNotEditing();
    msg::Message out(msg::Mask(msg::Request | msg::Command | msg::View | msg::Hide));
    node->sendBlocked(out);
  }
  leafManager.fastForward();
  if (!*isEdit)
  {
    project->hideAlterParents(feature->getId());
    node->sendBlocked(msg::buildShowThreeD(feature->getId()));
    node->sendBlocked(msg::buildShowOverlay(feature->getId()));
  }
  isActive = false;
}

void RemoveFaces::setSelections(const std::vector<slc::Message> &targets)
{
  assert(isActive);
  project->clearAllInputs(feature->getId());
  setPicksInsert(*feature, prm::Tags::Picks, targets);
  if (!targets.empty())
  {
    const ftr::Base *tf = project->findFeature(targets.front().featureId);
    feature->setColor(tf->getColor());
  }
}

void RemoveFaces::localUpdate()
{
  assert(isActive);
  feature->updateModel(project->getPayload(feature->getId()));
  feature->updateVisual();
  feature->setModelDirty();
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
}

void RemoveFaces::go()
{
  const slc::Containers &containers = eventHandler->getSelections();
  
  std::vector<slc::Message> tm; //target messages
  for (const auto &c : containers)
  {
    if (c.selectionType == slc::Type::Face)
      tm.push_back(slc::EventHandler::containerToMessage(c));
  }
  
  if (!tm.empty())
  {
    setSelections(tm);
    node->sendBlocked(msg::buildStatusMessage("Remove Faces Added", 2.0));
    node->sendBlocked(msg::Message(msg::Request | msg::Selection | msg::Clear));
    node->sendBlocked(msg::buildHideThreeD(tm.front().featureId));
    node->sendBlocked(msg::buildHideOverlay(tm.front().featureId));
    return;
  }
  
  node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
  node->sendBlocked(msg::buildHideThreeD(feature->getId()));
  node->sendBlocked(msg::buildHideOverlay(feature->getId()));
  viewBase = std::make_unique<cmv::RemoveFaces>(this);
}

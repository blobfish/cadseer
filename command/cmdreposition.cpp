/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2017  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "globalutilities.h"
#include "project/prjproject.h"
#include "application/appmainwindow.h"
#include "selection/slceventhandler.h"
#include "message/msgnode.h"
#include "message/msgmessage.h"
#include "viewer/vwrwidget.h"
#include "annex/anncsysdragger.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrbase.h"
#include "command/cmdreposition.h"

using namespace cmd;

Reposition::Reposition() : Base() {}

Reposition::~Reposition(){}

std::string Reposition::getStatusMessage()
{
  return QObject::tr("Select feature to reposition").toStdString();
}

void Reposition::activate()
{
  isActive = true;
  go();
  sendDone();
}

void Reposition::deactivate()
{
  isActive = false;
}

void Reposition::go()
{
  shouldUpdate = false;
  //only works with preselection for now.
  const osg::Matrixd& cs = mainWindow->getViewer()->getPresentSystem();
  const slc::Containers &containers = eventHandler->getSelections();
  for (const auto &container : containers)
  {
    if (!slc::isObjectType(container.selectionType))
    {
      node->send(msg::buildStatusMessage("Skipping Non Object Selection", 2.0));
      continue;
    }
    ftr::Base *baseFeature = project->findFeature(container.featureId); assert(baseFeature);
    //skip if we have a 'type' of csys that is linked.
    auto clp = baseFeature->getParameters(prm::Tags::CSysType);
    if (!clp.empty() && clp.front()->getInt() != 0)
    {
      node->send(msg::buildStatusMessage("Skipping Linked System", 2.0));
      continue;
    }
    
    //if has a dragger work through that.
    if (baseFeature->hasAnnex(ann::Type::CSysDragger))
    {
      ann::CSysDragger &da = baseFeature->getAnnex<ann::CSysDragger>(ann::Type::CSysDragger);
      if (!da.parameter->isConstant())
      {
        node->send(msg::buildStatusMessage("Skipping Expression Linked System", 2.0));
        continue;
      }
      da.draggerUpdate(cs, ann::CSysDragger::RespectLink);
      shouldUpdate = true;
    }
    else
    {
      //fall back to work directly on parameter.
      auto paramSys = baseFeature->getParameters(prm::Tags::CSys);
      if (paramSys.empty())
      {
        node->send(msg::buildStatusMessage("Skipping Feature Without CSys Parameter", 2.0));
        continue;
      }
      paramSys.front()->setValue(cs);
      shouldUpdate = true;
    }
  }
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "message/msgnode.h"
#include "selection/slceventhandler.h"
#include "viewer/vwrwidget.h"
#include "tools/featuretools.h"
#include "command/cmdsystemtoselection.h"

using namespace cmd;

SystemToSelection::SystemToSelection() : Base()
{
  shouldUpdate = false;
}

SystemToSelection::~SystemToSelection() {}

std::string SystemToSelection::getStatusMessage()
{
  return QObject::tr("Select geometry for derived system").toStdString();
}

void SystemToSelection::activate()
{
  isActive = true;
  go();
  sendDone();
}

void SystemToSelection::deactivate()
{
  isActive = false;
}

void SystemToSelection::go()
{
  const slc::Containers &containers = eventHandler->getSelections();
  if (containers.empty())
  {
    node->sendBlocked(msg::buildStatusMessage("No selection for system derivation", 2.0));
    return;
  }
  
  slc::Messages msgs;
  for (const auto &c : containers) msgs.push_back(eventHandler->containerToMessage(c));
  auto freshSys = tls::gleanSystem(msgs);
  if (!freshSys)
  {
    node->sendBlocked(msg::buildStatusMessage("Selection not supported for system definition", 2.0));
    return;
  }
  viewer->setPresentSystem(*freshSys);
}

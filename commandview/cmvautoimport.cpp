/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QFileDialog>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QTimer>
#include <QTextEdit>

#include "project/prjproject.h"
#include "command/cmdautoimport.h"
#include "commandview/cmvautoimport.h"

using boost::uuids::uuid;

using namespace cmv;

struct AutoImport::Stow
{
  cmd::AutoImport *command;
  cmv::AutoImport *view;
  QTimer timer;
  
  QLineEdit *pathEdit = nullptr;
  QPushButton *browseButton = nullptr;
  QTextEdit *fileList = nullptr;
  
  Stow(cmd::AutoImport *cIn, cmv::AutoImport *vIn)
  : command(cIn)
  , view(vIn)
  {
    buildGui();
    connect(browseButton, &QPushButton::clicked, view, &AutoImport::requestBrowse);
    connect(pathEdit, &QLineEdit::editingFinished, view, &AutoImport::editingFinished);
    connect(&timer, &QTimer::timeout, view, &AutoImport::timeOut);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    pathEdit = new QLineEdit(view);
    browseButton = new QPushButton(tr("Browse"), view);
    QHBoxLayout *browseLayout = new QHBoxLayout();
    browseLayout->addWidget(pathEdit);
    browseLayout->addWidget(browseButton);
    mainLayout->addLayout(browseLayout);
    
    fileList = new QTextEdit(view);
    fileList->setWordWrapMode(QTextOption::NoWrap);
    fileList->setReadOnly(true);
    mainLayout->addWidget(fileList);
    
    // mainLayout->addStretch();
  }
};

AutoImport::AutoImport(cmd::AutoImport *cIn)
: Base("cmv::AutoImport")
, stow(std::make_unique<Stow>(cIn, this))
{
  goMaskDefault();
}

AutoImport::~AutoImport() = default;

void AutoImport::setPath(const QString &pIn)
{
  //we assume everything is legit.
  stow->pathEdit->setText(pIn);
  stow->fileList->clear();
  stow->timer.start(1000); //every second.
}

void AutoImport::fileAdded(const QString &freshFile)
{
  stow->fileList->append(freshFile);
  stow->fileList->append(QString::fromUtf8("\n"));
}

void AutoImport::requestBrowse()
{
  auto freshDirectory = QFileDialog::getExistingDirectory
  (
    this
    , tr("Watch Directory")
    , stow->pathEdit->text()
  );
  if (freshDirectory.isEmpty()) return;
  stow->command->changeDirectory(freshDirectory.toStdString());
  //if the command is successful it will call back to this->setPath()
}

void AutoImport::timeOut()
{
  stow->command->scan();
}

void AutoImport::editingFinished()
{
  auto freshDirectory = stow->pathEdit->text();
  if (freshDirectory.isEmpty()) return;
  stow->command->changeDirectory(freshDirectory.toStdString());
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QVBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrdatumpoint.h"
#include "tools/featuretools.h"
#include "command/cmddatumpoint.h"
#include "commandview/cmvdatumpoint.h"

using boost::uuids::uuid;

using namespace cmv;

struct DatumPoint::Stow
{
  cmd::DatumPoint *command;
  cmv::DatumPoint *view;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  
  Stow(cmd::DatumPoint *cIn, cmv::DatumPoint *vIn)
  : command(cIn)
  , view(vIn)
  {
    buildGui();
    prmView->updateHideInactive();
    connect(prmModel, &tbl::Model::dataChanged, view, &DatumPoint::modelChanged);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, command->feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    updateCue();
  }
  
  void updateCue()
  {
    tbl::SelectionCue cue;
    cue.singleSelection = true;
    cue.accrueEnabled = false;
    
    switch(command->feature->getCurrentPointType())
    {
      case ftr::DatumPoint::PointType::Constant: return;
      case ftr::DatumPoint::PointType::AtPoint:
      {
        cue.mask = slc::AllPointsEnabled | slc::EndPointsSelectable;
        cue.statusPrompt = tr("Select Point");
        break;
      }
      case ftr::DatumPoint::PointType::EdgeParameter:
      {
        cue.mask = slc::NearestPointsBoth | slc::WiresEnabled | slc::EdgesEnabled;
        cue.statusPrompt = tr("Select Nearest Point, Wire Or Edge");
        break;
      }
      case ftr::DatumPoint::PointType::FaceParameters:
      {
        cue.mask = slc::FacesBoth;
        cue.statusPrompt = tr("Select Face At Desired Point");
        break;
      }
    }
    prmModel->setCue(command->feature->getParameter(prm::Tags::Picks), cue);
  }
};

DatumPoint::DatumPoint(cmd::DatumPoint *cIn)
: Base("cmv::DatumPoint")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

DatumPoint::~DatumPoint() = default;

void DatumPoint::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  
  auto *param = stow->prmModel->getParameter(index); assert(param);
  if (param->getTag() == ftr::DatumPoint::Tags::pointType)
  {
    auto freshType = static_cast<ftr::DatumPoint::PointType>(param->getInt());
    auto *pickParam = stow->command->feature->getParameter(prm::Tags::Picks);
    stow->prmModel->setMessages(pickParam, {});
    switch(freshType)
    {
      case ftr::DatumPoint::PointType::Constant: {stow->command->setToConstant(); break;}
      case ftr::DatumPoint::PointType::AtPoint: {stow->command->setToAtPoint({}); break;}
      case ftr::DatumPoint::PointType::EdgeParameter: {stow->command->setToEdgeParameter({}); break;}
      case ftr::DatumPoint::PointType::FaceParameters: {stow->command->setToFaceParameters({}); break;}
    }
    stow->prmView->updateHideInactive();
    stow->updateCue();
  }
  
  auto currentType = stow->command->feature->getCurrentPointType(); //might have been changed above.
  
  if (param->getTag() == prm::Tags::Picks)
  {
    if (currentType == ftr::DatumPoint::PointType::AtPoint)
      stow->command->setToAtPoint(stow->prmModel->getMessages(param));
    if (currentType == ftr::DatumPoint::PointType::EdgeParameter)
      stow->command->setToEdgeParameter(stow->prmModel->getMessages(param));
    if (currentType == ftr::DatumPoint::PointType::FaceParameters)
      stow->command->setToFaceParameters(stow->prmModel->getMessages(param));
  }
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2020 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QComboBox>
#include <QGroupBox>
#include <QPushButton>
#include <QToolButton>
#include <QButtonGroup>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QMenu>
#include <QItemSelection>

#include <osg/Quat>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "dialogs/dlgsplitterdecorated.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "feature/ftrdatumsystem.h"
#include "command/cmddatumsystem.h"
#include "commandview/cmvdatumsystem.h"

using boost::uuids::uuid;

namespace
{
  namespace Rotation
  {
    enum Constant
    {
      X90
      , Y90
      , Z90
      , X90Neg
      , Y90Neg
      , Z90Neg
      , X180
      , Y180
      , Z180
    };
    struct Group
    {
      Constant constant;
      QString text;
      osg::Quat value;
    };
    using Groups = std::vector<Group>;
    Groups buildGroups()
    {
      Groups out;
      out.push_back({Constant::X90, QObject::tr("X 90"), osg::Quat(osg::PI_2, osg::Vec3d(1.0, 0.0, 0.0))});
      out.push_back({Constant::Y90, QObject::tr("Y 90"), osg::Quat(osg::PI_2, osg::Vec3d(0.0, 1.0, 0.0))});
      out.push_back({Constant::Z90, QObject::tr("Z 90"), osg::Quat(osg::PI_2, osg::Vec3d(0.0, 0.0, 1.0))});
      out.push_back({Constant::X90Neg, QObject::tr("X -90"), osg::Quat(osg::PI_2, osg::Vec3d(-1.0, 0.0, 0.0))});
      out.push_back({Constant::Y90Neg, QObject::tr("Y -90"), osg::Quat(osg::PI_2, osg::Vec3d(0.0, -1.0, 0.0))});
      out.push_back({Constant::Z90Neg, QObject::tr("Z -90"), osg::Quat(osg::PI_2, osg::Vec3d(0.0, 0.0, -1.0))});
      out.push_back({Constant::X180, QObject::tr("X 180"), osg::Quat(osg::PI, osg::Vec3d(1.0, 0.0, 0.0))});
      out.push_back({Constant::Y180, QObject::tr("Y 180"), osg::Quat(osg::PI, osg::Vec3d(0.0, 1.0, 0.0))});
      out.push_back({Constant::Z180, QObject::tr("Z 180"), osg::Quat(osg::PI, osg::Vec3d(0.0, 0.0, 1.0))});
      return out;
    }
  }
}

using namespace cmv;

struct DatumSystem::Stow
{
  cmd::DatumSystem *command;
  cmv::DatumSystem *view;
  prm::Parameters parameters;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  
  cmv::tbl::Model *postOpPrmModel = nullptr;
  cmv::tbl::View *postOpPrmView = nullptr;
  QToolButton *translation = nullptr;
  QToolButton *rotation = nullptr;
  QToolButton *addPostOp = nullptr;
  QToolButton *removePostOp = nullptr;
  QToolButton *moveOpUp = nullptr;
  QToolButton *moveOpDown = nullptr;
  dlg::SplitterDecorated *splitter = nullptr;
  
  Rotation::Groups rotationGroups = Rotation::buildGroups();
  osg::Quat  currentMenuRotation = rotationGroups.at(0).value;
  
  Stow(cmd::DatumSystem *cIn, cmv::DatumSystem *vIn)
  : command(cIn)
  , view(vIn)
  {
    parameters = command->feature->getParameters();
    //we have to exclude post of parameters from this list.
    for (const auto *param : command->feature->getPostOps())
    {
      auto it = std::find(parameters.begin(), parameters.end(), param);
      if (it != parameters.end()) parameters.erase(it);
    }
    
    buildGui();
    splitter->restoreSettings("cmv::DatumSystem::splitter");
    plug();
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    splitter = new dlg::SplitterDecorated(view);
    splitter->setOrientation(Qt::Vertical);
    splitter->setChildrenCollapsible(false);
    mainLayout->addWidget(splitter);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, parameters, std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    splitter->addWidget(prmView);
    
    const auto *ft = command->feature;
    
    //no cues for constant
    {
      //linked
      tbl::SelectionCue cue;
      cue.singleSelection = true;
      cue.mask = slc::ObjectsBoth;
      cue.statusPrompt = tr("Select Feature To Link Axis");
      cue.accrueEnabled = false;
      prmModel->setCue(ft->getParameter(ftr::DatumSystem::Tags::Linked), cue);
    }
    {
      //points
      tbl::SelectionCue cue;
      cue.singleSelection = false;
      cue.mask = slc::AllPointsEnabled | slc::EndPointsSelectable;
      cue.statusPrompt = tr("Select 2 Points For Axis");
      cue.accrueEnabled = false;
      prmModel->setCue(ft->getParameter(ftr::DatumSystem::Tags::Points), cue);
    }
    {
      //shape infer
      tbl::SelectionCue cue;
      cue.singleSelection = true;
      cue.mask = slc::FacesBoth | slc::EdgesBoth | slc::AllPointsEnabled;
      cue.statusPrompt = tr("Select 1 Shape For Infer");
      cue.accrueEnabled = false;
      prmModel->setCue(ft->getParameter(ftr::DatumSystem::Tags::ShapeInfer), cue);
    }
    {
      //define
      tbl::SelectionCue originCue;
      originCue.singleSelection = true;
      originCue.mask = slc::ObjectsEnabled | slc::AllPointsEnabled;
      originCue.statusPrompt = tr("Select Object Or Point To Define Origin");
      originCue.accrueEnabled = false;
      prmModel->setCue(ft->getParameter(prm::Tags::Origin), originCue);
      
      tbl::SelectionCue axisCue;
      axisCue.singleSelection = false;
      axisCue.mask = slc::ObjectsEnabled | slc::FacesEnabled | slc::EdgesEnabled | slc::AllPointsEnabled;
      axisCue.statusPrompt = tr("Select Object Or Shape To Define Axis");
      axisCue.accrueEnabled = false;
      prmModel->setCue(ft->getParameter(ftr::DatumSystem::Tags::Axis0), axisCue);
      prmModel->setCue(ft->getParameter(ftr::DatumSystem::Tags::Axis1), axisCue);
    }
    
    //adding post op gui.
    auto *groupBox = new QGroupBox(QObject::tr("Post Operations"), view);
    splitter->addWidget(groupBox);
    auto *groupLayout = new QVBoxLayout();
    groupBox->setLayout(groupLayout);
    
    auto *gridLayout = new QGridLayout();
    groupLayout->addLayout(gridLayout);
    //first row
    auto *typeGroup = new QButtonGroup(groupBox);
    translation = new QToolButton(groupBox);
    translation->setText(QObject::tr("Move"));
    translation->setCheckable(true);
    translation->setSizePolicy(QSizePolicy::Expanding, translation->sizePolicy().verticalPolicy());
    typeGroup->addButton(translation, 0);
    gridLayout->addWidget(translation, 0, 0);
    translation->setChecked(true);
    
    rotation = new QToolButton(groupBox);
    rotation->setCheckable(true);
    rotation->setText(QObject::tr("Spin X 90"));
    rotation->setSizePolicy(QSizePolicy::Expanding, translation->sizePolicy().verticalPolicy());
    QMenu *menu = new QMenu(rotation);
    int index = 0; //encode index of groups into action
    for (const auto &rg : rotationGroups)
    {
      auto *action = menu->addAction(rg.text);
      action->setData(index);
      ++index;
    }
    rotation->setMenu(menu);
    QObject::connect(menu, &QMenu::triggered, view, &DatumSystem::spinMenuTriggered);
    typeGroup->addButton(rotation, 1);
    gridLayout->addWidget(rotation, 0, 1);
    
    //second row
    addPostOp = new QToolButton(groupBox); addPostOp->setText(QObject::tr("Add"));
    addPostOp->setSizePolicy(QSizePolicy::Expanding, translation->sizePolicy().verticalPolicy());
    gridLayout->addWidget(addPostOp, 1, 0);
    removePostOp = new QToolButton(groupBox); removePostOp->setText(QObject::tr("Remove"));
    removePostOp->setSizePolicy(QSizePolicy::Expanding, translation->sizePolicy().verticalPolicy());
    removePostOp->setEnabled(false);
    gridLayout->addWidget(removePostOp, 1, 1);
    
    //third row
    moveOpUp = new QToolButton(groupBox); moveOpUp->setText(QObject::tr("Up"));
    moveOpUp->setSizePolicy(QSizePolicy::Expanding, translation->sizePolicy().verticalPolicy());
    moveOpUp->setEnabled(false);
    gridLayout->addWidget(moveOpUp, 2, 0);
    moveOpDown = new QToolButton(groupBox); moveOpDown->setText(QObject::tr("Down"));
    moveOpDown->setSizePolicy(QSizePolicy::Expanding, translation->sizePolicy().verticalPolicy());
    moveOpDown->setEnabled(false);
    gridLayout->addWidget(moveOpDown, 2, 1);
    
    std::optional<tls::Resolver> resolver2(up);
    postOpPrmModel = new tbl::Model(groupBox, command->feature->getPostOps(), std::move(resolver2));
    postOpPrmView = new tbl::View(groupBox, postOpPrmModel, true);
    groupLayout->addWidget(postOpPrmView);
    groupLayout->addStretch();
  }
  
  void plug()
  {
    connect(prmModel, &tbl::Model::dataChanged, view, &DatumSystem::modelChanged);
    connect(postOpPrmModel, &tbl::Model::dataChanged, view, &DatumSystem::postOpChanged);
    connect(addPostOp, &QPushButton::clicked, view, &DatumSystem::addPostOp);
    connect(postOpPrmView->selectionModel(), &QItemSelectionModel::selectionChanged, view, &DatumSystem::postOpSelectionChange);
    connect(removePostOp, &QPushButton::clicked, view, &DatumSystem::removePostOp);
    connect(moveOpUp, &QPushButton::clicked, view, &DatumSystem::movePostOpUp);
    connect(moveOpDown, &QPushButton::clicked, view, &DatumSystem::movePostOpDown);
  }
  
  void setDefine()
  {
    const auto *f = command->feature;
    const auto &oMsgs = prmModel->getMessages(f->getParameter(prm::Tags::Origin));
    const auto &a0Msgs = prmModel->getMessages(f->getParameter(ftr::DatumSystem::Tags::Axis0));
    const auto &a1Msgs = prmModel->getMessages(f->getParameter(ftr::DatumSystem::Tags::Axis1));
    command->setDefine(oMsgs, a0Msgs, a1Msgs);
  }
};

DatumSystem::DatumSystem(cmd::DatumSystem *cIn)
: Base("cmv::DatumSystem")
, stow(new Stow(cIn, this))
{
  node->sendBlocked(msg::buildStatusMessage("Double Click Parameter To Edit"));
  goSelectionToolbar();
  goMaskDefault();
}

DatumSystem::~DatumSystem() = default;

void DatumSystem::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  using DSType = ftr::DatumSystem::SystemType;
  namespace DSTags = ftr::DatumSystem::Tags;
  prm::Parameter *par = stow->parameters.at(index.row());
  if (par->getTag() == DSTags::SystemType)
  {
    //assign par to 'trick' tests below.
    const auto *ft = stow->command->feature;
    auto systemType = static_cast<DSType>(par->getInt());
    switch(systemType)
    {
      case DSType::Constant:{stow->command->setConstant(); break;}
      case DSType::Linked:{par = ft->getParameter(DSTags::Linked); break;}
      case DSType::Through3Points:{par = ft->getParameter(DSTags::Points); break;}
      case DSType::ShapeInfer:{par = ft->getParameter(DSTags::ShapeInfer); break;}
      case DSType::Define:{par = ft->getParameter(DSTags::AxisStyle); break;}
    }
    stow->prmView->updateHideInactive();
  }
  
  const auto &msgs = stow->prmModel->getMessages(par);
  if (par->getTag() == DSTags::Linked) stow->command->setLinked(msgs);
  else if (par->getTag() == DSTags::Points) stow->command->set3Points(msgs);
  else if (par->getTag() == DSTags::ShapeInfer) stow->command->setShapeInfer(msgs);
  else if (par->getTag() == DSTags::AxisStyle) stow->setDefine();
  else if (par->getTag() == prm::Tags::Origin) stow->setDefine();
  else if (par->getTag() == DSTags::Axis0) stow->setDefine();
  else if (par->getTag() == DSTags::Axis1) stow->setDefine();
  else if (par->getTag() == prm::Tags::AutoSize) stow->prmView->updateHideInactive();
  
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

void DatumSystem::postOpChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
}

void DatumSystem::addPostOp()
{
  if (stow->translation->isChecked())
  {
    auto *param = stow->command->feature->createPostOpTranslation();
    stow->postOpPrmModel->addParameter(param);
    stow->command->localUpdate();
    node->sendBlocked(msg::buildStatusMessage("Translation Operation Added", 2.0));
    node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  }
  else if (stow->rotation->isChecked())
  {
    auto *param = stow->command->feature->createPostOpRotation();
    param->setValue(stow->currentMenuRotation);
    stow->postOpPrmModel->addParameter(param);
    stow->command->localUpdate();
    node->sendBlocked(msg::buildStatusMessage("Rotation Operation Added", 2.0));
    node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  }
}

void DatumSystem::removePostOp()
{
  auto currentSelection = stow->postOpPrmView->selectionModel()->selectedRows(1);
  assert(!currentSelection.isEmpty()); if (currentSelection.isEmpty()) return;
  assert(currentSelection.front().isValid()); if (!currentSelection.front().isValid()) return;
  stow->postOpPrmModel->removeParameter(currentSelection.front());
  stow->command->feature->removePostOp(currentSelection.front().row());
  stow->postOpPrmView->clearSelection();
  
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage("Operation Removed", 2.0));
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
}

void DatumSystem::spinMenuTriggered(QAction *action)
{
  auto v = action->data();
  assert(v.isValid() && v.canConvert<int>());
  int groupIndex = action->data().toInt();
  assert(groupIndex >= 0 && groupIndex < static_cast<int>(stow->rotationGroups.size()));
  
  stow->rotation->setText(QObject::tr("Spin") + QChar::Space + stow->rotationGroups.at(groupIndex).text);
  stow->currentMenuRotation = stow->rotationGroups.at(groupIndex).value;
  stow->rotation->setChecked(true);
}

void DatumSystem::postOpSelectionChange(const QItemSelection &selected, const QItemSelection&)
{
  if (selected.indexes().isEmpty() || !selected.indexes().front().isValid())
  {
    stow->removePostOp->setEnabled(false);
    stow->moveOpUp->setEnabled(false);
    stow->moveOpDown->setEnabled(false);
  }
  else
  {
    stow->removePostOp->setEnabled(true);
    if (stow->command->feature->getPostOpSize() == 1)
    {
      stow->moveOpUp->setEnabled(false);
      stow->moveOpDown->setEnabled(false);
    }
    else
    {
      int row = selected.indexes().front().row();
      if (row > 0) stow->moveOpUp->setEnabled(true);
      else stow->moveOpUp->setEnabled(false);
      if (row < stow->command->feature->getPostOpSize() - 1) stow->moveOpDown->setEnabled(true);
      else stow->moveOpDown->setEnabled(false);
    }
  }
}

void DatumSystem::movePostOpUp()
{
  auto currentSelection = stow->postOpPrmView->selectionModel()->selectedRows(1);
  assert(!currentSelection.isEmpty()); if (currentSelection.isEmpty()) return;
  assert(currentSelection.front().isValid()); if (!currentSelection.front().isValid()) return;
  int index = currentSelection.front().row();
  stow->command->feature->moveUp(index);
  stow->postOpPrmModel->moveParameterUp(currentSelection.front());
  stow->postOpPrmView->clearSelection();
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage("Operation Moved Up", 2.0));
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
}

void DatumSystem::movePostOpDown()
{
  auto currentSelection = stow->postOpPrmView->selectionModel()->selectedRows(1);
  assert(!currentSelection.isEmpty()); if (currentSelection.isEmpty()) return;
  assert(currentSelection.front().isValid()); if (!currentSelection.front().isValid()) return;
  int index = currentSelection.front().row();
  stow->command->feature->moveDown(index);
  stow->postOpPrmModel->moveParameterDown(currentSelection.front());
  stow->postOpPrmView->clearSelection();
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage("Operation Moved Down", 2.0));
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
}

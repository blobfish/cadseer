/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "feature/ftrpick.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "command/cmdexplode.h"
#include "commandview/cmvexplode.h"

using boost::uuids::uuid;

using namespace cmv;

struct Explode::Stow
{
  cmd::Explode *command;
  cmv::Explode *view;
  prm::Parameter source{QObject::tr("Source"), ftr::Picks(), prm::Tags::Picks};
  prm::Parameter shapeType{QObject::tr("Shape Type"), 0, "ShapeType"};
  prm::Parameters parameters;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
//   std::vector<prm::Observer> observers;
  
  Stow(cmd::Explode *cIn, cmv::Explode *vIn)
  : command(cIn)
  , view(vIn)
  {
    QStringList tStrings =
    {
      QObject::tr("CompSolids")
      , QObject::tr("Solids")
      , QObject::tr("Shells")
      , QObject::tr("Faces")
      , QObject::tr("Wires")
      , QObject::tr("Edges")
      , QObject::tr("All")
    };
    shapeType.setEnumeration(tStrings);
    
    parameters = {&source, &shapeType};
    buildGui();
    connect(prmModel, &tbl::Model::dataChanged, view, &Explode::modelChanged);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    prmModel = new tbl::Model(view, parameters, std::nullopt);
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    tbl::SelectionCue cue;
    cue.singleSelection = false;
    cue.mask = slc::AllEnabled & ~slc::AllPointsEnabled & ~slc::AllPointsSelectable ;
    cue.statusPrompt = tr("Select Entities To Explode");
    cue.accrueEnabled = false;
    prmModel->setCue(&source, cue);
    
    QHBoxLayout *eLayout = new QHBoxLayout();
    eLayout->addStretch();
    QPushButton *eButton = new QPushButton(tr("Explode"), view);
    eLayout->addWidget(eButton);
    eLayout->addStretch();
    mainLayout->addLayout(eLayout);
    QObject::connect(eButton, &QPushButton::clicked, [this](){this->goExplode();});
  }
  
  void goExplode()
  {
    const auto &msgs = prmModel->getMessages(&source);
    if (msgs.empty())
    {
      view->node->send(msg::buildStatusMessage("Nothing Selected For Explode", 2.0));
      return;
    }
    int type = shapeType.getInt();
    if (type == 6) command->goExplodeAll(msgs);
    else
    {
      type += 1; //we don't expose compound so bump up to match enum
      command->goExplode(msgs, static_cast<TopAbs_ShapeEnum>(type));
    }
    msg::Message mOut(msg::Mask(msg::Request | msg::Command | msg::Done));
    app::instance()->queuedMessage(mOut);
  }
};

Explode::Explode(cmd::Explode *cIn)
: Base("cmv::Explode")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

Explode::~Explode() = default;

void Explode::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  auto changedTag = stow->prmModel->getParameter(index)->getTag();
  if (changedTag == prm::Tags::Picks)
  {
    ftr::Picks picksToSet;
    const auto &selections = stow->prmModel->getMessages(&stow->source);
    for (const auto &s : selections)
    {
      const auto *p = app::instance()->getProject();
      auto *f = p->findFeature(s.featureId);
      assert(f); if (!f) continue;
      picksToSet.push_back(tls::convertToPick(s, *f, p->getShapeHistory()));
    }
    stow->source.setValue(picksToSet);
  }
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

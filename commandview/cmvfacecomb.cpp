/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QVBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "feature/ftrfacecomb.h"
#include "command/cmdfacecomb.h"
#include "commandview/cmvfacecomb.h"

using boost::uuids::uuid;

using namespace cmv;

struct FaceComb::Stow
{
  cmd::FaceComb *command;
  cmv::FaceComb *view;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  
  Stow(cmd::FaceComb *cIn, cmv::FaceComb *vIn)
  : command(cIn)
  , view(vIn)
  {
    buildGui();
    connect(prmModel, &tbl::Model::dataChanged, view, &FaceComb::modelChanged);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, command->feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    tbl::SelectionCue cue;
    cue.singleSelection = true;
    cue.mask = slc::ObjectsEnabled | slc::FacesBoth;
    cue.statusPrompt = tr("Select Face");
    cue.accrueEnabled = false;
    prmModel->setCue(command->feature->getParameter(prm::Tags::Picks), cue);
  }
};

FaceComb::FaceComb(cmd::FaceComb *cIn)
: Base("cmv::FaceComb")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

FaceComb::~FaceComb() = default;

void FaceComb::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  auto changedTag = stow->prmModel->getParameter(index)->getTag();
  if (changedTag == prm::Tags::Picks)
  {
    const auto &picks = stow->prmModel->getMessages(stow->command->feature->getParameter(prm::Tags::Picks));
    stow->command->setSelections(picks);
  }
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

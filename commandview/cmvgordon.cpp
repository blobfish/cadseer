/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2025 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
// #include <QComboBox>
// #include <QPushButton>
// #include <QLabel>
// #include <QLineEdit>
// #include <QStackedWidget>
// #include <QGridLayout>
#include <QVBoxLayout>
// #include <QHBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
// #include "annex/annseershape.h"
// #include "preferences/preferencesXML.h"
// #include "preferences/prfmanager.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
// #include "expressions/exprmanager.h"
// #include "expressions/exprvalue.h"
// #include "library/lbrplabel.h"
#include "tools/featuretools.h"
// #include "tools/idtools.h"
// #include "feature/ftrinputtype.h"
#include "feature/ftrgordon.h"
#include "command/cmdgordon.h"
#include "commandview/cmvgordon.h"

using boost::uuids::uuid;

using namespace cmv;

struct Gordon::Stow
{
  cmd::Gordon *command;
  cmv::Gordon *view;
  prm::Parameters parameters;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
//   prm::Observer observer{[&](){prmView->updateHideInactive();}};
  
  Stow(cmd::Gordon *cIn, cmv::Gordon *vIn)
  : command(cIn)
  , view(vIn)
  {
    parameters = command->feature->getParameters();
    buildGui();
    connect(prmModel, &tbl::Model::dataChanged, view, &Gordon::modelChanged);
    // command->feature->getParameter(ftr::Gordon::Tags::GordonType)->connect(observer);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, command->feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    tbl::SelectionCue cue;
    cue.singleSelection = false;
    cue.mask = slc::ObjectsEnabled | slc::WiresEnabled | slc::EdgesBoth;
    cue.statusPrompt = tr("Select U Edges");
    cue.accrueEnabled = false;
    prmModel->setCue(command->feature->getParameter(ftr::Gordon::Tags::uPicks), cue);
    cue.statusPrompt = tr("Select V Edges");
    prmModel->setCue(command->feature->getParameter(ftr::Gordon::Tags::vPicks), cue);
  }
};

Gordon::Gordon(cmd::Gordon *cIn)
: Base("cmv::Gordon")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

Gordon::~Gordon() = default;

bool Gordon::eventFilter(QObject *watched, QEvent *event)
{
  //installed on child widgets so we can dismiss persistent editors
  if(event->type() == QEvent::FocusIn)
  {
    stow->prmView->closePersistent();
  }
  
  return QObject::eventFilter(watched, event);
}

void Gordon::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  auto changedTag = stow->prmModel->getParameter(index)->getTag();
  if (changedTag == ftr::Gordon::Tags::uPicks || changedTag == ftr::Gordon::Tags::vPicks)
  {
    const auto &uPicks = stow->prmModel->getMessages(stow->command->feature->getParameter(ftr::Gordon::Tags::uPicks));
    const auto &vPicks = stow->prmModel->getMessages(stow->command->feature->getParameter(ftr::Gordon::Tags::vPicks));
    stow->command->setSelections(uPicks, vPicks);
  }
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

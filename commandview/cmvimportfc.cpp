/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2022 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <BRep_Builder.hxx>
#include <BRepTools.hxx>

#include <QSettings>
#include <QGroupBox>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QTimer>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "tools/idtools.h"
#include "feature/ftrinert.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "dialogs/dlgsplitterdecorated.h"
#include "subprojects/libfcexplore/fcexplorer.h"
#include "command/cmdimportfc.h"
#include "commandview/cmvimportfc.h"

using boost::uuids::uuid;

namespace
{
  struct NameVisitor
  {
    std::vector<std::string> names;
    void visitObject(const Object &o)
    {
      names.emplace_back(o.name());
    }
  };
  
  struct LabelNameVisitor
  {
    std::string objectName;
    std::string labelName;
    bool found = false;
    LabelNameVisitor(const std::string &objectNameIn) : objectName(objectNameIn){}
    
    void visitObjectData(const Object &o)
    {
      if (o.name() == objectName) found = true;
      else found = false;
    }
    
    void visitObjectDataProperty(const Property &property)
    {
      if (!found) return;
      if (property.String().present() && property.name() == "Label")
        labelName = property.String()->value();
    }
  };
  
  struct PartFileVisitor
  {
    std::string name;
    std::string partFile;
    bool found = false;
    PartFileVisitor(const std::string &nameIn) : name(nameIn){}
    
    void visitObjectData(const Object &o)
    {
      if (o.name() == name) found = true;
      else found = false;
    }
    
    void visitObjectDataProperty(const Property &property)
    {
      if (!found) return;
      if (property.Part().present() && property.name() == "Shape")
        partFile = property.Part()->file();
    }
  };
  
  /* We skip partdesign::body because they are incoherent with
   * the dependency graph.
   */
  struct FilterVisitor
  {
    bool skip = false;
    std::vector<std::string> objects;
    std::vector<std::string> referenced;
    std::vector<std::string> shapeObjects;
    const std::set<std::string> skipTypes
    {
      ("PartDesign::Body")
      , ("App::DocumentObjectGroup")
    };
    const std::set<std::string> skipBenches
    {
      ("TechDraw")
    };
    std::set<std::string> skipNames;
    std::string currentObjectName;
    
    void visitObject(const Object &o)
    {
      objects.emplace_back(o.name());
      if (o.type().present()) //pretty sure type has to be here in this 'Object' context
      {
        std::string type = o.type().get();
        if (skipTypes.count(type) != 0)
        {
          skipNames.emplace(o.name());
          return;
        }
        auto pos = type.find(':');
        if (pos != std::string::npos)
        {
          auto workBench = type.substr(0, pos);
          if (skipBenches.count(workBench) != 0)
          {
            skipNames.emplace(o.name());
            return;
          }
        }
      }
    }
    
    void visitObjectData(const Object &o)
    {
      if (skipNames.count(o.name()) != 0) skip = true;
      else skip = false;
      currentObjectName = o.name();
    }
    
    void visitObjectDataProperty(const Property &p)
    {
      if (skip) return;
      
      auto add = [&](std::string_view s)
      {
        referenced.emplace_back(s);
        if (referenced.back().empty()) referenced.pop_back(); //we have empty values in files.
      };
        
        if (p.Link().present() && p.Link()->value().present())
          add(p.Link()->value().get());
      
      if (p.LinkList().present())
      {
        for (const auto &l : p.LinkList()->Link())
        {
          if (l.value().present())
            add(l.value().get());
        }
      }
      
      if (p.LinkSub().present()) add(p.LinkSub()->value());
      
      if (p.LinkSubList().present())
      {
        for (const auto &l : p.LinkSubList()->Link())
          if (l.obj().present())
            add(l.obj().get());
      }
      
      if (p.Part().present() && !p.Part()->file().empty())
        shapeObjects.emplace_back(currentObjectName);
    }
    
    void clean()
    {
      auto uniquefy = [](std::vector<std::string> &strings)
      {
        std::sort(strings.begin(), strings.end());
        auto it = std::unique(strings.begin(), strings.end());
        strings.erase(it, strings.end());
      };
      
      uniquefy(objects);
      uniquefy(referenced);
      uniquefy(shapeObjects);
    }
    
    std::vector<std::string> leaves()
    {
      std::vector<std::string> minusSkipped;
      std::set_difference
      (
        objects.begin(), objects.end()
        , skipNames.begin(), skipNames.end()
        , std::back_inserter(minusSkipped)
      );
      
      std::vector<std::string> leavesOut;
      std::set_difference
      (
        minusSkipped.begin(), minusSkipped.end()
        , referenced.begin(), referenced.end()
        , std::back_inserter(leavesOut)
      );
      return leavesOut;
    }
    
    std::vector<std::string> shapeLeaves()
    {
      std::vector<std::string> leafs = leaves();
      std::vector<std::string> leavesOut;
      std::set_intersection
      (
        leafs.begin(), leafs.end()
        , shapeObjects.begin(), shapeObjects.end()
        , std::back_inserter(leavesOut)
      );
      return leavesOut;
    }
  };
}

using namespace cmv;

FilterModel::FilterModel(QObject *parent)
: QAbstractTableModel(parent)
{
  checkStates.resize(2, Qt::Unchecked);
  checkStates.front() = Qt::Checked;
}

int FilterModel::rowCount(const QModelIndex&) const
{
  return static_cast<int>(checkStates.size());
}

int FilterModel::columnCount(const QModelIndex&) const
{
  return 2;
}

QVariant FilterModel::data(const QModelIndex &index, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (index.column() == 0) return (QVariant());
    if (index.row() == 0) return QString::fromUtf8("BRep");
    else return QString::fromUtf8("Leaves");
  }
  if (role == Qt::CheckStateRole && index.column() == 0)
  {
    return checkStates.at(index.row());
  }
  return QVariant();
}

bool FilterModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!index.isValid() || index.column() != 0 || role != Qt::CheckStateRole) return false;
  checkStates.at(index.row()) = static_cast<Qt::CheckState>(value.toInt());
  dataChanged(index, index);
  return true;
}

Qt::ItemFlags FilterModel::flags(const QModelIndex &index) const
{
  if (!index.isValid()) return QAbstractItemModel::flags(index);
  
  if (index.column() == 0)
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable |Qt::ItemIsUserCheckable;
  return Qt::ItemIsEnabled;
}

QVariant FilterModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (orientation != Qt::Orientation::Horizontal || role != Qt::DisplayRole) return QVariant();
  if (section == 0) return QObject::tr("Apply");
  return QObject::tr("Name");
}


FilterView::FilterView(QWidget *parent)
: QTableView(parent)
{
  horizontalHeader()->setStretchLastSection(true);
  verticalHeader()->hide();
  setAlternatingRowColors(true);
}

FeatureModel::FeatureModel(const Records &recordsIn, QObject *parent)
: QAbstractTableModel(parent)
, records(recordsIn)
{}

int FeatureModel::rowCount(const QModelIndex&) const
{
  return records.size();
}

int FeatureModel::columnCount(const QModelIndex&) const
{
  return 3;
}

QVariant FeatureModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid()) return QVariant();
  
  if (role == Qt::DisplayRole)
  {
    if (index.column() == 0) return (QVariant());
    if (index.column() == 1) return QString::fromStdString(records.at(index.row()).objectName);
    return QString::fromStdString(records.at(index.row()).labelName);
  }
  if (role == Qt::CheckStateRole && index.column() == 0)
  {
    return records.at(index.row()).state;
  }
  return QVariant();
}

bool FeatureModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!index.isValid() || index.column() != 0 || role != Qt::CheckStateRole) return false;
  Qt::CheckState newState =  static_cast<Qt::CheckState>(value.toInt());
  records.at(index.row()).state = newState;
  dataChanged(index, index);
  if (newState == Qt::Checked) addObject(index);
  else if (newState == Qt::Unchecked) removeObject(index);
  return true;
}

Qt::ItemFlags FeatureModel::flags(const QModelIndex &index) const
{
  if (!index.isValid()) return QAbstractItemModel::flags(index);
  if (index.column() == 0)
    return Qt::ItemIsEnabled |Qt::ItemIsUserCheckable;
  return Qt::ItemIsEnabled;
}

QVariant FeatureModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (orientation != Qt::Orientation::Horizontal || role != Qt::DisplayRole) return QVariant();
  if (section == 0) return QObject::tr("Import");
  if (section == 1) return QObject::tr("Name");
  return QObject::tr("Label");
}

FeatureProxyModel::FeatureProxyModel(const fcx::DExplorer &eIn, QObject* parent)
: QSortFilterProxyModel(parent)
, explorer(eIn)
{
  setDynamicSortFilter(false);
}

void FeatureProxyModel::setBRep(bool stateIn)
{
  if (stateIn == filterForBRep) return;
  filterForBRep = stateIn;
  updateAccepted();
  invalidateFilter();
}

void FeatureProxyModel::setLeaf(bool stateIn)
{
  if (stateIn == filterForLeaf) return;
  filterForLeaf = stateIn;
  updateAccepted();
  invalidateFilter();
}

bool FeatureProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex&) const
{
  if (std::binary_search(acceptRows.begin(), acceptRows.end(), sourceRow))
    return true;
  return false;
}

void FeatureProxyModel::updateAccepted()
{
  acceptRows.clear();
  
  FilterVisitor visitor;
  explorer.visitObjects<decltype(visitor)>(visitor);
  explorer.visitObjectDatas<decltype(visitor)>(visitor);
  visitor.clean();
  
  std::vector<std::string> search;
  if (!filterForBRep && !filterForLeaf)
    search = visitor.objects;
  else if (filterForBRep && !filterForLeaf)
    search = visitor.shapeObjects;
  else if (!filterForBRep && filterForLeaf)
    search = visitor.leaves();
  else //filter on both.
    search = visitor.shapeLeaves();
  
  assert(sourceModel());
  const auto &sm = *sourceModel();
  for (int index = 0; index < sm.rowCount(); ++index)
  {
    std::string test = sm.data(sm.index(index, 1), Qt::DisplayRole).toString().toStdString();
    if (std::binary_search(search.begin(), search.end(), test))
      acceptRows.push_back(index);
  }
  //acceptRows should naturally be in sorted order.
}

FeatureView::FeatureView(QWidget *parent)
: QTableView(parent)
{
  horizontalHeader()->setStretchLastSection(true);
  verticalHeader()->hide();
  setAlternatingRowColors(true);
}

struct ImportFC::Stow
{
  cmd::ImportFC *command = nullptr;
  cmv::ImportFC *view = nullptr;
  fcx::DExplorer explorer;
  
  struct Record
  {
    std::string name = "";
    std::string labelName = "";
    boost::uuids::uuid id = gu::createNilId();
    Record(const std::string &nIn) : name(nIn){}
  };
  std::vector<Record> records;
  
  dlg::SplitterDecorated *splitter = nullptr;
  FilterModel *filterModel = nullptr;
  FilterView *filterView = nullptr;
  FeatureModel *featureModel = nullptr;
  FeatureProxyModel *featureProxyModel = nullptr;
  FeatureView *featureView = nullptr;
  QPushButton *checkAllButton = nullptr;
  QPushButton *uncheckAllButton = nullptr;
  
  Stow(cmd::ImportFC *cIn, cmv::ImportFC *vIn)
  : command(cIn)
  , view(vIn)
  , explorer(command->getPath())
  {
    explore();
    buildGui();
    if (!explorer.isValid()) return;
    connect();
    restore();
  }
  
  ~Stow()
  {
    if (!explorer.isValid()) return;
    QSettings &settings = app::instance()->getUserSettings();
    settings.beginGroup("ImportFC");
    settings.setValue("filterheader", filterView->horizontalHeader()->saveState());
    settings.setValue("featureheader", featureView->horizontalHeader()->saveState());
    settings.endGroup();
  }
  
  void explore()
  {
    if (!explorer.isValid()) return;
    NameVisitor visitor;
    explorer.visitObjects<decltype(visitor)>(visitor);
    for (const auto &n : visitor.names)
    {
      records.emplace_back(n);
      LabelNameVisitor labelVisitor(n);
      explorer.visitObjectDatas(labelVisitor);
      records.back().labelName = labelVisitor.labelName;
    }
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    QString fileString = QString::fromStdString(command->getOriginalName().string());
    QLabel *pathLabel = new QLabel(view);
    mainLayout->addWidget(pathLabel);
    if (!explorer.isValid())
    {
      QString failMessage = QString::fromStdString(explorer.getParseMessage());
      pathLabel->setText(fileString + " Failed With: " + failMessage);
      return;
    }
    pathLabel->setText(QObject::tr("Importing ") + fileString);
    
    splitter = new dlg::SplitterDecorated(view);
    splitter->setOrientation(Qt::Vertical);
    
    filterModel = new FilterModel(view);
    QGroupBox *filtersBox = new QGroupBox(tr("Filters"), splitter);
    QVBoxLayout *filterLayout = new QVBoxLayout(filtersBox);
    filterView = new FilterView(filtersBox);
    filterLayout->addWidget(filterView);
    splitter->addWidget(filtersBox);
    
    FeatureModel::Records tempRecords;
    for (const auto &r : records) tempRecords.emplace_back(r.name, r.labelName);
    
    featureModel = new FeatureModel(tempRecords, view);
    featureProxyModel = new FeatureProxyModel(explorer, view);
    featureProxyModel->setSourceModel(featureModel);
    featureProxyModel->setBRep(true); //after setting of source model. checked by default.
    QGroupBox *featuresBox = new QGroupBox(tr("Features"), splitter);
    QVBoxLayout *featuresLayout = new QVBoxLayout(featuresBox);
    QHBoxLayout *buttonLayout = new QHBoxLayout();
    checkAllButton = new QPushButton(tr("Check All"), featuresBox);
    uncheckAllButton = new QPushButton(tr("Uncheck All"), featuresBox);
    buttonLayout->addWidget(checkAllButton);
    buttonLayout->addWidget(uncheckAllButton);
    buttonLayout->addStretch();
    featuresLayout->addLayout(buttonLayout);
    featureView = new FeatureView(featuresBox);
    featuresLayout->addWidget(featureView);
    splitter->addWidget(featuresBox);
    
    mainLayout->addWidget(splitter);
  }
  
  void connect()
  {
    filterView->setModel(filterModel);
    featureView->setModel(featureProxyModel);
    
    QObject::connect(filterModel, &FilterModel::dataChanged, view, &ImportFC::filtersChanged);
    QObject::connect(featureModel, &FeatureModel::addObject, view, &ImportFC::addObject);
    QObject::connect(featureModel, &FeatureModel::removeObject, view, &ImportFC::removeObject);
    QObject::connect(checkAllButton, &QPushButton::clicked, view, &ImportFC::checkAll);
    QObject::connect(uncheckAllButton, &QPushButton::clicked, view, &ImportFC::uncheckAll);
  }
  
  void restore()
  {
    splitter->restoreSettings("cmv::ImportFC::splitter");
    
    QSettings &settings = app::instance()->getUserSettings();
    settings.beginGroup("ImportFC");
    filterView->horizontalHeader()->restoreState(settings.value("filterheader").toByteArray());
    featureView->horizontalHeader()->restoreState(settings.value("featureheader").toByteArray());
    settings.endGroup();
  }
};

ImportFC::ImportFC(cmd::ImportFC *cIn)
: Base("cmv::ImportFC")
, stow(std::make_unique<Stow>(cIn, this))
{
}

ImportFC::~ImportFC() = default;

void ImportFC::filtersChanged(const QModelIndex&, const QModelIndex&)
{
  const auto &checkedStates = stow->filterModel->getCheckStates();
  stow->featureProxyModel->setBRep(checkedStates.at(0) == Qt::Checked);
  stow->featureProxyModel->setLeaf(checkedStates.at(1) == Qt::Checked);
}

//not using Project::readOCC because it does special processing
//of compounds that we don't want here.
void ImportFC::addObject(const QModelIndex &index)
{
  auto &r = stow->records.at(index.row());
  assert(r.id.is_nil());
  
  //always add an id to keep in sync with feature model checkboxes.
  r.id = gu::createRandomId();
  
  PartFileVisitor visitor(r.name);
  stow->explorer.visitObjectDatas(visitor);
  
  if (visitor.partFile.empty())
  {
    node->sendBlocked(msg::buildStatusMessage("Couldn't find part file for object", 2.0));
    return;
  }
  std::filesystem::path partFilePath = stow->command->getZipDirectory() / visitor.partFile;
  if (!std::filesystem::exists(partFilePath))
  {
    node->sendBlocked(msg::buildStatusMessage("File path doesn't exist", 2.0));
    return;
  }
  if (std::filesystem::file_size(partFilePath) == 0)
  {
    node->sendBlocked(msg::buildStatusMessage("File is empty", 2.0));
    return;
  }
  
  TopoDS_Shape shapeIn;
  BRep_Builder junk;
  std::fstream file(partFilePath.string().c_str());
  BRepTools::Read(shapeIn, file, junk);
  if (shapeIn.IsNull())
  {
    node->sendBlocked(msg::buildStatusMessage("Shape is null", 2.0));
    return;
  }
  
  auto inert = std::make_unique<ftr::Inert::Feature>(shapeIn);
  r.id = inert->getId();
  auto *f = project->addFeature(std::move(inert));
  f->setName(QString::fromStdString(r.name));
  f->updateModel(project->getPayload(f->getId()));
  f->updateVisual();
  f->setModelDirty(); //otherwise we don't write feature to disk on actual update.
  node->sendBlocked(msg::Request | msg::DAG | msg::View | msg::Update);
  node->sendBlocked(msg::buildHideOverlay(r.id));
  node->sendBlocked(msg::buildStatusMessage("File Added", 2.0));
}

void ImportFC::removeObject(const QModelIndex &index)
{
  auto &r = stow->records.at(index.row());
  assert(!r.id.is_nil());
  
  //might be dummy, see addObject.
  if (project->hasFeature(r.id)) project->removeFeature(r.id);
  
  r.id = gu::createNilId();
}

void ImportFC::checkAll()
{
  auto *pm = stow->featureProxyModel;
  for (int index = 0; index < pm->rowCount(); ++index)
  {
    auto pmIndex = pm->index(index, 0);
    if (pm->data(pmIndex, Qt::CheckStateRole) != Qt::Unchecked) continue;
    auto mIndex = pm->mapToSource(pmIndex);
    stow->featureModel->setData(mIndex, Qt::Checked, Qt::CheckStateRole);
  }
}

void ImportFC::uncheckAll()
{
  auto *pm = stow->featureProxyModel;
  for (int index = 0; index < pm->rowCount(); ++index)
  {
    auto pmIndex = pm->index(index, 0);
    if (pm->data(pmIndex, Qt::CheckStateRole) != Qt::Checked) continue;
    auto mIndex = pm->mapToSource(pmIndex);
    stow->featureModel->setData(mIndex, Qt::Unchecked, Qt::CheckStateRole);
  }
}

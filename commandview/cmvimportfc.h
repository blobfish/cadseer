/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2022 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CMV_IMPORTFC_H
#define CMV_IMPORTFC_H

#include <memory>

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QTableView>

#include "commandview/cmvbase.h"

namespace cmd{class ImportFC;}
namespace fcx{class DExplorer;}

namespace cmv
{
  class FilterModel : public QAbstractTableModel
  {
    Q_OBJECT
  public:
    FilterModel(QObject*);
    const std::vector<Qt::CheckState>& getCheckStates(){return checkStates;}
  protected:
    int rowCount(const QModelIndex&) const override;
    int columnCount(const QModelIndex&) const override;
    QVariant data(const QModelIndex&, int) const override;
    bool setData(const QModelIndex&, const QVariant&, int) override;
    Qt::ItemFlags flags(const QModelIndex&) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;
  private:
    std::vector<Qt::CheckState> checkStates;
  };
  
  class FilterView : public QTableView
  {
    Q_OBJECT
  public:
    FilterView(QWidget*);
  };
  
  class FeatureModel : public QAbstractTableModel
  {
    Q_OBJECT
  public:
    struct Record
    {
      Qt::CheckState state = Qt::Unchecked;
      std::string objectName = "";
      std::string labelName = "";
      Record(const std::string &nameIn, const std::string &labelNameIn)
      : objectName(nameIn), labelName(labelNameIn){}
    };
    using Records = std::vector<Record>;
  private:
    Records records;
    
  public:
    FeatureModel(const Records&, QObject*);
    int rowCount(const QModelIndex&) const override;
    int columnCount(const QModelIndex&) const override;
    QVariant data(const QModelIndex&, int) const override;
    bool setData(const QModelIndex&, const QVariant&, int) override;
    Qt::ItemFlags flags(const QModelIndex&) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;
  Q_SIGNALS:
    void addObject(const QModelIndex&);
    void removeObject(const QModelIndex&);
  };
  
  class FeatureProxyModel : public QSortFilterProxyModel
  {
    Q_OBJECT
  public:
    explicit FeatureProxyModel(const fcx::DExplorer&, QObject *parent = 0);
  public Q_SLOTS:
    void setBRep(bool);
    void setLeaf(bool);
    
  protected:
    const fcx::DExplorer &explorer;
    bool filterForBRep = false;
    bool filterForLeaf = false;
    std::vector<int> acceptRows;
    
    bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
    void updateAccepted();
  };
  
  class FeatureView : public QTableView
  {
    Q_OBJECT
  public:
    FeatureView(QWidget*);
  };
  
  class ImportFC : public Base
  {
    Q_OBJECT
  public:
    ImportFC(cmd::ImportFC*);
    ~ImportFC() override;
  public Q_SLOTS:
    void filtersChanged(const QModelIndex&, const QModelIndex&);
    void addObject(const QModelIndex&);
    void removeObject(const QModelIndex&);
    void checkAll();
    void uncheckAll();
  private:
    struct Stow;
    std::unique_ptr<Stow> stow;
  };
}

#endif // CMV_IMPORTFC_H

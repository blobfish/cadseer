/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QVBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "feature/ftrinstance.h"
#include "command/cmdinstance.h"
#include "commandview/cmvinstance.h"

using boost::uuids::uuid;

using namespace cmv;

struct Instance::Stow
{
  cmd::Instance *command;
  cmv::Instance *view;
  prm::Parameters parameters;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  prm::Observer observer{[&](){prmView->updateHideInactive();}};
  
  Stow(cmd::Instance *cIn, cmv::Instance *vIn)
  : command(cIn)
  , view(vIn)
  {
    parameters = command->feature->getParameters();
    buildGui();
    connect(prmModel, &tbl::Model::dataChanged, view, &Instance::modelChanged);
    
    command->feature->getParameter(ftr::Instance::Tags::InstanceType)->connect(observer);
    command->feature->getParameter(ftr::Instance::Tags::Filter)->connect(observer);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, command->feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    //common
    tbl::SelectionCue cue;
    cue.accrueEnabled = false;
    cue.singleSelection = true;
    cue.mask = (slc::AllEnabled | slc::ObjectsSelectable) & ~(slc::AllPointsEnabled | slc::AllPointsSelectable);
    cue.statusPrompt = tr("Select Instance Source");
    prmModel->setCue(command->feature->getParameter(ftr::Instance::Tags::Source), cue);
    
    //linear
    cue.mask = slc::ObjectsBoth | slc::FacesEnabled | slc::EdgesEnabled;
    cue.statusPrompt = tr("Select Reference System");
    prmModel->setCue(command->feature->getParameter(ftr::Instance::Tags::LinearCSys), cue);
    
    //polar
    cue.statusPrompt = tr("Select Reference Axis");
    prmModel->setCue(command->feature->getParameter(ftr::Instance::Tags::PolarAxis), cue);
    
    //mirror
    cue.statusPrompt = tr("Select Mirror Plane");
    prmModel->setCue(command->feature->getParameter(ftr::Instance::Tags::MirrorPlane), cue);
    
    //interpolate
    cue.statusPrompt = tr("Select 1 or 2 Reference Systems");
    cue.singleSelection = false;
    prmModel->setCue(command->feature->getParameter(ftr::Instance::Tags::InterpolateCSys), cue);
    
    //spine
    cue.mask = slc::ObjectsBoth | slc::WiresEnabled | slc::EdgesEnabled;
    cue.singleSelection = true;
    cue.statusPrompt = tr("Select Spine");
    prmModel->setCue(command->feature->getParameter(ftr::Instance::Tags::SpineSpine), cue);
  }
};

Instance::Instance(cmd::Instance *cIn)
: Base("cmv::Instance")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

Instance::~Instance() = default;

bool Instance::eventFilter(QObject *watched, QEvent *event)
{
  //installed on child widgets so we can dismiss persistent editors
  if(event->type() == QEvent::FocusIn)
  {
    stow->prmView->closePersistent();
  }
  
  return QObject::eventFilter(watched, event);
}

void Instance::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  
  using namespace ftr::Instance::Tags;
  
  auto goLinear = [&]()
  {
    const auto &source = stow->prmModel->getMessages(Source);
    const auto &csys = stow->prmModel->getMessages(LinearCSys);
    stow->command->setLinear(source, csys);
  };
  auto goPolar = [&]()
  {
    const auto &source = stow->prmModel->getMessages(Source);
    const auto &axes = stow->prmModel->getMessages(PolarAxis);
    stow->command->setPolar(source, axes);
  };
  auto goMirror = [&]()
  {
    const auto &source = stow->prmModel->getMessages(Source);
    const auto &planes = stow->prmModel->getMessages(MirrorPlane);
    stow->command->setMirror(source, planes);
  };
  auto goInterpolate = [&]()
  {
    const auto &source = stow->prmModel->getMessages(Source);
    const auto &csys = stow->prmModel->getMessages(InterpolateCSys);
    stow->command->setInterpolate(source, csys);
  };
  auto goSpine = [&]()
  {
    const auto &source = stow->prmModel->getMessages(Source);
    const auto &spine = stow->prmModel->getMessages(SpineSpine);
    stow->command->setSpine(source, spine);
  };
  
  auto currentType = static_cast<ftr::Instance::IType>(stow->command->feature->getParameter(InstanceType)->getInt());
  auto dispatch = [&]()
  {
    switch (currentType)
    {
      case ftr::Instance::IType::Linear:{goLinear(); break;}
      case ftr::Instance::IType::Polar:{goPolar(); break;}
      case ftr::Instance::IType::Mirror:{goMirror(); break;}
      case ftr::Instance::IType::Interpolate:{goInterpolate(); break;}
      case ftr::Instance::IType::Spine:{goSpine(); break;}
    }
  };
  
  std::map<std::string_view, std::function<void()>> functionMap =
  {
    std::make_pair(ftr::Instance::Tags::InstanceType, dispatch)
    , std::make_pair(ftr::Instance::Tags::Source, dispatch)
    , std::make_pair(ftr::Instance::Tags::LinearCSys, goLinear)
    , std::make_pair(ftr::Instance::Tags::PolarAxis, goPolar)
    , std::make_pair(ftr::Instance::Tags::MirrorPlane, goMirror)
    , std::make_pair(ftr::Instance::Tags::InterpolateCSys, goInterpolate)
    , std::make_pair(ftr::Instance::Tags::SpineSpine, goSpine)
  };
  const auto *parameter = stow->prmModel->getParameter(index);
  auto it = functionMap.find(parameter->getTag());
  if (it != functionMap.end()) it->second();
  
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

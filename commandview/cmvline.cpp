/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QVBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "feature/ftrline.h"
#include "command/cmdline.h"
#include "commandview/cmvline.h"

using boost::uuids::uuid;

using namespace cmv;

struct Line::Stow
{
  cmd::Line *command;
  cmv::Line *view;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  prm::Observer observer{[&](){prmView->updateHideInactive();}};
  
  Stow(cmd::Line *cIn, cmv::Line *vIn)
  : command(cIn)
  , view(vIn)
  {
    buildGui();
    connect(prmModel, &tbl::Model::dataChanged, view, &Line::modelChanged);
    command->feature->getParameter(ftr::Line::Tags::lineType)->connect(observer);
    command->feature->getParameter(ftr::Line::Tags::method)->connect(observer);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, command->feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    tbl::SelectionCue aCue;
    aCue.singleSelection = true;
    aCue.mask = slc::AllEnabled | slc::EndPointsSelectable | slc::AllPointsEnabled;
    aCue.statusPrompt = tr("Select Extrema Entities");
    aCue.accrueEnabled = false;
    prmModel->setCue(command->feature->getParameter(ftr::Line::Tags::originPick), aCue);
    prmModel->setCue(command->feature->getParameter(ftr::Line::Tags::finalePick), aCue);
    
    aCue.mask = slc::ObjectsBoth | slc::FacesEnabled | slc::EdgesEnabled;
    aCue.statusPrompt = tr("Select Direction");
    prmModel->setCue(command->feature->getParameter(ftr::Line::Tags::directionPick), aCue);
  }
};

Line::Line(cmd::Line *cIn)
: Base("cmv::Line")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

Line::~Line() = default;

void Line::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  
  auto setExtrema = [&]()
  {
    const auto &ops = stow->prmModel->getMessages(ftr::Line::Tags::originPick);
    const auto &fps = stow->prmModel->getMessages(ftr::Line::Tags::finalePick);
    
    slc::Messages allPicks;
    allPicks.insert(allPicks.end(), ops.begin(), ops.end());
    allPicks.insert(allPicks.end(), fps.begin(), fps.end());
    stow->command->setToExtrema(allPicks);
  };
  
  auto setDirection = [&]()
  {
    const auto &ops = stow->prmModel->getMessages(ftr::Line::Tags::originPick);
    const auto &dps = stow->prmModel->getMessages(ftr::Line::Tags::directionPick);
    
    slc::Messages allPicks;
    allPicks.insert(allPicks.end(), ops.begin(), ops.end());
    allPicks.insert(allPicks.end(), dps.begin(), dps.end());
    stow->command->setOriginDirectionLength(allPicks);
  };
  
  auto setLineType = [&]()
  {
    auto t = stow->command->feature->getParameter(ftr::Line::Tags::lineType)->getInt();
    if (t == 0) setExtrema();
    if (t == 1) setDirection();
  };
    
  auto setMethod = [&]()
  {
    auto m = stow->command->feature->getParameter(ftr::Line::Tags::method)->getInt();
    if (m == 0) setLineType(); //method = picks.
  };
  
  std::map<std::string_view, std::function<void()>> functionMap =
  {
    {ftr::Line::Tags::lineType, setMethod}
    , {ftr::Line::Tags::method, setMethod}
    , {ftr::Line::Tags::originPick, setMethod}
    , {ftr::Line::Tags::finalePick, setExtrema}
    , {ftr::Line::Tags::directionPick, setDirection}
  };
  const auto *parameter = stow->prmModel->getParameter(index);
  auto it = functionMap.find(parameter->getTag());
  if (it != functionMap.end()) it->second();
  
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QPushButton>
#include <QListWidget>
#include <QVBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "feature/ftrbase.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "command/cmdmatch.h"
#include "commandview/cmvmatch.h"

using boost::uuids::uuid;

using namespace cmv;

struct Match::Stow
{
  std::string_view SourceTag = "SourceTag";
  std::string_view TargetsTag = "TargetsTag";
  
  cmd::Match *command;
  cmv::Match *view;
  prm::Parameter source{QObject::tr("Source"), ftr::Picks(), SourceTag};
  prm::Parameter targets{QObject::tr("Targets"), ftr::Picks(), TargetsTag};
  prm::Parameters parameters;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  QListWidget *listWidget = nullptr;
//   std::vector<prm::Observer> observers;
  
  Stow(cmd::Match *cIn, cmv::Match *vIn)
  : command(cIn)
  , view(vIn)
  {
    parameters = {&source, &targets};
    buildGui();
    connect(prmModel, &tbl::Model::dataChanged, view, &Match::modelChanged);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    prmModel = new tbl::Model(view, parameters, std::nullopt);
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    tbl::SelectionCue cue;
    cue.singleSelection = true;
    cue.mask = slc::ObjectsBoth;
    cue.statusPrompt = tr("Select Source Feature");
    cue.accrueEnabled = false;
    prmModel->setCue(&source, cue);
    
    cue.singleSelection = false;
    cue.statusPrompt = tr("Select Target Features");
    prmModel->setCue(&targets, cue);
    
    listWidget = new QListWidget(view);
    mainLayout->addWidget(listWidget);
    
    QHBoxLayout *mLayout = new QHBoxLayout();
    mLayout->addStretch();
    QPushButton *mButton = new QPushButton(tr("Match"), view);
    mLayout->addWidget(mButton);
    mLayout->addStretch();
    mainLayout->addLayout(mLayout);
    QObject::connect(mButton, &QPushButton::clicked, [this](){this->goMatch();});
  }
  
  void reconcileList()
  {
    listWidget->clear();
    
    const auto &sourceMsgs = prmModel->getMessages(SourceTag);
    const auto &targetMsgs = prmModel->getMessages(TargetsTag);
    if (sourceMsgs.empty() || targetMsgs.empty()) return;
    command->setSource(sourceMsgs.front().featureId);
    const auto *sFeature = app::instance()->getProject()->findFeature(sourceMsgs.front().featureId); assert(sFeature);
    for (const auto &tm : targetMsgs) command->addTarget(tm.featureId);
    auto tags = command->reconcileParameterTags();
    QFlags itemFlags = Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
    for (const auto &t : tags)
    {
      auto *item = new QListWidgetItem(sFeature->getParameter(t)->getName(), listWidget);
      item->setFlags(itemFlags);
      item->setCheckState(Qt::Checked);
      item->setData(Qt::UserRole, t.data());
    }
  }
  
  void goMatch()
  {
    std::vector<std::string> tagStrings;
    for (int row = 0; row < listWidget->count(); ++row)
    {
      auto *item = listWidget->item(row);
      if (item->checkState() != Qt::Checked) continue;
      tagStrings.emplace_back(item->data(Qt::UserRole).toString().toStdString());
    }
    command->goMatch(tagStrings);
    msg::Message mOut(msg::Mask(msg::Request | msg::Command | msg::Done));
    app::instance()->queuedMessage(mOut);
  }
};

Match::Match(cmd::Match *cIn)
: Base("cmv::Match")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

Match::~Match() = default;

void Match::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  auto changedTag = stow->prmModel->getParameter(index)->getTag();
  
  const auto *project = app::instance()->getProject();
  auto buildPicks = [&](std::string_view tag) -> ftr::Picks
  {
    ftr::Picks picksToSet;
    const auto &selections = stow->prmModel->getMessages(tag);
    for (const auto &s : selections)
    {
      auto *f = project->findFeature(s.featureId);
      assert(f); if (!f) continue;
      picksToSet.push_back(tls::convertToPick(s, *f, project->getShapeHistory()));
    }
    return picksToSet;
  };
  
  if (changedTag == stow->SourceTag) stow->source.setValue(buildPicks(changedTag));
  if (changedTag == stow->TargetsTag) stow->targets.setValue(buildPicks(changedTag));
  stow->reconcileList();
  
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>
#include <optional>

#include <gp_Lin.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <TopoDS.hxx>
#include <TopExp.hxx>
#include <BRep_Tool.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <Extrema_ECC.hxx>
#include <Extrema_POnCurv.hxx>

#include <QVBoxLayout>
#include <QPlainTextEdit>
#include <QTextStream>

#include <osg/Matrixd>
#include <osgText/Text>
#include <osg/AutoTransform>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "feature/ftrbase.h"
#include "annex/annseershape.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "viewer/vwrmessage.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "globalutilities.h"
#include "tools/tlsstring.h"
#include "tools/occtools.h"
#include "tools/tlsangle.h"
#include "library/lbrplabel.h"
#include "library/lbrangulardimension.h"
#include "command/cmdmeasureangular.h"
#include "commandview/cmvmeasureangular.h"

namespace
{
  struct Axis
  {
    osg::Vec3d head;
    osg::Vec3d tail;
    
    Axis() = delete;
    Axis(const osg::Vec3d &hIn, const osg::Vec3d &tIn) : head(hIn), tail(tIn)
    {
      if (!isValid()) return;

    }
    
    bool isValid() const
    {
      return (head - tail).length() > std::numeric_limits<float>::epsilon();
    }
    
    osg::Vec3d getDirection() const
    {
      assert(isValid());
      osg::Vec3d out = head - tail;
      out.normalize();
      return out;
    }
    
    bool updateToBox(const TopoDS_Shape &shape)
    {
      if (!isValid()) return false;
      
      gp_Lin line = gp_Lin(gu::toOcc(tail).XYZ(), gu::toOcc(head - tail));
      BRepBuilderAPI_MakeEdge em(line); if (!em.IsDone()) return false;
      TopoDS_Edge edge = em;
      
      occt::BoundingBox bb(shape);
      auto theBox = bb.buildPrimitiveBox(); if (!theBox) return false;
      
      BRepAlgoAPI_Common intersect(edge, *theBox); if (!intersect.IsDone()) return false;
      auto results = intersect.Modified(edge); if (results.IsEmpty()) return false;
      assert(results.First().ShapeType() == TopAbs_EDGE);
      auto resultEdge = TopoDS::Edge(results.First());
      tail = gu::toOsg(BRep_Tool::Pnt(TopExp::FirstVertex(resultEdge)));
      head = gu::toOsg(BRep_Tool::Pnt(TopExp::LastVertex(resultEdge)));
      return true;
    }
  };
}

using namespace cmv;

struct MeasureAngular::Stow
{
  cmd::MeasureAngular *command;
  cmv::MeasureAngular *view;
  slc::Messages messages;
  QPlainTextEdit *textEdit;
  
  Stow(cmd::MeasureAngular *cIn, cmv::MeasureAngular *vIn)
  : command(cIn)
  , view(vIn)
  {
    buildGui();
    setupDispatcher();
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    textEdit = new QPlainTextEdit(view);
    textEdit->setReadOnly(true);
    textEdit->setWordWrapMode(QTextOption::NoWrap);
    textEdit->setPlainText(tr("Select 2 Objects to Measure"));
    mainLayout->addWidget(textEdit);
  }
  
  void slcAdded(const msg::Message &mIn)
  {
    if (view->isHidden()) return;
    slc::add(messages, mIn.getSLC());
    if (messages.size() >= 2) go();
  }
  
  void slcRemoved(const msg::Message &mIn)
  {
    if (view->isHidden()) return;
    slc::remove(messages, mIn.getSLC());
  }
  
  void setupDispatcher()
  {
    view->sift->insert
    (
      {
        std::make_pair
        (
          msg::Response | msg::Post | msg::Selection | msg::Add
          , std::bind(&Stow::slcAdded, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Response | msg::Pre | msg::Selection | msg::Remove
          , std::bind(&Stow::slcRemoved, this, std::placeholders::_1)
        )
      }
    );
  }
  
  void glean()
  {
    auto getPoint = [&](const slc::Message &mIn) -> std::optional<osg::Vec3d>
    {
      if (mIn.featureType == ftr::Type::DatumPoint)
      {
        const auto *feat = app::instance()->getProject()->findFeature(mIn.featureId);
        auto prms = feat->getParameters(prm::Tags::Origin);
        assert(!prms.empty()); if (prms.empty()) return std::nullopt;
        return prms.front()->getVector();
      }
      if (slc::isPointType(mIn.type)) return mIn.pointLocation;
      return std::nullopt;
    };
    auto getVector = [&](const slc::Message &mIn) -> std::optional<Axis>
    {
      const auto *feat = app::instance()->getProject()->findFeature(mIn.featureId); assert(feat);
      std::optional<TopoDS_Shape> selectedShape;
      if (feat->hasAnnex(ann::Type::SeerShape) && !feat->getAnnex<ann::SeerShape>().isNull())
      {
        const auto &ss = feat->getAnnex<ann::SeerShape>();
        if (!mIn.shapeId.is_nil()) selectedShape = ss.getOCCTShape(mIn.shapeId);
        else selectedShape = ss.getRootOCCTShape();
      }
        
      std::optional<Axis> outAxis;
      auto sizeToBox =[&]()
      {
        if (!outAxis || !selectedShape) return;
        outAxis->updateToBox(*selectedShape);
      };
      
      if (slc::isObjectType(mIn.type))
      {
        auto prms = feat->getParameters(prm::Tags::CSys);
        if (!prms.empty())
        {
          const auto &sys = prms.front()->getMatrix();
          osg::Vec3d origin = sys.getTrans();
          outAxis = Axis(origin + gu::getZVector(sys), origin);
          sizeToBox();
          return *outAxis;
        }
        if (mIn.featureType == ftr::Type::DatumAxis)
        {
          auto prms = feat->getParameters(prm::Tags::Origin);
          assert(!prms.empty()); if (prms.empty()) return std::nullopt;
          auto origin = prms.front()->getVector();
          
          prms = feat->getParameters(prm::Tags::Direction);
          assert(!prms.empty()); if (prms.empty()) return std::nullopt;
          auto direction = prms.front()->getVector();
          
          prms = feat->getParameters(prm::Tags::Size);
          assert(!prms.empty()); if (prms.empty()) return std::nullopt;
          auto size = prms.front()->getDouble();
          
          outAxis = Axis(origin + direction * size / 2.0, origin + direction * size / -2.0);
          if (!outAxis->isValid()) return std::nullopt;
          sizeToBox();
          return *outAxis;
        }
      }
      else if (slc::isShapeType(mIn.type) && selectedShape)
      {
        auto gleaned = occt::gleanAxis(*selectedShape);
        if (!gleaned.second) return std::nullopt;
        auto occtAxis = gleaned.first;
        
        //allow edge picks to influence direction
        if (mIn.type == slc::Type::Edge)
        {
          auto startPoint = gu::toOsg(BRep_Tool::Pnt(TopExp::FirstVertex(TopoDS::Edge(*selectedShape))));
          auto finishPoint = gu::toOsg(BRep_Tool::Pnt(TopExp::LastVertex(TopoDS::Edge(*selectedShape))));
          double startDist = (mIn.pointLocation - startPoint).length();
          double finishDist = (mIn.pointLocation - finishPoint).length();
          if (startDist < finishDist) occtAxis.Reverse();
        }
        
        outAxis = Axis(gu::toOsg(occtAxis.Location()) + gu::toOsg(occtAxis.Direction()), gu::toOsg(occtAxis.Location()));
        if (!outAxis->isValid()) return std::nullopt;
        sizeToBox();
        return *outAxis;
      }
      return std::nullopt;
    };
    
    if (messages.size() < 2) return;
    tls::Angle angleHelper;
    std::optional<osg::Vec3d> tempPoint;
    auto assignAxisPoints = [&](const osg::Vec3d &p1)
    {
      if (tempPoint)
      {
        angleHelper.addSegment(*tempPoint, p1);
        tempPoint = std::nullopt;
      }
      else tempPoint = p1;
    };
    for (const auto &m : messages)
    {
      auto point = getPoint(m);
      if (point)
      {
        if (tempPoint) assignAxisPoints(*point);
        else tempPoint = point;
      }
      else
      {
        tempPoint = std::nullopt;
        auto vector = getVector(m);
        if (vector) angleHelper.addSegment(vector->head, vector->tail);
      }
    }
    if (angleHelper.getState() != tls::Angle::Ready) return;
    view->node->send(msg::Message(msg::Request | msg::Selection | msg::Clear));
    angleHelper.process();
    if (angleHelper.getState() < tls::Angle::Ang)
    {
      view->node->sendBlocked(msg::buildStatusMessage("Angle Dimension Failed", 2.0));
      return;
    }
    
    QString infoMessage;
    QTextStream stream(&infoMessage);
    stream << "Measure angular:"
    << Qt::endl << "  " << QString::fromStdString(tls::prettyDouble(osg::RadiansToDegrees(angleHelper.getAngle()))) << " degrees";
    textEdit->setPlainText(infoMessage);
    
    if (angleHelper.getState() < tls::Angle::TextPosition)
    {
      view->node->sendBlocked(msg::buildStatusMessage("Angle Dimension Failed", 2.0));
      return;
    }
    if ((angleHelper.getMask() & tls::ang::Parallel).any())
    {
      view->node->sendBlocked(msg::buildStatusMessage("No Angle Dimension For Parallel", 2.0));
      return;
    }
    
    vwr::Message vwrMessage;
    vwrMessage.node = angleHelper.buildAngularDimension();
    msg::Message message(msg::Request | msg::Add | msg::Overlay, vwrMessage);
    view->node->sendBlocked(message);
    
    view->node->sendBlocked(msg::buildStatusMessage("Angle Dimension Added", 2.0));
  }
  
  void go()
  {
    glean();
  }
};

MeasureAngular::MeasureAngular(cmd::MeasureAngular *cIn)
: Base("cmv::MeasureAngular")
, stow(std::make_unique<Stow>(cIn, this))
{
  maskDefault = slc::AllEnabled | slc::AllPointsEnabled | slc::EdgesSelectable | slc::FacesSelectable;
  goSelectionToolbar();
  goMaskDefault();
}

MeasureAngular::~MeasureAngular() = default;

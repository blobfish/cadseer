/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QAction>
#include <QMenu>
#include <QContextMenuEvent>
#include <QMessageBox>
#include <QClipboard>
#include <QTextStream>
#include <QPushButton>

#include "application/appapplication.h"
#include "application/appmessage.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "dialogs/dlgsplitterdecorated.h"
#include "commandview/cmvtable.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "expressions/exprmanager.h"
#include "expressions/exprtablemodel.h"
#include "tools/featuretools.h"
#include "feature/ftrprimitive.h"
#include "feature/ftrparametriccurve.h"
#include "command/cmdparametriccurve.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvparametriccurve.h"

using boost::uuids::uuid;
using namespace cmv;

PCEProxyModel::PCEProxyModel(expr::Manager &emIn, QObject *parent)
: QSortFilterProxyModel(parent), eMan(emIn)
{
  setDynamicSortFilter(false);
}

QModelIndex PCEProxyModel::addCommon(std::function<void()> f)
{
  expr::TableModel *tableModel = getTableModel();
  int rowCount = tableModel->rowCount();
  f();
  QModelIndex sourceIndex = tableModel->index(rowCount, 0);
  this->invalidateFilter();
  QModelIndex out = this->mapFromSource(sourceIndex); assert(out.isValid());
  return out;
}

QModelIndex PCEProxyModel::addScalar()
{
  expr::TableModel *tableModel = getTableModel();
  return addCommon(std::bind(&expr::TableModel::addScalar, tableModel));
}

QModelIndex PCEProxyModel::addVector()
{
  expr::TableModel *tableModel = getTableModel();
  return addCommon(std::bind(&expr::TableModel::addVector, tableModel));
}

QModelIndex PCEProxyModel::addRotation()
{
  expr::TableModel *tableModel = getTableModel();
  return addCommon(std::bind(&expr::TableModel::addRotation, tableModel));
}

QModelIndex PCEProxyModel::addCSys()
{
  expr::TableModel *tableModel = getTableModel();
  return addCommon(std::bind(&expr::TableModel::addCSys, tableModel));
}

bool PCEProxyModel::isCritical(const QModelIndex &proxyIndex) const
{
  if (!proxyIndex.isValid()) return false;
  const auto *tableModel = getTableModel();
  auto sourceIndex = mapToSource(proxyIndex);
  int formulaId = tableModel->data(sourceIndex, Qt::UserRole).toInt();
  if (formulaId < 5) return true;
  return false;
}

expr::TableModel* PCEProxyModel::getTableModel() const
{
  expr::TableModel *myModel = dynamic_cast<expr::TableModel*>(this->sourceModel());
  assert(myModel);
  return myModel;
}

bool PCEProxyModel::filterAcceptsRow(int row, const QModelIndex&) const
{
  //show everything but 't'
  assert(row >= 0 && row < sourceModel()->rowCount());
  int formulaId = sourceModel()->data(sourceModel()->index(row, 0), Qt::UserRole).toInt();
  if (formulaId == 1) return false;
  return true;
}

Qt::ItemFlags PCEProxyModel::flags(const QModelIndex& index) const
{
  if (!index.isValid()) return Qt::NoItemFlags;
  
  static const auto ReadOnly = Qt::ItemFlags(Qt::NoItemFlags);
  static const auto Modify = Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
  
  // I guess I am suppose to just know whether the index is for the filtered model or the source?
  auto sourceIndex = this->mapToSource(index);
  int formulaId = sourceModel()->data(sourceIndex, Qt::UserRole).toInt();
  if (sourceIndex.column() == 0 && formulaId < 5) return ReadOnly;
  return Modify;
}

PCEView::PCEView(QWidget *parentIn): TableViewBase(parentIn)
{
  QObject::connect(addScalarAction, &QAction::triggered, std::bind(&PCEView::addScalarSlot, this));
  QObject::connect(addVectorAction, &QAction::triggered, std::bind(&PCEView::addVectorSlot, this));
  QObject::connect(addRotationAction, &QAction::triggered, std::bind(&PCEView::addRotationSlot, this));
  QObject::connect(addCSysAction, &QAction::triggered, std::bind(&PCEView::addCSysSlot, this));
  QObject::connect(removeFormulaAction, &QAction::triggered, std::bind(&PCEView::removeFormulaSlot, this));
  QObject::connect(copyFormulaValueAction, &QAction::triggered, std::bind(&PCEView::copyFormulaValueSlot, this));
  QObject::connect(exportFormulaAction, &QAction::triggered, std::bind(&PCEView::exportFormulaSlot, this));
  QObject::connect(importFormulaAction, &QAction::triggered, std::bind(&PCEView::importFormulaSlot, this));
}
void PCEView::contextMenuEvent(QContextMenuEvent *event)
{
  QMenu menu;
  menu.addAction(addScalarAction);
  menu.addAction(addVectorAction);
  menu.addAction(addRotationAction);
  menu.addAction(addCSysAction);
  menu.addAction(removeFormulaAction);
  menu.addSeparator();
  menu.addAction(exportFormulaAction);
  menu.addAction(importFormulaAction);
  menu.addSeparator();
  menu.addAction(copyFormulaValueAction);
  
  bool canRemove = true;
  auto *pm = dynamic_cast<PCEProxyModel*>(model()); assert(pm);
  for (const auto &i : this->selectionModel()->selectedIndexes())
  {
    if (pm->isCritical(i))
    {
      canRemove = false;
      break;
    }
  }
  removeFormulaAction->setEnabled(canRemove);
  
  int selectionCount = this->selectionModel()->selectedIndexes().size();
  exportFormulaAction->setEnabled(selectionCount > 0);
  copyFormulaValueAction->setEnabled(selectionCount == 1);
  
  menu.exec(event->globalPos());
}

void PCEView::addScalarSlot()
{
  QModelIndex proxyIndex = static_cast<PCEProxyModel*>(this->model())->addScalar();
  triggerEdit(proxyIndex);
}

void PCEView::addVectorSlot()
{
  QModelIndex proxyIndex = static_cast<PCEProxyModel*>(this->model())->addVector();
  triggerEdit(proxyIndex);
}

void PCEView::addRotationSlot()
{
  QModelIndex proxyIndex = static_cast<PCEProxyModel*>(this->model())->addRotation();
  triggerEdit(proxyIndex);
}

void PCEView::addCSysSlot()
{
  QModelIndex proxyIndex = static_cast<PCEProxyModel*>(this->model())->addCSys();
  triggerEdit(proxyIndex);
}

void PCEView::removeFormulaSlot()
{
  QSortFilterProxyModel *pModel = dynamic_cast<PCEProxyModel*>(this->model()); assert(pModel);
  expr::TableModel *myModel = dynamic_cast<expr::TableModel*>(pModel->sourceModel()); assert(myModel);
  QModelIndexList indexes = this->selectedIndexes();
  QModelIndexList sourceIndexes;
  for (const auto &i : indexes) sourceIndexes.append(pModel->mapToSource(i));
  myModel->removeExpression(sourceIndexes);
}

void PCEView::exportFormulaSlot()
{
  std::filesystem::path filePath = getExportPath();
  if (filePath.empty()) return;
  QSortFilterProxyModel *pModel = dynamic_cast<QSortFilterProxyModel *>(this->model()); assert(pModel);
  expr::TableModel *myModel = dynamic_cast<expr::TableModel*>(pModel->sourceModel()); assert(myModel);
  QModelIndexList indexes = this->selectedIndexes();
  QModelIndexList sourceIndexes;
  for (const auto &i : indexes) sourceIndexes.append(pModel->mapToSource(i));
  std::ofstream fileStream;
  fileStream.open(filePath.c_str());
  if (!fileStream.is_open())
  {
    QMessageBox::critical(this, tr("Error:"), tr("Couldn't Open File"));
    return;
  }
  myModel->exportExpressions(sourceIndexes, fileStream);
  fileStream.close();
}

void PCEView::importFormulaSlot()
{
  std::filesystem::path filePath = getImportPath();
  if (filePath.empty()) return;
  std::ifstream fileStream;
  fileStream.open(filePath.c_str());
  if (!fileStream.is_open()) return;
  QSortFilterProxyModel *pModel = dynamic_cast<QSortFilterProxyModel*>(this->model()); assert(pModel);
  expr::TableModel *myModel = dynamic_cast<expr::TableModel*>(pModel->sourceModel()); assert(myModel);
  auto results = myModel->importExpressions(fileStream);
}

void PCEView::copyFormulaValueSlot()
{
  QSortFilterProxyModel *pModel = dynamic_cast<QSortFilterProxyModel *>(this->model()); assert(pModel);
  expr::TableModel *myModel = dynamic_cast<expr::TableModel*>(pModel->sourceModel()); assert(myModel);
  QModelIndexList indexes = this->selectedIndexes();
  if (indexes.size() != 1) return;
  QModelIndex sourceIndex = myModel->index(pModel->mapToSource(indexes.front()).row(), 2);
  QApplication::clipboard()->setText(myModel->data(sourceIndex).toString());
}

struct ParametricCurve::Stow
{
  cmd::ParametricCurve *command;
  cmv::ParametricCurve *view;
  prm::Parameters parameters;
  dlg::SplitterDecorated *mainSplitter = nullptr;
  QPushButton *infoButton = nullptr;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  PCEProxyModel *pceProxyModel = nullptr;
  PCEView *pceView = nullptr;
  
  Stow(cmd::ParametricCurve *cIn, cmv::ParametricCurve *vIn)
  : command(cIn)
  , view(vIn)
  {
    parameters = command->feature->getParameters();
    buildGui();
    glue();
    mainSplitter->restoreSettings("cmv::ParametricCurve::mainSplitter");
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    mainSplitter = new dlg::SplitterDecorated(view);
    mainSplitter->setOrientation(Qt::Vertical);
    mainSplitter->setChildrenCollapsible(false);
    mainLayout->addWidget(mainSplitter);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, command->feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    mainSplitter->addWidget(prmView);
    
    pceView = new PCEView(view);
    pceView->setDragEnabled(true);
    auto *mainTable = new expr::TableModel(command->feature->getManager(), pceView);
    pceProxyModel = new PCEProxyModel(command->feature->getManager(), pceView);
    pceProxyModel->setSourceModel(mainTable);
    pceView->setModel(pceProxyModel);
    mainSplitter->addWidget(pceView);

    infoButton = new QPushButton(tr("Curve Info"), view);
    infoButton->setCheckable(true);
    mainLayout->addWidget(infoButton);
    
    tbl::SelectionCue cue;
    cue.singleSelection = true;
    cue.mask = slc::ObjectsBoth;
    cue.statusPrompt = tr("Select Feature To Link CSys");
    cue.accrueEnabled = false;
    prmModel->setCue(command->feature->getParameter(prm::Tags::CSysLinked), cue);
  }
  
  void glue()
  {
    connect(prmModel, &tbl::Model::dataChanged, view, &ParametricCurve::modelChanged);
    connect(pceProxyModel, &tbl::Model::dataChanged, view, &ParametricCurve::modelChanged);
    connect(pceProxyModel, &tbl::Model::modelReset, view, &ParametricCurve::modelReset);
    connect(infoButton, &QPushButton::toggled, view, &ParametricCurve::infoButtonToggled);
    pceView->installEventFilter(view);
  }

  void showInfo()
  {
    QString infoMessage;
    QTextStream stream(&infoMessage);
    const auto &f = *command->feature;
    if (f.isFailure())
    {
      view->node->send(msg::buildStatusMessage("Feature Is Not Up To Date.", 2.0));
      return;
    }
    const auto &ss = f.getAnnex<ann::SeerShape>(ann::Type::SeerShape);
    if (ss.isNull())
    {
      view->node->send(msg::buildStatusMessage("SeerShape Is Null", 2.0));
      return;
    }
    auto children = ss.useGetChildrenOfType(ss.getRootOCCTShape(), TopAbs_EDGE);
    if (children.empty() || children.front().ShapeType() != TopAbs_EDGE)
    {
      view->node->send(msg::buildStatusMessage("No Edge In Shape", 2.0));
      return;
    }
    f.getShapeInfo(stream, ss.findId(children.front()));

    app::Message appMessage;
    appMessage.infoMessage = infoMessage;
    msg::Message viewInfoMessage(msg::Request | msg::Info | msg::Text, appMessage);
    view->node->send(viewInfoMessage);
  }
};

ParametricCurve::ParametricCurve(cmd::ParametricCurve *cIn)
: Base("cmv::ParametricCurve")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

ParametricCurve::~ParametricCurve() = default;

bool ParametricCurve::eventFilter(QObject *watched, QEvent *event)
{
  //installed on child widgets so we can dismiss persistent editors
  if(event->type() == QEvent::FocusIn) stow->prmView->closePersistent();
  return QObject::eventFilter(watched, event);
}

void ParametricCurve::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  auto *param = stow->prmModel->getParameter(index);
  
  if (param->getTag() == prm::Tags::CSysType)
  {
    if (static_cast<ftr::Primitive::CSysType>(param->getInt()) == ftr::Primitive::Linked)
      param = stow->command->feature->getParameter(prm::Tags::CSysLinked); //trick next if.
    else
      stow->command->setToConstant();
    stow->prmView->updateHideInactive();
  }
  if (param->getTag() == prm::Tags::CSysLinked) stow->command->setToLinked(stow->prmModel->getMessages(param));
  
  stow->command->localUpdate();
  if (stow->infoButton->isChecked()) stow->showInfo();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

void ParametricCurve::modelReset()
{
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

void ParametricCurve::infoButtonToggled(bool freshState)
{
  if (freshState) stow->showInfo();
  //Not going to shut down the info window when toggled off.
  //Messaging doesn't support it and not sure the user will want that.
}

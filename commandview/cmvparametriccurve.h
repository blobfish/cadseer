/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CMV_PARAMETRICCURVE_H
#define CMV_PARAMETRICCURVE_H

#include <memory>

#include <QSortFilterProxyModel>

#include "commandview/cmvbase.h"
#include "expressions/exprtableview.h"

namespace cmd{class ParametricCurve;}
namespace expr{class Manager; class TableModel;}

namespace cmv
{
  class PCEProxyModel : public QSortFilterProxyModel
  {
    Q_OBJECT
  public:
    explicit PCEProxyModel(expr::Manager &eManagerIn, QObject *parent = 0);
    QModelIndex addScalar();
    QModelIndex addVector();
    QModelIndex addRotation();
    QModelIndex addCSys();
    bool isCritical(const QModelIndex&) const;
    expr::TableModel* getTableModel() const;
  protected:
    bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    int columnCount(const QModelIndex& = QModelIndex()) const override {return 2;}
  private:
    expr::Manager &eMan;
    int continuity = 0;
    QModelIndex addCommon(std::function<void()>);
  };
  
  class PCEView : public expr::TableViewBase
  {
    Q_OBJECT
  public:
    explicit PCEView(QWidget *parentIn = 0);
  protected:
    void contextMenuEvent(QContextMenuEvent*) override;
  public Q_SLOTS:
    void addScalarSlot();
    void addVectorSlot();
    void addRotationSlot();
    void addCSysSlot();
    void removeFormulaSlot();
    void copyFormulaValueSlot();
    void exportFormulaSlot();
    void importFormulaSlot();
  };
  
  class ParametricCurve : public Base
  {
    Q_OBJECT
  public:
    ParametricCurve(cmd::ParametricCurve*);
    ~ParametricCurve() override;
  protected:
    bool eventFilter(QObject*, QEvent*) override;
  private Q_SLOTS:
    void modelChanged(const QModelIndex&, const QModelIndex&);
    void modelReset();
    void infoButtonToggled(bool);
  private:
    struct Stow;
    std::unique_ptr<Stow> stow;
  };
}

#endif // CMV_PARAMETRICCURVE_H

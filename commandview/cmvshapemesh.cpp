/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QPushButton>
#include <QVBoxLayout>

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "feature/ftrshapemesh.h"
#include "command/cmdshapemesh.h"
#include "commandview/cmvshapemesh.h"

using boost::uuids::uuid;

using namespace cmv;

struct ShapeMesh::Stow
{
  cmd::ShapeMesh *command;
  cmv::ShapeMesh *view;
  prm::Parameters parameters;
  cmv::tbl::Model *prmModel = nullptr;
  cmv::tbl::View *prmView = nullptr;
  QPushButton *resetButton = nullptr;
  
  Stow(cmd::ShapeMesh *cIn, cmv::ShapeMesh *vIn)
  : command(cIn)
  , view(vIn)
  {
    parameters = command->feature->getParameters();
    buildGui();
    connect(prmModel, &tbl::Model::dataChanged, view, &ShapeMesh::modelChanged);
    connect(resetButton, &QPushButton::clicked, view, &ShapeMesh::goReset);
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    resetButton = new QPushButton(tr("Reset"), view);
    mainLayout->addWidget(resetButton);
    
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(view, command->feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(view, prmModel, true);
    mainLayout->addWidget(prmView);
    
    tbl::SelectionCue cue;
    cue.singleSelection = false;
    cue.mask = ~slc::AllPointsEnabled | ~slc::AllSelectable;
    cue.statusPrompt = tr("Select Entities To Mesh");
    cue.accrueEnabled = false;
    prmModel->setCue(command->feature->getParameter(prm::Tags::Picks), cue);
  }
};

ShapeMesh::ShapeMesh(cmd::ShapeMesh *cIn)
: Base("cmv::ShapeMesh")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

ShapeMesh::~ShapeMesh() = default;

void ShapeMesh::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid()) return;
  auto changedTag = stow->prmModel->getParameter(index)->getTag();
  if (changedTag == prm::Tags::Picks)
  {
    const auto &picks = stow->prmModel->getMessages(stow->command->feature->getParameter(prm::Tags::Picks));
    stow->command->setSelections(picks);
  }
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

void ShapeMesh::goReset()
{
  stow->command->feature->resetParameters();
  stow->command->localUpdate();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
  goMaskDefault();
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2020 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <optional>
#include <bitset>

#include <osgViewer/Viewer>

#include <QSettings>
#include <QRadioButton>
#include <QButtonGroup>
#include <QGroupBox>
#include <QToolBar>
#include <QTabWidget>
#include <QTimer>
#include <QLineEdit>
#include <QVBoxLayout>

#include "application/appapplication.h"
#include "application/appmainwindow.h"
#include "project/prjproject.h"
#include "expressions/exprmanager.h"
#include "viewer/vwrwidget.h"
#include "viewer/vwrmessage.h"
#include "message/msgmessage.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "commandview/cmvexpressionedit.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "feature/ftrprimitive.h"
#include "feature/ftrsketch.h"
#include "sketch/sktvisual.h"
#include "sketch/sktselection.h"
#include "tools/featuretools.h"
#include "command/cmdsketch.h"
#include "commandview/cmvselectioncue.h"
#include "commandview/cmvtable.h"
#include "commandview/cmvsketch.h"

using boost::uuids::uuid;

namespace
{
  //helps track the 2 different selections
  struct SlcState
  {
  private:
    using State = std::bitset<2>;
    State globalSelection = 1;
    State sketchSelection = 2;
    State currentState = globalSelection; //state when view is created.
  public:
    bool isGlobalOn(){return (currentState & globalSelection).any();}
    bool isGlobalOff(){return !isGlobalOn();}
    void setGlobalOn(){currentState |= globalSelection;}
    void setGlobalOff(){currentState &= ~globalSelection;}
    
    bool isSketchOn(){return (currentState & sketchSelection).any();}
    bool isSketchOff(){return !isSketchOn();}
    void setSketchOn(){currentState |= sketchSelection;}
    void setSketchOff(){currentState &= ~sketchSelection;}
  };
}

using namespace cmv;

struct Sketch::Stow
{
  cmv::Sketch *view = nullptr;
  cmd::Sketch *command = nullptr;
  ftr::Sketch::Feature *feature = nullptr;
  skt::Visual *visual = nullptr;
  osg::ref_ptr<skt::Selection> selection;
  SlcState slcState;
  bool isAutoCoincident = true;
  bool isAutoTangent = true;
  
  tbl::Model *prmModel = nullptr;
  tbl::View *prmView  = nullptr;
  QTabWidget *tabWidget = nullptr;
  
  QButtonGroup *geoGroup = nullptr;
  QAction *pointAction = nullptr;
  QAction *lineAction = nullptr;
  QAction *arcAction = nullptr;
  QAction *circleAction = nullptr;
  QAction *bezierAction = nullptr;
  QAction *coincidentAction = nullptr;
  QAction *horizontalAction = nullptr;
  QAction *verticalAction = nullptr;
  QAction *tangentAction = nullptr;
  QAction *symmetryAction = nullptr;
  QAction *parallelAction = nullptr;
  QAction *perpendicularAction = nullptr;
  QAction *equalAction = nullptr;
  QAction *equalAngleAction = nullptr;
  QAction *midPointAction = nullptr;
  QAction *whereDraggedAction = nullptr;
  QAction *distanceAction = nullptr;
  QAction *diameterAction = nullptr;
  QAction *angleAction = nullptr;
  QAction *commandCancelAction = nullptr;
  QAction *toggleConstructionAction = nullptr;
  QAction *removeAction = nullptr;
  QAction *removeLastAction = nullptr;
  QAction *resetViewAction = nullptr;
  QAction *chainAction = nullptr;
  QAction *autoCoincidentAction = nullptr;
  QAction *autoTangentAction = nullptr;
  QAction *dummyAction = nullptr; //see uncheck function.
  
  Stow(cmd::Sketch *cIn, cmv::Sketch *vIn)
  : view(vIn)
  , command(cIn)
  , feature(command->feature)
  , visual(feature->getVisual())
  , selection(new skt::Selection(visual))
  {
    buildGui();
    
    QSettings &settings = app::instance()->getUserSettings();
    settings.beginGroup("cmv::Sketch");
    //load settings
    settings.endGroup();
    
    view->sift->insert
    ({
      std::make_pair
      (
        msg::Response | msg::Sketch | msg::Selection | msg::Add
        , std::bind(&Stow::selectionAddedDispatched, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Response | msg::Sketch | msg::Shape | msg::Done
        , std::bind(&Stow::unCheck, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Response | msg::Sketch | msg::Selection | msg::Clear
        , std::bind(&Stow::selectionCleared, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Edit | msg::Parameter
        , std::bind(&Stow::parameterEdit, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Toggle | msg::Auto | msg::Coincident
        , std::bind(&Stow::toggleAutoCoincident, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Toggle | msg::Auto | msg::Tangent
        , std::bind(&Stow::toggleAutoTangent, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::View | msg::Top
        , std::bind(&Stow::resetView, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Command | msg::Done
        , std::bind(&Stow::cancel, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Remove
        , std::bind(&Stow::remove, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Point
        , std::bind(&Stow::point, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Line
        , std::bind(&Stow::line, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Arc
        , std::bind(&Stow::arc, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Circle
        , std::bind(&Stow::circle, this, std::placeholders::_1)
      )
      , std::make_pair
      (
        msg::Request | msg::Sketch | msg::Bezier
        , std::bind(&Stow::bezier, this, std::placeholders::_1)
      )
    });
  }
  
  void buildGui()
  {
    QSizePolicy adjust = view->sizePolicy();
    adjust.setVerticalPolicy(QSizePolicy::Expanding);
    view->setSizePolicy(adjust);
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    clearContentMargins(view);
    
    tabWidget = new QTabWidget(view);
    mainLayout->addWidget(tabWidget);
    
    QWidget *constraintPage = new QWidget(tabWidget);
    QHBoxLayout *constraintLayout = new QHBoxLayout();
    constraintPage->setLayout(constraintLayout);
    clearContentMargins(constraintPage);
    tabWidget->addTab(constraintPage, QIcon(":resources/images/sketchCoincident.svg"), QString());
    tabWidget->setTabToolTip(0, tr("Constraints"));
    
    constraintLayout->addStretch();
    QToolBar *viewSelectToolbar = new QToolBar(view); //view tool bar
    viewSelectToolbar->setOrientation(Qt::Vertical);
    QAction *viewEntitiesAction = new QAction(QIcon(":resources/images/sketchViewEntities.svg"), tr("View Entities"), view);
    viewEntitiesAction->setCheckable(true);
    viewEntitiesAction->setChecked(true);
    connect(viewEntitiesAction, &QAction::toggled, view, &Sketch::viewEntitiesToggled);
    viewSelectToolbar->addAction(viewEntitiesAction);
    QAction *viewConstraintsAction = new QAction(QIcon(":resources/images/sketchViewConstraints.svg"), tr("View Constraints"), view);
    viewConstraintsAction->setCheckable(true);
    viewConstraintsAction->setChecked(true);
    connect(viewConstraintsAction, &QAction::toggled, view, &Sketch::viewConstraintsToggled);
    viewSelectToolbar->addAction(viewConstraintsAction);
    QAction *viewDimensionsAction = new QAction(QIcon(":resources/images/sketchViewDimensions.svg"), tr("View Dimensions"), view);
    viewDimensionsAction->setCheckable(true);
    viewDimensionsAction->setChecked(true);
    connect(viewDimensionsAction, &QAction::toggled, view, &Sketch::viewDimensionsToggled);
    viewSelectToolbar->addAction(viewDimensionsAction);
    QAction *viewWorkAction = new QAction(QIcon(":resources/images/sketchViewWork.svg"), tr("View Work"), view);
    viewWorkAction->setCheckable(true);
    viewWorkAction->setChecked(true);
    connect(viewWorkAction, &QAction::toggled, view, &Sketch::viewWorkToggled);
    viewSelectToolbar->addAction(viewWorkAction);
    viewSelectToolbar->addSeparator();
    QAction *selectEntitiesAction = new QAction(QIcon(":resources/images/sketchSelectEntities.svg"), tr("Select Entities"), view);
    selectEntitiesAction->setCheckable(true);
    selectEntitiesAction->setChecked(true);
    connect(selectEntitiesAction, &QAction::toggled, view, &Sketch::selectEntitiesToggled);
    viewSelectToolbar->addAction(selectEntitiesAction);
    QAction *selectConstraintsAction = new QAction(QIcon(":resources/images/sketchSelectConstraints.svg"), tr("Select Constraints"), view);
    selectConstraintsAction->setCheckable(true);
    selectConstraintsAction->setChecked(true);
    connect(selectConstraintsAction, &QAction::toggled, view, &Sketch::selectConstraintsToggled);
    viewSelectToolbar->addAction(selectConstraintsAction);
    QAction *selectWorkAction = new QAction(QIcon(":resources/images/sketchSelectWork.svg"), tr("Select Work"), view);
    selectWorkAction->setCheckable(true);
    selectWorkAction->setChecked(true);
    connect(selectWorkAction, &QAction::toggled, view, &Sketch::selectWorkToggled);
    viewSelectToolbar->addAction(selectWorkAction);
    viewSelectToolbar->addSeparator();
    commandCancelAction = new QAction(QIcon(":resources/images/editCommandCancel.svg"), tr("Cancel Command"), view);
    commandCancelAction->setDisabled(true);
    connect(commandCancelAction, &QAction::triggered, view, &Sketch::cancel);
    viewSelectToolbar->addAction(commandCancelAction);
    toggleConstructionAction = new QAction(QIcon(":resources/images/base.svg"), tr("Toggle Construction"), view);
    toggleConstructionAction->setDisabled(true);
    connect(toggleConstructionAction, &QAction::triggered, view, &Sketch::toggleConstruction);
    viewSelectToolbar->addAction(toggleConstructionAction);
    removeAction = new QAction(QIcon(":resources/images/editRemove.svg"), tr("Remove"), view);
    removeAction->setDisabled(true);
    connect(removeAction, &QAction::triggered, view, &Sketch::remove);
    viewSelectToolbar->addAction(removeAction);
    constraintLayout->addWidget(viewSelectToolbar);
    removeLastAction = new QAction(QIcon(":resources/images/sketchRemoveFailure.svg"), tr("Remove Last Constraint"), view);
    removeLastAction->setEnabled(true);
    connect(removeLastAction, &QAction::triggered, view, &Sketch::removeLast);
    viewSelectToolbar->addAction(removeLastAction);
    constraintLayout->addWidget(viewSelectToolbar);
    resetViewAction = new QAction(QIcon(":resources/images/sketchResetView.svg"), tr("Reset View"), view);
    connect(resetViewAction, &QAction::triggered, view, &Sketch::resetView);
    viewSelectToolbar->addAction(resetViewAction);
    constraintLayout->addWidget(viewSelectToolbar);
    
    QToolBar *constraintToolbar = new QToolBar(view); //constraint tool bar
    constraintToolbar->setOrientation(Qt::Vertical);
    coincidentAction = new QAction(QIcon(":resources/images/sketchCoincident.svg"), tr("Coincident"), view);
    coincidentAction->setDisabled(true);
    connect(coincidentAction, &QAction::triggered, view, &Sketch::addCoincident);
    constraintToolbar->addAction(coincidentAction);
    tangentAction = new QAction(QIcon(":resources/images/sketchTangent.svg"), tr("Tangent"), view);
    tangentAction->setDisabled(true);
    connect(tangentAction, &QAction::triggered, view, &Sketch::addTangent);
    constraintToolbar->addAction(tangentAction);
    horizontalAction = new QAction(QIcon(":resources/images/sketchHorizontal.svg"), tr("Horizontal"), view);
    horizontalAction->setDisabled(true);
    connect(horizontalAction, &QAction::triggered, view, &Sketch::addHorizontal);
    constraintToolbar->addAction(horizontalAction);
    verticalAction = new QAction(QIcon(":resources/images/sketchVertical.svg"), tr("Vertical"), view);
    verticalAction->setDisabled(true);
    connect(verticalAction, &QAction::triggered, view, &Sketch::addVertical);
    constraintToolbar->addAction(verticalAction);
    equalAction = new QAction(QIcon(":resources/images/sketchEqual.svg"), tr("Equal"), view);
    equalAction->setDisabled(true);
    connect(equalAction, &QAction::triggered, view, &Sketch::addEqual);
    constraintToolbar->addAction(equalAction);
    parallelAction = new QAction(QIcon(":resources/images/sketchParallel.svg"), tr("Parallel"), view);
    parallelAction->setDisabled(true);
    connect(parallelAction, &QAction::triggered, view, &Sketch::addParallel);
    constraintToolbar->addAction(parallelAction);
    perpendicularAction = new QAction(QIcon(":resources/images/sketchPerpendicular.svg"), tr("Perpendicular"), view);
    perpendicularAction->setDisabled(true);
    connect(perpendicularAction, &QAction::triggered, view, &Sketch::addPerpendicular);
    constraintToolbar->addAction(perpendicularAction);
    symmetryAction = new QAction(QIcon(":resources/images/sketchSymmetry.svg"), tr("Symmetry"), view);
    symmetryAction->setDisabled(true);
    connect(symmetryAction, &QAction::triggered, view, &Sketch::addSymmetric);
    constraintToolbar->addAction(symmetryAction);
    equalAngleAction = new QAction(QIcon(":resources/images/sketchEqualAngle.svg"), tr("Equal Angle"), view);
    equalAngleAction->setDisabled(true);
    connect(equalAngleAction, &QAction::triggered, view, &Sketch::addEqualAngle);
    constraintToolbar->addAction(equalAngleAction);
    midPointAction = new QAction(QIcon(":resources/images/sketchMidPoint.svg"), tr("Mid Point"), view);
    midPointAction->setDisabled(true);
    connect(midPointAction, &QAction::triggered, view, &Sketch::addMidpoint);
    constraintToolbar->addAction(midPointAction);
    whereDraggedAction = new QAction(QIcon(":resources/images/sketchDragged.svg"), tr("Dragged"), view);
    whereDraggedAction->setDisabled(true);
    connect(whereDraggedAction, &QAction::triggered, view, &Sketch::addWhereDragged);
    constraintToolbar->addAction(whereDraggedAction);
    constraintLayout->addWidget(constraintToolbar);
    
    QToolBar *entityToolbar = new QToolBar(view); //entity tool bar
    entityToolbar->setOrientation(Qt::Vertical);
    geoGroup = new QButtonGroup(view); //defaults to exclusive.
    
    distanceAction = new QAction(QIcon(":resources/images/sketchDistance.svg"), tr("Distance"), view);
    distanceAction->setDisabled(true);
    connect(distanceAction, &QAction::triggered, view, &Sketch::addDistance);
    entityToolbar->addAction(distanceAction);
    diameterAction = new QAction(QIcon(":resources/images/sketchDiameter.svg"), tr("Diameter"), view);
    diameterAction->setDisabled(true);
    connect(diameterAction, &QAction::triggered, view, &Sketch::addDiameter);
    entityToolbar->addAction(diameterAction);
    angleAction = new QAction(QIcon(":resources/images/sketchAngle.svg"), tr("Angle"), view);
    angleAction->setDisabled(true);
    connect(angleAction, &QAction::triggered, view, &Sketch::addAngle);
    entityToolbar->addAction(angleAction);
    constraintLayout->addWidget(entityToolbar);
    entityToolbar->addSeparator();
    chainAction = new QAction(QIcon(":resources/images/sketchChain.svg"), tr("Chain"), view);
    chainAction->setCheckable(true);
    connect(chainAction, &QAction::toggled, view, &Sketch::chainToggled);
    entityToolbar->addAction(chainAction);
    autoCoincidentAction = new QAction(QIcon(":resources/images/sketchAutoCoincident.svg"), tr("Auto Coincident"), view);
    autoCoincidentAction->setCheckable(true);
    autoCoincidentAction->setChecked(true);
    connect(autoCoincidentAction, &QAction::toggled, view, &Sketch::autoCoincidentToggled);
    entityToolbar->addAction(autoCoincidentAction);
    autoTangentAction = new QAction(QIcon(":resources/images/sketchAutoTangent.svg"), tr("Auto Tangent"), view);
    autoTangentAction->setCheckable(true);
    autoTangentAction->setChecked(true);
    connect(autoTangentAction, &QAction::toggled, view, &Sketch::autoTangentToggled);
    entityToolbar->addAction(autoTangentAction);
    entityToolbar->addSeparator();
    pointAction = new QAction(QIcon(":resources/images/sketchPoint.svg"), tr("Point"), view);
    pointAction->setCheckable(true);
    connect(pointAction, &QAction::triggered, view, &Sketch::addPoint);
    entityToolbar->addAction(pointAction);
    geoGroup->addButton(static_cast<QAbstractButton*>(entityToolbar->widgetForAction(pointAction)));
    lineAction = new QAction(QIcon(":resources/images/sketchLine.svg"), tr("Line"), view);
    lineAction->setCheckable(true);
    connect(lineAction, &QAction::triggered, view, &Sketch::addLine);
    entityToolbar->addAction(lineAction);
    geoGroup->addButton(static_cast<QAbstractButton*>(entityToolbar->widgetForAction(lineAction)));
    arcAction = new QAction(QIcon(":resources/images/sketchArc.svg"), tr("Arc"), view);
    arcAction->setCheckable(true);
    connect(arcAction, &QAction::triggered, view, &Sketch::addArc);
    entityToolbar->addAction(arcAction);
    geoGroup->addButton(static_cast<QAbstractButton*>(entityToolbar->widgetForAction(arcAction)));
    circleAction = new QAction(QIcon(":resources/images/sketchCircle.svg"), tr("Circle"), view);
    circleAction->setCheckable(true);
    connect(circleAction, &QAction::triggered, view, &Sketch::addCircle);
    entityToolbar->addAction(circleAction);
    geoGroup->addButton(static_cast<QAbstractButton*>(entityToolbar->widgetForAction(circleAction)));
    bezierAction = new QAction(QIcon(":resources/images/sketchBezeir.svg"), tr("Bezeir"), view);
    bezierAction->setCheckable(true);
    connect(bezierAction, &QAction::triggered, view, &Sketch::addBezier);
    entityToolbar->addAction(bezierAction);
    geoGroup->addButton(static_cast<QAbstractButton*>(entityToolbar->widgetForAction(bezierAction)));
    dummyAction = new QAction(QIcon(":resources/images/start.svg"), tr("Dummy"), view); //see uncheck
    dummyAction->setCheckable(true);
    dummyAction->setVisible(false);
    entityToolbar->addAction(dummyAction);
    geoGroup->addButton(static_cast<QAbstractButton*>(entityToolbar->widgetForAction(dummyAction)));

    QWidget *parameterPage = new QWidget(tabWidget);
    QHBoxLayout *parameterLayout = new QHBoxLayout();
    parameterPage->setLayout(parameterLayout);
    clearContentMargins(parameterPage);
    tabWidget->addTab(parameterPage, QIcon(":resources/images/sketchDistance.svg"), QString());
    tabWidget->setTabToolTip(1, tr("Parameters"));
    
    //limit the use of the new parameter table to the csys parameters
    //and leave dimensional parameters as is using parameter edit.
    // prm::Parameters params
    // {
    //   feature->getParameter(prm::Tags::CSysType)
    //   , feature->getParameter(prm::Tags::CSys)
    //   , feature->getParameter(prm::Tags::CSysLinked)
    // };
    // prmModel = new tbl::Model(parameterPage, feature, params);
    ftr::UpdatePayload up = view->project->getPayload(command->feature->getId());
    std::optional<tls::Resolver> resolver(up);
    prmModel = new tbl::Model(parameterPage, feature->getParameters(), std::move(resolver));
    prmView = new tbl::View(parameterPage, prmModel, true);
    tbl::SelectionCue cue;
    cue.singleSelection = true;
    cue.mask = slc::ObjectsBoth;
    cue.statusPrompt = tr("Select Feature To Link CSys");
    cue.accrueEnabled = false;
    prmModel->setCue(feature->getParameter(prm::Tags::CSysLinked), cue);
    parameterLayout->addWidget(prmView);
    connect(prmModel, &tbl::Model::dataChanged, view, &Sketch::modelChanged);
    connect(prmView, &tbl::View::openingPersistent, view, &Sketch::sketchSelectionStop);
    connect(prmView, &tbl::View::closingPersistent, view, &Sketch::sketchSelectionGo);
    connect(tabWidget, &QTabWidget::currentChanged, [this](){visual->clearSelection(); evaluateActiveControls();});
  }
  
  void sketchSelectionGo()
  {
    if (slcState.isSketchOn()) return;
    slcState.setSketchOn();
    osgViewer::View* view = app::instance()->getMainWindow()->getViewer()->getOsgViewer();
    assert(view);
    view->addEventHandler(selection.get());
    visual->setActiveSketch();
    feature->getOverlaySwitch()->setNodeMask(feature->getOverlaySwitch()->getNodeMask() | skt::ActiveSketch.to_ulong());
  }

  void sketchSelectionStop()
  {
    if (slcState.isSketchOff()) return;
    slcState.setSketchOff();
    osgViewer::View* view = app::instance()->getMainWindow()->getViewer()->getOsgViewer();
    assert(view);
    view->removeEventHandler(selection.get());
    visual->clearActiveSketch();
    feature->getOverlaySwitch()->setNodeMask(feature->getOverlaySwitch()->getNodeMask() & (~skt::ActiveSketch).to_ulong());
  }

  void selectionGo()
  {
    if (slcState.isGlobalOn()) return;
    slcState.setGlobalOn();
    view->node->sendBlocked(msg::buildSelectionMask(slc::AllEnabled));
  }

  void selectionStop()
  {
    if (slcState.isGlobalOff()) return;
    slcState.setGlobalOff();
    view->node->sendBlocked(msg::buildSelectionMask(~slc::All));
  }
  
  void selectionAddedDispatched(const msg::Message&)
  {
    evaluateActiveControls();
  }
  
  //https://forum.qt.io/topic/6419/how-to-uncheck-button-in-qbuttongroup/8
  //I tried turning off exclusive and unchecking and turning exclusive back on.
  //  it worked, but had an annoying side effect of user having to hit a button twice that had been unchecked.
  //  now trying the hidden button method. That works without side effect
  void unCheck(const msg::Message&)
  {
    dummyAction->setChecked(true);
    evaluateActiveControls();
  }
  
  void selectionCleared(const msg::Message&)
  {
    evaluateActiveControls();
  }
  
  void parameterEdit(const msg::Message &mIn)
  {
    const auto &pid = mIn.getSLC().shapeId; //a lie
    QModelIndex index = prmModel->getIndex(pid);
    if (!index.isValid()) return; //TODO reset selection state.
    if (tabWidget->currentIndex() != 1)
    {
      //TODO cache the fact we switched the tab widget, so we can switch it back later ???
      tabWidget->setCurrentIndex(1);
    }
    prmView->setCurrentIndex(index);
    prmView->edit(index);
  }
  
  void toggleAutoCoincident(const msg::Message&)
  {
    autoCoincidentAction->toggle();
  }
  
  void toggleAutoTangent(const msg::Message&)
  {
    autoTangentAction->toggle();
  }
  
  void resetView(const msg::Message&)
  {
    view->resetView();
  }
  
  void cancel(const msg::Message&)
  {
    view->cancel();
  }
  
  void remove(const msg::Message&)
  {
    view->remove();
  }
  
  void point(const msg::Message&)
  {
    if (!pointAction->isChecked()) pointAction->trigger();
  }
  
  void line(const msg::Message&)
  {
    if (!lineAction->isChecked()) lineAction->trigger();
  }
  
  void arc(const msg::Message&)
  {
    if (!arcAction->isChecked()) arcAction->trigger();
  }
  
  void circle(const msg::Message&)
  {
    if (!circleAction->isChecked()) circleAction->trigger();
  }
  
  void bezier(const msg::Message&)
  {
    if (!bezierAction->isChecked()) bezierAction->trigger();
  }
  
  void evaluateActiveControls()
  {
    switch (visual->getState())
    {
      case skt::State::selection:
      {
        (visual->canCoincident()) ? coincidentAction->setEnabled(true) : coincidentAction->setEnabled(false);
        (visual->canHorizontal()) ? horizontalAction->setEnabled(true) : horizontalAction->setEnabled(false);
        (visual->canVertical()) ? verticalAction->setEnabled(true) : verticalAction->setEnabled(false);
        (visual->canTangent()) ? tangentAction->setEnabled(true) : tangentAction->setEnabled(false);
        (visual->canSymmetry()) ? symmetryAction->setEnabled(true) : symmetryAction->setEnabled(false);
        (visual->canParallel()) ? parallelAction->setEnabled(true) : parallelAction->setEnabled(false);
        (visual->canPerpendicular()) ? perpendicularAction->setEnabled(true) : perpendicularAction->setEnabled(false);
        (visual->canEqual()) ? equalAction->setEnabled(true) : equalAction->setEnabled(false);
        (visual->canEqualAngle()) ? equalAngleAction->setEnabled(true) : equalAngleAction->setEnabled(false);
        (visual->canMidPoint()) ? midPointAction->setEnabled(true) : midPointAction->setEnabled(false);
        (visual->canWhereDragged()) ? whereDraggedAction->setEnabled(true) : whereDraggedAction->setEnabled(false);
        (visual->canDistance()) ? distanceAction->setEnabled(true) : distanceAction->setEnabled(false);
        (visual->canDiameter()) ? diameterAction->setEnabled(true) : diameterAction->setEnabled(false);
        (visual->canAngle()) ? angleAction->setEnabled(true) : angleAction->setEnabled(false);
        commandCancelAction->setEnabled(false);
        (visual->canToggleConstruction()) ? toggleConstructionAction->setEnabled(true) : toggleConstructionAction->setEnabled(false);
        (visual->canRemove()) ? removeAction->setEnabled(true) : removeAction->setEnabled(false);
        (visual->canRemoveLast()) ? removeLastAction->setEnabled(true) : removeLastAction->setEnabled(false);
        chainAction->setEnabled(true);
        autoCoincidentAction->setEnabled(true);
        autoTangentAction->setEnabled(true);
        pointAction->setEnabled(true);
        lineAction->setEnabled(true);
        arcAction->setEnabled(true);
        circleAction->setEnabled(true);
        bezierAction->setEnabled(true);
        break;
      }
      case skt::State::drag:
      {
        coincidentAction->setEnabled(false);
        horizontalAction->setEnabled(false);
        verticalAction->setEnabled(false);
        tangentAction->setEnabled(false);
        symmetryAction->setEnabled(false);
        parallelAction->setEnabled(false);
        perpendicularAction->setEnabled(false);
        equalAction->setEnabled(false);
        equalAngleAction->setEnabled(false);
        midPointAction->setEnabled(false);
        whereDraggedAction->setEnabled(false);
        distanceAction->setEnabled(false);
        diameterAction->setEnabled(false);
        angleAction->setEnabled(false);
        commandCancelAction->setEnabled(false);
        toggleConstructionAction->setEnabled(false);
        removeAction->setEnabled(false);
        removeLastAction->setEnabled(false);
        chainAction->setEnabled(false);
        autoCoincidentAction->setEnabled(false);
        autoTangentAction->setEnabled(false);
        pointAction->setEnabled(false);
        lineAction->setEnabled(false);
        arcAction->setEnabled(false);
        circleAction->setEnabled(false);
        bezierAction->setEnabled(false);
        break;
      }
      case skt::State::dynamic:
      {
        coincidentAction->setEnabled(false);
        horizontalAction->setEnabled(false);
        verticalAction->setEnabled(false);
        tangentAction->setEnabled(false);
        symmetryAction->setEnabled(false);
        parallelAction->setEnabled(false);
        perpendicularAction->setEnabled(false);
        equalAction->setEnabled(false);
        equalAngleAction->setEnabled(false);
        midPointAction->setEnabled(false);
        whereDraggedAction->setEnabled(false);
        distanceAction->setEnabled(false);
        diameterAction->setEnabled(false);
        angleAction->setEnabled(false);
        commandCancelAction->setEnabled(true);
        toggleConstructionAction->setEnabled(false);
        removeAction->setEnabled(false);
        removeLastAction->setEnabled(false);
        chainAction->setEnabled(true);
        autoCoincidentAction->setEnabled(true);
        autoTangentAction->setEnabled(true);
        pointAction->setEnabled(true);
        lineAction->setEnabled(true);
        arcAction->setEnabled(true);
        circleAction->setEnabled(true);
        bezierAction->setEnabled(true);
        break;
      }
      case skt::State::error:
      {
        coincidentAction->setEnabled(false);
        horizontalAction->setEnabled(false);
        verticalAction->setEnabled(false);
        tangentAction->setEnabled(false);
        symmetryAction->setEnabled(false);
        parallelAction->setEnabled(false);
        perpendicularAction->setEnabled(false);
        equalAction->setEnabled(false);
        equalAngleAction->setEnabled(false);
        midPointAction->setEnabled(false);
        whereDraggedAction->setEnabled(false);
        distanceAction->setEnabled(false);
        diameterAction->setEnabled(false);
        angleAction->setEnabled(false);
        commandCancelAction->setEnabled(false);
        toggleConstructionAction->setEnabled(false);
        (visual->canRemove()) ? removeAction->setEnabled(true) : removeAction->setEnabled(false);
        (visual->canRemoveLast()) ? removeLastAction->setEnabled(true) : removeLastAction->setEnabled(false);
        chainAction->setEnabled(false);
        autoCoincidentAction->setEnabled(false);
        autoTangentAction->setEnabled(false);
        pointAction->setEnabled(false);
        lineAction->setEnabled(false);
        arcAction->setEnabled(false);
        circleAction->setEnabled(false);
        bezierAction->setEnabled(false);
        break;
      }
    }
  }
  
  //bookkeeping for dimensional constraints tied to parameters.
  void removeSync()
  {
    auto &em = app::instance()->getProject()->getManager();
    auto params = feature->getObsoleteParameters(); //feature detects parameters no longer tied to constraints
    for (auto *param : params)
    {
      if (!param->isConstant())
      {
        assert(em.isLinked(param->getId()));
        em.removeLink(param->getId());
      }
      prmModel->removeParameter(param);
    }
    feature->removeObsoleteParameters();
  }
};

Sketch::Sketch(cmd::Sketch *cIn)
: Base("cmv::Sketch")
, stow(new Stow(cIn, this))
{
  stow->selection->setMask(skt::WorkPlaneOrigin | skt::WorkPlaneAxis | skt::Entity | skt::Constraint | skt::SelectionPlane);
}

Sketch::~Sketch()
{
  stow->visual->showAll();
}

void Sketch::activate()
{
  node->sendBlocked(msg::buildHideThreeD(stow->feature->getId()));
  node->sendBlocked(msg::buildShowOverlay(stow->feature->getId()));
  stow->feature->draggerHide();
  sketchSelectionGo();
  node->sendBlocked(msg::Message(msg::Request | msg::Overlay | msg::Selection | msg::Freeze));
}

void Sketch::deactivate()
{
  stow->visual->cancel();
  stow->visual->clearSelection();
  sketchSelectionStop();
  stow->feature->draggerShow();
  
  node->sendBlocked(msg::Message(msg::Request | msg::Overlay | msg::Selection | msg::Thaw));
}

void Sketch::sketchSelectionStop()
{
  stow->sketchSelectionStop();
  stow->selectionGo();
}

void Sketch::sketchSelectionGo()
{
  stow->selectionStop();
  stow->sketchSelectionGo();
  node->sendBlocked(msg::buildStatusMessage("Select Command Or Entities"));
}

void Sketch::chainToggled(bool state)
{
  stow->visual->cancel();
  stow->unCheck(msg::Message());
  if (state)
    stow->visual->setChainOn();
  else
    stow->visual->setChainOff();
}

void Sketch::autoCoincidentToggled(bool state)
{
  stow->visual->setAutoCoincident(state);
  if (!state) stow->autoTangentAction->setChecked(false); //tangent needs coincident
}

void Sketch::autoTangentToggled(bool state)
{
  stow->visual->setAutoTangent(state);
  if (state) stow->autoCoincidentAction->setChecked(true);
}

void Sketch::addPoint(bool state)
{
  if (state)
  {
    if (!stow->visual->isChainOn() && stow->visual->getState() != skt::State::selection) stow->visual->cancel();
    stow->visual->addPoint();
    stow->evaluateActiveControls();
  }
}

void Sketch::addLine(bool state)
{
  if (state)
  {
    if (!stow->visual->isChainOn() && stow->visual->getState() != skt::State::selection) stow->visual->cancel();
    stow->visual->addLine();
    stow->evaluateActiveControls();
  }
}

void Sketch::addArc(bool state)
{
  if (state)
  {
    if (!stow->visual->isChainOn() && stow->visual->getState() != skt::State::selection) stow->visual->cancel();
    stow->visual->addArc();
    stow->evaluateActiveControls();
  }
}

void Sketch::addCircle(bool state)
{
  if (state)
  {
    if (!stow->visual->isChainOn() && stow->visual->getState() != skt::State::selection) stow->visual->cancel();
    stow->visual->addCircle();
    stow->evaluateActiveControls();
  }
}

void Sketch::addBezier(bool state)
{
  if (state)
  {
    if (!stow->visual->isChainOn() && stow->visual->getState() != skt::State::selection) stow->visual->cancel();
    stow->visual->addCubicBezier();
    stow->evaluateActiveControls();
  }
}

void Sketch::remove()
{
  //evaluateActiveControls should prevent being here in correct state. Fix that, not this.
  assert(stow->visual->getState() == skt::State::selection || stow->visual->getState() == skt::State::error);
  stow->visual->remove(); //removes constraints, even ones with feature parameters.
  stow->removeSync();
  stow->evaluateActiveControls();
}

void Sketch::removeLast()
{
  assert(stow->visual->getState() == skt::State::selection || stow->visual->getState() == skt::State::error);
  stow->visual->removeLast();
  stow->removeSync();
  stow->evaluateActiveControls();
}

void Sketch::cancel()
{
  stow->visual->cancel();
  stow->unCheck(msg::Message());
  stow->evaluateActiveControls();
}

void Sketch::toggleConstruction()
{
  stow->visual->toggleConstruction();
}

void Sketch::resetView()
{
  vwr::Message vm(stow->feature->getId());
  node->send(msg::Message(msg::Request | msg::View | msg::Feature, vm));
}

void Sketch::addCoincident()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addCoincident();
  stow->evaluateActiveControls();
}

void Sketch::addHorizontal()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addHorizontal();
}

void Sketch::addVertical()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addVertical();
}

void Sketch::addTangent()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addTangent();
}

void Sketch::addDistance()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  
  auto p = stow->visual->addDistance();
  if (p)
  {
    stow->feature->addHPPair(p->first, p->second);
    stow->prmModel->addParameter(p->second.get());
  }
}

void Sketch::addDiameter()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  
  auto p = stow->visual->addDiameter();
  if (p)
  {
    stow->feature->addHPPair(p->first, p->second);
    stow->prmModel->addParameter(p->second.get());
  }
}

void Sketch::addAngle()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  
  auto p = stow->visual->addAngle();
  if (p)
  {
    stow->feature->addHPPair(p->first, p->second);
    stow->prmModel->addParameter(p->second.get());
  }
}

void Sketch::addSymmetric()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addSymmetric();
}

void Sketch::addParallel()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addParallel();
}

void Sketch::addPerpendicular()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addPerpendicular();
}

void Sketch::addEqual()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addEqual();
}

void Sketch::addEqualAngle()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addEqualAngle();
}

void Sketch::addMidpoint()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addMidpoint();
}

void Sketch::addWhereDragged()
{
  if (stow->visual->getState() != skt::State::selection)
    stow->visual->cancel();
  stow->visual->addWhereDragged();
}

void Sketch::viewEntitiesToggled(bool state)
{
  if (state)
    stow->visual->showEntity();
  else
    stow->visual->hideEntity();
}

void Sketch::viewConstraintsToggled(bool state)
{
  if (state)
    stow->visual->showConstraint();
  else
    stow->visual->hideConstraint();
}

void Sketch::viewDimensionsToggled(bool state)
{
  if (state) stow->visual->showDimensions();
  else stow->visual->hideDimensions();
}

void Sketch::viewWorkToggled(bool state)
{
  if (state)
    stow->visual->showPlane();
  else
    stow->visual->hidePlane();
}

void Sketch::selectEntitiesToggled(bool state)
{
  if (state)
    stow->selection->entitiesOn();
  else
    stow->selection->entitiesOff();
}

void Sketch::selectConstraintsToggled(bool state)
{
  if (state)
    stow->selection->constraintsOn();
  else
    stow->selection->constraintsOff();
}

void Sketch::selectWorkToggled(bool state)
{
  if (state)
  {
    stow->selection->workPlaneAxesOn();
    stow->selection->workPlaneOriginOn();
  }
  else
  {
    stow->selection->workPlaneAxesOff();
    stow->selection->workPlaneOriginOff();
  }
}
void Sketch::modelChanged(const QModelIndex &index, const QModelIndex&)
{
  if (!index.isValid())
    return;
  
  prm::Parameter *par = stow->prmModel->getParameter(index);
  
  if (par->getTag() == prm::Tags::CSysType)
  {
    const auto *ft = stow->command->feature;
    auto systemType = static_cast<ftr::Primitive::CSysType>(par->getInt());
    switch(systemType)
    {
      case ftr::Primitive::Constant:{
        stow->command->setConstant();
        break;}
      case ftr::Primitive::Linked:{
        par = ft->getParameter(prm::Tags::CSysLinked); //just trick lower to call.
        break;}
    }
    stow->prmView->updateHideInactive();
  }
  
  if (par->getTag() == prm::Tags::CSysLinked)
    stow->command->setLinked(stow->prmModel->getMessages(par));
  
  stow->command->localUpdate();
  stow->visual->clearSelection();
  node->sendBlocked(msg::buildStatusMessage(stow->command->getStatusMessage()));
}

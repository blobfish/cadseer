/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QSettings>
#include <QPushButton>
#include <QVBoxLayout>

#include "project/prjproject.h"
#include "command/cmdtest.h"
#include "commandview/cmvtest.h"

using boost::uuids::uuid;

using namespace cmv;

struct Test::Stow
{
  cmd::Test *command;
  cmv::Test *view;
  
  QPushButton *bopAlgoButton = nullptr;
  QPushButton *pCurveButton = nullptr;
  QPushButton *indexButton = nullptr;
  
  Stow(cmd::Test *cIn, cmv::Test *vIn)
  : command(cIn)
  , view(vIn)
  {
    buildGui();
  }
  
  void buildGui()
  {
    QVBoxLayout *mainLayout = new QVBoxLayout();
    view->setLayout(mainLayout);
    Base::clearContentMargins(view);
    view->setSizePolicy(view->sizePolicy().horizontalPolicy(), QSizePolicy::Expanding);
    
    bopAlgoButton = new QPushButton(tr("BOPAlgo"), view);
    pCurveButton = new QPushButton(tr("PCurve"), view);
    indexButton = new QPushButton(tr("Index"), view);
    
    mainLayout->addWidget(bopAlgoButton);
    mainLayout->addWidget(pCurveButton);
    mainLayout->addWidget(indexButton);
    mainLayout->addStretch();
    
    QObject::connect(bopAlgoButton, &QPushButton::clicked, [&](){command->bopalgoTestDispatched();});
    QObject::connect(pCurveButton, &QPushButton::clicked, [&](){command->visualizePCurves();});
    QObject::connect(indexButton, &QPushButton::clicked, [&](){command->goIndex();});
  }
};

Test::Test(cmd::Test *cIn)
: Base("cmv::Test")
, stow(std::make_unique<Stow>(cIn, this))
{
  goSelectionToolbar();
  goMaskDefault();
}

Test::~Test() = default;

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DBS_APPLICATION_H
#define DBS_APPLICATION_H

#include <filesystem>

#include <sdbus-c++/sdbus-c++.h>

#include "application/appapplication.h"
#include "application/appmessage.h"
#include "preferences/prfmanager.h"
#include "project/prjproject.h"
#include "project/prjmessage.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "dbus/generated/applicationadaptor.h"
#include "dbus/dbsqueue.h"

namespace dbs
{
  class Application final : public sdbus::AdaptorInterfaces<org::blobfish::cadseer::application_adaptor>
  {
  public:
    Application(sdbus::IConnection& connection, sdbus::ObjectPath objectPath, dbs::Queue &qIn)
    : AdaptorInterfaces(connection, std::move(objectPath))
    , queue(qIn)
    {
      registerAdaptor();
      
      node.connect(msg::hub());
      node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
      sift.name = "dbs::Application";
      sift.insert
      (
        {
          std::make_pair
          (
            msg::Response | msg::Pre | msg::Application | msg::Close, std::bind(&Application::goApplicationClosing, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Request | msg::DBus | msg::Recent | msg::Project | msg::List, std::bind(&Application::goListRecentProjects, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Response | msg::Post | msg::Open | msg::Project, std::bind(&Application::goProjectOpened, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Response | msg::Post | msg::Close | msg::Project, std::bind(&Application::goProjectClosed, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Response | msg::Post | msg::New | msg::Project, std::bind(&Application::goProjectCreated, this, std::placeholders::_1)
          )
        }
      );
    }
    
    ~Application()
    {
      unregisterAdaptor();
    }
    
  protected:
    dbs::Queue &queue;
    msg::Node node;
    msg::Sift sift;
    
    //methods
    
    std::string listRecentProjects() override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::DBus | msg::Recent | msg::Project | msg::List;
      queue.queue.push_back(msg::Message(mask, app::Message()));
      return RESPONSE;
    }
    
    std::string openProject(const std::string& path) override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      
      prj::Message pMessage;
      pMessage.directory = path;
      queue.queue.push_back(msg::Message(msg::Mask(msg::Request | msg::Open | msg::Project), pMessage));
      return RESPONSE;
    }
    
    //signals
    
    void goApplicationClosing(const msg::Message&)
    {
      emitApplicationClosing();
    }
    
    void goListRecentProjects(const msg::Message&)
    {
      std::vector<std::string> out;
      const auto &recent = prf::manager().getRecentProjects();
      for (const auto &r : recent) out.emplace_back(r.string());
      emitRecentList(out);
    }
    
    void goProjectOpened(const msg::Message &mIn)
    {
      emitProjectOpened(mIn.getPRJ().directory);
    }
    
    void goProjectClosed(const msg::Message &mIn)
    {
      emitProjectClosed(mIn.getPRJ().directory);
    }
    
    void goProjectCreated(const msg::Message &mIn)
    {
      emitProjectCreated(mIn.getPRJ().directory);
    }
  };
}

#endif //DBS_APPLICATION_H

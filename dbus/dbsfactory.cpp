/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "application/appapplication.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "project/prjmessage.h"
#include "feature/ftrbase.h"
#include "dbus/dbsfactoryworker.h"
#include "dbus/dbsfactory.h"

using namespace dbs;

struct Factory::Stow
{
  msg::Node node;
  msg::Sift sift;
  std::map<ftr::Type, std::function<ftr::Base*()>> dispatcher;
  
  Stow()
  {
    node.connect(msg::hub());
    node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
    sift.name = "dbs::Factory";
    
    sift.insert
    (
      {
        std::make_pair
        (
          msg::Request | msg::DBus | msg::Feature | msg::Add, std::bind(&Stow::goAddFeature, this, std::placeholders::_1)
        )
      }
    );
    
    dispatcher.insert({ftr::Type::Box, dbs::buildBox});
    dispatcher.insert({ftr::Type::BoundingBox, dbs::buildBoundingBox});
    dispatcher.insert({ftr::Type::DatumPoint, dbs::buildDatumPoint});
    dispatcher.insert({ftr::Type::DatumAxis, dbs::buildDatumAxis});
  }
  
  void goAddFeature(const msg::Message &mIn)
  {
    //hack of project message.
    const auto &type = mIn.getPRJ().directory;
    const auto &name = mIn.getPRJ().gitMessage;
    
    auto oType = ftr::toType(type);
    if (!oType) return; //What to do? DBus error?
    auto it = dispatcher.find(*oType);
    if (it == dispatcher.end()) return; //Error?
    auto *feature = it->second();
    feature->setName(QString::fromStdString(name));
  }
};

Factory::Factory() : stow(std::make_unique<Stow>()) {}
Factory::~Factory() = default;

std::vector<std::string> dbs::Factory::supportedList()
{
  std::vector<std::string> out;
  for (const auto &[t, f] : stow->dispatcher) out.push_back(ftr::toString(t));
  return out;
}

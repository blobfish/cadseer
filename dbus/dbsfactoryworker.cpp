/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "application/appapplication.h"
#include "project/prjproject.h"
#include "feature/ftrbox.h"
#include "feature/ftrboundingbox.h"
#include "feature/ftrdatumpoint.h"
#include "feature/ftrdatumaxis.h"
#include "dbus/dbsfactoryworker.h"

ftr::Base* dbs::buildBox()
{
  return app::instance()->getProject()->addFeature(std::make_unique<ftr::Box::Feature>());
}

ftr::Base* dbs::buildBoundingBox()
{
  return app::instance()->getProject()->addFeature(std::make_unique<ftr::BoundingBox::Feature>());
}

ftr::Base* dbs::buildDatumPoint()
{
  return app::instance()->getProject()->addFeature(std::make_unique<ftr::DatumPoint::Feature>());
}

ftr::Base* dbs::buildDatumAxis()
{
  return app::instance()->getProject()->addFeature(std::make_unique<ftr::DatumAxis::Feature>());
}


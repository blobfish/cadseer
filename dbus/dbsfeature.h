/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DBS_FEATURE_H
#define DBS_FEATURE_H

#include <sdbus-c++/sdbus-c++.h>

#include "tools/idtools.h"
#include "application/appapplication.h"
#include "project/prjproject.h"
#include "expressions/exprmanager.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "parameter/prmparameter.h"
#include "feature/ftrbase.h"
#include "feature/ftrmessage.h"
#include "tools/idtools.h"
#include "dbus/generated/featureadaptor.h"
#include "dbus/dbsqueue.h"

namespace dbs
{
  class Feature final : public sdbus::AdaptorInterfaces<org::blobfish::cadseer::feature_adaptor>
  {
  public:
    Feature(sdbus::IConnection& connection, sdbus::ObjectPath objectPath, dbs::Queue &qIn)
    : AdaptorInterfaces(connection, std::move(objectPath))
    , queue(qIn)
    {
      registerAdaptor();
      
      node.connect(msg::hub());
      node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
      sift.name = "dbs::Feature";
      sift.insert
      (
        {
          std::make_pair
          (
            msg::Request | msg::DBus | msg::Feature | msg::Name, std::bind(&Feature::goFeatureName, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Request | msg::DBus | msg::Feature | msg::Parameter | msg::List, std::bind(&Feature::goParameterList, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Request | msg::DBus | msg::Feature | msg::Parameter | msg::Edit, std::bind(&Feature::goSetParameterByTag, this, std::placeholders::_1)
          )
        }
      );
    }
    
    ~Feature()
    {
      unregisterAdaptor();
    }
    
  protected:
    dbs::Queue &queue;
    msg::Node node;
    msg::Sift sift;
    
    std::string name(const std::string& id) override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::DBus | msg::Feature | msg::Name;
      ftr::Message out;
      out.featureId = gu::stringToId(id);
      queue.queue.push_back(msg::Message(mask, out));
      return RESPONSE;
    }
    
    std::string parameters(const std::string& id) override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::DBus | msg::Feature | msg::Parameter | msg::List;
      ftr::Message out;
      out.featureId = gu::stringToId(id);
      queue.queue.push_back(msg::Message(mask, out));
      return RESPONSE;
    }
    
    std::string setParameterByTag(const std::string& id, const std::string &tag, const std::string &value) override
    {
      if (id.empty() || tag.empty() || value.empty()) return "Invalid Input";
      
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::DBus | msg::Feature | msg::Parameter | msg::Edit;
      prj::Message out; //hack project message
      out.featureIds.emplace_back(gu::stringToId(id));
      out.directory = tag;
      out.gitMessage = value;
      queue.queue.push_back(msg::Message(mask, out));
      return RESPONSE;
    }
    
    void goFeatureName(const msg::Message &mIn)
    {
      auto *feature = app::instance()->getProject()->findFeature(mIn.getFTR().featureId);
      if (!feature) return; //Error?
      emitName(feature->getName().toStdString());
    }
    
    void goParameterList(const msg::Message &mIn)
    {
      auto *feature = app::instance()->getProject()->findFeature(mIn.getFTR().featureId);
      if (!feature) return; //Error?
      
      std::vector<std::vector<std::string>> out;
      auto addParameter = [&](const prm::Parameter *p)
      {
        std::vector<std::string> current;
        current.emplace_back(gu::idToString(p->getId()));
        current.emplace_back(p->getName().toStdString());
        current.emplace_back(p->getTag());
        current.emplace_back(p->getValueTypeString());
        current.emplace_back(p->adaptToString());
        out.emplace_back(current);
      };
      
      for (const auto &p : feature->getParameters()) addParameter(p);
      emitParameterList(out);
    }
    
    void goSetParameterByTag(const msg::Message &mIn)
    {
      assert(mIn.isPRJ()); const auto &pm = mIn.getPRJ();
      assert(!pm.featureIds.empty());
      ftr::Base *feature = app::instance()->getProject()->findFeature(mIn.getPRJ().featureIds.front());
      if (!feature) return; //Error
      
      const auto &tag = pm.directory;
      const auto &value = pm.gitMessage;
      
      auto parameters = feature->getParameters(tag);
      if (parameters.size() != 1) return; //Error
      
      //now we create a temp expression manager to ensure compatible
      //types and to set the parameter value.
      expr::Manager localManager;
      std::string formula("temp = ");
      formula += value;
      auto result = localManager.parseString(formula);
      if (result.isAllGood())
      {
        auto oId = localManager.getExpressionId(result.expressionName);
        assert(oId);
        localManager.assignParameter(parameters.front(), *oId);
      }
    }
  };
}

#endif //DBS_FEATURE_H

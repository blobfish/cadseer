/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <sdbus-c++/sdbus-c++.h>

#include <QTimer>

#include "application/appapplication.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "dbus/dbsapplication.h"
#include "dbus/dbsproject.h"
#include "dbus/dbsfeature.h"
#include "dbus/dbsqueue.h"
#include "dbus/dbsfactory.h"
#include "dbus/dbsmanager.h"

using namespace dbs;

/* sdbus-c++ has it's own unique event loop. So in order to synchronize with
 * Qt event loop, We create a queue of messages that is filled in during the sdbus loop.
 * Then based upon on a qtimer event we dispatch those message to the qt event loop.
 * We are using a mutex to avoid asynchronous access to the message queue. Unfortunately
 * this makes all things dbus asynchronous and that carries over to outside application
 * So all responses to incoming messages will simply be the string 'received'.
 * 
 * Our generated object is responsible for both incoming and outgoing messages ... or
 * can we/should we split them? I like having only one queue to keep processing order.
 * So each interface build a request message and send to manager? I don't like the idea
 * of triggering an application wide message there. This is going to be triggered by
 * outside thread, so we want something 'low impact'. Each interface takes a reference
 * to the queue and uses that to append an incoming message. outgoing messages will
 * be triggered by manager at qtimer::timeout through message back to interface. Each
 * interface will need a node and sift.
 */
struct Manager::Stow
{
  msg::Node node;
  msg::Sift sift;
  
  Queue queue;
  QTimer timer;
  sdbus::ServiceName serviceName{"org.blobfish.cadseer"};
  sdbus::ObjectPath objectPath{"/org/blobfish/cadseer"};
  std::unique_ptr<sdbus::IConnection> connection = sdbus::createSessionBusConnection(serviceName);
  Application application{*connection, objectPath, queue};
  Project project{*connection, objectPath, queue};
  Feature feature{*connection, objectPath, queue};
  Factory factory{};
  
  Stow()
  {
    node.connect(msg::hub());
    node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
    sift.name = "dbs::Manager";
    
    sift.insert
    (
      {
        std::make_pair
        (
          msg::Request | msg::DBus | msg::Feature | msg::List | msg::Info, std::bind(&Stow::goListFeatureTypes, this, std::placeholders::_1)
        )
      }
    );
    
    connection->enterEventLoopAsync();
    QObject::connect(&timer, &QTimer::timeout, std::bind(&Stow::dispatch, this));
    timer.start(100); //10 times a second.
  }
  
  void dispatch()
  {
    std::unique_lock<std::mutex> lock(queue.mutex, std::try_to_lock);
    if(!lock.owns_lock()) return; //don't freeze and wait, get next timeout.
    for (const auto &m : queue.queue) node.send(m);
    queue.queue.clear();
    queue.mutex.unlock();
  }
  
  
  void goListFeatureTypes(const msg::Message&)
  {
    project.emitFeatureTypeList(factory.supportedList());
  }
};

Manager::Manager() : stow(std::make_unique<Stow>()) {}
Manager::~Manager() = default;

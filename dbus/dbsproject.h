/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DBS_PROJECT_H
#define DBS_PROJECT_H

#include <sdbus-c++/sdbus-c++.h>

#include "application/appapplication.h"
#include "application/appmessage.h"
#include "project/prjproject.h"
#include "project/prjmessage.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "feature/ftrbase.h"
#include "tools/idtools.h"
#include "dbus/generated/projectadaptor.h"
#include "dbus/dbsqueue.h"

namespace dbs
{
  class Project final : public sdbus::AdaptorInterfaces<org::blobfish::cadseer::project_adaptor>
  {
  public:
    Project(sdbus::IConnection& connection, sdbus::ObjectPath objectPath, dbs::Queue &qIn)
    : AdaptorInterfaces(connection, std::move(objectPath))
    , queue(qIn)
    {
      registerAdaptor();
      
      node.connect(msg::hub());
      node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
      sift.name = "dbs::Project";
      sift.insert
      (
        {
          std::make_pair
          (
            msg::Request | msg::DBus | msg::Feature | msg::List, std::bind(&Project::goListFeatures, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Response | msg::Post | msg::Add | msg::Feature, std::bind(&Project::goFeatureAdded, this, std::placeholders::_1)
          )
          , std::make_pair
          (
            msg::Response | msg::Pre | msg::Remove | msg::Feature, std::bind(&Project::goFeatureRemoved, this, std::placeholders::_1)
          )
        }
      );
    }
    
    ~Project()
    {
      unregisterAdaptor();
    }
    
  protected:
    dbs::Queue &queue;
    msg::Node node;
    msg::Sift sift;
    
    std::string update() override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::Project | msg::Update;
      queue.queue.push_back(msg::Message(mask));
      return RESPONSE;
    }
    
    std::string forceUpdate() override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::Force | msg::Update;
      queue.queue.push_back(msg::Message(mask));
      return RESPONSE;
    }
    
    std::string listFeatures() override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::DBus | msg::Feature | msg::List;
      queue.queue.push_back(msg::Message(mask, app::Message()));
      return RESPONSE;
    }
    
    std::string listFeatureTypes() override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::DBus | msg::Feature | msg::List | msg::Info;
      queue.queue.push_back(msg::Message(mask, app::Message()));
      return RESPONSE;
    }
    
    std::string importFile(const std::string &filePath) override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      prj::Message pm;
      pm.directory = filePath;
      queue.queue.push_back(msg::Message(msg::Request | msg::Import, pm));
      return RESPONSE;
    }
    
    std::string addFeature(const std::string &type, const std::string &name) override
    {
      std::lock_guard<std::mutex> lock(queue.mutex);
      msg::Mask mask = msg::Request | msg::DBus | msg::Feature | msg::Add;
      prj::Message mOut;
      mOut.directory = type;
      mOut.gitMessage = name;
      queue.queue.push_back(msg::Message(mask, mOut));
      return RESPONSE;
    }
    
    void goListFeatures(const msg::Message&)
    {
      std::vector<std::string> out;
      auto features = app::instance()->getProject()->getAllFeatures();
      for (const auto *f : features) out.emplace_back(gu::idToString(f->getId()));
      emitFeatureList(out);
    }
    
    void goFeatureAdded(const msg::Message &msgIn)
    {
      emitFeatureAdded(gu::idToString(msgIn.getPRJ().feature->getId()));
    }
    
    void goFeatureRemoved(const msg::Message &msgIn)
    {
      emitFeatureRemoved(gu::idToString(msgIn.getPRJ().feature->getId()));
    }
  };
}

#endif //DBS_PROJECT_H

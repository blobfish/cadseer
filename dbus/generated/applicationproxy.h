
/*
 * This file was automatically generated by sdbus-c++-xml2cpp; DO NOT EDIT!
 */

#ifndef __sdbuscpp__applicationproxy_h__proxy__H__
#define __sdbuscpp__applicationproxy_h__proxy__H__

#include <sdbus-c++/sdbus-c++.h>
#include <string>
#include <tuple>

namespace org {
namespace blobfish {
namespace cadseer {

class application_proxy
{
public:
    static constexpr const char* INTERFACE_NAME = "org.blobfish.cadseer.application";

protected:
    application_proxy(sdbus::IProxy& proxy)
        : m_proxy(proxy)
    {
    }

    application_proxy(const application_proxy&) = delete;
    application_proxy& operator=(const application_proxy&) = delete;
    application_proxy(application_proxy&&) = delete;
    application_proxy& operator=(application_proxy&&) = delete;

    ~application_proxy() = default;

    void registerProxy()
    {
        m_proxy.uponSignal("applicationClosing").onInterface(INTERFACE_NAME).call([this](){ this->onApplicationClosing(); });
        m_proxy.uponSignal("recentList").onInterface(INTERFACE_NAME).call([this](const std::vector<std::string>& Projects){ this->onRecentList(Projects); });
        m_proxy.uponSignal("projectOpened").onInterface(INTERFACE_NAME).call([this](const std::string& path){ this->onProjectOpened(path); });
        m_proxy.uponSignal("projectCreated").onInterface(INTERFACE_NAME).call([this](const std::string& path){ this->onProjectCreated(path); });
        m_proxy.uponSignal("projectClosed").onInterface(INTERFACE_NAME).call([this](const std::string& path){ this->onProjectClosed(path); });
    }

    virtual void onApplicationClosing() = 0;
    virtual void onRecentList(const std::vector<std::string>& Projects) = 0;
    virtual void onProjectOpened(const std::string& path) = 0;
    virtual void onProjectCreated(const std::string& path) = 0;
    virtual void onProjectClosed(const std::string& path) = 0;

public:
    std::string listRecentProjects()
    {
        std::string result;
        m_proxy.callMethod("listRecentProjects").onInterface(INTERFACE_NAME).storeResultsTo(result);
        return result;
    }

    std::string openProject(const std::string& path)
    {
        std::string result;
        m_proxy.callMethod("openProject").onInterface(INTERFACE_NAME).withArguments(path).storeResultsTo(result);
        return result;
    }

private:
    sdbus::IProxy& m_proxy;
};

}}} // namespaces

#endif

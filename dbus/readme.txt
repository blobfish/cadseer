cd dbus/generated
sdbus-c++-xml2cpp ../idl/application.xml --adaptor=applicationadaptor.h --proxy=applicationproxy.h
sdbus-c++-xml2cpp ../idl/project.xml --adaptor=projectadaptor.h --proxy=projectproxy.h
sdbus-c++-xml2cpp ../idl/feature.xml --adaptor=featureadaptor.h --proxy=featureproxy.h


//terminal commands to test dbus support. commands run in different terminals.
watch:
dbus-monitor sender='org.blobfish.cadseer'

trigger: //methods only see monitor for signals.
application:
//list recent projects
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.application.listRecentProjects

//open project
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.application.openProject \
string:"/home/tanderson/development/cadseer/test/projects/importTest"

project:
//update current project
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.update

//force update current project
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.forceUpdate

//list features of current project
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.listFeatures

//list all feature types
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.listFeatureTypes

//create a box type feature
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.addFeature \
string:"Box" \
string:"dbusBox"

//create a bounding box type feature
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.addFeature \
string:"BoundingBox" \
string:"dbusBoundingBox"

//create a datum point type feature
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.addFeature \
string:"DatumPoint" \
string:"dbusDatumPoint"

//create a datum axis type feature
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.addFeature \
string:"DatumAxis" \
string:"dbusDatumAxis"

//import a file
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.project.importFile \
string:"/home/tanderson/development/cadseer/test/files/ZMoterMount.brep"

feature:
//get feature name from id
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.feature.name \
string:"041caf0e-bddd-4ff4-a7b6-33025569c3c5"

//get list of parameters from a feature.
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.feature.parameters \
string:"27994be4-a84c-45f3-93ec-51f2a2d47054"

//find a features parameter by tag and change the value.
dbus-send \
--session \
--type=method_call \
--print-reply \
--dest=org.blobfish.cadseer \
/org/blobfish/cadseer \
org.blobfish.cadseer.feature.setParameterByTag \
string:"27994be4-a84c-45f3-93ec-51f2a2d47054" \
string:"Length" \
string:"10.0 + 5.0"

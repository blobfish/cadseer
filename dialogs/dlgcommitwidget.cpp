/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QLabel>
#include <QDateTimeEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <subprojects/libgit2pp/src/signature.hpp>
#include <subprojects/libgit2pp/src/oid.hpp>

#include "dialogs/dlgcommitwidget.h"

using namespace dlg;

CommitWidget::CommitWidget(QWidget *parent):
QWidget(parent)
{
  setContentsMargins(0, 0, 0, 0);
  buildGui();
}

CommitWidget::~CommitWidget() = default;

void CommitWidget::setCommit(const git2::Commit &cIn)
{
  updateSha(cIn.oid());
  git2::Signature author = cIn.author();
  authorNameLabel->setText(QString::fromStdString(author.name()));
  authorEMailLabel->setText(QString::fromStdString(author.email()));
  auto commitDateTime = QDateTime::fromSecsSinceEpoch(author.when(), Qt::LocalTime, author.when_offset());
  dateTimeEdit->setDateTime(commitDateTime);
  textEdit->setText(QString::fromStdString(cIn.message()));
  updateRelative();
}

void CommitWidget::setMessage(const std::string &mIn)
{
  textEdit->setText(QString::fromStdString(mIn));
}

void CommitWidget::clear()
{
  shaLabel->clear();
  authorNameLabel->clear();
  authorEMailLabel->clear();
  dateTimeEdit->clear();
  textEdit->clear();
}

void CommitWidget::updateSha(const git2::OId &id)
{
  std::string idString = id.format();
  if (idString.size() > shaLength) idString = idString.substr(0, shaLength);
  shaLabel->setText(QString::fromStdString(idString));
}

void CommitWidget::updateRelative()
{
  auto commitDateTime = dateTimeEdit->dateTime();
  auto now = QDateTime::currentDateTime();
  
  auto days = commitDateTime.daysTo(now);
  if (days < 2) //days is the number of times midnight has been crossed. so 10 minutes can be 1 day.
  {
    auto seconds = commitDateTime.secsTo(now);
    if (seconds < 120){relativeLabel->setText(QString::number(seconds) + tr(" Seconds Ago")); return;}
    int minutes = seconds / 60;
    if (minutes < 120){relativeLabel->setText(QString::number(minutes) + tr(" Minutes Ago")); return;}
    int hours = minutes / 60;
    relativeLabel->setText(QString::number(hours) + tr(" Hours Ago"));
    return;
  }
  if (days < 30) {relativeLabel->setText(QString::number(days) + tr(" Days Ago")); return;}
  int months = days / 30;
  if (months < 12) {relativeLabel->setText(QString::number(months) + tr(" Months Ago")); return;}
  int years = months / 12;
  months = months - (years * 12);
  QString message = QString::number(years) + tr(" Years Ago");
  if (months > 0) message += tr(" And ") + QString::number(months) + tr(" Months Ago");
  relativeLabel->setText(message);
}

void CommitWidget::buildGui()
{
  //just the visual labels
  QLabel *sha = new QLabel(tr("Sha:"), this);
  QLabel *authorName = new QLabel(tr("Author Name:"), this);
  QLabel *authorEMail = new QLabel(tr("Author EMail:"), this);
  QLabel *dateTime = new QLabel(tr("Date Time:"), this);
  QLabel *relative = new QLabel(tr("Relative:"), this);
  
  //actual value labels
  shaLabel = new QLabel(this);
  authorNameLabel = new QLabel(this);
  authorEMailLabel = new QLabel(this);
  dateTimeEdit = new QDateTimeEdit(this); dateTimeEdit->setReadOnly(true);
  relativeLabel = new QLabel(this);
  
  QGridLayout *gl = new QGridLayout();
  gl->addWidget(sha, 0, 0, Qt::AlignRight); gl->addWidget(shaLabel, 0, 1, Qt::AlignLeft);
  gl->addWidget(authorName, 1, 0, Qt::AlignRight); gl->addWidget(authorNameLabel, 1, 1, Qt::AlignLeft);
  gl->addWidget(authorEMail, 2, 0, Qt::AlignRight); gl->addWidget(authorEMailLabel, 2, 1, Qt::AlignLeft);
  gl->addWidget(dateTime, 3, 0, Qt::AlignRight); gl->addWidget(dateTimeEdit, 3, 1, Qt::AlignLeft);
  gl->addWidget(relative, 4, 0, Qt::AlignRight); gl->addWidget(relativeLabel, 4, 1, Qt::AlignLeft);
  
  QHBoxLayout *labelLayout = new QHBoxLayout();
  labelLayout->addStretch();
  labelLayout->addLayout(gl);
  
  textEdit = new QTextEdit(this); textEdit->setReadOnly(true);
  textEdit->setWhatsThis(tr("A descriptive message of the commit changes"));
  
  QVBoxLayout *mainLayout = new QVBoxLayout();
  mainLayout->setContentsMargins(0, 0, 0, 0);
  mainLayout->addLayout(labelLayout);
  mainLayout->addWidget(textEdit);
  this->setLayout(mainLayout);
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DLG_COMMITWIDGET_H
#define DLG_COMMITWIDGET_H

#include <QWidget>

#include <subprojects/libgit2pp/src/commit.hpp>
#include "subprojects/libgit2pp/src/tag.hpp"

class QLabel;
class QDateTimeEdit;
class QTextEdit;

namespace dlg
{
  /**
  * We want to display all the information of the commit and not the tag.
  * The exception to this is we want the tag message to be displayed.
  * so setCommit fills in everything, yes even message, and in the case
  * of a tag caller can overwrite with setMessage. see cmv::Revision.
  */
  class CommitWidget : public QWidget
  {
  public:
    CommitWidget(QWidget *parent);
    ~CommitWidget() override;
    
    void setCommit(const git2::Commit&);
    void setMessage(const std::string&);
    void clear();
    void setShaLength(std::size_t l){shaLength = l;} //recall set* to take effect
    
    QLabel *shaLabel;
    QLabel *authorNameLabel;
    QLabel *authorEMailLabel;
    QDateTimeEdit *dateTimeEdit;
    QLabel *relativeLabel;
    QTextEdit *textEdit;
    
  private:
    void buildGui();
    void updateSha(const git2::OId&);
    void updateRelative();
    
    std::size_t shaLength = 12;
  };
}

#endif // DLG_COMMITWIDGET_H

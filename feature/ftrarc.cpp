/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gp_Ax3.hxx>
#include <gp_Circ.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <TopExp.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "library/lbripgroup.h"
#include "library/lbrcsysdragger.h"
#include "library/lbrlineardimension.h"
#include "feature/ftrprimitive.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/idtools.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlarcsarc.h"
#include "feature/ftrarc.h"

using boost::uuids::uuid;
using namespace ftr::Arc;
QIcon Feature::icon = QIcon(":/resources/images/sketchArc.svg");

struct Feature::Stow
{
  Feature &feature;
  Primitive primitive;
  osg::ref_ptr<osg::MatrixTransform> gridTransform{new osg::MatrixTransform()}; //for dragger link
  
  prm::Parameter closed{QObject::tr("Closed"), true, Tags::closed};
  prm::Parameter angle{prm::Names::Angle, 360.0, prm::Tags::Angle};
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> closedLabel{new lbr::PLabel(&closed)};
  osg::ref_ptr<lbr::PLabel> angleLabel{new lbr::PLabel(&angle)};
  
  uuid edgeId{gu::createRandomId()};
  uuid vertex0Id{gu::createRandomId()};
  uuid vertex1Id{gu::createRandomId()};
  
  Stow(Feature &fIn)
  : feature(fIn)
  , primitive(Primitive::Input{fIn, fIn.parameters, fIn.annexes})
  {
    primitive.addRadius(2.0); //TODO add to preferences
    angle.setConstraint(prm::Constraint::buildNonZeroPositiveAngle());
    
    closed.connect(dirtyObserver); closed.connect(syncObserver); feature.parameters.push_back(&closed);
    angle.connect(dirtyObserver); feature.parameters.push_back(&angle);
    
    feature.overlaySwitch->addChild(gridTransform);
    gridTransform->addChild(grid);
    grid->addChild(closedLabel);
    grid->addChild(angleLabel);
    
    primitive.csysDragger.dragger->linkToMatrix(gridTransform.get());
    primitive.radiusIP->setMatrixDims(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(0.0, 1.0, 0.0)));
    primitive.radiusIP->setMatrixDragger(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(-1.0, 0.0, 0.0)));
    primitive.radiusIP->setDimsFlipped(true);
    primitive.radiusIP->setRotationAxis(osg::Vec3d(0.0, 0.0, 1.0), osg::Vec3d(-1.0, 0.0, 0.0));
    
    prmActiveSync();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    
    if (closed.getBool()) angle.setActive(false);
    else angle.setActive(true);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Arc");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->primitive.sShape.reset();
  try
  {
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    stow->primitive.csysLinkUpdate(pIn);
    
    double p0 = 0.0;
    double p1 = osg::DegreesToRadians(360.0);
    auto localRadius = stow->primitive.radius->getDouble();
    if (!stow->closed.getBool()) p1 = osg::DegreesToRadians(stow->angle.getDouble());
    gp_Circ circ(gp_Ax2(), localRadius);
    BRepBuilderAPI_MakeEdge em(circ, p0, p1);
    if (!em.IsDone()) throw std::runtime_error("Edge Maker Failed");
    TopoDS_Edge edge = em.Edge();
    
    //put shape into position.
    const auto &localMatrix = stow->primitive.csys.getMatrix();
    gp_Trsf transformation;
    transformation.SetTransformation(gp_Ax3(gu::toOcc(localMatrix)));
    transformation.Invert();
    TopLoc_Location location(transformation);
    edge.Location(location);
    
    stow->primitive.sShape.setOCCTShape(edge, getId());
    stow->primitive.sShape.updateId(edge, stow->edgeId);
    stow->primitive.sShape.updateId(TopExp::FirstVertex(edge), stow->vertex0Id);
    if (!TopExp::LastVertex(edge).IsSame(TopExp::FirstVertex(edge)))
      stow->primitive.sShape.updateId(TopExp::LastVertex(edge), stow->vertex1Id);
    stow->primitive.sShape.ensureNoNils();
    stow->primitive.sShape.ensureNoDuplicates();
    stow->primitive.sShape.ensureEvolve();
    
    stow->grid->setPosition(osg::Vec3d(0.0, localRadius, 0.0) * localMatrix);
    stow->gridTransform->setMatrix(osg::Matrixd::identity());
    mainTransform->setMatrix(osg::Matrixd::identity());
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  stow->primitive.IPsToCsys();
  if (!lastUpdateLog.empty()) std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::arcs::Arc so
  (
    Base::serialOut()
    , stow->primitive.sShape.serialOut()
    , stow->primitive.csysType.serialOut()
    , stow->primitive.csys.serialOut()
    , stow->primitive.csysLinked.serialOut()
    , stow->closed.serialOut()
    , stow->angle.serialOut()
    , stow->primitive.radius->serialOut()
    , stow->primitive.csysDragger.serialOut()
    , stow->primitive.radiusIP->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
    , stow->closedLabel->serialOut()
    , stow->angleLabel->serialOut()
    , gu::idToString(stow->edgeId)
    , gu::idToString(stow->vertex0Id)
    , gu::idToString(stow->vertex1Id)
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::arcs::arc(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::arcs::Arc &so)
{
  Base::serialIn(so.base());
  stow->primitive.sShape.serialIn(so.seerShape());
  stow->primitive.csysType.serialIn(so.csysType());
  stow->primitive.csys.serialIn(so.csys());
  stow->primitive.csysLinked.serialIn(so.csysLinked());
  stow->closed.serialIn(so.closed());
  stow->angle.serialIn(so.angle());
  stow->primitive.radius->serialIn(so.radius());
  stow->primitive.csysDragger.serialIn(so.csysDragger());
  stow->primitive.radiusIP->serialIn(so.radiusIP());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  stow->closedLabel->serialIn(so.closedLabel());
  stow->angleLabel->serialIn(so.angleLabel());
  stow->edgeId = gu::stringToId(so.edgeId());
  stow->vertex0Id = gu::stringToId(so.vertex0Id());
  stow->vertex1Id = gu::stringToId(so.vertex1Id());
}

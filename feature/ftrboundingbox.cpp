/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <BRepMesh_IncrementalMesh.hxx>

#include <osg/Switch>
#include <osg/Geometry>
#include <osg/LineWidth>

#include "globalutilities.h"
#include "preferences/preferencesXML.h"
#include "preferences/prfmanager.h"
#include "modelviz/mdvnodemaskdefs.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "tools/occtools.h"
#include "feature/ftrupdatepayload.h"
#include "tools/featuretools.h"
#include "project/serial/generated/prjsrlbbxsboundingbox.h"
#include "feature/ftrboundingbox.h"

using boost::uuids::uuid;
using namespace ftr::BoundingBox;
QIcon Feature::icon = QIcon(":/resources/images/boundingbox.svg");

struct Feature::Stow
{
  Feature &feature;
  
  prm::Parameter BBType{QObject::tr("Bounding Box Type"), 0, Tags::Type};
  prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  prm::Parameter origin{prm::Names::Origin, osg::Vec3d(), prm::Tags::Origin};
  prm::Parameter length{prm::Names::Length, 1.0, prm::Tags::Length};
  prm::Parameter width{prm::Names::Width, 1.0, prm::Tags::Width};
  prm::Parameter height{prm::Names::Height, 1.0, prm::Tags::Height};
  
  osg::ref_ptr<osg::Geometry> box;
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> BBTypeLabel{new lbr::PLabel(&BBType)};
  osg::ref_ptr<lbr::PLabel> originLabel{new lbr::PLabel(&origin)};
  osg::ref_ptr<lbr::PLabel> lengthLabel{new lbr::PLabel(&length)};
  osg::ref_ptr<lbr::PLabel> widthLabel{new lbr::PLabel(&width)};
  osg::ref_ptr<lbr::PLabel> heightLabel{new lbr::PLabel(&height)};
  
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  Stow(Feature &fIn) : feature(fIn)
  {
    QStringList tStrings =
    {
      QObject::tr("Constant")
      , QObject::tr("Picks")
    };
    BBType.setEnumeration(tStrings);
    BBType.connect(dirtyObserver);
    BBType.connect(syncObserver);
    feature.parameters.push_back(&BBType);
    
    picks.connect(dirtyObserver); feature.parameters.push_back(&picks);
    origin.connect(dirtyObserver); feature.parameters.push_back(&origin);
    
    length.setConstraint(prm::Constraint::buildZeroPositive());
    width.setConstraint(prm::Constraint::buildZeroPositive());
    height.setConstraint(prm::Constraint::buildZeroPositive());
    length.connect(dirtyObserver); feature.parameters.push_back(&length);
    width.connect(dirtyObserver); feature.parameters.push_back(&width);
    height.connect(dirtyObserver); feature.parameters.push_back(&height);
    
    buildBox();
    updateBox();
    feature.overlaySwitch->addChild(grid);
    BBTypeLabel->refresh(); grid->addChild(BBTypeLabel);
    grid->addChild(originLabel);
    grid->addChild(lengthLabel);
    grid->addChild(widthLabel);
    grid->addChild(heightLabel);
    
    prmActiveSync();
  }
  
  void buildBox()
  {
    box = new osg::Geometry();
    feature.getMainSwitch()->addChild(box);
    osg::Vec3Array *vertices = new osg::Vec3Array(8);
    box->setVertexArray(vertices);
    osg::Vec4Array *colors = new osg::Vec4Array();
    box->setColorArray(colors);
    box->setColorBinding(osg::Geometry::BIND_OVERALL);
    colors->push_back(osg::Vec4(0.88, 0.2, 0.88, 1.0));
    box->setNodeMask(mdv::noIntersect);
    box->setName("box");
    box->setDataVariance(osg::Object::STATIC);
    box->setUseDisplayList(false);
    // box->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(5.0f));
    box->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    
    //we don't worry about zero length lines.
    box->addPrimitiveSet(new osg::DrawArrays(GL_LINE_LOOP, 0, 4));
    box->addPrimitiveSet(new osg::DrawArrays(GL_LINE_LOOP, 4, 4));
    auto *de = new osg::DrawElementsUInt(GL_LINES, 8);
    de->addElement(0);
    de->addElement(4);
    de->addElement(1);
    de->addElement(5);
    de->addElement(2);
    de->addElement(6);
    de->addElement(3);
    de->addElement(7);
    box->addPrimitiveSet(de);
  }
  
  void updateBox()
  {
    osg::Vec3d xProj = osg::Vec3d(1.0, 0.0, 0.0) * length.getDouble();
    osg::Vec3d yProj = osg::Vec3d(0.0, 1.0, 0.0) * width.getDouble();
    osg::Vec3d zProj = osg::Vec3d(0.0, 0.0, 1.0) * height.getDouble();
    
    osg::Vec3Array *vertices = dynamic_cast<osg::Vec3Array*>(box->getVertexArray()); assert(vertices);
    (*vertices)[0] = origin.getVector();
    (*vertices)[1] = (*vertices)[0] + xProj;
    (*vertices)[2] = (*vertices)[1] + yProj;
    (*vertices)[3] = (*vertices)[2] + -xProj;
    (*vertices)[4] = origin.getVector() + zProj;
    (*vertices)[5] = (*vertices)[4] + xProj;
    (*vertices)[6] = (*vertices)[5] + yProj;
    (*vertices)[7] = (*vertices)[6] + -xProj;
    vertices->dirty();
    
    box->dirtyBound();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    
    int localType = BBType.getInt();
    if (localType == 0)
    {
      picks.setActive(false);
      origin.setActive(true);
      length.setActive(true);
      width.setActive(true);
      height.setActive(true);
    }
    else
    {
      picks.setActive(true);
      origin.setActive(false);
      length.setActive(false);
      width.setActive(false);
      height.setActive(false);
    }
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("BoundingBox");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const ftr::UpdatePayload &plIn)
{
  lastUpdateLog.clear();
  
  prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
  int localType = stow->BBType.getInt();
  if (localType == 1)
  {
    const auto &localPicks = stow->picks.getPicks();
    if (localPicks.empty()) throw std::runtime_error("No picks to resolve");
    tls::Resolver resolver(plIn);
    occt::BoundingBox localBB;
    double linear = prf::manager().rootPtr->visual().mesh().linearDeflection();
    double angular = osg::DegreesToRadians(prf::manager().rootPtr->visual().mesh().angularDeflection());
    for (const auto &p : localPicks)
    {
      if (!resolver.resolve(p)) continue;
      for (const auto &s : resolver.getShapes())
      {
        //oh this is ugly. See notes: -> tags: prj: ftr: occt triangulation: boundingbox
        BRepMesh_IncrementalMesh(s, linear, Standard_False, angular, Standard_True);
        localBB.add(s);
      }
    }
    if (localBB.getOcctBox().IsVoid()) throw std::runtime_error("Void Box");
    stow->origin.setValue(gu::toOsg(localBB.getCorners().front()));
    stow->length.setValue(localBB.getLength());
    stow->width.setValue(localBB.getWidth());
    stow->height.setValue(localBB.getHeight());
  }
  
  setModelClean();
}

void Feature::updateVisual()
{
  stow->updateBox();
  setVisualClean();
}

void Feature::setByPoints(const std::vector<double> &psIn)
{
  if (psIn.size() != 6) return;
  osg::Vec3d p0(psIn.at(0), psIn.at(1), psIn.at(2));
  osg::Vec3d p1(psIn.at(3), psIn.at(4), psIn.at(5));
  
  double l = p1.x() - p0.x();
  double w = p1.y() - p0.y();
  double h = p1.z() - p0.z();
  
  if (l < 0.0 || w < 0.0 || h < 0.0) return;
  stow->BBType.setValue(0); //convert to constant if not already.
  stow->origin.setValue(p0);
  stow->length.setValue(l);
  stow->width.setValue(w);
  stow->height.setValue(h);
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::bbxs::BoundingBox so
  (
    Base::serialOut()
    , stow->BBType.serialOut()
    , stow->picks.serialOut()
    , stow->origin.serialOut()
    , stow->length.serialOut()
    , stow->width.serialOut()
    , stow->height.serialOut()
    , stow->BBTypeLabel->serialOut()
    , stow->originLabel->serialOut()
    , stow->lengthLabel->serialOut()
    , stow->widthLabel->serialOut()
    , stow->heightLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::bbxs::boundingbox(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::bbxs::BoundingBox &so)
{
  Base::serialIn(so.base());
  stow->BBType.serialIn(so.BBType());
  stow->picks.serialIn(so.picks());
  stow->origin.serialIn(so.origin());
  stow->length.serialIn(so.length());
  stow->width.serialIn(so.width());
  stow->height.serialIn(so.height());
  stow->BBTypeLabel->serialIn(so.BBTypeLabel());
  stow->originLabel->serialIn(so.originLabel());
  stow->lengthLabel->serialIn(so.lengthLabel());
  stow->widthLabel->serialIn(so.widthLabel());
  stow->heightLabel->serialIn(so.heightLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
}

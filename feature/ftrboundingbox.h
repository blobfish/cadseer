/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_BOUNDINGBOX_H
#define FTR_BOUNDINGBOX_H

#include "feature/ftrbase.h"

namespace prj{namespace srl{namespace bbxs{class BoundingBox;}}}

namespace ftr
{
  namespace BoundingBox
  {
    namespace Tags
    {
      inline constexpr std::string_view Type = "Type";
    }
    /* Just a visual box, no seer shape.
     */
    class Feature : public Base
    {
    public:
      Feature();
      ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      void updateVisual() override;
      Type getType() const override {return Type::BoundingBox;}
      const std::string& getTypeString() const override {return toString(Type::BoundingBox);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::bbxs::BoundingBox&);
      
      void setByPoints(const std::vector<double>&); //makes constant type if not already.
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}

#endif //FTR_BOUNDINGBOX_H

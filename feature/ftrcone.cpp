/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gp_Ax3.hxx>
#include <TopoDS.hxx>
#include <BRepPrimAPI_MakeCone.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "tools/idtools.h"
#include "preferences/preferencesXML.h"
#include "preferences/prfmanager.h"
#include "library/lbrlineardimension.h"
#include "library/lbripgroup.h"
#include "project/serial/generated/prjsrlcnscone.h"
#include "annex/annseershape.h"
#include "annex/anncsysdragger.h"
#include "tools/featuretools.h"
#include "feature/ftrpick.h"
#include "feature/ftrupdatepayload.h"
#include "feature/ftrprimitive.h"
#include "parameter/prmparameter.h"
#include "feature/ftrcone.h"

using namespace ftr::Cone;
using boost::uuids::uuid;

enum class FeatureTag
{
  Root,         //!< compound
  Solid,        //!< solid
  Shell,        //!< shell
  FaceBottom,   //!< bottom of cone
  FaceConical,  //!< conical face
  FaceTop,      //!< might be empty
  WireBottom,   //!< wire on base face
  WireConical,  //!< wire along conical face
  WireTop,      //!< wire along top
  EdgeBottom,   //!< bottom edge.
  EdgeConical,  //!< edge on conical face
  EdgeTop,      //!< top edge
  VertexBottom, //!< bottom vertex
  VertexTop     //!< top vertex
};

static const std::map<FeatureTag, std::string> featureTagMap =
{
  {FeatureTag::Root, "Root"},
  {FeatureTag::Solid, "Solid"},
  {FeatureTag::Shell, "Shell"},
  {FeatureTag::FaceBottom, "FaceBase"},
  {FeatureTag::FaceConical, "FaceConical"},
  {FeatureTag::FaceTop, "FaceTop"},
  {FeatureTag::WireBottom, "WireBottom"},
  {FeatureTag::WireConical, "WireConical"},
  {FeatureTag::WireTop, "WireTop"},
  {FeatureTag::EdgeBottom, "EdgeBottom"},
  {FeatureTag::EdgeConical, "EdgeConical"},
  {FeatureTag::EdgeTop, "EdgeTop"},
  {FeatureTag::VertexBottom, "VertexBottom"},
  {FeatureTag::VertexTop, "VertexTop"}
};

QIcon Feature::icon = QIcon(":/resources/images/cone.svg");

inline static const prf::Cone& pCone(){return prf::manager().rootPtr->features().cone().get();}

struct Feature::Stow
{
  Feature &feature;
  Primitive primitive;
  
  const osg::Matrixd radiusDimBase{osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(0.0, 1.0, 0.0))};
  const osg::Matrixd radiusDraggerBase{osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(-1.0, 0.0, 0.0))};
  
  Stow() = delete;
  Stow(Feature &fIn)
  : feature(fIn)
  , primitive(Primitive::Input{fIn, fIn.parameters, fIn.annexes})
  {
    primitive.addRadius1(pCone().radius1());
    primitive.addRadius2(pCone().radius2());
    //constraints on radius in primitives are non zero positive. overwrite.
    primitive.radius1->setConstraint(prm::Constraint::buildZeroPositive());
    primitive.radius2->setConstraint(prm::Constraint::buildZeroPositive());
    primitive.addHeight(pCone().height());
    
    auto radiusAxisBase = std::make_tuple(osg::Vec3d(0.0, 0.0, 1.0), osg::Vec3d(-1.0, 0.0, 0.0));
    
    primitive.radius1IP->setMatrixDims(radiusDimBase);
    primitive.radius1IP->setMatrixDragger(radiusDraggerBase);
    primitive.radius1IP->setDimsFlipped(true);
    primitive.radius1IP->setRotationAxis(std::get<0>(radiusAxisBase), std::get<1>(radiusAxisBase));
    
    primitive.radius2IP->setMatrixDims(radiusDimBase);
    primitive.radius2IP->setMatrixDragger(radiusDraggerBase);
    primitive.radius2IP->setDimsFlipped(true);
    primitive.radius2IP->setRotationAxis(std::get<0>(radiusAxisBase), std::get<1>(radiusAxisBase));

    primitive.heightIP->setMatrixDims(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(1.0, 0.0, 0.0)));
    primitive.heightIP->setRotationAxis(osg::Vec3d(0.0, 0.0, 1.0), osg::Vec3d(0.0, -1.0, 0.0));
    
    initializeMaps();
  }
  
  void updateIPs()
  {
    primitive.IPsToCsys();
    
    //radius1 on base is in correct location.
    
    //radius2 dragger and dims need to match height
    primitive.radius2IP->setMatrixDims(radiusDimBase * osg::Matrixd::translate(0.0, 0.0, primitive.height->getDouble()));
    primitive.radius2IP->setMatrixDragger(radiusDraggerBase * osg::Matrixd::translate(0.0, 0.0, primitive.height->getDouble()));
    
    //push height dimension of to max radius.
    double offset = std::max(primitive.radius1->getDouble(), primitive.radius2->getDouble());
    primitive.heightIP->mainDim->setSqueeze(offset);
    primitive.heightIP->mainDim->setExtensionOffset(offset);
  }
  
  //the quantity of cone shapes can change so generating maps from first update can lead to missing
  //ids and shapes. So here we will generate the maps with all necessary rows.
  void initializeMaps()
  {
    //result
    std::vector<uuid> tempIds; //save ids for later.
    for (unsigned int index = 0; index < 14; ++index)
    {
      uuid tempId = gu::createRandomId();
      tempIds.push_back(tempId);
      primitive.sShape.insertEvolve(gu::createNilId(), tempId);
    }
    
    //helper lamda
    auto insertIntoFeatureMap = [this](const uuid &idIn, FeatureTag featureTagIn)
    {
      primitive.sShape.insertFeatureTag(idIn, featureTagMap.at(featureTagIn));
    };
    
    //first we do the compound that is root. this is not in box maker.
    insertIntoFeatureMap(tempIds.at(0), FeatureTag::Root);
    insertIntoFeatureMap(tempIds.at(1), FeatureTag::Solid);
    insertIntoFeatureMap(tempIds.at(2), FeatureTag::Shell);
    insertIntoFeatureMap(tempIds.at(3), FeatureTag::FaceBottom);
    insertIntoFeatureMap(tempIds.at(4), FeatureTag::FaceConical);
    insertIntoFeatureMap(tempIds.at(5), FeatureTag::FaceTop);
    insertIntoFeatureMap(tempIds.at(6), FeatureTag::WireBottom);
    insertIntoFeatureMap(tempIds.at(7), FeatureTag::WireConical);
    insertIntoFeatureMap(tempIds.at(8), FeatureTag::WireTop);
    insertIntoFeatureMap(tempIds.at(9), FeatureTag::EdgeBottom);
    insertIntoFeatureMap(tempIds.at(10), FeatureTag::EdgeConical);
    insertIntoFeatureMap(tempIds.at(11), FeatureTag::EdgeTop);
    insertIntoFeatureMap(tempIds.at(12), FeatureTag::VertexBottom);
    insertIntoFeatureMap(tempIds.at(13), FeatureTag::VertexTop);
    //   std::cout << std::endl << std::endl <<
    //     "result Container: " << std::endl << resultContainer << std::endl << std::endl <<
    //     "feature Container:" << std::endl << featureContainer << std::endl << std::endl <<
    //     "evolution Container:" << std::endl << evolutionContainer << std::endl << std::endl;
  }
  
  /* Cones allow a zero radius on one end resulting in the top or bottom face might
   * degenerate to a point. This invalidates any regularity in shape indexes or offsets.
   * So the assigning of ids to shapes is not simple like a box. Here we look for partners
   * to match shapes after relocation. This assumes the cone construction doesn't 're-use'
   * shapes.
   */
  void updateResult(BRepPrimAPI_MakeCone& coneBuilderIn)
  {
    //helper lamda
    auto updateShapeByTag = [this](const TopoDS_Shape &shapeIn, FeatureTag featureTagIn)
    {
      if (shapeIn.IsNull()) return; //null shapes for zero radii.
      uuid localId = primitive.sShape.featureTagId(featureTagMap.at(featureTagIn));
      primitive.sShape.updateId(shapeIn, localId);
    };
    
    auto shapes = primitive.sShape.getAllShapes();
    auto findPartner = [&](const TopoDS_Shape &sIn) -> TopoDS_Shape
    {
      if (sIn.IsNull()) return sIn; //null shapes for zero radii.
      for (const auto &s : shapes) if (s.IsPartner(sIn)) return s;
      throw std::runtime_error("Couldn't find partner");
    };
    
    BRepPrim_Cone &coneSubMaker = coneBuilderIn.Cone(); //forces coneBuilderIn to be non-const.
    
    updateShapeByTag(findPartner(coneBuilderIn.Shape()), FeatureTag::Solid);
    updateShapeByTag(findPartner(coneSubMaker.Shell()), FeatureTag::Shell);
    updateShapeByTag(findPartner(coneSubMaker.LateralFace()), FeatureTag::FaceConical);
    updateShapeByTag(findPartner(coneSubMaker.LateralWire()), FeatureTag::WireConical);
    updateShapeByTag(findPartner(coneSubMaker.StartEdge()), FeatureTag::EdgeConical); //seam
    updateShapeByTag(findPartner(coneSubMaker.BottomStartVertex()), FeatureTag::VertexBottom);
    updateShapeByTag(findPartner(coneSubMaker.TopStartVertex()), FeatureTag::VertexTop);
    
    if (coneSubMaker.HasBottom())
    {
      updateShapeByTag(findPartner(coneSubMaker.BottomFace()), FeatureTag::FaceBottom);
      updateShapeByTag(findPartner(coneSubMaker.BottomWire()), FeatureTag::WireBottom);
      updateShapeByTag(findPartner(coneSubMaker.BottomEdge()), FeatureTag::EdgeBottom);
    }
    
    if (coneSubMaker.HasTop())
    {
      updateShapeByTag(findPartner(coneSubMaker.TopFace()), FeatureTag::FaceTop);
      updateShapeByTag(findPartner(coneSubMaker.TopWire()), FeatureTag::WireTop);
      updateShapeByTag(findPartner(coneSubMaker.TopEdge()), FeatureTag::EdgeTop);
    }
  }
};

//only complete rotational cone. no partials. because top or bottom radius
//maybe 0.0, faces and wires might be null and edges maybe degenerate.
Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Cone");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

/*
void Feature::setCSys(const osg::Matrixd &csysIn)
{
  osg::Matrixd oldSystem = csys->getMatrix();
  if (!csys->setValue(csysIn))
    return; // already at this csys
    
  //apply the same transformation to dragger, so dragger moves with it.
  osg::Matrixd diffMatrix = osg::Matrixd::inverse(oldSystem) * csysIn;
  csysDragger->draggerUpdate(csysDragger->dragger->getMatrix() * diffMatrix);
}
*/

void Feature::updateModel(const UpdatePayload &plIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->primitive.sShape.reset();
  try
  {
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    stow->primitive.csysLinkUpdate(plIn);
    
    double rad1 = stow->primitive.radius1->getDouble();
    double rad2 = stow->primitive.radius2->getDouble();
    if (rad1 < std::numeric_limits<float>::epsilon()) rad1 = 0.0;
    if (rad2 < std::numeric_limits<float>::epsilon()) rad2 = 0.0;
    BRepPrimAPI_MakeCone coneMaker(rad1, rad2, stow->primitive.height->getDouble());
    coneMaker.Build();
    if (!coneMaker.IsDone()) throw std::runtime_error("Couldn't build cone.");
    
    TopoDS_Solid solid = TopoDS::Solid(coneMaker.Shape());
    gp_Trsf transform;
    transform.SetTransformation(gu::toOcc(stow->primitive.csys.getMatrix()));
    transform.Invert();
    solid.Location(TopLoc_Location(transform));
    stow->primitive.sShape.setOCCTShape(solid, getId());
    stow->updateResult(coneMaker);
    
    mainTransform->setMatrix(osg::Matrixd::identity());
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in cone update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in cone update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in cone update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  stow->updateIPs();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::cns::Cone coneOut
  (
    Base::serialOut(),
    stow->primitive.csysType.serialOut(),
    stow->primitive.radius1->serialOut(),
    stow->primitive.radius2->serialOut(),
    stow->primitive.height->serialOut(),
    stow->primitive.csys.serialOut(),
    stow->primitive.csysLinked.serialOut(),
    stow->primitive.csysDragger.serialOut(),
    stow->primitive.sShape.serialOut(),
    stow->primitive.heightIP->serialOut(),
    stow->primitive.radius1IP->serialOut(),
    stow->primitive.radius2IP->serialOut()
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::cns::cone(stream, coneOut, infoMap);
}

void Feature::serialRead(const prj::srl::cns::Cone& sCone)
{
  Base::serialIn(sCone.base());
  stow->primitive.csysType.serialIn(sCone.csysType());
  stow->primitive.radius1->serialIn(sCone.radius1());
  stow->primitive.radius2->serialIn(sCone.radius2());
  stow->primitive.height->serialIn(sCone.height());
  stow->primitive.csys.serialIn(sCone.csys());
  stow->primitive.csysLinked.serialIn(sCone.csysLinked());
  stow->primitive.csysDragger.serialIn(sCone.csysDragger());
  stow->primitive.sShape.serialIn(sCone.seerShape());
  stow->primitive.heightIP->serialIn(sCone.heightIP());
  stow->primitive.radius1IP->serialIn(sCone.radius1IP());
  stow->primitive.radius2IP->serialIn(sCone.radius2IP());
}

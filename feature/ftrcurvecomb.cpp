/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <TopoDS.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <GCPnts_QuasiUniformAbscissa.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <GeomLProp_CLProps.hxx>
#include <gp_Quaternion.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "modelviz/mdvcurvecomb.h"
#include "feature/ftrpick.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlccbscurvecomb.h"
#include "feature/ftrcurvecomb.h"

using boost::uuids::uuid;
using namespace ftr::CurveComb;
QIcon Feature::icon = QIcon(":/resources/images/curveComb.svg");

struct Feature::Stow
{
  Feature &feature;
  ann::SeerShape sShape;
  prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  prm::Parameter count{QObject::tr("Count"), 100, Tags::count};
  prm::Parameter scale{prm::Names::Scale, 1.0, prm::Tags::Scale};
  prm::Parameter autoRange{QObject::tr("Auto Range"), true, Tags::autoRange};
  prm::Parameter flip{QObject::tr("Flip"), false, Tags::flip};
  prm::Parameter uMin{QObject::tr("U Min"), 0.0, Tags::umin};
  prm::Parameter uMax{QObject::tr("U Max"), 1.0, Tags::umax};

  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  prm::Observer rangeObserver{std::bind(&Stow::rangeSync, this)};
  
  osg::ref_ptr<lbr::PLabel> countLabel{new lbr::PLabel(&count)};
  osg::ref_ptr<lbr::PLabel> scaleLabel{new lbr::PLabel(&scale)};
  osg::ref_ptr<lbr::PLabel> autoRangeLabel{new lbr::PLabel(&autoRange)};
  osg::ref_ptr<lbr::PLabel> flipLabel{new lbr::PLabel(&flip)};
  osg::ref_ptr<lbr::PLabel> uMinLabel{new lbr::PLabel(&uMin)};
  osg::ref_ptr<lbr::PLabel> uMaxLabel{new lbr::PLabel(&uMax)};
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<osg::MatrixTransform> comb = mdv::buildCurveComb();
  
  Stow(Feature &fIn)
  : feature(fIn)
  {
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));

    feature.parameters.push_back(&picks); picks.connect(dirtyObserver);
    feature.parameters.push_back(&count); count.connect(dirtyObserver);
    feature.parameters.push_back(&scale); scale.connect(dirtyObserver);
    feature.parameters.push_back(&autoRange); autoRange.connect(dirtyObserver); autoRange.connect(syncObserver);
    feature.parameters.push_back(&flip); flip.connect(dirtyObserver);
    feature.parameters.push_back(&uMin); uMin.connect(dirtyObserver); uMin.connect(rangeObserver);
    feature.parameters.push_back(&uMax); uMax.connect(dirtyObserver); uMax.connect(rangeObserver);
    
    auto constraint = prm::Constraint::buildUnit();
    constraint.intervals.front().lower.value = 3.0;
    constraint.intervals.front().upper.value = 10000.0;
    count.setConstraint(constraint);
    constraint.intervals.front().lower.value = 0.0001;
    constraint.intervals.front().upper.value = 10000.0;
    scale.setConstraint(constraint);
    
    grid->addChild(countLabel.get());
    grid->addChild(scaleLabel.get());
    grid->addChild(autoRangeLabel.get());
    grid->addChild(flipLabel.get());
    feature.overlaySwitch->addChild(grid.get());

    //going to position these on boundary
    feature.overlaySwitch->addChild(uMinLabel);
    feature.overlaySwitch->addChild(uMaxLabel);

    feature.getMainTransform()->addChild(comb);

    prmActiveSync();
  }

  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    prm::ObserverBlocker rangeBlocker(rangeObserver);

    auto ar = autoRange.getBool();
    uMin.setActive(!ar);
    uMax.setActive(!ar);
  }

  //ensure that umin <= umax and vmin <= vmax
  void rangeSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    prm::ObserverBlocker rangeBlocker(rangeObserver);

    //always adjust the max value. A zero range is acceptable.
    if ((uMax.getDouble() - uMin.getDouble()) < Precision::Confusion()) uMax.setValue(uMin.getDouble());
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("CurveComb");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }

    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    prm::ObserverBlocker rangeBlocker(stow->rangeObserver);
    
    auto thePicks = stow->picks.getPicks();
    if (thePicks.empty()) throw std::runtime_error("No Selection");
    tls::Resolver pr(pIn);
    if (!pr.resolve(thePicks.front())) throw std::runtime_error("invalid pick resolution");

    std::optional<TopoDS_Edge> theEdge;
    switch (pr.getPick().selectionType)
    {
      case slc::Type::Edge:
      {
        auto theShapes = pr.getShapes();
        if (theShapes.empty()) throw std::runtime_error("no pick shapes");
        if (theShapes.front().ShapeType() != TopAbs_EDGE) {assert(0); throw std::runtime_error("incorrect type");}
        theEdge = TopoDS::Edge(theShapes.front());
        break;
      }
      case slc::Type::Object: case slc::Type::Feature:
      {
        const auto &ss = *pr.getSeerShape();
        auto edges = ss.useGetChildrenOfType(ss.getRootOCCTShape(), TopAbs_EDGE);
        if (edges.empty()) throw std::runtime_error("No edges in feature");
        if (edges.front().ShapeType() != TopAbs_EDGE) {assert(0); throw std::runtime_error("incorrect type");}
        theEdge = TopoDS::Edge(edges.front());
        break;
      }
      default:
      {
        throw std::runtime_error("unsupported selection type");
        break;
      }
    }
    if (!theEdge) throw std::runtime_error("Unable to resolve face");

    BRepAdaptor_Curve adaptor(*theEdge);
    if (stow->autoRange.getBool())
    {
      stow->uMin.setValue(adaptor.FirstParameter());
      stow->uMax.setValue(adaptor.LastParameter());
    }
    else
    {
      //keep user numbers on or inside edge.
      stow->uMin.setValue(std::max(stow->uMin.getDouble(), adaptor.FirstParameter()));
      stow->uMax.setValue(std::min(stow->uMax.getDouble(), adaptor.LastParameter()));
    }

    //some sanity.
    if ((stow->uMax.getDouble() - stow->uMin.getDouble()) < Precision::Confusion())
      throw std::runtime_error("Invalid Range");
    
    //we don't really care about ids
    stow->sShape.setOCCTShape(*theEdge, getId());
    stow->sShape.ensureNoNils();
    stow->sShape.ensureEvolve();
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::updateVisual()
{
  try
  {
    //clear visual and fill in when done.
    auto *cb = dynamic_cast<mdv::CurveCombCallback*>(stow->comb->getUpdateCallback());
    cb->setPoints(std::vector<osg::Vec3d>(), std::vector<osg::Vec3d>());
    if (stow->sShape.isNull()) throw std::runtime_error("SeerShape is Null");
    
    auto edges = stow->sShape.useGetChildrenOfType(stow->sShape.getRootOCCTShape(), TopAbs_EDGE);
    if (edges.empty()) return;
    
    //update grid to bounding box center of edge.
    auto bb = occt::BoundingBox(edges.front());
    stow->grid->setPosition(gu::toOsg(bb.getCenter()));
    
    BRepAdaptor_Curve adapt(TopoDS::Edge(edges.front()));
    double curveScale = GCPnts_AbscissaPoint::Length(adapt) * 2.0;
    double overallScale = curveScale * stow->scale.getDouble();

    //put labels into place.
    stow->uMinLabel->setMatrix(osg::Matrixd::translate(gu::toOsg(adapt.Value(stow->uMin.getDouble()))));
    stow->uMaxLabel->setMatrix(osg::Matrixd::translate(gu::toOsg(adapt.Value(stow->uMax.getDouble()))));
    
    //get points on curve and their comb projected points.
    std::vector<osg::Vec3d> curvePoints;
    std::vector<osg::Vec3d> projections;
    GCPnts_QuasiUniformAbscissa step(adapt, stow->count.getInt(), stow->uMin.getDouble(), stow->uMax.getDouble());
    if (!step.IsDone()) throw std::runtime_error("Abscissa Failed");
    for (auto index = 1; index <= step.NbPoints(); ++index) //1 based array
    {
      double p = step.Parameter(index);
      curvePoints.push_back(gu::toOsg(adapt.Value(p)));
      projections.push_back(curvePoints.back()); //default to same point and change if good calc.
      
      GeomLProp_CLProps props(adapt.Curve().Curve(), p, 2, gp::Resolution());
      if (!props.IsTangentDefined()) continue;
      auto xPrime = props.D1(); if (xPrime.Magnitude() < std::numeric_limits<float>::epsilon()) continue;
      auto xPrimePrime = props.D2(); if (xPrimePrime.Magnitude() < std::numeric_limits<float>::epsilon()) continue;
      xPrime.Normalize();
      xPrimePrime.Normalize();
      auto biNormal = (xPrime ^ xPrimePrime).Normalized();
      auto normal = (biNormal ^ xPrime).Normalized(); //ensure ortho
      normal = adapt.Trsf().GetRotation() * normal;
      double curvature = props.Curvature();
      osg::Vec3d projection = gu::toOsg(normal);
      projection *= overallScale * curvature;
      if (stow->flip.getBool()) projection = -projection;
      projections.back() = curvePoints.back() + projection;
    }
    cb->setPoints(curvePoints, projections);
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " visual update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " visual update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " visual update." << std::endl;
    lastUpdateLog += s.str();
  }
  setVisualClean();
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::ccbs::CurveComb so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->picks.serialOut()
    , stow->count.serialOut()
    , stow->scale.serialOut()
    , stow->autoRange.serialOut()
    , stow->flip.serialOut()
    , stow->uMin.serialOut()
    , stow->uMax.serialOut()
    , stow->countLabel->serialOut()
    , stow->scaleLabel->serialOut()
    , stow->autoRangeLabel->serialOut()
    , stow->flipLabel->serialOut()
    , stow->uMinLabel->serialOut()
    , stow->uMaxLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::ccbs::curvecomb(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::ccbs::CurveComb &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.sShape());
  stow->picks.serialIn(so.picks());
  stow->count.serialIn(so.count());
  stow->scale.serialIn(so.scale());
  stow->autoRange.serialIn(so.autoRange());
  stow->flip.serialIn(so.flip());
  stow->uMin.serialIn(so.uMin());
  stow->uMax.serialIn(so.uMax());
  stow->countLabel->serialIn(so.countLabel());
  stow->scaleLabel->serialIn(so.scaleLabel());
  stow->autoRangeLabel->serialIn(so.autoRangeLabel());
  stow->flipLabel->serialIn(so.flipLabel());
  stow->uMinLabel->serialIn(so.uMinLabel());
  stow->uMaxLabel->serialIn(so.uMaxLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
}

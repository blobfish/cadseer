/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_CURVECOMB_H
#define FTR_CURVECOMB_H

#include "feature/ftrbase.h"

namespace prj{namespace srl{namespace ccbs{class CurveComb;}}}

namespace ftr
{
  namespace CurveComb
  {
    namespace Tags
    {
      inline constexpr std::string_view count = "count";
      inline constexpr std::string_view flip = "flip";
      inline constexpr std::string_view autoRange = "autoRange";
      inline constexpr std::string_view umin = "umin";
      inline constexpr std::string_view umax = "umax";
    }
    
    class Feature : public Base
    {
    public:
      Feature();
      ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      void updateVisual() override;
      Type getType() const override {return Type::CurveComb;}
      const std::string& getTypeString() const override {return toString(Type::CurveComb);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::ccbs::CurveComb&);
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}

#endif //FTR_CURVECOMB_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <TopoDS.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <BRepAdaptor_Surface.hxx>
#include <BRepAdaptor_CompCurve.hxx>
#include <GCPnts_AbscissaPoint.hxx>

#include <osg/Switch>
#include <osg/Geometry>
#include <osg/AutoTransform>
#include <osg/PositionAttitudeTransform>

#include "globalutilities.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "modelviz/mdvdatumpoint.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrldtpsdatumpoint.h"
#include "feature/ftrdatumpoint.h"

using boost::uuids::uuid;
using namespace ftr::DatumPoint;
QIcon Feature::icon = QIcon(":/resources/images/datumPoint.svg");

struct Feature::Stow
{
  Feature &feature;
  prm::Parameter pointType{QObject::tr("Point Type"), 0, Tags::pointType};
  prm::Parameter origin{prm::Names::Origin, osg::Vec3d(), prm::Tags::Origin};
  prm::Parameter size{prm::Names::Size, 1.0, prm::Tags::Size};
  prm::Parameter parameterU{QObject::tr("Parameter U"), 0.0, Tags::parameterU};
  prm::Parameter parameterV{QObject::tr("Parameter V"), 0.0, Tags::parameterV};
  prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  prm::Observer observer{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  osg::ref_ptr<lbr::PLabel> pointTypeLabel{new lbr::PLabel(&pointType)};
  osg::ref_ptr<lbr::PLabel> originLabel{new lbr::PLabel(&origin)};
  osg::ref_ptr<lbr::PLabel> sizeLabel{new lbr::PLabel(&size)};
  osg::ref_ptr<lbr::PLabel> parameterULabel{new lbr::PLabel(&parameterU)};
  osg::ref_ptr<lbr::PLabel> parameterVLabel{new lbr::PLabel(&parameterV)};
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<mdv::DatumPoint> pointViz = new mdv::DatumPoint();
  
  osg::ref_ptr<osg::AutoTransform> autoTransform = new osg::AutoTransform;
  osg::ref_ptr<osg::PositionAttitudeTransform> sizeTransform = new osg::PositionAttitudeTransform;
  
  Stow(Feature &fIn)
  : feature(fIn)
  {
    QStringList tStrings =
    {
      QObject::tr("Constant")
      , QObject::tr("At Point")
      , QObject::tr("Edge Parameter")
      , QObject::tr("Face Parameters")
    };
    pointType.setEnumeration(tStrings);
    pointType.connect(syncObserver);
    pointType.connect(observer);
    feature.parameters.push_back(&pointType);
    
    origin.connect(observer);
    feature.parameters.push_back(&origin);
    
    size.connect(observer);
    feature.parameters.push_back(&size);
    
    parameterU.setConstraint(prm::Constraint::buildUnit());
    parameterU.connect(observer);
    feature.parameters.push_back(&parameterU);
    
    parameterV.setConstraint(prm::Constraint::buildUnit());
    parameterV.connect(observer);
    feature.parameters.push_back(&parameterV);
    
    picks.connect(observer);
    feature.parameters.push_back(&picks);
    
    pointTypeLabel->refresh();
    grid->addChild(pointTypeLabel);
    grid->addChild(originLabel);
    grid->addChild(sizeLabel);
    grid->addChild(parameterULabel);
    grid->addChild(parameterVLabel);
    feature.overlaySwitch->addChild(grid);
    
    autoTransform->setAutoScaleToScreen(true);
    autoTransform->addChild(sizeTransform);
    sizeTransform->addChild(pointViz);
    feature.getMainTransform()->addChild(autoTransform);
    
    prmActiveSync();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker blocker(observer); //no recursion.
    auto typeIndex = static_cast<PointType>(pointType.getInt());
    switch (typeIndex)
    {
      case PointType::Constant:
      {
        origin.setActive(true);
        picks.setActive(false);
        parameterU.setActive(false);
        parameterV.setActive(false);
        break;
      }
      case PointType::AtPoint:
      {
        origin.setActive(false);
        picks.setActive(true);
        parameterU.setActive(false);
        parameterV.setActive(false);
        break;
      }
      case PointType::EdgeParameter:
      {
        origin.setActive(false);
        picks.setActive(true);
        parameterU.setActive(true);
        parameterV.setActive(false);
        break;
      }
      case PointType::FaceParameters:
      {
        origin.setActive(false);
        picks.setActive(true);
        parameterU.setActive(true);
        parameterV.setActive(true);
        break;
      }
    }
  }
  
  void updateVisual()
  {
    const osg::Vec3d &loc = origin.getVector();
    autoTransform->setPosition(loc);
    auto s = size.getDouble();
    sizeTransform->setScale(osg::Vec3d(s, s, s));
    grid->setPosition(loc);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("DatumPoint");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

PointType Feature::getCurrentPointType()
{
  return static_cast<PointType>(stow->pointType.getInt());
}

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  try
  {
    prm::ObserverBlocker blocker(stow->observer); //we set the location parameter, don't trigger dirty.
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    osg::Vec3d resultLocation = stow->origin.getVector();
    tls::Resolver pr(pIn);
    auto goResolve =[&]()
    {
      const auto &picks = stow->picks.getPicks();
      if (picks.empty()) throw std::runtime_error("No Picks");
      if (!pr.resolve(picks.front())) throw std::runtime_error("invalid pick resolution");
    };
    
    auto typeIndex = static_cast<PointType>(stow->pointType.getInt());
    switch (typeIndex)
    {
      case PointType::Constant:{break;} //do nothing.
      case PointType::AtPoint:
      {
        goResolve();
        auto points = pr.getPoints();
        if (points.empty()) throw std::runtime_error("No Points In Resolver");
        resultLocation = points.front();
        break;
      }
      case PointType::EdgeParameter:
      {
        goResolve();
        auto shapes = pr.getShapes(); if (shapes.empty()) throw std::runtime_error("No shapes In Resolver");
        if (shapes.front().ShapeType() == TopAbs_WIRE)
        {
          BRepAdaptor_CompCurve adapt(TopoDS::Wire(shapes.front()));
          double length = GCPnts_AbscissaPoint::Length(adapt);
          GCPnts_AbscissaPoint eval(adapt, length * stow->parameterU.getDouble(), adapt.FirstParameter());
          if (!eval.IsDone()) throw std::runtime_error("evaluator failed for wire");
          resultLocation = gu::toOsg(adapt.Value(eval.Parameter()));
        }
        else if (shapes.front().ShapeType() == TopAbs_EDGE)
        {
          BRepAdaptor_Curve adapt(TopoDS::Edge(shapes.front()));
          double length = GCPnts_AbscissaPoint::Length(adapt);
          GCPnts_AbscissaPoint eval(adapt, length * stow->parameterU.getDouble(), adapt.FirstParameter());
          if (!eval.IsDone()) throw std::runtime_error("evaluator failed for wire");
          resultLocation = gu::toOsg(adapt.Value(eval.Parameter()));
        }
        else throw std::runtime_error("Unsupported shape for edge parameter");
        break;
      }
      case PointType::FaceParameters:
      {
        goResolve();
        auto shapes = pr.getShapes(); if (shapes.empty()) throw std::runtime_error("No shapes In Resolver");
        if (shapes.front().ShapeType() == TopAbs_FACE)
        {
          const auto &f = TopoDS::Face(shapes.front());
          auto [localU, localV] = occt::deNormalize(f, stow->parameterU.getDouble(), stow->parameterV.getDouble());
          BRepAdaptor_Surface adapt(f);
          resultLocation = gu::toOsg(adapt.Value(localU, localV));
        }
        else throw std::runtime_error("Unsupported shape for face parameters");
        break;
      }
    }
    
    stow->origin.setValue(resultLocation);
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::updateVisual()
{
  stow->updateVisual();
  setVisualClean();
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::dtps::DatumPoint so
  (
    Base::serialOut()
    , stow->pointType.serialOut()
    , stow->origin.serialOut()
    , stow->size.serialOut()
    , stow->parameterU.serialOut()
    , stow->parameterV.serialOut()
    , stow->picks.serialOut()
    , stow->pointTypeLabel->serialOut()
    , stow->originLabel->serialOut()
    , stow->sizeLabel->serialOut()
    , stow->parameterULabel->serialOut()
    , stow->parameterVLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::dtps::datumpoint(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::dtps::DatumPoint &so)
{
  Base::serialIn(so.base());
  stow->pointType.serialIn(so.pointType());
  stow->origin.serialIn(so.origin());
  stow->size.serialIn(so.size());
  stow->parameterU.serialIn(so.parameterU());
  stow->parameterV.serialIn(so.parameterV());
  stow->picks.serialIn(so.picks());
  stow->pointTypeLabel->serialIn(so.pointTypeLabel());
  stow->originLabel->serialIn(so.originLabel());
  stow->sizeLabel->serialIn(so.sizeLabel());
  stow->parameterULabel->serialIn(so.parameterULabel());
  stow->parameterVLabel->serialIn(so.parameterVLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  stow->updateVisual();
}

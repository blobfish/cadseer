/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <BRepAdaptor_Curve.hxx>
#include <TopoDS.hxx>

#include <osg/Switch>
#include <osg/PositionAttitudeTransform>

#include "globalutilities.h"
#include "annex/anncsysdragger.h"
#include "library/lbrcsysdragger.h"
#include "library/lbrplabel.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/tlsosgtools.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrldtmsdatumsystem.h"
#include "modelviz/mdvdatumsystem.h"
#include "feature/ftrdatumsystem.h"

using boost::uuids::uuid;

using namespace ftr::DatumSystem;

QIcon Feature::icon = QIcon(":/resources/images/datumSystem.svg");

struct Feature::Stow
{
  Feature &feature;
  
  prm::Parameter systemType{QObject::tr("Type"), 0, Tags::SystemType};
  prm::Parameter csys{prm::Names::CSys, osg::Matrixd::identity(), prm::Tags::CSys};
  prm::Parameter autoSize{prm::Names::AutoSize, false, prm::Tags::AutoSize};
  prm::Parameter size{prm::Names::Size, 10.0, prm::Tags::Size};
  prm::Parameter linkedPicks{QObject::tr("Linked"), ftr::Picks(), Tags::Linked};
  prm::Parameter pointsPicks{QObject::tr("Points"), ftr::Picks(), Tags::Points};
  prm::Parameter shapeInferPicks{QObject::tr("Shape Infer"), ftr::Picks(), Tags::ShapeInfer};
  prm::Parameter originPick{prm::Names::Origin, ftr::Picks(), prm::Tags::Origin};
  prm::Parameter axis0Picks{QObject::tr("Axis 1"), ftr::Picks(), Tags::Axis0};
  prm::Parameter axis1Picks{QObject::tr("Axis 2"), ftr::Picks(), Tags::Axis1};
  prm::Parameter axisStyle{QObject::tr("Axis Style"), 0, Tags::AxisStyle};
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer visualObserver{std::bind(&Feature::setVisualDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  ann::CSysDragger csysDragger{&feature, &csys};
  
  //unique ptr allows vector to grow and shrink without invalidating the pointers.
  std::vector<std::unique_ptr<prm::Parameter>> postOpParameters;
  
  osg::ref_ptr<mdv::DatumSystem> display{new mdv::DatumSystem()};
  osg::ref_ptr<lbr::PLabel> autoSizeLabel{new lbr::PLabel(&autoSize)};
  osg::ref_ptr<lbr::PLabel> sizeLabel{new lbr::PLabel(&size)};
  osg::ref_ptr<osg::PositionAttitudeTransform> scale{new osg::PositionAttitudeTransform()};
  double cachedSize;
  
  Stow(Feature &fIn) : feature(fIn)
  {
    auto initPrm = [&](prm::Parameter &prmIn)
    {
      prmIn.connect(dirtyObserver);
      feature.parameters.push_back(&prmIn);
    };
    
    QStringList tStrings = //keep in sync with enum in header.
    {
      QObject::tr("Constant")
      , QObject::tr("Linked")
      , QObject::tr("Points")
      , QObject::tr("Shape Infer")
      , QObject::tr("Define")
    };
    systemType.setEnumeration(tStrings);
    systemType.connect(syncObserver);
    autoSize.connect(syncObserver);
    initPrm(systemType);
    
    QStringList styleStrings =
    {
      QObject::tr("XY")
      , QObject::tr("XZ")
      , QObject::tr("YZ")
      , QObject::tr("YX")
      , QObject::tr("ZX")
      , QObject::tr("ZY")
    };
    axisStyle.setEnumeration(styleStrings);
    
    initPrm(autoSize);
    size.connect(visualObserver); feature.parameters.push_back(&size);
    initPrm(csys);
    initPrm(linkedPicks);
    initPrm(pointsPicks);
    initPrm(shapeInferPicks);
    initPrm(originPick);
    initPrm(axisStyle);
    initPrm(axis0Picks);
    initPrm(axis1Picks);
    
    csysDragger.dragger->hide(lbr::CSysDragger::SwitchIndexes::LinkIcon);
    feature.annexes.insert(std::make_pair(ann::Type::CSysDragger, &csysDragger));
    feature.overlaySwitch->addChild(csysDragger.dragger);
    
    double temp = size.getDouble();
    scale->setScale(osg::Vec3d(temp, temp, temp));
    scale->addChild(display.get());
    feature.mainTransform->addChild(scale.get());
    
    feature.overlaySwitch->addChild(autoSizeLabel);
    feature.overlaySwitch->addChild(sizeLabel);
    
    cachedSize = size.getDouble();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker syncBlocker(syncObserver);
    
    auto st = static_cast<SystemType>(systemType.getInt());
    switch(st)
    {
      case Constant:{
        csys.setActive(true);
        autoSize.setValue(false);
        autoSize.setActive(false);
        size.setActive(true);
        linkedPicks.setActive(false);
        pointsPicks.setActive(false);
        shapeInferPicks.setActive(false);
        originPick.setActive(false);
        axis0Picks.setActive(false);
        axis1Picks.setActive(false);
        axisStyle.setActive(false);
        break;}
      case Linked:{
        csys.setActive(false);
        autoSize.setValue(false);
        autoSize.setActive(false);
        size.setActive(true);
        linkedPicks.setActive(true);
        pointsPicks.setActive(false);
        shapeInferPicks.setActive(false);
        originPick.setActive(false);
        axis0Picks.setActive(false);
        axis1Picks.setActive(false);
        axisStyle.setActive(false);
        break;}
      case Through3Points:{
        csys.setActive(false);
        autoSize.setActive(true);
        size.setActive(!autoSize.getBool());
        linkedPicks.setActive(false);
        pointsPicks.setActive(true);
        shapeInferPicks.setActive(false);
        originPick.setActive(false);
        axis0Picks.setActive(false);
        axis1Picks.setActive(false);
        axisStyle.setActive(false);
        break;}
      case ShapeInfer:{
        csys.setActive(false);
        autoSize.setActive(true);
        size.setActive(!autoSize.getBool());
        linkedPicks.setActive(false);
        pointsPicks.setActive(false);
        shapeInferPicks.setActive(true);
        originPick.setActive(false);
        axis0Picks.setActive(false);
        axis1Picks.setActive(false);
        axisStyle.setActive(false);
        break;}
      case Define:{
        csys.setActive(false);
        autoSize.setActive(true);
        size.setActive(!autoSize.getBool());
        linkedPicks.setActive(false);
        pointsPicks.setActive(false);
        shapeInferPicks.setActive(false);
        originPick.setActive(true);
        axis0Picks.setActive(true);
        axis1Picks.setActive(true);
        axisStyle.setActive(true);
        break;}
    }
  }
  
  void updateLinked(const UpdatePayload &pli)
  {
    const auto &cp = linkedPicks.getPicks();
    if (cp.size() != 1)
      throw std::runtime_error("Wrong number of linked picks");
    
    tls::Resolver resolver(pli);
    if (!resolver.resolve(cp.front()))
      throw std::runtime_error("Couldn't resolve linked pick");
    
    auto csysPrms = resolver.getFeature()->getParameters(prm::Tags::CSys);
    if (csysPrms.empty())
      throw std::runtime_error("Linked feature has no csys parameter");
    const auto &cm = csysPrms.front()->getMatrix();
    
    csys.setValue(cm);
    csysDragger.resetDragger();
  }
  
  void update3Points(const UpdatePayload &pIn)
  {
    tls::Resolver pr(pIn);
    std::vector<osg::Vec3d> points;
    for (const auto &p : pointsPicks.getPicks())
    {
      if (!pr.resolve(p))
        throw std::runtime_error("Through3P: Pick resolution failed");
      auto tps = pr.getPoints();
      if (tps.empty())
        throw std::runtime_error("Through3P: No resolved points");
      if (tps.size() > 1)
      {
        std::ostringstream s; s << "WARNING: Through3P: more than one resolved point. Using first" << std::endl;
        feature.lastUpdateLog += s.str();
      }
      points.push_back(tps.front());
    }
    if (points.size() != 3)
      throw std::runtime_error("Through3P: couldn't get 3 points");
    std::array<std::optional<osg::Vec3d>, 4> oPoints;
    oPoints[0] = points[0];
    oPoints[1] = points[1];
    oPoints[2] = points[2];
    
    auto ocsys = tls::matrixFromPoints(oPoints);
    if (!ocsys) throw std::runtime_error("Through3P: couldn't derive matrix from 3 points");
    csys.setValue(*ocsys);
    csysDragger.resetDragger();
    
    osg::BoundingSphere bs;
    bs.expandBy(points.at(0));
    bs.expandBy(points.at(1));
    bs.expandBy(points.at(2));
    cachedSize = bs.radius();
  }
  
  void updateShapeInfer(const UpdatePayload &plIn)
  {
    const auto &thePicks = shapeInferPicks.getPicks();
    if (thePicks.empty()) throw std::runtime_error("No Picks to infer");
    
    tls::Resolver pr(plIn);
    if (!pr.resolve(thePicks.front())) throw std::runtime_error("Failed pick resolution in infer");
    auto shapes = pr.getShapes(false);
    if (shapes.empty()) throw std::runtime_error("No resolved shapes in infer");
    occt::BoundingBox bb(shapes.front());
    if (slc::isPointType(pr.getPick().selectionType))
    {
      if (shapes.front().ShapeType() != TopAbs_EDGE) throw std::runtime_error("No curve for frenet in infer");
      double baseU = pr.getPick().u;
      if (pr.getPick().selectionType == slc::Type::StartPoint) baseU = 0.0;
      if (pr.getPick().selectionType == slc::Type::EndPoint) baseU = 1.0;
      BRepAdaptor_Curve ca(TopoDS::Edge(shapes.front()));
      double p0 = ca.FirstParameter();
      double p1 = ca.LastParameter();
      double u = (p1 - p0) * baseU + p0;
      gp_Pnt origin;
      gp_Vec prime0;
      gp_Vec prime1;
      ca.D2(u, origin, prime0, prime1); //throws exception.
      auto biNormal = prime0 ^ prime1;
      prime0.Normalize();
      biNormal.Normalize();
      csys.setValue(gu::toOsg(gp_Ax2(origin, biNormal, prime0)));
      cachedSize = bb.getDiagonal() * 0.1;
    }
    else
    {
      auto oSystem = occt::gleanSystem(shapes.front());
      if (!oSystem) throw std::runtime_error("Unable to glean system in infer");
      csys.setValue(gu::toOsg(*oSystem));
      cachedSize = bb.getDiagonal() * 0.5;
    }
  }
  
  void updateDefine(const UpdatePayload &plIn)
  {
    tls::Resolver resolver(plIn);
    
    //origin
    osg::Vec3d newOrigin(0.0, 0.0, 0.0);
    const auto &pointPicks = originPick.getPicks();
    if (!pointPicks.empty() && resolver.resolve(pointPicks.front()))
    {
      if (resolver.getFeature()->getType() == ftr::Type::DatumPoint)
      {
        const auto *param = resolver.getFeature()->getParameter(prm::Tags::Origin);
        if (!param) throw std::runtime_error("No location parameter in datum point");
        newOrigin = param->getVector();
      }
      else
      {
        auto points = resolver.getPoints();
        if (!points.empty()) newOrigin = points.front();
      }
    }
    
    //rotation
    osg::Vec3d newX(1.0,0.0,0.0), newY(0.0,1.0,0.0), newZ(0.0,0.0,1.0);
    const auto &picks0 = axis0Picks.getPicks();
    const auto &picks1 = axis1Picks.getPicks();
    if (!picks0.empty() || !picks1.empty())
    {
      auto deriveVector = [&](const ftr::Picks &psIn) -> std::optional<osg::Vec3d>
      {
        std::vector<osg::Vec3d> points;
        occt::ShapeVector shapes;
        prm::Parameter *datumAxisParameter = nullptr;
        for (const auto &p : psIn)
        {
          if (!resolver.resolve(p)) continue;
          auto tp = resolver.getPoints();
          points.insert(points.end(), tp.begin(), tp.end());
          auto ts = resolver.getShapes();
          shapes.insert(shapes.end(), ts.begin(), ts.end());
          if (resolver.getFeature()->getType() == ftr::Type::DatumAxis)
            datumAxisParameter = resolver.getFeature()->getParameter(prm::Tags::Direction);
        }
        if (datumAxisParameter) return datumAxisParameter->getVector();
        if (points.size() == 2) return points.front() - points.back();
        if (shapes.size() == 1)
        {
          auto ap = occt::gleanAxis(shapes.front());
          if (ap.second) return gu::toOsg(ap.first.Direction());
        }
        return std::nullopt;
      };
      
      //we don't use tls::matrixFromAxes because it doesn't allow to control which is master.
      //helper lambdas
      auto isBad = [](const std::optional<osg::Vec3d> &oAxis) -> bool
      {
        return !oAxis || !oAxis->valid() || oAxis->length() < std::numeric_limits<float>::epsilon();
      };
      
      auto areParallel = [](const osg::Vec3d &v0, const osg::Vec3d &v1) -> bool
      {
        if ((1 - std::fabs(v0 * v1)) < std::numeric_limits<float>::epsilon()) return true;
        return false;
      };
      auto vec0 = deriveVector(picks0);
      auto vec1 = deriveVector(picks1);
      if (isBad(vec0) || isBad(vec1)) throw std::runtime_error("Bad axis vectors");
      vec0->normalize();
      vec1->normalize();
      if (areParallel(*vec0, *vec1)) throw std::runtime_error("Parallel axis vectors");
      switch (axisStyle.getInt())
      {
        case 0: //XY
        {
          newX = *vec0;
          newZ = *vec0 ^ *vec1; newZ.normalize();
          newY = newZ ^ newX; newY.normalize();
          break;
        }
        case 1: //XZ
        {
          newX = *vec0;
          newY = *vec1 ^ newX; newY.normalize();
          newZ = newX ^ newY; newZ.normalize();
          break;
        }
        case 2: //YZ
        {
          newY = *vec0;
          newX = newY ^ *vec1; newX.normalize();
          newZ = newX ^ newY; newZ.normalize();
          break;
        }
        case 3: //YX
        {
          newY = *vec0;
          newZ = *vec1 ^ newY; newZ.normalize();
          newX = newY ^ newZ; newX.normalize();
          break;
        }
        case 4: //ZX
        {
          newZ = *vec0;
          newY = newZ ^ *vec1; newY.normalize();
          newX = newY ^ newZ; newX.normalize();
          break;
        }
        case 5: //ZY
        {
          newZ = *vec0;
          newX = *vec1 ^ newZ; newX.normalize();
          newY = newZ ^ newX; newY.normalize();
          break;
        }
        default: // unknown
        {
          throw std::runtime_error("Unknown Axis Style");
          break;
        }
      }
    }
    osg::Matrixd fm;
    fm(0,0) = newX.x(); fm(0,1) = newX.y(); fm(0,2) = newX.z();
    fm(1,0) = newY.x(); fm(1,1) = newY.y(); fm(1,2) = newY.z();
    fm(2,0) = newZ.x(); fm(2,1) = newZ.y(); fm(2,2) = newZ.z();
    fm.setTrans(newOrigin);
    csys.setValue(fm);
  }
  
  void applyPostOp()
  {
    auto workMatrix = csys.getMatrix();
    for (const auto &op : postOpParameters)
    {
      if (op->getValueType() == typeid(osg::Vec3d)) workMatrix.setTrans(op->getVector() * workMatrix);
      else if (op->getValueType() == typeid(osg::Quat)) workMatrix.setRotate(op->getQuat() * workMatrix.getRotate());
    }
    csys.setValue(workMatrix);
  }
  
  void updateVisual()
  {
    if (autoSize.getBool())
    {
      prm::ObserverBlocker blocker(visualObserver);
      size.setValue(cachedSize);
    }
    
    const osg::Matrixd &cs = csys.getMatrix(); //current system
    feature.mainTransform->setMatrix(cs);
    
    double ts = size.getDouble();
    scale->setScale(osg::Vec3d(ts, ts, ts));
    
    double lo = ts * 1.2; //label offset
    osg::Vec3d asll = osg::Vec3d (lo, 0, 0) * cs;
    autoSizeLabel->setMatrix(osg::Matrixd::translate(asll));
    
    osg::Vec3d sll = osg::Vec3d (0, lo, 0) * cs;
    sizeLabel->setMatrix(osg::Matrixd::translate(sll));
  }
};


Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Datum System");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
  
  stow->prmActiveSync();
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  try
  {
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    auto sysType = static_cast<SystemType>(stow->systemType.getInt());
    switch (sysType)
    {
      case Constant:{
        stow->csysDragger.resetDragger();
        break;}
      case Linked:{
        stow->updateLinked(pIn);
        break;}
      case Through3Points:{
        stow->update3Points(pIn);
        break;}
      case ShapeInfer:{
        stow->updateShapeInfer(pIn);
        break;}
      case Define:{
        stow->updateDefine(pIn);
        break;}
    }
    if (!stow->postOpParameters.empty()) stow->applyPostOp();
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::updateVisual()
{
  stow->updateVisual();
  setVisualClean();
}

//note we are not adding these parameters to feature vector.
prm::Parameter* Feature::createPostOpTranslation()
{
  auto &r = stow->postOpParameters.emplace_back
    (std::make_unique<prm::Parameter>(QObject::tr("Translation"), osg::Vec3d(), Tags::Translation));
  r->connect(stow->dirtyObserver);
  parameters.push_back(r.get());
  this->setModelDirty();
  return stow->postOpParameters.back().get();
}

prm::Parameter* Feature::createPostOpRotation()
{
  auto &r = stow->postOpParameters.emplace_back
    (std::make_unique<prm::Parameter>(QObject::tr("Rotation"), osg::Quat(), Tags::Rotation));
  r->connect(stow->dirtyObserver);
  parameters.push_back(r.get());
  this->setModelDirty();
  return stow->postOpParameters.back().get();
}

prm::Parameter* Feature::getPostOp(int index) const
{
  assert(index >= 0 && index < static_cast<int>(stow->postOpParameters.size()));
  return stow->postOpParameters.at(index).get();
}

void Feature::removePostOp(int index)
{
  assert(index >= 0 && index < static_cast<int>(stow->postOpParameters.size()));
  auto postOpIt = stow->postOpParameters.begin() + index;
  removeParameter(postOpIt->get());
  stow->postOpParameters.erase(postOpIt);
}

// up as in, up to front. not up in numerical index sense.
void Feature::moveUp(int index)
{
  assert(stow->postOpParameters.size() > 1);
  assert(index > 0 && index < static_cast<int>(stow->postOpParameters.size()));
  stow->postOpParameters.at(index).swap(stow->postOpParameters.at(index - 1));
  this->setModelDirty();
}

// down as in, down to back. not down in numerical index sense.
void Feature::moveDown(int index)
{
  assert(stow->postOpParameters.size() > 1);
  assert(index >= 0 && index < static_cast<int>(stow->postOpParameters.size() - 1));
  stow->postOpParameters.at(index).swap(stow->postOpParameters.at(index + 1));
  this->setModelDirty();
}

prm::Parameters Feature::getPostOps() const
{
  prm::Parameters out;
  for (const auto &op : stow->postOpParameters) out.push_back(op.get());
  return out;
}

int Feature::getPostOpSize() const
{
  return stow->postOpParameters.size();
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::dtms::DatumSystem so
  (
    Base::serialOut()
    , stow->systemType.serialOut()
    , stow->csys.serialOut()
    , stow->autoSize.serialOut()
    , stow->size.serialOut()
    , stow->csysDragger.serialOut()
    , stow->autoSizeLabel->serialOut()
    , stow->sizeLabel->serialOut()
    , stow->cachedSize
  );
  
  switch(static_cast<SystemType>(stow->systemType.getInt()))
  {
    case Constant:{break;} //nothing
    case Linked:{so.linkedPicks() = stow->linkedPicks.serialOut(); break;}
    case Through3Points:{so.pointsPicks() = stow->pointsPicks.serialOut(); break;}
    case ShapeInfer:{so.shapeInferPicks() = stow->shapeInferPicks.serialOut(); break;}
    case Define:
    {
      so.originPick() = stow->originPick.serialOut();
      so.axis0Picks() = stow->axis0Picks.serialOut();
      so.axis1Picks() = stow->axis1Picks.serialOut();
      so.axisStyle() = stow->axisStyle.serialOut();
      break;
    }
  }
  
  for (const auto &pop : stow->postOpParameters) so.postOps().push_back(pop->serialOut());
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::dtms::datumsystem(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::dtms::DatumSystem &so)
{
  Base::serialIn(so.base());
  stow->systemType.serialIn(so.systemType());
  stow->csys.serialIn(so.csys());
  stow->autoSize.serialIn(so.autoSize());
  stow->size.serialIn(so.size());
  stow->csysDragger.serialIn(so.csysDragger());
  stow->autoSizeLabel->serialIn(so.autoSizeLabel());
  stow->sizeLabel->serialIn(so.sizeLabel());
  stow->cachedSize = so.cachedSize();
  

  switch(static_cast<SystemType>(stow->systemType.getInt()))
  {
    case Constant:{break;} //nothing
    case Linked:{if (so.linkedPicks()) stow->linkedPicks.serialIn(so.linkedPicks().get()); break;}
    case Through3Points:{if (so.pointsPicks()) stow->pointsPicks.serialIn(so.pointsPicks().get()); break;}
    case ShapeInfer:{if (so.shapeInferPicks()) stow->shapeInferPicks.serialIn(so.shapeInferPicks().get()); break;}
    case Define:
    {
      if (so.originPick()) stow->originPick.serialIn(so.originPick().get());
      if (so.axis0Picks()) stow->axis0Picks.serialIn(so.axis0Picks().get());
      if (so.axis1Picks()) stow->axis1Picks.serialIn(so.axis1Picks().get());
      if (so.axisStyle()) stow->axisStyle.serialIn(so.axisStyle().get());
      break;
    }
  }
  
  for (const auto &pop : so.postOps())
  {
    auto &r = stow->postOpParameters.emplace_back(std::make_unique<prm::Parameter>(pop));
    r->connect(stow->dirtyObserver);
    parameters.push_back(r.get());
  }
  
  mainTransform->setMatrix(stow->csys.getMatrix());
}

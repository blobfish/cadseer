/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2017  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <optional>

#include <TopoDS.hxx>
#include <BRepLib.hxx>
#include <BRepTools_Quilt.hxx>
#include <BRepCheck_Analyzer.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "tools/occtools.h"
#include "annex/annseershape.h"
#include "feature/ftrshapecheck.h"
#include "project/serial/generated/prjsrlextsextract.h"
#include "feature/ftrupdatepayload.h"
#include "tools/featuretools.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "feature/ftrextract.h"

using namespace ftr::Extract;

using boost::uuids::uuid;

QIcon Feature::icon = QIcon(":/resources/images/extract.svg");

struct Feature::Stow
{
  Feature &feature;
  
  prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  prm::Parameter angle{prm::Names::Angle, 0.0, prm::Tags::Angle};
  prm::Parameter filter{QObject::tr("Filter"), 0, Tags::filter};
  prm::Parameter sew{QObject::tr("Sew"), false, Tags::sew};
  prm::Parameter checkShape{QObject::tr("Check Shape"), true, Tags::checkShape};
  
  ann::SeerShape sShape;
  
  osg::ref_ptr<lbr::PLabel> angleLabel{new lbr::PLabel(&angle)};
  osg::ref_ptr<lbr::PLabel> filterLabel{new lbr::PLabel(&filter)};
  osg::ref_ptr<lbr::PLabel> sewLabel{new lbr::PLabel(&sew)};
  osg::ref_ptr<lbr::PLabel> checkShapeLabel{new lbr::PLabel(&checkShape)};
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  
  Stow() = delete;
  Stow(Feature &fIn)
  : feature(fIn)
  {
    picks.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&picks);
    
    angle.connectValue(std::bind(&Feature::setModelDirty, &feature));
    prm::Interval interval
    (
      prm::Boundary(0.0, prm::Boundary::End::Closed)
      , prm::Boundary(180.0, prm::Boundary::End::Open)
    );
    prm::Constraint c;
    c.intervals.push_back(interval);
    angle.setConstraint(c);
    angle.setActive(false);
    feature.parameters.push_back(&angle);
    
    QStringList filterStrings =
    {
      QObject::tr("None")
      , QObject::tr("Valid Only")
      , QObject::tr("Invalid Only")
    };
    filter.setEnumeration(filterStrings);
    filter.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&filter);
    filterLabel->refresh();
    
    sew.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&sew);
    
    checkShape.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&checkShape);
    
    grid->addChild(angleLabel.get());
    grid->addChild(filterLabel.get());
    grid->addChild(sewLabel.get());
    grid->addChild(checkShapeLabel.get());
    feature.overlaySwitch->addChild(grid.get());
    
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Extract");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &payloadIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    //no new failure state.
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    std::vector<const Base*> parents = payloadIn.getFeatures(std::string());
    if (parents.size() != 1) throw std::runtime_error("Wrong number of parent features");
    
    /* we break all shapes into 3 groups.
     * shapes will be a straight copy with no post processing
     * attempt to quilt the faces into shells.
     * edges will be a straight copy with no post processing
     */
    tls::Resolver pr(payloadIn);
    occt::ShapeVector shapes;
    occt::ShapeVector faces;
    occt::ShapeVector edges;
    std::optional<osg::Matrixd> accrueLocation;
    
    auto addShape = [&](const TopoDS_Shape &shapeIn) -> bool
    {
      auto isShapeValid =[&]() -> bool
      {
        BRepCheck_Analyzer checker(shapeIn);
        return checker.IsValid();
      };
      switch(stow->filter.getInt())
      {
        case 0: {return true; break;}
        case 1: {if(isShapeValid()) return true; else return false; break;}
        case 2: {if(!isShapeValid()) return true; else return false; break;}
        default:{assert(0); throw std::runtime_error("Invalid Filter"); break;}
      }
    };
    
    auto collect =[&]()
    {
      for (const auto &s : pr.getShapes())
      {
        if (!accrueLocation)
        {
          occt::BoundingBox bb(s);
          accrueLocation = osg::Matrixd::translate(gu::toOsg(bb.getCenter()));
          stow->grid->setPosition(gu::toOsg(bb.getCenter()));
        }
        switch (pr.getPick().accrue)
        {
          case slc::Accrue::AllFaces:
          {
            for (const auto &f : pr.getSeerShape()->useGetChildrenOfType(s, TopAbs_FACE))
              if (addShape(f))
                faces.push_back(f);
            break;
          }
          case slc::Accrue::AllEdges:
          {
            for (const auto &e : pr.getSeerShape()->useGetChildrenOfType(s, TopAbs_EDGE))
              if (addShape(e))
                edges.push_back(e);
            break;
          }
          case slc::Accrue::Tangent:
          {
            //just tangent faces for now.
            if (s.ShapeType() == TopAbs_FACE)
            {
              for (const auto &f : occt::walkTangentFaces(pr.getSeerShape()->getRootOCCTShape(), TopoDS::Face(s), stow->angle.getDouble()))
                if (addShape(f))
                  faces.push_back(f);
            }
            else if (addShape(s))
            {
              if (s.ShapeType() == TopAbs_EDGE) edges.push_back(s);
              else shapes.push_back(s);
            }
            break;
          }
          case slc::Accrue::None:
          {
            if (addShape(s))
            {
              if (s.ShapeType() == TopAbs_FACE) faces.push_back(s);
              else if (s.ShapeType() == TopAbs_EDGE) edges.push_back(s);
              else shapes.push_back(s);
            }
            break;
          }
        }
      }
    };
    
    const ann::SeerShape *targetSeerShape = nullptr;
    for (const auto &p : stow->picks.getPicks())
    {
      if (!pr.resolve(p)) continue;
      if (targetSeerShape == nullptr && pr.getSeerShape() != nullptr) targetSeerShape = pr.getSeerShape();
      collect();
    }
    if (targetSeerShape == nullptr) throw std::runtime_error("Null seer shape");
    
    occt::ShapeVector outShapes = shapes;
    //apparently just throw all faces into the quilter and it will sort it out?
    if (faces.size() > 1 && stow->sew.getBool())
    {
      BRepTools_Quilt quilter;
      for (const auto &f : faces) quilter.Add(f);
      occt::ShellVector quilts = occt::ShapeVectorCast(TopoDS::Compound(quilter.Shells()));
      for (const auto &s : quilts)
      {
        auto solid = occt::buildSolid(s);
        if (solid) outShapes.push_back(*solid);
        else outShapes.push_back(s);
      }
    }
    else outShapes.insert(outShapes.end(), faces.begin(), faces.end());
    
    //not going to do anything to 'connect' edges.
    outShapes.insert(outShapes.end(), edges.begin(), edges.end());
    
    //make sure we don't make a compound containing compounds.
    occt::ShapeVector nonCompounds;
    for (const auto &s : outShapes)
    {
      occt::ShapeVector temp = occt::getNonCompounds(s);
      std::copy(temp.begin(), temp.end(), std::back_inserter(nonCompounds));
    }

    TopoDS_Compound out = static_cast<TopoDS_Compound>(occt::ShapeVectorCast(nonCompounds));
    if (out.IsNull()) throw std::runtime_error("null shape");
    
    if (stow->checkShape.getBool())
    {
      ShapeCheck check(out);
      if (!check.isValid()) throw std::runtime_error("shapeCheck failed");
    }
    
    //these seem to be taking care of it.
    stow->sShape.setOCCTShape(out, getId());
    stow->sShape.shapeMatch(*targetSeerShape);
    stow->sShape.uniqueTypeMatch(*targetSeerShape);
    stow->sShape.outerWireMatch(*targetSeerShape);
    stow->sShape.derivedMatch();
    stow->sShape.dumpNils("extract feature");
    stow->sShape.dumpDuplicates("extract feature");
    stow->sShape.ensureNoNils();
    stow->sShape.ensureNoDuplicates();
    stow->sShape.ensureEvolve();
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in extract update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in extract update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in extract update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::exts::Extract extractOut
  (
    Base::serialOut()
    , stow->picks.serialOut()
    , stow->angle.serialOut()
    , stow->filter.serialOut()
    , stow->sew.serialOut()
    , stow->sShape.serialOut()
    , stow->angleLabel->serialOut()
    , stow->filterLabel->serialOut()
    , stow->sewLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid.get())
  );
  
  extractOut.checkShape() = stow->checkShape.serialOut();
  extractOut.checkShapeLabel() = stow->checkShapeLabel->serialOut();
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::exts::extract(stream, extractOut, infoMap);
}

void Feature::serialRead(const prj::srl::exts::Extract &sExtractIn)
{
  Base::serialIn(sExtractIn.base());
  stow->picks.serialIn(sExtractIn.picks());
  stow->angle.serialIn(sExtractIn.angle());
  stow->filter.serialIn(sExtractIn.filter());
  stow->sew.serialIn(sExtractIn.sew());
  stow->sShape.serialIn(sExtractIn.seerShape());
  stow->angleLabel->serialIn(sExtractIn.angleLabel());
  stow->filterLabel->serialIn(sExtractIn.filterLabel());
  stow->sewLabel->serialIn(sExtractIn.sewLabel());
  
  if (sExtractIn.checkShape()) stow->checkShape.serialIn(sExtractIn.checkShape().get());
  if (sExtractIn.checkShapeLabel()) stow->checkShapeLabel->serialIn(sExtractIn.checkShapeLabel().get());
  
  lbr::PLabelGridCallback::serialIn(sExtractIn.gridLocation(), stow->grid.get());
}

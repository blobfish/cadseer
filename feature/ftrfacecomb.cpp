/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <osg/Switch>

#include <TopoDS.hxx>
#include <BRepAdaptor_Surface.hxx>
#include <BRepTools.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <GCE2d_MakeLine.hxx>
#include <BRepLib.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <GCPnts_QuasiUniformAbscissa.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <GeomLProp_SLProps.hxx>
#include <GeomLib.hxx>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "modelviz/mdvcurvecomb.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlfcbsfacecomb.h"
#include "feature/ftrfacecomb.h"

using boost::uuids::uuid;
using namespace ftr::FaceComb;
QIcon Feature::icon = QIcon(":/resources/images/faceComb.svg");

struct Feature::Stow
{
  Feature &feature;
  ann::SeerShape sShape;
  
  prm::Parameter autoRange{QObject::tr("Auto Range"), true, Tags::autoRange};
  prm::Parameter flip{QObject::tr("Flip"), false, Tags::flip};
  prm::Parameter umin{QObject::tr("U Min"), 0.0, Tags::umin};
  prm::Parameter umax{QObject::tr("U Max"), 1.0, Tags::umax};
  prm::Parameter vmin{QObject::tr("V Min"), 0.0, Tags::vmin};
  prm::Parameter vmax{QObject::tr("V Max"), 1.0, Tags::vmax};
  prm::Parameter ucount{QObject::tr("U Count"), 5, Tags::ucount};
  prm::Parameter vcount{QObject::tr("V Count"), 5, Tags::vcount};
  prm::Parameter upoints{QObject::tr("U Points"), 15, Tags::upoints};
  prm::Parameter vpoints{QObject::tr("V Points"), 15, Tags::vpoints};
  prm::Parameter scale{prm::Names::Scale, 1.0, prm::Tags::Scale};
  prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  prm::Observer rangeObserver{std::bind(&Stow::rangeSync, this)};
  
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> autoRangeLabel{new lbr::PLabel(&autoRange)};
  osg::ref_ptr<lbr::PLabel> flipLabel{new lbr::PLabel(&flip)};
  osg::ref_ptr<lbr::PLabel> uminLabel{new lbr::PLabel(&umin)};
  osg::ref_ptr<lbr::PLabel> umaxLabel{new lbr::PLabel(&umax)};
  osg::ref_ptr<lbr::PLabel> vminLabel{new lbr::PLabel(&vmin)};
  osg::ref_ptr<lbr::PLabel> vmaxLabel{new lbr::PLabel(&vmax)};
  osg::ref_ptr<lbr::PLabel> ucountLabel{new lbr::PLabel(&ucount)};
  osg::ref_ptr<lbr::PLabel> vcountLabel{new lbr::PLabel(&vcount)};
  osg::ref_ptr<lbr::PLabel> upointsLabel{new lbr::PLabel(&upoints)};
  osg::ref_ptr<lbr::PLabel> vpointsLabel{new lbr::PLabel(&vpoints)};
  osg::ref_ptr<lbr::PLabel> scaleLabel{new lbr::PLabel(&scale)};
  
  
  Stow(Feature &fIn) : feature(fIn)
  {
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    
    feature.parameters.push_back(&autoRange); autoRange.connect(dirtyObserver); autoRange.connect(syncObserver);
    feature.parameters.push_back(&flip); flip.connect(dirtyObserver);
    feature.parameters.push_back(&umin); umin.connect(dirtyObserver); umin.connect(rangeObserver);
    feature.parameters.push_back(&umax); umax.connect(dirtyObserver); umax.connect(rangeObserver);
    feature.parameters.push_back(&vmin); vmin.connect(dirtyObserver); vmin.connect(rangeObserver);
    feature.parameters.push_back(&vmax); vmax.connect(dirtyObserver); vmax.connect(rangeObserver);
    
    feature.parameters.push_back(&ucount); ucount.connect(dirtyObserver);
    feature.parameters.push_back(&vcount); vcount.connect(dirtyObserver);
    auto faceConstraint = prm::Constraint::buildZeroPositive();
    faceConstraint.intervals.front().upper.value = 100.0;
    ucount.setConstraint(faceConstraint);
    vcount.setConstraint(faceConstraint);
    
    feature.parameters.push_back(&upoints); upoints.connect(dirtyObserver);
    feature.parameters.push_back(&vpoints); vpoints.connect(dirtyObserver);
    auto edgeConstraint = prm::Constraint::buildUnit(); //just use unit to be a template.
    edgeConstraint.intervals.front().lower.value = 3.0;
    edgeConstraint.intervals.front().upper.value = 1000.0;
    upoints.setConstraint(edgeConstraint);
    vpoints.setConstraint(edgeConstraint);
    
    feature.parameters.push_back(&scale); scale.connect(dirtyObserver);
    auto scaleConstraint = prm::Constraint::buildUnit();
    scaleConstraint.intervals.front().lower.value = 0.01;
    scaleConstraint.intervals.front().upper.value = 1000.0;
    scale.setConstraint(scaleConstraint);
    
    feature.parameters.push_back(&picks); picks.connect(dirtyObserver);
    
    feature.overlaySwitch->addChild(grid);
    grid->addChild(autoRangeLabel);
    grid->addChild(flipLabel);
    grid->addChild(ucountLabel);
    grid->addChild(vcountLabel);
    grid->addChild(upointsLabel);
    grid->addChild(vpointsLabel);
    grid->addChild(scaleLabel);
    
    //going to position these on boundary
    feature.overlaySwitch->addChild(uminLabel);
    feature.overlaySwitch->addChild(umaxLabel);
    feature.overlaySwitch->addChild(vminLabel);
    feature.overlaySwitch->addChild(vmaxLabel);
    
    prmActiveSync();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    prm::ObserverBlocker rangeBlocker(rangeObserver);
    
    auto ar = autoRange.getBool();
    umin.setActive(!ar);
    umax.setActive(!ar);
    vmin.setActive(!ar);
    vmax.setActive(!ar);
  }
  
  //ensure that umin <= umax and vmin <= vmax
  void rangeSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    prm::ObserverBlocker rangeBlocker(rangeObserver);
    
    //always adjust the max value. A zero range is acceptable.
    if ((umax.getDouble() - umin.getDouble()) < Precision::Confusion()) umax.setValue(umin.getDouble());
    if ((vmax.getDouble() - vmin.getDouble()) < Precision::Confusion()) vmax.setValue(vmin.getDouble());
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("FaceComb");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    prm::ObserverBlocker rangeBlocker(stow->rangeObserver);
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    tls::Resolver pr(pIn);
    const auto &picks = stow->picks.getPicks();
    if (picks.empty()) throw std::runtime_error("empty picks");
    if (!pr.resolve(stow->picks.getPicks().front())) throw std::runtime_error("invalid pick resolution");
    if (!pr.getSeerShape() || pr.getSeerShape()->isNull()) throw std::runtime_error("invalid pick");
    
    std::optional<TopoDS_Face> theFace;
    switch (pr.getPick().selectionType)
    {
      case slc::Type::Face:
      {
        auto theShapes = pr.getShapes();
        if (theShapes.empty()) throw std::runtime_error("no pick shapes");
        if (theShapes.front().ShapeType() != TopAbs_FACE) {assert(0); throw std::runtime_error("incorrect type");}
        theFace = TopoDS::Face(theShapes.front());
        break;
      }
      case slc::Type::Object: case slc::Type::Feature:
      {
        const auto &ss = *pr.getSeerShape();
        auto faces = ss.useGetChildrenOfType(ss.getRootOCCTShape(), TopAbs_FACE);
        if (faces.empty()) throw std::runtime_error("No faces in feature");
        if (faces.front().ShapeType() != TopAbs_FACE) {assert(0); throw std::runtime_error("incorrect type");}
        theFace = TopoDS::Face(faces.front());
        break;
      }
      default:
      {
        throw std::runtime_error("unsupported selection type");
        break;
      }
    }
    if (!theFace) throw std::runtime_error("Unable to resolve face");

    BRepAdaptor_Surface sa(*theFace);
    if (stow->autoRange.getBool())
    {
      //this is probably susceptible to whole bounding box of spline poles etc..
      double fumin, fumax, fvmin, fvmax;
      BRepTools::UVBounds(*theFace, fumin, fumax, fvmin, fvmax);
      stow->umin.setValue(fumin);
      stow->umax.setValue(fumax);
      stow->vmin.setValue(fvmin);
      stow->vmax.setValue(fvmax);
    }
    
    //ensure our ranges are valid with surface.
    if (stow->umin.getDouble() < sa.FirstUParameter()) stow->umin.setValue(sa.FirstUParameter());
    if (stow->umax.getDouble() > sa.LastUParameter()) stow->umax.setValue(sa.LastUParameter());
    if (stow->vmin.getDouble() < sa.FirstVParameter()) stow->vmin.setValue(sa.FirstVParameter());
    if (stow->vmax.getDouble() > sa.LastVParameter()) stow->vmax.setValue(sa.LastVParameter());
    
    //just cache the values.
    double umin = stow->umin.getDouble();
    double umax = stow->umax.getDouble();
    int ucount = stow->ucount.getInt();
    double vmin = stow->vmin.getDouble();
    double vmax = stow->vmax.getDouble();
    int vcount = stow->vcount.getInt();
    
    //some sanity.
    if ((umax-umin) < Precision::Confusion()) ucount = 0;
    if ((vmax-vmin) < Precision::Confusion()) vcount = 0;
    if (ucount == 0 && vcount == 0) throw std::runtime_error("No Curves To Create");
    
    //we don't really care about ids
    stow->sShape.setOCCTShape(*theFace, getId());
    stow->sShape.ensureNoNils();
    stow->sShape.ensureEvolve();

    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::updateVisual()
{
  try
  {
    getMainTransform()->removeChildren(0, getMainTransform()->getNumChildren());
    if (stow->sShape.isNull()) throw std::runtime_error("Null Shape");
    auto faces = stow->sShape.useGetChildrenOfType(stow->sShape.getRootOCCTShape(), TopAbs_FACE);
    if (faces.empty()) throw std::runtime_error("No faces to comb");
    
    //data has been sanitized in model update, just go.
    TopoDS_Face theFace = TopoDS::Face(faces.front());
    BRepAdaptor_Surface sa(theFace);
    double directionScale = 1.0;
    if (stow->flip.getBool()) directionScale = -1.0;
    
    //just cache the values for brevity.
    double umin = stow->umin.getDouble();
    double umax = stow->umax.getDouble();
    double umid = (umax - umin) / 2.0 + umin;
    int ucount = stow->ucount.getInt();
    double vmin = stow->vmin.getDouble();
    double vmax = stow->vmax.getDouble();
    double vmid = (vmax - vmin) / 2.0 + vmin;
    int vcount = stow->vcount.getInt();
    
    //put labels into place.
    stow->uminLabel->setMatrix(osg::Matrixd::translate(gu::toOsg(sa.Value(umin, vmid))));
    stow->umaxLabel->setMatrix(osg::Matrixd::translate(gu::toOsg(sa.Value(umax, vmid))));
    stow->vminLabel->setMatrix(osg::Matrixd::translate(gu::toOsg(sa.Value(umid, vmin))));
    stow->vmaxLabel->setMatrix(osg::Matrixd::translate(gu::toOsg(sa.Value(umid, vmax))));
    stow->grid->setPosition(gu::toOsg(sa.Value(umid, vmid)));
    
    //I want to use the bounding box to set a consistent curve scale.
    occt::BoundingBox bb;
    bb.add(theFace);
    double curveScale = bb.getDiagonal();
    double overallScale = curveScale * stow->scale.getDouble();
    
    using opencascade::handle;
    handle<Geom_Surface> surfaceCopy = dynamic_cast<Geom_Surface*>(sa.Surface().Surface()->Copy().get());
    
    std::optional<TopoDS_Edge> currentEdge;
    handle<Geom2d_Line> pcurve;
    auto buildEdge = [&](double u0, double v0, double u1, double v1)
    {
      currentEdge.reset();
      pcurve.Nullify();
      gp_Pnt2d p0(u0, v0);
      gp_Pnt2d p1(u1, v1);
      double distance = p1.Distance(p0);
      if (distance < Precision::PConfusion()) return;
      GCE2d_MakeLine lm(p0, p1);
      if (!lm.IsDone()) return;
      pcurve = lm;
      BRepBuilderAPI_MakeEdge em(pcurve, surfaceCopy, 0.0, distance);
      if (!em.IsDone()) return;
      TopoDS_Edge out = em;
      if (!BRepLib::BuildCurve3d(out)) return;
      currentEdge = out;
    };
    
    std::vector<osg::Vec3d> curvePoints, projections;
    GeomLProp_SLProps props(surfaceCopy, 2, gp::Resolution());
    auto buildComb = [&](int count)
    {
      assert(currentEdge);
      assert(!pcurve.IsNull());
      curvePoints.clear();
      projections.clear();
      
      std::vector<osg::Vec3d> tempPoints, tempProjects;
      BRepAdaptor_Curve adapt(*currentEdge);
      //get points on curve
      GCPnts_QuasiUniformAbscissa step(adapt, count); //might change for separate counts for u and v
      if (!step.IsDone()) throw std::runtime_error("Abscissa Failed");
      
      for (auto index = 1; index <= step.NbPoints(); ++index) //1 based array
      {
        gp_Pnt2d uv = pcurve->Value(step.Parameter(index));
        props.SetParameters(uv.X(), uv.Y());
        tempPoints.push_back(gu::toOsg(props.Value().Transformed(sa.Trsf())));
        tempProjects.push_back(tempPoints.back()); //default to same point and change if good.
        if (!props.IsNormalDefined()) continue; //just skip
        osg::Vec3d norm = gu::toOsg(props.Normal().Transformed(sa.Trsf()));
        norm *= overallScale * props.MeanCurvature() * directionScale;
        tempProjects.back() += norm;
      }
      curvePoints = tempPoints;
      projections = tempProjects;
    };
    
    auto disperse = [](double p0, double p1, int c) -> std::vector<double>
    {
      std::vector<double> out;
      if (c < 1) return out;
      switch (c)
      {
        case 1: //put 1 in middle parameter range.
        {
          out.push_back((p1 - p0) / 2.0 + p0);
          break;
        }
        case 2: //put 1 at start and 1 at end.
        {
          out.push_back(p0);
          out.push_back(p1);
          break;
        }
        default:
        {
          int spaceCount = c - 1;
          double step = (p1 - p0) / spaceCount;
          if (step < Precision::PConfusion()) throw std::runtime_error("dispersal step too small");
          for (int i = 0; i < c; ++i) out.push_back(p0 + (i * step));
        }
      }
      return out;
    };
    
    auto addCurveComb = [&]()
    {
      osg::ref_ptr<osg::MatrixTransform> comb = mdv::buildCurveComb();
      getMainTransform()->addChild(comb);
      auto *cb = dynamic_cast<mdv::CurveCombCallback*>(comb->getUpdateCallback());
      cb->setPoints(curvePoints, projections);
    };
    
    auto vSteps = disperse(vmin, vmax, ucount);
    for (const auto &cv : vSteps)
    {
      buildEdge(umin, cv, umax, cv);
      if (!currentEdge) continue;
      buildComb(stow->vpoints.getInt());
      if (curvePoints.empty() || projections.empty() || (curvePoints.size() != projections.size())) continue;
      addCurveComb();
    }
    
    auto uSteps = disperse(umin, umax, vcount);
    for (const auto &cu : uSteps)
    {
      buildEdge(cu, vmin, cu, vmax);
      if (!currentEdge) continue;
      buildComb(stow->upoints.getInt());
      if (curvePoints.empty() || projections.empty() || (curvePoints.size() != projections.size())) continue;
      addCurveComb();
    }
    
    setVisualClean();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::fcbs::FaceComb so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->autoRange.serialOut()
    , stow->flip.serialOut()
    , stow->umin.serialOut()
    , stow->umax.serialOut()
    , stow->vmin.serialOut()
    , stow->vmax.serialOut()
    , stow->ucount.serialOut()
    , stow->vcount.serialOut()
    , stow->upoints.serialOut()
    , stow->vpoints.serialOut()
    , stow->scale.serialOut()
    , stow->picks.serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
    , stow->autoRangeLabel->serialOut()
    , stow->flipLabel->serialOut()
    , stow->uminLabel->serialOut()
    , stow->umaxLabel->serialOut()
    , stow->vminLabel->serialOut()
    , stow->vmaxLabel->serialOut()
    , stow->ucountLabel->serialOut()
    , stow->vcountLabel->serialOut()
    , stow->upointsLabel->serialOut()
    , stow->vpointsLabel->serialOut()
    , stow->scaleLabel->serialOut()
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::fcbs::facecomb(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::fcbs::FaceComb &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.seerShape());
  stow->autoRange.serialIn(so.autoRange());
  stow->flip.serialIn(so.flip());
  stow->umin.serialIn(so.umin());
  stow->umax.serialIn(so.umax());
  stow->vmin.serialIn(so.vmin());
  stow->vmax.serialIn(so.vmax());
  stow->ucount.serialIn(so.ucount());
  stow->vcount.serialIn(so.vcount());
  stow->upoints.serialIn(so.upoints());
  stow->vpoints.serialIn(so.vpoints());
  stow->scale.serialIn(so.scale());
  stow->picks.serialIn(so.picks());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  stow->autoRangeLabel->serialIn(so.autoRangeLabel());
  stow->flipLabel->serialIn(so.flipLabel());
  stow->uminLabel->serialIn(so.uminLabel());
  stow->umaxLabel->serialIn(so.umaxLabel());
  stow->vminLabel->serialIn(so.vminLabel());
  stow->vmaxLabel->serialIn(so.vmaxLabel());
  stow->ucountLabel->serialIn(so.ucountLabel());
  stow->vcountLabel->serialIn(so.vcountLabel());
  stow->upointsLabel->serialIn(so.upointsLabel());
  stow->vpointsLabel->serialIn(so.vpointsLabel());
  stow->scaleLabel->serialIn(so.scaleLabel());
}

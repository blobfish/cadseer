/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2025 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <osg/Switch>

#include <BRepAdaptor_Curve.hxx>
#include <BRepAdaptor_CompCurve.hxx>
#include <TopoDS.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <TopExp.hxx>
#include <BRep_Tool.hxx>

#include "occ_gordon.h"

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/idtools.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlgrdsgordon.h"
#include "feature/ftrgordon.h"


namespace
{
  //https://dev.opencascade.org/content/how-find-continuity-wire
  //not sure how much this custom adaptor is helpful?
  class WireAdaptor : public BRepAdaptor_CompCurve
  {
    mutable std::optional<GeomAbs_Shape> shape;
    TopoDS_Wire theWire;
  public:
    WireAdaptor(const TopoDS_Wire &wIn) : BRepAdaptor_CompCurve(wIn), theWire(wIn){};
    GeomAbs_Shape Continuity() const override
    {
      if (shape) return *shape;
      shape = GeomAbs_C2; //c2 is max for AdvApprox;
      TopTools_IndexedMapOfShape edgeMap;
      TopExp::MapShapes(theWire, TopAbs_EDGE, edgeMap);
      for (int index = 1; index <= edgeMap.Extent(); ++index)
      {
        BRepAdaptor_Curve ca(TopoDS::Edge(edgeMap(index)));
        shape = std::min(ca.Continuity(), *shape);
      }
      if (*shape == GeomAbs_C0) return *shape; //no sense going on.

      //now we will look at continuity between edges.
      //we are assuming the wire has one connected sequence
      TopTools_IndexedDataMapOfShapeListOfShape vertexToEdgeMap;
      TopExp::MapShapesAndAncestors(theWire, TopAbs_VERTEX, TopAbs_EDGE, vertexToEdgeMap);
      for (int index = 1; index <= vertexToEdgeMap.Extent(); ++index)
      {
        auto vertex = TopoDS::Vertex(vertexToEdgeMap.FindKey(index));
        auto edgeList = vertexToEdgeMap(index);
        if (edgeList.Extent() != 2) continue;
        BRepAdaptor_Curve ca0(TopoDS::Edge(edgeList.First()));
        BRepAdaptor_Curve ca1(TopoDS::Edge(edgeList.Last()));
        double p0, p1;
        if (!BRep_Tool::Parameter(vertex, ca0.Edge(), p0)) continue;
        if (!BRep_Tool::Parameter(vertex, ca1.Edge(), p1)) continue;
        GeomAbs_Shape curveMin = std::min(ca0.Continuity(), ca1.Continuity());

        auto isContinous = [&](int d) -> bool
        {
          auto curve0d = ca0.DN(p0, d);
          if (ca0.Edge().Orientation() == TopAbs_REVERSED) curve0d.Reverse();
          auto curve1d = ca1.DN(p1, d);
          if (ca1.Edge().Orientation() == TopAbs_REVERSED) curve1d.Reverse();
          return curve0d.IsParallel(curve1d, Precision::Angular())
            && !curve0d.IsOpposite(curve1d, Precision::Angular());
        };
        GeomAbs_Shape vertexShape = GeomAbs_C0;
        if (curveMin >= GeomAbs_C1 && isContinous(1))
        {
          vertexShape = GeomAbs_C1;
          if (curveMin >= GeomAbs_C2 && isContinous(2))
            vertexShape = GeomAbs_C2;
        }
        shape = std::min(*shape, vertexShape);
      }
      return *shape;
    }
  };
}

using boost::uuids::uuid;
using namespace ftr::Gordon;
QIcon Feature::icon = QIcon(":/resources/images/gordon.svg");

struct Feature::Stow
{
  Feature &feature;
  ann::SeerShape sShape;
  
  prm::Parameter uPicks{QObject::tr("U Picks"), ftr::Picks(), Tags::uPicks};
  prm::Parameter vPicks{QObject::tr("V Picks"), ftr::Picks(), Tags::vPicks};
  prm::Parameter tolerance{prm::Names::Tolerance, 0.001, prm::Tags::Tolerance};
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1); //future proof
  osg::ref_ptr<lbr::PLabel> toleranceLabel{new lbr::PLabel(&tolerance)};

  uuid faceId{gu::createRandomId()};
  uuid wireId{gu::createRandomId()};
  std::vector<uuid> edgeIds;
  std::vector<uuid> vertexIds;
  
  Stow(Feature &fIn) : feature(fIn)
  {
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
//
    uPicks.connect(dirtyObserver); feature.parameters.push_back(&uPicks);
    vPicks.connect(dirtyObserver); feature.parameters.push_back(&vPicks);
    tolerance.connect(dirtyObserver); feature.parameters.push_back(&tolerance);

    tolerance.setConstraint(prm::Constraint::buildNonZeroPositive());

    grid->addChild(toleranceLabel);
    feature.overlaySwitch->addChild(grid);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Gordon");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    tls::Resolver pr(pIn);
    using Adaptors = std::vector<opencascade::handle<Adaptor3d_Curve>>;
    auto loadShapes = [&pr](const ftr::Picks &picksIn, Adaptors &adapts)
    {
      for (const auto &p : picksIn)
      {
        if (!pr.resolve(p)) continue;
        if (!pr.getSeerShape() || pr.getSeerShape()->isNull()) continue;
        const auto *ss = pr.getSeerShape();
        switch(p.selectionType)
        {
          case slc::Type::Object: case slc::Type::Feature:
          {
            auto wires = ss->useGetChildrenOfType(ss->getRootOCCTShape(), TopAbs_WIRE);
            for (const auto &w : wires) adapts.emplace_back(new WireAdaptor(TopoDS::Wire(w)));
            if (!adapts.empty()) continue;
            auto edges = ss->useGetChildrenOfType(ss->getRootOCCTShape(), TopAbs_EDGE);
            for (const auto &e : edges) adapts.emplace_back(new BRepAdaptor_Curve(TopoDS::Edge(e)));
            break;
          }
          case slc::Type::Edge:
          {
            for (const auto &s : pr.getShapes()) adapts.emplace_back(new BRepAdaptor_Curve(TopoDS::Edge(s)));
            break;
          }
          case slc::Type::Wire:
          {
            for (const auto &s : pr.getShapes()) adapts.emplace_back(new WireAdaptor(TopoDS::Wire(s)));
            break;
          }
          default: {continue; break;}
        }
      }
    };
    Adaptors uAdapts, vAdapts;
    loadShapes(stow->uPicks.getPicks(), uAdapts);
    loadShapes(stow->vPicks.getPicks(), vAdapts);
    if (uAdapts.size() < 2 || vAdapts.size() < 2) throw std::runtime_error("Not enough shapes");

    auto surface = occ_gordon::interpolate_curve_network(uAdapts, vAdapts, stow->tolerance.getDouble());
    if (surface.IsNull()) throw std::runtime_error("Null Surface");
    BRepBuilderAPI_MakeFace faceMaker(surface, stow->tolerance.getDouble());
    if (!faceMaker.IsDone() || faceMaker.Face().IsNull()) throw std::runtime_error("Failed to make face");

    auto face = faceMaker.Face();
    ShapeCheck check(face);
    if (!check.isValid()) throw std::runtime_error("shapeCheck failed");

    auto &ss = stow->sShape;
    ss.setOCCTShape(face, getId());
    auto faces = ss.useGetChildrenOfType(ss.getRootOCCTShape(), TopAbs_FACE);
    if (faces.size() != 1) throw std::runtime_error("No faces in result");
    ss.updateId(faces.front(), stow->faceId);

    auto wires = ss.useGetChildrenOfType(ss.getRootOCCTShape(), TopAbs_WIRE);
    if (wires.size() != 1) throw std::runtime_error("No wires in result");
    ss.updateId(wires.front(), stow->wireId);

    auto edges = ss.useGetChildrenOfType(ss.getRootOCCTShape(), TopAbs_EDGE);
    while (stow->edgeIds.size() < edges.size()) stow->edgeIds.push_back(gu::createRandomId());
    for (std::size_t index = 0; index < edges.size(); ++index) ss.updateId(edges.at(index), stow->edgeIds.at(index));

    auto vertices = ss.useGetChildrenOfType(ss.getRootOCCTShape(), TopAbs_VERTEX);
    while (stow->vertexIds.size() < vertices.size()) stow->vertexIds.push_back(gu::createRandomId());
    for (std::size_t index = 0; index < vertices.size(); ++index) ss.updateId(vertices.at(index), stow->vertexIds.at(index));
    ss.ensureNoNils();
    ss.ensureNoDuplicates();
    ss.ensureEvolve();

    occt::BoundingBox bb(face);
    stow->grid->setPosition(gu::toOsg(bb.getCenter()));

    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::grds::Gordon so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->uPicks.serialOut()
    , stow->vPicks.serialOut()
    , stow->tolerance.serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
    , stow->toleranceLabel->serialOut()
    , gu::idToString(stow->faceId)
    , gu::idToString(stow->wireId)
  );
  for (const auto &id : stow->edgeIds) so.edgeIds().push_back(gu::idToString(id));
  for (const auto &id : stow->vertexIds) so.vertexIds().push_back(gu::idToString(id));

  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::grds::gordon(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::grds::Gordon &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.seerShape());
  stow->uPicks.serialIn(so.uPicks());
  stow->vPicks.serialIn(so.vPicks());
  stow->tolerance.serialIn(so.tolerance());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  stow->toleranceLabel->serialIn(so.toleranceLabel());
  stow->faceId = gu::stringToId(so.faceId());
  stow->wireId = gu::stringToId(so.wireId());
  for (const auto &id : so.edgeIds()) stow->edgeIds.push_back(gu::stringToId(id));
  for (const auto &id : so.vertexIds()) stow->vertexIds.push_back(gu::stringToId(id));
}

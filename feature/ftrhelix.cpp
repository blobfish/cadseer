/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Geom2d_Line.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepLib.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "library/lbripgroup.h"
#include "library/lbrcsysdragger.h"
#include "library/lbrlineardimension.h"
#include "feature/ftrprimitive.h"
#include "parameter/prmparameter.h"
#include "tools/featuretools.h"
#include "tools/idtools.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlhlxshelix.h"
#include "feature/ftrhelix.h"

using boost::uuids::uuid;
using namespace ftr::Helix;
QIcon Feature::icon = QIcon(":/resources/images/helix.svg");

struct Feature::Stow
{
  Feature &feature;
  Primitive primitive;
  
  prm::Parameter helixType{QObject::tr("Helix Type"), 0, Tags::helixType};
  prm::Parameter turns{QObject::tr("Turns"), 1.0, Tags::turns};
  prm::Parameter pitch{QObject::tr("Pitch"), 1.0, Tags::pitch};
  
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  osg::ref_ptr<osg::MatrixTransform> gridTransform{new osg::MatrixTransform()}; //for dragger link
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> helixTypeLabel{new lbr::PLabel(&helixType)};
  osg::ref_ptr<lbr::PLabel> turnsLabel{new lbr::PLabel(&turns)};
  osg::ref_ptr<lbr::PLabel> pitchLabel{new lbr::PLabel(&pitch)};
  
  uuid wireId = gu::createRandomId();
  std::vector<uuid> edgeIds;
  
  Stow(Feature &fIn)
  : feature(fIn)
  , primitive(Primitive::Input{fIn, fIn.parameters, fIn.annexes})
  {
    QStringList helixTypeStrings =
    {
      QObject::tr("Turns & Height")
      , QObject::tr("Turns & Pitch")
      , QObject::tr("Pitch & Height")
    };
    helixType.setEnumeration(helixTypeStrings);
    helixType.connect(dirtyObserver);
    helixType.connect(syncObserver);
    feature.parameters.push_back(&helixType);
    
    primitive.addRadius(2.0);
    primitive.addHeight(10.0);
    turns.setConstraint(prm::Constraint::buildNonZeroPositive());
    pitch.setConstraint(prm::Constraint::buildNonZeroPositive());
    turns.connect(dirtyObserver); feature.parameters.push_back(&turns);
    pitch.connect(dirtyObserver); feature.parameters.push_back(&pitch);
    
    feature.overlaySwitch->addChild(gridTransform);
    gridTransform->addChild(grid);
    grid->addChild(helixTypeLabel);
    grid->addChild(turnsLabel);
    grid->addChild(pitchLabel);
    helixTypeLabel->refresh();
    
    primitive.csysDragger.dragger->linkToMatrix(gridTransform.get());
    
    primitive.radiusIP->setMatrixDims(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(0.0, 1.0, 0.0)));
    primitive.radiusIP->setMatrixDragger(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(-1.0, 0.0, 0.0)));
    primitive.radiusIP->setDimsFlipped(false);
    primitive.radiusIP->mainDim->setExtensionOffset(0.0);
    primitive.radiusIP->setRotationAxis(osg::Vec3d(0.0, 0.0, 1.0), osg::Vec3d(1.0, 0.0, 0.0));
    
    primitive.heightIP->setMatrixDims(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(1.0, 0.0, 0.0)));
    primitive.heightIP->setRotationAxis(osg::Vec3d(0.0, 0.0, 1.0), osg::Vec3d(0.0, 1.0, 0.0));
    
    prmActiveSync();
  }
  
  void updateIPs()
  {
    primitive.IPsToCsys();
    
    //height of radius dragger
    static const auto rot = osg::Matrixd::rotate(osg::Quat(osg::PI_2, osg::Vec3d(-1.0, 0.0, 0.0)));
    double push = primitive.height->getDouble() * 0.1;
    auto trans = osg::Matrixd::translate(0.0, 0.0, -push);
    primitive.radiusIP->setMatrixDragger(rot * trans);
    primitive.radiusIP->mainDim->setSqueeze(push);
    
    primitive.heightIP->mainDim->setSqueeze(primitive.radius->getDouble());
    primitive.heightIP->mainDim->setExtensionOffset(primitive.radius->getDouble() * 0.25);
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    
    switch (helixType.getInt())
    {
      case 0: //turns and height
      {
        turns.setActive(true);
        primitive.height->setActive(true);
        pitch.setActive(false);
        break;
      }
      case 1: //turns and pitch
      {
        turns.setActive(true);
        primitive.height->setActive(false);
        pitch.setActive(true);
        break;
      }
      case 2: //pitch and height
      {
        turns.setActive(false);
        primitive.height->setActive(true);
        pitch.setActive(true);
        break;
      }
      default: {break;}
    }
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Helix");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->primitive.sShape.reset();
  try
  {
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    stow->primitive.csysLinkUpdate(pIn);
    
    //we will keep turns, height and pitch in sync to each other.
    //and we will keep local copies in sync with parameter values.
    double localTurns = stow->turns.getDouble();
    double localHeight = stow->primitive.height->getDouble();
    double localPitch = stow->pitch.getDouble();
    switch (stow->helixType.getInt())
    {
      case 0: //turns and height
      {
        localPitch = localHeight / localTurns;
        stow->pitch.setValue(localPitch);
        break;
      }
      case 1: //turns and pitch
      {
        localHeight = localTurns * localPitch;
        stow->primitive.height->setValue(localHeight);
        break;
      }
      case 2: //pitch and height
      {
        localTurns = localHeight / localPitch;
        stow->turns.setValue(localTurns);
        break;
      }
      default: {break;}
    }
    
    int wholeTurns = localTurns;
    double partialTurn = localTurns;
    if (wholeTurns > 0) partialTurn = std::fmod(localTurns, static_cast<double>(wholeTurns));
    
    //setup for building pCurve.
    gp_Vec2d proj(osg::PI * 2.0, localPitch);
    double dist = proj.Magnitude();
    gp_Dir2d direction(proj);
    opencascade::handle<Geom2d_Line> line = new Geom2d_Line(gp_Pnt2d(0.0, 0.0), direction);
    opencascade::handle<Geom_CylindricalSurface> cylinder = new Geom_CylindricalSurface(gp_Ax3(), stow->primitive.radius->getDouble());
    
    auto buildEdge = [&](double p0, double p1) -> TopoDS_Edge
    {
      BRepBuilderAPI_MakeEdge em(line, cylinder, p0, p1);
      if (!em.IsDone()) throw std::runtime_error("Couldn't build edge");
      if (!BRepLib::BuildCurve3d(em.Edge(), Precision::Confusion())) throw std::runtime_error("Couldn't build 3d curve");
      return em.Edge();
    };
    
    auto movedEdge = [&](const TopoDS_Edge &eIn, int index) -> TopoDS_Edge
    {
      gp_Trsf transform;
      transform.SetTranslation(gp_Vec(0.0, 0.0, static_cast<double>(index) * localPitch));
      TopLoc_Location location(transform);
      return TopoDS::Edge(eIn.Located(location));
    };
    
    BRepBuilderAPI_MakeWire wm;
    auto addEdge = [&](const TopoDS_Edge &eIn)
    {
        wm.Add(eIn);
        if (!wm.IsDone()) throw std::runtime_error("Couldn't build wire");
    };
    
    if (wholeTurns > 0)
    {
      TopoDS_Edge wholeTurn = buildEdge(0.0, dist);
      for (int index = 0; index < wholeTurns; ++index) addEdge(movedEdge(wholeTurn, index));
    }
    
    if (std::fabs(partialTurn) > Precision::Confusion())
    {
      auto partialProj = proj * partialTurn;
      double lastParameter = partialProj.Magnitude();
      if (lastParameter > Precision::PConfusion())
      {
        auto partialEdge = buildEdge(0.0, lastParameter);
        partialEdge = movedEdge(partialEdge, wholeTurns);
        addEdge(partialEdge);
      }
    }
    
    if (!wm.IsDone()) throw std::runtime_error("No wire constructed");
    auto wire = wm.Wire();
    
    //put shape into position.
    const auto &localMatrix = stow->primitive.csys.getMatrix();
    gp_Trsf transformation;
    transformation.SetTransformation(gp_Ax3(gu::toOcc(localMatrix)));
    transformation.Invert();
    TopLoc_Location location(transformation);
    wire.Location(location);
    
    ShapeCheck check(wire); if (!check.isValid()) throw std::runtime_error("shapeCheck failed");
    auto &sShape = stow->primitive.sShape; //convenience.
    sShape.setOCCTShape(wire, getId());
    sShape.updateId(wire, stow->wireId);
    auto edges = sShape.useGetChildrenOfType(sShape.getRootOCCTShape(), TopAbs_EDGE); //consistent order?
    while (stow->edgeIds.size() < edges.size()) stow->edgeIds.emplace_back(gu::createRandomId());
    for (std::size_t i = 0; i < edges.size(); ++i) sShape.updateId(edges.at(i), stow->edgeIds.at(i));
    sShape.derivedMatch();
    sShape.ensureNoNils();
    sShape.ensureNoDuplicates();
    sShape.ensureEvolve();
    
    stow->grid->setPosition(osg::Vec3d(0.0, 0.0, localHeight * 0.66) * localMatrix);
    stow->gridTransform->setMatrix(osg::Matrixd::identity());
    mainTransform->setMatrix(osg::Matrixd::identity());
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  stow->updateIPs();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  
  prj::srl::hlxs::Helix so
  (
    Base::serialOut()
    , stow->primitive.sShape.serialOut()
    , stow->primitive.csysType.serialOut()
    , stow->primitive.csys.serialOut()
    , stow->primitive.csysLinked.serialOut()
    , stow->helixType.serialOut()
    , stow->primitive.radius->serialOut()
    , stow->primitive.height->serialOut()
    , stow->turns.serialOut()
    , stow->pitch.serialOut()
    , stow->primitive.csysDragger.serialOut()
    , stow->primitive.radiusIP->serialOut()
    , stow->primitive.heightIP->serialOut()
    , stow->helixTypeLabel->serialOut()
    , stow->turnsLabel->serialOut()
    , stow->pitchLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
    , gu::idToString(stow->wireId)
  );
  for (const auto &eId : stow->edgeIds) so.edgeIds().push_back(gu::idToString(eId));
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::hlxs::helix(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::hlxs::Helix &so)
{
  Base::serialIn(so.base());
  stow->primitive.sShape.serialIn(so.seerShape());
  stow->helixType.serialIn(so.helixType());
  stow->primitive.csysType.serialIn(so.csysType());
  stow->primitive.csys.serialIn(so.csys());
  stow->primitive.csysLinked.serialIn(so.csysLinked());
  stow->primitive.radius->serialIn(so.radius());
  stow->primitive.height->serialIn(so.height());
  stow->turns.serialIn(so.turns());
  stow->pitch.serialIn(so.pitch());
  stow->primitive.csysDragger.serialIn(so.csysDragger());
  stow->primitive.radiusIP->serialIn(so.radiusIP());
  stow->primitive.heightIP->serialIn(so.heightIP());
  stow->helixTypeLabel->serialIn(so.helixTypeLabel());
  stow->turnsLabel->serialIn(so.turnsLabel());
  stow->pitchLabel->serialIn(so.pitchLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  stow->wireId = gu::stringToId(so.wireId());
  for (const auto &eId : so.edgeIds()) stow->edgeIds.emplace_back(gu::stringToId(eId));
}

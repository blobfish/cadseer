/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <gp_Ax3.hxx>
#include <gp_Trsf.hxx>
#include <TopLoc_Location.hxx>
#include <BRepBuilderAPI_Copy.hxx>
#include <ShapeUpgrade_RemoveLocations.hxx>

#include <osg/io_utils>

#include "project/serial/generated/prjsrlintsinert.h"
#include "globalutilities.h"
#include "tools/idtools.h"
#include "tools/occtools.h"
#include "library/lbrcsysdragger.h"
#include "annex/annseershape.h"
#include "annex/anncsysdragger.h"
#include "parameter/prmparameter.h"
#include "feature/ftrprimitive.h"
#include "feature/ftrupdatepayload.h"
#include "feature/ftrinert.h"

using namespace ftr::Inert;
using namespace boost::uuids;

QIcon Feature::icon = QIcon(":/resources/images/inert.svg");

struct Feature::Stow
{
  Feature &feature;
  Primitive primitive;
  osg::Matrixd cachedMatrix; //detect csys change
  std::vector<uuid> cachedOffetIds; //vector position to id.
  
  Stow() = delete;
  Stow(Feature &fIn)
  : feature(fIn)
  , primitive(Primitive::Input{fIn, fIn.parameters, fIn.annexes})
  {
  }
  
  void cacheIds()
  {
    //store a map of offset to id for restoration.
    cachedOffetIds.clear();
    occt::ShapeVector shapes = occt::mapShapes(primitive.sShape.getRootOCCTShape());
    for (const auto &s : shapes)
    {
      if (primitive.sShape.hasShape(s))
      {
        cachedOffetIds.push_back(primitive.sShape.findId(s));
      }
      else
      {
        std::ostringstream s; s << "WARNING: shape is not in seershape" << std::endl;
        feature.lastUpdateLog += s.str();
        cachedOffetIds.push_back(gu::createNilId()); //place holder will be skipped again.
      }
    }
  }
  
  void assignIds()
  {
    auto shapes = occt::mapShapes(primitive.sShape.getRootOCCTShape());
    
    if (shapes.size() != cachedOffetIds.size())
    {
      std::ostringstream s; s << "WARNING: shape count not equal to cached count" << std::endl;
      feature.lastUpdateLog += s.str();
      while (cachedOffetIds.size() < shapes.size()) cachedOffetIds.emplace_back(gu::createRandomId());
    }
    
    std::size_t count = 0;
    for (const auto &s : shapes)
    {
      if (primitive.sShape.hasShape(s))
        primitive.sShape.updateId(s, cachedOffetIds.at(count));
      else
        feature.lastUpdateLog += "WARNING: shape is not in moved shapeId container in\n";
      count++;
    }
  }
};

Feature::Feature(const TopoDS_Shape &shapeIn)
: Feature(shapeIn, osg::Matrixd::identity())
{}

Feature::Feature(const TopoDS_Shape &shapeIn, const osg::Matrixd &mIn)
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Inert");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
  
  stow->primitive.sShape.setOCCTShape(shapeIn, getId());
  stow->primitive.sShape.ensureNoNils();
  
  stow->primitive.csys.setValue(mIn);
  stow->primitive.csysDragger.resetDragger(); //set dragger to parameter.
  stow->cachedMatrix = mIn;
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &plIn)
{
  try
  {
    setFailure();
    lastUpdateLog.clear();
    
    stow->cacheIds();
    
    stow->primitive.csysLinkUpdate(plIn);
    const osg::Matrixd &prmMatrix = stow->primitive.csys.getMatrix();
    osg::Matrixd diff = osg::Matrixd::inverse(stow->cachedMatrix) * prmMatrix;
    
    TopoDS_Shape tempShape(stow->primitive.sShape.getRootOCCTShape());
    osg::Matrixd shapeMatrix = gu::toOsg(tempShape.Location().Transformation());
    osg::Matrixd freshMatrix = shapeMatrix * diff;
    //I have no idea why I have to 'inverse' here. I don't have to inverse when I pull the matrix out of the shape.
    gp_Ax3 tempAx3(gu::toOcc(osg::Matrixd::inverse(freshMatrix)));
    gp_Trsf tempTrsf;
    tempTrsf.SetTransformation(tempAx3);
    TopLoc_Location freshLocation(tempTrsf);
    tempShape.Location(freshLocation);
    stow->primitive.sShape.setOCCTShape(tempShape, getId());
    
    stow->assignIds();
    
    stow->primitive.sShape.dumpNils("inert");
    stow->primitive.sShape.ensureNoNils();
    
    stow->primitive.sShape.dumpDuplicates("inert");
    stow->primitive.sShape.ensureNoDuplicates();
    
    mainTransform->setMatrix(osg::Matrixd::identity());
    stow->cachedMatrix = stow->primitive.csys.getMatrix();
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in inert update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in inert update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in inert update." << std::endl;
    lastUpdateLog += s.str();
  }
  
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::updateAnchor()
{
  if (stow->primitive.csysType.getInt() != 0) return;
  if (!stow->primitive.csys.isConstant()) return;
  
  stow->cacheIds();
  
  const auto &dsys = stow->primitive.csysDragger.dragger->getMatrix();
  
  BRepBuilderAPI_Copy copier(stow->primitive.sShape.getRootOCCTShape());
  TopoDS_Shape tempShape(copier.Shape());
  osg::Matrixd shapeMatrix = gu::toOsg(tempShape.Location().Transformation());
  auto movedMatrix = dsys * osg::Matrixd::inverse(shapeMatrix);
  
  gp_Ax3 tempAx3(gu::toOcc(movedMatrix));
  gp_Trsf tempTrsf; tempTrsf.SetTransformation(tempAx3);
  TopLoc_Location freshLocation(tempTrsf);
  tempShape.Location(freshLocation);
  
  ShapeUpgrade_RemoveLocations rl;
  rl.SetRemoveLevel(TopAbs_COMPOUND);
  rl.Remove(tempShape);
  auto out = rl.ModifiedShape(tempShape);
  
  gp_Trsf trsf; trsf.SetTransformation(gp_Ax3(gu::toOcc(osg::Matrixd::inverse(dsys))));
  TopLoc_Location finishLocation(trsf);
  out.Location(finishLocation);
  stow->primitive.sShape.setOCCTShape(out, getId());
  
  stow->assignIds();
  stow->primitive.csys.setValue(dsys);
  stow->cachedMatrix = dsys;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::ints::Inert inertOut
  (
    Base::serialOut(),
    stow->primitive.sShape.serialOut(),
    stow->primitive.csys.serialOut(),
    stow->primitive.csysDragger.serialOut()
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::ints::inert(stream, inertOut, infoMap);
}

void Feature::serialRead(const prj::srl::ints::Inert& inert)
{
  Base::serialIn(inert.base());
  stow->primitive.sShape.serialIn(inert.seerShape());
  stow->primitive.csys.serialIn(inert.csys());
  stow->primitive.csysDragger.serialIn(inert.csysDragger());
  stow->cachedMatrix = stow->primitive.csys.getMatrix();
}

osg::Matrixd ftr::Inert::deriveSystem(const occt::ShapeVector &sIn)
{
  //determine the inert csys parameter.
  osg::Matrixd system = osg::Matrixd::identity();
  if (sIn.size() == 1) system = gu::toOsg(sIn.front().Location());
  else
  {
    //leave rotation at identity and set origin at bounding box center.
    occt::BoundingBox bb(sIn);
    system.setTrans(gu::toOsg(bb.getCenter()));
  }
  return system;
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gp_Ax3.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <BRepAdaptor_CompCurve.hxx>
#include <TopoDS.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "annex/anninstancemapper.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/tlsosgtools.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlinstinstance.h"
#include "feature/ftrinstance.h"

using boost::uuids::uuid;
using namespace ftr::Instance;
QIcon Feature::icon = QIcon(":/resources/images/instance.svg");

struct Feature::Stow
{
  Feature &feature;
  ann::SeerShape sShape;
  ann::InstanceMapper iMapper;
  std::set<int> fIndexes; //whitelist, blacklist
  occt::ShapeVector sourceShapes;
  std::vector<occt::ShapeVector> instancesList; //instances for each input shape.
  occt::ShapeVector allInstances; //all the instance shapes.
  
  //common
  prm::Parameter source{QObject::tr("Source"), ftr::Picks(), Tags::Source};
  prm::Parameter instanceType{QObject::tr("Instance Type"), 0, Tags::InstanceType};
  prm::Parameter filter{QObject::tr("Filter"), 0, Tags::Filter};
  prm::Parameter filterIndexes{QObject::tr("Filter Indexes"), std::string(), Tags::FilterIndexes};
  
  //linear
  prm::Parameter linearCSys{QObject::tr("CSys"), ftr::Picks(), Tags::LinearCSys};
  prm::Parameter linearXOffset{QObject::tr("X Offset"), 20.0, Tags::LinearXOffset};
  prm::Parameter linearYOffset{QObject::tr("Y Offset"), 20.0, Tags::LinearYOffset};
  prm::Parameter linearZOffset{QObject::tr("Z Offset"), 20.0, Tags::LinearZOffset};
  prm::Parameter linearXCount{QObject::tr("X Count"), 2,  Tags::LinearXCount};
  prm::Parameter linearYCount{QObject::tr("Y Count"), 2,  Tags::LinearYCount};
  prm::Parameter linearZCount{QObject::tr("Z Count"), 2,  Tags::LinearZCount};
  
  //polar
  prm::Parameter polarAxis{QObject::tr("Axis Pick"), ftr::Picks(), Tags::PolarAxis};
  prm::Parameter polarAngle{prm::Names::Angle, 360.0, prm::Tags::Angle};
  prm::Parameter polarCount{QObject::tr("Count"), 4, Tags::PolarCount};
  prm::Parameter polarInclusive{QObject::tr("Inclusive Angle"), true, Tags::PolarInclusive};
  
  //mirror
  prm::Parameter mirrorPlane{QObject::tr("Plane"), ftr::Picks(), Tags::MirrorPlane};
  
  //interpolate
  prm::Parameter interpolateCSys{QObject::tr("CSys"), ftr::Picks(), Tags::InterpolateCSys};
  prm::Parameter interpolateCount{QObject::tr("Count"), 3, Tags::InterpolateCount};
  prm::Parameter interpolateContact{QObject::tr("Contact"), false, Tags::InterpolateContact};
  prm::Parameter interpolateCorrection{QObject::tr("Correction"), false, Tags::InterpolateCorrection};
  
  //spine
  prm::Parameter spineSpine{QObject::tr("Spine"), ftr::Picks(), Tags::SpineSpine};
  prm::Parameter spineCount{QObject::tr("Count"), 3, Tags::SpineCount};
  prm::Parameter spineOrientation{QObject::tr("Orientation"), 0, Tags::SpineOrientation};
  prm::Parameter spineContact{QObject::tr("Contact"), false, Tags::SpineContact};
  prm::Parameter spineCorrection{QObject::tr("Correction"), false, Tags::SpineCorrection};
  
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  prm::Observer filterObserver{std::bind(&Stow::filterSync, this)};
  
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> instanceTypeLabel{new lbr::PLabel(&instanceType)};
  osg::ref_ptr<lbr::PLabel> filterLabel{new lbr::PLabel(&filter)};
  osg::ref_ptr<lbr::PLabel> filterIndexesLabel{new lbr::PLabel(&filterIndexes)};
  
  osg::ref_ptr<lbr::PLabel> linearXOffsetLabel{new lbr::PLabel(&linearXOffset)};
  osg::ref_ptr<lbr::PLabel> linearYOffsetLabel{new lbr::PLabel(&linearYOffset)};
  osg::ref_ptr<lbr::PLabel> linearZOffsetLabel{new lbr::PLabel(&linearZOffset)};
  osg::ref_ptr<lbr::PLabel> linearXCountLabel{new lbr::PLabel(&linearXCount)};
  osg::ref_ptr<lbr::PLabel> linearYCountLabel{new lbr::PLabel(&linearYCount)};
  osg::ref_ptr<lbr::PLabel> linearZCountLabel{new lbr::PLabel(&linearZCount)};
  
  osg::ref_ptr<lbr::PLabel> polarAngleLabel{new lbr::PLabel(&polarAngle)};
  osg::ref_ptr<lbr::PLabel> polarCountLabel{new lbr::PLabel(&polarCount)};
  osg::ref_ptr<lbr::PLabel> polarInclusiveLabel{new lbr::PLabel(&polarInclusive)};
  
  osg::ref_ptr<lbr::PLabel> interpolateCountLabel{new lbr::PLabel(&interpolateCount)};
  osg::ref_ptr<lbr::PLabel> interpolateContactLabel{new lbr::PLabel(&interpolateContact)};
  osg::ref_ptr<lbr::PLabel> interpolateCorrectionLabel{new lbr::PLabel(&interpolateCorrection)};
  
  osg::ref_ptr<lbr::PLabel> spineCountLabel{new lbr::PLabel(&spineCount)};
  osg::ref_ptr<lbr::PLabel> spineOrientationLabel{new lbr::PLabel(&spineOrientation)};
  osg::ref_ptr<lbr::PLabel> spineContactLabel{new lbr::PLabel(&spineContact)};
  osg::ref_ptr<lbr::PLabel> spineCorrectionLabel{new lbr::PLabel(&spineCorrection)};
  
  Stow(Feature &fIn) : feature(fIn)
  {
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    feature.annexes.insert(std::make_pair(ann::Type::InstanceMapper, &iMapper));
    
    auto commonConnect = [&](prm::Parameter &prmIn)
    {
      prmIn.connect(dirtyObserver);
      feature.parameters.push_back(&prmIn);
    };
    
    prm::Constraint countConstraint;
    prm::Boundary lower(2.0, prm::Boundary::End::Closed);
    prm::Boundary upper(std::numeric_limits<double>::max(), prm::Boundary::End::Closed);
    countConstraint.intervals.push_back(prm::Interval(lower, upper));
    
    commonConnect(source);
    
    QStringList instanceTypeStrings =
    {
      QObject::tr("Linear")
      , QObject::tr("Polar")
      , QObject::tr("Mirror")
      , QObject::tr("Interpolate")
      , QObject::tr("Spine")
    };
    instanceType.setEnumeration(instanceTypeStrings);
    commonConnect(instanceType);
    instanceType.connect(syncObserver);
    
    QStringList filterStrings = {QObject::tr("None"), QObject::tr("Black List"), QObject::tr("White List")};
    filter.setEnumeration(filterStrings);
    commonConnect(filter);
    filter.connect(filterObserver);
    commonConnect(filterIndexes);
    
    //we allow 1 for count on linear so we can eliminate an axis.
    linearXOffset.setConstraint(prm::Constraint::buildNonZero());
    linearYOffset.setConstraint(prm::Constraint::buildNonZero());
    linearZOffset.setConstraint(prm::Constraint::buildNonZero());
    linearXCount.setConstraint(prm::Constraint::buildNonZeroPositive());
    linearYCount.setConstraint(prm::Constraint::buildNonZeroPositive());
    linearZCount.setConstraint(prm::Constraint::buildNonZeroPositive());
    commonConnect(linearCSys);
    commonConnect(linearXOffset);
    commonConnect(linearYOffset);
    commonConnect(linearZOffset);
    commonConnect(linearXCount);
    commonConnect(linearYCount);
    commonConnect(linearZCount);
    
    polarAngle.setConstraint(prm::Constraint::buildNonZeroPositiveAngle());
    polarCount.setConstraint(countConstraint);
    commonConnect(polarAxis);
    commonConnect(polarAngle);
    commonConnect(polarCount);
    commonConnect(polarInclusive);
    
    commonConnect(mirrorPlane);
    
    interpolateCount.setConstraint(countConstraint);
    commonConnect(interpolateCSys);
    commonConnect(interpolateCount);
    commonConnect(interpolateContact);
    commonConnect(interpolateCorrection);
    
    spineCount.setConstraint(countConstraint);
    commonConnect(spineSpine);
    commonConnect(spineCount);
    QStringList orientationStrings = {QObject::tr("Foward"), QObject::tr("Reverse")};
    spineOrientation.setEnumeration(orientationStrings);
    commonConnect(spineOrientation);
    commonConnect(spineContact);
    commonConnect(spineCorrection);
    
    feature.overlaySwitch->addChild(grid);
    instanceTypeLabel->refresh(); grid->addChild(instanceTypeLabel);
    filterLabel->refresh(); grid->addChild(filterLabel);
    grid->addChild(filterIndexesLabel);
    grid->addChild(linearXOffsetLabel);
    grid->addChild(linearYOffsetLabel);
    grid->addChild(linearZOffsetLabel);
    grid->addChild(linearXCountLabel);
    grid->addChild(linearYCountLabel);
    grid->addChild(linearZCountLabel);
    grid->addChild(polarAngleLabel);
    grid->addChild(polarCountLabel);
    grid->addChild(polarInclusiveLabel);
    grid->addChild(interpolateCountLabel);
    grid->addChild(interpolateContactLabel);
    grid->addChild(interpolateCorrectionLabel);
    grid->addChild(spineCountLabel);
    spineOrientationLabel->refresh(); grid->addChild(spineOrientationLabel);
    grid->addChild(spineContactLabel);
    grid->addChild(spineCorrectionLabel);
    
    prmActiveSync();
    filterSync();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    
    auto setLinearActive = [&](bool state)
    {
      linearCSys.setActive(state);
      linearXOffset.setActive(state);
      linearYOffset.setActive(state);
      linearZOffset.setActive(state);
      linearXCount.setActive(state);
      linearYCount.setActive(state);
      linearZCount.setActive(state);
    };
    auto setPolarActive = [&](bool state)
    {
      polarAxis.setActive(state);
      polarAngle.setActive(state);
      polarCount.setActive(state);
      polarInclusive.setActive(state);
    };
    auto setMirrorActive = [&](bool state)
    {
      mirrorPlane.setActive(state);
    };
    auto setInterpolateActive = [&](bool state)
    {
      interpolateCSys.setActive(state);
      interpolateCount.setActive(state);
      interpolateContact.setActive(state);
      interpolateCorrection.setActive(state);
    };
    auto setSpineActive = [&](bool state)
    {
      spineSpine.setActive(state);
      spineCount.setActive(state);
      spineOrientation.setActive(state);
      spineContact.setActive(state);
      spineCorrection.setActive(state);
    };
    
    //set everything inactive and turn on current type.
    IType ct = static_cast<IType>(instanceType.getInt());
    setLinearActive(false);
    setPolarActive(false);
    setMirrorActive(false);
    setInterpolateActive(false);
    setSpineActive(false);
    switch (ct)
    {
      case Linear:{setLinearActive(true); break;}
      case Polar:{setPolarActive(true); break;}
      case Mirror:{setMirrorActive(true); break;}
      case Interpolate:{setInterpolateActive(true); break;}
      case Spine:{setSpineActive(true); break;}
    }
  }
  
  void filterSync()
  {
    if (filter.getInt() == 0) filterIndexes.setActive(false);
    else filterIndexes.setActive(true);
  }
  
  void initFilterIndexes()
  {
    fIndexes.clear();
    if (filter.getInt() == 0) return;
    const auto &filterString = filterIndexes.getString();
    if (!filterString.empty())
    {
      std::istringstream streamIn(filterString);
      std::string current;
      while (std::getline(streamIn, current, ';'))
      {
        if (current.empty()) continue;
        int currentIndex = -1;
        try {currentIndex = std::stoi(current);}
        catch(...) {continue;}
        fIndexes.insert(currentIndex);
      }
    }
  }
  
  bool shouldAdd(int theIndex)
  {
    switch (filter.getInt())
    {
      case 0: return true;
      case 1: return fIndexes.count(theIndex) == 0; //black list
      case 2: return fIndexes.count(theIndex) != 0; //white list
    }
    return true; //shouldn't get here.
  }
  
  void setSourceShapes(tls::Resolver &resolver)
  {
    sourceShapes.clear();
    const auto &sourcePicks = source.getPicks();
    if (sourcePicks.empty()) throw std::runtime_error("No input pick");
    if (!resolver.resolve(sourcePicks.front())) throw std::runtime_error("Invalid source resolution");
    sourceShapes = resolver.getShapes();
    if (sourceShapes.empty()) throw std::runtime_error("No source shapes");
    if (slc::isObjectType(resolver.getPick().selectionType))
      sourceShapes = occt::getNonCompounds(sourceShapes.front());
    if (sourceShapes.empty()) throw std::runtime_error("No source shapes");
  }
  
  void beginUpdate()
  {
    initFilterIndexes();
    instancesList.clear();
    allInstances.clear();
  }
  
  void endUpdate(const tls::Resolver &resolver, const ftr::ShapeHistory &shapeHistory)
  {
    assert(instancesList.size() == sourceShapes.size()); //a vector of shapes for every source shape.
    
    TopoDS_Compound result = occt::ShapeVectorCast(allInstances);
    ShapeCheck check(result);
    if (!check.isValid()) throw std::runtime_error("shapeCheck failed");
    sShape.setOCCTShape(result, feature.getId());
    
    for (std::size_t sIndex = 0; sIndex < sourceShapes.size(); ++sIndex)
    {
      iMapper.startMapping
      (
        *resolver.getSeerShape()
        , resolver.getSeerShape()->findId(sourceShapes.at(sIndex))
        , shapeHistory
      );
      std::size_t count = 0;
      for (const auto &si : instancesList.at(sIndex))
      {
        iMapper.mapIndex(sShape, si, count);
        count++;
      }
    }
  }
  
  void updateLinear(const UpdatePayload &plIn)
  {
    beginUpdate();
    tls::Resolver sourceResolver(plIn);
    setSourceShapes(sourceResolver);
    
    //get the matrix
    osg::Matrixd system = osg::Matrixd::identity();
    const auto &csysPicks = linearCSys.getPicks();
    if (!csysPicks.empty())
    {
      tls::Resolver csysResolver(plIn);
      if (!csysResolver.resolve(csysPicks.front())) throw std::runtime_error("Invalid csys resolution");
      if (slc::isObjectType(csysResolver.getPick().selectionType))
      {
        auto prms = csysResolver.getFeature()->getParameters(prm::Tags::CSys);
        if (prms.empty()) throw std::runtime_error("No csys parameter");
        system = prms.front()->getMatrix();
      }
      else
      {
        auto shapes = csysResolver.getShapes();
        if (shapes.empty()) throw std::runtime_error("No csys shapes");
        auto oSystem = occt::gleanSystem(shapes.front()); //only supporting 1 shape.
        if (!oSystem) throw std::runtime_error("Can't glean system from shape");
        system = gu::toOsg(*oSystem);
      }
    }
    
    osg::Vec3d xProjection = gu::getXVector(system) * linearXOffset.getDouble();
    osg::Vec3d yProjection = gu::getYVector(system) * linearYOffset.getDouble();
    osg::Vec3d zProjection = gu::getZVector(system) * linearZOffset.getDouble();
    for (const auto &tShape : sourceShapes)
    {
      int instanceCount = 0;
      osg::Matrixd shapeBase = gu::toOsg(tShape.Location().Transformation());
      osg::Vec3d baseTrans = shapeBase.getTrans();
      instancesList.emplace_back();
      for (int z = 0; z < linearZCount.getInt(); ++z)
      {
        for (int y = 0; y < linearYCount.getInt(); ++y)
        {
          for (int x = 0; x < linearXCount.getInt(); ++x, ++instanceCount)
          {
            if (!shouldAdd(instanceCount)) continue;
            osg::Vec3d no = baseTrans; //new origin
            no += zProjection * static_cast<double>(z);
            no += yProjection * static_cast<double>(y);
            no += xProjection * static_cast<double>(x);
            
            gp_Trsf nt = tShape.Location().Transformation(); //new Transformation
            nt.SetTranslationPart(gu::toOcc(no));
            TopLoc_Location loc(nt);
            
            instancesList.back().emplace_back(tShape.Located(loc));
            allInstances.emplace_back(instancesList.back().back());
          }
        }
      }
    }
    endUpdate(sourceResolver, plIn.shapeHistory);
  }
  
  void updatePolar(const UpdatePayload &plIn)
  {
    beginUpdate();
    tls::Resolver sourceResolver(plIn);
    setSourceShapes(sourceResolver);
    
    //get axis.
    gp_Ax1 rotationAxis; //default is at absolute origin and pointing in z+ direction.
    const auto &polarPicks = polarAxis.getPicks();
    if (!polarPicks.empty())
    {
      tls::Resolver polarResolver(plIn);
      if (!polarResolver.resolve(polarPicks.front())) throw std::runtime_error("Invalid axis resolution");
      if (slc::isObjectType(polarResolver.getPick().selectionType))
      {
        auto originPrms = polarResolver.getFeature()->getParameters(prm::Tags::Origin);
        auto directionPrms = polarResolver.getFeature()->getParameters(prm::Tags::Direction);
        auto csysPrms = polarResolver.getFeature()->getParameters(prm::Tags::CSys);
        if (!originPrms.empty() &&  !directionPrms.empty())
          rotationAxis = gp_Ax1((gu::toOcc(originPrms.front()->getVector()).XYZ()), gu::toOcc(directionPrms.front()->getVector()));
        else if (!csysPrms.empty())
        {
          const auto &m = csysPrms.front()->getMatrix();
          rotationAxis = gp_Ax1(gu::toOcc(m.getTrans()).XYZ(), gu::toOcc(gu::getZVector(m)).XYZ());
        }
        else throw std::runtime_error("Couldn't get rotation axis from feature");
      }
      else
      {
        auto shapes = polarResolver.getShapes();
        if (shapes.empty()) throw std::runtime_error("No axis shapes");
        auto axisPair = occt::gleanAxis(shapes.front()); //only supporting 1 shape.
        if (!axisPair.second) throw std::runtime_error("Can't glean axis from shape");
        rotationAxis = axisPair.first;
      }
    }
    
    //augment counts and angles for expected results.
    int ac = polarCount.getInt(); //actual count
    double aa = polarAngle.getDouble(); //actual angle
    bool ai = polarInclusive.getBool(); //actual inclusive angle
    if (ai)
    {
      if (std::fabs(std::fabs(aa) - 360.0) <= Precision::Confusion()) //angle parameter has been constrained to less than or equal to 360
        aa = aa / ac;
      else
        aa = aa / (ac - 1);
    }
    else //ai = false
    {
      //stay under 360. don't overlap. +1 because count includes the original.
      int maxCount = static_cast<int>(360.0 / std::fabs(aa)) + 1;
      ac = std::min(ac, maxCount);
      if (ac != polarCount.getInt())
      {
        std::ostringstream s; s << "Count has been limited to less than full rotation"  << std::endl;
        feature.lastUpdateLog.append(s.str());
      }
    }
    for (const auto &tShape : sourceShapes)
    {
      gp_Trsf baseTransform = tShape.Location().Transformation();
      instancesList.emplace_back();
      for (int index = 0; index < ac; ++index)
      {
        if (!shouldAdd(index)) continue;
        gp_Trsf rotation;
        rotation.SetRotation(rotationAxis, osg::DegreesToRadians(static_cast<double>(index) * aa));
        instancesList.back().push_back(tShape.Located(TopLoc_Location(rotation * baseTransform)));
        allInstances.emplace_back(instancesList.back().back());
      }
    }
    
    endUpdate(sourceResolver, plIn.shapeHistory);
  }
  
  void updateMirror(const UpdatePayload &plIn)
  {
    beginUpdate();
    tls::Resolver sourceResolver(plIn);
    setSourceShapes(sourceResolver);
    
    //get the plane
    std::optional<gp_Trsf> oTransform; //mirror transform.
    const auto &planePicks = mirrorPlane.getPicks();
    if (planePicks.empty()) throw std::runtime_error("No Plane Selected");
    tls::Resolver planeResolver(plIn);
    if (!planeResolver.resolve(planePicks.front())) throw std::runtime_error("Invalid plane resolution");
    if (slc::isObjectType(planeResolver.getPick().selectionType))
    {
      auto csysPrms = planeResolver.getFeature()->getParameters(prm::Tags::CSys);
      if (csysPrms.empty()) throw std::runtime_error("No CSys parameter");
      oTransform = gp_Trsf(); oTransform->SetMirror(gu::toOcc(csysPrms.front()->getMatrix()));
    }
    else
    {
      auto shapes = planeResolver.getShapes();
      if (shapes.empty()) throw std::runtime_error("No plane shapes");
      auto oSys = occt::gleanSystem(shapes.front()); //only supporting 1 shape.
      if (!oSys) throw std::runtime_error("Can't glean system from shape");
      oTransform = gp_Trsf(); oTransform->SetMirror(*oSys);
    }
    if (!oTransform) throw std::runtime_error("No plane defined");
    
    for (const auto &tShape : sourceShapes)
    {
      // we have to use BRepBuilderAPI_Transform, because just setting location fails with:
      // 'Location with scaling transformation is forbidden'
      BRepBuilderAPI_Transform mirrorTransform(*oTransform);
      instancesList.emplace_back();
      if (shouldAdd(0))
      {
        instancesList.back().emplace_back(tShape);
        allInstances.emplace_back(instancesList.back().back());
      }
      if (shouldAdd(1)) //makes no sense to filter out the mirror, but oh well consistency.
      {
        mirrorTransform.Perform(tShape);
        instancesList.back().emplace_back(mirrorTransform.Shape());
        allInstances.emplace_back(instancesList.back().back());
      }
    }
    
    endUpdate(sourceResolver, plIn.shapeHistory);
  }
  
  void updateInterpolate(const UpdatePayload &plIn)
  {
    beginUpdate();
    tls::Resolver sourceResolver(plIn);
    setSourceShapes(sourceResolver);
    
    //get the coordinate systems
    std::optional<osg::Matrixd> startCSys;
    std::optional<osg::Matrixd> finishCSys;
    auto getFeatureSystem = [&](const ftr::Base *f) -> std::optional<osg::Matrixd>
    {
      auto systems = f->getParameters(prm::Tags::CSys);
      if (systems.empty()) return std::nullopt;
      return systems.front()->getMatrix();
    };
    auto getPickSystem = [&](const ftr::Pick &p) -> std::optional<osg::Matrixd>
    {
      tls::Resolver systemResolver(plIn);
      if (!systemResolver.resolve(p)) return std::nullopt;
      
      if (slc::isObjectType(systemResolver.getPick().selectionType)) return getFeatureSystem(systemResolver.getFeature());
      auto shapes = systemResolver.getShapes();
      if (shapes.empty()) return std::nullopt;
      auto oSys = occt::gleanSystem(shapes.front()); //only supporting 1 shape.
      if (!oSys) return std::nullopt;
      return gu::toOsg(*oSys);
    };
    const auto &systems = interpolateCSys.getPicks();
    if (systems.empty()) throw std::runtime_error("No Coordinate Systems To Interpolate");
    if (systems.size() == 1)
    {
      startCSys = getFeatureSystem(sourceResolver.getFeature());
      finishCSys = getPickSystem(systems.front());
    }
    else // 2 or more systems
    {
      startCSys = getPickSystem(systems.front());
      finishCSys = getPickSystem(systems.back());
    }
    if (!startCSys || !finishCSys) throw std::runtime_error("Couldn't Derive Both Systems");
    
    //do interpolation. move both system by inverse start. then we work from absolute.
    double increment = 1.0 / (interpolateCount.getInt() - 1);
    osg::Vec3d linearProjection = (finishCSys->getTrans() - startCSys->getTrans()) * increment;
    
    for (const auto &tShape : sourceShapes)
    {
      instancesList.emplace_back();
      osg::Matrixd shapeSystem = gu::toOsg(tShape.Location().Transformation());
      osg::Matrixd startSystem = shapeSystem;
      if (interpolateContact.getBool()) startSystem.setTrans(startCSys->getTrans());
      if (interpolateCorrection.getBool()) startSystem.setRotate(startCSys->getRotate());
      for (int index = 0; index < interpolateCount.getInt(); ++index)
      {
        if (!shouldAdd(index)) continue;
        osg::Quat twist;
        twist.slerp(increment * index, osg::Quat(), finishCSys->getRotate() / startCSys->getRotate());
        osg::Quat newRotation = twist * startSystem.getRotate();
        
        osg::Vec3d newOrigin = startSystem.getTrans() + (linearProjection * index);
        osg::Matrixd newMatrix(newRotation);
        newMatrix.setTrans(newOrigin);
        
        gp_Trsf currentTransform;
        currentTransform.SetTransformation(gp_Ax3(gu::toOcc(newMatrix)));
        //gp_Ax2 looks as I expect, gp_Ax3 looks as I expect. Not sure why gp_Trsf needs to be inverted.
        currentTransform.Invert(); //????
        
        instancesList.back().push_back(tShape.Located(TopLoc_Location(currentTransform)));
        allInstances.emplace_back(instancesList.back().back());
      }
    }
    endUpdate(sourceResolver, plIn.shapeHistory);
  }
  
  /* A non-planar wire with lines causes this to fail as the adaptor cannot derive
   * normal. See notes.
   */
  void updateSpine(const UpdatePayload &plIn)
  {
    beginUpdate();
    tls::Resolver sourceResolver(plIn);
    setSourceShapes(sourceResolver);
    
    const auto &spines = spineSpine.getPicks();
    if (spines.empty()) throw std::runtime_error("No Input Spine");
    std::optional<osg::Vec3d> biNormal;
    tls::Resolver spineResolver(plIn);
    if (!spineResolver.resolve(spines.front())) throw std::runtime_error("Couldn't Resolve Spine");
    if (!spineResolver.getSeerShape()) throw std::runtime_error("No SeerShape In Spine Feature");
    
    TopoDS_Wire theWire;
    auto gleanWireBinormal = [&]()
    {
      if (theWire.IsNull()) return;
      BRepBuilderAPI_MakeFace fm(theWire, true);
      if (!fm.IsDone()) return;
      biNormal = gu::toOsg(occt::getNormal(fm.Face(), 0.0, 0.0));
    };
    switch (spineResolver.getPick().selectionType)
    {
      case slc::Type::Object: case slc::Type::Feature:
      {
        const auto *ss = spineResolver.getSeerShape();
        if (ss)
        {
          auto wires = ss->useGetChildrenOfType(ss->getRootOCCTShape(), TopAbs_WIRE);
          auto edges = ss->useGetChildrenOfType(ss->getRootOCCTShape(), TopAbs_EDGE);
          if (!wires.empty()) theWire = TopoDS::Wire(wires.front());
          else if (!edges.empty()) theWire = BRepBuilderAPI_MakeWire(TopoDS::Edge(edges.front()));
        }
        if (spineResolver.getFeature()->getType() == ftr::Type::Sketch)
        {
          auto csysPrms = spineResolver.getFeature()->getParameters(prm::Tags::CSys);
          if (csysPrms.empty()) throw std::runtime_error("No CSys Parameters In Sketch Feature");
          biNormal = gu::getZVector(csysPrms.front()->getMatrix());
        }
        else gleanWireBinormal();
        break;
      }
      case slc::Type::Wire:
      {
        auto shapes = spineResolver.getShapes();
        if (shapes.empty()) throw std::runtime_error("No wire shapes");
        if (shapes.front().ShapeType() != TopAbs_WIRE) throw std::runtime_error("Shape is not a wire");
        theWire = TopoDS::Wire(shapes.front());
        gleanWireBinormal();
        break;
      }
      case slc::Type::Edge:
      {
        auto shapes = spineResolver.getShapes();
        if (shapes.empty()) throw std::runtime_error("No edge shapes");
        if (shapes.front().ShapeType() != TopAbs_EDGE) throw std::runtime_error("Shape is not an edge");
        theWire = BRepBuilderAPI_MakeWire(TopoDS::Edge(shapes.front()));
        gleanWireBinormal();
        break;
      }
      default: {throw std::runtime_error("Unsupported Spine Type"); break;}
    }
    if (theWire.IsNull()) throw std::runtime_error("Couldn't derive spine shape");
    //don't use constructor with parameter range. It trims instead of reparameterize.
    BRepAdaptor_CompCurve adaptor(theWire, true);
    
    std::vector<double> params;
    int count = spineCount.getInt();
    int closedFactor = (adaptor.IsClosed()) ? count : count - 1;
    double increment = (adaptor.LastParameter() - adaptor.FirstParameter()) / static_cast<double>(closedFactor);
    std::vector<double> spineParameters;
    //going to do a separate loop here and generate an array of parameters to keep algo a little more simple.
    for (int index = 0; index < count; ++index) spineParameters.push_back(index * increment + adaptor.FirstParameter());
    if (spineOrientation.getInt() != 0) std::reverse(spineParameters.begin(), spineParameters.end());
    
    for (const auto &tShape : sourceShapes)
    {
      instancesList.emplace_back();
      osg::Matrixd shapeSystem = gu::toOsg(tShape.Location().Transformation());
      osg::Matrixd startSystem = shapeSystem;
      std::optional<osg::Matrixd> firstPosition;
      for (int index = 0; index < static_cast<int>(spineParameters.size()); ++index)
      {
        gp_Pnt origin;
        gp_Vec tangent, normal;
        std::optional<osg::Matrix> position;
        //when reversing we keep the same bi normal.
        if (biNormal)
        {
          adaptor.D1(spineParameters.at(index), origin, tangent);
          if (spineOrientation.getInt() != 0) tangent.Reverse();
          position = tls::matrixFromAxes({gu::toOsg(tangent), std::nullopt, biNormal, gu::toOsg(origin)});
        }
        else
        {
          adaptor.D2(spineParameters.at(index), origin, tangent, normal);
          if (spineOrientation.getInt() != 0) {tangent.Reverse(); normal.Reverse();}
          position = tls::matrixFromAxes({gu::toOsg(tangent), gu::toOsg(normal), std::nullopt, gu::toOsg(origin)});
        }
        if (!position) throw std::runtime_error("Couldn't get instance position");
        if (!firstPosition)
        {
          firstPosition = position;
          if (spineContact.getBool()) startSystem.setTrans(firstPosition->getTrans());
          if (spineCorrection.getBool()) startSystem.setRotate(firstPosition->getRotate());
        }
        if (!shouldAdd(index)) continue; //doing this down here so we set first position.
        
        osg::Matrixd calc = startSystem * osg::Matrixd::inverse(*firstPosition) * (*position);
        gp_Trsf currentTransform;
        currentTransform.SetTransformation(gp_Ax3(gu::toOcc(calc)));
        currentTransform.Invert(); //see interpolate.
        
        instancesList.back().push_back(tShape.Located(TopLoc_Location(currentTransform)));
        allInstances.emplace_back(instancesList.back().back());
      }
    }
    
    endUpdate(sourceResolver, plIn.shapeHistory);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Instance");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    auto currentType = getInstanceType();
    switch (currentType)
    {
      case Linear:{stow->updateLinear(pIn); break;}
      case Polar:{stow->updatePolar(pIn); break;}
      case Mirror:{stow->updateMirror(pIn); break;}
      case Interpolate:{stow->updateInterpolate(pIn); break;}
      case Spine:{stow->updateSpine(pIn); break;}
    }
    occt::BoundingBox bb(stow->sShape.getRootOCCTShape());
    stow->grid->setPosition(gu::toOsg(bb.getCenter()));
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

IType Feature::getInstanceType()
{
  return static_cast<IType>(stow->instanceType.getInt());
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::inst::Instance out
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->iMapper.serialOut()
    , stow->source.serialOut()
    , stow->instanceType.serialOut()
    , stow->filter.serialOut()
    , stow->filterIndexes.serialOut()
    , stow->instanceTypeLabel->serialOut()
    , stow->filterLabel->serialOut()
    , stow->filterIndexesLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
  );
  
  switch (getInstanceType())
  {
    case Linear:
    {
      out.linear() = prj::srl::inst::Linear
      (
        stow->linearCSys.serialOut()
        , stow->linearXOffset.serialOut()
        , stow->linearYOffset.serialOut()
        , stow->linearZOffset.serialOut()
        , stow->linearXCount.serialOut()
        , stow->linearYCount.serialOut()
        , stow->linearZCount.serialOut()
        , stow->linearXOffsetLabel->serialOut()
        , stow->linearYOffsetLabel->serialOut()
        , stow->linearZOffsetLabel->serialOut()
        , stow->linearXCountLabel->serialOut()
        , stow->linearYCountLabel->serialOut()
        , stow->linearZCountLabel->serialOut()
      );
      break;
    }
    case Polar:
    {
      out.polar() = prj::srl::inst::Polar
      (
        stow->polarAxis.serialOut()
        , stow->polarAngle.serialOut()
        , stow->polarCount.serialOut()
        , stow->polarInclusive.serialOut()
        , stow->polarAngleLabel->serialOut()
        , stow->polarCountLabel->serialOut()
        , stow->polarInclusiveLabel->serialOut()
      );
      break;
    }
    case Mirror:
    {
      out.mirror() = prj::srl::inst::Mirror(stow->mirrorPlane.serialOut());
      break;
    }
    case Interpolate:
    {
      out.interpolate() = prj::srl::inst::Interpolate
      (
        stow->interpolateCSys.serialOut()
        , stow->interpolateCount.serialOut()
        , stow->interpolateCountLabel->serialOut()
        , stow->interpolateContact.serialOut()
        , stow->interpolateContactLabel->serialOut()
        , stow->interpolateCorrection.serialOut()
        , stow->interpolateCorrectionLabel->serialOut()
      );
      break;
    }
    case Spine:
    {
      out.spine() = prj::srl::inst::Spine
      (
        stow->spineSpine.serialOut()
        , stow->spineCount.serialOut()
        , stow->spineOrientation.serialOut()
        , stow->spineCountLabel->serialOut()
        , stow->spineOrientationLabel->serialOut()
        , stow->spineContact.serialOut()
        , stow->spineContactLabel->serialOut()
        , stow->spineCorrection.serialOut()
        , stow->spineCorrectionLabel->serialOut()
      );
      break;
    }
  }
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::inst::instance(stream, out, infoMap);
}

void Feature::serialRead(const prj::srl::inst::Instance &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.seerShape());
  stow->iMapper.serialIn(so.iMapper());
  stow->source.serialIn(so.source());
  stow->instanceType.serialIn(so.instanceType());
  stow->filter.serialIn(so.filter());
  stow->filterIndexes.serialIn(so.filterIndexes());
  stow->instanceTypeLabel->serialIn(so.instanceTypeLabel());
  stow->filterLabel->serialIn(so.filterLabel());
  stow->filterIndexesLabel->serialIn(so.filterIndexesLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  
  switch (getInstanceType())
  {
    case Linear:
    {
      if (so.linear())
      {
        stow->linearCSys.serialIn(so.linear()->linearCSys());
        stow->linearXOffset.serialIn(so.linear()->linearXOffset());
        stow->linearYOffset.serialIn(so.linear()->linearYOffset());
        stow->linearZOffset.serialIn(so.linear()->linearZOffset());
        stow->linearXCount.serialIn(so.linear()->linearXCount());
        stow->linearYCount.serialIn(so.linear()->linearYCount());
        stow->linearZCount.serialIn(so.linear()->linearZCount());
        stow->linearXOffsetLabel->serialIn(so.linear()->linearXOffsetLabel());
        stow->linearYOffsetLabel->serialIn(so.linear()->linearYOffsetLabel());
        stow->linearZOffsetLabel->serialIn(so.linear()->linearZOffsetLabel());
        stow->linearXCountLabel->serialIn(so.linear()->linearXCountLabel());
        stow->linearYCountLabel->serialIn(so.linear()->linearYCountLabel());
        stow->linearZCountLabel->serialIn(so.linear()->linearZCountLabel());
      }
      break;
    }
    case Polar:
    {
      if (so.polar())
      {
        stow->polarAxis.serialIn(so.polar()->polarAxis());
        stow->polarAngle.serialIn(so.polar()->polarAngle());
        stow->polarCount.serialIn(so.polar()->polarCount());
        stow->polarInclusive.serialIn(so.polar()->polarInclusive());
        stow->polarAngleLabel->serialIn(so.polar()->polarAngleLabel());
        stow->polarCountLabel->serialIn(so.polar()->polarCountLabel());
        stow->polarInclusiveLabel->serialIn(so.polar()->polarInclusiveLabel());
      }
      break;
    }
    case Mirror:
    {
      if (so.mirror()) stow->mirrorPlane.serialIn(so.mirror()->mirrorPlane());
      break;
    }
    case Interpolate:
    {
      if (so.interpolate())
      {
        stow->interpolateCSys.serialIn(so.interpolate()->interpolateCSys());
        stow->interpolateCount.serialIn(so.interpolate()->interpolateCount());
        stow->interpolateCountLabel->serialIn(so.interpolate()->interpolateCountLabel());
        stow->interpolateContact.serialIn(so.interpolate()->interpolateContact());
        stow->interpolateContactLabel->serialIn(so.interpolate()->interpolateContactLabel());
        stow->interpolateCorrection.serialIn(so.interpolate()->interpolateCorrection());
        stow->interpolateCorrectionLabel->serialIn(so.interpolate()->interpolateCorrectionLabel());
      }
      break;
    }
    case Spine:
    {
      if (so.spine())
      {
        stow->spineSpine.serialIn(so.spine()->spineSpine());
        stow->spineCount.serialIn(so.spine()->spineCount());
        stow->spineOrientation.serialIn(so.spine()->spineOrientation());
        stow->spineCountLabel->serialIn(so.spine()->spineCountLabel());
        stow->spineOrientationLabel->serialIn(so.spine()->spineOrientationLabel());
        stow->spineContact.serialIn(so.spine()->spineContact());
        stow->spineContactLabel->serialIn(so.spine()->spineContactLabel());
        stow->spineCorrection.serialIn(so.spine()->spineCorrection());
        stow->spineCorrectionLabel->serialIn(so.spine()->spineCorrectionLabel());
      }
      break;
    }
  }
  stow->prmActiveSync();
}

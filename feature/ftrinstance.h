/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_INSTANCE_H
#define FTR_INSTANCE_H

#include "feature/ftrbase.h"

namespace prj{namespace srl{namespace inst{class Instance;}}}

namespace ftr
{
  namespace Instance
  {
    enum IType
    {
      Linear
      , Polar
      , Mirror
      , Interpolate
      , Spine
    };
    
    namespace Tags
    {
      inline constexpr std::string_view Source = "Source";
      inline constexpr std::string_view InstanceType = "InstanceType";
      inline constexpr std::string_view Filter = "Filter";
      inline constexpr std::string_view FilterIndexes = "FilterIndexes";
      
      //linear
      inline constexpr std::string_view LinearCSys = "LinearCSys";
      inline constexpr std::string_view LinearXOffset = "LinearXOffset";
      inline constexpr std::string_view LinearYOffset = "LinearYOffset";
      inline constexpr std::string_view LinearZOffset = "LinearZOffset";
      inline constexpr std::string_view LinearXCount = "LinearXCount";
      inline constexpr std::string_view LinearYCount = "LinearYCount";
      inline constexpr std::string_view LinearZCount = "LinearZCount";
      
      //polar
      inline constexpr std::string_view PolarAxis = "PolarAxis";
      inline constexpr std::string_view PolarCount = "PolarCount";
      inline constexpr std::string_view PolarInclusive = "PolarInclusive";
      
      //mirror
      inline constexpr std::string_view MirrorPlane = "MirrorPlane";
      
      //interpolate between 2 systems.
      inline constexpr std::string_view InterpolateCSys = "InterpolateCSys";
      inline constexpr std::string_view InterpolateCount = "InterpolateCount";
      inline constexpr std::string_view InterpolateContact = "InterpolateContact";
      inline constexpr std::string_view InterpolateCorrection = "InterpolateCorrection";
      
      //spine
      inline constexpr std::string_view SpineSpine = "SpineSpine";
      inline constexpr std::string_view SpineCount = "SpineCount";
      inline constexpr std::string_view SpineOrientation = "SpineOrientation";
      inline constexpr std::string_view SpineContact = "SpineContact";
      inline constexpr std::string_view SpineCorrection = "SpineCorrection";
    }
    
    class Feature : public Base
    {
    public:
      Feature();
      ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      Type getType() const override {return Type::Instance;}
      const std::string& getTypeString() const override {return toString(Type::Instance);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      IType getInstanceType();
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::inst::Instance&);
      
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}

#endif //FTR_INSTANCE_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gp_Lin.hxx>
#include <gp_Pln.hxx>
#include <TopoDS.hxx>
#include <TopExp.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepExtrema_DistShapeShape.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/idtools.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "feature/ftrupdatepayload.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "project/serial/generated/prjsrllnsline.h"
#include "feature/ftrline.h"

using namespace ftr::Line;
using boost::uuids::uuid;

QIcon Feature::icon = QIcon(":/resources/images/sketchLine.svg");

struct Feature::Stow
{
  Feature &feature;
  ann::SeerShape sShape;
  
  prm::Parameter lineType{QObject::tr("Line Type"), 0, Tags::lineType};
  prm::Parameter method{QObject::tr("Method"), 0, Tags::method};
  
  prm::Parameter origin{prm::Names::Origin, osg::Vec3d(), prm::Tags::Origin};
  prm::Parameter finale{QObject::tr("Finale"), osg::Vec3d(1.0, 0.0, 0.0), Tags::finale};
  prm::Parameter direction{prm::Names::Direction, osg::Vec3d(1.0, 0.0, 0.0), prm::Tags::Direction};
  prm::Parameter length{prm::Names::Length, 1.0, prm::Tags::Length};
  prm::Parameter originPick{prm::Names::Origin, ftr::Picks(), Tags::originPick};
  prm::Parameter finalePick{QObject::tr("Finale"), ftr::Picks(), Tags::finalePick};
  prm::Parameter directionPick{prm::Names::Direction, ftr::Picks(), Tags::directionPick};
  
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> lineTypeLabel{new lbr::PLabel(&lineType)};
  osg::ref_ptr<lbr::PLabel> methodLabel{new lbr::PLabel(&method)};
  osg::ref_ptr<lbr::PLabel> originLabel{new lbr::PLabel(&origin)};
  osg::ref_ptr<lbr::PLabel> finaleLabel{new lbr::PLabel(&finale)};
  osg::ref_ptr<lbr::PLabel> directionLabel{new lbr::PLabel(&direction)};
  osg::ref_ptr<lbr::PLabel> lengthLabel{new lbr::PLabel(&length)};
  
  uuid lineId{gu::createRandomId()};
  uuid v0Id{gu::createRandomId()};
  uuid v1Id{gu::createRandomId()};
  
  Stow(Feature &fIn)
  : feature(fIn)
  {
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    
    QStringList lineTypeStrings =
    {
      QObject::tr("Extrema")
      , QObject::tr("Origin, Direction, Length")
    };
    lineType.setEnumeration(lineTypeStrings);
    lineType.connect(dirtyObserver);
    lineType.connect(syncObserver);
    feature.parameters.push_back(&lineType);
    
    QStringList methodStrings =
    {
      QObject::tr("Picks")
      , QObject::tr("Constants")
    };
    method.setEnumeration(methodStrings);
    method.connect(dirtyObserver);
    method.connect(syncObserver);
    feature.parameters.push_back(&method);
    
    origin.connect(dirtyObserver); feature.parameters.push_back(&origin);
    finale.connect(dirtyObserver); feature.parameters.push_back(&finale);
    direction.connect(dirtyObserver); feature.parameters.push_back(&direction);
    length.connect(dirtyObserver); feature.parameters.push_back(&length);
    
    originPick.connect(dirtyObserver); feature.parameters.push_back(&originPick);
    finalePick.connect(dirtyObserver); feature.parameters.push_back(&finalePick);
    directionPick.connect(dirtyObserver); feature.parameters.push_back(&directionPick);
    
    length.setConstraint(prm::Constraint::buildNonZeroPositive());
    
    feature.overlaySwitch->addChild(grid);
    grid->addChild(lineTypeLabel);
    grid->addChild(methodLabel);
    grid->addChild(originLabel);
    grid->addChild(finaleLabel);
    grid->addChild(directionLabel);
    grid->addChild(lengthLabel);
    lineTypeLabel->refresh();
    methodLabel->refresh();
    
    prmActiveSync();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver); // why this ???
    prm::ObserverBlocker syncBlocker(syncObserver);
    
    switch (lineType.getInt())
    {
      case 0: //Extrema
      {
        switch (method.getInt())
        {
          case 0: //Picks
          {
            origin.setActive(false);
            finale.setActive(false);
            direction.setActive(false);
            length.setActive(false);
            originPick.setActive(true);
            finalePick.setActive(true);
            directionPick.setActive(false);
            break;
          }
          case 1: //constants
          {
            origin.setActive(true);
            finale.setActive(true);
            direction.setActive(false);
            length.setActive(false);
            originPick.setActive(false);
            finalePick.setActive(false);
            directionPick.setActive(false);
            break;
          }
          default: {break;}
        }
        break;
      }
      case 1: //origin direction length
      {
        switch (method.getInt())
        {
          case 0: //Picks
          {
            origin.setActive(false);
            finale.setActive(false);
            direction.setActive(false);
            length.setActive(true);
            originPick.setActive(true);
            finalePick.setActive(false);
            directionPick.setActive(true);
            break;
          }
          case 1: //constants
          {
            origin.setActive(true);
            finale.setActive(false);
            direction.setActive(true);
            length.setActive(true);
            originPick.setActive(false);
            finalePick.setActive(false);
            directionPick.setActive(false);
            break;
          }
          default: {break;}
        }
        break;
      }
      default: {break;}
    }
  }
  
  void buildLine(const gp_Pnt &p0, const gp_Pnt &p1)
  {
    BRepBuilderAPI_MakeEdge edgeMaker(p0, p1);
    if (!edgeMaker.IsDone()) throw std::runtime_error("Couldn't construct line edge");
    sShape.setOCCTShape(TopoDS::Edge(edgeMaker), feature.getId());
    sShape.updateId(edgeMaker.Edge(), lineId);
    sShape.updateId(TopExp::FirstVertex(edgeMaker.Edge()), v0Id);
    sShape.updateId(TopExp::LastVertex(edgeMaker.Edge()), v1Id);
    sShape.ensureNoNils();
    sShape.ensureNoDuplicates();
    sShape.ensureEvolve();
    
    auto project = gu::toOsg(p1) - gu::toOsg(p0);
    auto loc = gu::toOsg(p0) + (project * 0.5);
    grid->setPosition(loc);
    
    //keep parameters in sync in case data is from picks.
    origin.setValue(gu::toOsg(p0));
    finale.setValue(gu::toOsg(p1));
    project.normalize(); direction.setValue(project);
  }
  
  void goUpdateExtrema(tls::Resolver &resolver)
  {
    occt::ShapeVector allShapes;
    auto addPick = [&](const ftr::Pick &pIn)
    {
      if (!resolver.resolve(pIn)) throw std::runtime_error("Couldn't resolve extrema pick");
      if (slc::isObjectType(resolver.getPick().selectionType))
      {
        switch(resolver.getFeature()->getType())
        {
          case ftr::Type::DatumPlane:
          {
            auto sys = resolver.getFeature()->getParameter(prm::Tags::CSys)->getMatrix();
            auto o = sys.getTrans();
            auto z = gu::getZVector(sys);
            gp_Pln plane(gu::toOcc(o).XYZ(), gu::toOcc(z));
            allShapes.push_back(BRepBuilderAPI_MakeFace(plane).Face());
            break;
          }
          case ftr::Type::DatumAxis:
          {
            auto o = resolver.getFeature()->getParameter(prm::Tags::Origin)->getVector();
            auto d = resolver.getFeature()->getParameter(prm::Tags::Direction)->getVector();
            gp_Lin line(gu::toOcc(o).XYZ(), gu::toOcc(d));
            allShapes.push_back(BRepBuilderAPI_MakeEdge(line).Edge());
            break;
          }
          case ftr::Type::DatumPoint:
          {
            auto o = resolver.getFeature()->getParameter(prm::Tags::Origin)->getVector();
            allShapes.push_back(BRepBuilderAPI_MakeVertex(gu::toOcc(o).XYZ()));
            break;
          }
          default:{break;}
        }
      }
      auto shapes = resolver.getShapes();
      auto points = resolver.getPoints();
      if (shapes.empty()) for (const auto &p : points) shapes.push_back(BRepBuilderAPI_MakeVertex(gu::toOcc(p).XYZ()));
      allShapes.insert(allShapes.end(), shapes.begin(), shapes.end());
    };
    
    switch (method.getInt())
    {
      case 0: //picks
      {
        auto oPicks = originPick.getPicks();
        if (oPicks.empty()) throw std::runtime_error("No origin pick");
        addPick(oPicks.front());
        auto fPicks = finalePick.getPicks();
        if (fPicks.empty()) throw std::runtime_error("No finale pick");
        addPick(fPicks.front());
        break;
      }
      case 1: //constants
      {
        allShapes.push_back(BRepBuilderAPI_MakeVertex(gu::toOcc(origin.getVector()).XYZ()));
        allShapes.push_back(BRepBuilderAPI_MakeVertex(gu::toOcc(finale.getVector()).XYZ()));
        break;
      }
      default: {break;}
    }
    
    if (allShapes.size() != 2) throw std::runtime_error("Wrong number of extrema shapes");
    BRepExtrema_DistShapeShape extrema(allShapes.front(), allShapes.back(), Extrema_ExtFlag_MIN);
    if (!extrema.IsDone() || extrema.NbSolution() < 1) throw std::runtime_error("extrema failed");
    gp_Pnt p0 = extrema.PointOnShape1(1);
    gp_Pnt p1 = extrema.PointOnShape2(1);
    buildLine(p0, p1);
  }
  
  void goUpdateOriginDirectionLength(tls::Resolver &resolver)
  {
    std::optional<osg::Vec3d> resultOrigin;
    std::optional<osg::Vec3d> resultDirection;
    
    switch (method.getInt())
    {
      case 0: //picks
      {
        if (originPick.getPicks().empty()) throw std::runtime_error("No origin picks");
        if (directionPick.getPicks().empty()) throw std::runtime_error("No direction picks");
        
        if (!resolver.resolve(originPick.getPicks().front())) throw std::runtime_error("Failed to resolve origin picks");
        if (slc::isObjectType(resolver.getPick().selectionType))
        {
          if (auto os = resolver.getFeature()->getParameters(prm::Tags::Origin); !os.empty()) resultOrigin = os.front()->getVector();
          else if (auto cs = resolver.getFeature()->getParameters(prm::Tags::CSys); !cs.empty()) resultOrigin = cs.front()->getMatrix().getTrans();
        }
        else if (auto ps = resolver.getPoints(); !ps.empty()) resultOrigin = ps.front();
        
        if (!resolver.resolve(directionPick.getPicks().front())) throw std::runtime_error("Failed to resolve direction picks");
        if (slc::isObjectType(resolver.getPick().selectionType))
        {
          if (auto ds = resolver.getFeature()->getParameters(prm::Tags::Direction); !ds.empty()) resultDirection = ds.front()->getVector();
        }
        else if (auto shapes = resolver.getShapes(); !shapes.empty())
        {
          auto axisPair = occt::gleanAxis(shapes.front());
          if (!axisPair.second) throw std::runtime_error("couldn't glean axis");
          resultDirection = gu::toOsg(axisPair.first.Direction());
        }
        break;
      }
      case 1: //constants
      {
        resultOrigin = origin.getVector();
        resultDirection = direction.getVector();
        break;
      }
      default: {break;}
    }
    
    if (!resultOrigin) throw std::runtime_error("Couldn't get origin");
    if (!resultDirection) throw std::runtime_error("Couldn't get direction");
    gp_Pnt p0 = gu::toOcc(*resultOrigin).XYZ();
    gp_Pnt p1 = gu::toOcc((*resultOrigin) + ((*resultDirection) * length.getDouble())).XYZ();
    buildLine(p0, p1);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))

{
  name = QObject::tr("Line");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() {}

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    
    tls::Resolver resolver(pIn);
    switch (stow->lineType.getInt())
    {
      case 0: {stow->goUpdateExtrema(resolver); break;}
      case 1: {stow->goUpdateOriginDirectionLength(resolver); break;}
      default: {throw std::runtime_error("Unknown line type"); break;}
    }
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in line update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in line update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in line update. " << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::lns::Line so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->lineType.serialOut()
    , stow->method.serialOut()
    , stow->origin.serialOut()
    , stow->finale.serialOut()
    , stow->direction.serialOut()
    , stow->length.serialOut()
    , stow->originPick.serialOut()
    , stow->finalePick.serialOut()
    , stow->directionPick.serialOut()
    , stow->lineTypeLabel->serialOut()
    , stow->methodLabel->serialOut()
    , stow->originLabel->serialOut()
    , stow->finaleLabel->serialOut()
    , stow->directionLabel->serialOut()
    , stow->lengthLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
    , gu::idToString(stow->lineId)
    , gu::idToString(stow->v0Id)
    , gu::idToString(stow->v1Id)
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::lns::line(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::lns::Line &sli)
{
  Base::serialIn(sli.base());
  stow->sShape.serialIn(sli.seerShape());
  stow->lineType.serialIn(sli.lineType());
  stow->method.serialIn(sli.method());
  stow->origin.serialIn(sli.origin());
  stow->finale.serialIn(sli.finale());
  stow->direction.serialIn(sli.direction());
  stow->length.serialIn(sli.length());
  stow->originPick.serialIn(sli.originPick());
  stow->finalePick.serialIn(sli.finalePick());
  stow->directionPick.serialIn(sli.directionPick());
  stow->lineTypeLabel->serialIn(sli.lineTypeLabel());
  stow->methodLabel->serialIn(sli.methodLabel());
  stow->originLabel->serialIn(sli.originLabel());
  stow->finaleLabel->serialIn(sli.finaleLabel());
  stow->directionLabel->serialIn(sli.directionLabel());
  stow->lengthLabel->serialIn(sli.lengthLabel());
  lbr::PLabelGridCallback::serialIn(sli.gridLocation(), stow->grid);
  stow->lineId = gu::stringToId(sli.lineId());
  stow->v0Id = gu::stringToId(sli.v0Id());
  stow->v1Id = gu::stringToId(sli.v1Id());
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_LINE_H
#define FTR_LINE_H

#include "feature/ftrbase.h"

namespace prj{namespace srl{namespace lns{class Line;}}}

namespace ftr
{
  namespace Line
  {
    namespace Tags
    {
      inline constexpr std::string_view lineType = "lineType";
      inline constexpr std::string_view method = "method";
      inline constexpr std::string_view finale = "finale";
      inline constexpr std::string_view originPick = "originPick";
      inline constexpr std::string_view finalePick = "finalePick";
      inline constexpr std::string_view directionPick = "directionPick";
    }
    
    class Feature : public Base
    {
    public:
      
      Feature();
      virtual ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      Type getType() const override {return Type::Line;}
      const std::string& getTypeString() const override {return toString(Type::Line);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::lns::Line&);
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}



#endif //FTR_LINE_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2020 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <osg/Switch>

#include <BRep_Tool.hxx>
#include <BRepBuilderAPI_Copy.hxx>
#include <TopoDS.hxx>
#include <BRepTools_WireExplorer.hxx>
#include <GeomAPI.hxx>
#include <gp_Pln.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <GeomAdaptor_Surface.hxx>
#include <GeomConvert_ApproxSurface.hxx>
#include <Geom_BSplineSurface.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepLib.hxx>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/idtools.h"
#include "tools/tlsshapeid.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlmpcmappcurve.h"
#include "feature/ftrmappcurve.h"

namespace
{
  [[maybe_unused]] void dumpSurfacePrms(const opencascade::handle<Geom_Surface> &surface)
  {
    GeomAdaptor_Surface sa(surface);
    std::cout << std::endl
    << "U Min: " << sa.FirstUParameter() << "    U Max: " << sa.LastUParameter() << std::endl
    << "V MIn: " << sa.FirstVParameter() << "    V Max: " << sa.LastVParameter() << std::endl;
  }
  
  struct Range
  {
    double min = 0.0;
    double max = 1.0;
    bool isValid()
    {
      return ((max - min) > Precision::PConfusion());
    }
    double adaptTo(double vIn, const Range &other) const
    {
      assert(vIn >= min && vIn <= max);
      double scale = (vIn - min) / (max - min);
      double out = (other.max - other.min) * scale + other.min;
      out = std::clamp(out, other.min, other.max);
      return out;
    }
  };
  
  class RangeAdaptor : public GeomAdaptor_Surface
  {
  public:
    RangeAdaptor() = delete;
    RangeAdaptor(const opencascade::handle<Geom_Surface> &sIn, const Range &uIn, const Range &vIn)
    : GeomAdaptor_Surface(sIn), surfaceIn(sIn), targetURange(uIn), targetVRange(vIn)
    {
      surfaceInAdaptor = new GeomAdaptor_Surface(surfaceIn);
      sourceURange.min = surfaceInAdaptor->FirstUParameter();
      sourceURange.max = surfaceInAdaptor->LastUParameter();
      sourceVRange.min = surfaceInAdaptor->FirstVParameter();
      sourceVRange.max = surfaceInAdaptor->LastVParameter();
    }
    
    double FirstUParameter() const override {return targetURange.min;}
    double LastUParameter() const override {return targetURange.max;}
    double FirstVParameter() const override {return targetVRange.min;}
    double LastVParameter() const override {return targetVRange.max;}
    
    //verified that UTrim and VTrim are only called with the first and last parameters, no actual trimming.
    opencascade::handle<Adaptor3d_Surface> UTrim(double, double, double) const override
    {return opencascade::handle<Adaptor3d_Surface>(this);}
    opencascade::handle<Adaptor3d_Surface> VTrim(double, double, double) const override
    {return opencascade::handle<Adaptor3d_Surface>(this);}
    
    gp_Pnt Value(double uIn, double vIn) const override
    {
      double u = targetURange.adaptTo(uIn, sourceURange);
      double v = targetVRange.adaptTo(vIn, sourceVRange);
      return surfaceInAdaptor->Value(u, v);
    }
    
    void D1(const double uIn, const double vIn, gp_Pnt &pointOut, gp_Vec &d1u, gp_Vec &d1v) const override
    {
      double u = targetURange.adaptTo(uIn, sourceURange);
      double v = targetVRange.adaptTo(vIn, sourceVRange);
      return surfaceInAdaptor->D1(u, v, pointOut, d1u, d1v);
    }
    
    void D2(const double uIn, const double vIn, gp_Pnt &pointOut, gp_Vec &d1u, gp_Vec &d1v, gp_Vec &d2u, gp_Vec &d2v, gp_Vec &d2uv) const override
    {
      double u = targetURange.adaptTo(uIn, sourceURange);
      double v = targetVRange.adaptTo(vIn, sourceVRange);
      return surfaceInAdaptor->D2(u, v, pointOut, d1u, d1v, d2u, d2v, d2uv);
    }
    
    void D3(const double uIn, const double vIn, gp_Pnt &pointOut
    , gp_Vec &d1u, gp_Vec &d1v
    , gp_Vec &d2u, gp_Vec &d2v, gp_Vec &d2uv
    , gp_Vec &d3u, gp_Vec &d3v, gp_Vec &d3uuv, gp_Vec &d3uvv
    ) const override
    {
      double u = targetURange.adaptTo(uIn, sourceURange);
      double v = targetVRange.adaptTo(vIn, sourceVRange);
      return surfaceInAdaptor->D3(u, v, pointOut, d1u, d1v, d2u, d2v, d2uv, d3u, d3v, d3uuv, d3uvv);
    }
    
    gp_Vec DN(const double uIn, const double vIn, const int nu, const int nv) const override
    {
      double u = targetURange.adaptTo(uIn, sourceURange);
      double v = targetVRange.adaptTo(vIn, sourceVRange);
      return surfaceInAdaptor->DN(u, v, nu, nv);
    }
    
  private:
    opencascade::handle<Geom_Surface> surfaceIn;
    opencascade::handle<GeomAdaptor_Surface> surfaceInAdaptor;
    Range targetURange;
    Range targetVRange;
    Range sourceURange;
    Range sourceVRange;
  };
}

using boost::uuids::uuid;
using namespace ftr::MapPCurve;
QIcon Feature::icon = QIcon(":/resources/images/mapPCurve.svg");

struct Feature::Stow
{
  Feature &feature;
  prm::Parameter facePick{QObject::tr("Face Pick"), ftr::Picks(), Tags::facePick};
  prm::Parameter edgePicks{QObject::tr("Edge Picks"), ftr::Picks(), Tags::edgePicks};
  prm::Parameter uReverse{QObject::tr("U Reverse"), false, Tags::uReverse};
  prm::Parameter vReverse{QObject::tr("V Reverse"), false, Tags::vReverse};
  prm::Parameter reparameterize{QObject::tr("Reparameterize"), false, Tags::reparameterize};
  prm::Parameter umin{QObject::tr("U Minimum"), 0.0, Tags::umin};
  prm::Parameter umax{QObject::tr("U Maximum"), 1.0, Tags::umax};
  prm::Parameter vmin{QObject::tr("V Minimum"), 0.0, Tags::vmin};
  prm::Parameter vmax{QObject::tr("V Maximum"), 1.0, Tags::vmax};
  
  ann::SeerShape sShape;
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> uReverseLabel{new lbr::PLabel(&uReverse)};
  osg::ref_ptr<lbr::PLabel> vReverseLabel{new lbr::PLabel(&vReverse)};
  osg::ref_ptr<lbr::PLabel> reparameterizeLabel{new lbr::PLabel(&reparameterize)};
  osg::ref_ptr<lbr::PLabel> uminLabel{new lbr::PLabel(&umin)};
  osg::ref_ptr<lbr::PLabel> umaxLabel{new lbr::PLabel(&umax)};
  osg::ref_ptr<lbr::PLabel> vminLabel{new lbr::PLabel(&vmin)};
  osg::ref_ptr<lbr::PLabel> vmaxLabel{new lbr::PLabel(&vmax)};
  
  //TODO Add labels for reparameterize.
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  Stow() = delete;
  Stow(Feature &fIn)
  : feature(fIn)
  {
    auto commonConnect = [&](prm::Parameter &prmIn)
    {
      prmIn.connect(dirtyObserver);
      feature.parameters.push_back(&prmIn);
    };
    commonConnect(facePick);
    commonConnect(edgePicks);
    commonConnect(uReverse);
    commonConnect(vReverse);
    commonConnect(reparameterize);
    commonConnect(umin);
    commonConnect(umax);
    commonConnect(vmin);
    commonConnect(vmax);
    reparameterize.connect(syncObserver);
    
    feature.overlaySwitch->addChild(grid);
    grid->addChild(uReverseLabel);
    grid->addChild(vReverseLabel);
    grid->addChild(reparameterizeLabel);
    grid->addChild(uminLabel);
    grid->addChild(umaxLabel);
    grid->addChild(vminLabel);
    grid->addChild(vmaxLabel);
    
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    
    prmActiveSync();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
    
    if (reparameterize.getBool())
    {
      umin.setActive(true);
      umax.setActive(true);
      vmin.setActive(true);
      vmax.setActive(true);
    }
    else
    {
      umin.setActive(false);
      umax.setActive(false);
      vmin.setActive(false);
      vmax.setActive(false);
    }
  }
};

Feature::Feature():
Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("MapPCurve");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature(){}

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    
    if (stow->facePick.getPicks().empty()) throw std::runtime_error("no selected face");
    tls::Resolver pr(pIn);
    if (!pr.resolve(stow->facePick.getPicks().front())) throw std::runtime_error("invalid pick resolution for face");
    auto faceShapes = pr.getShapes();
    if (faceShapes.empty()) throw std::runtime_error("no shapes for resolution of face");
    const TopoDS_Shape &face = faceShapes.front();
    if (face.ShapeType() != TopAbs_FACE) throw std::runtime_error("shape for face is not a face");
    TopLoc_Location surfaceLocation;
    opencascade::handle<Geom_Surface> surface = BRep_Tool::Surface(TopoDS::Face(face), surfaceLocation);
    opencascade::handle<Geom_Surface> surfaceCopy;
    
    if (stow->reparameterize.getBool())
    {
      Range uRange{stow->umin.getDouble(), stow->umax.getDouble()};
      Range vRange{stow->vmin.getDouble(), stow->vmax.getDouble()};
      if (!uRange.isValid()) throw std::runtime_error("Invalid U parameter range");
      if (!vRange.isValid()) throw std::runtime_error("Invalid V parameter range");
      auto inputSurfaceAdaptor = opencascade::handle<RangeAdaptor>(new RangeAdaptor(surface, uRange, vRange));
      
      //values from .../occt/src/QABugs/QABugs_19.cxx:404
      //couldnt get GeomAbs_C1 to work with testing face
      GeomConvert_ApproxSurface approx(inputSurfaceAdaptor, 1.0e-4, GeomAbs_C0, GeomAbs_C0, 9, 9, 100, 1);
      if (!approx.IsDone()) throw std::runtime_error("Surface Approximation failed");
      surfaceCopy = approx.Surface();
    }
    else
    {
      //when not doing reparameterize we will set the parameters to the input surface.
      double umin, umax, vmin, vmax;
      surface->Bounds(umin, umax, vmin, vmax);
      stow->umin.setValue(umin);
      stow->umax.setValue(umax);
      stow->vmin.setValue(vmin);
      stow->vmax.setValue(vmax);
      surfaceCopy = dynamic_cast<Geom_Surface*>(surface->Copy().get());
    }
    
    if (surfaceCopy.IsNull()) throw std::runtime_error("Failed to create surface");
    if (stow->uReverse.getBool()) surfaceCopy->UReverse();
    if (stow->vReverse.getBool()) surfaceCopy->VReverse();
    // dumpSurfacePrms(surfaceCopy);
    
    occt::EdgeVector freeEdges;
    occt::WireVector wires;
    tls::ShapeIdContainer tempIdMap; //store shapes and ids to assign to seershape after the fact.
    for (const auto &ep : stow->edgePicks.getPicks())
    {
      if (!pr.resolve(ep)) throw std::runtime_error("invalid pick resolution for edge");
      const ann::SeerShape *iss = pr.getSeerShape();
      if (!iss) throw std::runtime_error("invalid edge seer shape");
      
      gp_Pln refPlane;
      auto buildEdge = [&](const TopoDS_Edge &edge) -> TopoDS_Edge
      {
        assert(iss->hasShape(edge));
        uuid oldEdgeId = iss->findId(edge);
        if (!stow->sShape.hasEvolveRecordIn(oldEdgeId))
          stow->sShape.insertEvolve(oldEdgeId, gu::createRandomId());
        BRepAdaptor_Curve curveAdaptor(edge);
        opencascade::handle<Geom2d_Curve> curve2d = GeomAPI::To2d(curveAdaptor.Curve().Curve(), refPlane);
        BRepBuilderAPI_MakeEdge em(curve2d, surfaceCopy, curveAdaptor.FirstParameter(), curveAdaptor.LastParameter());
        TopoDS_Edge freshEdge = em;
        if (!BRepLib::BuildCurve3d(freshEdge)) throw std::runtime_error("Couldn't build 3d curve");
        freshEdge = TopoDS::Edge(BRepBuilderAPI_Copy(freshEdge)); //remove pcurves and surface
        freshEdge.Location(surfaceLocation);
        tempIdMap.insert(stow->sShape.evolve(oldEdgeId).front(), freshEdge);
        return freshEdge;
      };
      
      auto addWire = [&](const TopoDS_Wire &wire)
      {
        BRepBuilderAPI_MakeWire nwm; //new wire maker.
        for (BRepTools_WireExplorer we(wire); we.More(); we.Next())
        {
          TopoDS_Edge freshEdge = buildEdge(we.Current());
          nwm.Add(freshEdge);
          if (nwm.Error() != BRepBuilderAPI_WireDone) throw std::runtime_error("Adding edge to wire failed");
          const auto &added = nwm.Edge();
          if (!added.IsNull()) tempIdMap.update(freshEdge, added);
        }
        wires.push_back(nwm);
        uuid oldWireId = iss->findId(wire);
        if (!stow->sShape.hasEvolveRecordIn(oldWireId))
          stow->sShape.insertEvolve(oldWireId, gu::createRandomId());
        tempIdMap.insert(stow->sShape.evolve(oldWireId).front(), wires.back());
      };
      
      auto addFreeEdge = [&](const TopoDS_Edge &edge)
      {
        TopoDS_Edge freshEdge = buildEdge(edge);
        if (!BRepLib::BuildCurve3d(freshEdge)) throw std::runtime_error("Couldn't build 3d curve");
        freshEdge.Location(surfaceLocation);
        freeEdges.push_back(freshEdge);
      };
      
      if (slc::isObjectType(ep.selectionType))
      {
        const auto *inputFeature = pr.getFeature();
        auto systemParameters =  inputFeature->getParameters(prm::Tags::CSys);
        if (!systemParameters.empty())
        {
          const prm::Parameter *inputSysParam = inputFeature->getParameters(prm::Tags::CSys).front();
          refPlane = gp_Pln(gu::toOcc(inputSysParam->getMatrix()));
        }
        for (const auto &w : iss->useGetChildrenOfType(iss->getRootOCCTShape(), TopAbs_WIRE))
          addWire(TopoDS::Wire(w));
        for (const auto &e : iss->useGetChildrenOfType(iss->getRootOCCTShape(), TopAbs_EDGE))
        {
          //filter out if edge belongs to a wire as we have processed these.
          if (!iss->useGetParentsOfType(e, TopAbs_WIRE).empty()) continue;
          addFreeEdge(TopoDS::Edge(e));
        }
      }
    }
    
    occt::ShapeVector all;
    all.insert(all.end(), wires.begin(), wires.end());
    all.insert(all.end(), freeEdges.begin(), freeEdges.end());
    TopoDS_Compound out = occt::ShapeVectorCast(all);
    
    ShapeCheck check(out);
    if (!check.isValid())
      throw std::runtime_error("shapeCheck failed");

    stow->sShape.setOCCTShape(out, getId());
    for (const auto &s : stow->sShape.getAllShapes())
    {
      if (tempIdMap.has(s))
        stow->sShape.updateId(s, tempIdMap.find(s));
    }
    //create vertex ids from edges. This is a hack as we loose vertex evolution from input feature.
    //BRepTools_WireExplorer seems to skip the first vertex and it is a PIA to try and sync the
    //vertices between WireExplorer with the WireMaker.
    stow->sShape.derivedMatch();
    stow->sShape.dumpNils(getTypeString());
    stow->sShape.ensureNoNils();
    stow->sShape.dumpDuplicates(getTypeString());
    stow->sShape.ensureNoDuplicates();
    stow->sShape.ensureEvolve();
    
    occt::BoundingBox bb(stow->sShape.getRootOCCTShape());
    stow->grid->setPosition(gu::toOsg(bb.getCenter()));
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

//need to update serial for added parameters and labels

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::mpc::MapPCurve so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->facePick.serialOut()
    , stow->edgePicks.serialOut()
    , stow->uReverse.serialOut()
    , stow->vReverse.serialOut()
    , stow->reparameterize.serialOut()
    , stow->umin.serialOut()
    , stow->umax.serialOut()
    , stow->vmin.serialOut()
    , stow->vmax.serialOut()
    , stow->uReverseLabel->serialOut()
    , stow->vReverseLabel->serialOut()
    , stow->reparameterizeLabel->serialOut()
    , stow->uminLabel->serialOut()
    , stow->umaxLabel->serialOut()
    , stow->vminLabel->serialOut()
    , stow->vmaxLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::mpc::mappcurve(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::mpc::MapPCurve &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.seerShape());
  stow->facePick.serialIn(so.facePick());
  stow->edgePicks.serialIn(so.edgePicks());
  stow->uReverse.serialIn(so.uReverse());
  stow->vReverse.serialIn(so.vReverse());
  stow->reparameterize.serialIn(so.reparameterize());
  stow->umin.serialIn(so.umin());
  stow->umax.serialIn(so.umax());
  stow->vmin.serialIn(so.vmin());
  stow->vmax.serialIn(so.vmax());
  stow->uReverseLabel->serialIn(so.uReverseLabel());
  stow->vReverseLabel->serialIn(so.vReverseLabel());
  stow->reparameterizeLabel->serialIn(so.reparameterizeLabel());
  stow->uminLabel->serialIn(so.uminLabel());
  stow->umaxLabel->serialIn(so.umaxLabel());
  stow->vminLabel->serialIn(so.vminLabel());
  stow->vmaxLabel->serialIn(so.vmaxLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
}

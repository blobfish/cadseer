/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2020 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_MAPPCURVE_H
#define FTR_MAPPCURVE_H

#include "feature/ftrbase.h"

namespace prj{namespace srl{namespace mpc{class MapPCurve;}}}

namespace ftr
{
  namespace MapPCurve
  {
    namespace Tags
    {
      inline constexpr std::string_view facePick = "facePick";
      inline constexpr std::string_view edgePicks = "edgePicks";
      inline constexpr std::string_view uReverse = "uReverse";
      inline constexpr std::string_view vReverse = "vReverse";
      inline constexpr std::string_view reparameterize = "reparameterize";
      inline constexpr std::string_view umin = "umin";
      inline constexpr std::string_view umax = "umax";
      inline constexpr std::string_view vmin = "vmin";
      inline constexpr std::string_view vmax = "vmax";
    }
    class Feature : public Base
    {
    public:
      Feature();
      ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      Type getType() const override {return Type::MapPCurve;}
      const std::string& getTypeString() const override {return toString(Type::MapPCurve);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::mpc::MapPCurve&);
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}

#endif //FTR_MAPPCURVE_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2017  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gp_Ax3.hxx>
#include <gp_Circ.hxx>
#include <gp_Pln.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <TopoDS.hxx>
#include <BRepClass3d.hxx> //outer shell
#include <BRepTools.hxx> //outer wire

#include <osg/Switch>

#include "globalutilities.h"
#include "tools/idtools.h"
#include "preferences/preferencesXML.h"
#include "preferences/prfmanager.h"
#include "annex/annseershape.h"
#include "library/lbripgroup.h"
#include "project/serial/generated/prjsrloblsoblong.h"
#include "annex/anncsysdragger.h"
#include "feature/ftrupdatepayload.h"
#include "feature/ftrprimitive.h"
#include "tools/featuretools.h"
#include "parameter/prmparameter.h"
#include "feature/ftroblong.h"

using namespace ftr::Oblong;
using boost::uuids::uuid;

QIcon Feature::icon = QIcon(":/resources/images/oblong.svg");

//duplicated from box.
enum class FeatureTag
{
  Root,         //!< compound
  Solid,        //!< solid
  Shell,        //!< shell
  FaceXP,       //!< x positive face
  FaceXN,       //!< x negative face
  FaceYP,       //!< y positive face
  FaceYN,       //!< y negative face
  FaceZP,       //!< z positive face
  FaceZN,       //!< z negative face
  WireXP,       //!< x positive wire
  WireXN,       //!< x negative wire
  WireYP,       //!< y positive wire
  WireYN,       //!< y negative wire
  WireZP,       //!< z positive wire
  WireZN,       //!< z negative wire
  EdgeXPYP,     //!< edge at intersection of x positive face and y positive face.
  EdgeXPZP,     //!< edge at intersection of x positive face and z positive face.
  EdgeXPYN,     //!< edge at intersection of x positive face and y negative face.
  EdgeXPZN,     //!< edge at intersection of x positive face and z negative face.
  EdgeXNYN,     //!< edge at intersection of x negative face and y negative face.
  EdgeXNZP,     //!< edge at intersection of x negative face and z positive face.
  EdgeXNYP,     //!< edge at intersection of x negative face and y positive face.
  EdgeXNZN,     //!< edge at intersection of x negative face and z negative face.
  EdgeYPZP,     //!< edge at intersection of y positive face and z positive face.
  EdgeYPZN,     //!< edge at intersection of y positive face and z negative face.
  EdgeYNZP,     //!< edge at intersection of y negative face and z positive face.
  EdgeYNZN,     //!< edge at intersection of y negative face and z negative face.
  VertexXPYPZP, //!< vertex at intersection of faces x+, y+, z+
  VertexXPYNZP, //!< vertex at intersection of faces x+, y-, z+
  VertexXPYNZN, //!< vertex at intersection of faces x+, y-, z-
  VertexXPYPZN, //!< vertex at intersection of faces x+, y+, z-
  VertexXNYNZP, //!< vertex at intersection of faces x-, y-, z+
  VertexXNYPZP, //!< vertex at intersection of faces x-, y+, z+
  VertexXNYPZN, //!< vertex at intersection of faces x-, y+, z-
  VertexXNYNZN  //!< vertex at intersection of faces x-, y-, z-
};

static const std::map<FeatureTag, std::string> featureTagMap =
{
  {FeatureTag::Root, "Root"},
  {FeatureTag::Solid, "Solid"},
  {FeatureTag::Shell, "Shell"},
  {FeatureTag::FaceXP, "FaceXP"},
  {FeatureTag::FaceXN, "FaceXN"},
  {FeatureTag::FaceYP, "FaceYP"},
  {FeatureTag::FaceYN, "FaceYN"},
  {FeatureTag::FaceZP, "FaceZP"},
  {FeatureTag::FaceZN, "FaceZN"},
  {FeatureTag::WireXP, "WireXP"},
  {FeatureTag::WireXN, "WireXN"},
  {FeatureTag::WireYP, "WireYP"},
  {FeatureTag::WireYN, "WireYN"},
  {FeatureTag::WireZP, "WireZP"},
  {FeatureTag::WireZN, "WireZN"},
  {FeatureTag::EdgeXPYP, "EdgeXPYP"},
  {FeatureTag::EdgeXPZP, "EdgeXPZP"},
  {FeatureTag::EdgeXPYN, "EdgeXPYN"},
  {FeatureTag::EdgeXPZN, "EdgeXPZN"},
  {FeatureTag::EdgeXNYN, "EdgeXNYN"},
  {FeatureTag::EdgeXNZP, "EdgeXNZP"},
  {FeatureTag::EdgeXNYP, "EdgeXNYP"},
  {FeatureTag::EdgeXNZN, "EdgeXNZN"},
  {FeatureTag::EdgeYPZP, "EdgeYPZP"},
  {FeatureTag::EdgeYPZN, "EdgeYPZN"},
  {FeatureTag::EdgeYNZP, "EdgeYNZP"},
  {FeatureTag::EdgeYNZN, "EdgeYNZN"},
  {FeatureTag::VertexXPYPZP, "VertexXPYPZP"},
  {FeatureTag::VertexXPYNZP, "VertexXPYNZP"},
  {FeatureTag::VertexXPYNZN, "VertexXPYNZN"},
  {FeatureTag::VertexXPYPZN, "VertexXPYPZN"},
  {FeatureTag::VertexXNYNZP, "VertexXNYNZP"},
  {FeatureTag::VertexXNYPZP, "VertexXNYPZP"},
  {FeatureTag::VertexXNYPZN, "VertexXNYPZN"},
  {FeatureTag::VertexXNYNZN, "VertexXNYNZN"}
};

inline static const prf::Oblong& pOb(){return prf::manager().rootPtr->features().oblong().get();}

struct Feature::Stow
{
  Feature &feature;
  Primitive primitive;
  
  Stow() = delete;
  Stow(Feature &fIn)
  : feature(fIn)
  , primitive(Primitive::Input{fIn, fIn.parameters, fIn.annexes})
  {
    primitive.addLength(pOb().length());
    primitive.addWidth(pOb().width());
    primitive.addHeight(pOb().height());
    
    primitive.lengthIP->setMatrixDims(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(0.0, 0.0, -1.0)));
    primitive.lengthIP->setRotationAxis(osg::Vec3d(1.0, 0.0, 0.0), osg::Vec3d(0.0, 0.0, 1.0));
    
    // width matrixDims are good at default.
    primitive.widthIP->setRotationAxis(osg::Vec3d(0.0, 1.0, 0.0), osg::Vec3d(0.0, 0.0, -1.0));
    
    primitive.heightIP->setMatrixDims(osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(1.0, 0.0, 0.0)));
    primitive.heightIP->setRotationAxis(osg::Vec3d(0.0, 0.0, 1.0), osg::Vec3d(0.0, 1.0, 0.0));
    
    initializeMaps();
  }

  
  void updateIPs()
  {
    primitive.IPsToCsys();
    
    double l = primitive.length->getDouble() / 2.0;
    double w = primitive.width->getDouble() / 2.0;
    double h = primitive.height->getDouble() / 2.0;
    
    {
      auto rotation = osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(0.0, 1.0, 0.0));
      auto translation = osg::Matrixd::translate(0.0, w, h);
      primitive.lengthIP->setMatrixDragger(rotation * translation);
    }
    {
      auto rotation = osg::Matrixd::rotate(osg::PI_2, osg::Vec3d(-1.0, 0.0, 0.0));
      auto translation = osg::Matrixd::translate(l, 0.0, h);
      primitive.widthIP->setMatrixDragger(rotation * translation);
    }
    //no need to rotate
    primitive.heightIP->setMatrixDragger(osg::Matrixd::translate(l, w, 0.0));
  }
  
  //duplicate of box.
  void initializeMaps()
  {
    //result 
    std::vector<uuid> tempIds; //save ids for later.
    for (unsigned int index = 0; index < 35; ++index)
    {
      uuid tempId = gu::createRandomId();
      tempIds.push_back(tempId);
      primitive.sShape.insertEvolve(gu::createNilId(), tempId);
    }
    
    //helper lamda
    auto insertIntoFeatureMap = [this](const uuid &idIn, FeatureTag featureTagIn)
    {
      primitive.sShape.insertFeatureTag(idIn, featureTagMap.at(featureTagIn));
    };
    
    insertIntoFeatureMap(tempIds.at(0), FeatureTag::Root);
    insertIntoFeatureMap(tempIds.at(1), FeatureTag::Solid);
    insertIntoFeatureMap(tempIds.at(2), FeatureTag::Shell);
    insertIntoFeatureMap(tempIds.at(3), FeatureTag::FaceXP);
    insertIntoFeatureMap(tempIds.at(4), FeatureTag::FaceXN);
    insertIntoFeatureMap(tempIds.at(5), FeatureTag::FaceYP);
    insertIntoFeatureMap(tempIds.at(6), FeatureTag::FaceYN);
    insertIntoFeatureMap(tempIds.at(7), FeatureTag::FaceZP);
    insertIntoFeatureMap(tempIds.at(8), FeatureTag::FaceZN);
    insertIntoFeatureMap(tempIds.at(9), FeatureTag::WireXP);
    insertIntoFeatureMap(tempIds.at(10), FeatureTag::WireXN);
    insertIntoFeatureMap(tempIds.at(11), FeatureTag::WireYP);
    insertIntoFeatureMap(tempIds.at(12), FeatureTag::WireYN);
    insertIntoFeatureMap(tempIds.at(13), FeatureTag::WireZP);
    insertIntoFeatureMap(tempIds.at(14), FeatureTag::WireZN);
    insertIntoFeatureMap(tempIds.at(15), FeatureTag::EdgeXPYP);
    insertIntoFeatureMap(tempIds.at(16), FeatureTag::EdgeXPZP);
    insertIntoFeatureMap(tempIds.at(17), FeatureTag::EdgeXPYN);
    insertIntoFeatureMap(tempIds.at(18), FeatureTag::EdgeXPZN);
    insertIntoFeatureMap(tempIds.at(19), FeatureTag::EdgeXNYN);
    insertIntoFeatureMap(tempIds.at(20), FeatureTag::EdgeXNZP);
    insertIntoFeatureMap(tempIds.at(21), FeatureTag::EdgeXNYP);
    insertIntoFeatureMap(tempIds.at(22), FeatureTag::EdgeXNZN);
    insertIntoFeatureMap(tempIds.at(23), FeatureTag::EdgeYPZP);
    insertIntoFeatureMap(tempIds.at(24), FeatureTag::EdgeYPZN);
    insertIntoFeatureMap(tempIds.at(25), FeatureTag::EdgeYNZP);
    insertIntoFeatureMap(tempIds.at(26), FeatureTag::EdgeYNZN);
    insertIntoFeatureMap(tempIds.at(27), FeatureTag::VertexXPYPZP);
    insertIntoFeatureMap(tempIds.at(28), FeatureTag::VertexXPYNZP);
    insertIntoFeatureMap(tempIds.at(29), FeatureTag::VertexXPYNZN);
    insertIntoFeatureMap(tempIds.at(30), FeatureTag::VertexXPYPZN);
    insertIntoFeatureMap(tempIds.at(31), FeatureTag::VertexXNYNZP);
    insertIntoFeatureMap(tempIds.at(32), FeatureTag::VertexXNYPZP);
    insertIntoFeatureMap(tempIds.at(33), FeatureTag::VertexXNYPZN);
    insertIntoFeatureMap(tempIds.at(34), FeatureTag::VertexXNYNZN);
  }
  
  void update()
  {
    double lengthIn = primitive.length->getDouble();
    double widthIn = primitive.width->getDouble();
    double heightIn = primitive.height->getDouble();
    if (lengthIn <= widthIn) throw std::runtime_error("Length should be greater than width");
    double radius = widthIn / 2.0;
    
    //building the base face.
    //build four vertices of base face. starting with lower left and going clockwise as
    //this should reflect the extruded solids orientation
    gp_Pnt point1(radius, 0.0, 0.0);
    gp_Pnt point2(radius, widthIn, 0.0);
    gp_Pnt point3(lengthIn - radius, widthIn, 0.0);
    gp_Pnt point4(lengthIn - radius, 0.0, 0.0);
    TopoDS_Vertex vertex1 = BRepBuilderAPI_MakeVertex(point1);
    TopoDS_Vertex vertex2 = BRepBuilderAPI_MakeVertex(point2);
    TopoDS_Vertex vertex3 = BRepBuilderAPI_MakeVertex(point3);
    TopoDS_Vertex vertex4 = BRepBuilderAPI_MakeVertex(point4);
    
    //linear edges
    TopoDS_Edge bEdge = BRepBuilderAPI_MakeEdge(vertex4, vertex1);
    TopoDS_Edge tEdge = BRepBuilderAPI_MakeEdge(vertex2, vertex3);
    
    //circular edges.
    //left edge
    gp_Pnt lCenter(radius, radius, 0.0);
    gp_Ax2 lAxis(lCenter, gp_Dir(0.0, 0.0, -1.0), gp_Dir(-1.0, 0.0, 0.0));
    gp_Circ lCircle(lAxis, radius);
    TopoDS_Edge lEdge = BRepBuilderAPI_MakeEdge(lCircle, vertex1, vertex2);
    //right edge
    gp_Pnt rCenter(lengthIn - radius, radius, 0.0);
    gp_Ax2 rAxis(lAxis); rAxis.SetLocation(rCenter);
    gp_Circ rCircle(rAxis, radius);
    TopoDS_Edge rEdge = BRepBuilderAPI_MakeEdge(rCircle, vertex3, vertex4);
    
    //make bottom face wire.
    TopoDS_Wire bWire = BRepBuilderAPI_MakeWire(lEdge, tEdge, rEdge, bEdge);
    
    //make bottom face.
    TopoDS_Face bFace = BRepBuilderAPI_MakeFace(gp_Pln(), bWire);
    
    //extrude bottom face
    gp_Vec eVector(0.0, 0.0, 1.0);
    eVector *= heightIn;
    BRepPrimAPI_MakePrism extruder(bFace, eVector, false);
    
    //put shape into position.
    gp_Trsf transformation;
    transformation.SetTransformation(gp_Ax3(gu::toOcc(primitive.csys.getMatrix())));
    transformation.Invert();
    TopoDS_Solid trsfSolid = TopoDS::Solid(extruder.Shape().Located(transformation));
    
    primitive.sShape.setOCCTShape(trsfSolid, feature.getId());
    auto shapes = primitive.sShape.getAllShapes();
    auto findPartner = [&](const TopoDS_Shape &sIn) -> TopoDS_Shape
    {
      if (sIn.IsNull()) return sIn;
      for (const auto &s : shapes)
      {
        if (!primitive.sShape.findId(s).is_nil()) continue;
        if (s.IsPartner(sIn)) return s;
      }
      throw std::runtime_error("Couldn't find partner");
    };
    auto updateShapeByTag = [this](const TopoDS_Shape &shapeIn, FeatureTag featureTagIn) -> TopoDS_Shape
    {
      uuid localId = primitive.sShape.featureTagId(featureTagMap.at(featureTagIn));
      primitive.sShape.updateId(shapeIn, localId);
      return shapeIn;
    };
    
    //add labels to solid and shell.
    updateShapeByTag(trsfSolid, FeatureTag::Solid); //don't need to find partner.
    updateShapeByTag(BRepClass3d::OuterShell(trsfSolid), FeatureTag::Shell);
    
    //add labels to faces. The result faces are the partners
    auto faceZN = updateShapeByTag(findPartner(extruder.FirstShape()), FeatureTag::FaceZN);
    auto faceXN = updateShapeByTag(findPartner(extruder.Generated(lEdge).First()), FeatureTag::FaceXN);
    auto faceYP = updateShapeByTag(findPartner(extruder.Generated(tEdge).First()), FeatureTag::FaceYP);
    auto faceXP = updateShapeByTag(findPartner(extruder.Generated(rEdge).First()), FeatureTag::FaceXP);
    auto faceYN = updateShapeByTag(findPartner(extruder.Generated(bEdge).First()), FeatureTag::FaceYN);
    auto faceZP = updateShapeByTag(findPartner(extruder.LastShape()), FeatureTag::FaceZP);
    
    //add labels to wires.
    updateShapeByTag(BRepTools::OuterWire(TopoDS::Face(faceZN)), FeatureTag::WireZN);
    updateShapeByTag(BRepTools::OuterWire(TopoDS::Face(faceXN)), FeatureTag::WireXN);
    updateShapeByTag(BRepTools::OuterWire(TopoDS::Face(faceYP)), FeatureTag::WireYP);
    updateShapeByTag(BRepTools::OuterWire(TopoDS::Face(faceXP)), FeatureTag::WireXP);
    updateShapeByTag(BRepTools::OuterWire(TopoDS::Face(faceYN)), FeatureTag::WireYN);
    updateShapeByTag(BRepTools::OuterWire(TopoDS::Face(faceZP)), FeatureTag::WireZP);
    
    //add labels to edges
    updateShapeByTag(findPartner(lEdge), FeatureTag::EdgeXNZN);
    updateShapeByTag(findPartner(tEdge), FeatureTag::EdgeYPZN);
    updateShapeByTag(findPartner(rEdge), FeatureTag::EdgeXPZN);
    updateShapeByTag(findPartner(bEdge), FeatureTag::EdgeYNZN);
    updateShapeByTag(findPartner(extruder.Generated(vertex1).First()), FeatureTag::EdgeXNYN);
    updateShapeByTag(findPartner(extruder.Generated(vertex2).First()), FeatureTag::EdgeXNYP);
    updateShapeByTag(findPartner(extruder.Generated(vertex3).First()), FeatureTag::EdgeXPYP);
    updateShapeByTag(findPartner(extruder.Generated(vertex4).First()), FeatureTag::EdgeXPYN);
    updateShapeByTag(findPartner(extruder.LastShape(lEdge)), FeatureTag::EdgeXNZP);
    updateShapeByTag(findPartner(extruder.LastShape(tEdge)), FeatureTag::EdgeYPZP);
    updateShapeByTag(findPartner(extruder.LastShape(rEdge)), FeatureTag::EdgeXPZP);
    updateShapeByTag(findPartner(extruder.LastShape(bEdge)), FeatureTag::EdgeYNZP);
    
    //add labels to vertices.
    updateShapeByTag(findPartner(vertex1), FeatureTag::VertexXNYNZN);
    updateShapeByTag(findPartner(vertex2), FeatureTag::VertexXNYPZN);
    updateShapeByTag(findPartner(vertex3), FeatureTag::VertexXPYPZN);
    updateShapeByTag(findPartner(vertex4), FeatureTag::VertexXPYNZN);
    updateShapeByTag(findPartner(extruder.LastShape(vertex1)), FeatureTag::VertexXNYNZP);
    updateShapeByTag(findPartner(extruder.LastShape(vertex2)), FeatureTag::VertexXNYPZP);
    updateShapeByTag(findPartner(extruder.LastShape(vertex3)), FeatureTag::VertexXPYPZP);
    updateShapeByTag(findPartner(extruder.LastShape(vertex4)), FeatureTag::VertexXPYNZP);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Oblong");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

/*
void Feature::setCSys(const osg::Matrixd &csysIn)
{
  osg::Matrixd oldSystem = csys->getMatrix();
  if (!csys->setValue(csysIn))
    return; // already at this csys
    
  //apply the same transformation to dragger, so dragger moves with it.
  osg::Matrixd diffMatrix = osg::Matrixd::inverse(oldSystem) * csysIn;
  csysDragger->draggerUpdate(csysDragger->dragger->getMatrix() * diffMatrix);
}
*/

void Feature::updateModel(const UpdatePayload &plIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->primitive.sShape.reset();
  try
  {
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    if (!(stow->primitive.length->getDouble() > stow->primitive.width->getDouble()))
      throw std::runtime_error("length must be greater than width");
    
    stow->primitive.csysLinkUpdate(plIn);
    
    stow->update();
    
    mainTransform->setMatrix(osg::Matrixd::identity());
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in oblong update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in oblong update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in oblong update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  stow->updateIPs();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::obls::Oblong oblongOut
  (
    Base::serialOut(),
    stow->primitive.csysType.serialOut(),
    stow->primitive.length->serialOut(),
    stow->primitive.width->serialOut(),
    stow->primitive.height->serialOut(),
    stow->primitive.csys.serialOut(),
    stow->primitive.csysLinked.serialOut(),
    stow->primitive.csysDragger.serialOut(),
    stow->primitive.sShape.serialOut(),
    stow->primitive.lengthIP->serialOut(),
    stow->primitive.widthIP->serialOut(),
    stow->primitive.heightIP->serialOut()
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::obls::oblong(stream, oblongOut, infoMap);
}

void Feature::serialRead(const prj::srl::obls::Oblong &sOblong)
{
  Base::serialIn(sOblong.base());
  stow->primitive.csysType.serialIn(sOblong.csysType());
  stow->primitive.length->serialIn(sOblong.length());
  stow->primitive.width->serialIn(sOblong.width());
  stow->primitive.height->serialIn(sOblong.height());
  stow->primitive.csys.serialIn(sOblong.csys());
  stow->primitive.csysLinked.serialIn(sOblong.csysLinked());
  stow->primitive.csysDragger.serialIn(sOblong.csysDragger());
  stow->primitive.sShape.serialIn(sOblong.seerShape());
  stow->primitive.lengthIP->serialIn(sOblong.lengthIP());
  stow->primitive.widthIP->serialIn(sOblong.widthIP());
  stow->primitive.heightIP->serialIn(sOblong.heightIP());
}

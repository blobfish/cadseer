/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gp_Ax3.hxx>
#include <Adaptor3d_Curve.hxx>
#include <GeomConvert_ApproxCurve.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <Geom_BSplineCurve.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>
#include <ShapeUpgrade_SplitCurve3dContinuity.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "library/lbrcsysdragger.h"
#include "feature/ftrprimitive.h"
#include "parameter/prmparameter.h"
#include "expressions/exprmanager.h"
#include "tools/occtools.h"
#include "tools/idtools.h"
#include "tools/featuretools.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlpcvsparametriccurve.h"
#include "feature/ftrparametriccurve.h"

namespace
{
  template <typename T>
  class Adapter : public Adaptor3d_Curve
  {
  public:
    T &stow;
    double pFirst = 0;
    double pLast = 0;
    bool copy = false;
    
    Adapter(T &sIn) : Adaptor3d_Curve(), stow(sIn)
    {
      pFirst = stow.tMin.getDouble();
      pLast = stow.tMax.getDouble();
    }
    
    Adapter(T &sIn, double pFirstIn, double pLastIn) : Adaptor3d_Curve(), stow(sIn)
    {
      pFirst = pFirstIn;
      pLast = pLastIn;
      copy = true;
    }
    
    Standard_Real FirstParameter()const override {return pFirst;}
    Standard_Real LastParameter() const override {return pLast;}
    GeomAbs_Shape Continuity() const override {return stow.getContinuity(stow.continuity);}
    GeomAbs_CurveType GetType() const override {return GeomAbs_OtherCurve;}
    Standard_Boolean IsClosed() const override {return false;}
    Standard_Boolean IsPeriodic() const override {return false;}
    Handle(Adaptor3d_Curve) Trim (const Standard_Real First, const Standard_Real Last, const Standard_Real /*Tol*/) const override
    {
      //what to do with tolerance?
      //Counted calls here for star example. C0=2, C1=2, C2=6
      //it doesn't appear interval count affects the number of times trim is called.
      //Dumped Tol for star example. All calls had 'Tol1e-09'.
      return opencascade::handle<Adaptor3d_Curve>(new Adapter(stow, First, Last));
    }
    Standard_Integer NbIntervals (const GeomAbs_Shape) const override
    {
      //Question: do the trimmed copies get asked about intervals?
      //because if they do, does that mean we have to take the trimmed range
      //into consideration for 'NbIntervals' and 'Intervals' overrides.
      //initial tests, say 'no' the trimmed adapters don't get queried about intervals.
      //so we will just error on that condition and see if it comes up.
      if (copy) {assert(0); throw std::runtime_error("Unexpected condition");}
      return stow.getIntervals().size() + 1;
    }
    void Intervals (TColStd_Array1OfReal& out, const GeomAbs_Shape) const override
    {
      if (copy) {assert(0); throw std::runtime_error("Unexpected condition");} //see above.
      
      out(out.Lower()) = pFirst;
      
      auto intervals = stow.getIntervals();
      // std::cout << "interval size: " << intervals.size() << std::endl;
      // std::cout << "interval out size: " << out.Length() << std::endl;
      auto it = intervals.begin();
      for (int index = out.Lower() + 1; index < out.Upper(); ++index, ++it) // yes missing equals on test is on purpose
        out(index) = *it;
      
      out(out.Upper()) = pLast;
      // std::cout << "lower: " << out.Lower() << "    upper: " << out.Upper() << std::endl;
    }
    gp_Pnt Value (const Standard_Real U) const override
    {
      stow.updateParameter(U);
      return stow.getD0();
    }
    void D0 (const Standard_Real U, gp_Pnt& P) const override
    {
      stow.updateParameter(U);
      P = stow.getD0();
    }
    void D1 (const Standard_Real U, gp_Pnt& P, gp_Vec& V) const override
    {
      stow.updateParameter(U);
      P = stow.getD0();
      V = stow.getD1();
    }
    void D2 (const Standard_Real U, gp_Pnt& P, gp_Vec& V1, gp_Vec& V2) const override
    {
      stow.updateParameter(U);
      P = stow.getD0();
      V1 = stow.getD1();
      V2 = stow.getD2();
    }
    // void D3 (const Standard_Real U, gp_Pnt& P, gp_Vec& V1, gp_Vec& V2, gp_Vec& V3) const override;
    // gp_Vec DN (const Standard_Real U, const Standard_Integer N) const override;
    Standard_Real Resolution (const Standard_Real R3d) const override {return Precision::Parametric(R3d);}
  };
}

using boost::uuids::uuid;
using namespace ftr::ParametricCurve;
QIcon Feature::icon = QIcon(":/resources/images/parametricCurve.svg");
// should t min and t max be parameters or an expressions?
struct Feature::Stow
{
public:
  Feature &feature;
  Primitive primitive;
  expr::Manager eMan;
  
  prm::Parameter tMin{QObject::tr("t Min"), -8.0, Tags::tMin};
  prm::Parameter tMax{QObject::tr("t Max"), 8.0, Tags::tMax};
  prm::Parameter continuity{QObject::tr("Continuity"), 0, Tags::continuity};
  prm::Parameter maxDegree{QObject::tr("Max Degree"), 12, Tags::maxDegree};
  prm::Parameter maxSegments{QObject::tr("Max Segments"), 100, Tags::maxSegments};
  prm::Parameter tol{QObject::tr("d0 Tolerance"), 0.1, Tags::Tol};
  prm::Parameter split{QObject::tr("Split"), false, Tags::split};
  prm::Parameter splitContinuity{QObject::tr("Split Continuity"), 1, Tags::splitContinuity};
  prm::Parameter splitTol{QObject::tr("Split Tolerance"), 0.001, Tags::splitTol};
  prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  osg::ref_ptr<osg::MatrixTransform> gridTransform{new osg::MatrixTransform()}; //for dragger link
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> tMinLabel{new lbr::PLabel(&tMin)};
  osg::ref_ptr<lbr::PLabel> tMaxLabel{new lbr::PLabel(&tMax)};
  osg::ref_ptr<lbr::PLabel> continuityLabel{new lbr::PLabel(&continuity)};
  osg::ref_ptr<lbr::PLabel> maxDegreeLabel{new lbr::PLabel(&maxDegree)};
  osg::ref_ptr<lbr::PLabel> maxSegmentsLabel{new lbr::PLabel(&maxSegments)};
  osg::ref_ptr<lbr::PLabel> tolLabel{new lbr::PLabel(&tol)};
  osg::ref_ptr<lbr::PLabel> splitLabel{new lbr::PLabel(&split)};
  osg::ref_ptr<lbr::PLabel> splitContinuityLabel{new lbr::PLabel(&splitContinuity)};
  osg::ref_ptr<lbr::PLabel> splitTolLabel{new lbr::PLabel(&splitTol)};
  
  uuid wireId{gu::createRandomId()};
  std::vector<uuid> edgeIds{gu::createRandomId()};
  std::vector<uuid> vertexIds{gu::createRandomId(), gu::createRandomId()};
  
  Stow(Feature &fIn) : feature(fIn)
  , primitive(Primitive::Input{fIn, fIn.parameters, fIn.annexes})
  {
    loadExpressionDefaults();
    
    feature.parameters.push_back(&tMin); tMin.connect(dirtyObserver);
    feature.parameters.push_back(&tMax); tMax.connect(dirtyObserver);
    QStringList tStrings =
    {
      QObject::tr("C0")
      , QObject::tr("C1")
      , QObject::tr("C2")
    };
    continuity.setEnumeration(tStrings);
    continuity.connect(dirtyObserver);
    continuity.connect(syncObserver);
    feature.parameters.push_back(&continuity);
    feature.parameters.push_back(&maxDegree); maxDegree.connect(dirtyObserver);
    feature.parameters.push_back(&maxSegments); maxSegments.connect(dirtyObserver);
    feature.parameters.push_back(&tol); tol.connect(dirtyObserver);
    feature.parameters.push_back(&split); split.connect(dirtyObserver);
    feature.parameters.push_back(&splitContinuity); splitContinuity.setEnumeration(tStrings); splitContinuity.connect(dirtyObserver);
    feature.parameters.push_back(&splitTol); splitTol.connect(dirtyObserver);

    //not useful at this point. See discussion below.
    split.setActive(false);
    splitContinuity.setActive(false);
    splitTol.setActive(false);
    
    feature.overlaySwitch->addChild(gridTransform);
    gridTransform->addChild(grid);
    grid->addChild(tMinLabel);
    grid->addChild(tMaxLabel);
    grid->addChild(continuityLabel); continuityLabel->refresh();
    grid->addChild(maxDegreeLabel);
    grid->addChild(maxSegmentsLabel);
    grid->addChild(tolLabel);
    grid->addChild(splitLabel);
    grid->addChild(splitContinuityLabel);; splitContinuityLabel->refresh();
    grid->addChild(splitTolLabel);
    
    primitive.csysDragger.dragger->linkToMatrix(gridTransform.get());
    
    prmActiveSync();
  }
  
  void prmActiveSync()
  {
    prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    prm::ObserverBlocker syncBlocker(syncObserver);
  }
  
  lcc::Result setExpression(std::string_view name, std::string_view value)
  {
    std::ostringstream stream;
    stream << name << " = " << value;
    return eMan.parseString(stream.str());
  }
  
  void loadExpressionDefaults()
  {
    //no throws.
    //ids 1 - 4. command view assumes this.
    //this establishes the expression types.
    setExpression(Tags::tValue, std::to_string(-8.0));
    setExpression(Tags::d0, "[4.0*t, pow(t,2), 0.0]");
    setExpression(Tags::d1, "[4.0, 2.0*t, 0.0]");
    setExpression(Tags::d2, "[0.0, 2.0, 0.0]");
  }
  
  GeomAbs_Shape getContinuity(const prm::Parameter &pIn) const
  {
    GeomAbs_Shape shape = GeomAbs_C0;
    if (pIn.getInt() > 0) shape = GeomAbs_C1;
    if (pIn.getInt() > 1) shape = GeomAbs_C2;
    if (pIn.getInt() > 2) shape = GeomAbs_C3;
    if (pIn.getInt() > 3) shape = GeomAbs_CN;
    return shape;
  }
  
  TopoDS_Wire buildWire()
  {
    using T = Adapter<decltype(*this)>;
    opencascade::handle<T> adapter = new T(*this);
    GeomConvert_ApproxCurve curveBuilder(adapter, tol.getDouble(), getContinuity(continuity), maxSegments.getInt(), maxDegree.getInt());
    setToMin(); //reset hidden t expression to the t min parameter.
    if (!curveBuilder.IsDone()) throw std::runtime_error("curveBuilder Is Not Done");
    if (!curveBuilder.HasResult()) throw std::runtime_error("No Result From curveBuilder");
    
    BRepBuilderAPI_MakeWire wireMaker;
    auto addEdge = [&](const TopoDS_Edge &eIn)
    {
      wireMaker.Add(eIn);
      if (!wireMaker.IsDone()) throw std::runtime_error("Wire Maker Failed");
    };
    auto addCurve = [&]()
    {
      BRepBuilderAPI_MakeEdge edgeMaker(curveBuilder.Curve(), tMin.getDouble(), tMax.getDouble());
      if (!edgeMaker.IsDone()) throw std::runtime_error("Failed To Create Edge");
      addEdge(edgeMaker.Edge());
    };
    
    if (split.getBool())
    {
      ShapeUpgrade_SplitCurve3dContinuity splitter;
      splitter.Init(curveBuilder.Curve(), tMin.getDouble(), tMax.getDouble());
      splitter.SetCriterion(getContinuity(splitContinuity));
      splitter.SetTolerance(splitTol.getDouble());
      splitter.Perform();
      auto splitResults = splitter.GetCurves();
      if (!splitResults->IsEmpty())
      {
        for (const auto &curve : *splitResults)
        {
          BRepBuilderAPI_MakeEdge edgeMaker(curve);
          if (!edgeMaker.IsDone()) throw std::runtime_error("Failed To Create Split Edge");
          addEdge(edgeMaker.Edge());
        }
      }
      else addCurve();
    }
    else addCurve();
    
    auto out = wireMaker.Wire();
    if (out.IsNull()) throw std::runtime_error("Null Wire");
    return out;
  }
  
  void updateParameter(double param)
  {
    std::ostringstream stream;
    stream << ftr::ParametricCurve::Tags::tValue << " = " << std::to_string(param);
    auto parseResult = eMan.parseString(stream.str());
    if (!parseResult.isAllGood()) throw std::runtime_error(parseResult.getError());
  }
  
  osg::Vec3d getValue(std::string_view name)
  {
    auto ev = eMan.getExpressionValue(name.data());
    expr::VectorVisitor edv;
    return boost::apply_visitor(edv, ev);
  }

  //sets the t expression to the 't min' parameter
  void setToMin()
  {
    updateParameter(tMin.getDouble());
  }
  
  gp_Pnt getD0()
  {
    return gu::toOcc(getValue(Tags::d0)).XYZ();
  }
  
  gp_Vec getD1()
  {
    return gu::toOcc(getValue(Tags::d1));
  }
  
  gp_Vec getD2()
  {
    return gu::toOcc(getValue(Tags::d2));
  }
  
  std::vector<double> getIntervals()
  {
    std::string_view base = "interval";
    std::vector<double> out;
    auto collect = [&](int index) -> bool
    {
      std::string name(base);
      name += std::to_string(index);
      if (!eMan.hasExpression(name)) return false;
      auto value = eMan.getExpressionValue(name);
      expr::DoubleVisitor edv;
      out.push_back(boost::apply_visitor(edv, value));
      return true;
    };
    for (int index = 1; index< maxSegments.getInt(); ++index) if (!collect(index)) break;
    // std::cout << "interval size: " << out.size() << std::endl;
    return out;
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("ParametricCurve");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;
expr::Manager& Feature::getManager() {return stow->eMan;}

/*
Current situation:
the derivatives have to be expressed in equations of t. we have to use an expression manager.

Parametric range is expressed as ftr::Parameters. Derivatives are expressed as expressions
that belong to an expressions manager owned by the parametric curve feature. This means
global expressions can be linked to the range parameters but not to the derivatives. Also
their is no connection between the range paramters and the derivative expressions. This doesn't
feel very natural, should we make the range parameters expressions?

Adapter3d_Curve has the ability to define intervals of continuity. I supported this by looking
for specific expressions. Then added the ability to split the curve by continuity through
ShapeUpgrade_SplitCurve3dContinuity. This isn't working as hoped and adds a lot complexity.
I am going to leave this extra functionality baked into this feature as it maybe useful someday.
However, I am going to hide this from user as it will do nothing but frustrate. Will also be left
out of the file format.

AdvApprox_ApproxAFunction doesn't support continuity above C2. See AdvApprox_ApproxAFunction.cxx:718
 */
void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->primitive.sShape.reset();
  try
  {
    prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    prm::ObserverBlocker syncBlocker(stow->syncObserver);
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    stow->primitive.csysLinkUpdate(pIn);
    
    if ((stow->tMax.getDouble() - stow->tMin.getDouble()) < Precision::Confusion())
      throw std::runtime_error("Invalid Parameter Range");
    
    stow->setToMin(); //always set the hidden t expression to the t min parameter. may throw.
    TopoDS_Wire out = stow->buildWire(); assert(!out.IsNull());
    ShapeCheck check(out);
    if (!check.isValid()) throw std::runtime_error("Shape Check failed");
    
    //put shape into position.
    const auto &localMatrix = stow->primitive.csys.getMatrix();
    gp_Trsf transformation;
    transformation.SetTransformation(gp_Ax3(gu::toOcc(localMatrix)));
    transformation.Invert();
    TopLoc_Location location(transformation);
    out.Location(location);
    
    //assumption here on consistent result index out of shape divide.
    auto &ls = stow->primitive.sShape;
    ls.setOCCTShape(out, getId());
    
    auto wires = ls.useGetChildrenOfType(ls.getRootOCCTShape(), TopAbs_WIRE);
    if (wires.empty()) throw std::runtime_error("No wires in seer shape");
    ls.updateId(wires.front(), stow->wireId);
    
    auto edges = ls.useGetChildrenOfType(ls.getRootOCCTShape(), TopAbs_EDGE);
    if (edges.empty()) throw std::runtime_error("No edges in seer shape");
    while (stow->edgeIds.size() < edges.size()) stow->edgeIds.push_back(gu::createRandomId());
    for (std::size_t index = 0; index < edges.size(); ++index)
      ls.updateId(edges.at(index), stow->edgeIds.at(index));
    
    auto vertices = ls.useGetChildrenOfType(ls.getRootOCCTShape(), TopAbs_WIRE);
    if (vertices.empty()) throw std::runtime_error("No vertices in seer shape");
    while (stow->vertexIds.size() < vertices.size()) stow->vertexIds.push_back(gu::createRandomId());
    
    // ls.dumpShapeIdContainer(std::cout);
    ls.ensureNoNils();
    ls.ensureNoDuplicates();
    ls.ensureEvolve();
    
    //find the boundbox corner farthest from the origin.
    //this keeps the grid label from interfering with the csys dragger.
    occt::BoundingBox bb(out);
    double maxDistance = -1.0;
    gp_Pnt origin(gu::toOcc(stow->primitive.csys.getMatrix().getTrans()).XYZ());
    gp_Pnt gridLocation;
    for (const auto &p : bb.getCorners())
    {
      double currentDistance = p.Distance(origin);
      if (currentDistance > maxDistance)
      {
        gridLocation = p;
        maxDistance = currentDistance;
      }
    }
    stow->grid->setPosition(gu::toOsg(gridLocation));
    stow->gridTransform->setMatrix(osg::Matrixd::identity());
    mainTransform->setMatrix(osg::Matrixd::identity());
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty()) std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::pcvs::ParametricCurve so
  (
    Base::serialOut()
    , stow->eMan.serialOut()
    , stow->primitive.sShape.serialOut()
    , stow->primitive.csysType.serialOut()
    , stow->primitive.csys.serialOut()
    , stow->primitive.csysLinked.serialOut()
    , stow->tMin.serialOut()
    , stow->tMax.serialOut()
    , stow->continuity.serialOut()
    , stow->maxDegree.serialOut()
    , stow->maxSegments.serialOut()
    , stow->tol.serialOut()
    , stow->tMinLabel->serialOut()
    , stow->tMaxLabel->serialOut()
    , stow->continuityLabel->serialOut()
    , stow->maxDegreeLabel->serialOut()
    , stow->maxSegmentsLabel->serialOut()
    , stow->tolLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
    , stow->primitive.csysDragger.serialOut()
    , gu::idToString(stow->wireId)
  );
  for (const auto &eId : stow->edgeIds) so.edgeIds().push_back(gu::idToString(eId));
  for (const auto &vId : stow->vertexIds) so.vertexIds().push_back(gu::idToString(vId));

  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::pcvs::parametriccurve(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::pcvs::ParametricCurve &so)
{
  Base::serialIn(so.base());
  stow->eMan.serialIn(so.eMan());
  stow->primitive.sShape.serialIn(so.seerShape());
  stow->primitive.csysType.serialIn(so.csysType());
  stow->primitive.csys.serialIn(so.csys());
  stow->primitive.csysLinked.serialIn(so.csysLinked());
  stow->tMin.serialIn(so.tMin());
  stow->tMax.serialIn(so.tMax());
  stow->continuity.serialIn(so.continuity());
  stow->maxDegree.serialIn(so.maxDegree());
  stow->maxSegments.serialIn(so.maxSegments());
  stow->tol.serialIn(so.tol());
  stow->tMinLabel->serialIn(so.tMinLabel());
  stow->tMaxLabel->serialIn(so.tMaxLabel());
  stow->continuityLabel->serialIn(so.continuityLabel());
  stow->maxDegreeLabel->serialIn(so.maxDegreeLabel());
  stow->maxSegmentsLabel->serialIn(so.maxSegmentsLabel());
  stow->tolLabel->serialIn(so.tolLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  stow->primitive.csysDragger.serialIn(so.csysDragger());
  stow->wireId = gu::stringToId(so.wireId());
  for (const auto &eId : so.edgeIds()) stow->edgeIds.push_back(gu::stringToId(eId));
  for (const auto &vId : so.vertexIds()) stow->vertexIds.push_back(gu::stringToId(vId));
}

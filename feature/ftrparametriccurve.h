/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_PARAMETRICCURVE_H
#define FTR_PARAMETRICCURVE_H

#include "feature/ftrbase.h"

namespace prj{namespace srl{namespace pcvs{class ParametricCurve;}}}
namespace expr{class Manager;}

namespace ftr
{
  namespace ParametricCurve
  {
    namespace Tags
    {
      inline constexpr std::string_view tMin = "tMin";
      inline constexpr std::string_view tMax = "tMax";
      inline constexpr std::string_view continuity = "continuity";
      inline constexpr std::string_view maxDegree = "maxDegree";
      inline constexpr std::string_view maxSegments = "maxSegments";
      inline constexpr std::string_view d0 = "d0";
      inline constexpr std::string_view d1 = "d1";
      inline constexpr std::string_view d2 = "d2";
      inline constexpr std::string_view Tol = "Tol";
      inline constexpr std::string_view tValue = "t";
      inline constexpr std::string_view split = "split";
      inline constexpr std::string_view splitContinuity = "splitContinuity";
      inline constexpr std::string_view splitTol = "splitTol";
    }
    
    class Feature : public Base
    {
    public:
      Feature();
      ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      Type getType() const override {return Type::ParametricCurve;}
      const std::string& getTypeString() const override {return toString(Type::ParametricCurve);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      expr::Manager& getManager();
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::pcvs::ParametricCurve&);
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}

#endif //FTR_PARAMETRICCURVE_H

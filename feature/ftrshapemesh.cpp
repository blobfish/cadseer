/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <TopoDS.hxx>
#include <BRep_Tool.hxx>
#include <BRepTools.hxx>
#include <TopExp_Explorer.hxx>
#include <BRepBuilderAPI_Copy.hxx>
#include <Poly_Triangulation.hxx>
#include <Poly_Polygon3D.hxx>
#include <Poly_PolygonOnTriangulation.hxx>
#include <GeomLib.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <IMeshTools_Parameters.hxx>

#include <osg/Switch>
#include <osg/Geometry>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "modelviz/mdvnodemaskdefs.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlshmsshapemesh.h"
#include "feature/ftrshapemesh.h"

using boost::uuids::uuid;
using namespace ftr::ShapeMesh;
QIcon Feature::icon = QIcon(":/resources/images/shapeMesh.svg");

struct Feature::Stow
{
  Feature &feature;
  ann::SeerShape sShape;
  
  //At the time of coding these values match the default values in IMeshTools_Parameters
  //But these constructed values are meaningless as we call reset function inside constructor.
  //This means that the reset function will be the master of the default values.
  prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  prm::Parameter angle{QObject::tr("Angle"), 0.5, Tags::angle};
  prm::Parameter deflection{QObject::tr("Deflection"), 0.001, Tags::deflection};
  prm::Parameter angleInterior{QObject::tr("angleInterior"), -1.0, Tags::angleInterior};
  prm::Parameter deflectionInterior{QObject::tr("deflectionInterior"), -1.0, Tags::deflectionInterior};
  prm::Parameter minSize{QObject::tr("minSize"), -1.0, Tags::minSize};
  prm::Parameter inParallel{QObject::tr("inParallel"), false, Tags::inParallel};
  prm::Parameter relative{QObject::tr("relative"), false, Tags::relative};
  prm::Parameter internalVerticesMode{QObject::tr("internalVerticesMode"), true, Tags::internalVerticesMode};
  prm::Parameter controlSurfaceDeflection{QObject::tr("controlSurfaceDeflection"), true, Tags::controlSurfaceDeflection};
  prm::Parameter enableControlSurfaceDeflectionAllSurfaces{QObject::tr("enableControlSurfaceDeflectionAllSurfaces"), false, Tags::enableControlSurfaceDeflectionAllSurfaces};
  prm::Parameter cleanModel{QObject::tr("cleanModel"), true, Tags::cleanModel};
  prm::Parameter adjustMinSize{QObject::tr("adjustMinSize"), false, Tags::adjustMinSize};
  prm::Parameter forceFaceDeflection{QObject::tr("forceFaceDeflection"), false, Tags::forceFaceDeflection};
  prm::Parameter allowQualityDecrease{QObject::tr("allowQualityDecrease"), false, Tags::allowQualityDecrease};
  
  prm::Observer dirtyModelObserver{std::bind(&Feature::setModelDirty, &feature)};
  prm::Observer dirtyVisualObserver{std::bind(&Feature::setModelDirty, &feature)};
  
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  //no label for picks
  osg::ref_ptr<lbr::PLabel> angleLabel{new lbr::PLabel(&angle)};
  osg::ref_ptr<lbr::PLabel> deflectionLabel{new lbr::PLabel(&deflection)};
  osg::ref_ptr<lbr::PLabel> angleInteriorLabel{new lbr::PLabel(&angleInterior)};
  osg::ref_ptr<lbr::PLabel> deflectionInteriorLabel{new lbr::PLabel(&deflectionInterior)};
  osg::ref_ptr<lbr::PLabel> minSizeLabel{new lbr::PLabel(&minSize)};
  osg::ref_ptr<lbr::PLabel> inParallelLabel{new lbr::PLabel(&inParallel)};
  osg::ref_ptr<lbr::PLabel> relativeLabel{new lbr::PLabel(&relative)};
  osg::ref_ptr<lbr::PLabel> internalVerticesModeLabel{new lbr::PLabel(&internalVerticesMode)};
  osg::ref_ptr<lbr::PLabel> controlSurfaceDeflectionLabel{new lbr::PLabel(&controlSurfaceDeflection)};
  osg::ref_ptr<lbr::PLabel> enableControlSurfaceDeflectionAllSurfacesLabel{new lbr::PLabel(&enableControlSurfaceDeflectionAllSurfaces)};
  osg::ref_ptr<lbr::PLabel> cleanModelLabel{new lbr::PLabel(&cleanModel)};
  osg::ref_ptr<lbr::PLabel> adjustMinSizeLabel{new lbr::PLabel(&adjustMinSize)};
  osg::ref_ptr<lbr::PLabel> forceFaceDeflectionLabel{new lbr::PLabel(&forceFaceDeflection)};
  osg::ref_ptr<lbr::PLabel> allowQualityDecreaseLabel{new lbr::PLabel(&allowQualityDecrease)};
  
  Stow(Feature &fIn) : feature(fIn)
  {
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    
    picks.connect(dirtyModelObserver); feature.parameters.push_back(&picks);
    
    auto connectVisual = [&](prm::Parameter *p) {p->connect(dirtyVisualObserver); feature.parameters.push_back(p);};
    connectVisual(&angle);
    connectVisual(&deflection);
    connectVisual(&angleInterior);
    connectVisual(&deflectionInterior);
    connectVisual(&minSize);
    connectVisual(&inParallel);
    connectVisual(&relative);
    connectVisual(&internalVerticesMode);
    connectVisual(&controlSurfaceDeflection);
    connectVisual(&enableControlSurfaceDeflectionAllSurfaces);
    connectVisual(&cleanModel);
    connectVisual(&adjustMinSize);
    connectVisual(&forceFaceDeflection);
    connectVisual(&allowQualityDecrease);
    
    feature.overlaySwitch->addChild(grid);
    grid->addChild(angleLabel);
    grid->addChild(deflectionLabel);
    grid->addChild(angleInteriorLabel);
    grid->addChild(deflectionInteriorLabel);
    grid->addChild(minSizeLabel);
    grid->addChild(inParallelLabel);
    grid->addChild(relativeLabel);
    grid->addChild(internalVerticesModeLabel);
    grid->addChild(controlSurfaceDeflectionLabel);
    grid->addChild(enableControlSurfaceDeflectionAllSurfacesLabel);
    grid->addChild(cleanModelLabel);
    grid->addChild(adjustMinSizeLabel);
    grid->addChild(forceFaceDeflectionLabel);
    grid->addChild(allowQualityDecreaseLabel);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("ShapeMesh");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
  resetParameters();
}

Feature::~Feature() = default;

void Feature::setOCCT(const IMeshTools_Parameters &mpIn)
{
  //parameters, if changed, will set the visual feature dirty
  stow->angle.setValue(mpIn.Angle);
  stow->deflection.setValue(mpIn.Deflection);
  stow->angleInterior.setValue(mpIn.AngleInterior);
  stow->deflectionInterior.setValue(mpIn.DeflectionInterior);
  stow->minSize.setValue(mpIn.MinSize);
  stow->inParallel.setValue(mpIn.InParallel);
  stow->relative.setValue(mpIn.Relative);
  stow->internalVerticesMode.setValue(mpIn.InternalVerticesMode);
  stow->controlSurfaceDeflection.setValue(mpIn.ControlSurfaceDeflection);
  stow->enableControlSurfaceDeflectionAllSurfaces.setValue(mpIn.EnableControlSurfaceDeflectionAllSurfaces);
  stow->cleanModel.setValue(mpIn.CleanModel);
  stow->adjustMinSize.setValue(mpIn.AdjustMinSize);
  stow->forceFaceDeflection.setValue(mpIn.ForceFaceDeflection);
  stow->allowQualityDecrease.setValue(mpIn.AllowQualityDecrease);
}

IMeshTools_Parameters Feature::getOCCT() const
{
  IMeshTools_Parameters out;
  out.Angle = stow->angle.getDouble();
  out.Deflection = stow->deflection.getDouble();
  out.AngleInterior = stow->angleInterior.getDouble();
  out.DeflectionInterior = stow->deflectionInterior.getDouble();
  out.MinSize = stow->minSize.getDouble();
  out.InParallel = stow->inParallel.getBool();
  out.Relative = stow->relative.getBool();
  out.InternalVerticesMode = stow->internalVerticesMode.getBool();
  out.ControlSurfaceDeflection = stow->controlSurfaceDeflection.getBool();
  out.EnableControlSurfaceDeflectionAllSurfaces = stow->enableControlSurfaceDeflectionAllSurfaces.getBool();
  out.CleanModel = stow->cleanModel.getBool();
  out.AdjustMinSize = stow->adjustMinSize.getBool();
  out.ForceFaceDeflection = stow->forceFaceDeflection.getBool();
  out.AllowQualityDecrease = stow->allowQualityDecrease.getBool();
  return out;
}

void Feature::resetParameters()
{
  setOCCT(IMeshTools_Parameters());
}

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    prm::ObserverBlocker dirtyModelBlocker(stow->dirtyModelObserver);
    prm::ObserverBlocker dirtyVisualBlocker(stow->dirtyVisualObserver);
    
    if (isSkipped()) {setSuccess(); throw std::runtime_error("feature is skipped");}
    
    const auto &thePicks = stow->picks.getPicks();
    if (thePicks.empty()) throw std::runtime_error("no shapes input");
    tls::Resolver pr(pIn);
    if (!pr.resolve(thePicks.front())) throw std::runtime_error("invalid pick resolution");
    auto shapes = pr.getShapes();
    if (shapes.empty()) throw std::runtime_error("no shapes");
    
    // put all non compound shapes under 1 compound.
    // I really don't already have something for this?
    occt::ShapeVector allShapes;
    for (const auto &s : shapes)
    {
      auto cs = occt::getNonCompounds(s);
      for (const auto &ncs : cs) allShapes.emplace_back(ncs);
    }
    if (allShapes.empty()) throw std::runtime_error("No compound shape");
    TopoDS_Compound out = occt::ShapeVectorCast(allShapes);
    
    //copy that so we don't alter any of the input data.
    BRepBuilderAPI_Copy copier(out);
    stow->sShape.setOCCTShape(copier.Shape(), getId());
    
    //we don't care about ids as this object isn't really going to be a workable shape
    stow->sShape.ensureNoNils();
    stow->sShape.ensureEvolve();
    
    occt::BoundingBox bb(copier.Shape());
    stow->grid->setPosition(gu::toOsg(bb.getCenter()));
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::updateVisual()
{
  mainTransform->removeChildren(0, mainTransform->getNumChildren());
  
  auto rootShape = stow->sShape.getRootOCCTShape();
  BRepTools::Clean(rootShape);
  BRepMesh_IncrementalMesh(rootShape, getOCCT());
  
  auto buildFace = [&](const TopoDS_Face &faceIn)
  {
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
    geom->setNodeMask(mdv::noIntersect);
    geom->setName("faces");
    geom->setDataVariance(osg::Object::STATIC);
    
    auto *vertices = new osg::Vec3Array();
    geom->setVertexArray(vertices);
    geom->setUseDisplayList(false);
    
    auto *colors = new osg::Vec4Array(1); (*colors)[0] = osg::Vec4(0.88, 0.2, 0.88, 1.0);
    geom->setColorArray(colors);
    geom->setColorBinding(osg::Geometry::BIND_OVERALL);
    
    auto *normals = new osg::Vec3Array();
    geom->setNormalArray(normals);
    geom->setNormalBinding(osg::Geometry::BIND_PER_VERTEX);
    
    TopLoc_Location location;
    const Handle(Poly_Triangulation) &triangulation = BRep_Tool::Triangulation(faceIn, location);
    if (triangulation.IsNull()) throw std::runtime_error("null triangulation in face construction");
    
    bool isReversed = (faceIn.Orientation() == TopAbs_REVERSED);
    gp_Trsf transformation;
    bool isIdentity = location.IsIdentity();
    transformation = location.Transformation();
    
    //vertices.
    unsigned int offset = vertices->size();
    for (int index = 1; index <= triangulation->NbNodes(); ++index)
    {
      gp_Pnt point = triangulation->Node(index);
      if(!isIdentity) point.Transform(transformation);
      vertices->push_back(osg::Vec3(point.X(), point.Y(), point.Z()));
    }
    
    //normals.
    assert(triangulation->HasUVNodes());
    opencascade::handle<Geom_Surface> surface = BRep_Tool::Surface(faceIn);
    if (surface.IsNull()) throw std::runtime_error("null surface in face construction");
    for (int index = 1; index <= triangulation->NbNodes(); ++index)
    {
      gp_Dir direction;
      int result = GeomLib::NormEstim(surface, triangulation->UVNode(index), Precision::Confusion(), direction);
      if (result != 0) lastUpdateLog += "WARNING: GeomLib::NormEstim failed";
      if (isReversed) direction.Reverse();
      normals->push_back(osg::Vec3(direction.X(), direction.Y(), direction.Z()));
    }
    
    osg::ref_ptr<osg::DrawElementsUInt> indices = new osg::DrawElementsUInt(GL_TRIANGLES, triangulation->NbTriangles() * 3);
    for (int index = 1; index <= triangulation->NbTriangles(); ++index)
    {
      int N1, N2, N3;
      triangulation->Triangle(index).Get(N1, N2, N3);
      
      int factor = (index - 1) * 3;
      if (isReversed)
      {
        (*indices)[factor] = N3 - 1  + offset;
        (*indices)[factor + 1] = N2 - 1 + offset;
        (*indices)[factor + 2] = N1 - 1 + offset;
      }
      else
      {
        (*indices)[factor] = N1 - 1 + offset;
        (*indices)[factor + 1] = N2 - 1 + offset;
        (*indices)[factor + 2] = N3 - 1 + offset;
      }
    }
    geom->addPrimitiveSet(indices.get());
    mainTransform->addChild(geom.get());
  };
  
  auto buildEdge = [&](const TopoDS_Edge &edgeIn)
  {
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
    geom->setNodeMask(mdv::noIntersect);
    geom->setName("edges");
    geom->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    geom->setDataVariance(osg::Object::STATIC);
    geom->setUseDisplayList(false);
    
    auto *vertices = new osg::Vec3Array();
    geom->setVertexArray(vertices);
    
    auto *colors = new osg::Vec4Array(1); (*colors)[0] = osg::Vec4(0.88, 0.2, 0.88, 1.0);
    geom->setColorArray(colors);
    geom->setColorBinding(osg::Geometry::BIND_OVERALL);
    
    TopLoc_Location location;
    const opencascade::handle<Poly_Polygon3D>& poly = BRep_Tool::Polygon3D(edgeIn, location);
    if (poly.IsNull())  throw std::runtime_error("edge triangulation is null");
    
    gp_Trsf transformation;
    bool identity = location.IsIdentity();
    transformation = location.Transformation();
    
    osg::ref_ptr<osg::DrawElementsUInt> indices = new osg::DrawElementsUInt(GL_LINE_STRIP);
    const TColgp_Array1OfPnt& nodes = poly->Nodes();
    indices->resizeElements(nodes.Size());
    unsigned int tempIndex = 0;
    for (auto point : nodes) //can't const ref, might transform.
    {
      if (!identity) point.Transform(transformation);
      vertices->push_back(osg::Vec3(point.X(), point.Y(), point.Z()));
      (*indices)[tempIndex] = vertices->size() - 1;
      tempIndex++;
    }
    
    geom->addPrimitiveSet(indices.get());
    mainTransform->addChild(geom.get());
  };
  
  //not too worried about performance here.
  TopTools_MapOfShape processed;
  TopExp_Explorer ex;
  for (ex.Init(rootShape, TopAbs_FACE); ex.More(); ex.Next())
  {
    const auto &f = ex.Value();
    if (!processed.Add(f)) continue;
    buildFace(TopoDS::Face(f));
  }
  for (ex.Init(rootShape, TopAbs_EDGE, TopAbs_FACE); ex.More(); ex.Next())
  {
    const auto &e = ex.Value();
    if (!processed.Add(e)) continue;
    buildEdge(TopoDS::Edge(e));
  }
  
  setVisualClean();
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::shms::ShapeMesh so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->picks.serialOut()
    , stow->angle.serialOut()
    , stow->deflection.serialOut()
    , stow->angleInterior.serialOut()
    , stow->deflectionInterior.serialOut()
    , stow->minSize.serialOut()
    , stow->inParallel.serialOut()
    , stow->relative.serialOut()
    , stow->internalVerticesMode.serialOut()
    , stow->controlSurfaceDeflection.serialOut()
    , stow->enableControlSurfaceDeflectionAllSurfaces.serialOut()
    , stow->cleanModel.serialOut()
    , stow->adjustMinSize.serialOut()
    , stow->forceFaceDeflection.serialOut()
    , stow->allowQualityDecrease.serialOut()
    , stow->angleLabel->serialOut()
    , stow->deflectionLabel->serialOut()
    , stow->angleInteriorLabel->serialOut()
    , stow->deflectionInteriorLabel->serialOut()
    , stow->minSizeLabel->serialOut()
    , stow->inParallelLabel->serialOut()
    , stow->relativeLabel->serialOut()
    , stow->internalVerticesModeLabel->serialOut()
    , stow->controlSurfaceDeflectionLabel->serialOut()
    , stow->enableControlSurfaceDeflectionAllSurfacesLabel->serialOut()
    , stow->cleanModelLabel->serialOut()
    , stow->adjustMinSizeLabel->serialOut()
    , stow->forceFaceDeflectionLabel->serialOut()
    , stow->allowQualityDecreaseLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
  );
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::shms::shapemesh(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::shms::ShapeMesh &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.seerShape());
  stow->picks.serialIn(so.picks());
  
  stow->angle.serialIn(so.angle());
  stow->deflection.serialIn(so.deflection());
  stow->angleInterior.serialIn(so.angleInterior());
  stow->deflectionInterior.serialIn(so.deflectionInterior());
  stow->minSize.serialIn(so.minSize());
  stow->inParallel.serialIn(so.inParallel());
  stow->relative.serialIn(so.relative());
  stow->internalVerticesMode.serialIn(so.internalVerticesMode());
  stow->controlSurfaceDeflection.serialIn(so.controlSurfaceDeflection());
  stow->enableControlSurfaceDeflectionAllSurfaces.serialIn(so.enableControlSurfaceDeflectionAllSurfaces());
  stow->cleanModel.serialIn(so.cleanModel());
  stow->adjustMinSize.serialIn(so.adjustMinSize());
  stow->forceFaceDeflection.serialIn(so.forceFaceDeflection());
  stow->allowQualityDecrease.serialIn(so.allowQualityDecrease());
  
  stow->angleLabel->serialIn(so.angleLabel());
  stow->deflectionLabel->serialIn(so.deflectionLabel());
  stow->angleInteriorLabel->serialIn(so.angleInteriorLabel());
  stow->deflectionInteriorLabel->serialIn(so.deflectionInteriorLabel());
  stow->minSizeLabel->serialIn(so.minSizeLabel());
  stow->inParallelLabel->serialIn(so.inParallelLabel());
  stow->relativeLabel->serialIn(so.relativeLabel());
  stow->internalVerticesModeLabel->serialIn(so.internalVerticesModeLabel());
  stow->controlSurfaceDeflectionLabel->serialIn(so.controlSurfaceDeflectionLabel());
  stow->enableControlSurfaceDeflectionAllSurfacesLabel->serialIn(so.enableControlSurfaceDeflectionAllSurfacesLabel());
  stow->cleanModelLabel->serialIn(so.cleanModelLabel());
  stow->adjustMinSizeLabel->serialIn(so.adjustMinSizeLabel());
  stow->forceFaceDeflectionLabel->serialIn(so.forceFaceDeflectionLabel());
  stow->allowQualityDecreaseLabel->serialIn(so.allowQualityDecreaseLabel());
  
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
}

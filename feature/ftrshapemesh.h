/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_SHAPEMESH_H
#define FTR_SHAPEMESH_H

#include "feature/ftrbase.h"

struct IMeshTools_Parameters;
namespace prj{namespace srl{namespace shms{class ShapeMesh;}}}

namespace ftr
{
  namespace ShapeMesh
  {
    namespace Tags
    {
      inline constexpr std::string_view angle = "angle";
      inline constexpr std::string_view deflection = "deflection";
      inline constexpr std::string_view angleInterior = "angleInterior";
      inline constexpr std::string_view deflectionInterior = "deflectionInterior";
      inline constexpr std::string_view minSize = "minSize";
      inline constexpr std::string_view inParallel = "inParallel";
      inline constexpr std::string_view relative = "relative";
      inline constexpr std::string_view internalVerticesMode = "internalVerticesMode";
      inline constexpr std::string_view controlSurfaceDeflection = "controlSurfaceDeflection";
      inline constexpr std::string_view enableControlSurfaceDeflectionAllSurfaces = "enableControlSurfaceDeflectionAllSurfaces";
      inline constexpr std::string_view cleanModel = "cleanModel";
      inline constexpr std::string_view adjustMinSize = "adjustMinSize";
      inline constexpr std::string_view forceFaceDeflection = "forceFaceDeflection";
      inline constexpr std::string_view allowQualityDecrease = "allowQualityDecrease";
    }
    
    /* Not really a function shape feature. This is just to have full control
     * over the meshing variables outside of cadseer default LOD visual shapes.
     */
    class Feature : public Base
    {
    public:
      Feature();
      ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      void updateVisual() override;
      Type getType() const override {return Type::ShapeMesh;}
      const std::string& getTypeString() const override {return toString(Type::ShapeMesh);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      void setOCCT(const IMeshTools_Parameters&);
      IMeshTools_Parameters getOCCT() const;
      void resetParameters();
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::shms::ShapeMesh&);
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}

#endif //FTR_SHAPEMESH_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <optional>

#include <gp_Ax2.hxx>
#include <gp_Ax3.hxx>
#include <gp_Circ.hxx>
#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <TopExp_Explorer.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>
#include <ShapeFix_Wire.hxx>
#include <Geom_BezierCurve.hxx>
#include <BOPAlgo_Builder.hxx>

#include <osg/Switch>
#include <osg/MatrixTransform>
#include <osg/PositionAttitudeTransform>

#include "globalutilities.h"
#include "tools/idtools.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/tlsshapeid.h"
#include "library/lbrcsysdragger.h"
#include "annex/annseershape.h"
#include "annex/anncsysdragger.h"
#include "sketch/sktsolver.h"
#include "sketch/sktvisual.h"
#include "project/serial/generated/prjsrlsktssketch.h"
#include "parameter/prmparameter.h"
#include "feature/ftrupdatepayload.h"
#include "feature/ftrprimitive.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrsketch.h"

using namespace ftr::Sketch;

using boost::uuids::uuid;

QIcon Feature::icon = QIcon(":/resources/images/sketch.svg");

struct Feature::Stow
{
  Feature &feature;
  Primitive primitive;
  skt::Solver solver;
  skt::Visual visual;
  std::vector<boost::uuids::uuid> wireIds;
  std::vector<std::pair<uint32_t, std::shared_ptr<prm::Parameter>>> hpPairs;
  osg::ref_ptr<osg::Switch> draggerSwitch{new osg::Switch()};
  
  Stow() = delete;
  Stow(Feature &fIn)
  : feature(fIn)
  , primitive(Primitive::Input{fIn, fIn.parameters, fIn.annexes})
  , solver()
  , visual(solver)
  {
    primitive.csysDragger.dragger->linkToMatrix(visual.getTransform());
    feature.overlaySwitch->addChild(visual.getTransform());
    visual.update();
    
    //we need to control visual of csysDragger.
    feature.overlaySwitch->removeChild(primitive.csysDragger.dragger);
    draggerSwitch->addChild(primitive.csysDragger.dragger);
    feature.overlaySwitch->addChild(draggerSwitch.get());
  }
  
  bool hasHPPair(uint32_t hIn)
  {
    for (const auto &p : hpPairs)
    {
      if (p.first == hIn)
        return true;
    }
    return false;
  }

  bool hasHPPair(const prm::Parameter *pIn)
  {
    for (const auto &p : hpPairs)
    {
      if (p.second.get() == pIn)
        return true;
    }
    return false;
  }

  void addHPPair(uint32_t hIn, const std::shared_ptr<prm::Parameter> &pIn)
  {
    assert(!hasHPPair(hIn));
    hpPairs.push_back(std::make_pair(hIn, pIn));
    feature.parameters.push_back(pIn.get());
    pIn->connectValue(std::bind(&Feature::setModelDirty, &feature));
  }

  prm::Parameter* getHPParameter(uint32_t hIn)
  {
    assert(hasHPPair(hIn));
    for (const auto &p : hpPairs)
    {
      if (p.first == hIn)
        return p.second.get();
    }
    return nullptr;
  }

  uint32_t getHPHandle(const prm::Parameter *pIn)
  {
    assert(hasHPPair(pIn));
    for (const auto &p : hpPairs)
    {
      if (p.second.get() == pIn)
        return p.first;
    }
    return 0;
  }
  
  void updateConstraintValue(const prm::Parameter *prmIn)
  {
    solver.updateConstraintValue(getHPHandle(prmIn), prmIn->getDouble());
    solver.solve(solver.getGroup(), true);
  }
  
  void updateSeerShape()
  {
    auto convertVertex = [&](skt::SSHandle pIn) -> TopoDS_Vertex
    {
      auto pe = solver.findEntity(pIn); assert(pe);
      osg::Vec3d point = osg::Vec3d(*solver.getParameterValue(pe->get().param[0]), *solver.getParameterValue(pe->get().param[1]), 0.0);
      return BRepBuilderAPI_MakeVertex(gu::toOcc(point).XYZ());
    };
    auto convertPoint = [&](skt::SSHandle pIn) -> osg::Vec3d
    {
      auto pe = solver.findEntity(pIn);
      assert(pe);
      osg::Vec3d point = osg::Vec3d(*solver.getParameterValue(pe->get().param[0]), *solver.getParameterValue(pe->get().param[1]), 0.0);
      return point;
    };
    auto convert = [&](skt::SSHandle hIn) -> std::optional<TopoDS_Edge>
    {
      auto entity = solver.findEntity(hIn);
      assert(entity); if (!entity) return std::nullopt;
      switch (entity->get().type)
      {
        case SLVS_E_LINE_SEGMENT:
        {
          BRepBuilderAPI_MakeEdge em(convertVertex(entity->get().point[0]), convertVertex(entity->get().point[1]));
          return em.Edge();
        }
        case SLVS_E_CIRCLE:
        {
          auto de = solver.findEntity(entity->get().distance);
          assert(de);
          double radius = *solver.getParameterValue(de->get().param[0]);
          //this doesn't need connection
          osg::Vec3d p0 = convertPoint(entity->get().point[0]);
          gp_Circ circle;
          circle.SetLocation(gp_Pnt(gu::toOcc(p0).XYZ()));
          circle.SetRadius(radius);
          BRepBuilderAPI_MakeEdge em(circle);
          return em.Edge();
        }
        case SLVS_E_ARC_OF_CIRCLE:
        {
          osg::Vec3d p0 = convertPoint(entity->get().point[0]);
          osg::Vec3d p1 = convertPoint(entity->get().point[1]);
          gp_Circ circle;
          circle.SetLocation(gp_Pnt(gu::toOcc(p0).XYZ()));
          circle.SetRadius((p1 - p0).length());
          BRepBuilderAPI_MakeEdge em(circle, convertVertex(entity->get().point[1]), convertVertex(entity->get().point[2]));
          return em.Edge();
        }
        case SLVS_E_CUBIC:
        {
          TColgp_Array1OfPnt occtPoints(1, 4);
          occtPoints.SetValue(1, gp_Pnt(gu::toOcc(convertPoint(entity->get().point[0])).XYZ()));
          occtPoints.SetValue(2, gp_Pnt(gu::toOcc(convertPoint(entity->get().point[1])).XYZ()));
          occtPoints.SetValue(3, gp_Pnt(gu::toOcc(convertPoint(entity->get().point[2])).XYZ()));
          occtPoints.SetValue(4, gp_Pnt(gu::toOcc(convertPoint(entity->get().point[3])).XYZ()));
          opencascade::handle<Geom_BezierCurve> c = new Geom_BezierCurve(occtPoints);
          return BRepBuilderAPI_MakeEdge(c, convertVertex(entity->get().point[0]), convertVertex(entity->get().point[3])).Edge();
        }
        default: {return std::nullopt; break;}
      }
      return std::nullopt;
    };
    
    tls::ShapeIdContainer tempShapeIds;
    auto common = [&](const skt::Solver::Chain &chainIn) -> occt::EdgeVector
    {
      occt::EdgeVector edges;
      for (auto h : chainIn)
      {
        auto oe = convert(h);
        assert(oe); if (!oe) continue;
        edges.push_back(*oe);
        auto eid = visual.getEntityId(h);
        tempShapeIds.insert(eid, *oe);
        if (!primitive.sShape.hasEvolveRecordIn(eid)) primitive.sShape.insertEvolve(gu::createNilId(), eid);
      }
      return edges;
    };
    auto goBOP = [&](const occt::EdgeVector &edges) -> std::optional<TopTools_DataMapOfShapeListOfShape>
    {
      BOPAlgo_Builder bop;
      for (const auto &e : edges) bop.AddArgument(e);
      bop.Perform();
      if (bop.HasErrors()) return std::nullopt;
      return bop.Images();
    };
    
    occt::WireVector wires;
    auto buildManifoldWire = [&](const skt::Solver::Chain &chainIn)
    {
      // screw trying to create vertices shared by edges.
      // just build edges with duplicated vertices and let bopalgo_builder sort it out.
      occt::EdgeVector edges = common(chainIn); if (edges.empty()) return;
      BRepBuilderAPI_MakeWire wm;
      if (edges.size() == 1) //don't run through bop single edge.
      {
        wm.Add(edges.front()); if (!wm.IsDone()) return;
      }
      else
      {
        auto images = goBOP(edges); if (!images) return;
        for (const auto &e : edges) //build wire and update ids altered by bop.
        {
          if (!images->IsBound(e)) continue;
          const auto &bindingList = images->Find(e);
          if (bindingList.Size() != 1) continue; //unexpected count of bound items.
          const auto &image = bindingList.First();
          if (!e.IsSame(image)) tempShapeIds.update(e, image);
          wm.Add(TopoDS::Edge(image)); if (!wm.IsDone()) return;
        }
      }
      wires.push_back(wm.Wire());
    };
    auto buildNonManifoldWire = [&](const skt::Solver::Chain &chainIn)
    {
      // still going to use bop to connect applicable edges
      // just not going to worry about edge order or orientation
      // non manifold chain can't have single edge like manifold so no consideration for condition
      occt::EdgeVector edges = common(chainIn); if (edges.empty()) return;
      auto images = goBOP(edges); if (!images) return;
      BRep_Builder builder;
      TopoDS_Wire nmWire;
      builder.MakeWire(nmWire);
      for (const auto &e : edges)
      {
        if (!images->IsBound(e)) continue;
        const auto &bindingList = images->Find(e);
        if (bindingList.Size() != 1) continue; //unexpected count of bound items.
        const auto &image = bindingList.First();
        if (!e.IsSame(image)) tempShapeIds.update(e, image);
        builder.Add(nmWire, image);
      }
      wires.push_back(nmWire);
    };
    
    skt::Solver::Chains manifold, nonManifold;
    std::tie(manifold, nonManifold) = solver.getOrderedChains([&](Slvs_hEntity h){return !visual.isConstruction(h);});
    for (const auto &mc : manifold) buildManifoldWire(mc);
    for (const auto &nmc : nonManifold) buildNonManifoldWire(nmc);
    
    if (wires.empty()) throw std::runtime_error("No wires from sketch");
    while (wireIds.size() < wires.size())
    {
      wireIds.push_back(gu::createRandomId());
      primitive.sShape.insertEvolve(gu::createNilId(), wireIds.back());
    }
    
    gp_Trsf transformation;
    gp_Ax2 ax2System = gu::toOcc(primitive.csys.getMatrix());
    transformation.SetTransformation(gp_Ax3(ax2System));
    transformation.Invert();
    TopLoc_Location location(transformation);
    occt::ShapeVector trsfWires;
    for (std::size_t i = 0; i < wires.size(); ++i)
    {
      tempShapeIds.insert(wireIds.at(i), wires.at(i));
      trsfWires.push_back(wires.at(i).Located(location));
    }
    
    primitive.sShape.setOCCTShape(static_cast<TopoDS_Compound>(occt::ShapeVectorCast(trsfWires)), feature.getId());
    for (auto &s : primitive.sShape.getAllNilShapes())
    {
      auto base = s.Located(TopLoc_Location());
      if (!tempShapeIds.has(base)) continue;
      primitive.sShape.updateId(s, tempShapeIds.find(base));
    }
    
    primitive.sShape.derivedMatch(); //ids vertices.
    // primitive.sShape.dumpShapeIdContainer(std::cout);
    primitive.sShape.ensureNoNils();
    primitive.sShape.ensureNoDuplicates();
    
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))

{
  name = QObject::tr("Sketch");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::draggerShow()
{
  stow->draggerSwitch->setAllChildrenOn();
}

void Feature::draggerHide()
{
  stow->draggerSwitch->setAllChildrenOff();
}

//! @details Setup a default sketch.
void Feature::buildDefault(const osg::Matrixd &mIn, double sizeIn)
{
  stow->solver.setWorkPlane();
  stow->solver.createXAxis();
  stow->solver.createYAxis();
  stow->solver.groupIncrement();
  
  stow->primitive.csys.setValue(mIn);
  stow->primitive.csysDragger.resetDragger();
  stow->visual.getTransform()->setMatrix(mIn);
  stow->visual.setSize(std::max(Precision::Confusion(), sizeIn));
  stow->visual.update();
  stow->visual.setAutoSize(true); //setSize disables.
}

skt::Visual* Feature::getVisual()
{
  return &stow->visual;
}

void Feature::addHPPair(uint32_t hIn, const std::shared_ptr<prm::Parameter> &pIn)
{
  stow->addHPPair(hIn, pIn);
}

prm::Parameters Feature::getObsoleteParameters()
{
  prm::Parameters out;
  for (auto it = stow->hpPairs.begin(); it != stow->hpPairs.end(); ++it)
    if (!stow->solver.hasConstraint(it->first)) out.push_back(it->second.get());
  return out;
}

void Feature::removeObsoleteParameters()
{
  for (auto it = stow->hpPairs.begin(); it != stow->hpPairs.end();)
  {
    if (!stow->solver.hasConstraint(it->first))
    {
      removeParameter(it->second.get());
      it = stow->hpPairs.erase(it);
    }
    else
      it++;
  }
}

void Feature::updateModel(const UpdatePayload &plIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->primitive.sShape.reset();
  try
  {
    prm::ObserverBlocker block(stow->primitive.csysObserver);
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    stow->primitive.csysLinkUpdate(plIn);
    
    //set solver constraints to parameters.
    for (const auto &p : stow->hpPairs)
      stow->solver.updateConstraintValue(p.first, p.second->getDouble());
    
    stow->solver.solve(stow->solver.getGroup(), true);
    if (stow->solver.getResultCode() != 0)
      throw std::runtime_error(stow->solver.getResultMessage());
    stow->visual.update();
    
    stow->updateSeerShape();
    mainTransform->setMatrix(osg::Matrixd::identity());
    stow->visual.getTransform()->setMatrix(stow->primitive.csys.getMatrix());
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in sketch update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in sketch update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in sketch update. " << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::skts::Sketch so
  (
    Base::serialOut()
    , stow->primitive.sShape.serialOut()
    , stow->solver.serialOut()
    , stow->visual.serialOut()
    , stow->primitive.csysType.serialOut()
    , stow->primitive.csys.serialOut()
    , stow->primitive.csysLinked.serialOut()
    , stow->primitive.csysDragger.serialOut()
  );
  
  for (const auto &wId : stow->wireIds)
    so.wireIds().push_back(gu::idToString(wId));
  
  for (const auto &hp : stow->hpPairs)
    so.handleParameterPairs().push_back(prj::srl::skts::HandleParameterPair(hp.first, hp.second->serialOut()));
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::skts::sketch(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::skts::Sketch &sIn)
{
  Base::serialIn(sIn.base());
  stow->primitive.sShape.serialIn(sIn.seerShape());
  stow->solver.serialIn(sIn.solver());
  stow->visual.serialIn(sIn.visual());
  stow->primitive.csysType.serialIn(sIn.csysType());
  stow->primitive.csys.serialIn(sIn.csys());
  stow->primitive.csysLinked.serialIn(sIn.csysLinked());
  stow->primitive.csysDragger.serialIn(sIn.csysDragger());
  
  for (const auto &wId : sIn.wireIds())
    stow->wireIds.push_back(gu::stringToId(wId));
  
  //kind of a hack, having to look up location.
  auto findLocation = [&](skt::SSHandle h) -> std::optional<osg::Vec3d>
  {
    for (const auto &r : sIn.visual().constraintMap())
    {
      if (r.handle() == h)
        return osg::Vec3d(r.location().x(), r.location().y(), r.location().z());
    }
    return std::nullopt;
  };
  for (const auto &pair : sIn.handleParameterPairs())
  {
    std::shared_ptr<prm::Parameter> p = std::make_shared<prm::Parameter>(QString(), 0.0);
    p->serialIn(pair.parameter());
    stow->addHPPair(pair.handle(), p);
    auto location = findLocation(pair.handle());
    if (location)
      stow->visual.connect(pair.handle(), p.get(), *location);
    else
      std::cout << "ERROR: in finding location of constraint in sketch serial read " << std::endl;
  }
  
  stow->visual.getTransform()->setMatrix(stow->primitive.csys.getMatrix());
  stow->solver.solve(stow->solver.getGroup(), true);
  stow->visual.update();
}

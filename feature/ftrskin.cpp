/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <BRepOffsetAPI_ThruSections.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepTools.hxx>
#include <TopoDS.hxx>
#include <TopExp.hxx>

#include <osg/Switch>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/idtools.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
#include "project/serial/generated/prjsrlsknsskin.h"
#include "feature/ftrskin.h"

using boost::uuids::uuid;
using namespace ftr::Skin;
QIcon Feature::icon = QIcon(":/resources/images/skin.svg");

//looked at occt source to get default values.
struct Feature::Stow
{
  Feature &feature;
  ann::SeerShape sShape;
  
  prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  prm::Parameter solid{QObject::tr("Solid"), false, Tags::solid};
  prm::Parameter ruled{QObject::tr("Ruled"), false, Tags::ruled};
  prm::Parameter tolerance{QObject::tr("Tolerance"), 1.0e-6, Tags::tolerance};
  prm::Parameter compatibility{QObject::tr("Compatibility"), true, Tags::compatibility};
  prm::Parameter smooth{QObject::tr("Smooth"), false, Tags::smooth};
  prm::Parameter weights{QObject::tr("Weights"), osg::Vec3d(0.4, 0.2, 0.4), Tags::weights};
  prm::Parameter maxDegree{QObject::tr("Max Degree"), 8, Tags::maxDegree};
  prm::Parameter parametricType{QObject::tr("Parametric Type"), 0, Tags::parametricType}; //ChordLength
  prm::Parameter continuity{QObject::tr("Continuity"), 4, Tags::continuity}; // C2
  prm::Observer observer{std::bind(&Feature::setModelDirty, &feature)};
  
  osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  osg::ref_ptr<lbr::PLabel> solidLabel{new lbr::PLabel(&solid)};
  osg::ref_ptr<lbr::PLabel> ruledLabel{new lbr::PLabel(&ruled)};
  osg::ref_ptr<lbr::PLabel> toleranceLabel{new lbr::PLabel(&tolerance)};
  osg::ref_ptr<lbr::PLabel> compatibilityLabel{new lbr::PLabel(&compatibility)};
  osg::ref_ptr<lbr::PLabel> smoothLabel{new lbr::PLabel(&smooth)};
  osg::ref_ptr<lbr::PLabel> weightsLabel{new lbr::PLabel(&weights)};
  osg::ref_ptr<lbr::PLabel> maxDegreeLabel{new lbr::PLabel(&maxDegree)};
  osg::ref_ptr<lbr::PLabel> parametricTypeLabel{new lbr::PLabel(&parametricType)};
  osg::ref_ptr<lbr::PLabel> continuityLabel{new lbr::PLabel(&continuity)};
  
  uuid solidId{gu::createRandomId()};
  uuid shellId{gu::createRandomId()};
  uuid firstFace{gu::createRandomId()};
  uuid lastFace{gu::createRandomId()};
  
  std::map<uuid, uuid> outerWireMap; //!< map face id to wire id
  using InstanceMap = std::map<uuid, std::vector<uuid>>;
  InstanceMap instanceMap; //edges and face.
  
  Stow(Feature &fIn) : feature(fIn)
  {
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    
    auto bind = [&](prm::Parameter &p, lbr::PLabel *l)
    {
      p.connect(observer);
      feature.parameters.push_back(&p);
      grid->addChild(l);
    };
    picks.connect(observer); feature.parameters.push_back(&picks);
    bind(solid, solidLabel);
    bind(ruled, ruledLabel);
    bind(tolerance, toleranceLabel);
    bind(compatibility, compatibilityLabel);
    bind(smooth, smoothLabel);
    bind(weights, weightsLabel);
    bind(maxDegree, maxDegreeLabel);
    
    QStringList pTypeStrings =
    {
      QObject::tr("Chord Length")
      , QObject::tr("Centripetal")
      , QObject::tr("IsoParametric")
    };
    parametricType.setEnumeration(pTypeStrings);
    bind(parametricType, parametricTypeLabel);
    parametricTypeLabel->refresh();
    
    QStringList continuityStrings =
    {
      QObject::tr("C0")
      , QObject::tr("G1")
      , QObject::tr("C1")
      , QObject::tr("G2")
      , QObject::tr("C2")
      , QObject::tr("C3")
    };
    continuity.setEnumeration(continuityStrings);
    bind(continuity, continuityLabel);
    continuityLabel->refresh();
    
    feature.overlaySwitch->addChild(grid);
    
    sShape.insertEvolve(gu::createNilId(), solidId);
    sShape.insertEvolve(gu::createNilId(), shellId);
    sShape.insertEvolve(gu::createNilId(), firstFace);
    sShape.insertEvolve(gu::createNilId(), lastFace);
  }
  
  void prmActiveSync()
  {
    // prm::ObserverBlocker blocker(observer);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Skin");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    prm::ObserverBlocker blocker(stow->observer);
    if (isSkipped()) {setSuccess(); throw std::runtime_error("feature is skipped");}
    
    const auto &thePicks = stow->picks.getPicks();
    if (thePicks.empty()) throw std::runtime_error("No Picks");
    
    tls::Resolver pr(pIn);
    BRepOffsetAPI_ThruSections skinner(stow->solid.getBool(), stow->ruled.getBool(), stow->tolerance.getDouble());
    occt::BoundingBox bb;
    std::vector<std::reference_wrapper<const ann::SeerShape>> inputs;
    auto addShapes = [&](const occt::ShapeVector &wsIn)
    {
      occt::ShapeVectorCast castMan(wsIn);
      bb.add(static_cast<occt::ShapeVector>(castMan));
      occt::WireVector wires = castMan; for (const auto &w : wires) skinner.AddWire(w);
      occt::VertexVector vertices = castMan; for (const auto &v : vertices) skinner.AddVertex(v);
      occt::EdgeVector edges = castMan; for (const auto &e : edges) skinner.AddWire(BRepBuilderAPI_MakeWire(e));
    };
    auto addResolved = [&]()
    {
      switch (pr.getPick().selectionType)
      {
        case slc::Type::Object: case slc::Type::Feature:
        {
          //can't imagine any feature without a seershape being useful here.
          //Use first acceptable geometry? Use all acceptable geometry?
          //Skin is sensitive to the order of input, so the chances of the right order are small.
          const auto *ss = pr.getSeerShape(); if (!ss) return;
          auto wires = ss->useGetChildrenOfType(ss->getRootOCCTShape(), TopAbs_WIRE);
          auto edges = ss->useGetChildrenOfType(ss->getRootOCCTShape(), TopAbs_EDGE);
          if (!wires.empty()) addShapes(wires);
          else if(!edges.empty()) addShapes(edges);
          break;
        }
        case slc::Type::Wire: case slc::Type::Edge: case slc::Type::StartPoint: case slc::Type::EndPoint:
          {addShapes(pr.getShapes()); break;}
        default: {break;}
      };
    };
    for (const auto &p : thePicks)
      if (pr.resolve(p))
      {
        addResolved();
        if (pr.getSeerShape()) inputs.push_back(*pr.getSeerShape());
      }
      else throw std::runtime_error("Invalid pick resolution");
    
    stow->grid->setPosition(gu::toOsg(bb.getCenter()));
    
    skinner.CheckCompatibility(stow->compatibility.getBool());
    skinner.SetSmoothing(stow->smooth.getBool());
    const auto &w = stow->weights.getVector(); skinner.SetCriteriumWeight(w.x(), w.y(), w.z());
    skinner.SetMaxDegree(stow->maxDegree.getInt());
    skinner.SetParType(static_cast<Approx_ParametrizationType>(stow->parametricType.getInt()));
    skinner.SetContinuity(static_cast<GeomAbs_Shape>(stow->continuity.getInt()));
    
    //boat is loaded.
    skinner.Build();
    skinner.Check(); //throws occt error.
    const auto &out = skinner.Shape();
    ShapeCheck check(out); if (!check.isValid()) throw std::runtime_error("shapeCheck failed");
    
    /* skinner.Modified:
     *   Not getting anything out of modified.
     *   Not even when the edges are split for compatibility.
     *   Not eve when we use ruled.
     *   I checked the occt code and modified isn't used for anything.
     * skinner.Generated:
     *   Not designed for vertices of edges, unless punctual/degenerated... or segfault.
     *   different edges map to same generated face.
     *   same behavior using ruled, which is wrong.
     *   looks like Generated is all about faces from edges.
     *     Any talk about vertices is all about degenerated edges/wires pass in as profiles,
     * skinner.GeneratedFace:
     *   This looks accurate for edges.
     *   Everything is lost when using compatible wires.
     * SeerShape::shapeMatch:
     *   Is matching vertices and edges with ruled = true.
     *   matching only vertices with ruled = false.
     *   when we have 2 sections, we match like ruled = true regardless of ruled value.
     *   this makes sense as 'sweeping' will make spline edges.
     * skinner.Wires() returns the exact same as what was put into it. Any modifications
     *   for compatibility are not reflected in these wires.
     * Another occt mess!!!! I am just going to use Generated and instance the results of each edge
     *   in the first wire. Incomplete as we get some duplicates.
     */
    stow->sShape.setOCCTShape(out, getId());
    for (const auto &rw : inputs) stow->sShape.shapeMatch(rw);
    for (const auto &s : stow->sShape.getAllNilShapes())
    {
      if (s.ShapeType() == TopAbs_SOLID) stow->sShape.updateId(s, stow->solidId);
      if (s.ShapeType() == TopAbs_SHELL) stow->sShape.updateId(s, stow->shellId);
      if (s == skinner.FirstShape()) stow->sShape.updateId(skinner.FirstShape(), stow->firstFace);
      if (s == skinner.LastShape()) stow->sShape.updateId(skinner.LastShape(), stow->lastFace);
    }
    
    //Instancing from input edges to result faces.
    const ann::SeerShape *css = nullptr;
    uuid cId = gu::createNilId();
    auto findInputId = [&](const TopoDS_Shape &inputShape)
    {
      css = nullptr;
      cId = gu::createNilId();
      for (const auto &ss : inputs)
      {
        if (!ss.get().hasShape(inputShape)) continue;
        css = &ss.get();
        cId = ss.get().findId(inputShape);
        break;
      }
    };
    for (const auto &s : skinner.Wires())
    {
      TopTools_IndexedMapOfShape freshShapeMap;
      TopExp::MapShapes(s, TopAbs_EDGE, freshShapeMap);
      for (int index = 1; index <= freshShapeMap.Extent(); ++index)
      {
        const auto &cs = freshShapeMap(index);
        const auto &generated = skinner.Generated(cs);
        if (generated.Size() == 0) continue;
        findInputId(cs); if (cId.is_nil()) continue;
        auto mapIt = stow->instanceMap.insert(std::make_pair(cId, Stow::InstanceMap::mapped_type())).first;
        while (static_cast<int>(mapIt->second.size()) < generated.Size())
          mapIt->second.push_back(gu::createRandomId());
        std::size_t gIndex = 0;
        for (const auto &gs : generated)
        {
          if (!stow->sShape.hasShape(gs)) continue; //assert? //warning in log?
          stow->sShape.updateId(gs, mapIt->second.at(gIndex));
          gIndex++;
        }
      }
    }
    
    //now do outerwires from faces.
    auto faces = stow->sShape.useGetChildrenOfType(stow->sShape.getRootOCCTShape(), TopAbs_FACE);
    for (const auto &f : faces)
    {
      uuid fId = stow->sShape.findId(f); if (fId.is_nil()) continue;
      auto mapIt = stow->outerWireMap.insert(std::make_pair(fId, gu::createRandomId())).first;
      if (!stow->sShape.hasEvolveRecordOut(mapIt->second))
        stow->sShape.insertEvolve(gu::createNilId(), mapIt->second);
      const TopoDS_Wire &ow = BRepTools::OuterWire(TopoDS::Face(f));
      stow->sShape.updateId(ow, mapIt->second);
    }
    
    stow->sShape.derivedMatch(); //derivedMatch is producing duplicates.
    stow->sShape.dumpNils("skin feature");
    stow->sShape.dumpDuplicates("skin feature");
    // stow->sShape.dumpShapeIdContainer(std::cout);
    stow->sShape.ensureNoNils();
    stow->sShape.ensureNoDuplicates();
    stow->sShape.ensureEvolve();
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::skns::Skin so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->picks.serialOut()
    , stow->solid.serialOut()
    , stow->ruled.serialOut()
    , stow->tolerance.serialOut()
    , stow->compatibility.serialOut()
    , stow->smooth.serialOut()
    , stow->weights.serialOut()
    , stow->maxDegree.serialOut()
    , stow->parametricType.serialOut()
    , stow->continuity.serialOut()
    , stow->solidLabel->serialOut()
    , stow->ruledLabel->serialOut()
    , stow->toleranceLabel->serialOut()
    , stow->compatibilityLabel->serialOut()
    , stow->smoothLabel->serialOut()
    , stow->weightsLabel->serialOut()
    , stow->maxDegreeLabel->serialOut()
    , stow->parametricTypeLabel->serialOut()
    , stow->continuityLabel->serialOut()
    , lbr::PLabelGridCallback::serialOut(stow->grid)
    , gu::idToString(stow->solidId)
    , gu::idToString(stow->shellId)
    , gu::idToString(stow->firstFace)
    , gu::idToString(stow->lastFace)
  );
  
  using SRL = prj::srl::spt::SeerShape;
  for (const auto &me : stow->outerWireMap)
    so.outerWireMap().push_back(SRL::EvolveContainerType(gu::idToString(me.first), gu::idToString(me.second)));
  
  for (const auto &p : stow->instanceMap)
  {
    prj::srl::skns::Instance instanceOut(gu::idToString(p.first));
    for (const auto &id : p.second) instanceOut.values().push_back(gu::idToString(id));
    so.instanceMap().push_back(instanceOut);
  }
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::skns::skin(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::skns::Skin &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.seerShape());
  stow->picks.serialIn(so.picks());
  
  stow->solid.serialIn(so.solid());
  stow->ruled.serialIn(so.ruled());
  stow->tolerance.serialIn(so.tolerance());
  stow->compatibility.serialIn(so.compatibility());
  stow->smooth.serialIn(so.smooth());
  stow->weights.serialIn(so.weights());
  stow->maxDegree.serialIn(so.maxDegree());
  stow->parametricType.serialIn(so.parametricType());
  stow->continuity.serialIn(so.continuity());
  stow->solidLabel->serialIn(so.solidLabel());
  stow->ruledLabel->serialIn(so.ruledLabel());
  stow->toleranceLabel->serialIn(so.toleranceLabel());
  stow->compatibilityLabel->serialIn(so.compatibilityLabel());
  stow->smoothLabel->serialIn(so.smoothLabel());
  stow->weightsLabel->serialIn(so.weightsLabel());
  stow->maxDegreeLabel->serialIn(so.maxDegreeLabel());
  stow->parametricTypeLabel->serialIn(so.parametricTypeLabel());
  stow->continuityLabel->serialIn(so.continuityLabel());
  lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
  
  stow->solidId = gu::stringToId(so.solidId());
  stow->shellId = gu::stringToId(so.shellId());
  stow->firstFace = gu::stringToId(so.firstFace());
  stow->lastFace = gu::stringToId(so.lastFace());
  
  for (const auto &owEntry : so.outerWireMap())
    stow->outerWireMap.insert(std::make_pair(gu::stringToId(owEntry.idIn()), gu::stringToId(owEntry.idOut())));
  
  for (const auto &iEntry : so.instanceMap())
  {
    std::vector<uuid> values;
    for (const auto &v : iEntry.values()) values.emplace_back(gu::stringToId(v));
    stow->instanceMap.insert(std::make_pair(gu::stringToId(iEntry.key()), values));
  }
}

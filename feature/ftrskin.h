/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_SKIN_H
#define FTR_SKIN_H

#include "feature/ftrbase.h"

namespace prj{namespace srl{namespace skns{class Skin;}}}

namespace ftr
{
  namespace Skin
  {
    namespace Tags
    {
      inline constexpr std::string_view solid = "solid";
      inline constexpr std::string_view ruled = "ruled";
      inline constexpr std::string_view tolerance = "tolerance";
      inline constexpr std::string_view compatibility = "compatibility";
      inline constexpr std::string_view smooth = "smooth";
      inline constexpr std::string_view parametricType = "parametricType";
      inline constexpr std::string_view continuity = "continuity";
      inline constexpr std::string_view weights = "weights";
      inline constexpr std::string_view maxDegree = "maxDegree";
    }
    
    class Feature : public Base
    {
    public:
      Feature();
      ~Feature() override;
      
      void updateModel(const UpdatePayload&) override;
      Type getType() const override {return Type::Skin;}
      const std::string& getTypeString() const override {return toString(Type::Skin);}
      const QIcon& getIcon() const override {return icon;}
      Descriptor getDescriptor() const override {return Descriptor::Create;}
      
      void serialWrite(const std::filesystem::path&) override;
      void serialRead(const prj::srl::skns::Skin&);
    private:
      static QIcon icon;
      struct Stow;
      std::unique_ptr<Stow> stow;
    };
  }
}

#endif //FTR_SKIN_H

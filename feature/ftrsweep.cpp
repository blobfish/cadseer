/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <optional>

#include <TopoDS.hxx>
#include <BRepFill_PipeShell.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepTools_WireExplorer.hxx>
#include <BRepTools.hxx>
#include <TopExp_Explorer.hxx>
#include <gp_Ax2.hxx>
#include <Law_Function.hxx>
#include <TColStd_HArray1OfReal.hxx> //ProjectCurveOnSurface needs this? occt 7.3
#include <ShapeConstruct_ProjectCurveOnSurface.hxx>
#include <BRep_Tool.hxx>
#include <BRep_Builder.hxx>

#include <osg/Switch>
#include <osg/AutoTransform>

#include "globalutilities.h"
#include "annex/annseershape.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "library/lbrplabel.h"
#include "library/lbrplabelgrid.h"
#include "tools/occtools.h"
#include "tools/featuretools.h"
#include "tools/idtools.h"
#include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
#include "feature/ftrdatumaxis.h"
#include "law/lwfvessel.h"
#include "feature/ftrlawspine.h"
#include "project/serial/generated/prjsrlswpssweep.h"
#include "feature/ftrsweep.h"

using namespace ftr::Sweep;
using boost::uuids::uuid;

QIcon Feature::icon = QIcon(":/resources/images/sweep.svg");

Profile::Profile()
: pick(QObject::tr("Profile"), ftr::Picks(), Tags::profile)
, contact(QObject::tr("Contact"), false, Tags::none)
, correction(QObject::tr("Correction"), false, Tags::none)
, contactLabel(new lbr::PLabel(&contact))
, correctionLabel(new lbr::PLabel(&correction))
, grid(lbr::PLabelGridCallback::buildGrid(1))
{
  grid->addChild(contactLabel);
  grid->addChild(correctionLabel);
}

Profile::Profile(const prj::srl::swps::SweepProfile &pfIn) : Profile()
{
  pick.serialIn(pfIn.pick());
  contact.serialIn(pfIn.contact());
  correction.serialIn(pfIn.correction());
  contactLabel->serialIn(pfIn.contactLabel());
  correctionLabel->serialIn(pfIn.correctionLabel());
  if (pfIn.grid()) lbr::PLabelGridCallback::serialIn(pfIn.grid().get(), grid);
}

Profile::~Profile() noexcept = default;

prj::srl::swps::SweepProfile Profile::serialOut() const
{
  prj::srl::swps::SweepProfile out
  (
    pick.serialOut()
    , contact.serialOut()
    , correction.serialOut()
    , contactLabel->serialOut()
    , correctionLabel->serialOut()
  );
  out.grid() = lbr::PLabelGridCallback::serialOut(grid);
  return out;
}

prm::Parameters Profile::getParameters()
{
  return {&pick, &contact, &correction};
}

Auxiliary::Auxiliary()
: pick(QObject::tr("Auxiliary Picks"), ftr::Picks(), Tags::auxPick)
, curvilinearEquivalence(QObject::tr("Curvelinear Equivalence"), false, Tags::auxCurvilinear)
, contactType(QObject::tr("Contact Type"), 0, Tags::auxContact)
, curvilinearEquivalenceLabel(new lbr::PLabel(&curvilinearEquivalence))
, contactTypeLabel(new lbr::PLabel(&contactType))
, grid(lbr::PLabelGridCallback::buildGrid(1))
{
  QStringList tStrings =
  {
    QObject::tr("No Contact")
    , QObject::tr("Contact")
    , QObject::tr("Contact On Border")
  };
  contactType.setEnumeration(tStrings);
  contactTypeLabel->refresh();
  grid->addChild(curvilinearEquivalenceLabel);
  grid->addChild(contactTypeLabel);
}

Auxiliary::Auxiliary(const prj::srl::swps::SweepAuxiliary &auxIn)
:Auxiliary()
{
  serialIn(auxIn);
}

Auxiliary::~Auxiliary() noexcept = default;

void Auxiliary::setActive(bool stateIn)
{
  pick.setActive(stateIn);
  curvilinearEquivalence.setActive(stateIn);
  contactType.setActive(stateIn);
}

prj::srl::swps::SweepAuxiliary Auxiliary::serialOut() const
{
  prj::srl::swps::SweepAuxiliary out
  (
    pick.serialOut()
    , curvilinearEquivalence.serialOut()
    , contactType.serialOut()
    , curvilinearEquivalenceLabel->serialOut()
    , contactTypeLabel->serialOut()
  );
  out.grid() = lbr::PLabelGridCallback::serialOut(grid);
  return out;
}

void Auxiliary::serialIn(const prj::srl::swps::SweepAuxiliary &auxIn)
{
  pick.serialIn(auxIn.pick());
  curvilinearEquivalence.serialIn(auxIn.curvilinearEquivalence());
  contactType.serialIn(auxIn.contactType());
  curvilinearEquivalenceLabel->serialIn(auxIn.curvilinearEquivalenceLabel());
  contactTypeLabel->serialIn(auxIn.contactTypeLabel());
  if (auxIn.grid()) lbr::PLabelGridCallback::serialIn(auxIn.grid().get(), grid);
}

Binormal::Binormal()
: picks(QObject::tr("Binormal Picks"), ftr::Picks(), Tags::biPicks)
, vector(QObject::tr("Vector"), osg::Vec3d(), Tags::biVector)
, vectorLabel(new lbr::PLabel(&vector))
{}

Binormal::~Binormal() noexcept = default;

prj::srl::swps::SweepBinormal Binormal::serialOut() const
{
  prj::srl::swps::SweepBinormal out
  (
    picks.serialOut()
    , vector.serialOut()
    , vectorLabel->serialOut()
  );
  return out;
}
void Binormal::serialIn(const prj::srl::swps::SweepBinormal &bnIn)
{
  picks.serialIn(bnIn.picks());
  vector.serialIn(bnIn.binormal());
  vectorLabel->serialIn(bnIn.binormalLabel());
}

void Binormal::setActive(bool stateIn)
{
  picks.setActive(stateIn);
  if (stateIn && picks.getPicks().empty())
    vector.setActive(true);
  else
    vector.setActive(false);
}

struct Feature::Stow
{
  Feature &feature;
  
  prm::Parameter spine{QObject::tr("Spine Pick"), ftr::Picks(), Tags::spine};
  prm::Parameter support{QObject::tr("Support Pick"), ftr::Picks(), Tags::support};
  prm::Parameter trihedron{QObject::tr("Trihedron"), 0, Tags::trihedron};
  prm::Parameter transition{QObject::tr("Transition"), 0, Tags::transition};
  prm::Parameter forceC1{QObject::tr("Force C1"), false, Tags::forcec1};
  prm::Parameter solid{QObject::tr("Solid"), false, Tags::solid};
  prm::Observer prmObserver{std::bind(&Feature::setModelDirty, &feature)};
  
  Profiles profiles;
  Auxiliary auxiliary;
  Binormal binormal;
  
  ann::SeerShape sShape;
  
  osg::ref_ptr<lbr::PLabel> trihedronLabel{new lbr::PLabel(&trihedron)};
  osg::ref_ptr<lbr::PLabel> transitionLabel{new lbr::PLabel(&transition)};
  osg::ref_ptr<lbr::PLabel> forceC1Label{new lbr::PLabel(&forceC1)};
  osg::ref_ptr<lbr::PLabel> solidLabel{new lbr::PLabel(&solid)};
  osg::ref_ptr<osg::AutoTransform> grid{lbr::PLabelGridCallback::buildGrid(1)};
  
  osg::ref_ptr<osg::Switch> auxiliarySwitch{new osg::Switch()};
  osg::ref_ptr<osg::Switch> binormalSwitch{new osg::Switch()};
  
  uuid solidId{gu::createRandomId()};
  uuid shellId{gu::createRandomId()};
  uuid firstFaceId{gu::createRandomId()};
  uuid lastFaceId{gu::createRandomId()};
  std::map<uuid, uuid> outerWireMap; //!< map face id to wire id
  typedef std::map<uuid, std::vector<uuid>> InstanceMap;
  InstanceMap instanceMap; //for profile vertices and edges -> edges and face.
  std::map<uuid, uuid> firstShapeMap; //faceId to edgeId for first shapes.
  std::map<uuid, uuid> lastShapeMap; //faceId to edgeId for last shapes.
  
  Stow() = delete;
  Stow(Feature &fIn)
  : feature(fIn)
  {
    spine.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&spine);
    
    support.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&support);
    
    QStringList tStrings =
    {
      QObject::tr("Corrected Frenet")
      , QObject::tr("Frenet")
      , QObject::tr("Discrete")
      , QObject::tr("Fixed")
      , QObject::tr("Constant Binormal")
      , QObject::tr("Support")
      , QObject::tr("Auxiliary")
    };
    trihedron.setEnumeration(tStrings);
    trihedron.connectValue(std::bind(&Feature::setModelDirty, &feature));
    trihedron.connectValue(std::bind(&Stow::prmActiveSync, this));
    feature.parameters.push_back(&trihedron);
    trihedronLabel->refresh();
    
    QStringList transStrings =
    {
      QObject::tr("Modified")
      , QObject::tr("Right")
      , QObject::tr("Round")
    };
    transition.setEnumeration(transStrings);
    transition.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&transition);
    transitionLabel->refresh();
    
    forceC1.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&forceC1);
    
    solid.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&solid);
    
    auxiliary.pick.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&auxiliary.pick);
    
    auxiliary.curvilinearEquivalence.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&auxiliary.curvilinearEquivalence);
    
    auxiliary.contactType.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&auxiliary.contactType);
    
    binormal.picks.connectValue(std::bind(&Feature::setModelDirty, &feature));
    binormal.picks.connectValue(std::bind(&Stow::prmActiveSync, this));
    feature.parameters.push_back(&binormal.picks);
    
    binormal.vector.connectValue(std::bind(&Feature::setModelDirty, &feature));
    feature.parameters.push_back(&binormal.vector);
    
    feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    
    grid->addChild(solidLabel.get());
    grid->addChild(trihedronLabel.get());
    grid->addChild(transitionLabel.get());
    grid->addChild(forceC1Label.get());
    feature.overlaySwitch->addChild(grid);
    
    auxiliarySwitch->addChild(auxiliary.grid);
    feature.overlaySwitch->addChild(auxiliarySwitch.get());
    
    feature.overlaySwitch->addChild(binormalSwitch.get());
    
    prmActiveSync();
  }
  
  void prmActiveSync()
  {
    int triType = trihedron.getInt();
    switch (triType)
    {
      case 4: //binormal
      {
        support.setActive(false);
        auxiliary.setActive(false);
        binormal.setActive(true);
        break;
      }
      case 5: //support
      {
        support.setActive(true);
        auxiliary.setActive(false);
        binormal.setActive(false);
        break;
      }
      case 6: //auxiliary
      {
        support.setActive(false);
        auxiliary.setActive(true);
        binormal.setActive(false);
        break;
      }
      default:
      {
        support.setActive(false);
        auxiliary.setActive(false);
        binormal.setActive(false);
        break;
      }
    }
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("Sweep");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

Profile& Feature::addProfile()
{
  stow->profiles.emplace_back();
  auto &p = stow->profiles.back();
  
  //not going to add these parameters to features general container. no point.
  p.pick.connectValue(std::bind(&Feature::setModelDirty, this));
  p.contact.connectValue(std::bind(&Feature::setModelDirty, this));
  p.correction.connectValue(std::bind(&Feature::setModelDirty, this));
  
  overlaySwitch->addChild(p.grid);
  
  //adding profile itself shouldn't make the feature dirty.
  return stow->profiles.back();
}

void Feature::removeProfile(int index)
{
  assert (index >=0 && index < static_cast<int>(stow->profiles.size()));
  auto lit = stow->profiles.begin();
  for (int i = 0; i < index; ++i, ++lit);
  const auto &p = *lit;
  overlaySwitch->removeChild(p.grid);
  stow->profiles.erase(lit);
  
  setModelDirty();
}

Profiles& Feature::getProfiles() const
{
  return stow->profiles;
}

void Feature::updateModel(const UpdatePayload &pIn)
{
  setFailure();
  lastUpdateLog.clear();
  stow->sShape.reset();
  try
  {
    prm::ObserverBlocker block(stow->prmObserver);
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
    auto getFeature = [&](const std::string& tagIn) -> const Base&
    {
      std::vector<const Base*> tfs = pIn.getFeatures(tagIn);
      if (tfs.size() != 1) throw std::runtime_error("wrong number of parents");
      return *(tfs.front());
    };
    
    auto getSeerShape = [&](const Base &bfIn) -> const ann::SeerShape&
    {
      if (!bfIn.hasAnnex(ann::Type::SeerShape)) throw std::runtime_error("parent doesn't have seer shape.");
      const ann::SeerShape &tss = bfIn.getAnnex<ann::SeerShape>();
      if (tss.isNull()) throw std::runtime_error("target seer shape is null");
      return tss;
    };
    
    auto isValidShape = [](const TopoDS_Shape &sIn)
    {
      if (sIn.ShapeType() == TopAbs_WIRE || sIn.ShapeType() == TopAbs_EDGE)  return true;
      return false;
    };
    
    auto ensureWire = [](const TopoDS_Shape &sIn) -> TopoDS_Wire
    {
      if (sIn.ShapeType() == TopAbs_WIRE) return TopoDS::Wire(sIn);
      if (sIn.ShapeType() == TopAbs_EDGE)
      {
        BRepBuilderAPI_MakeWire wm(TopoDS::Edge(sIn));
        if (!wm.IsDone()) throw std::runtime_error("couldn't create wire from edge");
        return wm.Wire();
      }
      
      throw std::runtime_error("unsupported shape type in ensureWire");
    };
    
    tls::Resolver resolver(pIn);
    auto getWire = [&]() -> TopoDS_Wire
    {
      std::optional<TopoDS_Wire> out;
      if (!slc::isShapeType(resolver.getPick().selectionType))
      {
        auto shapes = resolver.getSeerShape()->useGetNonCompoundChildren();
        if (shapes.empty())
          throw std::runtime_error("no shapes found");
        if (shapes.size() > 1)
        {
          std::ostringstream s; s << "Warning more than 1 shape found. Using first" << std::endl;
          lastUpdateLog += s.str();
        }
        if (!isValidShape(shapes.front()))
          throw std::runtime_error("invalid shape from feature");
        out = ensureWire(shapes.front());
      }
      else
      {
        auto rShapes = resolver.getShapes();
        if (rShapes.empty()) throw std::runtime_error("no shapes from pick resolution");
        if (rShapes.size() > 1)
        {
          std::ostringstream s; s << "Warning more than 1 resolved shape. Using first" << std::endl;
          lastUpdateLog += s.str();
        }
        if (!isValidShape(rShapes.front())) throw std::runtime_error("invalid shape from resolve");
        out = ensureWire(rShapes.front());
      }
      
      if (!out) throw std::runtime_error("couldn't get wire");
      return *out;
    };
    
    TopoDS_Wire spineShape;
    const ann::SeerShape *spineSeerShape = nullptr;
    opencascade::handle<Law_Function> law;
    auto setSpine = [&](const ftr::Pick &pickIn)
    {
      if (!resolver.resolve(pickIn)) throw std::runtime_error("couldn't resolve spine pick");
      spineShape = getWire();
      spineSeerShape = resolver.getSeerShape(); assert(spineSeerShape);
      if (slc::isObjectType(pickIn.selectionType) && resolver.getFeature()->getType() == ftr::Type::LawSpine)
      {
        const auto *lawSpineFeature = dynamic_cast<const ftr::LawSpine::Feature*>(resolver.getFeature());
        assert(lawSpineFeature);
        const auto &vessel = lawSpineFeature->getVessel();
        if (!vessel.isValid()) throw std::runtime_error("Invalid law spine");
        law = lawSpineFeature->getVessel().buildLawFunction();
        stow->trihedron.setActive(false);
      }
      else stow->trihedron.setActive(true);
    };
    
    occt::BoundingBox globalBounder; //use this to place labels
    if (stow->spine.getPicks().empty()) throw std::runtime_error("No Spine Picks");
    setSpine(stow->spine.getPicks().front());
    globalBounder.add(spineShape);
    
    BRepFill_PipeShell sweeper(spineShape);
    switch (stow->trihedron.getInt())
    {
      case 0: // corrected frenet
        sweeper.Set(false);
        break;
      case 1: // frenet
        sweeper.Set(true);
        break;
      case 2: // discrete
        sweeper.SetDiscrete();
        break;
      case 3: //fixed
      {
        //I wasn't able to change results by varying the parameter.
        //thinking it is just there to satisfy the 'Set' overload.
        sweeper.Set(gp_Ax2());
        break;
      }
      case 4: //constant bi normal.
      {
        auto getBinormalShape = [&](const Base &feature, const Pick &pick) -> TopoDS_Shape
        {
          TopoDS_Shape out;
          assert(feature.hasAnnex(ann::Type::SeerShape));
          const ann::SeerShape &seerShape = getSeerShape(feature);
          if (pick.isEmpty())
          {
            auto shapes = seerShape.useGetNonCompoundChildren();
            if (shapes.empty())
              throw std::runtime_error("no non compound children for binormal pick");
            out = shapes.front();
          }
          else
          {
            auto rShapes = resolver.getShapes(pick);
            if (rShapes.empty()) throw std::runtime_error("couldn't resolve binormal 1st pick");
            if (rShapes.size() > 1)
            {
              std::ostringstream s; s << "Warning more than 1 resolved pick for binormal. Using first" << std::endl;
              lastUpdateLog += s.str();
            }
            out = rShapes.front();
          }
          return out;
        };
        
        const auto &bps = stow->binormal.picks.getPicks();
        stow->binormalSwitch->setAllChildrenOff();
        if (bps.size() == 0)
        {
          stow->binormalSwitch->setAllChildrenOn();
        }
        else if (bps.size() == 1)
        {
          const Base &f0 = getFeature(bps.front().tag);
          if (f0.hasAnnex(ann::Type::SeerShape))
          {
            TopoDS_Shape shape = getBinormalShape(f0, bps.front());
            if (shape.IsNull())
              throw std::runtime_error("TopoDS_Shape is null for binormal pick");
            auto axis = occt::gleanAxis(shape);
            if (!axis.second)
              throw std::runtime_error("couldn't glean axis for binormal");
            stow->binormal.vector.setValue(gu::toOsg(axis.first.Direction()));
          }
          else
          {
            auto directionPrms = f0.getParameters(prm::Tags::Direction);
            if (!directionPrms.empty())
              stow->binormal.vector.setValue(directionPrms.front()->getVector());
          }
        }
        else if (bps.size() == 2)
        {
          auto getPoint = [&](int index) -> osg::Vec3d
          {
            auto thePoints = resolver.getPoints(bps.at(index));
            if (thePoints.empty()) throw std::runtime_error("empty binormal points");
            if (thePoints.size() > 1)
            {
              std::ostringstream s; s << "Warning more than 1 resolved binormal point. Using first" << std::endl;
              lastUpdateLog += s.str();
            }
            return thePoints.front();
          };
          
          if
          (
            (!slc::isPointType(bps.at(0).selectionType))
            || (!slc::isPointType(bps.at(1).selectionType))
          )
            throw std::runtime_error("need points only for 2 binormal picks");
            
          osg::Vec3d pd = getPoint(0) - getPoint(1);
          if (!pd.valid() || pd.length() < std::numeric_limits<float>::epsilon())
            throw std::runtime_error("invalid direction for 2 binormal picks");
          pd.normalize();
          stow->binormal.vector.setValue(pd);
        }
        else
          throw std::runtime_error("unsupported number of binormal picks");
        sweeper.Set(gp_Dir(gu::toOcc(stow->binormal.vector.getVector())));
        break;
      }
      case 5: //support
      {
        if (stow->support.getPicks().empty()) throw std::runtime_error("No support picks");
        auto rShapes = resolver.getShapes(stow->support.getPicks().front());
        if (rShapes.empty()) throw std::runtime_error("invalid support pick resolution");
        if (rShapes.size() > 1)
        {
          std::ostringstream s; s << "Warning more than 1 resolved support pick. Using first" << std::endl;
          lastUpdateLog += s.str();
        }
        const TopoDS_Shape &supportShape = rShapes.front();
        if (/*supportShape.ShapeType() != TopAbs_SHELL && */supportShape.ShapeType() != TopAbs_FACE)
          throw std::runtime_error("invalid shape type for support");
        const TopoDS_Face &supportFace = TopoDS::Face(supportShape);
        opencascade::handle<Geom_Surface> supportSurface = BRep_Tool::Surface(supportFace);
        if (supportSurface.IsNull()) throw std::runtime_error("support surface is null");
        
        //put spine on support shape. For now we are assuming that we have
        //1 spine edge and and one support face.
        //projection is a PIA. we will want to move this somewhere else.
        ann::SeerShape temp;
        temp.setOCCTShape(spineShape, gu::createRandomId());
        occt::ShapeVector childShapes = temp.useGetChildrenOfType(temp.getRootOCCTShape(), TopAbs_EDGE);
        occt::EdgeVector childEdges = occt::ShapeVectorCast(childShapes);
        if (childEdges.empty())
          throw std::runtime_error("no spine child shapes for support");
        TopoDS_Edge spineEdge = childEdges.front();
        double p1, p2;
        opencascade::handle<Geom_Curve> spineCurve = BRep_Tool::Curve(spineEdge, p1, p2);
        if (spineCurve.IsNull())
          throw std::runtime_error("spine curve is null");
        opencascade::handle<Geom2d_Curve> pCurve;
        ShapeConstruct_ProjectCurveOnSurface projector;
        projector.SetSurface(supportSurface);
        if (!projector.Perform(spineCurve, p1, p2, pCurve))
          throw std::runtime_error("projection failed");
        BRep_Builder builder;
        builder.UpdateEdge(spineEdge, pCurve, supportFace, Precision::Confusion());
        sweeper.Set(supportFace);
        
        globalBounder.add(supportShape);
        break;
      }
      case 6: // auxiliary
      {
        const auto &aps = stow->auxiliary.pick.getPicks();
        if (aps.empty()) throw std::runtime_error("No auxiliary pick");
        if (!resolver.resolve(aps.front())) throw std::runtime_error("Can't resolve auxiliary spine pick");
        TopoDS_Wire wire = getWire();
        if (!wire.IsNull())
        {
          bool cle = stow->auxiliary.curvilinearEquivalence.getBool();
          BRepFill_TypeOfContact toc = static_cast<BRepFill_TypeOfContact>(stow->auxiliary.contactType.getInt());
          sweeper.Set(wire, cle, toc);
          globalBounder.add(wire);
          occt::BoundingBox wb(wire);
          stow->auxiliary.grid->setPosition(gu::toOsg(wb.getFaceCenters().at(4)));
        }
        else
          throw std::runtime_error("auxiliary spine wire is invalid");
        break;
      }
      default:
        sweeper.Set(false);
    }
    sweeper.SetTransition(static_cast<BRepFill_TransitionStyle>(stow->transition.getInt())); //default is BRepFill_Modified = 0
    occt::WireVector profileWires; //use for shape mapping
    std::vector<std::reference_wrapper<const ann::SeerShape>> seerShapeProfileRefs; //use for shape mapping
    for (const auto &p : stow->profiles)
    {
      if (p.pick.getPicks().empty()) continue;
      if (!resolver.resolve(p.pick.getPicks().front())) throw std::runtime_error("Can't resolve profile pick.");
      TopoDS_Wire wire = getWire();
      const Base &bf = getFeature(p.pick.getPicks().front().tag);
      const ann::SeerShape &ss = getSeerShape(bf);
      seerShapeProfileRefs.push_back(ss);
      
      if (law.IsNull())
        sweeper.Add(wire, p.contact.getBool(), p.correction.getBool());
      else
      {
        if (stow->profiles.size() == 1)
          sweeper.SetLaw(wire, law, p.contact.getBool(), p.correction.getBool());
        else
        {
          sweeper.Add(wire, p.contact.getBool(), p.correction.getBool());
          std::ostringstream s; s << "Multiple profiles override law" << std::endl;
          lastUpdateLog += s.str();
        }
      }
      
      globalBounder.add(wire);
      occt::BoundingBox bounder(wire);
      p.grid->setPosition(gu::toOsg(bounder.getCenter()));
      profileWires.push_back(wire);
    }
    
    sweeper.SetForceApproxC1(stow->forceC1.getBool());
    if (!sweeper.Build()) throw std::runtime_error("Sweep operation failed");
    if (stow->solid.getBool()) sweeper.MakeSolid();
    globalBounder.add(sweeper.Shape());
    
    ShapeCheck check(sweeper.Shape());
    if (!check.isValid())
      throw std::runtime_error("Shape check failed");
    
    stow->sShape.setOCCTShape(sweeper.Shape(), getId());
    stow->sShape.updateId(stow->sShape.getRootOCCTShape(), this->getId());
    stow->sShape.setRootShapeId(this->getId());
    if (!stow->sShape.hasEvolveRecordOut(this->getId()))
      stow->sShape.insertEvolve(gu::createNilId(), this->getId());
    
    /* Notes about sweeps generated method:
     * No entries for the spine. This might be a bug, because the occt code appears
     *   to have support for mapping between spine edges and resultant faces.
     * Edges in profiles create shells. Even if only 1 resultant face.
     * Vertices in profiles create wires. Even if only 1 resultant edge.
     * FYI: Face seems are at spine junctions not at profile transitions.
     * UPDATE: Things have changed 7/3/24
     * Spine has entries now. both edges and vertices.
     *   spine edges generate faces. Note: same faces come from profile generated
     *   spine vertices generate edges.
     *     first vertex and last vertex generate same edge as FirstShape and LastShape
     *     only when solid is false? When solid is true FirstShape and LastShape are
     *     faces that are not generated anywhere else.
     * Edges in profiles no longer create shells, just a list of faces.
     * Vertices in profiles no longer create wires, just a list of edges.
     * All profiles edges are still linked to all output faces.
     * 
     * if solid is false first and last shape will be wires.
     * if solid is true first and last shape will be faces.
     * 
     * Sweep isn't using any shapes from the original profiles in the resultant shape.
     * Even with the most basic sweep. Everything must be derived from 'Generated'
     * Is 'FirstShape' orientated the same as the first passed in profile?
     *     meaning can we use order to map input edge to output edge.
     *     I guess not, 'FirstShape' doesn't even match the first profile passed in!
     * 
     * Sweeps with multiple profiles:
     * Sweep profiles have the same number of edges. Each edge of a profile is tied
     * to the corresponding edge in the other profiles. This set of edges are tied
     * to all generated faces regardless if outside the face. Each edge set may produce
     * multiple faces.
     * 
     * Considered making an id set of edges to match to generated faces. Problem is the generated
     * face orders of the shells are different for each edge. This would cause faces to 'swap'
     * ids with certain profile changes. Yuck! I would rather have a new id that causes child features
     * to fail instead of child features referencing the 'wrong' face. That might cause the child
     * feature to successfully update, but not in the way as intended by user.
     * 
     * Use spine edges and vertices to instance map for result face and edges.
     * Only edges left at this point should be the first and last shape. We should be able to
     * get the last edge with a dedicated map from the face id.
     * have a first and last face id, if solid and first and last shape is a face.
     * Map for outer wire for each face.
     * have a shell id
     * have a solid id, if solid.
     * 
     */
    
// #define DUMPALLGENERATED
#ifdef DUMPALLGENERATED
    //begin testing results from Generated method.
    auto generatedShapes = [&](const TopoDS_Shape &sIn) -> occt::ShapeVector
    {
      TopTools_ListOfShape los;
      sweeper.Generated(sIn, los);
      return occt::ShapeVectorCast(los);
    };
    
    auto dumpWireGenerated = [&](const TopoDS_Wire &wIn) //input wire either a profile or spine
    {
      for (const auto &s : occt::mapShapes(wIn))
      {
        std::cout << "  " << s << std::endl << "    Generated:" << std::endl;
        for (const auto &gs : generatedShapes(s)) std::cout << "      " << gs << std::endl;
      }
    };
    
    std::cout << std::endl << "Spine: ";
    dumpWireGenerated(spineShape);
    std::cout << std::endl;
      
    std::cout << "profiles ordered by pick:" << std::endl;
    int wireIndex = -1;
    for (const auto &w : profileWires)
    {
      wireIndex++;
      std::cout << "Profile #" << wireIndex;
      dumpWireGenerated(w);
      std::cout << std::endl;
    }
    
    auto dumpFirstLast = [&](const TopoDS_Shape &sIn, std::string_view name)
    {
      std::cout << std::endl << name;
      if (sIn.IsNull()) return;
      for (const auto &s : occt::mapShapes(sIn)) std::cout << "  " << s << std::endl;
    };
    dumpFirstLast(sweeper.FirstShape(), "First Shape:");
    std::cout << std::endl;
    dumpFirstLast(sweeper.LastShape(), "Last Shape:");
    std::cout << std::endl << std::endl;
#endif //DUMPALLGENERATED
    
    //begin id mapping scheme.
    auto getOutputId = [&](const uuid &inputId, std::size_t index) -> uuid
    {
      uuid outputId = gu::createNilId();
      auto mapIt = stow->instanceMap.find(inputId);
      if (mapIt == stow->instanceMap.end())
      {
        assert(index == 0);
        outputId = gu::createRandomId();
        std::vector<uuid> resultIds(1, outputId);
        stow->instanceMap.insert(std::make_pair(inputId, resultIds));
      }
      else
      {
        std::vector<uuid> &resultIds = mapIt->second;
        while (resultIds.size() <= index)
          resultIds.push_back(gu::createRandomId());
        outputId = resultIds.at(index);
      }
      assert(!outputId.is_nil());
      return outputId;
    };
    
    for (const auto &ss : occt::mapShapes(spineShape))
    {
      TopTools_ListOfShape temp;
      sweeper.Generated(ss, temp);
      if (temp.IsEmpty()) continue;
      auto generatedShapes = static_cast<occt::ShapeVector>(occt::ShapeVectorCast(temp));
      if (!spineSeerShape->hasShape(ss)) {assert(0); throw std::runtime_error("no spine shape in input seer shape");}
      uuid inputId = spineSeerShape->findId(ss);
      int index = -1;
      for (const auto &gs : generatedShapes)
      {
        index++;
        auto outputId = getOutputId(inputId, index);
        stow->sShape.updateId(gs, outputId);
        stow->sShape.insertEvolve(inputId, outputId);
      }
    }
    
    //first and last shapes.
    TopoDS_Wire firstWire;
    TopoDS_Wire lastWire;
    if (sweeper.FirstShape().ShapeType() == TopAbs_FACE && sweeper.LastShape().ShapeType() == TopAbs_FACE)
    {
      stow->sShape.updateId(sweeper.FirstShape(), stow->firstFaceId);
      if (!stow->sShape.hasEvolveRecordOut(stow->firstFaceId))
        stow->sShape.insertEvolve(gu::createNilId(), stow->firstFaceId);
      firstWire = BRepTools::OuterWire(TopoDS::Face(sweeper.FirstShape()));
      
      stow->sShape.updateId(sweeper.LastShape(), stow->lastFaceId);
      if (!stow->sShape.hasEvolveRecordOut(stow->lastFaceId))
        stow->sShape.insertEvolve(gu::createNilId(), stow->lastFaceId);
      lastWire = BRepTools::OuterWire(TopoDS::Face(sweeper.LastShape()));
    }
    else if (sweeper.FirstShape().ShapeType() == TopAbs_WIRE && sweeper.LastShape().ShapeType() == TopAbs_WIRE)
    {
      firstWire = TopoDS::Wire(sweeper.FirstShape());
      lastWire = TopoDS::Wire(sweeper.LastShape());
    }
    if ((!firstWire.IsNull()) && (!lastWire.IsNull()))
    {
      auto getFirstLastId = [&](const uuid &idIn, std::map<uuid, uuid> &mapId) -> uuid
      {
        auto it = mapId.find(idIn);
        if (it != mapId.end())
          return it->second;
        uuid freshId = gu::createRandomId();
        mapId.insert(std::make_pair(idIn, freshId));
        return freshId;
      };
      
      auto goMapEdge = [&](const TopoDS_Wire &wireIn, std::map<uuid, uuid> &mapId)
      {
        BRepTools_WireExplorer we(wireIn);
        for (; we.More(); we.Next())
        {
          auto parentFaces = stow->sShape.useGetParentsOfType(we.Current(), TopAbs_FACE);
          for (const auto &pf : parentFaces)
          {
            uuid pfid = stow->sShape.findId(pf);
            if (pfid.is_nil() || pfid == stow->firstFaceId)
              continue;
            uuid freshId = getFirstLastId(pfid, mapId);
            stow->sShape.updateId(we.Current(), freshId);
            if (!stow->sShape.hasEvolveRecordOut(freshId))
              stow->sShape.insertEvolve(gu::createNilId(), freshId);
            break;
          }
        }
      };
      
      goMapEdge(firstWire, stow->firstShapeMap);
      goMapEdge(lastWire, stow->lastShapeMap);
    }
    
    //now do outerwires from faces.
    auto faces = stow->sShape.useGetChildrenOfType(stow->sShape.getRootOCCTShape(), TopAbs_FACE);
    for (const auto &f : faces)
    {
      uuid fId = stow->sShape.findId(f);
      if (fId.is_nil())
        continue;
      uuid owId = gu::createNilId();
      auto it = stow->outerWireMap.find(fId);
      if (it != stow->outerWireMap.end())
        owId = it->second;
      else
      {
        owId = gu::createRandomId();
        stow->outerWireMap.insert(std::make_pair(fId, owId));
      }
      if (!stow->sShape.hasEvolveRecordOut(owId))
        stow->sShape.insertEvolve(gu::createNilId(), owId);
      const TopoDS_Wire &ow = BRepTools::OuterWire(TopoDS::Face(f));
      stow->sShape.updateId(ow, owId);
    }
    
    stow->sShape.derivedMatch();
    
    //update id for shell and solid. Note: we are not checking for multiple shells or solids.
    for (const auto &s : stow->sShape.getAllNilShapes())
    {
      if (s.ShapeType() == TopAbs_SOLID)
        stow->sShape.updateId(s, stow->solidId);
      if (s.ShapeType() == TopAbs_SHELL)
        stow->sShape.updateId(s, stow->shellId);
    }
    if (!stow->sShape.hasEvolveRecordOut(stow->solidId))
      stow->sShape.insertEvolve(gu::createNilId(), stow->solidId);
    if (!stow->sShape.hasEvolveRecordOut(stow->shellId))
      stow->sShape.insertEvolve(gu::createNilId(), stow->shellId);
    
    //shapeMatch is useless. sweep never uses any of the original geometry.
    // std::cout << std::endl;
    // stow->sShape.dumpShapeIdContainer(std::cout);
    stow->sShape.dumpNils("sweep feature");
    stow->sShape.dumpDuplicates("sweep feature");
    stow->sShape.ensureNoNils();
    stow->sShape.ensureNoDuplicates();
    stow->sShape.ensureEvolve();
    
    stow->grid->setPosition(gu::toOsg(globalBounder.getFaceCenters().at(5)));
    if (stow->trihedron.getInt() == 6)
      stow->auxiliarySwitch->setAllChildrenOn();
    else
      stow->auxiliarySwitch->setAllChildrenOff();
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &dIn)
{
  prj::srl::swps::Sweep so
  (
    Base::serialOut()
    , stow->sShape.serialOut()
    , stow->trihedron.serialOut()
    , stow->transition.serialOut()
    , stow->forceC1.serialOut()
    , stow->solid.serialOut()
    , stow->spine.serialOut()
    , stow->auxiliary.serialOut()
    , stow->support.serialOut()
    , stow->binormal.serialOut()
    , stow->trihedronLabel->serialOut()
    , stow->transitionLabel->serialOut()
    , stow->forceC1Label->serialOut()
    , stow->solidLabel->serialOut()
    , gu::idToString(stow->solidId)
    , gu::idToString(stow->shellId)
    , gu::idToString(stow->firstFaceId)
    , gu::idToString(stow->lastFaceId)
  );
  
  for (const auto &p : stow->profiles)
    so.profiles().push_back(p.serialOut());
  
  auto serializeMap = [](const std::map<uuid, uuid> &map) -> prj::srl::spt::SeerShape::EvolveContainerSequence
  {
    prj::srl::spt::SeerShape::EvolveContainerSequence out;
    for (const auto &p : map)
    {
      prj::srl::spt::SeerShape::EvolveContainerType r
      (
        gu::idToString(p.first),
        gu::idToString(p.second)
      );
      out.push_back(r);
    }
    return out;
  };
  
  so.outerWireMap() = serializeMap(stow->outerWireMap);
  so.firstShapeMap() = serializeMap(stow->firstShapeMap);
  so.lastShapeMap() = serializeMap(stow->lastShapeMap);
  
  for (const auto &p : stow->instanceMap)
  {
    prj::srl::swps::Instance instanceOut(gu::idToString(p.first));
    for (const auto &id : p.second)
      instanceOut.values().push_back(gu::idToString(id));
    so.instanceMap().push_back(instanceOut);
  }
  
  so.grid() = lbr::PLabelGridCallback::serialOut(stow->grid);
  
  xml_schema::NamespaceInfomap infoMap;
  std::ofstream stream(buildFilePathName(dIn).string());
  prj::srl::swps::sweep(stream, so, infoMap);
}

void Feature::serialRead(const prj::srl::swps::Sweep &so)
{
  Base::serialIn(so.base());
  stow->sShape.serialIn(so.seerShape());
  stow->trihedron.serialIn(so.trihedron());
  stow->transition.serialIn(so.transition());
  stow->forceC1.serialIn(so.forceC1());
  stow->solid.serialIn(so.solid());
  stow->spine.serialIn(so.spine());
  stow->auxiliary.serialIn(so.auxiliary());
  stow->support.serialIn(so.support());
  stow->binormal.serialIn(so.binormal());
  stow->trihedronLabel->serialIn(so.trihedronLabel());
  stow->transitionLabel->serialIn(so.transitionLabel());
  stow->forceC1Label->serialIn(so.forceC1Label());
  stow->solidLabel->serialIn(so.solidLabel());
  stow->solidId = gu::stringToId(so.solidId());
  stow->shellId = gu::stringToId(so.shellId());
  stow->firstFaceId = gu::stringToId(so.firstFaceId());
  stow->lastFaceId = gu::stringToId(so.lastFaceId());
  
  for (const auto &p : so.profiles())
  {
    auto &pro = stow->profiles.emplace_back(p);
    
    pro.pick.connectValue(std::bind(&Feature::setModelDirty, this));
    pro.contact.connectValue(std::bind(&Feature::setModelDirty, this));
    pro.correction.connectValue(std::bind(&Feature::setModelDirty, this));
    
    overlaySwitch->addChild(pro.grid);
  }
  
  auto serializeMap = [](const prj::srl::spt::SeerShape::EvolveContainerSequence &container) -> std::map<uuid, uuid>
  {
    std::map<uuid, uuid> out;
    for (const auto &r : container)
      out.insert(std::make_pair(gu::stringToId(r.idIn()), gu::stringToId(r.idOut())));
    return out;
  };
  stow->outerWireMap = serializeMap(so.outerWireMap());
  stow->firstShapeMap = serializeMap(so.firstShapeMap());
  stow->lastShapeMap = serializeMap(so.lastShapeMap());
  
  for (const auto &p : so.instanceMap())
  {
    std::vector<uuid> valuesIn;
    for (const auto &v : p.values())
      valuesIn.push_back(gu::stringToId(v));
    stow->instanceMap.insert(std::make_pair(gu::stringToId(p.key()), valuesIn));
  }
  
  if (so.grid()) lbr::PLabelGridCallback::serialIn(so.grid().get(), stow->grid);
  
  stow->prmActiveSync();
}

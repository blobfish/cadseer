/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <boost/bimap.hpp>
#include <boost/assign/list_of.hpp>

#include "feature/ftrtypes.h"

using namespace ftr;

using TypeMap = boost::bimap<Type, std::string>;
static thread_local TypeMap map = boost::assign::list_of<TypeMap::relation>
(Type::Base, "Base")
(Type::CSys, "CSys")
(Type::Box, "Box")
(Type::Sphere, "Sphere")
(Type::Cone, "Cone")
(Type::Cylinder, "Cylinder")
(Type::Blend, "Blend")
(Type::Chamfer, "Chamfer")
(Type::Draft, "Draft")
(Type::Inert, "Inert")
(Type::Boolean, "Boolean")
(Type::Union, "Union")
(Type::Subtract, "Subtract")
(Type::Intersect, "Intersect")
(Type::DatumPlane, "DatumPlane")
(Type::Hollow, "Hollow")
(Type::Oblong, "Oblong")
(Type::Extract, "Extract")
(Type::Squash, "Squash")
(Type::Strip, "Strip")
(Type::Nest, "Nest")
(Type::DieSet, "DieSet")
(Type::Quote, "Quote")
(Type::Refine, "Refine")
(Type::Instance, "Instance")
(Type::InstanceLinear, "InstanceLinear") //DEPRECATED
(Type::InstanceMirror, "InstanceMirror") //DEPRECATED
(Type::InstancePolar, "InstancePolar") //DEPRECATED
(Type::Offset, "Offset")
(Type::Thicken, "Thicken")
(Type::Sew, "Sew")
(Type::Trim, "Trim")
(Type::RemoveFaces, "RemoveFaces")
(Type::Torus, "Torus")
(Type::Thread, "Thread")
(Type::DatumAxis, "DatumAxis")
(Type::Sketch, "Sketch")
(Type::Extrude, "Extrude")
(Type::Revolve, "Revolve")
(Type::SurfaceMesh, "SurfaceMesh")
(Type::Line, "Line")
(Type::TransitionCurve, "TransitionCurve")
(Type::Ruled, "Ruled")
(Type::ImagePlane, "ImagePlane")
(Type::Sweep, "Sweep")
(Type::DatumSystem, "DatumSystem")
(Type::SurfaceReMesh, "SurfaceReMesh")
(Type::SurfaceMeshFill, "SurfaceMeshFill")
(Type::MapPCurve, "MapPCurve")
(Type::Untrim, "Untrim")
(Type::Face, "Face")
(Type::Fill, "Fill")
(Type::Prism, "Prism")
(Type::UnderCut, "UnderCut")
(Type::Mutate, "Mutate")
(Type::LawSpine, "LawSpine")
(Type::CurveComb, "CurveComb")
(Type::DatumPoint, "DatumPoint")
(Type::Skin, "Skin")
(Type::BoundingBox, "BoundingBox")
(Type::Arc, "Arc")
(Type::Helix, "Helix")
(Type::FaceComb, "FaceComb")
(Type::ShapeMesh, "ShapeMesh")
(Type::ParametricCurve, "ParametricCurve")
(Type::Gordon, "Gordon");

const std::string& ftr::toString(Type typeIn)
{
  assert(map.left.count(typeIn) == 1);
  return map.left.at(typeIn);
}

std::optional<Type> ftr::toType(const std::string &sIn)
{
  if (map.right.count(sIn) != 1) return std::nullopt;
  return map.right.at(sIn);
}

std::vector<std::string> ftr::allTypeStrings()
{
  std::vector<std::string> out;
  for (const auto &p : map.left) out.push_back(p.second);
  std::sort(out.begin(), out.end());
  return out;
}

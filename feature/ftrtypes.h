/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FTR_TYPES_H
#define FTR_TYPES_H

#include <optional>
#include <string>
#include <vector>

namespace ftr
{
  //! feature types. make sure to update map structure in cpp file
  enum class Type
  {
    Base, //!< feature base class.
    CSys, //!< feature base class.
    Box, //!< feature box class.
    Sphere, //!< feature sphere class.
    Cone, //!< feature cone class.
    Cylinder, //!< feature cylinder class.
    Blend, //!< feature blend class.
    Chamfer, //!< feature chamfer class.
    Draft, //!< feature draft class.
    Inert, //!< feature inert class.
    Boolean, //!< feature boolean base class.
    Union, //!< feature union class.
    Subtract, //!< feature subtract class.
    Intersect, //!< feature intersect class.
    DatumPlane, //!< feature datum plane class.
    Hollow, //!< feature hollow class.
    Oblong, //!< feature oblong class.
    Extract, //!< feature extract class.
    Squash, //!< feature squash class.
    Strip, //!< feature strip class.
    Nest, //!< feature nest class.
    DieSet, //!< feature dieset class.
    Quote, //!< feature quote class.
    Refine, //!< feature refine class.
    Instance,
    InstanceLinear, //!< feature for linear patterns.   DEPRECATED
    InstanceMirror, //!< feature for mirrored patterns. DEPRECATED
    InstancePolar, //!< feature for polar patterns. DEPRECATED
    Offset, //!< feature for offset shapes.
    Thicken, //!< feature for thickening sheets.
    Sew, //!< feature for sewing sheets.
    Trim, //!< feature for trimming solids.
    RemoveFaces, //!< feature for simplifing solids.
    Torus, //!< feature for torus primitive
    Thread, //!< feature for screw threads
    DatumAxis, //!< feature for datum axis
    Sketch, //!< feature for sketch
    Extrude, //!< feature for extrude
    Revolve, //!< feature for revolve
    SurfaceMesh, //!< feature for surface mesh creation.
    Line, //!< feature for lines
    TransitionCurve, //!< feature for lines
    Ruled, //!< feature for ruled surfaces
    ImagePlane, //!< feature for image planes
    Sweep, //!< feature for sweep surfaces
    DatumSystem, //!< feature for datum coordinate systems
    SurfaceReMesh, //!< feature for surface remesh
    SurfaceMeshFill, //!< feature for surface remesh
    MapPCurve, //!< feature for mapping 2d pcurves to 3d surface curves
    Untrim, //!< feature for removing trim of faces.
    Face, //!< feature for creating a face. 
    Fill, //!< feature for creating a fill face. 
    Prism,
    UnderCut,
    Mutate,
    LawSpine,
    CurveComb,
    DatumPoint,
    Skin,
    BoundingBox,
    ShapeMesh,
    Arc,
    Helix,
    FaceComb,
    ParametricCurve,
    Gordon
  };
  
  const std::string& toString(Type typeIn);
  std::optional<Type> toType(const std::string&); //return 'Base' if not found
  std::vector<std::string> allTypeStrings();
}

#endif //FTR_TYPES_H

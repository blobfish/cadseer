/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMP_BREP_H
#define IMP_BREP_H

#include <filesystem>

#include <application/appapplication.h>
#include <project/prjproject.h>

namespace imp
{
  inline std::string importBrep(app::Application &app, const std::filesystem::path &filePath)
  {
    app.getProject()->readOCC(filePath.string());
    return std::string();
  }
}

#endif //IMP_BREP_H

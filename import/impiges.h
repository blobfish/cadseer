/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMP_IGES_H
#define IMP_IGES_H

#include <filesystem>

#include <IGESControl_Reader.hxx>

#include <application/appapplication.h>
#include <project/prjproject.h>
#include <feature/ftrinert.h>
#include "tools/occtools.h"

namespace imp
{
  inline std::string importIges(app::Application &app, const std::filesystem::path &pathIn)
  {
    IGESControl_Reader reader;
    reader.SetReadVisible(Standard_True);
    auto readResult = reader.ReadFile(pathIn.string().c_str());
    if (readResult == IFSelect_RetVoid)
    {
      std::string error = QObject::tr("Nothing To Translate For File: ").toStdString() + pathIn.string();
      return error;
    }
    else if(readResult == IFSelect_RetError)
    {
      std::string error = QObject::tr("Input Data Error For File: ").toStdString() + pathIn.string();
      return error;
    }
    else if(readResult == IFSelect_RetFail)
    {
      std::string error = QObject::tr("Execution Failed Error For File: ").toStdString() + pathIn.string();
      return error;
    }
    else if(readResult == IFSelect_RetStop)
    {
      std::string error = QObject::tr("Execution Stopped For File: ").toStdString() + pathIn.string();
      return error;
    }
    
    reader.TransferRoots();
    int nos = reader.NbShapes();
    occt::ShapeVector allRoots;
    for (int index = 1; index <= nos; ++index)
    {
      occt::ShapeVector ncs = occt::getNonCompounds(reader.Shape(index));
      allRoots.insert(allRoots.end(), ncs.begin(), ncs.end());
    }
    if (allRoots.empty()) return "No Shapes To Import From IGES File";
    TopoDS_Compound comp = occt::ShapeVectorCast(allRoots);
    auto csys = ftr::Inert::deriveSystem(allRoots);
    std::string baseName = pathIn.stem().string();
    auto *inert = app.getProject()->addFeature(std::make_unique<ftr::Inert::Feature>(comp, csys));
    inert->setName(QString::fromStdString(baseName));
    //why did I have the following 3 lines?
    inert->updateModel(app.getProject()->getPayload(inert->getId()));
    inert->updateVisual();
    inert->setModelDirty();
    
    return std::string();
  }
}

#endif //IMP_IGES_H


/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "application/appapplication.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "project/prjmessage.h"
#include "import/impmap.h"
#include "import/impbrep.h"
#include "import/impstep.h"
#include "import/impiges.h"
#include "import/impoff.h"
#include "import/impply.h"
#include "import/impstl.h"
#include "import/impmanager.h"

using namespace imp;

struct Manager::Stow
{
  app::Application &theApp;
  msg::Node node;
  msg::Sift sift;
  Map map;
  
  Stow() = delete;
  Stow(app::Application &appIn) : theApp(appIn)
  {
    node.connect(msg::hub());
    node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
    sift.name = "imp::Manager"; //set derived class or use other constructor
    
    sift.insert
    (
      {
        std::make_pair
        (
          msg::Request | msg::Import, std::bind(&Stow::importFile, this, std::placeholders::_1)
        )
      }
    );
    
    map.insert(std::make_pair(Extensions{".brep", ".brp"}, importBrep));
    map.insert(std::make_pair(Extensions{".step", ".stp"}, importStep));
    map.insert(std::make_pair(Extensions{".iges", ".igs"}, importIges));
    map.insert(std::make_pair(Extensions{".off"}, importOFF));
    map.insert(std::make_pair(Extensions{".ply"}, importPLY));
    map.insert(std::make_pair(Extensions{".stl"}, importSTL));
  }
  
  void importFile(const msg::Message &msgIn)
  {
    assert(msgIn.isPRJ()); if (!msgIn.isPRJ()) return;
    std::filesystem::path file = msgIn.getPRJ().directory;
    if (!std::filesystem::exists(file) || !std::filesystem::is_regular_file(file)) return;
    auto ext = file.extension();
    for (const auto &e : map)
    {
      if (!e.first.isMatch(ext.u8string())) continue;
      auto result = e.second(theApp, file);
      std::string message = file.string();
      if (result.empty()) message += ": import succeeded";
      else message = result;
      node.sendBlocked(msg::buildStatusMessage(message, 4.0));
    }
  }
};

Manager::Manager(app::Application &appIn) : stow(std::make_unique<Stow>(appIn))
{
  
}

Manager::~Manager() = default;

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMP_MANAGER_H
#define IMP_MANAGER_H

#include <memory>

namespace app{class Application;}

//! @namespace imp @brief All things importation of external files
namespace imp
{
  /*! @struct Manager
   * @brief Controller for importing of external files.
   * @details Single instance of this struct to live in
   * application and work via the message interface. Dispatching
   * is based upon file extensions.
   */
  struct Manager
  {
    Manager() = delete;
    Manager(app::Application&);
    ~Manager();
    
  private:
    struct Stow;
    std::unique_ptr<Stow> stow;
  };
}

#endif //IMP_MANAGER_H

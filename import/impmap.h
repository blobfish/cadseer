/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMP_MAP_H
#define IMP_MAP_H

#include <map>
#include <functional>
#include <string>
#include <vector>
#include <algorithm>
#include <initializer_list>
#include <filesystem>

namespace app{class Application;}

namespace imp
{
  using Importer = std::function<std::string(app::Application&, const std::filesystem::path&)>;
  struct Extensions
  {
    static std::string toLower(const std::string &sIn)
    {
      auto copy = sIn;
      std::transform(sIn.begin(), sIn.end(), copy.begin(), [](unsigned char c){ return std::tolower(c); });
      return copy;
    }
    
    struct Predicate
    {
      bool operator()(const Extensions &e0, const Extensions &e1) const
      {
        auto it0 = e0.exts.begin();
        auto it1 = e1.exts.begin();
        while(it0 != e0.exts.end() && it1 != e1.exts.end())
        {
          if (*it0 < *it1) return true;
          it0++; it1++;
        }
        return false;
      }
    };
    
    Extensions() = default;
    Extensions(std::initializer_list<std::string> listIn)
    {
      for (const auto &e : listIn) exts.push_back(toLower(e));
    }
    
    Extensions& append(const std::string &sIn)
    {
      exts.push_back(toLower(sIn));
      return *this;
    }
    
    bool isMatch(const std::string &extIn) const
    {
      auto lower = toLower(extIn);
      for (const auto &e : exts) if (e == lower) return true;
      return false;
    }
  private: //force the use of toLower.
    std::vector<std::string> exts;
  };
  
  using Map = std::map<Extensions, Importer, Extensions::Predicate>;
}

#endif //IMP_MAP_H

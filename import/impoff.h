/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMP_OFF_H
#define IMP_OFF_H

#include <filesystem>

#include <application/appapplication.h>
#include <project/prjproject.h>
#include "annex/annsurfacemesh.h"
#include "feature/ftrsurfacemesh.h"

namespace imp
{
  inline std::string importOFF(app::Application &app, const std::filesystem::path &pathIn)
  {
    std::unique_ptr<ann::SurfaceMesh> mesh = std::make_unique<ann::SurfaceMesh>();
    if (!mesh->readOFF(pathIn.string()))
    {
      std::string error = QObject::tr("Failed To Import File: ").toStdString() + pathIn.string();
      return error;
    }
    
    auto *meshFeature = new ftr::SurfaceMesh::Feature();
    app.getProject()->addFeature(std::unique_ptr<ftr::SurfaceMesh::Feature>(meshFeature));
    meshFeature->setName(QString::fromStdString(pathIn.stem().string()));
    meshFeature->setMesh(std::move(mesh));
    
    meshFeature->updateModel(app.getProject()->getPayload(meshFeature->getId()));
    meshFeature->updateVisual();
    meshFeature->setModelDirty();
    
    return std::string();
  }
}

#endif //IMP_OFF_H



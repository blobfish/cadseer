/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMP_STEP_H
#define IMP_STEP_H

#include <filesystem>

#include <STEPControl_Reader.hxx>

#include <osg/Matrixd>

#include <application/appapplication.h>
#include <project/prjproject.h>
#include <feature/ftrinert.h>
#include "tools/occtools.h"

namespace imp
{
  inline std::string importStep(app::Application &app, const std::filesystem::path &filePath)
  {
    STEPControl_Reader scr; //step control reader.
    if (scr.ReadFile(filePath.string().c_str()) != IFSelect_RetDone)
    {
      std::string em("Failed To Read: ");
      em += filePath.string();
      return em;
    }
    
    //todo check units!
    
    scr.TransferRoots();
    int nos = scr.NbShapes(); //number of shapes.
    
    if (nos < 1)
    {
      std::string em("No Shapes In File: ");
      em += filePath.string().c_str();
      return em;
    }
    
    occt::ShapeVector allRoots;
    for (int index = 1; index <= nos; ++index)
    {
      occt::ShapeVector ncs = occt::getNonCompounds(scr.Shape(index));
      allRoots.insert(allRoots.end(), ncs.begin(), ncs.end());
    }
    if (allRoots.empty()) return "No Shapes To Import From Step File";
    TopoDS_Compound comp = occt::ShapeVectorCast(allRoots);
    
    //determine the inert csys parameter.
    osg::Matrixd system = ftr::Inert::deriveSystem(allRoots);
    
    std::string baseName = filePath.stem().string();
    auto *inert = new ftr::Inert::Feature(comp, system);
    app.getProject()->addFeature(std::unique_ptr<ftr::Inert::Feature>(inert));
    inert->setName(QString::fromStdString(baseName));
    //why did I have the following 3 lines?
    inert->updateModel(app.getProject()->getPayload(inert->getId()));
    inert->updateVisual();
    inert->setModelDirty();
    
    return std::string();
  }
}

#endif //IMP_STEP_H

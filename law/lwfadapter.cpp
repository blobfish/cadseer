/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2022 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>
#include <optional>

#include <BRep_Tool.hxx>
#include <TopoDS.hxx>
#include <TopExp.hxx>
#include <ChFi3d_FilBuilder.hxx>
#include <ChFiDS_FilSpine.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <BRepAdaptor_CompCurve.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepTools_WireExplorer.hxx>
#include <BRepGProp.hxx>
#include <GProp_GProps.hxx>

#include "tools/occtools.h"
#include "annex/annseershape.h"
#include "law/lwfadapter.h"

using namespace lwf;

struct Adapter::Stow
{
  enum Mode
  {
    None
    , FilSpine
    , AdaptorCurve
  };
  
  const ann::SeerShape &sShape;
  boost::uuids::uuid edgeId;
  opencascade::handle<ChFiDS_FilSpine> spine;
  BRepAdaptor_CompCurve curveAdaptor;
  Mode mode;
  
  Stow() = delete;
  Stow(const ann::SeerShape &ssIn, const boost::uuids::uuid &edgeIdIn)
  : sShape(ssIn)
  , edgeId(edgeIdIn)
  {
    //don't set me up
    assert(sShape.hasId(edgeId));
    assert(sShape.getOCCTShape(edgeId).ShapeType() == TopAbs_EDGE);
    
    std::vector<boost::uuids::uuid> parents;
    auto assignFilSpine = [&]()
    {
      mode = FilSpine;
      ChFi3d_FilBuilder tempBuilder(sShape.getOCCTShape(parents.front()));
      tempBuilder.Add(TopoDS::Edge(sShape.getOCCTShape(edgeId)));
      spine = dynamic_cast<ChFiDS_FilSpine*>(tempBuilder.Value(1).get());
    };
    
    parents = sShape.useGetParentsOfType(edgeId, TopAbs_SOLID);
    if (!parents.empty()) assignFilSpine();
    else
    {
      parents = sShape.useGetParentsOfType(edgeId, TopAbs_SHELL);
      if (!parents.empty()) assignFilSpine();
      //pull from face using BRepTools_WireExplorer?
      else
      {
        parents = sShape.useGetParentsOfType(edgeId, TopAbs_WIRE);
        if (!parents.empty())
        {
          mode = AdaptorCurve;
          curveAdaptor.Initialize(TopoDS::Wire(sShape.getOCCTShape(parents.front())), true);
          //else create a wire from single edge?
        }
      }
    }
  }
};

Adapter::Adapter(const ann::SeerShape &ssIn, const boost::uuids::uuid &eIdIn)
: stow(std::make_unique<Stow>(ssIn, eIdIn))
{}

Adapter::Adapter(Adapter&&) noexcept = default;

Adapter::~Adapter() = default;

Adapter& Adapter::operator=(Adapter &&) noexcept = default;

bool Adapter::isValid() const
{
  return stow->mode != Stow::None;
}

std::pair<double, double> Adapter::getRange() const
{
  assert(isValid());
  if (stow->mode == Stow::FilSpine)
    return std::make_pair(stow->spine->FirstParameter(), stow->spine->LastParameter());
  if (stow->mode == Stow::AdaptorCurve)
    return std::make_pair(stow->curveAdaptor.FirstParameter(), stow->curveAdaptor.LastParameter());
  return std::make_pair(0.0, 0.0);
}

bool Adapter::isPeriodic() const
{
  assert(isValid());
  if (stow->mode == Stow::FilSpine) return stow->spine->IsPeriodic();
  //see doc for BRepAdaptor_CompCurve. It specifies that it can never be periodic and
  //I looked at the source code and it just returns false. For what we are trying to do here
  //I think we can just cheat check if it is closed.
  if (stow->mode == Stow::AdaptorCurve) return stow->curveAdaptor.IsClosed();
  return false;
}

/*!@brief Get location from spine parameter.
 */
gp_Pnt Adapter::location(double spineParameter) const
{
  assert(isValid());
  //It appears 'Value' constrains the point within range.
  if (stow->mode == Stow::FilSpine) return stow->spine->Value(spineParameter);
  if (stow->mode == Stow::AdaptorCurve) return stow->curveAdaptor.Value(spineParameter);
  return gp_Pnt();
}

/*!@brief Get parameter along spine for vertex
 * @param[in] vIn is a vertex on the spine.
 * @return Spine parameter or nullopt if vertex is not on spine.
 * @details Get parameter along spine for vertex
 * In the case of periodic the first(0.0) will be returned.
 */
std::optional<double> Adapter::toSpineParameter(const TopoDS_Vertex &vIn) const
{
  assert(isValid());
  
  if (stow->mode == Stow::FilSpine)
  {
    double out = stow->spine->Absc(vIn);
    if (out < 0.0) return std::nullopt;
    return out;
  }
  if (stow->mode == Stow::AdaptorCurve)
  {
    double currentAbsc = 0.0;
    BRepTools_WireExplorer ex(stow->curveAdaptor.Wire());
    for (; ex.More(); ex.Next())
    {
      if (TopExp::FirstVertex(ex.Current(), true).IsSame(vIn)) return currentAbsc;
      GProp_GProps props;
      BRepGProp::LinearProperties(ex.Current(), props);
      currentAbsc += props.Mass();
      if (TopExp::LastVertex(ex.Current(), true).IsSame(vIn)) return currentAbsc;
    }
  }
  return std::nullopt;
}

/*!@brief Get parameter along spine for edge and edge parameter
 * @details Get parameter along spine for edge and edge parameter
 * @param[in] edgeIn is an edge on the spine.
 * @param[in] prmIn is a parameter on edge.
 * @return Spine parameter or nullopt is edge is not on spine or prmIn out of edge range.
 */
std::optional<double> Adapter::toSpineParameter(const TopoDS_Edge &edgeIn, double prmIn) const
{
  assert(isValid());
  
  BRepAdaptor_Curve ac(edgeIn);
  if (prmIn < ac.FirstParameter() || prmIn > ac.LastParameter()) return std::nullopt;
  
  if (stow->mode == Stow::FilSpine)
  {
    //Are we going to be fighting orientation between pick edge and spine edge?
    int index = stow->spine->Index(edgeIn);
    if (index == 0) return std::nullopt; //occt is 1 based.
    return stow->spine->Absc(prmIn, index);
  }
  
  if (stow->mode == Stow::AdaptorCurve)
  {
    double currentAbsc = 0.0;
    BRepTools_WireExplorer ex(stow->curveAdaptor.Wire());
    for (; ex.More(); ex.Next())
    {
      GProp_GProps props;
      BRepGProp::LinearProperties(ex.Current(), props);
      if (!ex.Current().IsSame(edgeIn))
      {
        currentAbsc += props.Mass();
        continue;
      }
      double p0, p1;
      TopLoc_Location Dummy;
      auto curve = BRep_Tool::Curve(ex.Current(), p0, p1);
      auto copy = opencascade::handle(dynamic_cast<Geom_Curve*>(curve->Copy().get())); assert(copy);
      prmIn = std::clamp(prmIn, p0, p1);
      BRepBuilderAPI_MakeEdge em(copy, p0, prmIn);
      if (!em.IsDone()) return std::nullopt;
      BRepGProp::LinearProperties(em.Edge(), props);
      currentAbsc += props.Mass();
      return currentAbsc;
    }
  }
  return std::nullopt;
}

/*!@brief Get shape relative to spine parameter.
 * @param[in] prmIn is a parameter on spine.
 * @return Shape related to spine parameter.
 * @details Get shape relative to spine parameter.
 * If prmIn is out of spine range then first or last vertex
 * will be returned. Vertex shape is preferred.
 */
std::pair<TopoDS_Shape, double> Adapter::fromSpineParameter(double prmIn) const
{
  assert(isValid());
  
  if (stow->mode == Stow::FilSpine)
  {
    //check if out of range.
    if (prmIn <= stow->spine->FirstParameter())
      return std::make_pair(stow->spine->FirstVertex(), 0.0);
    if (prmIn >= stow->spine->LastParameter())
      return std::make_pair(stow->spine->LastVertex(), 0.0);
    
    //ChFiDS_FilSpine isn't set up to convert spine parameters to vertices. so we do it ourselves.
    occt::ShapeVector vertices;
    for (int index = 1; index <= stow->spine->NbEdges(); ++index)
    {
      auto ce = stow->spine->Edges(index);
      assert(stow->sShape.hasShape(ce));
      auto verts = stow->sShape.useGetChildrenOfType(ce, TopAbs_VERTEX);
      vertices.insert(vertices.end(), verts.begin(), verts.end());
    }
    occt::uniquefy(vertices);
    
    auto prmPoint = stow->spine->Value(prmIn);
    for (const auto &v : vertices)
    {
      auto vtxPoint = BRep_Tool::Pnt(TopoDS::Vertex(v));
      auto d = prmPoint.Distance(vtxPoint);
      if (d <= Precision::Confusion())
        return std::make_pair(v, 0.0);
    }
    
    //still here? just get the edge a parameter.
    auto edgeIndex = stow->spine->Index(prmIn);
    double edgePrm;
    stow->spine->Parameter(edgeIndex, prmIn, edgePrm);
    return std::make_pair(stow->spine->Edges(edgeIndex), edgePrm);
  }
  if (stow->mode == Stow::AdaptorCurve)
  {
    prmIn = std::clamp(prmIn, stow->curveAdaptor.FirstParameter(), stow->curveAdaptor.LastParameter());
    auto allVertices = stow->sShape.useGetChildrenOfType(stow->curveAdaptor.Wire(), TopAbs_VERTEX);
    occt::uniquefy(allVertices);
    auto prmPoint = stow->curveAdaptor.Value(prmIn);
    for (const auto &v : allVertices)
    {
      auto d = prmPoint.Distance(BRep_Tool::Pnt(TopoDS::Vertex(v)));
      if (d <= BRep_Tool::Tolerance(TopoDS::Vertex(v))) return std::make_pair(v, 0.0);
    }
    
    TopoDS_Edge edge;
    double edgeParameter;
    stow->curveAdaptor.Edge(prmIn, edge, edgeParameter);
    return std::make_pair(edge, edgeParameter);
  }
  return std::make_pair(TopoDS_Shape(), 0.0);
}

TopoDS_Wire Adapter::buildWire() const
{
  assert(isValid());
  
  if (stow->mode == Stow::FilSpine)
  {
    BRepBuilderAPI_MakeWire wm;
    for (int index = 1; index <= stow->spine->NbEdges(); ++index)
      wm.Add(stow->spine->Edges(index));
    assert(wm.IsDone());
    return wm;
  }
  if (stow->mode == Stow::AdaptorCurve) return stow->curveAdaptor.Wire();
  return TopoDS_Wire();
}

bool Adapter::isSpineEdge(const TopoDS_Edge &eIn) const
{
  if (stow->mode == Stow::FilSpine) return stow->spine->Index(eIn) != 0;
  if (stow->mode == Stow::AdaptorCurve)
  {
    if (!stow->sShape.hasShape(eIn)) return false;
    auto edgeId = stow->sShape.findId(eIn);
    assert(stow->sShape.hasShape(stow->curveAdaptor.Wire()));
    auto wireId = stow->sShape.findId(stow->curveAdaptor.Wire());
    auto wireParents = stow->sShape.useGetParentsOfType(edgeId, TopAbs_WIRE);
    for (const auto &wp : wireParents) if (wp == wireId) return true;
  }
  return false;
}

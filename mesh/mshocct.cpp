/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2020 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include <CGAL/Polygon_mesh_processing/repair.h>
#include <CGAL/Polygon_mesh_processing/repair_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>

#include <BRepBuilderAPI_Copy.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <BRep_Tool.hxx>
#include <BRepTools.hxx>
#include <TopoDS.hxx>
#include <Poly_Triangulation.hxx>

#include "tools/occtools.h"
#include "mesh/mshmesh.h"
#include "mesh/mshparameters.h"
#include "mesh/mshocct.h"

static msh::srf::Stow generate(const TopoDS_Shape &shapeIn, const msh::prm::OCCT &prmsIn)
{
  assert(!shapeIn.IsNull());
  msh::srf::Stow out;
  if (shapeIn.IsNull())
    return out;
  
  //copy shape so we leave mesh intact in the original shape.
  //don't copy geometry or mesh.
  BRepBuilderAPI_Copy copier(shapeIn, Standard_False, Standard_False);
  TopoDS_Shape copy(copier.Shape());
  BRepMesh_IncrementalMesh(copy, prmsIn);
  
  std::vector<msh::srf::Point> points;
  std::vector<std::vector<std::size_t>> polygons;
  
  for (const auto &face : occt::mapShapes(copy))
  {
    if (face.ShapeType() != TopAbs_FACE)
      continue;

    //meshes are defined at geometry location and need to be transformed
    //into topological position
    TopLoc_Location location;
    const Handle(Poly_Triangulation) &triangulation = BRep_Tool::Triangulation(TopoDS::Face(face), location);
    if (triangulation.IsNull())
    {
      std::cout << "warning null triangulation in face. " << BOOST_CURRENT_FUNCTION << std::endl;
      continue;
    }
    gp_Trsf transformation = location.Transformation();
    
    std::size_t offset = points.size();
    for (int index = 1; index < triangulation->NbNodes() + 1; ++index)
    {
      gp_Pnt point = triangulation->Node(index);
      point.Transform(transformation);
      points.emplace_back(msh::srf::Point(point.X(), point.Y(), point.Z()));
    }
    
    for (int index = 1; index < triangulation->NbTriangles() + 1; ++index)
    {
      int N1, N2, N3;
      triangulation->Triangle(index).Get(N1, N2, N3);
      std::vector<std::size_t> polygon;
      if (face.Orientation() == TopAbs_FORWARD)
      {
        polygon.push_back(N1 - 1 + offset);
        polygon.push_back(N2 - 1 + offset);
        polygon.push_back(N3 - 1 + offset);
      }
      else
      {
        polygon.push_back(N1 - 1 + offset);
        polygon.push_back(N3 - 1 + offset);
        polygon.push_back(N2 - 1 + offset);
      }
      polygons.push_back(polygon);
    }
  }
  
  CGAL::Polygon_mesh_processing::repair_polygon_soup(points, polygons);
  CGAL::Polygon_mesh_processing::orient_polygon_soup(points, polygons);
  if (!CGAL::Polygon_mesh_processing::is_polygon_soup_a_polygon_mesh(polygons))
  {
    std::cout << "INVALID polygon soup" << std::endl;
    return out;
  }
  CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(points, polygons, out.mesh);
  
  CGAL::Polygon_mesh_processing::remove_isolated_vertices(out.mesh);
  out.mesh.collect_garbage();
  return out;
}

/*! @brief Generate a mesh from a face with parameters.
 * 
 * @param faceIn The source face for a surface mesh. Expects not null
 * @param prmsIn The parameters controlling surface mesh generation.
 * 
 */
msh::srf::Stow msh::srf::generate(const TopoDS_Face &faceIn, const msh::prm::OCCT &prmsIn)
{
  return generate(dynamic_cast<const TopoDS_Shape&>(faceIn), prmsIn);
}

/*! @brief Generate a mesh from a shell with parameters.
 * 
 * @param shellIn The source shell for a surface mesh. Expects not null
 * @param prmsIn The parameters controlling surface mesh generation.
 * 
 */
msh::srf::Stow msh::srf::generate(const TopoDS_Shell &shellIn, const msh::prm::OCCT &prmsIn)
{
  return generate(dynamic_cast<const TopoDS_Shape&>(shellIn), prmsIn);
}

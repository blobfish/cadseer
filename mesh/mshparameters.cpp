/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <boost/bimap.hpp>
#include <boost/assign/list_of.hpp>

#include "project/serial/generated/prjsrlmshsmesh.h"
#include "mesh/mshparameters.h"

using namespace msh::prm;

/*
    IMeshTools_Parameters defaults:
    Angle(0.5),
    Deflection(0.001),
    AngleInterior(-1.0),
    DeflectionInterior(-1.0),
    MinSize (-1.0),
    InParallel (Standard_False),
    Relative (Standard_False),
    InternalVerticesMode (Standard_True),
    ControlSurfaceDeflection (Standard_True),
    CleanModel (Standard_True),
    AdjustMinSize (Standard_False)
*/
OCCT::OCCT()
: IMeshTools_Parameters()
{
  //changing some defaults.
  Deflection = 0.25;
  InParallel = Standard_True;
}

void OCCT::serialIn(const prj::srl::mshs::ParametersOCCT &psIn)
{
  Angle = psIn.angle();
  Deflection = psIn.deflection();
  AngleInterior = psIn.angleInterior();
  DeflectionInterior = psIn.deflectionInterior();
  MinSize = psIn.minSize();
  InParallel = psIn.inParallel();
  Relative = psIn.relative();
  InternalVerticesMode = psIn.internalVerticesMode();
  ControlSurfaceDeflection = psIn.controlSurfaceDeflection();
  CleanModel = psIn.cleanModel();
  AdjustMinSize = psIn.adjustMinSize();
}

prj::srl::mshs::ParametersOCCT OCCT::serialOut() const
{
  return prj::srl::mshs::ParametersOCCT
  (
    Angle
    , Deflection
    , AngleInterior
    , DeflectionInterior
    , MinSize
    , InParallel
    , Relative
    , InternalVerticesMode
    , ControlSurfaceDeflection
    , CleanModel
    , AdjustMinSize
  );
}

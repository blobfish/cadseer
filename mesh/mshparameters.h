/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2019  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MSH_PARAMETERS_H
#define MSH_PARAMETERS_H

#include <IMeshTools_Parameters.hxx>

namespace prj
{
  namespace srl
  {
    namespace mshs
    {
      class ParametersOCCT;
    }
  }
}

namespace msh
{
  namespace prm
  {
    /**
    * @struct OCCT
    * @brief Parameters for generating mesh using occt
    */
    struct OCCT : public IMeshTools_Parameters
    {
      OCCT();
      void serialIn(const prj::srl::mshs::ParametersOCCT&);
      prj::srl::mshs::ParametersOCCT serialOut() const;
    };
  }
}

#endif // MSH_PARAMETERS_H

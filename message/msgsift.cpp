/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>

#include "message/msgsift.h"

using namespace msg;

Sift::Sift(std::initializer_list<Map::value_type> mIn)
{
  mapPairs.insert(mIn);
}

void Sift::insert(Mask m, Handler h)
{
  mapPairs.insert(std::make_pair(m, h));
}

void Sift::insert(std::initializer_list<Map::value_type> mIn)
{
  mapPairs.insert(mIn);
}

void Sift::receive(const Message &mIn) const
{
  auto it = mapPairs.find(mIn.mask);
  if (it == mapPairs.end()) return;

  if (stackDepth > 1) //Let's not worry about 1 recursion.
    std::cout << "WARNING: " << name << " stack depth: " << stackDepth << std::endl;
  stackDepth++;
  it->second(mIn);
  stackDepth--;
}

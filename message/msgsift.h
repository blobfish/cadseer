/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MSG_SIFT_H
#define MSG_SIFT_H

#include <unordered_map>

#include "message/msgmessage.h"

namespace msg
{
  /*! @struct Sift
   * @brief Message filter.
   * 
   * @details Objects can use this structure to filter
   * incoming messages of interest.
   */
  struct Sift
  {
  public:
    using Map = std::unordered_map<Mask, Handler>;
    
    Sift() = default;
    Sift(std::initializer_list<Map::value_type>);
    void insert(Mask, Handler);
    void insert(std::initializer_list<Map::value_type>);
    void receive(const Message&) const;
    std::string name = "no name"; //used for any error messages.
  private:
    Map mapPairs;
    mutable std::size_t stackDepth = 0;
  };
}

#endif // MSG_SIFT_H

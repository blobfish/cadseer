/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include <osg/Geometry>
#include <osg/LineWidth>

#include "modelviz/mdvcurvecomb.h"

using namespace mdv;

void CurveCombCallback::setPoints(const std::vector<osg::Vec3d> &crv, const std::vector<osg::Vec3d> &cmb)
{
  if (crv.size() != cmb.size()) return;
  curvePoints = crv;
  combPoints = cmb;
  dirty = true;
}

bool CurveCombCallback::run(osg::Object *object, osg::Object *data)
{
  if (!dirty) return traverse(object, data);
  if (curvePoints.empty()) return traverse(object, data);
  if (curvePoints.size() != combPoints.size()) return false;
  
  osg::MatrixTransform *t = dynamic_cast<osg::MatrixTransform*>(object); assert(t); if (!t) return false;
  
  osg::Geometry *fins = dynamic_cast<osg::Geometry*>(t->getChild(0)); assert(fins); if (!fins) return false;
  osg::Vec3Array *finVerts = dynamic_cast<osg::Vec3Array*>(fins->getVertexArray()); assert(finVerts);
  finVerts->clear();
  for (std::size_t i = 0; i < curvePoints.size(); ++i)
  {
    finVerts->push_back(curvePoints.at(i));
    finVerts->push_back(combPoints.at(i));
  }
  auto *finSet = dynamic_cast<osg::DrawArrays*>(fins->getPrimitiveSet(0)); assert(finSet);
  finSet->setFirst(0);
  finSet->setCount(finVerts->size());
  
  osg::Geometry *path = dynamic_cast<osg::Geometry*>(t->getChild(1)); assert(path); if (!path) return false;
  osg::Vec3Array *pathVerts = dynamic_cast<osg::Vec3Array*>(path->getVertexArray()); assert(pathVerts);
  pathVerts->clear();
  pathVerts->insert(pathVerts->end(), combPoints.begin(), combPoints.end());
  auto *pathSet = dynamic_cast<osg::DrawArrays*>(path->getPrimitiveSet(0)); assert(pathSet);
  pathSet->setFirst(0);
  pathSet->setCount(combPoints.size());
  
  dirty = false;
  return traverse(object, data);
}

//to make 2 or 1 geometries.
osg::MatrixTransform* mdv::buildCurveComb()
{
  auto *out = new osg::MatrixTransform();
  auto addGeometry = [&](const osg::Vec4 &cIn) -> osg::Geometry*
  {
    osg::Geometry *geometry = new osg::Geometry();
    geometry->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    geometry->setDataVariance(osg::Object::DYNAMIC);
    geometry->setUseDisplayList(false);
    osg::Vec4Array *ca = new osg::Vec4Array();
    ca->push_back(cIn);
    geometry->setColorArray(ca);
    geometry->setColorBinding(osg::Geometry::BIND_OVERALL);
    geometry->setVertexArray(new osg::Vec3Array());
    out->addChild(geometry);
    return geometry;
  };
  
  auto fins = addGeometry(osg::Vec4(0.0, 0.0, 1.0, 1.0));
  fins->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 0, 0));
  
  auto path = addGeometry(osg::Vec4(0.0, 1.0, 0.0, 1.0));
  path->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, 0));
  path->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(5.0));
  
  out->setUpdateCallback(new CurveCombCallback());
  return out;
}

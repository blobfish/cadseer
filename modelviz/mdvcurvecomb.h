/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MDV_CURVECOMB_H
#define MDV_CURVECOMB_H

#include <vector>

#include <osg/Vec3d>
#include <osg/Callback>
#include <osg/MatrixTransform>

namespace mdv
{
  class CurveCombCallback : public osg::Callback
  {
  public:
    void setPoints(const std::vector<osg::Vec3d>&, const std::vector<osg::Vec3d>&);
    bool run(osg::Object*, osg::Object*) override;
  protected:
    bool dirty = true;
    std::vector<osg::Vec3d> curvePoints;
    std::vector<osg::Vec3d> combPoints;
  };
  
  osg::MatrixTransform* buildCurveComb();
}

#endif //MDV_CURVECOMB_H

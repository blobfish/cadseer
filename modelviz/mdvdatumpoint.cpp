/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023 Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <cassert>

#include <osg/PolygonMode>
#include <osgUtil/Optimizer>

#include "library/lbrspherebuilder.h"
#include "modelviz/mdvnodemaskdefs.h"
#include "modelviz/mdvdatumpoint.h"

using namespace mdv;

DatumPoint::DatumPoint() : Base()
{
  setUseDisplayList(false);
  setDataVariance(osg::Object::DYNAMIC);
  setUseVertexBufferObjects(true);
  setNodeMask(mdv::datum);
  setName("datumPoint");
  getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
  getOrCreateStateSet()->setAttributeAndModes(new osg::PolygonMode(osg::PolygonMode::FRONT, osg::PolygonMode::FILL));
  color = osg::Vec4(1.0, 0.8, 0.0, 1.0);
  
  setVertexArray(new osg::Vec3Array());
  setNormalArray(new osg::Vec3Array(osg::Array::BIND_PER_VERTEX));
  setColorArray(new osg::Vec4Array(osg::Array::BIND_OVERALL));
  
  lbr::SphereBuilder sBuilder;
  sBuilder.setRadius(1.0);
  sBuilder.setIsoLines(16);
  osg::ref_ptr<osg::Geometry> temp(sBuilder);
  temp->setColorArray(new osg::Vec4Array(osg::Array::BIND_OVERALL, 1, &color));
  osgUtil::Optimizer::MergeGeometryVisitor::mergeGeometry(*this, *temp);
}

DatumPoint::DatumPoint(const DatumPoint &rhs, const osg::CopyOp& copyOperation) : Base(rhs, copyOperation)
{
  setUseDisplayList(false);
  setDataVariance(osg::Object::DYNAMIC);
}

void DatumPoint::setToColor()
{
  auto *colors = dynamic_cast<osg::Vec4Array*>(getColorArray()); assert(colors);
  assert(colors->size() == 1);
  colors->at(0) = color;
  colors->dirty();
}

void DatumPoint::setToPreHighlight()
{
  auto *colors = dynamic_cast<osg::Vec4Array*>(getColorArray()); assert(colors);
  assert(colors->size() == 1);
  colors->at(0) = colorPreHighlight;
  colors->dirty();
}

void DatumPoint::setToHighlight()
{
  auto *colors = dynamic_cast<osg::Vec4Array*>(getColorArray()); assert(colors);
  assert(colors->size() == 1);
  colors->at(0) = colorHighlight;
  colors->dirty();
}

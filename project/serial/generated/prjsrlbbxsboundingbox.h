// Copyright (c) 2005-2014 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD, an XML Schema to
// C++ data binding compiler.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// In addition, as a special exception, Code Synthesis Tools CC gives
// permission to link this program with the Xerces-C++ library (or with
// modified versions of Xerces-C++ that use the same license as Xerces-C++),
// and distribute linked combinations including the two. You must obey
// the GNU General Public License version 2 in all respects for all of
// the code used other than Xerces-C++. If you modify this copy of the
// program, you may extend this exception to your version of the program,
// but you are not obligated to do so. If you do not wish to do so, delete
// this exception statement from your version.
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef PRJ_SRL_BBXS_PRJSRLBBXSBOUNDINGBOX_H
#define PRJ_SRL_BBXS_PRJSRLBBXSBOUNDINGBOX_H

#ifndef XSD_CXX11
#define XSD_CXX11
#endif

#ifndef XSD_USE_CHAR
#define XSD_USE_CHAR
#endif

#ifndef XSD_CXX_TREE_USE_CHAR
#define XSD_CXX_TREE_USE_CHAR
#endif

// Begin prologue.
//
//
// End prologue.

#include <xsd/cxx/config.hxx>

#if (XSD_INT_VERSION != 4000000L)
#error XSD runtime version mismatch
#endif

#include <xsd/cxx/pre.hxx>

#include "../../../xmlbase.h"

// Forward declarations.
//
namespace prj
{
  namespace srl
  {
    namespace bbxs
    {
      class BoundingBox;
    }
  }
}


#include <memory>    // ::std::unique_ptr
#include <limits>    // std::numeric_limits
#include <algorithm> // std::binary_search
#include <utility>   // std::move

#include <xsd/cxx/xml/char-utf8.hxx>

#include <xsd/cxx/tree/exceptions.hxx>
#include <xsd/cxx/tree/elements.hxx>
#include <xsd/cxx/tree/containers.hxx>
#include <xsd/cxx/tree/list.hxx>

#include <xsd/cxx/xml/dom/parsing-header.hxx>

#include "prjsrlsptparameter.h"

#include "prjsrlsptoverlay.h"

#include "prjsrlsptbase.h"

namespace prj
{
  namespace srl
  {
    namespace bbxs
    {
      class BoundingBox: public ::xml_schema::Type
      {
        public:
        // base
        //
        typedef ::prj::srl::spt::Base BaseType;
        typedef ::xsd::cxx::tree::traits< BaseType, char > BaseTraits;

        const BaseType&
        base () const;

        BaseType&
        base ();

        void
        base (const BaseType& x);

        void
        base (::std::unique_ptr< BaseType > p);

        // BBType
        //
        typedef ::prj::srl::spt::Parameter BBTypeType;
        typedef ::xsd::cxx::tree::traits< BBTypeType, char > BBTypeTraits;

        const BBTypeType&
        BBType () const;

        BBTypeType&
        BBType ();

        void
        BBType (const BBTypeType& x);

        void
        BBType (::std::unique_ptr< BBTypeType > p);

        // picks
        //
        typedef ::prj::srl::spt::Parameter PicksType;
        typedef ::xsd::cxx::tree::traits< PicksType, char > PicksTraits;

        const PicksType&
        picks () const;

        PicksType&
        picks ();

        void
        picks (const PicksType& x);

        void
        picks (::std::unique_ptr< PicksType > p);

        // origin
        //
        typedef ::prj::srl::spt::Parameter OriginType;
        typedef ::xsd::cxx::tree::traits< OriginType, char > OriginTraits;

        const OriginType&
        origin () const;

        OriginType&
        origin ();

        void
        origin (const OriginType& x);

        void
        origin (::std::unique_ptr< OriginType > p);

        // length
        //
        typedef ::prj::srl::spt::Parameter LengthType;
        typedef ::xsd::cxx::tree::traits< LengthType, char > LengthTraits;

        const LengthType&
        length () const;

        LengthType&
        length ();

        void
        length (const LengthType& x);

        void
        length (::std::unique_ptr< LengthType > p);

        // width
        //
        typedef ::prj::srl::spt::Parameter WidthType;
        typedef ::xsd::cxx::tree::traits< WidthType, char > WidthTraits;

        const WidthType&
        width () const;

        WidthType&
        width ();

        void
        width (const WidthType& x);

        void
        width (::std::unique_ptr< WidthType > p);

        // height
        //
        typedef ::prj::srl::spt::Parameter HeightType;
        typedef ::xsd::cxx::tree::traits< HeightType, char > HeightTraits;

        const HeightType&
        height () const;

        HeightType&
        height ();

        void
        height (const HeightType& x);

        void
        height (::std::unique_ptr< HeightType > p);

        // BBTypeLabel
        //
        typedef ::prj::srl::spt::PLabel BBTypeLabelType;
        typedef ::xsd::cxx::tree::traits< BBTypeLabelType, char > BBTypeLabelTraits;

        const BBTypeLabelType&
        BBTypeLabel () const;

        BBTypeLabelType&
        BBTypeLabel ();

        void
        BBTypeLabel (const BBTypeLabelType& x);

        void
        BBTypeLabel (::std::unique_ptr< BBTypeLabelType > p);

        // originLabel
        //
        typedef ::prj::srl::spt::PLabel OriginLabelType;
        typedef ::xsd::cxx::tree::traits< OriginLabelType, char > OriginLabelTraits;

        const OriginLabelType&
        originLabel () const;

        OriginLabelType&
        originLabel ();

        void
        originLabel (const OriginLabelType& x);

        void
        originLabel (::std::unique_ptr< OriginLabelType > p);

        // lengthLabel
        //
        typedef ::prj::srl::spt::PLabel LengthLabelType;
        typedef ::xsd::cxx::tree::traits< LengthLabelType, char > LengthLabelTraits;

        const LengthLabelType&
        lengthLabel () const;

        LengthLabelType&
        lengthLabel ();

        void
        lengthLabel (const LengthLabelType& x);

        void
        lengthLabel (::std::unique_ptr< LengthLabelType > p);

        // widthLabel
        //
        typedef ::prj::srl::spt::PLabel WidthLabelType;
        typedef ::xsd::cxx::tree::traits< WidthLabelType, char > WidthLabelTraits;

        const WidthLabelType&
        widthLabel () const;

        WidthLabelType&
        widthLabel ();

        void
        widthLabel (const WidthLabelType& x);

        void
        widthLabel (::std::unique_ptr< WidthLabelType > p);

        // heightLabel
        //
        typedef ::prj::srl::spt::PLabel HeightLabelType;
        typedef ::xsd::cxx::tree::traits< HeightLabelType, char > HeightLabelTraits;

        const HeightLabelType&
        heightLabel () const;

        HeightLabelType&
        heightLabel ();

        void
        heightLabel (const HeightLabelType& x);

        void
        heightLabel (::std::unique_ptr< HeightLabelType > p);

        // gridLocation
        //
        typedef ::prj::srl::spt::Vec3d GridLocationType;
        typedef ::xsd::cxx::tree::traits< GridLocationType, char > GridLocationTraits;

        const GridLocationType&
        gridLocation () const;

        GridLocationType&
        gridLocation ();

        void
        gridLocation (const GridLocationType& x);

        void
        gridLocation (::std::unique_ptr< GridLocationType > p);

        // Constructors.
        //
        BoundingBox (const BaseType&,
                     const BBTypeType&,
                     const PicksType&,
                     const OriginType&,
                     const LengthType&,
                     const WidthType&,
                     const HeightType&,
                     const BBTypeLabelType&,
                     const OriginLabelType&,
                     const LengthLabelType&,
                     const WidthLabelType&,
                     const HeightLabelType&,
                     const GridLocationType&);

        BoundingBox (::std::unique_ptr< BaseType >,
                     ::std::unique_ptr< BBTypeType >,
                     ::std::unique_ptr< PicksType >,
                     ::std::unique_ptr< OriginType >,
                     ::std::unique_ptr< LengthType >,
                     ::std::unique_ptr< WidthType >,
                     ::std::unique_ptr< HeightType >,
                     ::std::unique_ptr< BBTypeLabelType >,
                     ::std::unique_ptr< OriginLabelType >,
                     ::std::unique_ptr< LengthLabelType >,
                     ::std::unique_ptr< WidthLabelType >,
                     ::std::unique_ptr< HeightLabelType >,
                     ::std::unique_ptr< GridLocationType >);

        BoundingBox (const ::xercesc::DOMElement& e,
                     ::xml_schema::Flags f = 0,
                     ::xml_schema::Container* c = 0);

        BoundingBox (const BoundingBox& x,
                     ::xml_schema::Flags f = 0,
                     ::xml_schema::Container* c = 0);

        virtual BoundingBox*
        _clone (::xml_schema::Flags f = 0,
                ::xml_schema::Container* c = 0) const;

        BoundingBox&
        operator= (const BoundingBox& x);

        virtual 
        ~BoundingBox ();

        // Implementation.
        //
        protected:
        void
        parse (::xsd::cxx::xml::dom::parser< char >&,
               ::xml_schema::Flags);

        protected:
        ::xsd::cxx::tree::one< BaseType > base_;
        ::xsd::cxx::tree::one< BBTypeType > BBType_;
        ::xsd::cxx::tree::one< PicksType > picks_;
        ::xsd::cxx::tree::one< OriginType > origin_;
        ::xsd::cxx::tree::one< LengthType > length_;
        ::xsd::cxx::tree::one< WidthType > width_;
        ::xsd::cxx::tree::one< HeightType > height_;
        ::xsd::cxx::tree::one< BBTypeLabelType > BBTypeLabel_;
        ::xsd::cxx::tree::one< OriginLabelType > originLabel_;
        ::xsd::cxx::tree::one< LengthLabelType > lengthLabel_;
        ::xsd::cxx::tree::one< WidthLabelType > widthLabel_;
        ::xsd::cxx::tree::one< HeightLabelType > heightLabel_;
        ::xsd::cxx::tree::one< GridLocationType > gridLocation_;
      };
    }
  }
}

#include <iosfwd>

#include <xercesc/sax/InputSource.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>

namespace prj
{
  namespace srl
  {
    namespace bbxs
    {
      // Parse a URI or a local file.
      //

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (const ::std::string& uri,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (const ::std::string& uri,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (const ::std::string& uri,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse std::istream.
      //

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::std::istream& is,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::std::istream& is,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::std::istream& is,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::std::istream& is,
                   const ::std::string& id,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::std::istream& is,
                   const ::std::string& id,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::std::istream& is,
                   const ::std::string& id,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse xercesc::InputSource.
      //

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::xercesc::InputSource& is,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::xercesc::InputSource& is,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::xercesc::InputSource& is,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse xercesc::DOMDocument.
      //

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (const ::xercesc::DOMDocument& d,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::bbxs::BoundingBox >
      boundingbox (::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());
    }
  }
}

#include <iosfwd>

#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/framework/XMLFormatter.hpp>

#include <xsd/cxx/xml/dom/auto-ptr.hxx>

namespace prj
{
  namespace srl
  {
    namespace bbxs
    {
      void
      operator<< (::xercesc::DOMElement&, const BoundingBox&);

      // Serialize to std::ostream.
      //

      void
      boundingbox (::std::ostream& os,
                   const ::prj::srl::bbxs::BoundingBox& x, 
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      boundingbox (::std::ostream& os,
                   const ::prj::srl::bbxs::BoundingBox& x, 
                   ::xml_schema::ErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      boundingbox (::std::ostream& os,
                   const ::prj::srl::bbxs::BoundingBox& x, 
                   ::xercesc::DOMErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      // Serialize to xercesc::XMLFormatTarget.
      //

      void
      boundingbox (::xercesc::XMLFormatTarget& ft,
                   const ::prj::srl::bbxs::BoundingBox& x, 
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      boundingbox (::xercesc::XMLFormatTarget& ft,
                   const ::prj::srl::bbxs::BoundingBox& x, 
                   ::xml_schema::ErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      boundingbox (::xercesc::XMLFormatTarget& ft,
                   const ::prj::srl::bbxs::BoundingBox& x, 
                   ::xercesc::DOMErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      // Serialize to an existing xercesc::DOMDocument.
      //

      void
      boundingbox (::xercesc::DOMDocument& d,
                   const ::prj::srl::bbxs::BoundingBox& x,
                   ::xml_schema::Flags f = 0);

      // Serialize to a new xercesc::DOMDocument.
      //

      ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument >
      boundingbox (const ::prj::srl::bbxs::BoundingBox& x, 
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   ::xml_schema::Flags f = 0);
    }
  }
}

#include <xsd/cxx/post.hxx>

// Begin epilogue.
//
//
// End epilogue.

#endif // PRJ_SRL_BBXS_PRJSRLBBXSBOUNDINGBOX_H

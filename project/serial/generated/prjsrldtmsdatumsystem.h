// Copyright (c) 2005-2014 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD, an XML Schema to
// C++ data binding compiler.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// In addition, as a special exception, Code Synthesis Tools CC gives
// permission to link this program with the Xerces-C++ library (or with
// modified versions of Xerces-C++ that use the same license as Xerces-C++),
// and distribute linked combinations including the two. You must obey
// the GNU General Public License version 2 in all respects for all of
// the code used other than Xerces-C++. If you modify this copy of the
// program, you may extend this exception to your version of the program,
// but you are not obligated to do so. If you do not wish to do so, delete
// this exception statement from your version.
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef PRJ_SRL_DTMS_PRJSRLDTMSDATUMSYSTEM_H
#define PRJ_SRL_DTMS_PRJSRLDTMSDATUMSYSTEM_H

#ifndef XSD_CXX11
#define XSD_CXX11
#endif

#ifndef XSD_USE_CHAR
#define XSD_USE_CHAR
#endif

#ifndef XSD_CXX_TREE_USE_CHAR
#define XSD_CXX_TREE_USE_CHAR
#endif

// Begin prologue.
//
//
// End prologue.

#include <xsd/cxx/config.hxx>

#if (XSD_INT_VERSION != 4000000L)
#error XSD runtime version mismatch
#endif

#include <xsd/cxx/pre.hxx>

#include "../../../xmlbase.h"

// Forward declarations.
//
namespace prj
{
  namespace srl
  {
    namespace dtms
    {
      class DatumSystem;
    }
  }
}


#include <memory>    // ::std::unique_ptr
#include <limits>    // std::numeric_limits
#include <algorithm> // std::binary_search
#include <utility>   // std::move

#include <xsd/cxx/xml/char-utf8.hxx>

#include <xsd/cxx/tree/exceptions.hxx>
#include <xsd/cxx/tree/elements.hxx>
#include <xsd/cxx/tree/containers.hxx>
#include <xsd/cxx/tree/list.hxx>

#include <xsd/cxx/xml/dom/parsing-header.hxx>

#include "prjsrlsptparameter.h"

#include "prjsrlsptoverlay.h"

#include "prjsrlsptbase.h"

namespace prj
{
  namespace srl
  {
    namespace dtms
    {
      class DatumSystem: public ::xml_schema::Type
      {
        public:
        // base
        //
        typedef ::prj::srl::spt::Base BaseType;
        typedef ::xsd::cxx::tree::traits< BaseType, char > BaseTraits;

        const BaseType&
        base () const;

        BaseType&
        base ();

        void
        base (const BaseType& x);

        void
        base (::std::unique_ptr< BaseType > p);

        // systemType
        //
        typedef ::prj::srl::spt::Parameter SystemTypeType;
        typedef ::xsd::cxx::tree::traits< SystemTypeType, char > SystemTypeTraits;

        const SystemTypeType&
        systemType () const;

        SystemTypeType&
        systemType ();

        void
        systemType (const SystemTypeType& x);

        void
        systemType (::std::unique_ptr< SystemTypeType > p);

        // csys
        //
        typedef ::prj::srl::spt::Parameter CsysType;
        typedef ::xsd::cxx::tree::traits< CsysType, char > CsysTraits;

        const CsysType&
        csys () const;

        CsysType&
        csys ();

        void
        csys (const CsysType& x);

        void
        csys (::std::unique_ptr< CsysType > p);

        // autoSize
        //
        typedef ::prj::srl::spt::Parameter AutoSizeType;
        typedef ::xsd::cxx::tree::traits< AutoSizeType, char > AutoSizeTraits;

        const AutoSizeType&
        autoSize () const;

        AutoSizeType&
        autoSize ();

        void
        autoSize (const AutoSizeType& x);

        void
        autoSize (::std::unique_ptr< AutoSizeType > p);

        // size
        //
        typedef ::prj::srl::spt::Parameter SizeType;
        typedef ::xsd::cxx::tree::traits< SizeType, char > SizeTraits;

        const SizeType&
        size () const;

        SizeType&
        size ();

        void
        size (const SizeType& x);

        void
        size (::std::unique_ptr< SizeType > p);

        // linkedPicks
        //
        typedef ::prj::srl::spt::Parameter LinkedPicksType;
        typedef ::xsd::cxx::tree::optional< LinkedPicksType > LinkedPicksOptional;
        typedef ::xsd::cxx::tree::traits< LinkedPicksType, char > LinkedPicksTraits;

        const LinkedPicksOptional&
        linkedPicks () const;

        LinkedPicksOptional&
        linkedPicks ();

        void
        linkedPicks (const LinkedPicksType& x);

        void
        linkedPicks (const LinkedPicksOptional& x);

        void
        linkedPicks (::std::unique_ptr< LinkedPicksType > p);

        // pointsPicks
        //
        typedef ::prj::srl::spt::Parameter PointsPicksType;
        typedef ::xsd::cxx::tree::optional< PointsPicksType > PointsPicksOptional;
        typedef ::xsd::cxx::tree::traits< PointsPicksType, char > PointsPicksTraits;

        const PointsPicksOptional&
        pointsPicks () const;

        PointsPicksOptional&
        pointsPicks ();

        void
        pointsPicks (const PointsPicksType& x);

        void
        pointsPicks (const PointsPicksOptional& x);

        void
        pointsPicks (::std::unique_ptr< PointsPicksType > p);

        // shapeInferPicks
        //
        typedef ::prj::srl::spt::Parameter ShapeInferPicksType;
        typedef ::xsd::cxx::tree::optional< ShapeInferPicksType > ShapeInferPicksOptional;
        typedef ::xsd::cxx::tree::traits< ShapeInferPicksType, char > ShapeInferPicksTraits;

        const ShapeInferPicksOptional&
        shapeInferPicks () const;

        ShapeInferPicksOptional&
        shapeInferPicks ();

        void
        shapeInferPicks (const ShapeInferPicksType& x);

        void
        shapeInferPicks (const ShapeInferPicksOptional& x);

        void
        shapeInferPicks (::std::unique_ptr< ShapeInferPicksType > p);

        // originPick
        //
        typedef ::prj::srl::spt::Parameter OriginPickType;
        typedef ::xsd::cxx::tree::optional< OriginPickType > OriginPickOptional;
        typedef ::xsd::cxx::tree::traits< OriginPickType, char > OriginPickTraits;

        const OriginPickOptional&
        originPick () const;

        OriginPickOptional&
        originPick ();

        void
        originPick (const OriginPickType& x);

        void
        originPick (const OriginPickOptional& x);

        void
        originPick (::std::unique_ptr< OriginPickType > p);

        // axis0Picks
        //
        typedef ::prj::srl::spt::Parameter Axis0PicksType;
        typedef ::xsd::cxx::tree::optional< Axis0PicksType > Axis0PicksOptional;
        typedef ::xsd::cxx::tree::traits< Axis0PicksType, char > Axis0PicksTraits;

        const Axis0PicksOptional&
        axis0Picks () const;

        Axis0PicksOptional&
        axis0Picks ();

        void
        axis0Picks (const Axis0PicksType& x);

        void
        axis0Picks (const Axis0PicksOptional& x);

        void
        axis0Picks (::std::unique_ptr< Axis0PicksType > p);

        // axis1Picks
        //
        typedef ::prj::srl::spt::Parameter Axis1PicksType;
        typedef ::xsd::cxx::tree::optional< Axis1PicksType > Axis1PicksOptional;
        typedef ::xsd::cxx::tree::traits< Axis1PicksType, char > Axis1PicksTraits;

        const Axis1PicksOptional&
        axis1Picks () const;

        Axis1PicksOptional&
        axis1Picks ();

        void
        axis1Picks (const Axis1PicksType& x);

        void
        axis1Picks (const Axis1PicksOptional& x);

        void
        axis1Picks (::std::unique_ptr< Axis1PicksType > p);

        // axisStyle
        //
        typedef ::prj::srl::spt::Parameter AxisStyleType;
        typedef ::xsd::cxx::tree::optional< AxisStyleType > AxisStyleOptional;
        typedef ::xsd::cxx::tree::traits< AxisStyleType, char > AxisStyleTraits;

        const AxisStyleOptional&
        axisStyle () const;

        AxisStyleOptional&
        axisStyle ();

        void
        axisStyle (const AxisStyleType& x);

        void
        axisStyle (const AxisStyleOptional& x);

        void
        axisStyle (::std::unique_ptr< AxisStyleType > p);

        // postOps
        //
        typedef ::prj::srl::spt::Parameter PostOpsType;
        typedef ::xsd::cxx::tree::sequence< PostOpsType > PostOpsSequence;
        typedef PostOpsSequence::iterator PostOpsIterator;
        typedef PostOpsSequence::const_iterator PostOpsConstIterator;
        typedef ::xsd::cxx::tree::traits< PostOpsType, char > PostOpsTraits;

        const PostOpsSequence&
        postOps () const;

        PostOpsSequence&
        postOps ();

        void
        postOps (const PostOpsSequence& s);

        // csysDragger
        //
        typedef ::prj::srl::spt::CSysDragger CsysDraggerType;
        typedef ::xsd::cxx::tree::traits< CsysDraggerType, char > CsysDraggerTraits;

        const CsysDraggerType&
        csysDragger () const;

        CsysDraggerType&
        csysDragger ();

        void
        csysDragger (const CsysDraggerType& x);

        void
        csysDragger (::std::unique_ptr< CsysDraggerType > p);

        // autoSizeLabel
        //
        typedef ::prj::srl::spt::PLabel AutoSizeLabelType;
        typedef ::xsd::cxx::tree::traits< AutoSizeLabelType, char > AutoSizeLabelTraits;

        const AutoSizeLabelType&
        autoSizeLabel () const;

        AutoSizeLabelType&
        autoSizeLabel ();

        void
        autoSizeLabel (const AutoSizeLabelType& x);

        void
        autoSizeLabel (::std::unique_ptr< AutoSizeLabelType > p);

        // sizeLabel
        //
        typedef ::prj::srl::spt::PLabel SizeLabelType;
        typedef ::xsd::cxx::tree::traits< SizeLabelType, char > SizeLabelTraits;

        const SizeLabelType&
        sizeLabel () const;

        SizeLabelType&
        sizeLabel ();

        void
        sizeLabel (const SizeLabelType& x);

        void
        sizeLabel (::std::unique_ptr< SizeLabelType > p);

        // cachedSize
        //
        typedef ::xml_schema::Double CachedSizeType;
        typedef ::xsd::cxx::tree::traits< CachedSizeType, char, ::xsd::cxx::tree::schema_type::double_ > CachedSizeTraits;

        const CachedSizeType&
        cachedSize () const;

        CachedSizeType&
        cachedSize ();

        void
        cachedSize (const CachedSizeType& x);

        // Constructors.
        //
        DatumSystem (const BaseType&,
                     const SystemTypeType&,
                     const CsysType&,
                     const AutoSizeType&,
                     const SizeType&,
                     const CsysDraggerType&,
                     const AutoSizeLabelType&,
                     const SizeLabelType&,
                     const CachedSizeType&);

        DatumSystem (::std::unique_ptr< BaseType >,
                     ::std::unique_ptr< SystemTypeType >,
                     ::std::unique_ptr< CsysType >,
                     ::std::unique_ptr< AutoSizeType >,
                     ::std::unique_ptr< SizeType >,
                     ::std::unique_ptr< CsysDraggerType >,
                     ::std::unique_ptr< AutoSizeLabelType >,
                     ::std::unique_ptr< SizeLabelType >,
                     const CachedSizeType&);

        DatumSystem (const ::xercesc::DOMElement& e,
                     ::xml_schema::Flags f = 0,
                     ::xml_schema::Container* c = 0);

        DatumSystem (const DatumSystem& x,
                     ::xml_schema::Flags f = 0,
                     ::xml_schema::Container* c = 0);

        virtual DatumSystem*
        _clone (::xml_schema::Flags f = 0,
                ::xml_schema::Container* c = 0) const;

        DatumSystem&
        operator= (const DatumSystem& x);

        virtual 
        ~DatumSystem ();

        // Implementation.
        //
        protected:
        void
        parse (::xsd::cxx::xml::dom::parser< char >&,
               ::xml_schema::Flags);

        protected:
        ::xsd::cxx::tree::one< BaseType > base_;
        ::xsd::cxx::tree::one< SystemTypeType > systemType_;
        ::xsd::cxx::tree::one< CsysType > csys_;
        ::xsd::cxx::tree::one< AutoSizeType > autoSize_;
        ::xsd::cxx::tree::one< SizeType > size_;
        LinkedPicksOptional linkedPicks_;
        PointsPicksOptional pointsPicks_;
        ShapeInferPicksOptional shapeInferPicks_;
        OriginPickOptional originPick_;
        Axis0PicksOptional axis0Picks_;
        Axis1PicksOptional axis1Picks_;
        AxisStyleOptional axisStyle_;
        PostOpsSequence postOps_;
        ::xsd::cxx::tree::one< CsysDraggerType > csysDragger_;
        ::xsd::cxx::tree::one< AutoSizeLabelType > autoSizeLabel_;
        ::xsd::cxx::tree::one< SizeLabelType > sizeLabel_;
        ::xsd::cxx::tree::one< CachedSizeType > cachedSize_;
      };
    }
  }
}

#include <iosfwd>

#include <xercesc/sax/InputSource.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>

namespace prj
{
  namespace srl
  {
    namespace dtms
    {
      // Parse a URI or a local file.
      //

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (const ::std::string& uri,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (const ::std::string& uri,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (const ::std::string& uri,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse std::istream.
      //

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::std::istream& is,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::std::istream& is,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::std::istream& is,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::std::istream& is,
                   const ::std::string& id,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::std::istream& is,
                   const ::std::string& id,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::std::istream& is,
                   const ::std::string& id,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse xercesc::InputSource.
      //

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::xercesc::InputSource& is,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::xercesc::InputSource& is,
                   ::xml_schema::ErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::xercesc::InputSource& is,
                   ::xercesc::DOMErrorHandler& eh,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse xercesc::DOMDocument.
      //

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (const ::xercesc::DOMDocument& d,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::dtms::DatumSystem >
      datumsystem (::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d,
                   ::xml_schema::Flags f = 0,
                   const ::xml_schema::Properties& p = ::xml_schema::Properties ());
    }
  }
}

#include <iosfwd>

#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/framework/XMLFormatter.hpp>

#include <xsd/cxx/xml/dom/auto-ptr.hxx>

namespace prj
{
  namespace srl
  {
    namespace dtms
    {
      void
      operator<< (::xercesc::DOMElement&, const DatumSystem&);

      // Serialize to std::ostream.
      //

      void
      datumsystem (::std::ostream& os,
                   const ::prj::srl::dtms::DatumSystem& x, 
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      datumsystem (::std::ostream& os,
                   const ::prj::srl::dtms::DatumSystem& x, 
                   ::xml_schema::ErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      datumsystem (::std::ostream& os,
                   const ::prj::srl::dtms::DatumSystem& x, 
                   ::xercesc::DOMErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      // Serialize to xercesc::XMLFormatTarget.
      //

      void
      datumsystem (::xercesc::XMLFormatTarget& ft,
                   const ::prj::srl::dtms::DatumSystem& x, 
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      datumsystem (::xercesc::XMLFormatTarget& ft,
                   const ::prj::srl::dtms::DatumSystem& x, 
                   ::xml_schema::ErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      void
      datumsystem (::xercesc::XMLFormatTarget& ft,
                   const ::prj::srl::dtms::DatumSystem& x, 
                   ::xercesc::DOMErrorHandler& eh,
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   const ::std::string& e = "UTF-8",
                   ::xml_schema::Flags f = 0);

      // Serialize to an existing xercesc::DOMDocument.
      //

      void
      datumsystem (::xercesc::DOMDocument& d,
                   const ::prj::srl::dtms::DatumSystem& x,
                   ::xml_schema::Flags f = 0);

      // Serialize to a new xercesc::DOMDocument.
      //

      ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument >
      datumsystem (const ::prj::srl::dtms::DatumSystem& x, 
                   const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
                   ::xml_schema::Flags f = 0);
    }
  }
}

#include <xsd/cxx/post.hxx>

// Begin epilogue.
//
//
// End epilogue.

#endif // PRJ_SRL_DTMS_PRJSRLDTMSDATUMSYSTEM_H

// Copyright (c) 2005-2014 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD, an XML Schema to
// C++ data binding compiler.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// In addition, as a special exception, Code Synthesis Tools CC gives
// permission to link this program with the Xerces-C++ library (or with
// modified versions of Xerces-C++ that use the same license as Xerces-C++),
// and distribute linked combinations including the two. You must obey
// the GNU General Public License version 2 in all respects for all of
// the code used other than Xerces-C++. If you modify this copy of the
// program, you may extend this exception to your version of the program,
// but you are not obligated to do so. If you do not wish to do so, delete
// this exception statement from your version.
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

#include <xsd/cxx/pre.hxx>

#include "prjsrlextsextract.h"

namespace prj
{
  namespace srl
  {
    namespace exts
    {
      // Extract
      // 

      const Extract::BaseType& Extract::
      base () const
      {
        return this->base_.get ();
      }

      Extract::BaseType& Extract::
      base ()
      {
        return this->base_.get ();
      }

      void Extract::
      base (const BaseType& x)
      {
        this->base_.set (x);
      }

      void Extract::
      base (::std::unique_ptr< BaseType > x)
      {
        this->base_.set (std::move (x));
      }

      const Extract::PicksType& Extract::
      picks () const
      {
        return this->picks_.get ();
      }

      Extract::PicksType& Extract::
      picks ()
      {
        return this->picks_.get ();
      }

      void Extract::
      picks (const PicksType& x)
      {
        this->picks_.set (x);
      }

      void Extract::
      picks (::std::unique_ptr< PicksType > x)
      {
        this->picks_.set (std::move (x));
      }

      const Extract::AngleType& Extract::
      angle () const
      {
        return this->angle_.get ();
      }

      Extract::AngleType& Extract::
      angle ()
      {
        return this->angle_.get ();
      }

      void Extract::
      angle (const AngleType& x)
      {
        this->angle_.set (x);
      }

      void Extract::
      angle (::std::unique_ptr< AngleType > x)
      {
        this->angle_.set (std::move (x));
      }

      const Extract::FilterType& Extract::
      filter () const
      {
        return this->filter_.get ();
      }

      Extract::FilterType& Extract::
      filter ()
      {
        return this->filter_.get ();
      }

      void Extract::
      filter (const FilterType& x)
      {
        this->filter_.set (x);
      }

      void Extract::
      filter (::std::unique_ptr< FilterType > x)
      {
        this->filter_.set (std::move (x));
      }

      const Extract::SewType& Extract::
      sew () const
      {
        return this->sew_.get ();
      }

      Extract::SewType& Extract::
      sew ()
      {
        return this->sew_.get ();
      }

      void Extract::
      sew (const SewType& x)
      {
        this->sew_.set (x);
      }

      void Extract::
      sew (::std::unique_ptr< SewType > x)
      {
        this->sew_.set (std::move (x));
      }

      const Extract::CheckShapeOptional& Extract::
      checkShape () const
      {
        return this->checkShape_;
      }

      Extract::CheckShapeOptional& Extract::
      checkShape ()
      {
        return this->checkShape_;
      }

      void Extract::
      checkShape (const CheckShapeType& x)
      {
        this->checkShape_.set (x);
      }

      void Extract::
      checkShape (const CheckShapeOptional& x)
      {
        this->checkShape_ = x;
      }

      void Extract::
      checkShape (::std::unique_ptr< CheckShapeType > x)
      {
        this->checkShape_.set (std::move (x));
      }

      const Extract::SeerShapeType& Extract::
      seerShape () const
      {
        return this->seerShape_.get ();
      }

      Extract::SeerShapeType& Extract::
      seerShape ()
      {
        return this->seerShape_.get ();
      }

      void Extract::
      seerShape (const SeerShapeType& x)
      {
        this->seerShape_.set (x);
      }

      void Extract::
      seerShape (::std::unique_ptr< SeerShapeType > x)
      {
        this->seerShape_.set (std::move (x));
      }

      const Extract::AngleLabelType& Extract::
      angleLabel () const
      {
        return this->angleLabel_.get ();
      }

      Extract::AngleLabelType& Extract::
      angleLabel ()
      {
        return this->angleLabel_.get ();
      }

      void Extract::
      angleLabel (const AngleLabelType& x)
      {
        this->angleLabel_.set (x);
      }

      void Extract::
      angleLabel (::std::unique_ptr< AngleLabelType > x)
      {
        this->angleLabel_.set (std::move (x));
      }

      const Extract::FilterLabelType& Extract::
      filterLabel () const
      {
        return this->filterLabel_.get ();
      }

      Extract::FilterLabelType& Extract::
      filterLabel ()
      {
        return this->filterLabel_.get ();
      }

      void Extract::
      filterLabel (const FilterLabelType& x)
      {
        this->filterLabel_.set (x);
      }

      void Extract::
      filterLabel (::std::unique_ptr< FilterLabelType > x)
      {
        this->filterLabel_.set (std::move (x));
      }

      const Extract::SewLabelType& Extract::
      sewLabel () const
      {
        return this->sewLabel_.get ();
      }

      Extract::SewLabelType& Extract::
      sewLabel ()
      {
        return this->sewLabel_.get ();
      }

      void Extract::
      sewLabel (const SewLabelType& x)
      {
        this->sewLabel_.set (x);
      }

      void Extract::
      sewLabel (::std::unique_ptr< SewLabelType > x)
      {
        this->sewLabel_.set (std::move (x));
      }

      const Extract::CheckShapeLabelOptional& Extract::
      checkShapeLabel () const
      {
        return this->checkShapeLabel_;
      }

      Extract::CheckShapeLabelOptional& Extract::
      checkShapeLabel ()
      {
        return this->checkShapeLabel_;
      }

      void Extract::
      checkShapeLabel (const CheckShapeLabelType& x)
      {
        this->checkShapeLabel_.set (x);
      }

      void Extract::
      checkShapeLabel (const CheckShapeLabelOptional& x)
      {
        this->checkShapeLabel_ = x;
      }

      void Extract::
      checkShapeLabel (::std::unique_ptr< CheckShapeLabelType > x)
      {
        this->checkShapeLabel_.set (std::move (x));
      }

      const Extract::GridLocationType& Extract::
      gridLocation () const
      {
        return this->gridLocation_.get ();
      }

      Extract::GridLocationType& Extract::
      gridLocation ()
      {
        return this->gridLocation_.get ();
      }

      void Extract::
      gridLocation (const GridLocationType& x)
      {
        this->gridLocation_.set (x);
      }

      void Extract::
      gridLocation (::std::unique_ptr< GridLocationType > x)
      {
        this->gridLocation_.set (std::move (x));
      }
    }
  }
}

#include <xsd/cxx/xml/dom/parsing-source.hxx>

namespace prj
{
  namespace srl
  {
    namespace exts
    {
      // Extract
      //

      Extract::
      Extract (const BaseType& base,
               const PicksType& picks,
               const AngleType& angle,
               const FilterType& filter,
               const SewType& sew,
               const SeerShapeType& seerShape,
               const AngleLabelType& angleLabel,
               const FilterLabelType& filterLabel,
               const SewLabelType& sewLabel,
               const GridLocationType& gridLocation)
      : ::xml_schema::Type (),
        base_ (base, this),
        picks_ (picks, this),
        angle_ (angle, this),
        filter_ (filter, this),
        sew_ (sew, this),
        checkShape_ (this),
        seerShape_ (seerShape, this),
        angleLabel_ (angleLabel, this),
        filterLabel_ (filterLabel, this),
        sewLabel_ (sewLabel, this),
        checkShapeLabel_ (this),
        gridLocation_ (gridLocation, this)
      {
      }

      Extract::
      Extract (::std::unique_ptr< BaseType > base,
               ::std::unique_ptr< PicksType > picks,
               ::std::unique_ptr< AngleType > angle,
               ::std::unique_ptr< FilterType > filter,
               ::std::unique_ptr< SewType > sew,
               ::std::unique_ptr< SeerShapeType > seerShape,
               ::std::unique_ptr< AngleLabelType > angleLabel,
               ::std::unique_ptr< FilterLabelType > filterLabel,
               ::std::unique_ptr< SewLabelType > sewLabel,
               ::std::unique_ptr< GridLocationType > gridLocation)
      : ::xml_schema::Type (),
        base_ (std::move (base), this),
        picks_ (std::move (picks), this),
        angle_ (std::move (angle), this),
        filter_ (std::move (filter), this),
        sew_ (std::move (sew), this),
        checkShape_ (this),
        seerShape_ (std::move (seerShape), this),
        angleLabel_ (std::move (angleLabel), this),
        filterLabel_ (std::move (filterLabel), this),
        sewLabel_ (std::move (sewLabel), this),
        checkShapeLabel_ (this),
        gridLocation_ (std::move (gridLocation), this)
      {
      }

      Extract::
      Extract (const Extract& x,
               ::xml_schema::Flags f,
               ::xml_schema::Container* c)
      : ::xml_schema::Type (x, f, c),
        base_ (x.base_, f, this),
        picks_ (x.picks_, f, this),
        angle_ (x.angle_, f, this),
        filter_ (x.filter_, f, this),
        sew_ (x.sew_, f, this),
        checkShape_ (x.checkShape_, f, this),
        seerShape_ (x.seerShape_, f, this),
        angleLabel_ (x.angleLabel_, f, this),
        filterLabel_ (x.filterLabel_, f, this),
        sewLabel_ (x.sewLabel_, f, this),
        checkShapeLabel_ (x.checkShapeLabel_, f, this),
        gridLocation_ (x.gridLocation_, f, this)
      {
      }

      Extract::
      Extract (const ::xercesc::DOMElement& e,
               ::xml_schema::Flags f,
               ::xml_schema::Container* c)
      : ::xml_schema::Type (e, f | ::xml_schema::Flags::base, c),
        base_ (this),
        picks_ (this),
        angle_ (this),
        filter_ (this),
        sew_ (this),
        checkShape_ (this),
        seerShape_ (this),
        angleLabel_ (this),
        filterLabel_ (this),
        sewLabel_ (this),
        checkShapeLabel_ (this),
        gridLocation_ (this)
      {
        if ((f & ::xml_schema::Flags::base) == 0)
        {
          ::xsd::cxx::xml::dom::parser< char > p (e, true, false, false);
          this->parse (p, f);
        }
      }

      void Extract::
      parse (::xsd::cxx::xml::dom::parser< char >& p,
             ::xml_schema::Flags f)
      {
        for (; p.more_content (); p.next_content (false))
        {
          const ::xercesc::DOMElement& i (p.cur_element ());
          const ::xsd::cxx::xml::qualified_name< char > n (
            ::xsd::cxx::xml::dom::name< char > (i));

          // base
          //
          if (n.name () == "base" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< BaseType > r (
              BaseTraits::create (i, f, this));

            if (!base_.present ())
            {
              this->base_.set (::std::move (r));
              continue;
            }
          }

          // picks
          //
          if (n.name () == "picks" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< PicksType > r (
              PicksTraits::create (i, f, this));

            if (!picks_.present ())
            {
              this->picks_.set (::std::move (r));
              continue;
            }
          }

          // angle
          //
          if (n.name () == "angle" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< AngleType > r (
              AngleTraits::create (i, f, this));

            if (!angle_.present ())
            {
              this->angle_.set (::std::move (r));
              continue;
            }
          }

          // filter
          //
          if (n.name () == "filter" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< FilterType > r (
              FilterTraits::create (i, f, this));

            if (!filter_.present ())
            {
              this->filter_.set (::std::move (r));
              continue;
            }
          }

          // sew
          //
          if (n.name () == "sew" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< SewType > r (
              SewTraits::create (i, f, this));

            if (!sew_.present ())
            {
              this->sew_.set (::std::move (r));
              continue;
            }
          }

          // checkShape
          //
          if (n.name () == "checkShape" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< CheckShapeType > r (
              CheckShapeTraits::create (i, f, this));

            if (!this->checkShape_)
            {
              this->checkShape_.set (::std::move (r));
              continue;
            }
          }

          // seerShape
          //
          if (n.name () == "seerShape" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< SeerShapeType > r (
              SeerShapeTraits::create (i, f, this));

            if (!seerShape_.present ())
            {
              this->seerShape_.set (::std::move (r));
              continue;
            }
          }

          // angleLabel
          //
          if (n.name () == "angleLabel" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< AngleLabelType > r (
              AngleLabelTraits::create (i, f, this));

            if (!angleLabel_.present ())
            {
              this->angleLabel_.set (::std::move (r));
              continue;
            }
          }

          // filterLabel
          //
          if (n.name () == "filterLabel" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< FilterLabelType > r (
              FilterLabelTraits::create (i, f, this));

            if (!filterLabel_.present ())
            {
              this->filterLabel_.set (::std::move (r));
              continue;
            }
          }

          // sewLabel
          //
          if (n.name () == "sewLabel" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< SewLabelType > r (
              SewLabelTraits::create (i, f, this));

            if (!sewLabel_.present ())
            {
              this->sewLabel_.set (::std::move (r));
              continue;
            }
          }

          // checkShapeLabel
          //
          if (n.name () == "checkShapeLabel" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< CheckShapeLabelType > r (
              CheckShapeLabelTraits::create (i, f, this));

            if (!this->checkShapeLabel_)
            {
              this->checkShapeLabel_.set (::std::move (r));
              continue;
            }
          }

          // gridLocation
          //
          if (n.name () == "gridLocation" && n.namespace_ ().empty ())
          {
            ::std::unique_ptr< GridLocationType > r (
              GridLocationTraits::create (i, f, this));

            if (!gridLocation_.present ())
            {
              this->gridLocation_.set (::std::move (r));
              continue;
            }
          }

          break;
        }

        if (!base_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "base",
            "");
        }

        if (!picks_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "picks",
            "");
        }

        if (!angle_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "angle",
            "");
        }

        if (!filter_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "filter",
            "");
        }

        if (!sew_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "sew",
            "");
        }

        if (!seerShape_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "seerShape",
            "");
        }

        if (!angleLabel_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "angleLabel",
            "");
        }

        if (!filterLabel_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "filterLabel",
            "");
        }

        if (!sewLabel_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "sewLabel",
            "");
        }

        if (!gridLocation_.present ())
        {
          throw ::xsd::cxx::tree::expected_element< char > (
            "gridLocation",
            "");
        }
      }

      Extract* Extract::
      _clone (::xml_schema::Flags f,
              ::xml_schema::Container* c) const
      {
        return new class Extract (*this, f, c);
      }

      Extract& Extract::
      operator= (const Extract& x)
      {
        if (this != &x)
        {
          static_cast< ::xml_schema::Type& > (*this) = x;
          this->base_ = x.base_;
          this->picks_ = x.picks_;
          this->angle_ = x.angle_;
          this->filter_ = x.filter_;
          this->sew_ = x.sew_;
          this->checkShape_ = x.checkShape_;
          this->seerShape_ = x.seerShape_;
          this->angleLabel_ = x.angleLabel_;
          this->filterLabel_ = x.filterLabel_;
          this->sewLabel_ = x.sewLabel_;
          this->checkShapeLabel_ = x.checkShapeLabel_;
          this->gridLocation_ = x.gridLocation_;
        }

        return *this;
      }

      Extract::
      ~Extract ()
      {
      }
    }
  }
}

#include <istream>
#include <xsd/cxx/xml/sax/std-input-source.hxx>
#include <xsd/cxx/tree/error-handler.hxx>

namespace prj
{
  namespace srl
  {
    namespace exts
    {
      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (const ::std::string& u,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0,
          (f & ::xml_schema::Flags::keep_dom) == 0);

        ::xsd::cxx::tree::error_handler< char > h;

        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::xsd::cxx::xml::dom::parse< char > (
            u, h, p, f));

        h.throw_if_failed< ::xsd::cxx::tree::parsing< char > > ();

        return ::std::unique_ptr< ::prj::srl::exts::Extract > (
          ::prj::srl::exts::extract (
            std::move (d), f | ::xml_schema::Flags::own_dom, p));
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (const ::std::string& u,
               ::xml_schema::ErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0,
          (f & ::xml_schema::Flags::keep_dom) == 0);

        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::xsd::cxx::xml::dom::parse< char > (
            u, h, p, f));

        if (!d.get ())
          throw ::xsd::cxx::tree::parsing< char > ();

        return ::std::unique_ptr< ::prj::srl::exts::Extract > (
          ::prj::srl::exts::extract (
            std::move (d), f | ::xml_schema::Flags::own_dom, p));
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (const ::std::string& u,
               ::xercesc::DOMErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::xsd::cxx::xml::dom::parse< char > (
            u, h, p, f));

        if (!d.get ())
          throw ::xsd::cxx::tree::parsing< char > ();

        return ::std::unique_ptr< ::prj::srl::exts::Extract > (
          ::prj::srl::exts::extract (
            std::move (d), f | ::xml_schema::Flags::own_dom, p));
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::std::istream& is,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0,
          (f & ::xml_schema::Flags::keep_dom) == 0);

        ::xsd::cxx::xml::sax::std_input_source isrc (is);
        return ::prj::srl::exts::extract (isrc, f, p);
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::std::istream& is,
               ::xml_schema::ErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0,
          (f & ::xml_schema::Flags::keep_dom) == 0);

        ::xsd::cxx::xml::sax::std_input_source isrc (is);
        return ::prj::srl::exts::extract (isrc, h, f, p);
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::std::istream& is,
               ::xercesc::DOMErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::sax::std_input_source isrc (is);
        return ::prj::srl::exts::extract (isrc, h, f, p);
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::std::istream& is,
               const ::std::string& sid,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0,
          (f & ::xml_schema::Flags::keep_dom) == 0);

        ::xsd::cxx::xml::sax::std_input_source isrc (is, sid);
        return ::prj::srl::exts::extract (isrc, f, p);
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::std::istream& is,
               const ::std::string& sid,
               ::xml_schema::ErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0,
          (f & ::xml_schema::Flags::keep_dom) == 0);

        ::xsd::cxx::xml::sax::std_input_source isrc (is, sid);
        return ::prj::srl::exts::extract (isrc, h, f, p);
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::std::istream& is,
               const ::std::string& sid,
               ::xercesc::DOMErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::xml::sax::std_input_source isrc (is, sid);
        return ::prj::srl::exts::extract (isrc, h, f, p);
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::xercesc::InputSource& i,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xsd::cxx::tree::error_handler< char > h;

        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::xsd::cxx::xml::dom::parse< char > (
            i, h, p, f));

        h.throw_if_failed< ::xsd::cxx::tree::parsing< char > > ();

        return ::std::unique_ptr< ::prj::srl::exts::Extract > (
          ::prj::srl::exts::extract (
            std::move (d), f | ::xml_schema::Flags::own_dom, p));
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::xercesc::InputSource& i,
               ::xml_schema::ErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::xsd::cxx::xml::dom::parse< char > (
            i, h, p, f));

        if (!d.get ())
          throw ::xsd::cxx::tree::parsing< char > ();

        return ::std::unique_ptr< ::prj::srl::exts::Extract > (
          ::prj::srl::exts::extract (
            std::move (d), f | ::xml_schema::Flags::own_dom, p));
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::xercesc::InputSource& i,
               ::xercesc::DOMErrorHandler& h,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::xsd::cxx::xml::dom::parse< char > (
            i, h, p, f));

        if (!d.get ())
          throw ::xsd::cxx::tree::parsing< char > ();

        return ::std::unique_ptr< ::prj::srl::exts::Extract > (
          ::prj::srl::exts::extract (
            std::move (d), f | ::xml_schema::Flags::own_dom, p));
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (const ::xercesc::DOMDocument& doc,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties& p)
      {
        if (f & ::xml_schema::Flags::keep_dom)
        {
          ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
            static_cast< ::xercesc::DOMDocument* > (doc.cloneNode (true)));

          return ::std::unique_ptr< ::prj::srl::exts::Extract > (
            ::prj::srl::exts::extract (
              std::move (d), f | ::xml_schema::Flags::own_dom, p));
        }

        const ::xercesc::DOMElement& e (*doc.getDocumentElement ());
        const ::xsd::cxx::xml::qualified_name< char > n (
          ::xsd::cxx::xml::dom::name< char > (e));

        if (n.name () == "extract" &&
            n.namespace_ () == "http://www.cadseer.com/prj/srl/exts")
        {
          ::std::unique_ptr< ::prj::srl::exts::Extract > r (
            ::xsd::cxx::tree::traits< ::prj::srl::exts::Extract, char >::create (
              e, f, 0));
          return r;
        }

        throw ::xsd::cxx::tree::unexpected_element < char > (
          n.name (),
          n.namespace_ (),
          "extract",
          "http://www.cadseer.com/prj/srl/exts");
      }

      ::std::unique_ptr< ::prj::srl::exts::Extract >
      extract (::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d,
               ::xml_schema::Flags f,
               const ::xml_schema::Properties&)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > c (
          ((f & ::xml_schema::Flags::keep_dom) &&
           !(f & ::xml_schema::Flags::own_dom))
          ? static_cast< ::xercesc::DOMDocument* > (d->cloneNode (true))
          : 0);

        ::xercesc::DOMDocument& doc (c.get () ? *c : *d);
        const ::xercesc::DOMElement& e (*doc.getDocumentElement ());

        const ::xsd::cxx::xml::qualified_name< char > n (
          ::xsd::cxx::xml::dom::name< char > (e));

        if (f & ::xml_schema::Flags::keep_dom)
          doc.setUserData (::xml_schema::dom::tree_node_key,
                           (c.get () ? &c : &d),
                           0);

        if (n.name () == "extract" &&
            n.namespace_ () == "http://www.cadseer.com/prj/srl/exts")
        {
          ::std::unique_ptr< ::prj::srl::exts::Extract > r (
            ::xsd::cxx::tree::traits< ::prj::srl::exts::Extract, char >::create (
              e, f, 0));
          return r;
        }

        throw ::xsd::cxx::tree::unexpected_element < char > (
          n.name (),
          n.namespace_ (),
          "extract",
          "http://www.cadseer.com/prj/srl/exts");
      }
    }
  }
}

#include <ostream>
#include <xsd/cxx/tree/error-handler.hxx>
#include <xsd/cxx/xml/dom/serialization-source.hxx>

namespace prj
{
  namespace srl
  {
    namespace exts
    {
      void
      operator<< (::xercesc::DOMElement& e, const Extract& i)
      {
        e << static_cast< const ::xml_schema::Type& > (i);

        // base
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "base",
              e));

          s << i.base ();
        }

        // picks
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "picks",
              e));

          s << i.picks ();
        }

        // angle
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "angle",
              e));

          s << i.angle ();
        }

        // filter
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "filter",
              e));

          s << i.filter ();
        }

        // sew
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "sew",
              e));

          s << i.sew ();
        }

        // checkShape
        //
        if (i.checkShape ())
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "checkShape",
              e));

          s << *i.checkShape ();
        }

        // seerShape
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "seerShape",
              e));

          s << i.seerShape ();
        }

        // angleLabel
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "angleLabel",
              e));

          s << i.angleLabel ();
        }

        // filterLabel
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "filterLabel",
              e));

          s << i.filterLabel ();
        }

        // sewLabel
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "sewLabel",
              e));

          s << i.sewLabel ();
        }

        // checkShapeLabel
        //
        if (i.checkShapeLabel ())
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "checkShapeLabel",
              e));

          s << *i.checkShapeLabel ();
        }

        // gridLocation
        //
        {
          ::xercesc::DOMElement& s (
            ::xsd::cxx::xml::dom::create_element (
              "gridLocation",
              e));

          s << i.gridLocation ();
        }
      }

      void
      extract (::std::ostream& o,
               const ::prj::srl::exts::Extract& s,
               const ::xml_schema::NamespaceInfomap& m,
               const ::std::string& e,
               ::xml_schema::Flags f)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0);

        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::prj::srl::exts::extract (s, m, f));

        ::xsd::cxx::tree::error_handler< char > h;

        ::xsd::cxx::xml::dom::ostream_format_target t (o);
        if (!::xsd::cxx::xml::dom::serialize (t, *d, e, h, f))
        {
          h.throw_if_failed< ::xsd::cxx::tree::serialization< char > > ();
        }
      }

      void
      extract (::std::ostream& o,
               const ::prj::srl::exts::Extract& s,
               ::xml_schema::ErrorHandler& h,
               const ::xml_schema::NamespaceInfomap& m,
               const ::std::string& e,
               ::xml_schema::Flags f)
      {
        ::xsd::cxx::xml::auto_initializer i (
          (f & ::xml_schema::Flags::dont_initialize) == 0);

        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::prj::srl::exts::extract (s, m, f));
        ::xsd::cxx::xml::dom::ostream_format_target t (o);
        if (!::xsd::cxx::xml::dom::serialize (t, *d, e, h, f))
        {
          throw ::xsd::cxx::tree::serialization< char > ();
        }
      }

      void
      extract (::std::ostream& o,
               const ::prj::srl::exts::Extract& s,
               ::xercesc::DOMErrorHandler& h,
               const ::xml_schema::NamespaceInfomap& m,
               const ::std::string& e,
               ::xml_schema::Flags f)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::prj::srl::exts::extract (s, m, f));
        ::xsd::cxx::xml::dom::ostream_format_target t (o);
        if (!::xsd::cxx::xml::dom::serialize (t, *d, e, h, f))
        {
          throw ::xsd::cxx::tree::serialization< char > ();
        }
      }

      void
      extract (::xercesc::XMLFormatTarget& t,
               const ::prj::srl::exts::Extract& s,
               const ::xml_schema::NamespaceInfomap& m,
               const ::std::string& e,
               ::xml_schema::Flags f)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::prj::srl::exts::extract (s, m, f));

        ::xsd::cxx::tree::error_handler< char > h;

        if (!::xsd::cxx::xml::dom::serialize (t, *d, e, h, f))
        {
          h.throw_if_failed< ::xsd::cxx::tree::serialization< char > > ();
        }
      }

      void
      extract (::xercesc::XMLFormatTarget& t,
               const ::prj::srl::exts::Extract& s,
               ::xml_schema::ErrorHandler& h,
               const ::xml_schema::NamespaceInfomap& m,
               const ::std::string& e,
               ::xml_schema::Flags f)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::prj::srl::exts::extract (s, m, f));
        if (!::xsd::cxx::xml::dom::serialize (t, *d, e, h, f))
        {
          throw ::xsd::cxx::tree::serialization< char > ();
        }
      }

      void
      extract (::xercesc::XMLFormatTarget& t,
               const ::prj::srl::exts::Extract& s,
               ::xercesc::DOMErrorHandler& h,
               const ::xml_schema::NamespaceInfomap& m,
               const ::std::string& e,
               ::xml_schema::Flags f)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::prj::srl::exts::extract (s, m, f));
        if (!::xsd::cxx::xml::dom::serialize (t, *d, e, h, f))
        {
          throw ::xsd::cxx::tree::serialization< char > ();
        }
      }

      void
      extract (::xercesc::DOMDocument& d,
               const ::prj::srl::exts::Extract& s,
               ::xml_schema::Flags)
      {
        ::xercesc::DOMElement& e (*d.getDocumentElement ());
        const ::xsd::cxx::xml::qualified_name< char > n (
          ::xsd::cxx::xml::dom::name< char > (e));

        if (n.name () == "extract" &&
            n.namespace_ () == "http://www.cadseer.com/prj/srl/exts")
        {
          e << s;
        }
        else
        {
          throw ::xsd::cxx::tree::unexpected_element < char > (
            n.name (),
            n.namespace_ (),
            "extract",
            "http://www.cadseer.com/prj/srl/exts");
        }
      }

      ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument >
      extract (const ::prj::srl::exts::Extract& s,
               const ::xml_schema::NamespaceInfomap& m,
               ::xml_schema::Flags f)
      {
        ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d (
          ::xsd::cxx::xml::dom::serialize< char > (
            "extract",
            "http://www.cadseer.com/prj/srl/exts",
            m, f));

        ::prj::srl::exts::extract (*d, s, f);
        return d;
      }
    }
  }
}

#include <xsd/cxx/post.hxx>

// Begin epilogue.
//
//
// End epilogue.


// Copyright (c) 2005-2014 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD, an XML Schema to
// C++ data binding compiler.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// In addition, as a special exception, Code Synthesis Tools CC gives
// permission to link this program with the Xerces-C++ library (or with
// modified versions of Xerces-C++ that use the same license as Xerces-C++),
// and distribute linked combinations including the two. You must obey
// the GNU General Public License version 2 in all respects for all of
// the code used other than Xerces-C++. If you modify this copy of the
// program, you may extend this exception to your version of the program,
// but you are not obligated to do so. If you do not wish to do so, delete
// this exception statement from your version.
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef PRJ_SRL_GRDS_PRJSRLGRDSGORDON_H
#define PRJ_SRL_GRDS_PRJSRLGRDSGORDON_H

#ifndef XSD_CXX11
#define XSD_CXX11
#endif

#ifndef XSD_USE_CHAR
#define XSD_USE_CHAR
#endif

#ifndef XSD_CXX_TREE_USE_CHAR
#define XSD_CXX_TREE_USE_CHAR
#endif

// Begin prologue.
//
//
// End prologue.

#include <xsd/cxx/config.hxx>

#if (XSD_INT_VERSION != 4000000L)
#error XSD runtime version mismatch
#endif

#include <xsd/cxx/pre.hxx>

#include "../../../xmlbase.h"

// Forward declarations.
//
namespace prj
{
  namespace srl
  {
    namespace grds
    {
      class Gordon;
    }
  }
}


#include <memory>    // ::std::unique_ptr
#include <limits>    // std::numeric_limits
#include <algorithm> // std::binary_search
#include <utility>   // std::move

#include <xsd/cxx/xml/char-utf8.hxx>

#include <xsd/cxx/tree/exceptions.hxx>
#include <xsd/cxx/tree/elements.hxx>
#include <xsd/cxx/tree/containers.hxx>
#include <xsd/cxx/tree/list.hxx>

#include <xsd/cxx/xml/dom/parsing-header.hxx>

#include "prjsrlsptparameter.h"

#include "prjsrlsptoverlay.h"

#include "prjsrlsptseershape.h"

#include "prjsrlsptbase.h"

namespace prj
{
  namespace srl
  {
    namespace grds
    {
      class Gordon: public ::xml_schema::Type
      {
        public:
        // base
        //
        typedef ::prj::srl::spt::Base BaseType;
        typedef ::xsd::cxx::tree::traits< BaseType, char > BaseTraits;

        const BaseType&
        base () const;

        BaseType&
        base ();

        void
        base (const BaseType& x);

        void
        base (::std::unique_ptr< BaseType > p);

        // seerShape
        //
        typedef ::prj::srl::spt::SeerShape SeerShapeType;
        typedef ::xsd::cxx::tree::traits< SeerShapeType, char > SeerShapeTraits;

        const SeerShapeType&
        seerShape () const;

        SeerShapeType&
        seerShape ();

        void
        seerShape (const SeerShapeType& x);

        void
        seerShape (::std::unique_ptr< SeerShapeType > p);

        // uPicks
        //
        typedef ::prj::srl::spt::Parameter UPicksType;
        typedef ::xsd::cxx::tree::traits< UPicksType, char > UPicksTraits;

        const UPicksType&
        uPicks () const;

        UPicksType&
        uPicks ();

        void
        uPicks (const UPicksType& x);

        void
        uPicks (::std::unique_ptr< UPicksType > p);

        // vPicks
        //
        typedef ::prj::srl::spt::Parameter VPicksType;
        typedef ::xsd::cxx::tree::traits< VPicksType, char > VPicksTraits;

        const VPicksType&
        vPicks () const;

        VPicksType&
        vPicks ();

        void
        vPicks (const VPicksType& x);

        void
        vPicks (::std::unique_ptr< VPicksType > p);

        // tolerance
        //
        typedef ::prj::srl::spt::Parameter ToleranceType;
        typedef ::xsd::cxx::tree::traits< ToleranceType, char > ToleranceTraits;

        const ToleranceType&
        tolerance () const;

        ToleranceType&
        tolerance ();

        void
        tolerance (const ToleranceType& x);

        void
        tolerance (::std::unique_ptr< ToleranceType > p);

        // gridLocation
        //
        typedef ::prj::srl::spt::Vec3d GridLocationType;
        typedef ::xsd::cxx::tree::traits< GridLocationType, char > GridLocationTraits;

        const GridLocationType&
        gridLocation () const;

        GridLocationType&
        gridLocation ();

        void
        gridLocation (const GridLocationType& x);

        void
        gridLocation (::std::unique_ptr< GridLocationType > p);

        // toleranceLabel
        //
        typedef ::prj::srl::spt::PLabel ToleranceLabelType;
        typedef ::xsd::cxx::tree::traits< ToleranceLabelType, char > ToleranceLabelTraits;

        const ToleranceLabelType&
        toleranceLabel () const;

        ToleranceLabelType&
        toleranceLabel ();

        void
        toleranceLabel (const ToleranceLabelType& x);

        void
        toleranceLabel (::std::unique_ptr< ToleranceLabelType > p);

        // faceId
        //
        typedef ::xml_schema::String FaceIdType;
        typedef ::xsd::cxx::tree::traits< FaceIdType, char > FaceIdTraits;

        const FaceIdType&
        faceId () const;

        FaceIdType&
        faceId ();

        void
        faceId (const FaceIdType& x);

        void
        faceId (::std::unique_ptr< FaceIdType > p);

        // wireId
        //
        typedef ::xml_schema::String WireIdType;
        typedef ::xsd::cxx::tree::traits< WireIdType, char > WireIdTraits;

        const WireIdType&
        wireId () const;

        WireIdType&
        wireId ();

        void
        wireId (const WireIdType& x);

        void
        wireId (::std::unique_ptr< WireIdType > p);

        // edgeIds
        //
        typedef ::xml_schema::String EdgeIdsType;
        typedef ::xsd::cxx::tree::sequence< EdgeIdsType > EdgeIdsSequence;
        typedef EdgeIdsSequence::iterator EdgeIdsIterator;
        typedef EdgeIdsSequence::const_iterator EdgeIdsConstIterator;
        typedef ::xsd::cxx::tree::traits< EdgeIdsType, char > EdgeIdsTraits;

        const EdgeIdsSequence&
        edgeIds () const;

        EdgeIdsSequence&
        edgeIds ();

        void
        edgeIds (const EdgeIdsSequence& s);

        // vertexIds
        //
        typedef ::xml_schema::String VertexIdsType;
        typedef ::xsd::cxx::tree::sequence< VertexIdsType > VertexIdsSequence;
        typedef VertexIdsSequence::iterator VertexIdsIterator;
        typedef VertexIdsSequence::const_iterator VertexIdsConstIterator;
        typedef ::xsd::cxx::tree::traits< VertexIdsType, char > VertexIdsTraits;

        const VertexIdsSequence&
        vertexIds () const;

        VertexIdsSequence&
        vertexIds ();

        void
        vertexIds (const VertexIdsSequence& s);

        // Constructors.
        //
        Gordon (const BaseType&,
                const SeerShapeType&,
                const UPicksType&,
                const VPicksType&,
                const ToleranceType&,
                const GridLocationType&,
                const ToleranceLabelType&,
                const FaceIdType&,
                const WireIdType&);

        Gordon (::std::unique_ptr< BaseType >,
                ::std::unique_ptr< SeerShapeType >,
                ::std::unique_ptr< UPicksType >,
                ::std::unique_ptr< VPicksType >,
                ::std::unique_ptr< ToleranceType >,
                ::std::unique_ptr< GridLocationType >,
                ::std::unique_ptr< ToleranceLabelType >,
                const FaceIdType&,
                const WireIdType&);

        Gordon (const ::xercesc::DOMElement& e,
                ::xml_schema::Flags f = 0,
                ::xml_schema::Container* c = 0);

        Gordon (const Gordon& x,
                ::xml_schema::Flags f = 0,
                ::xml_schema::Container* c = 0);

        virtual Gordon*
        _clone (::xml_schema::Flags f = 0,
                ::xml_schema::Container* c = 0) const;

        Gordon&
        operator= (const Gordon& x);

        virtual 
        ~Gordon ();

        // Implementation.
        //
        protected:
        void
        parse (::xsd::cxx::xml::dom::parser< char >&,
               ::xml_schema::Flags);

        protected:
        ::xsd::cxx::tree::one< BaseType > base_;
        ::xsd::cxx::tree::one< SeerShapeType > seerShape_;
        ::xsd::cxx::tree::one< UPicksType > uPicks_;
        ::xsd::cxx::tree::one< VPicksType > vPicks_;
        ::xsd::cxx::tree::one< ToleranceType > tolerance_;
        ::xsd::cxx::tree::one< GridLocationType > gridLocation_;
        ::xsd::cxx::tree::one< ToleranceLabelType > toleranceLabel_;
        ::xsd::cxx::tree::one< FaceIdType > faceId_;
        ::xsd::cxx::tree::one< WireIdType > wireId_;
        EdgeIdsSequence edgeIds_;
        VertexIdsSequence vertexIds_;
      };
    }
  }
}

#include <iosfwd>

#include <xercesc/sax/InputSource.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>

namespace prj
{
  namespace srl
  {
    namespace grds
    {
      // Parse a URI or a local file.
      //

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (const ::std::string& uri,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (const ::std::string& uri,
              ::xml_schema::ErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (const ::std::string& uri,
              ::xercesc::DOMErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse std::istream.
      //

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::std::istream& is,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::std::istream& is,
              ::xml_schema::ErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::std::istream& is,
              ::xercesc::DOMErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::std::istream& is,
              const ::std::string& id,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::std::istream& is,
              const ::std::string& id,
              ::xml_schema::ErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::std::istream& is,
              const ::std::string& id,
              ::xercesc::DOMErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse xercesc::InputSource.
      //

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::xercesc::InputSource& is,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::xercesc::InputSource& is,
              ::xml_schema::ErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::xercesc::InputSource& is,
              ::xercesc::DOMErrorHandler& eh,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      // Parse xercesc::DOMDocument.
      //

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (const ::xercesc::DOMDocument& d,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());

      ::std::unique_ptr< ::prj::srl::grds::Gordon >
      gordon (::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument > d,
              ::xml_schema::Flags f = 0,
              const ::xml_schema::Properties& p = ::xml_schema::Properties ());
    }
  }
}

#include <iosfwd>

#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/framework/XMLFormatter.hpp>

#include <xsd/cxx/xml/dom/auto-ptr.hxx>

namespace prj
{
  namespace srl
  {
    namespace grds
    {
      void
      operator<< (::xercesc::DOMElement&, const Gordon&);

      // Serialize to std::ostream.
      //

      void
      gordon (::std::ostream& os,
              const ::prj::srl::grds::Gordon& x, 
              const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
              const ::std::string& e = "UTF-8",
              ::xml_schema::Flags f = 0);

      void
      gordon (::std::ostream& os,
              const ::prj::srl::grds::Gordon& x, 
              ::xml_schema::ErrorHandler& eh,
              const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
              const ::std::string& e = "UTF-8",
              ::xml_schema::Flags f = 0);

      void
      gordon (::std::ostream& os,
              const ::prj::srl::grds::Gordon& x, 
              ::xercesc::DOMErrorHandler& eh,
              const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
              const ::std::string& e = "UTF-8",
              ::xml_schema::Flags f = 0);

      // Serialize to xercesc::XMLFormatTarget.
      //

      void
      gordon (::xercesc::XMLFormatTarget& ft,
              const ::prj::srl::grds::Gordon& x, 
              const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
              const ::std::string& e = "UTF-8",
              ::xml_schema::Flags f = 0);

      void
      gordon (::xercesc::XMLFormatTarget& ft,
              const ::prj::srl::grds::Gordon& x, 
              ::xml_schema::ErrorHandler& eh,
              const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
              const ::std::string& e = "UTF-8",
              ::xml_schema::Flags f = 0);

      void
      gordon (::xercesc::XMLFormatTarget& ft,
              const ::prj::srl::grds::Gordon& x, 
              ::xercesc::DOMErrorHandler& eh,
              const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
              const ::std::string& e = "UTF-8",
              ::xml_schema::Flags f = 0);

      // Serialize to an existing xercesc::DOMDocument.
      //

      void
      gordon (::xercesc::DOMDocument& d,
              const ::prj::srl::grds::Gordon& x,
              ::xml_schema::Flags f = 0);

      // Serialize to a new xercesc::DOMDocument.
      //

      ::xml_schema::dom::unique_ptr< ::xercesc::DOMDocument >
      gordon (const ::prj::srl::grds::Gordon& x, 
              const ::xml_schema::NamespaceInfomap& m = ::xml_schema::NamespaceInfomap (),
              ::xml_schema::Flags f = 0);
    }
  }
}

#include <xsd/cxx/post.hxx>

// Begin epilogue.
//
//
// End epilogue.

#endif // PRJ_SRL_GRDS_PRJSRLGRDSGORDON_H

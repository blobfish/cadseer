/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <assert.h>

#include <boost/current_function.hpp>

#include <osg/Geometry>
#include <osg/View>
#include <osgViewer/View>
#include <osg/ValueObject>
#include <osg/Point>
#include <osg/Switch>
#include <osgUtil/PolytopeIntersector>

#include "modelviz/mdvnodemaskdefs.h"
#include "globalutilities.h"
#include "application/appapplication.h"
#include "project/prjproject.h"
#include "feature/ftrbase.h"
#include "annex/annseershape.h"
#include "message/msgnode.h"
#include "message/msgsift.h"
#include "selection/slcdefinitions.h"
#include "selection/slcmessage.h"
#include "selection/slcinterpreter.h"
#include "selection/slcintersection.h"
#include "selection/slcvisitors.h"
#include "selection/slceventhandler.h"

//build polytope at origin. 8 sided.
static osg::Polytope buildBasePolytope(double radius)
{
  //apparently the order of the addition matters. intersector
  //wants opposites sequenced. This seems to be working.
  osg::Polytope out;
  osg::Matrixd rotation = osg::Matrixd::rotate(osg::DegreesToRadians(45.0), osg::Vec3d(0.0, 0.0, 1.0));
  osg::Vec3d base = osg::Vec3d(1.0, 0.0, 0.0);
  
  out.add(osg::Plane(base, radius));
  out.add(osg::Plane(-base, radius));
  
  base = rotation * base;
  
  out.add(osg::Plane(base, radius));
  out.add(osg::Plane(-base, radius));
  
  base = rotation * base;
  
  out.add(osg::Plane(base, radius));
  out.add(osg::Plane(-base, radius));
  
  base = rotation * base;
  
  out.add(osg::Plane(base, radius));
  out.add(osg::Plane(-base, radius));
  
  out.add(osg::Plane(0.0,0.0,1.0, 0.0)); //last has to be 'cap'
  
  return out;
}

static osg::Polytope buildPolytope(double x, double y, double radius)
{
  static osg::Polytope base = buildBasePolytope(radius);
  osg::Polytope out(base);
  out.transform(osg::Matrixd::translate(x, y, 0.0));
  
  return out;
}

static osg::Geometry* buildTempPoint(const osg::Vec3d& pointIn)
{
  osg::Geometry *geometry = new osg::Geometry();
  geometry->setNodeMask(mdv::point);
  geometry->setCullingActive(false);
  geometry->setDataVariance(osg::Object::DYNAMIC);
  osg::Vec3Array *vertices = new osg::Vec3Array();
  vertices->push_back(pointIn);
  geometry->setVertexArray(vertices);
  osg::Vec4Array *colors = new osg::Vec4Array();
  colors->push_back(osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f));
  geometry->setColorArray(colors);
  geometry->setColorBinding(osg::Geometry::BIND_OVERALL);
  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, 1));
  
  geometry->getOrCreateStateSet()->setAttribute(new osg::Point(10.0));
  geometry->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
  geometry->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF);
  
  return geometry;
}

using namespace boost::uuids;
using namespace slc;

struct EventHandler::Stow
{
  EventHandler &eventHandler;
  osg::ref_ptr<osg::Group> viewerRoot;
  msg::Node node;
  msg::Sift sift;
  unsigned int nodeMask = ~(mdv::backGroundCamera | mdv::gestureCamera | mdv::csys | mdv::point | mdv::noIntersect);
  Mask selectionMask;
  osg::Vec4 preHighlightColor = osg::Vec4(1.0, 1.0, 0.0, 1.0);
  osg::Vec4 selectionColor = osg::Vec4(1.0, 1.0, 1.0, 1.0);
  Containers selectionContainers;
  Container lastPrehighlight;
  osg::BoundingSphere selectionBound;
  
  Stow(EventHandler &evIn, osg::Group *viewerRootIn)
  : eventHandler(evIn)
  , viewerRoot(viewerRootIn)
  {
    node.connect(msg::hub());
    sift.name = "slc::EventHandler";
    node.setHandler(std::bind(&msg::Sift::receive, &sift, std::placeholders::_1));
    setupDispatcher();
    setSelectionMask(slc::AllEnabled);
  }
  
  void setSelectionMask(Mask maskIn)
  {
    selectionMask = maskIn;
    if
    (
      canSelectWires(selectionMask) ||
      canSelectFaces(selectionMask) ||
      canSelectShells(selectionMask) ||
      canSelectSolids(selectionMask) ||
      canSelectFeatures(selectionMask) ||
      canSelectObjects(selectionMask) ||
      canSelectNearestPoints(selectionMask)
    )
      nodeMask |= mdv::face;
    else
      nodeMask &= ~mdv::face;
    
    if
    (
      canSelectEdges(selectionMask) ||
      canSelectWires(selectionMask) ||
      canSelectFeatures(selectionMask) ||
      canSelectObjects(selectionMask) ||
      canSelectPoints(selectionMask)
    )
      nodeMask |= mdv::edge;
    else
      nodeMask &= ~mdv::edge;
    
    //obsolete. we no longer generate vertices
    //   if ((Selection::pointsSelectable & selectionMask) == Selection::pointsSelectable)
    //       nodeMask |= NodeMaskDef::vertex;
    //   else
    //       nodeMask &= ~NodeMaskDef::vertex;
  }
  
  void setPrehighlight(slc::Container &selected)
  {
    lastPrehighlight = selected;
    if (slc::isPointType(selected.selectionType))
    {
      assert(selected.pointGeometry.valid());
      osg::Vec4Array *colors = dynamic_cast<osg::Vec4Array*>(selected.pointGeometry->getColorArray());
      assert(colors);
      colors->at(0) = osg::Vec4(1.0, 1.0, 0.0, 1.0);
      colors->dirty();
    }
    else
      selectionOperation(selected.featureId, selected.selectionIds, HighlightVisitor::Operation::PreHighlight);
    
    msg::Message addMessage
    (
      msg::Response | msg::Post | msg::Preselection | msg::Add
      , containerToMessage(lastPrehighlight)
    );
    node.send(addMessage);
  }
  
  void clearPrehighlight()
  {
    if (lastPrehighlight.selectionType == Type::None)
      return;
    
    msg::Message removeMessage
    (
      msg::Response | msg::Pre | msg::Preselection | msg::Remove
      , containerToMessage(lastPrehighlight)
    );
    node.send(removeMessage);
    
    if (slc::isPointType(lastPrehighlight.selectionType))
    {
      assert(lastPrehighlight.pointGeometry.valid());
      osg::Group *parent = lastPrehighlight.pointGeometry->getParent(0)->asGroup();
      parent->removeChild(lastPrehighlight.pointGeometry);
    }
    else
    {
      selectionOperation(lastPrehighlight.featureId, lastPrehighlight.selectionIds, HighlightVisitor::Operation::Restore);
      //certain situations, like selecting wires, where prehighlight can over write something already selected. Then
      //when the prehighlight gets cleared and the color restored the selection is lost. for now lets just re select
      //everything and do something different if we have performance problems.
      for (const auto &current : selectionContainers)
        selectionOperation(current.featureId, current.selectionIds, HighlightVisitor::Operation::Highlight);
    }
    
    lastPrehighlight = Container();
  }
  
  bool alreadySelected(const slc::Container &testContainer)
  {
    return slc::has(selectionContainers, testContainer);
  }
  
  /* selections only get added one at a time unlike removals that can happen
   * one at a time or all at once. This difference makes addSelection and
   * removeSelection quite different.
   */
  
  void addSelection(const slc::Container &cIn)
  {
    assert(!alreadySelected(cIn)); //don't set me up.
    if (slc::isPointType(cIn.selectionType))
    {
      assert(cIn.pointGeometry.valid());
      auto *g = cIn.pointGeometry.get();
      osg::Vec4Array *colors = dynamic_cast<osg::Vec4Array*>(g->getColorArray()); assert(colors);
      (*colors)[0] = osg::Vec4(1.0, 1.0, 1.0, 1.0);
      colors->dirty();
      g->dirtyDisplayList();
      if (!viewerRoot->containsNode(g)) //point created from intersection is already added.
        viewerRoot->addChild(g); //point created from message needs to be added.
    }
    else
    {
      selectionOperation(cIn.featureId, cIn.selectionIds, HighlightVisitor::Operation::Highlight);
    }
    //caller is responsible for pre and post selection messages.
    slc::add(selectionContainers, cIn);
    expandBound(selectionContainers.back());
    sendBoundMessage();
  }
  
  void removeSelection(slc::Containers::reverse_iterator rit)
  {
    assert(rit != selectionContainers.rend()); //don't set me up
    
    slc::Message smOut = containerToMessage(*rit);
    msg::Message preMessage(msg::Response | msg::Pre | msg::Selection | msg::Remove, smOut);
    node.send(preMessage);
    
    if (slc::isPointType(rit->selectionType))
    {
      assert(rit->pointGeometry.valid());
      osg::Group *parent = rit->pointGeometry->getParent(0)->asGroup();
      parent->removeChild(rit->pointGeometry);
    }
    else
      selectionOperation(rit->featureId, rit->selectionIds, HighlightVisitor::Operation::Restore);
    selectionContainers.erase((++rit).base()); //jfc
    
    msg::Message postMessage(msg::Response | msg::Post | msg::Selection | msg::Remove, smOut);
    node.send(postMessage);
  }
  
  void clearSelections()
  {
    // clear in reverse order to fix wire issue were edges remained highlighted. edge was remembering already selected color.
    // something else will have to been when we get into delselection. maybe pool of selection and indexes for container?
    Containers::reverse_iterator it;
    for (it = selectionContainers.rbegin(); it != selectionContainers.rend(); ++it)
      removeSelection(it);
    resetSelectionBound();
  }
  
  void selectionOperation
  (
    const uuid &featureIdIn
    , const std::vector<uuid> &idsIn
    , HighlightVisitor::Operation operationIn
  )
  {
    MainSwitchVisitor sVisitor(featureIdIn);
    viewerRoot->accept(sVisitor);
    if (!sVisitor.out)
      return;
    HighlightVisitor hVisitor(idsIn, operationIn);
    sVisitor.out->accept(hVisitor);
  }
  
  void setupDispatcher()
  {
    sift.insert
    (
      {
        std::make_pair
        (
          msg::Request | msg::Preselection | msg::Add
          , std::bind(&Stow::requestPreselectionAdditionDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::Preselection | msg::Remove
          , std::bind(&Stow::requestPreselectionSubtractionDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::Selection | msg::Add
          , std::bind(&Stow::requestSelectionAdditionDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::Selection | msg::Remove
          , std::bind(&Stow::requestSelectionSubtractionDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Request | msg::Selection | msg::Clear
          , std::bind(&Stow::requestSelectionClearDispatched, this, std::placeholders::_1)
        )
        , std::make_pair
        (
          msg::Response | msg::Selection | msg::SetMask
          , std::bind(&Stow::selectionMaskDispatched, this, std::placeholders::_1)
        )
      }
    );
  }
  
  void requestPreselectionAdditionDispatched(const msg::Message &messageIn)
  {
    slc::Message sMessage = messageIn.getSLC();
    slc::Container container = messageToContainer(sMessage);
    if (container == lastPrehighlight || alreadySelected(container)) return;
    //setPrehighlight handles points
    clearPrehighlight();
    setPrehighlight(container);
  }
  
  void requestPreselectionSubtractionDispatched(const msg::Message &messageIn)
  {
    slc::Message sMessage = messageIn.getSLC();
    slc::Container container = messageToContainer(sMessage);
    assert(container == lastPrehighlight);
    //clearPrehighlight handles points
    clearPrehighlight();
  }
  
  void requestSelectionAdditionDispatched(const msg::Message &messageIn)
  {
    clearPrehighlight();
    
    slc::Message sMessage = messageIn.getSLC();
    slc::Container container = messageToContainer(sMessage);
    sMessage = containerToMessage(container); //hack for no feature type coming from dag view.
    if (alreadySelected(container)) return;
    
    node.send(msg::Message(msg::Response | msg::Pre | msg::Selection | msg::Add, sMessage));
    addSelection(container);
    node.send(msg::Message(msg::Response | msg::Post | msg::Selection | msg::Add, sMessage));
  }
  
  void requestSelectionSubtractionDispatched(const msg::Message &messageIn)
  {
    slc::Message sMessage = messageIn.getSLC();
    slc::Container container = messageToContainer(sMessage);
    auto rit = std::find(selectionContainers.rbegin(), selectionContainers.rend(), container);
    assert(rit != selectionContainers.rend());
    if (rit == selectionContainers.rend()) return;
    removeSelection(rit);
    resetSelectionBound();
  }
  
  void requestSelectionClearDispatched(const msg::Message &)
  {
    clearPrehighlight();
    clearSelections();
  }
  
  void selectionMaskDispatched(const msg::Message &messageIn)
  {
    slc::Message sMsg = messageIn.getSLC();
    setSelectionMask(sMsg.selectionMask);
  }
  
  void resetSelectionBound()
  {
    selectionBound.init(); //clears the sphere.
    for (const auto &c : selectionContainers) expandBound(c);
    sendBoundMessage();
  }
  
  void expandBound(const Container &c)
  {
    if (c.isPointType())
    {
      selectionBound.expandBy(c.pointLocation);
    }
    else if (c.isObjectType())
    {
      slc::MainSwitchVisitor v(c.featureId);
      viewerRoot->accept(v);
      if (v.out) selectionBound.expandBy(v.out->getBound());
    }
    else if (c.isShapeType())
    {
      slc::MainSwitchVisitor v(c.featureId);
      viewerRoot->accept(v);
      if (v.out)
      {
        slc::BoundingSphereVisitor bv(selectionBound, c.selectionIds);
        v.out->accept(bv);
      }
    }
  }
  
  void sendBoundMessage()
  {
    slc::Message sm;
    sm.pointLocation = selectionBound.center();
    sm.radius = selectionBound.radius();
    node.send(msg::Message(msg::Response | msg::Selection | msg::Sphere | msg::Update, sm));
  }
};

EventHandler::EventHandler(osg::Group *viewerRootIn)
: osgGA::GUIEventHandler()
, stow(std::make_unique<Stow>(*this, viewerRootIn))
{}

const Containers& EventHandler::getSelections() const
{
  return stow->selectionContainers;
}

bool EventHandler::handle(const osgGA::GUIEventAdapter &eventAdapter,
                    osgGA::GUIActionAdapter &actionAdapter, osg::Object*, osg::NodeVisitor*)
{
  if (eventAdapter.getModKeyMask() & osgGA::GUIEventAdapter::MODKEY_LEFT_CTRL)
  {
    stow->clearPrehighlight();
    return false;
  }
  
  if(eventAdapter.getHandled())
  {
    stow->clearPrehighlight();
    return true; //overlay has taken event;
  }
    
  //escape key should dispatch to cancel command.
  if (eventAdapter.getEventType() == osgGA::GUIEventAdapter::KEYUP)
  {
    if (eventAdapter.getKey() == osgGA::GUIEventAdapter::KEY_Escape)
    {
      stow->node.send(msg::Message(msg::Request | msg::Command | msg::Done));
      return true;
    }
  }
  
  if (eventAdapter.getEventType() == osgGA::GUIEventAdapter::MOVE)
  {
    osg::View* view = actionAdapter.asView();
    if (!view)
      return false;
      
    Intersections currentIntersections;
      
    //use poly on just the edges for now.
    if (stow->nodeMask & mdv::edge)
    {
      osg::ref_ptr<osgUtil::PolytopeIntersector> polyPicker = new osgUtil::PolytopeIntersector
      (
        osgUtil::Intersector::WINDOW,
        buildPolytope(eventAdapter.getX(), eventAdapter.getY(), 16.0)
//         eventAdapter.getX() - 16.0,
//         eventAdapter.getY() - 16.0,
//         eventAdapter.getX() + 16.0,
//         eventAdapter.getY() + 16.0
      );
      osgUtil::IntersectionVisitor polyVisitor(polyPicker.get());
      polyVisitor.setTraversalMask(stow->nodeMask & ~mdv::face);
      view->getCamera()->accept(polyVisitor);
      append(currentIntersections, polyPicker->getIntersections());
    }
      
    //use linesegment on just the faces for now.
    if (stow->nodeMask & mdv::face)
    {
      osg::ref_ptr<osgUtil::LineSegmentIntersector> linePicker = new osgUtil::LineSegmentIntersector
      (
        osgUtil::Intersector::WINDOW,
        eventAdapter.getX(),
        eventAdapter.getY()
      );
      osgUtil::IntersectionVisitor lineVisitor(linePicker.get());
      lineVisitor.setTraversalMask(stow->nodeMask & ~mdv::edge);
//         lineVisitor.setUseKdTreeWhenAvailable(false); //temp for testing.
      view->getCamera()->accept(lineVisitor);
      append(currentIntersections, linePicker->getIntersections());
    }

    if (!currentIntersections.empty())
    {
      Interpreter interpreter(currentIntersections, stow->selectionMask);

      slc::Container newContainer;
      //loop to get first non selected geometry.
      for (const auto& container : interpreter.containersOut)
      {
        if(stow->alreadySelected(container)) continue;
        newContainer = container;
        break;
      }

      if (newContainer == stow->lastPrehighlight)
      {
        //update the point location though.
        stow->lastPrehighlight.pointLocation = newContainer.pointLocation;
        return false;
      }

      stow->clearPrehighlight();

      if (newContainer.selectionType == slc::Type::None)
        return false;
        
      //this is here so we make sure we get through all selection
      //conditions before we construct point geometry.
      if (isPointType(newContainer.selectionType))
      {
        osg::ref_ptr<osg::Geometry> pointGeometry(buildTempPoint(newContainer.pointLocation));
        newContainer.pointGeometry = pointGeometry.get();
        stow->viewerRoot->addChild(pointGeometry.get());
      }
      stow->setPrehighlight(newContainer);
    }
    else
      stow->clearPrehighlight();
  }

  if (eventAdapter.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON &&
          eventAdapter.getEventType() == osgGA::GUIEventAdapter::RELEASE)
  {
    if (stow->lastPrehighlight.selectionType != slc::Type::None)
    {
      //prehighlight gets 'moved' into selections so can't call
      //clear prehighlight, but we still clear the prehighlight
      //selections we need to make observers aware of this 'hidden' change.
      msg::Message clearMessage
      (
        msg::Response | msg::Pre | msg::Preselection | msg::Remove
        , containerToMessage(stow->lastPrehighlight)
      );
      stow->node.send(clearMessage);
      
      slc::Message smOut = containerToMessage(stow->lastPrehighlight);
      msg::Message preMessage(msg::Response | msg::Pre | msg::Selection | msg::Add, smOut);
      stow->node.send(preMessage);
      
      stow->addSelection(stow->lastPrehighlight);
      
      msg::Message postMessage(msg::Response | msg::Post | msg::Selection | msg::Add, smOut);
      stow->lastPrehighlight = Container(); //set to null before signal in case we end up in 'this' again.
      stow->node.send(postMessage);
    }
    //not clearing the selection anymore on a empty pick.
//       else
//         clearSelections();
  }
    
  if (eventAdapter.getButton() == osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON &&
          eventAdapter.getEventType() == osgGA::GUIEventAdapter::RELEASE)
  {
    stow->clearSelections();
  }

  if (eventAdapter.getButton() == osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON &&
          eventAdapter.getEventType() == osgGA::GUIEventAdapter::PUSH)
  {
    stow->clearPrehighlight();
  }

  if (eventAdapter.getEventType() == osgGA::GUIEventAdapter::DRAG)
  {
      //don't get button info here, need to cache.
  }

  return false;
}

Message EventHandler::containerToMessage(const Container &containerIn)
{
  slc::Message out;
  out.type = containerIn.selectionType;
  out.featureId = containerIn.featureId;
  out.featureType = containerIn.featureType;
  out.shapeId = containerIn.shapeId;
  out.pointLocation = containerIn.pointLocation;
  
  return out;
}

Container EventHandler::messageToContainer(const Message &messageIn)
{
  /*
    should incoming selection messages ignore selection mask?
      I am thinking yes right now. Selection masks are only really
      useful for cursor picking.
    Keep in mind that we highlight lower dimension objects for wireframe rendering style.
      For example we highlight the edges when we highlight faces so we get visual feedback
      in wireframe mode. Probably we should cache the current rendering style and highlight
      accordingly, to keep this as lean as possible.
  */
  
  assert(!messageIn.featureId.is_nil());
  const ftr::Base *feature = dynamic_cast<app::Application *>(qApp)->getProject()->findFeature(messageIn.featureId);
  assert(feature);
  
  slc::Container container;
  container.selectionType = messageIn.type;
  container.accrue = messageIn.accrue;
  container.featureId = messageIn.featureId;
  container.featureType = messageIn.featureType;
  container.shapeId = messageIn.shapeId;
  container.pointLocation = messageIn.pointLocation;
  if (container.featureType == ftr::Type::Base)
    container.featureType = feature->getType();
  
  if (feature->hasAnnex(ann::Type::SeerShape) && !feature->getAnnex<ann::SeerShape>().isNull())
  {
    const ann::SeerShape &seerShape = feature->getAnnex<ann::SeerShape>();
    auto addAllEdges =[&]()
    {
      auto temp = seerShape.useGetChildrenOfType(seerShape.getRootShapeId(), TopAbs_EDGE);
      container.selectionIds.insert(container.selectionIds.end(), temp.begin(), temp.end());
    };
    auto addAllFaces =[&]()
    {
      auto temp = seerShape.useGetChildrenOfType(seerShape.getRootShapeId(), TopAbs_FACE);
      container.selectionIds.insert(container.selectionIds.end(), temp.begin(), temp.end());
    };
    if
    (
      messageIn.type == slc::Type::Object
      || messageIn.type == slc::Type::Solid
      || messageIn.type == slc::Type::Shell
    )
    {
      switch (messageIn.accrue)
      {
        case slc::Accrue::AllEdges:{addAllEdges(); break;}
        default:{addAllFaces(); addAllEdges(); break;}
      }
    }
    //skip feature for now.
    else if (messageIn.type == slc::Type::Face)
    {
      if (container.accrue == Accrue::Tangent)
        container.selectionIds = seerShape.useWalkTangentFaces(container.shapeId, container.accrue.angle);
      else
        container.selectionIds.push_back(container.shapeId);
    }
    else if (messageIn.type == slc::Type::Edge)
    {
      if (container.accrue == Accrue::Tangent)
        container.selectionIds = seerShape.useWalkTangentEdges(container.shapeId);
      else
        container.selectionIds.push_back(container.shapeId);
    }
    else if (messageIn.type == slc::Type::Wire)
    {
      container.selectionIds = seerShape.useGetChildrenOfType(container.shapeId, TopAbs_EDGE);
    }
    else if (isPointType(messageIn.type))
    {
      container.pointGeometry = buildTempPoint(messageIn.pointLocation);
    }
  }
  
  return container;
}

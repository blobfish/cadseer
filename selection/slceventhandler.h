/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2015  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SLC_SELECTIONEVENTHANDLER_H
#define SLC_SELECTIONEVENTHANDLER_H

#include <memory>

#include <osgGA/GUIEventHandler>

#include "selection/slccontainer.h"

namespace osg{class Group;}

namespace slc
{
  struct Message;
  class EventHandler : public osgGA::GUIEventHandler
  {
  public:
    EventHandler(osg::Group* viewerRootIn);
    const slc::Containers& getSelections() const;

    static slc::Container messageToContainer(const slc::Message &);
    static slc::Message containerToMessage(const slc::Container &);
  protected:
    bool handle(const osgGA::GUIEventAdapter&, osgGA::GUIActionAdapter&, osg::Object*, osg::NodeVisitor*) override;
  private:
    struct Stow;
    std::unique_ptr<Stow> stow;
  };
}

#endif // SLC_SELECTIONEVENTHANDLER_H

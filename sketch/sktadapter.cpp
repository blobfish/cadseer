/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include <osg/Quat>

#include "sketch/sktsolver.h"
#include "sketch/sktadapter.h"

using namespace skt;

PointAdapter::PointAdapter(const skt::Solver &solverIn, skt::SSHandle ph)
{
  bool typeTest = solverIn.isEntityType(ph, SLVS_E_POINT_IN_2D) || solverIn.isEntityType(ph, SLVS_E_POINT_IN_3D);
  assert(typeTest); if (!typeTest) return;
  auto pe = solverIn.findEntity(ph); assert(pe); if (!pe) return;
  
  point = osg::Vec3d
  (
    *solverIn.getParameterValue(pe->get().param[0])
    , *solverIn.getParameterValue(pe->get().param[1])
    , 0.0
  );
  if (solverIn.isEntityType(ph, SLVS_E_POINT_IN_3D)) point->z() = *solverIn.getParameterValue(pe->get().param[2]);
}

PointAdapter::operator osg::Vec3d() const
{
  assert(point);
  if (!point) return osg::Vec3d();
  return *point;
}

PointAdapter::operator osg::Vec3f() const
{
  assert(point);
  if (!point) return osg::Vec3f();
  return *point;
}

CurveAdapter::CurveAdapter(const skt::Solver &solverIn, skt::SSHandle eh)
{
  auto ce = solverIn.findEntity(eh); assert(ce); if (!ce) return;
  type = ce->get().type;
  
  switch (ce->get().type)
  {
    case SLVS_E_LINE_SEGMENT:
    {
      startPointHandle = ce->get().point[0];
      finishPointHandle = ce->get().point[1];
      PointAdapter pa0(solverIn, *startPointHandle); start = pa0.point;
      PointAdapter pa1(solverIn, *finishPointHandle); finish = pa1.point;
      break;
    }
    case SLVS_E_ARC_OF_CIRCLE:
    {
      startPointHandle = ce->get().point[1];
      finishPointHandle = ce->get().point[2];
      PointAdapter pa0(solverIn, ce->get().point[0]); center = pa0.point;
      PointAdapter pa1(solverIn, *startPointHandle); start = pa1.point;
      PointAdapter pa2(solverIn, *finishPointHandle); finish = pa2.point;
      break;
    }
    case SLVS_E_CUBIC:
    {
      startPointHandle = ce->get().point[0];
      finishPointHandle = ce->get().point[3];
      PointAdapter pa0(solverIn, *startPointHandle); start = pa0.point;
      PointAdapter pa1(solverIn, ce->get().point[1]); startPrime = pa1.point;
      PointAdapter pa2(solverIn, ce->get().point[2]); finishPrime = pa2.point;
      PointAdapter pa3(solverIn, *finishPointHandle); finish = pa3.point;
      break;
    }
    default:
    {
      type = 0;
      break;
    }
  }
}

std::optional<osg::Vec3d> CurveAdapter::startTangent()
{
  switch (type)
  {
    case SLVS_E_LINE_SEGMENT:
    {
      auto out = *start - *finish;
      if (out.length() < std::numeric_limits<float>::epsilon()) return std::nullopt;
      out.normalize();
      return out;
    }
    case SLVS_E_ARC_OF_CIRCLE:
    {
      osg::Vec3d tangent = *start - *center;
      if (tangent.length() < std::numeric_limits<float>::epsilon()) return std::nullopt;
      tangent.normalize();
      osg::Quat rotation(osg::DegreesToRadians(90.0), osg::Vec3d(0.0, 0.0, -1.0));
      return rotation * tangent;
    }
    case SLVS_E_CUBIC:
    {
      auto out = *start - *startPrime;
      if (out.length() < std::numeric_limits<float>::epsilon()) return std::nullopt;
      out.normalize();
      return out;
    }
    default:
    {
      assert(0);
    }
  }
  return std::nullopt;
}

std::optional<osg::Vec3d> CurveAdapter::finishTangent()
{
  switch (type)
  {
    case SLVS_E_LINE_SEGMENT:
    {
      auto out = *finish - *start;
      if (out.length() < std::numeric_limits<float>::epsilon()) return std::nullopt;
      out.normalize();
      return out;
    }
    case SLVS_E_ARC_OF_CIRCLE:
    {
      osg::Vec3d tangent = *finish - *center;
      if (tangent.length() < std::numeric_limits<float>::epsilon()) return std::nullopt;
      tangent.normalize();
      osg::Quat rotation(osg::DegreesToRadians(90.0), osg::Vec3d(0.0, 0.0, 1.0));
      return rotation * tangent;
    }
    case SLVS_E_CUBIC:
    {
      auto out = *finish - *finishPrime;
      if (out.length() < std::numeric_limits<float>::epsilon()) return std::nullopt;
      out.normalize();
      return out;
    }
    default:
    {
      assert(0);
    }
  }
  return std::nullopt;
}

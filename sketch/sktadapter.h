/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_ADAPTER_H
#define SKT_ADAPTER_H

#include <optional>

#include <osg/Vec3d>
#include <osg/Vec3f>

#include "subprojects/solvespace/include/slvs.h"
#include "sketch/skttypes.h"

namespace skt
{
  struct Solver;
  
  //adapting solve space point to OSG vec3d
  struct PointAdapter
  {
    std::optional<osg::Vec3d> point;
    
    PointAdapter() = delete;
    PointAdapter(const skt::Solver &solverIn, skt::SSHandle ph);
    operator osg::Vec3d() const;
    operator osg::Vec3f() const;
  };
  
  //adapting solvespace curve to OSG vec3d points
  struct CurveAdapter
  {
    int type = 0;
    std::optional<osg::Vec3d> start; //all
    std::optional<osg::Vec3d> startPrime; //bezier
    std::optional<osg::Vec3d> finishPrime; //bezier
    std::optional<osg::Vec3d> finish; //all
    std::optional<osg::Vec3d> center; //arc
    
    std::optional<SSHandle> startPointHandle;
    std::optional<SSHandle> finishPointHandle;
    
    CurveAdapter() = delete;
    CurveAdapter(const skt::Solver &solverIn, skt::SSHandle eh);
    
    bool isLine() {return type == SLVS_E_LINE_SEGMENT;}
    bool isArc() {return type == SLVS_E_ARC_OF_CIRCLE;}
    bool isBezier() {return type == SLVS_E_CUBIC;}
    bool isConnectable() {return isLine() || isArc() || isBezier();}
    
    std::optional<osg::Vec3d> startTangent();
    std::optional<osg::Vec3d> finishTangent();
  };
}

#endif //SKT_ADAPTER_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include <osg/BoundingSphere>
#include <osg/Matrixd>

#include "library/lbrrangemask.h"
#include "sketch/sktsolver.h"
#include "sketch/sktadapter.h"
#include "sketch/sktanglewrench.h"

using namespace skt;

AngleWrench::AngleWrench(Solver &sIn, const SSHandles &linesIn, bool doSense)
: solver(sIn)
{
  assert(linesIn.size() == 2); if (linesIn.size() != 2) return;
  
  auto addLine = [&](SSHandle line)
  {
    auto ol = sIn.findEntity(line); assert(ol); assert(sIn.isEntityLine(*ol));
    osg::Vec3d startPoint = PointAdapter(sIn, ol->get().point[0]);
    osg::Vec3d finishPoint = PointAdapter(sIn, ol->get().point[1]);
    angleTool.addSegment(finishPoint, startPoint);
  };
  addLine(linesIn.front());
  addLine(linesIn.back());
  if (doSense) angleTool.processSensed(std::nullopt);
  else angleTool.process(std::nullopt);
  if (angleTool.getState() < tls::Angle::Origin) validity = false;
}

osg::Vec3d AngleWrench::getIntersectionPoint() const
{
  return angleTool.getOrigin();
}

//false for solve space means the curves are oriented the same.
bool AngleWrench::gleanOther() const
{
  return angleTool.getOther();
}

double AngleWrench::getAngle() const
{
  return angleTool.getAngle();
}

osg::Matrixd AngleWrench::getCSys() const
{
  if (!validity || angleTool.getState() < tls::Angle::System) return osg::Matrixd();
  return angleTool.getSystem();
}

osg::Vec3d AngleWrench::getTextLocation() const
{
  if (!validity || angleTool.getState() < tls::Angle::TextPosition) return osg::Vec3d();
  return angleTool.getTextPosition();
}

lbr::RangeMask AngleWrench::getFirstRangeMask() const
{
  if (!validity || angleTool.getState() < tls::Angle::RangeMasks) return {0.0, 0.0};
  return angleTool.getFirstRangeMask();
}

lbr::RangeMask AngleWrench::getSecondRangeMask() const
{
  if (!validity || angleTool.getState() < tls::Angle::RangeMasks) return {0.0, 0.0};
  return angleTool.getLastRangeMask();
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_ANGLE_H
#define SKT_ANGLE_H

#include <osg/Vec3d>

#include "tools/tlsangle.h"
#include "sketch/skttypes.h"

namespace osg{class Matrixd;}
namespace lbr{struct RangeMask;}

/* Ok building and updating angle constraints and dimensions is messy.
* So this stuff here is to separate this mess from the skt::Visual mess.
*/
namespace skt
{
  struct Solver;
  /*
   What to do when the angle is 0 or 180?
   */
  struct AngleWrench
  {
  private:
    Solver &solver;
    bool validity = true;
    tls::Angle angleTool;
  public:
    AngleWrench(Solver&, const SSHandles&, bool = false);
    bool isValid() {return validity;}
    osg::Vec3d getIntersectionPoint() const;
    bool gleanOther() const; //'other' is the solvespace constraint value.
    double getAngle() const;
    osg::Matrixd getCSys() const;
    osg::Vec3d getTextLocation() const;
    lbr::RangeMask getFirstRangeMask() const;
    lbr::RangeMask getSecondRangeMask() const;
  };
}

#endif //SKT_ANGLE_H

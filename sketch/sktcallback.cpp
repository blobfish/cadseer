/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include "library/lbrplabel.h"
#include "sketch/sktosg.h"
#include "sketch/sktcallback.h"

using namespace skt;

//determines if the color of object should be updated
bool ColorCallback::shouldApplyColor()
{
  //always update color when highlight state changes.
  if (highlight.isDirty()) return true;
  //now highlight hasn't changed and special cases require change.
  if (color.isDirty() && highlight.next == Highlight::none) return true;
  if (preHighlightColor.isDirty() && highlight.next == Highlight::preHighlighted) return true;
  if (highlightColor.isDirty() && highlight.next == Highlight::highlighted) return true;
  return false;
}

void ColorCallback::resolveColor(osg::Geometry *g)
{
  if (shouldApplyColor())
  {
    osg::Vec4 colorOut;
    switch(highlight.next) //highlight doesn't get updated until leaf does it. So use 'next'
    {
      case Highlight::none: {colorOut = color.next; break;}
      case Highlight::preHighlighted: {colorOut = preHighlightColor.next; break;}
      case Highlight::highlighted: {colorOut = highlightColor.next; break;}
    }
    skt::setColor(g, colorOut);
  }
}

DepthCallback::DepthCallback() : BaseCallback(), defaultDepth{0.0, 0.0}
{
  depth = new osg::Depth(osg::Depth::LESS, 0.0, 1.0);
}

void DepthCallback::resolveDepth()
{
  if (!defaultDepth.isDirty() && !highlight.isDirty()) return;
  if (highlight.next == Highlight::none) pushBack();
  else pullForward();
}


//should have general curve callback or a class for each type?
CurveCallback::CurveCallback() : ColorCallback(), width{0.0, 0.0}
{
  lineWidth = new osg::LineWidth();
}

bool CurveCallback::run(osg::Object *object, osg::Object *data)
{
  osg::Geometry *g = dynamic_cast<osg::Geometry*>(object); assert(g); if (!g) return false;
  resolveColor(g);
  resolveDepth();
  resolveWidth();
  clean();
  return traverse(object, data);
}

void CurveCallback::resolveWidth()
{
  if (!width.isDirty() && !highlight.isDirty()) return;
  lineWidth->setWidth(width.next);
}

osg::Geometry* CurveCallback::buildCurve()
{
  osg::Geometry *out = buildGeometry(osg::Vec4());
  out->setVertexArray(new osg::Vec3Array());
  out->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, 0));
  out->getOrCreateStateSet()->setAttributeAndModes(depth);
  out->getOrCreateStateSet()->setAttributeAndModes(lineWidth);
  out->setUpdateCallback(this);
  return out;
}

PointCallback::PointCallback() : ColorCallback(), size{0.0, 0.0}
{
  point = new osg::Point();
}

void PointCallback::resolveSize()
{
  if (!size.isDirty() && !highlight.isDirty()) return;
  if (highlight.next == Highlight::none) point->setSize(size.next);
  else point->setSize(size.next * 1.5);
}

bool PointCallback::run(osg::Object *object, osg::Object *data)
{
  osg::Geometry *g = dynamic_cast<osg::Geometry*>(object); assert(g); if (!g) return false;
  resolveColor(g);
  resolveDepth();
  resolveSize();
  clean();
  return traverse(object, data);
}

osg::Geometry* PointCallback::buildPoint()
{
  osg::Geometry *out = buildGeometry(osg::Vec4());
  out->setVertexArray(new osg::Vec3Array(1));
  out->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, 1));
  out->getOrCreateStateSet()->setAttributeAndModes(depth);
  out->getOrCreateStateSet()->setAttributeAndModes(point);
  out->setUpdateCallback(this);
  return out;
}

bool ConstraintCallback::run(osg::Object *object, osg::Object *data)
{
  resolveDepth();
  
  osgText::Text *theText = nullptr;
  if (osg::AutoTransform *transform = dynamic_cast<osg::AutoTransform*>(object))
    theText = dynamic_cast<osgText::Text*>(transform->getChild(0));
  else if (osg::Group *group = dynamic_cast<osg::Group*>(object))
    if (osg::AutoTransform *transform = dynamic_cast<osg::AutoTransform*>(group->getChild(0)))
      theText = dynamic_cast<osgText::Text*>(transform->getChild(0));
  
  if (shouldApplyColor() && theText)
  {
    osg::Vec4 colorOut;
    switch(highlight.next) //highlight doesn't get updated until leaf does it. So use 'next'
    {
      case Highlight::none: {colorOut = color.next; break;}
      case Highlight::preHighlighted: {colorOut = preHighlightColor.next; break;}
      case Highlight::highlighted: {colorOut = highlightColor.next; break;}
    }
    theText->setColor(colorOut);
  }
  if (text.isDirty() && theText) theText->setText(text.next);
  
  clean();
  return traverse(object, data);
}

osg::AutoTransform* ConstraintCallback::buildConstraint1()
{
  osg::AutoTransform *out = new osg::AutoTransform();
  out->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_SCREEN);
  out->setAutoScaleToScreen(true);
  out->getOrCreateStateSet()->setAttributeAndModes(depth);
  osgText::Text *hText = buildConstraintText("", osg::Vec4());
  out->addChild(hText);
  out->setUpdateCallback(this);
  return out;
}

osg::Group* ConstraintCallback::buildConstraint2()
{
  osg::Group *out = new osg::Group();
  out->getOrCreateStateSet()->setAttributeAndModes(depth);
  osgText::Text *text = buildConstraintText("", osg::Vec4());
  auto build = [&]()
  {
    osg::AutoTransform *t = new osg::AutoTransform();
    t->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_SCREEN);
    t->setAutoScaleToScreen(true);
    t->addChild(text);
    out->addChild(t);
  };
  build();
  build();
  out->setUpdateCallback(this);
  return out;
}

DimensionCallback::DimensionCallback(osg::Node *target)
{
  target->getOrCreateStateSet()->setAttributeAndModes(depth);
  target->setUpdateCallback(this);
}

bool DimensionCallback::run(osg::Object *object, osg::Object *data)
{
  lbr::PLabel *label = dynamic_cast<lbr::PLabel*>(object); assert(label); if (!label) return false;
  if (shouldApplyColor())
  {
    osg::Vec4 colorOut;
    switch(highlight.next) //highlight doesn't get updated until leaf does it. So use 'next'
    {
      case Highlight::none: {label->setTextColor(); break;}
      case Highlight::preHighlighted: {label->setTextColor(preHighlightColor.next); break;}
      case Highlight::highlighted: {label->setTextColor(highlightColor.next); break;}
    }
  }
  resolveDepth();
  clean();
  return traverse(object, data);
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_CALLBACK_H
#define SKT_CALLBACK_H

#include <osg/Vec4>
#include <osg/Depth>
#include <osg/LineWidth>
#include <osg/Point>
#include <osg/Geometry>
#include <osg/Callback>
#include <osg/NodeVisitor>
#include <osg/MatrixTransform>

// osg callbacks to abstract away mess of highlighting and maybe more.
// how to use this with highlight of dimensions, which are also controlled by callbacks.
/* To use:
 * Create leaf class
 * Set desired values
 * call build* to generate osg::Geometry*
 * Do use leaf class again as it is tied to the generated osg::Geometry.
 */

namespace skt
{
  template<typename T>
  struct Payload
  {
    T current;
    T next;
    bool isDirty(){return current != next;}
    void clean() {current = next;}
  };
  
  class BaseCallback : public osg::Callback
  {
  public:
    enum class Highlight
    {
      none //no highlighting
      , preHighlighted
      , highlighted
    };
    void setHighlight(Highlight sIn) {highlight.next = sIn;}
    
  protected:
    Payload<Highlight> highlight;
    
    void clean(){highlight.clean();}
  };
  
  class DepthCallback : public BaseCallback
  {
  public:
    DepthCallback();
    void setDefaultDepth(float vIn) {defaultDepth.next = vIn;}
  protected:
    Payload<float> defaultDepth; //low value.
    osg::ref_ptr<osg::Depth> depth;
    
    void pullForward() {depth->setRange(0.0, 1.0);}
    void pushBack() {depth->setRange(defaultDepth.next, 1.0);}
    void resolveDepth();
    void clean(){defaultDepth.clean(); BaseCallback::clean();}
  };
  
  class ColorCallback : public DepthCallback
  {
  public:
    void setColor(const osg::Vec4 &cIn) {color.next = cIn;}
    void setPrehighlightColor(const osg::Vec4 &cIn) {preHighlightColor.next = cIn;}
    void setHighlightColor(const osg::Vec4 &cIn) {highlightColor.next = cIn;}
  protected:
    Payload<osg::Vec4> color;
    Payload<osg::Vec4> preHighlightColor;
    Payload<osg::Vec4> highlightColor;
    
    bool shouldApplyColor();
    void resolveColor(osg::Geometry *);
    void clean()
    {
      color.clean();
      preHighlightColor.clean();
      highlightColor.clean();
      DepthCallback::clean();
    }
  };
  
  class CurveCallback : public ColorCallback
  {
  public:
    CurveCallback();
    void setWidth(float wIn) {width.next = wIn;}
    bool run(osg::Object*, osg::Object*) override;
    osg::Geometry* buildCurve();
  protected:
    Payload<float> width;
    osg::ref_ptr<osg::LineWidth> lineWidth;
    
    void resolveWidth();
    void setClean() {width.clean(); ColorCallback::clean();}
  };
  
  class PointCallback : public ColorCallback
  {
  public:
    PointCallback();
    void setSize(float sIn) {size.next = sIn;}
    bool run(osg::Object*, osg::Object*) override;
    osg::Geometry* buildPoint();
  protected:
    Payload<float> size;
    osg::ref_ptr<osg::Point> point;
    
    void resolveSize();
    void clean() {size.clean(); ColorCallback::clean();}
  };
  
  class ConstraintCallback : public ColorCallback
  {
  public:
    void setText(std::string_view tIn) {text.next = tIn;}
    bool run(osg::Object*, osg::Object*) override;
    osg::AutoTransform* buildConstraint1();
    osg::Group* buildConstraint2();
  protected:
    Payload<std::string> text;
    
    void clean() {text.clean(); ColorCallback::clean();}
  };
  
  class DimensionCallback : public ColorCallback
  {
  public:
    DimensionCallback(osg::Node*);
    bool run(osg::Object*, osg::Object*) override;
  };

  class BaseCallbackVisitor : public osg::NodeVisitor
  {
  public:
    BaseCallbackVisitor() : NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN){}
    void apply(osg::MatrixTransform &tIn) override
    {
      out = dynamic_cast<BaseCallback*>(tIn.getUpdateCallback());
      traverse(tIn);
    }
    BaseCallback *out = nullptr;
  };
  
  //node visitors don't look at the node that they are accepted to.
  inline BaseCallback* getBaseCallback(osg::Node *nIn)
  {
    if (BaseCallback *cb = dynamic_cast<BaseCallback*>(nIn->getUpdateCallback())) return cb;
    BaseCallbackVisitor vis;
    nIn->accept(vis);
    return vis.out;
  }
}

#endif //SKT_CALLBACK_H

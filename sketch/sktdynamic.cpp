/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>

#include <osg/Quat>

#include "sketch/sktsolver.h"
#include "sketch/sktadapter.h"
#include "sketch/sktdynamic.h"

using namespace skt;

bool Parameter::setValue(double vIn)
{
  auto oParam = solver.alterParameter(handle);
  if (!oParam) return false;
  oParam->get().val = vIn;
  return true;
}


Point::Point(Solver &sIn, const Slvs_Entity &eIn) : Typed(sIn, eIn)
{
  assert(type == SLVS_E_POINT_IN_3D || type == SLVS_E_POINT_IN_2D);
}

void Point::set(const osg::Vec3d &pIn)
{
  x().setValue(pIn.x());
  y().setValue(pIn.y());
  if (is3d()) z().setValue(pIn.z());
}

void Point::move(const osg::Vec3d &projectionIn)
{
  auto movement= toOSG() + projectionIn;
  set(movement);
}

Parameter Point::x() const
{
  auto oPoint = solver.findEntity(handle); assert(oPoint);
  auto oParameter = solver.findParameter(oPoint->get().param[0]); assert(oParameter);
  return Parameter(solver, oParameter->get());
}

Parameter Point::y() const
{
  auto oPoint = solver.findEntity(handle); assert(oPoint);
  auto oParameter = solver.findParameter(oPoint->get().param[1]); assert(oParameter);
  return Parameter(solver, oParameter->get());
}

Parameter Point::z() const
{
  assert(type == SLVS_E_POINT_IN_3D);
  auto oPoint = solver.findEntity(handle); assert(oPoint);
  auto oParameter = solver.findParameter(oPoint->get().param[1]); assert(oParameter);
  return Parameter(solver, oParameter->get());
}
SSHandles Point::getParameterHandles() const
{
  auto oPoint = solver.findEntity(handle); assert(oPoint);
  SSHandles out;
  out.push_back(oPoint->get().param[0]);
  out.push_back(oPoint->get().param[1]);
  if (type == SLVS_E_POINT_IN_3D) out.push_back(oPoint->get().param[2]);
  return out;
}

void Geometry::getHiddenConstraints(SSHandles &hsIn)
{
  hsIn.insert(hsIn.end(), hiddenConstraints.begin(), hiddenConstraints.end());
  hiddenConstraints.clear();
}

void Geometry::removeTransient()
{
  //solver doesn't care if it doesn't exist, so no check
  for (auto h : transientConstraints) solver.removeConstraint(h);
  transientConstraints.clear();
}

void Geometry::removePersistent()
{
  for (auto h : persistentConstraints) solver.removeConstraint(h);
  persistentConstraints.clear();
}

Geometry* Dot::advance() const
{
  return new Dot(solver);
}

void Dot::goPick(const Spot &sIn)
{
  switch (state)
  {
    case State::Started:
    {
      firstSpot = sIn;
      lastSpot = sIn;
      
      //build point.
      const auto &point = solver.createPoint2d(firstSpot.point.x(), firstSpot.point.y());
      typed = std::make_unique<Typed>(solver, point);
      //not doing any constraints at this time.
      
      state = State::Finished;
      break;
    }
    case State::FirstPicked:
    case State::Dragging:
    case State::Finished: {return;}
  }
}

std::optional<Point> Dot::getStartPoint() const
{
  assert(typed);
  auto dot = solver.findEntity(typed->getHandle()); assert(dot);
  return Point::fromHandle(solver, dot->get().h);
}

std::optional<Point> Dot::getFinishPoint() const
{
  return getStartPoint();
}

std::string Dot::getMessage() const
{
  return std::string("Select Position For New Point");
}

Geometry* Line::advance() const
{
  Line *out = new Line(solver);
  Spot newSpot(solver, getFinishPoint()->getHandle(), lastSpot.point);
  newSpot.setContext(lastSpot);
  out->goPick(newSpot);
  return out;
}

void Line::goPick(const Spot &sIn)
{
  switch (state)
  {
    case State::Started:
    {
      firstSpot = sIn;
      state = State::FirstPicked;
      break;
    }
    case State::FirstPicked: {return;}
    case State::Dragging:
    {
      assert(typed);
      lastSpot = sIn;
      lastSpot.goPointsCoincident(solver, getFinishPoint()->getHandle());
      lastSpot.goTangent(solver, typed->getHandle());
      state = State::Finished;
      break;
    }
    case State::Finished: {return;}
  }
}
void Line::goDrag(const osg::Vec3d &vIn)
{
  switch (state)
  {
    case State::Started: {return;}
    case State::FirstPicked:
    {
      if ((vIn - firstSpot.point).length() > std::numeric_limits<float>::epsilon())
      {
        //build points and line
        const auto &p0 = solver.createPoint2d(firstSpot.point.x(), firstSpot.point.y());
        const auto &p1 = solver.createPoint2d(vIn.x(), vIn.y());
        auto segment = solver.createLineSegment(p0, p1);
        firstSpot.goPointsCoincident(solver, p0.h);
        firstSpot.goTangent(solver, segment.h);
        typed = std::make_unique<Typed>(solver, segment);
        state = State::Dragging;
      }
      break;
    }
    case State::Dragging:
    {
      assert(typed);
      auto endPoint = getFinishPoint();
      endPoint->set(vIn);
      break;
    }
    case State::Finished: {return;}
  }
}

std::optional<Point> Line::getStartPoint() const
{
  assert(typed);
  auto line = solver.findEntity(typed->getHandle()); assert(line);
  return Point::fromHandle(solver, line->get().point[0]);
}

std::optional<Point> Line::getFinishPoint() const
{
  assert(typed);
  auto line = solver.findEntity(typed->getHandle()); assert(line);
  return Point::fromHandle(solver, line->get().point[1]);
}

std::optional<osg::Vec3d> Line::getStartTangent() const
{
  auto temp = getStartPoint()->toOSG() - getFinishPoint()->toOSG();
  temp.normalize();
  return temp;
}

std::optional<osg::Vec3d> Line::getFinishTangent() const
{
  auto temp = getFinishPoint()->toOSG() - getStartPoint()->toOSG();
  temp.normalize();
  return temp;
}

void Line::cancel()
{
  if (state < State::Dragging) return;
  assert(typed);
  firstSpot.cancel(solver);
  lastSpot.cancel(solver);
  solver.removeEntity(typed->getHandle());
  solver.removeOrphanedParameters();
}

SSHandles Line::getDontSelect() const
{
  SSHandles out;
  if (state != State::Dragging) return out;
  assert(typed);
  out.push_back(getStartPoint()->getHandle());
  out.push_back(getFinishPoint()->getHandle());
  out.push_back(typed->getHandle());
  return out;
}

SSHandles Line::getDragParameters() const
{
  SSHandles out = getStartPoint()->getParameterHandles();
  SSHandles fHandles = getFinishPoint()->getParameterHandles();
  out.insert(out.end(), fHandles.begin(), fHandles.end());
  return out;
}

std::string Line::getMessage() const
{
  if (state < State::Dragging) return std::string("Select Position For Line Start");
  return std::string("Select Position For Line Finish");
}

Geometry* Arc::advance() const
{
  Arc *out = new Arc(solver);
  Spot newSpot(solver, getFinishPoint()->getHandle(), lastSpot.point);
  newSpot.setContext(lastSpot);
  out->goPick(newSpot);
  return out;
}

void Arc::goPick(const Spot &sIn)
{
  switch (state)
  {
    case State::Started:
    {
      firstSpot = sIn;
      state = State::FirstPicked;
      break;
    }
    case State::FirstPicked: {return;}
    case State::Dragging:
    {
      assert(typed);
      lastSpot = sIn;
      lastSpot.goPointsCoincident(solver, getFinishPoint()->getHandle());
      lastSpot.goTangent(solver, typed->getHandle());
      state = State::Finished;
      break;
    }
    case State::Finished: {return;}
  }
}

void Arc::goDrag(const osg::Vec3d &vIn)
{
  switch (state)
  {
    case State::Started: {return;}
    case State::FirstPicked:
    {
      osg::Vec3d projection = vIn - firstSpot.point;
      if (projection.length() < std::numeric_limits<float>::epsilon() * 100.0) return;
      //get orientation
      orientation = deriveOrientation(projection); if (orientation == Orientation::Unknown) return;
      //I tried just using an inverse normal to get arcs to go the other way. didn't work.
      //build points of arc
      const auto &p0 = solver.createPoint2d(firstSpot.point.x(), firstSpot.point.y());
      const auto &p1 = solver.createPoint2d(vIn.x(), vIn.y());
      osg::Vec3d centerPoint = deriveCenter(projection);
      const auto &center = solver.createPoint2d(centerPoint.x(), centerPoint.y());
      
      auto arc = solver.createArcOfCircle(center, p0, p1);
      typed = std::make_unique<Typed>(solver, arc);
      if(orientation == Orientation::Reversed) flip();
      firstSpot.goPointsCoincident(solver, p0.h);
      firstSpot.goTangent(solver, arc.h);
      
      state = State::Dragging;
      break;
    }
    case State::Dragging:
    {
      assert(typed);
      osg::Vec3d projection = vIn - firstSpot.point;
      if (projection.length() < std::numeric_limits<float>::epsilon()) return;
      auto currentOrientation = deriveOrientation(projection);
      if (currentOrientation == Orientation::Unknown) return;
      
      auto setPoint = [&]()
      {
        auto endPoint = getFinishPoint();
        endPoint->set(vIn);
      };
      
      if (orientation == currentOrientation)
      {
        setPoint();
      }
      else
      {
        //flipping arc
        orientation = currentOrientation;
        auto cp = getCenterPoint();
        cp.set(deriveCenter(projection));
        flip();
        setPoint();
      }
      
      break;
    }
    case State::Finished: {return;}
  }
}

std::optional<Point> Arc::getStartPoint() const
{
  if (orientation == Orientation::Reversed) return finishPoint();
  return startPoint();
}

std::optional<Point> Arc::getFinishPoint() const
{
  if (orientation == Orientation::Reversed) return startPoint();
  return finishPoint();
}

Point Arc::getCenterPoint() const
{
  assert(typed);
  auto arc = solver.findEntity(typed->getHandle()); assert(arc);
  return Point::fromHandle(solver, arc->get().point[0]);
}

std::optional<osg::Vec3d> Arc::getStartTangent() const
{
  if (orientation == Orientation::Reversed) return finishTangent();
  return startTangent();
}

std::optional<osg::Vec3d> Arc::getFinishTangent() const
{
  if (orientation == Orientation::Reversed) return startTangent();
  return finishTangent();
}

void Arc::cancel()
{
  if (state < State::Dragging) return;
  assert(typed);
  firstSpot.cancel(solver);
  lastSpot.cancel(solver);
  solver.removeEntity(typed->getHandle());
  solver.removeOrphanedParameters();
}

SSHandles Arc::getDontSelect() const
{
  SSHandles out;
  if (state != State::Dragging) return out;
  assert(typed);
  out.push_back(getCenterPoint().getHandle());
  out.push_back(getStartPoint()->getHandle());
  out.push_back(getFinishPoint()->getHandle());
  out.push_back(typed->getHandle());
  return out;
}

SSHandles Arc::getDragParameters() const
{
  SSHandles out = getStartPoint()->getParameterHandles();
  SSHandles fHandles = getFinishPoint()->getParameterHandles();
  out.insert(out.end(), fHandles.begin(), fHandles.end());
  return out;
}

Arc::Orientation Arc::deriveOrientation(const osg::Vec3d &projection)
{
  if (!oPreviousTangent) return Orientation::Forward;
  
  auto badVector = [](const osg::Vec3d &vIn) -> bool
  {
    return vIn.isNaN() || vIn.length() < std::numeric_limits<float>::epsilon() * 100.0;
  };
  
  if (badVector(*oPreviousTangent) || badVector(projection)) {return Orientation::Unknown;}
    
  auto cross = projection ^ *oPreviousTangent;
  if (badVector(cross)) {return Orientation::Unknown;}
  if (cross.z() > 0.0) {return Orientation::Reversed;}
  if (cross.z() < 0.0) {return Orientation::Forward;}
  return Orientation::Unknown;
}

osg::Vec3d Arc::deriveCenter(const osg::Vec3d &projection)
{
  //if we have a previous tangent then we want to move center out normal to it.
  //trying to ensure center is on the correct side. Seems to be incorrect with fast movement.
  
  //just half way between first point and current cursor, if no previous curve.
  if (!oPreviousTangent) return projection * 0.5 + firstSpot.point;
  osg::Quat rotation(osg::DegreesToRadians(90.0), osg::Vec3d(0.0, 0.0, 1.0));
  if (orientation == Orientation::Reversed) rotation = osg::Quat(osg::DegreesToRadians(90.0), osg::Vec3d(0.0, 0.0, -1.0));
  osg::Vec3d tempPoint = *oPreviousTangent * projection.length();
  tempPoint = rotation * tempPoint;
  return firstSpot.point + tempPoint;
}

void Arc::flip()
{
  auto arc = solver.alterEntity(typed->getHandle()); assert(arc);
  std::swap(arc->get().point[1], arc->get().point[2]);
  firstSpot.toggleTangentOther(solver);
}

std::optional<Point> Arc::startPoint() const
{
  assert(typed);
  auto arc = solver.findEntity(typed->getHandle()); assert(arc);
  return Point::fromHandle(solver, arc->get().point[1]);
}

std::optional<Point> Arc::finishPoint() const
{
  assert(typed);
  auto arc = solver.findEntity(typed->getHandle()); assert(arc);
  return Point::fromHandle(solver, arc->get().point[2]);
}

std::optional<osg::Vec3d> Arc::startTangent() const
{
  osg::Vec3d tangent = startPoint()->toOSG() - getCenterPoint().toOSG();
  tangent.normalize();
  osg::Quat rotation(osg::DegreesToRadians(90.0), osg::Vec3d(0.0, 0.0, -1.0));
  return rotation * tangent;
}

std::optional<osg::Vec3d> Arc::finishTangent() const
{
  osg::Vec3d tangent = finishPoint()->toOSG() - getCenterPoint().toOSG();
  tangent.normalize();
  osg::Quat rotation(osg::DegreesToRadians(90.0), osg::Vec3d(0.0, 0.0, 1.0));
  return rotation * tangent;
}

std::string Arc::getMessage() const
{
  if (state < State::Dragging) return std::string("Select Position For Arc Start");
  return std::string("Select Position For Arc Finish");
}

Geometry* Bezier::advance() const
{
  Bezier *out = new Bezier(solver);
  Spot newSpot(solver, getFinishPoint()->getHandle(), lastSpot.point);
  newSpot.setContext(lastSpot);
  out->goPick(newSpot);
  return out;
}

void Bezier::goPick(const Spot &sIn)
{
  switch (state)
  {
    case State::Started:
    {
      firstSpot = sIn;
      state = State::FirstPicked;
      break;
    }
    case State::FirstPicked: {return;}
    case State::Dragging:
    {
      assert(typed);
      lastSpot = sIn;
      lastSpot.goPointsCoincident(solver, getFinishPoint()->getHandle());
      lastSpot.goTangent(solver, typed->getHandle());
      state = State::Finished;
      break;
    }
    case State::Finished: {return;}
  }
}

void Bezier::goDrag(const osg::Vec3d &vIn)
{
  switch (state)
  {
    case State::Started: {return;}
    case State::FirstPicked:
    {
      osg::Vec3d span (vIn - firstSpot.point);
      if (span.isNaN() || span.length() < std::numeric_limits<float>::epsilon() * 100.0) return;
      //build points and curve.
      const auto &first = solver.createPoint2d(firstSpot.point.x(), firstSpot.point.y());
      osg::Vec3d firstPrimePoint = span * 0.33 + firstSpot.point;
      const auto &firstPrime = solver.createPoint2d(firstPrimePoint.x(), firstPrimePoint.y());
      osg::Vec3d lastPrimePoint = span * 0.66 + firstSpot.point;
      const auto &lastPrime = solver.createPoint2d(lastPrimePoint.x(), lastPrimePoint.y());
      const auto &last = solver.createPoint2d(vIn.x(), vIn.y());
      const auto &curve = solver.createCubicBezier(first, firstPrime, lastPrime, last);
      
      //setup temporary distance constraints.
      auto pushConstraint = [&](SSHandle h)
      {
        transientConstraints.push_back(h);
        hiddenConstraints.push_back(h);
      };
      pushConstraint(solver.addPointPointDistance(firstPrimePoint.length(), first.h, firstPrime.h));
      pushConstraint(solver.addPointPointDistance(firstPrimePoint.length(), last.h, lastPrime.h));
      
      firstSpot.goPointsCoincident(solver, first.h);
      firstSpot.goTangent(solver, curve.h);
      typed = std::make_unique<Typed>(solver, curve);
      state = State::Dragging;
      break;
    }
    case State::Dragging:
    {
      assert(typed);
      
      osg::Vec3d span (vIn - firstSpot.point);
      if (span.isNaN() || span.length() < std::numeric_limits<float>::epsilon() * 100.0) return;
      //I guess just assume transient constraints.
      assert(transientConstraints.size() > 1);
      solver.updateConstraintValue(transientConstraints.front(), span.length() * 0.33);
      solver.updateConstraintValue(transientConstraints.back(), span.length() * 0.33);

      auto endPoint = getFinishPoint();
      endPoint->set(vIn);
      break;
    }
    case State::Finished: {return;}
  }
}

std::optional<Point> Bezier::getStartPoint() const
{
  assert(typed);
  auto bezier = solver.findEntity(typed->getHandle()); assert(bezier);
  return Point::fromHandle(solver, bezier->get().point[0]);
}

std::optional<Point> Bezier::getFinishPoint() const
{
  assert(typed);
  auto bezier = solver.findEntity(typed->getHandle()); assert(bezier);
  return Point::fromHandle(solver, bezier->get().point[3]);
}

Point Bezier::getStartPrimePoint() const
{
  assert(typed);
  auto bezier = solver.findEntity(typed->getHandle()); assert(bezier);
  return Point::fromHandle(solver, bezier->get().point[1]);
}

Point Bezier::getFinishPrimePoint() const
{
  assert(typed);
  auto bezier = solver.findEntity(typed->getHandle()); assert(bezier);
  return Point::fromHandle(solver, bezier->get().point[2]);
}

std::optional<osg::Vec3d> Bezier::getStartTangent() const
{
  auto temp = getStartPoint()->toOSG() - getStartPrimePoint().toOSG();
  temp.normalize();
  return temp;
}

std::optional<osg::Vec3d> Bezier::getFinishTangent() const
{
  auto temp = getFinishPoint()->toOSG() - getFinishPrimePoint().toOSG();
  temp.normalize();
  return temp;
}

void Bezier::cancel()
{
  if (state < State::Dragging) return;
  assert(typed);
  firstSpot.cancel(solver);
  lastSpot.cancel(solver);
  solver.removeEntity(typed->getHandle());
  solver.removeOrphanedParameters();
}

SSHandles Bezier::getDontSelect() const
{
  SSHandles out;
  if (state != State::Dragging) return out;
  assert(typed);
  out.push_back(typed->getHandle());
  out.push_back(getStartPoint()->getHandle());
  out.push_back(getStartPrimePoint().getHandle());
  out.push_back(getFinishPrimePoint().getHandle());
  out.push_back(getFinishPoint()->getHandle());
  return out;
}

SSHandles Bezier::getDragParameters() const
{
  SSHandles out = getStartPoint()->getParameterHandles();
  SSHandles fHandles = getFinishPoint()->getParameterHandles();
  out.insert(out.end(), fHandles.begin(), fHandles.end());
  return out;
}

std::string Bezier::getMessage() const
{
  if (state < State::Dragging) return std::string("Select Position For Bezier Start");
  return std::string("Select Position For Bezier Finish");
}

Geometry* Circle::advance() const
{
  return new Circle(solver);
}

void Circle::goPick(const Spot &sIn)
{
  switch (state)
  {
    case State::Started:
    {
      firstSpot = sIn;
      state = State::FirstPicked;
      break;
    }
    case State::FirstPicked: {return;}
    case State::Dragging:
    {
      assert(typed);
      lastSpot = sIn;
      // lastSpot.goPointsCoincident(solver, getFinishPoint()->getHandle());
      // lastSpot.goTangent(solver, typed->getHandle());
      state = State::Finished;
      break;
    }
    case State::Finished: {return;}
  }
}

void Circle::goDrag(const osg::Vec3d &vIn)
{
  switch (state)
  {
    case State::Started: {return;}
    case State::FirstPicked:
    {
      
      osg::Vec3d span (vIn - firstSpot.point);
      if (span.isNaN() || span.length() < std::numeric_limits<float>::epsilon() * 100.0) return;
      const auto &center = solver.createPoint2d(firstSpot.point.x(), firstSpot.point.y());
      const auto &distance = solver.createDistance(span.length());
      auto circle = solver.createCircle(center, distance);
      firstSpot.goPointsCoincident(solver, center.h);
      typed = std::make_unique<Typed>(solver, circle);
      state = State::Dragging;
      break;
    }
    case State::Dragging:
    {
      assert(typed);
      osg::Vec3d span (vIn - firstSpot.point);
      if (span.isNaN() || span.length() < std::numeric_limits<float>::epsilon() * 100.0) return;
      auto circle = solver.findEntity(typed->getHandle()); assert(circle);
      auto distance = solver.findEntity(circle->get().distance); assert(distance);
      auto distanceParameter = solver.alterParameter(distance->get().param[0]); assert(distanceParameter);
      distanceParameter->get().val = span.length();
      break;
    }
    case State::Finished: {return;}
  }
}

void Circle::cancel()
{
  if (state < State::Dragging) return;
  assert(typed);
  firstSpot.cancel(solver);
  lastSpot.cancel(solver);
  solver.removeEntity(typed->getHandle());
  solver.removeOrphanedParameters();
}

Point Circle::getCenterPoint() const
{
  assert(typed);
  auto circle = solver.findEntity(typed->getHandle()); assert(circle);
  return Point::fromHandle(solver, circle->get().point[0]);
}

SSHandles Circle::getDontSelect() const
{
  if (state < State::Dragging) return {};
  return {getCenterPoint().getHandle(), typed->getHandle()};
}

SSHandles Circle::getDragParameters() const
{
  if (state < State::Dragging) return {};
  auto circle = solver.findEntity(typed->getHandle()); assert(circle);
  auto distance = solver.findEntity(circle->get().distance); assert(distance);
  auto distanceParameter = solver.findParameter(distance->get().param[0]); assert(distanceParameter);
  return {distanceParameter->get().h};
}

std::string Circle::getMessage() const
{
  if (state < State::Dragging) return std::string("Select Position For Circle Center");
  return std::string("Select Position For Point On Circle");
}

void Fluid::setContext(const Context &cIn)
{
  context = cIn;
  //do anything?
}

void Fluid::goPoint()
{
  if (!curves.empty())
  {
    curves.back()->cancel();
    curves.pop_back();
  }
  state = State::point;
  curves.push_back(std::make_unique<Dot>(solver));
}

void Fluid::goLine()
{
  std::optional<Spot> spot;
  if (!curves.empty())
  {
    curves.back()->cancel(); //note: clears spot constraint handles, so following copy is valid.
    spot = curves.back()->getFirstSpot();
    curves.pop_back();
    if (curves.empty() || !curves.back()->isConnectable()) spot = std::nullopt;
  }
  state = State::line;
  curves.push_back(std::make_unique<Line>(solver));
  if (spot) curves.back()->goPick(*spot);
}

void Fluid::goArc()
{
  std::optional<Spot> spot;
  if (!curves.empty())
  {
    curves.back()->cancel();
    spot = curves.back()->getFirstSpot();
    curves.pop_back();
    if (curves.empty() || !curves.back()->isConnectable()) spot = std::nullopt;
  }
  state = State::arc;
  curves.push_back(std::make_unique<Arc>(solver));
  if (spot) curves.back()->goPick(*spot);
}

void Fluid::goCircle()
{
  if (!curves.empty())
  {
    curves.back()->cancel();
    curves.pop_back();
  }
  state = State::circle;
  curves.push_back(std::make_unique<Circle>(solver));
}

void Fluid::goBezier()
{
  std::optional<Spot> spot;
  if (!curves.empty())
  {
    curves.back()->cancel();
    spot = curves.back()->getFirstSpot();
    curves.pop_back();
    if (curves.empty() || !curves.back()->isConnectable()) spot = std::nullopt;
  }
  state = State::bezier;
  curves.push_back(std::make_unique<Bezier>(solver));
  if (spot) curves.back()->goPick(*spot);
}

void Fluid::cancel()
{
  if (!curves.empty()) curves.back()->cancel();
  removeTransient();
  curves.clear();
}

void Fluid::finish()
{
  removeTransient();
  curves.clear();
}

void Fluid::goPick(const Spot &spotIn)
{
  if (!curves.empty())
  {
    auto &c = curves.back();
    c->goPick(spotIn);
    if (context.chain && c->getState() == Geometry::State::Finished)
    {
      if (!c->getLastSpot().isPinned())
      {
        transientConstraints.push_back(solver.addWhereDragged(c->getFinishPoint()->getHandle()));
        hiddenConstraints.push_back(transientConstraints.back());
      }
      if (!curves.back()->getLastSpot().isConnected(solver)) curves.emplace_back(curves.back()->advance());
    }
  }
}

void Fluid::goDrag(const osg::Vec3d &positionIn)
{
  if (curves.empty()) return;
  auto &c = curves.back();
  if (curves.size() == 1 && c->getState() > Geometry::State::FirstPicked && c->getFirstSpot().isPinned()) //First curve is special. an existing curve isn't in curve array and we don't know orientation
  {
    std::optional<osg::Vec3d> outVec;
    auto pointHandle = c->getFirstSpot().coincident.entity;
    auto curveHandle = solver.findCurveOfEndPoint(pointHandle);
    if (curveHandle)
    {
      CurveAdapter ca(solver, *curveHandle);
      auto oe = solver.findEntity(*curveHandle); assert(oe);
      if (ca.isConnectable())
      {
        if (ca.startPointHandle && *ca.startPointHandle == pointHandle) outVec = ca.startTangent();
        if (ca.finishPointHandle && *ca.finishPointHandle == pointHandle) outVec = ca.finishTangent();
      }
      if (outVec) c->setPreviousTangent(*outVec);
      
      for (auto it = std::begin(oe->get().point); it != std::end(oe->get().point); ++it)
      {
        if (*it == 0) continue;
        auto prms = Point::fromHandle(solver, *it).getParameterHandles();
        dragParameters.insert(dragParameters.end(), prms.begin(), prms.end());
      }
      uniquefy(dragParameters);
    }
  }
  if (curves.size() > 1) c->setPreviousTangent((curves.at(curves.size() - 2))->getFinishTangent());
  c->goDrag(positionIn);
}

SSHandles Fluid::getDontSelect()
{
  if (curves.empty()) return SSHandles();
  return curves.back()->getDontSelect();
}

void Fluid::removeTransient()
{
  for (auto h : transientConstraints) solver.removeConstraint(h);
  transientConstraints.clear();
}

bool Fluid::popTransient()
{
  if (transientConstraints.empty()) return false;
  solver.removeConstraint(transientConstraints.front());
  transientConstraints.erase(transientConstraints.begin());
  return true;
}

SSHandles Fluid::getHiddenConstraints()
{
  SSHandles out = hiddenConstraints;
  for (auto &c : curves) c->getHiddenConstraints(out);
  hiddenConstraints.clear();
  return out;
}

bool Fluid::isTerminated()
{
  if (curves.empty()) return false; //what does no curves mean? assert?
  if (context.chain)
  {
    //we only return true when chaining if the user selected a point that gets constrained coincidentally.
    if (curves.back()->getLastSpot().isConnected(solver)) return true;
    return false;
  }
  return curves.back()->getState() == Geometry::State::Finished;
}

SSHandles Fluid::getDragParameters() const
{
  SSHandles out;
  if (curves.empty()) return out;
  if (curves.back()->getState() < Geometry::State::Dragging) return out;
  if (!curves.front()->getFirstSpot().isPinned() && curves.front()->isConnectable())
  {
    auto fcp = curves.front()->getStartPoint()->getParameterHandles();
    out.insert(out.end(), fcp.begin(), fcp.end());
  }
  auto lcp = curves.back()->getDragParameters();
  out.insert(out.end(), lcp.begin(), lcp.end());
  out.insert(out.end(), dragParameters.begin(), dragParameters.end());
  return out;
}

std::string Fluid::getMessage() const
{
  if (curves.empty()) return std::string("Select Entity For Creation");
  return curves.back()->getMessage();
}

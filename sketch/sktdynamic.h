/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_DYNAMIC_H
#define SKT_DYNAMIC_H

#include <optional>
#include <memory>

#include "sketch/sktsolver.h"
#include <sketch/sktadapter.h>

namespace skt
{
  struct Context
  {
    //matches default command view state
    bool chain = false;
    bool autoCoincident = true;
    bool autoTangent = true;
  };
  
  struct Target
  {
    SSHandle entity = 0;
    SSHandle constraint = 0;
    bool autoConstraint = false;
    
    bool shouldBuild() const {return autoConstraint && entity != 0 && constraint == 0;}
    bool hasEntity() const {return entity != 0;}
    bool hasConstraint() const {return constraint != 0;}
    void cancel(Solver &solver)
    {
      if (constraint != 0)
      {
        solver.removeConstraint(constraint);
        constraint = 0;
      }
      //entity is for 'other' so don't remove.
    }
  };
  
  //data from a user pick.
  struct Spot
  {
    Target coincident;
    Target tangent;
    osg::Vec3d point;
    
    Spot() = default;
    Spot(const Solver &solver, SSHandle preHighlight, const osg::Vec3d &pIn)
    {
      point = pIn;
      if (preHighlight == 0) return;
      if (isPoint(solver, preHighlight))
      {
        coincident.entity = preHighlight;
        auto curve = solver.findCurveOfEndPoint(preHighlight);
        if (curve && solver.isEntityConnectable(*curve)) tangent.entity = *curve;
      }
      if (solver.isEntityLine(preHighlight) || solver.isEntityCircular(preHighlight)) coincident.entity = preHighlight;
    }
    
    void setContext(const Context &cIn)
    {
      coincident.autoConstraint = cIn.autoCoincident;
      tangent.autoConstraint = cIn.autoTangent;
    }
    
    void setContext(const Spot &other)
    {
      coincident.autoConstraint = other.coincident.autoConstraint;
      tangent.autoConstraint = other.tangent.autoConstraint;
    }
    
    bool goPointsCoincident(Solver &solver, SSHandle other)
    {
      if (!coincident.shouldBuild()) return false;
      if (isPoint(solver, coincident.entity) && isPoint(solver, other))
      {
        coincident.constraint = solver.addPointsCoincident(coincident.entity, other);
        return true;
      }
      if (solver.isEntityLine(coincident.entity) && solver.isEntityPoint(other))
      {
        coincident.constraint = solver.addPointOnLine(other, coincident.entity);
        return true;
      }
      if (solver.isEntityCircular(coincident.entity) && solver.isEntityPoint(other))
      {
        coincident.constraint = solver.addPointOnCircle(other, coincident.entity);
        return true;
      }
      return false;
    }
    
    bool goTangent(Solver &solver, SSHandle other)
    {
      if (!tangent.shouldBuild()) return false;
      
      SSHandles lines, arcs, cubics;
      if (solver.isEntityType(tangent.entity, SLVS_E_LINE_SEGMENT)) lines.push_back(tangent.entity);
      if (solver.isEntityType(other, SLVS_E_LINE_SEGMENT)) lines.push_back(other);
      if (solver.isEntityType(tangent.entity, SLVS_E_ARC_OF_CIRCLE)) arcs.push_back(tangent.entity);
      if (solver.isEntityType(other, SLVS_E_ARC_OF_CIRCLE)) arcs.push_back(other);
      if (solver.isEntityType(tangent.entity, SLVS_E_CUBIC)) cubics.push_back(tangent.entity);
      if (solver.isEntityType(other, SLVS_E_CUBIC)) cubics.push_back(other);
      
      if (lines.size() == 1 && arcs.size() == 1)
      {
        tangent.constraint = solver.addArcLineTangent(arcs.front(), lines.front());
        return true;
      }
      if (lines.size() == 1 && cubics.size() == 1)
      {
        tangent.constraint = solver.addCubicLineTangent(cubics.front(), lines.front());
        return true;
      }
      if (arcs.size() == 1 && cubics.size() == 1)
      {
        tangent.constraint = solver.addCurveCurveTangent(cubics.front(), arcs.front());
        return true;
      }
      if (cubics.size() == 2)
      {
        tangent.constraint = solver.addCurveCurveTangent(cubics.front(), cubics.back());
        return true;
      }
      if (arcs.size() == 2)
      {
        tangent.constraint = solver.addCurveCurveTangent(arcs.front(), arcs.back());
        return true;
      }
      
      return false;
    }
    
    void toggleTangentOther(Solver &solver)
    {
      if (!tangent.hasConstraint()) return;
      
      auto toggle = [](int &o)
      {
        if (o == 0) o = 1;
        else o = 0;
      };
      
      auto tc = solver.alterConstraint(tangent.constraint); assert(tc);
      auto te = solver.findEntity(tangent.entity); assert(te);
      if (solver.isEntityType(te->get().h, SLVS_E_LINE_SEGMENT)) toggle(tc->get().other);
      else toggle(tc->get().other2); //not sure which to do for bezier.
    }
    
    void cancel(Solver &solver)
    {
      coincident.cancel(solver);
      tangent.cancel(solver);
    }
    
    bool isConnected(Solver &solver) const {return coincident.hasConstraint() && solver.isEntityPoint(coincident.entity);}
    bool isPinned() const {return coincident.hasConstraint();}
    
    static bool isPoint(const Solver &solver, SSHandle h)
    {
      if (solver.isEntityType(h, SLVS_E_POINT_IN_2D) || solver.isEntityType(h, SLVS_E_POINT_IN_3D)) return true;
      return false;
    }
  };
  
  
  
  /* We build objects from const refs to solvespace types, but we don't store the refs.
   * The Objects live in a vector in solver so references will be regularly invalid.
   * value types.
   */
  class Base
  {
  protected:
    Solver &solver;
    SSHandle handle = 0;
    SSHandle group = 0;
    
    Base(Solver &sIn, SSHandle hIn, SSHandle gIn) : solver(sIn), handle(hIn), group(gIn)
    {
      assert(isValid());
    }
    bool isValid() const {return handle > 0 && group > 0;}
  public:
    SSHandle getHandle() const {return handle;}
    SSHandle getGroup() const {return group;}
  };
  
  class Parameter : public Base
  {
  private:
    double value = 0.0;
  public:
    Parameter() = delete;
    Parameter(Solver &sIn, const Slvs_Param& pIn) : Base(sIn, pIn.h, pIn.group), value(pIn.val){}
    double getValue() const {return value;}
    bool setValue(double vIn);
  };
  
  class Typed : public Base
  {
  protected:
    int type = 0;
  public:
    int getType(){return type;}
    Typed(Solver &sIn, const Slvs_Entity &eIn) : Base(sIn, eIn.h, eIn.group), type(eIn.type){}
  };
  
  class Point : public Typed
  {
  public:
    Point(Solver&, const Slvs_Entity&);
    bool is2d(){return type == SLVS_E_POINT_IN_2D;}
    bool is3d(){return type == SLVS_E_POINT_IN_3D;}
    osg::Vec3d toOSG() const {return PointAdapter(solver, handle);}
    void set(const osg::Vec3d&);
    void move(const osg::Vec3d&);
    Parameter x() const;
    Parameter y() const;
    Parameter z() const;
    SSHandles getParameterHandles() const;
    
    static Point fromHandle(Solver &sIn, SSHandle hIn)
    {
      auto oe = sIn.findEntity(hIn); assert(oe);
      return Point(sIn, oe->get());
    }
  };
  
  /* begin reference semantics
   * I tried to avoid this Object hell.
   */
  class Geometry
  {
  public:
    enum class State
    {
      Started //empty initial state
      , FirstPicked //user has picked start point, but no curve has been created
      , Dragging //user has moved cursor enough to create a curve
      , Finished //user has made second pick and this is not longer being modified.
    };
    
    Geometry(Solver &sIn) : solver(sIn){}
    virtual ~Geometry() {removeTransient();}
    
    virtual void goPick(const Spot&) = 0;
    virtual void setPreviousTangent(std::optional<osg::Vec3d>){}
    virtual void goDrag(const osg::Vec3d&) = 0;
    virtual Geometry* advance() const = 0; //dynamically allocated. Next curve. Think chaining.
    virtual void cancel() = 0; //terminate curve and clean up everything
    virtual void reset() = 0; //remove everything except starting point and coincident.
    virtual bool isConnectable() const = 0;
    virtual SSHandles getDontSelect() const {return SSHandles();} //handles for selection to avoid.
    virtual SSHandles getDragParameters() const {return SSHandles();}
    virtual std::optional<Point> getStartPoint() const = 0;
    virtual std::optional<Point> getFinishPoint() const = 0;
    virtual std::optional<osg::Vec3d> getStartTangent() const = 0;
    virtual std::optional<osg::Vec3d> getFinishTangent() const = 0;
    virtual std::string getMessage() const {return std::string();}
    
    Geometry::State getState() const {return state;}
    const Spot& getFirstSpot() const {return firstSpot;} //so we can switch curve types while dragging.
    const Spot& getLastSpot() const {return lastSpot;} //so we can check for terminating chain.
    void getHiddenConstraints(SSHandles &); //resets internal list.
  protected:
    Solver &solver;
    std::unique_ptr<Typed> typed;
    Geometry::State state = State::Started;
    Spot firstSpot;
    Spot lastSpot;
    SSHandles transientConstraints; //temporary constraints only used during dynamic interaction.
    SSHandles hiddenConstraints; //transient constraints that we don't want the user to see.
    SSHandles persistentConstraints; //constraints left if finished but removed if cancelled.
    
    void removeTransient();
    void removePersistent();
  };
  
  //we already used point
  class Dot : public Geometry
  {
  public:
    Dot(Solver &sIn) : Geometry(sIn) {}
    Geometry* advance() const override;
    void goPick(const Spot&) override;
    void goDrag(const osg::Vec3d&) override {} //no dragging for Dot.
    bool isConnectable() const override {return false;} //yes it could be, but why would we want to?
    std::optional<Point> getStartPoint() const override;
    std::optional<Point> getFinishPoint() const override;
    std::optional<osg::Vec3d> getStartTangent() const override {return std::nullopt;}
    std::optional<osg::Vec3d> getFinishTangent() const override {return std::nullopt;}
    void cancel() override {}
    void reset() override {}
    std::string getMessage() const override;
  };
  
  class Line : public Geometry
  {
  public:
    Line(Solver &sIn) : Geometry(sIn) {}
    Geometry* advance() const override;
    void goPick(const Spot&) override;
    void goDrag(const osg::Vec3d&) override;
    bool isConnectable() const override {return true;}
    std::optional<Point> getStartPoint() const override;
    std::optional<Point> getFinishPoint() const override;
    std::optional<osg::Vec3d> getStartTangent() const override;
    std::optional<osg::Vec3d> getFinishTangent() const override;
    void cancel() override;
    void reset() override {} //TODO ?
    SSHandles getDontSelect() const override;
    SSHandles getDragParameters() const override;
    std::string getMessage() const override;
  };
  
  class Arc : public Geometry
  {
  public:
    Arc(Solver &sIn) : Geometry(sIn) {}
    Geometry* advance() const override;
    void goPick(const Spot&) override;
    void setPreviousTangent(std::optional<osg::Vec3d> tIn) override {oPreviousTangent = tIn;}
    void goDrag(const osg::Vec3d&) override;
    bool isConnectable() const override {return true;} //maybe check for coincident on first and last point
    std::optional<Point> getStartPoint() const override; //respect orientation
    std::optional<Point> getFinishPoint() const override; //respect orientation
    Point getCenterPoint() const;
    std::optional<osg::Vec3d> getStartTangent() const override; //respect orientation
    std::optional<osg::Vec3d> getFinishTangent() const override; //respect orientation
    void cancel() override;
    void reset() override {} //TODO ?
    SSHandles getDontSelect() const override;
    SSHandles getDragParameters() const override;
    std::string getMessage() const override;
  private:
    std::optional<osg::Vec3d> oPreviousTangent;
    enum class Orientation
    {
      Unknown
      , Forward //Counter Clockwise
      , Reversed //Clockwise
    };
    Orientation orientation;
    Orientation deriveOrientation(const osg::Vec3d&);
    osg::Vec3d deriveCenter(const osg::Vec3d&);
    void flip();
    std::optional<Point> startPoint() const; //ignore orientation
    std::optional<Point> finishPoint() const; //ignore orientation
    std::optional<osg::Vec3d> startTangent() const; //ignore orientation
    std::optional<osg::Vec3d> finishTangent() const; //ignore orientation
  };
  
  class Bezier : public Geometry
  {
  public:
    Bezier(Solver &sIn) : Geometry(sIn) {}
    Geometry* advance() const override;
    void goPick(const Spot&) override;
    void goDrag(const osg::Vec3d&) override;
    bool isConnectable() const override {return true;} //maybe check for coincident on first and last point
    std::optional<Point> getStartPoint() const override;
    std::optional<Point> getFinishPoint() const override;
    Point getStartPrimePoint() const;
    Point getFinishPrimePoint() const;
    std::optional<osg::Vec3d> getStartTangent() const override;
    std::optional<osg::Vec3d> getFinishTangent() const override;
    void cancel() override;
    void reset() override {} //TODO ?
    SSHandles getDontSelect() const override;
    SSHandles getDragParameters() const override;
    std::string getMessage() const override;
  };
  
  class Circle : public Geometry
  {
  public:
    Circle(Solver &sIn) : Geometry(sIn) {}
    Geometry* advance() const override;
    void goPick(const Spot&) override;
    void goDrag(const osg::Vec3d&) override;
    bool isConnectable() const override {return false;}
    std::optional<Point> getStartPoint() const override {return std::nullopt;}
    std::optional<Point> getFinishPoint() const override {return getCenterPoint();} //lie to get through fluid chain
    std::optional<osg::Vec3d> getStartTangent() const override {return std::nullopt;}
    std::optional<osg::Vec3d> getFinishTangent() const override {return std::nullopt;}
    void cancel() override;
    void reset() override {}
    SSHandles getDontSelect() const override;
    SSHandles getDragParameters() const override;
    std::string getMessage() const override;
    
    Point getCenterPoint() const;
  };
  
  //temporary name. change to dynamic once we have dynamic eliminated.
  class Fluid
  {
    Solver &solver;
    Context context;
    std::vector<std::unique_ptr<Geometry>> curves;
    SSHandles transientConstraints; //temporary constraints used during dynamic interaction.
    SSHandles hiddenConstraints; //transient constraints that we don't want the user to see.
    SSHandles dragParameters; //Used for when starting from an existing point and curve.
    enum class State
    {
      none //!< empty initial state
      , point //!< the point command is running
      , line //!< the line command is running.
      , arc //!< the arc command is running.
      , circle //!< the circle command is running.
      , bezier //!< the cubic bezier command is running.
    };
    State state;
    
  public:
    Fluid(Solver &sIn) : solver(sIn){}
    
    void setContext(const Context&);
    
    void goPoint();
    void goLine();
    void goArc();
    void goCircle();
    void goBezier();
    
    void cancel();
    void finish();
    
    void goPick(const Spot&);
    void goDrag(const osg::Vec3d&);
    
    SSHandles getDontSelect();
    void removeTransient();
    bool popTransient();
    SSHandles getHiddenConstraints(); //resets internal list.
    bool isTerminated(); //self termination.
    SSHandles getDragParameters() const;
    std::string getMessage() const;
  };
}

#endif //SKT_DYNAMIC_H

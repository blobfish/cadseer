/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sketch/sktmapping.h"

using namespace skt;

Map::ORecRef Map::getORecord(SSHandle key)
{
  for (auto &r : records) if (r.handle == key) return r;
  return std::nullopt;
}

Map::ORecRef Map::getORecord(const boost::uuids::uuid &key)
{
  for (auto &r : records) if (r.id == key) return r;
  return std::nullopt;
}

Map::ORecRef Map::getORecord(const osg::ref_ptr<osg::Node> &key)
{
  for (auto &r : records) if (r.node == key) return r;
  return std::nullopt;
}

void Map::setAllUnreferenced()
{
  for (auto &r : records) r.referenced = false;
}

void Map::removeUnreferenced()
{
  for (auto it = records.begin(); it != records.end();)
  {
    if (!it->referenced) it = records.erase(it);
    else ++it;
  }
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_MAPPING_H
#define SKT_MAPPING_H

#include <optional>
#include <functional>

#include <boost/uuid/uuid.hpp>

#include <osg/ref_ptr>
#include <osg/Node>

#include "sketch/skttypes.h"

namespace skt
{
  /*! @struct Map
   * @brief Associations between: solvespace handles, Geometry ids. openscenegraph nodes. 
   * @details I could use 1 osg::Geometry node for the whole sketch
   * or individual nodes for each entity. 1 node will
   * have performance, but be a pain to work with. See mdv::ShapeGeometryPrivate
   * Individual nodes will be much easier to work with but possible slow performance.
   * Going with individual for now. Lets don't premature optimize. Sketches should
   * be small to moderate on complexity. Not going to use boost multi index
   * for this.
   */
  struct Map
  {
    /*!@struct Record
     * @brief Associations between: solvespace handles, Geometry ids. openscenegraph nodes. 
     */
    struct Record
    {
      SSHandle handle = 0; //!< solvespace handle
      boost::uuids::uuid id; //!< id for output geometry where applicable.
      osg::ref_ptr<osg::Node> node; //!< geometry or a transform with child text
      bool referenced = false; //!< helps with finding entities automatically removed by solver.
      bool construction = false; //!< for construction geometry.
      bool hidden = false; //!< hidden constraints used during dynamic dragging.
      
      Record() = default;
      Record(SSHandle hIn, const boost::uuids::uuid &idIn, bool cIn)
      : handle(hIn), id(idIn), construction(cIn){}
    };
    std::vector<Record> records; //!< all the records for mapping
    using ORecRef = std::optional<std::reference_wrapper<Record>>;
    
    /*! @brief Get a record.
     * @param key Solvespace handle used for search.
     * @return Optional that may contain a Record reference.
     */
    ORecRef getORecord(SSHandle key);
    
    /*! @brief Get a record.
     * @param key id used for search.
     * @return Optional that may contain a Record reference.
     */
    ORecRef getORecord(const boost::uuids::uuid &key);
    
    /*! @brief Get a record.
     * @param key openscenegraph reference pointer used for search.
     * @return Optional that may contain a Record reference.
     */
    ORecRef getORecord(const osg::ref_ptr<osg::Node> &key);
    
    /*! @brief Set all records as unreferenced.
     * @details Used at the beginning of an update to keep track
     * of objects still in the solver.
     */
    void setAllUnreferenced();
    
    /*! @brief Clear records that are unreferenced.
     * @details Used at the ending of an update to keep
     * records in sync with the solver.
     */
    void removeUnreferenced();
  };
}

#endif //SKT_MAPPING_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>

#include <Geom_BezierCurve.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <TopoDS_Edge.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <IMeshTools_Parameters.hxx>
#include <BRep_Tool.hxx>
#include <TopoDS.hxx>

#include "sketch/sktnodemasks.h"
#include "sketch/sktadapter.h"
#include "sketch/sktosg.h"

using namespace skt;

osg::Geometry* skt::buildGeometry(const osg::Vec4 &colorIn, std::string_view nameIn)
{
  osg::Geometry *out = new osg::Geometry();
  out->setName(std::string(nameIn));
  out->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
  out->setDataVariance(osg::Object::DYNAMIC);
  out->setUseDisplayList(false);
  
  osg::Vec4Array *ca = new osg::Vec4Array();
  ca->push_back(colorIn);
  out->setColorArray(ca);
  out->setColorBinding(osg::Geometry::BIND_OVERALL);
  
  return out;
}

osgText::Text* skt::buildConstraintText(const std::string &text, const osg::Vec4 &colorIn)
{
  osgText::Text *out = new osgText::Text();
  out->setName("text");
  out->setFont("fonts/arial.ttf");
  out->setColor(colorIn);
  out->setAlignment(osgText::Text::CENTER_CENTER);
  out->setText(text);
  //   out->setBackdropType(osgText::Text::OUTLINE);
  //   out->setBackdropColor(osg::Vec4(1.0, 1.0, 1.0, 1.0));
  //   out->setShaderTechnique(osgText::ShaderTechnique::GREYSCALE);
  return out;
}

osg::AutoTransform* skt::buildConstraint1(const std::string &nodeName, const std::string &text, const osg::Vec4 &colorIn)
{
  osg::AutoTransform *out = new osg::AutoTransform();
  out->setName(nodeName);
  out->setNodeMask(Constraint.to_ulong());
  out->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_SCREEN);
  out->setAutoScaleToScreen(true);
  osgText::Text *hText = buildConstraintText(text, colorIn);
  out->addChild(hText);
  return out;
}

osg::Group* skt::buildConstraint2(const std::string &nodeName, const std::string &textIn, const osg::Vec4 &colorIn)
{
  osg::Group *out = new osg::Group();
  out->setName(nodeName);
  out->setNodeMask(Constraint.to_ulong());
  osgText::Text *text = buildConstraintText(textIn, colorIn);
  
  auto build = [&]()
  {
    osg::AutoTransform *t = new osg::AutoTransform();
    t->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_SCREEN);
    t->setAutoScaleToScreen(true);
    t->addChild(text);
    out->addChild(t);
  };
  build();
  build();
  return out;
}

void skt::updatePoint(osg::Geometry *geometry, const PointAdapter &p0)
{
  assert(geometry); if (!geometry) return;
  osg::Vec3Array *verts = dynamic_cast<osg::Vec3Array*>(geometry->getVertexArray()); assert(verts); if (!verts) return;
  assert(verts->size() == 1); if (verts->size() != 1) return;
  (*verts)[0] = p0;
  verts->dirty();
  geometry->dirtyBound();
}

void skt::updateLine(osg::Geometry *geometry, const PointAdapter &p0, const PointAdapter &p1)
{
  assert(geometry); if (!geometry) return;
  osg::Vec3Array *verts = dynamic_cast<osg::Vec3Array*>(geometry->getVertexArray()); assert(verts); if (!verts) return;
  verts->resize(2);
  dynamic_cast<osg::DrawArrays*>(geometry->getPrimitiveSet(0))->setCount(verts->size());
  (*verts)[0] = p0;
  (*verts)[1] = p1;
  verts->dirty();
  geometry->dirtyBound();
}

void skt::updateArcGeometry(osg::Geometry *geometry, const osg::Vec3d &ac, const osg::Vec3d &as, const osg::Vec3d &af)
{
  assert(geometry); if (!geometry) return;
  osg::Vec3Array *verts = dynamic_cast<osg::Vec3Array*>(geometry->getVertexArray());
  assert(verts);
  
  double angle = 0.0;
  osg::Vec3d r1 = as - ac;
  osg::Vec3d r2 = af - ac;
  double radius = r1.length();
  if (std::fabs(r2.length() - r1.length()) > std::numeric_limits<float>::epsilon())
  {
    std::cout << "WARNING! radei are different for arc points" << std::endl;
    return;
  }
  
  r1.normalize();
  r2.normalize();
  osg::Vec3d cross = r1 ^ r2;
  double dot = r1 * r2;
  if (std::fabs(cross.z()) < std::numeric_limits<float>::epsilon())
  {
    if (dot > 0.0)
      angle = osg::PI * 2.0;
    else
      angle = osg::PI;
  }
  else
  {
    angle = std::acos(dot);
    if (cross.z() < 0.0)
      angle = (osg::PI * 2.0) - angle;
  }
  double sides = 16.0;
  if (angle > osg::PI)
    sides = 32.0;
  verts->resizeArray(sides + 1);
  dynamic_cast<osg::DrawArrays*>(geometry->getPrimitiveSet(0))->setCount(verts->size());
  (*verts)[0] = as;
  double angleIncrement = angle / sides;
  osg::Quat rotation(angleIncrement, osg::Vec3d(0.0, 0.0, 1.0));
  osg::Vec3d currentPoint = r1;
  for (int index = 1; index < sides; ++index)
  {
    currentPoint = rotation * currentPoint;
    (*verts)[index] = currentPoint * radius + ac;
  }
  (*verts)[sides] = af;
  verts->dirty();
  geometry->dirtyBound();
}

//! @brief Update polygon representation of an bezier. No LOD.
void skt::updateBezierGeometry(osg::Geometry *g, const osg::Vec3d &p0, const osg::Vec3d &p1, const osg::Vec3d &p2, const osg::Vec3d &p3)
{
  assert(g); if (!g) return;
  
  //JFC occt sucks!
  TColgp_Array1OfPnt occtPoints(1, 4);
  auto pConvert = [](const osg::Vec3d &vIn) -> gp_Pnt
  {
    return gp_Pnt(vIn.x(), vIn.y(), vIn.z());
  };
  occtPoints.SetValue(1, pConvert(p0));
  occtPoints.SetValue(2, pConvert(p1));
  occtPoints.SetValue(3, pConvert(p2));
  occtPoints.SetValue(4, pConvert(p3));
  
  opencascade::handle<Geom_BezierCurve> c = new Geom_BezierCurve(occtPoints);
  auto edge = BRepBuilderAPI_MakeEdge(c).Edge();
  
  IMeshTools_Parameters mp;
  mp.Deflection = 0.1;
  mp.Angle = 0.25;
  mp.Relative = Standard_True;
  BRepMesh_IncrementalMesh mesh(edge, mp);
  mesh.Perform();
  assert(mesh.IsDone());
  
  TopLoc_Location location;
  const opencascade::handle<Poly_Polygon3D>& poly = BRep_Tool::Polygon3D(TopoDS::Edge(edge), location);
  if (poly.IsNull())
    return;
  //we shouldn't have any transformation.
  const TColgp_Array1OfPnt& nodes = poly->Nodes();
  
  osg::Vec3Array *verts = dynamic_cast<osg::Vec3Array*>(g->getVertexArray());
  assert(verts);
  verts->resizeArray(nodes.Length());
  
  int index = 0;
  for (const auto &p : nodes)
  {
    (*verts)[index] = osg::Vec3d(p.X(), p.Y(), p.Z());
    ++index;
  }
  dynamic_cast<osg::DrawArrays*>(g->getPrimitiveSet(0))->setCount(verts->size());
  verts->dirty();
  g->dirtyBound();
}

void skt::setColor(osg::Geometry *g, const osg::Vec4 &cIn)
{
  osg::Vec4Array *cs = dynamic_cast<osg::Vec4Array*>(g->getColorArray()); assert(cs); if(!cs) return;
  assert(cs->size() == 1); if(cs->size() != 1) return;
  (*cs)[0] = cIn;
  cs->dirty();
}

const std::pair<std::string, std::string>& skt::getConstraintTextPair(int type)
{
  static const std::pair<std::string, std::string> empty;
  static const std::map<int, std::pair<std::string, std::string>> constraintTextMap =
  {
    std::make_pair(SLVS_C_HORIZONTAL, std::make_pair("horizontal", "H"))
    , std::make_pair(SLVS_C_VERTICAL, std::make_pair("vertical", "V"))
    , std::make_pair(SLVS_C_POINTS_COINCIDENT, std::make_pair("coincidence", "C"))
    , std::make_pair(SLVS_C_PT_ON_LINE, std::make_pair("coincidence", "C"))
    , std::make_pair(SLVS_C_PT_ON_CIRCLE, std::make_pair("coincidence", "C"))
    , std::make_pair(SLVS_C_ARC_LINE_TANGENT, std::make_pair("tangent", "T"))
    , std::make_pair(SLVS_C_EQUAL_LENGTH_LINES, std::make_pair("equality", "E"))
    , std::make_pair(SLVS_C_EQUAL_RADIUS, std::make_pair("equality", "E"))
    , std::make_pair(SLVS_C_EQUAL_ANGLE, std::make_pair("equality", "E"))
    , std::make_pair(SLVS_C_SYMMETRIC_HORIZ, std::make_pair("symmetric", "S"))
    , std::make_pair(SLVS_C_SYMMETRIC_VERT, std::make_pair("symmetric", "S"))
    , std::make_pair(SLVS_C_SYMMETRIC_LINE, std::make_pair("symmetric", "S"))
    , std::make_pair(SLVS_C_PARALLEL, std::make_pair("parallel", "P"))
    , std::make_pair(SLVS_C_PERPENDICULAR, std::make_pair("perpendicular", "PP"))
    , std::make_pair(SLVS_C_AT_MIDPOINT, std::make_pair("midpoint", "M"))
    , std::make_pair(SLVS_C_WHERE_DRAGGED, std::make_pair("whereDragged", "G"))
    , std::make_pair(SLVS_C_CURVE_CURVE_TANGENT, std::make_pair("tangent", "T"))
    , std::make_pair(SLVS_C_CUBIC_LINE_TANGENT, std::make_pair("tangent", "T"))
  };
  
  //no warning about type not being in map, because that is common for dimensional constraints.
  auto it = constraintTextMap.find(type);
  if (it == constraintTextMap.end()) return empty;
  return it->second;
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_OSG_H
#define SKT_OSG_H

#include <string_view>

#include <osg/NodeVisitor>
#include <osg/Drawable>
#include <osg/Geometry>
#include <osg/AutoTransform>
#include <osg/PositionAttitudeTransform>
#include <osgText/Text>
#include <osg/Point>
#include <osg/Depth>
#include <osg/Switch>
#include <osg/LineWidth>

#include "parameter/prmparameter.h"
#include "library/lbrplabel.h"
#include "library/lbrangulardimension.h"
#include "library/lbrsketchlineardimension.h"
#include "library/lbrdiameterdimension.h"
#include "sketch/sktadapter.h"

//functions and classes to help with connection between OSG and sketch visual
namespace skt
{
  /*! @class ForceCullCallback
   * @brief Assigned to selection plane to keep it invisible to user.
   */
  class ForceCullCallback : public osg::DrawableCullCallback
  {
  public:
    bool cull(osg::NodeVisitor*, osg::Drawable*, osg::State*) const override
    {
      return true;
    }
  };
  
  /*! @brief Make a drawable invisible to user.
   * @param drawable Object to make invisible.
   */
  inline void setDrawableToAlwaysCull(osg::Drawable& drawable)
  {
    drawable.setCullCallback(new ForceCullCallback());
  }
  
  /*! @class NoBoundCallback
   * @brief Assigned to selection plane to keep large selection plane from influencing bounding operations.
   */
  class NoBoundCallback : public osg::Drawable::ComputeBoundingBoxCallback
  {
  public:
    //! Override computeBound
    osg::BoundingBox computeBound (const osg::Drawable&) const override
    {
      return osg::BoundingBox();
    }
  };
  
  /*! @brief Make a drawable have no bounding box.
   * @param drawable Object to have an invalid bound.
   */
  inline void setDrawableToNoBound(osg::Drawable& drawable)
  {
    drawable.setComputeBoundingBoxCallback(new NoBoundCallback());
  }
  
  
  /*! @brief Find a named child.
   * @param parent Object to search.
   * @param nameIn Name of child to search.
   * @return optional that may contain templated pointer.
   * @note This searches one level. Not a recursive search.
   */
  template <class T>
  std::optional<T*> getNamedChild(osg::Group *parent, std::string_view nameIn)
  {
    for (unsigned int i = 0; i < parent->getNumChildren(); ++i)
    {
      if (parent->getChild(i)->getName() == nameIn)
      {
        T* out = dynamic_cast<T*>(parent->getChild(i));
        if (out) return out;
      }
    }
    return std::nullopt;
  }
  
  /*! @brief General openscenegraph geometry construction.
   * 
   * @details General openscenegraph geometry construction.
   * Assigns a color vector using color and binding overall.
   * No vertex array is assigned.
   * Lighting is turned off.
   * Data variance is dynamic
   * @param colorIn is the color to bind overall.
   * @return General purpose openscenegraph dynamically allocated Geometry pointer.
   */
  osg::Geometry* buildGeometry(const osg::Vec4 &colorIn, std::string_view nameIn = "");
  
  /*! @brief General openscenegraph constraint text construction.
   *
   * @param text is the text to assign.
   * @param colorIn is the color of text.
   * @return Dynamically allocated Text pointer.
   */
  osgText::Text* buildConstraintText(const std::string &text, const osg::Vec4 &colorIn);
  
  /*! @brief General openscenegraph constraint construction.
   * @param nodeName Is the name  assigned to transform node.
   * @param text Is the text displayed.
   * @param colorIn is the color of text.
   * @return Dynamically allocated transform pointer.
   * @note For one visible text item.
   */
  osg::AutoTransform* buildConstraint1(const std::string &nodeName, const std::string &text, const osg::Vec4 &colorIn);
  
  /*! @brief General openscenegraph constraint construction.
   * @param nodeName Is the name  assigned to transform node.
   * @param textIn Is the text displayed.
   * @param colorIn is the color of text.
   * @return Dynamically allocated transform pointer.
   * @note For two visible text items.
   */
  osg::Group* buildConstraint2(const std::string &nodeName, const std::string &textIn, const osg::Vec4 &colorIn);
  
  /*! @brief Update point location.
   * @param geometry is the geometry responsible for display arc
   * @param p0 Is the new location of point
   */
  void updatePoint(osg::Geometry *geometry, const PointAdapter &p0);
  
  
  /*! @brief Update locations of line end points.
   * @param geometry is the geometry responsible for display arc
   * @param p0 Is the new location of start point
   * @param p1 Is the new location of finish point
   */
  void updateLine(osg::Geometry *geometry, const PointAdapter &p0, const PointAdapter &p1);
  
  
  /*! @brief Update polygon representation of an arc. Not very smart on polygon granularity.
   * @param geometry is the geometry responsible for display arc
   * @param ac Is the arc center
   * @param as Is the arc start
   * @param af Is the arc finish
   */
  void updateArcGeometry(osg::Geometry *geometry, const osg::Vec3d &ac, const osg::Vec3d &as, const osg::Vec3d &af);
  
  /*! @brief Update polygon representation of an bezier. Not very smart on polygon granularity.
   * @param geometry is the geometry responsible for display arc
   * @param p0 Is the bezier start
   * @param p1 Is the bezier start tangent
   * @param p2 Is the bezier end tangent
   * @param p3 Is the bezier end
   */
  void updateBezierGeometry(osg::Geometry *g, const osg::Vec3d &p0, const osg::Vec3d &p1, const osg::Vec3d &p2, const osg::Vec3d &p3);
  
  void setColor(osg::Geometry*, const osg::Vec4&);
  
  const std::pair<std::string, std::string>& getConstraintTextPair(int type);
  
  /*! @brief Convenience struct for accumulating osg::Vec3
   * 
   * osg::Vec3Array is dynamically allocated. assign to osg::ref_ptr.
   */
  struct Vec3Amass
  {
    Vec3Amass(){array = new osg::Vec3Array();}
    Vec3Amass(std::initializer_list<osg::Vec3> listIn)
    {
      array = new osg::Vec3Array();
      array->assign(listIn.begin(), listIn.end());
    }
    Vec3Amass& operator<< (const osg::Vec3 &vIn){array->push_back(vIn); return *this;}
    operator osg::Vec3Array*(){return array;}
    
  private:
    osg::Vec3Array *array;
  };
}

#endif //SKT_OSG_H

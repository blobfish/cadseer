/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <osg/LineStipple>
#include <osg/BlendFunc>
#include <osg/Hint>

#include "sketch/sktnodemasks.h"
#include "sketch/sktosg.h"
#include "sketch/sktcallback.h"
#include "sketch/skttrunk.h"

using namespace skt;

Trunk::Trunk()
{
  transform = new osg::MatrixTransform();
  theSwitch = new osg::Switch();
  planeGroup = new osg::Group();
  entityGroup = new osg::Group();
  constraintGroup = new osg::Group();
  dimensionGroup = new osg::Group();
  statusTextTransform = new osg::PositionAttitudeTransform();
  statusText = new osgText::Text();
  constructionWidth = new osg::LineWidth(constructionWidthValue);
  
  transform->addChild(theSwitch.get());
  theSwitch->addChild(planeGroup.get());
  theSwitch->addChild(entityGroup.get());
  theSwitch->addChild(constraintGroup.get());
  theSwitch->addChild(dimensionGroup.get());
  theSwitch->setAllChildrenOn();
  
  statusText->setName("statusText");
  statusText->setFont("fonts/arial.ttf");
  statusText->setColor(osg::Vec4(0.0, 0.0, 1.0, 1.0));
  statusText->setBackdropType(osgText::Text::OUTLINE);
  statusText->setBackdropColor(osg::Vec4(1.0, 1.0, 1.0, 1.0));
  statusText->setAlignment(osgText::Text::RIGHT_TOP);
  statusTextTransform->addChild(statusText);
  transform->addChild(statusTextTransform); //should we add underneath switch?
  
  //Some of this is for nice looking points. Didn't work.
  transform->getOrCreateStateSet()->setMode(GL_LINE_SMOOTH, osg::StateAttribute::ON);
  transform->getOrCreateStateSet()->setMode(GL_POINT_SMOOTH, osg::StateAttribute::ON);
  transform->getOrCreateStateSet()->setAttributeAndModes(new osg::Hint(GL_POINT_SMOOTH_HINT, GL_NICEST));
  osg::BlendFunc* bf = new osg::BlendFunc(osg::BlendFunc::SRC_ALPHA, osg::BlendFunc::ONE_MINUS_SRC_ALPHA); 
  transform->getOrCreateStateSet()->setAttributeAndModes(bf);
}

osg::Geometry* Trunk::buildAddEntityPoint()
{
  auto *pcb = new PointCallback();
  pcb->setColor(entityColor);
  pcb->setPrehighlightColor(preHighlightColor);
  pcb->setHighlightColor(highlightColor);
  pcb->setDefaultDepth(pointDepthValue);
  pcb->setSize(pointSize);
  osg::Geometry *out = pcb->buildPoint();
  out->setNodeMask(Entity.to_ulong());
  out->setName(std::string(pointName));
  entityGroup->addChild(out);
  return out;
}

osg::Geometry* Trunk::buildAddEntityCurve()
{
  auto *ccb = new CurveCallback();
  ccb->setColor(entityColor);
  ccb->setPrehighlightColor(preHighlightColor);
  ccb->setHighlightColor(highlightColor);
  ccb->setDefaultDepth(curveDepthValue);
  ccb->setWidth(curveWidth);
  osg::Geometry *out = ccb->buildCurve();
  out->setNodeMask(Entity.to_ulong());
  entityGroup->addChild(out);
  return out;
}

osg::AutoTransform* Trunk::buildAddConstraint1(int type)
{
  const auto &ctMap = getConstraintTextPair(type); assert(!ctMap.first.empty() && !ctMap.second.empty());
  auto *ccb = new ConstraintCallback();
  ccb->setColor(constraintColor);
  ccb->setPrehighlightColor(preHighlightColor);
  ccb->setHighlightColor(highlightColor);
  ccb->setDefaultDepth(constraintDepth);
  ccb->setText(ctMap.second);
  osg::AutoTransform *out = ccb->buildConstraint1();
  out->setName(ctMap.first);
  out->setNodeMask(Constraint.to_ulong());
  constraintGroup->addChild(out);
  return out;
}

osg::Group* Trunk::buildAddConstraint2(int type)
{
  const auto &ctMap = getConstraintTextPair(type); assert(!ctMap.first.empty() && !ctMap.second.empty());
  auto *ccb = new ConstraintCallback();
  ccb->setColor(constraintColor);
  ccb->setPrehighlightColor(preHighlightColor);
  ccb->setHighlightColor(highlightColor);
  ccb->setDefaultDepth(constraintDepth);
  ccb->setText(ctMap.second);
  osg::Group *out = ccb->buildConstraint2();
  out->setName(ctMap.first);
  out->setNodeMask(Constraint.to_ulong());
  constraintGroup->addChild(out);
  return out;
}

void Trunk::buildPlane()
{
  planeGroup->removeChildren(0, planeGroup->getNumChildren());
  
  auto buildCorners = [](float s) -> osg::Vec3Array*
  {
    return Vec3Amass({osg::Vec3(s, s, 0.0) , osg::Vec3(-s, s, 0.0), osg::Vec3(-s, -s, 0.0), osg::Vec3(s, -s, 0.0)});
  };
  
  //we don't worry about the initial size as we update it right after creation.
  double size = 1.0;
  
  osg::Vec3Array *planeCorners = buildCorners(size);
  osg::Geometry *planeQuad = buildGeometry(planeColor, planeQuadName);
  planeQuad->setNodeMask(WorkPlane.to_ulong());
  auto *planeDepth = new osg::Depth(osg::Depth::LESS, planeDepthValue, 1.00);
  planeQuad->getOrCreateStateSet()->setAttributeAndModes(planeDepth);
  planeQuad->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
  planeQuad->setNormalArray(Vec3Amass({osg::Vec3d(0.0, 0.0, 1.0)}));
  planeQuad->setNormalBinding(osg::Geometry::BIND_OVERALL);
  planeQuad->setVertexArray(planeCorners);
  planeQuad->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0 , planeCorners->size()));
  planeGroup->addChild(planeQuad);
  
  osg::LineStipple *stipple = new osg::LineStipple(2, 0xf0f0);
  osg::Geometry *planeLines = buildGeometry(osg::Vec4(0.0, 0.0, 1.0, 1.0), planeLinesName);
  planeLines->getOrCreateStateSet()->setAttributeAndModes(constructionWidth);
  planeLines->getOrCreateStateSet()->setAttributeAndModes(stipple);
  planeLines->setVertexArray(planeCorners);
  planeLines->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, 0 , planeCorners->size()));
  planeGroup->addChild(planeLines);
  
  //selection plane so we can intersect something for drawing entities.
  osg::Geometry *selectionPlane = buildGeometry(osg::Vec4(1.0, 0.0, 0.0, 1.0), selectionPlaneName);
  selectionPlane->setNodeMask(SelectionPlane.to_ulong());
  selectionPlane->setVertexArray(buildCorners(10000.0));
  selectionPlane->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0 , 4));
  setDrawableToAlwaysCull(*selectionPlane);
  setDrawableToNoBound(*selectionPlane);
  osg::AutoTransform *autoScale = new osg::AutoTransform();
  autoScale->setAutoScaleToScreen(true);
  autoScale->addChild(selectionPlane);
  theSwitch->addChild(autoScale); //separate from other work geometries.
  
  auto buildAxis = [&](const osg::Vec4 &color, std::string_view name, osg::Vec3Array *verts)
  {
    auto *ccb = new CurveCallback();
    ccb->setColor(color);
    ccb->setPrehighlightColor(preHighlightColor);
    ccb->setHighlightColor(highlightColor);
    ccb->setDefaultDepth(planeDepthValue);
    ccb->setWidth(constructionWidthValue);
    osg::Geometry *out = ccb->buildCurve();
    out->setName(std::string(name));
    out->getOrCreateStateSet()->setAttributeAndModes(stipple);
    out->setNodeMask(WorkPlaneAxis.to_ulong());
    out->setVertexArray(verts);
    out->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 0 , 2));
    planeGroup->addChild(out);
  };
  buildAxis(xAxisColor, xAxisName, Vec3Amass({osg::Vec3(-size, 0.0, 0.0), osg::Vec3(size, 0.0, 0.0)}));
  buildAxis(yAxisColor, yAxisName, Vec3Amass({osg::Vec3(0.0, -size, 0.0), osg::Vec3(0.0, size, 0.0)}));
  
  auto *pcb = new PointCallback();
  pcb->setColor(originColor);
  pcb->setPrehighlightColor(preHighlightColor);
  pcb->setHighlightColor(highlightColor);
  pcb->setDefaultDepth(pointDepthValue);
  pcb->setSize(pointSize);
  osg::Geometry *origin = pcb->buildPoint();
  origin->setNodeMask(WorkPlaneOrigin.to_ulong());
  origin->setName(std::string(originName));
  planeGroup->addChild(origin);
}

void Trunk::updatePlane(double size)
{
  auto updateVerts = [&](std::string_view name, const std::vector<osg::Vec3> &points)
  {
    auto g = getNamedChild<osg::Geometry>(planeGroup, name); assert(g); if (!g) return;
    osg::Vec3Array *pVerts = dynamic_cast<osg::Vec3Array*>((*g)->getVertexArray()); assert(pVerts);
    assert(pVerts->size() == points.size());
    pVerts->assign(points.begin(), points.end());
    pVerts->dirty();
    (*g)->dirtyBound();
  };
  updateVerts(planeQuadName, {osg::Vec3(size, size, 0.0), osg::Vec3(-size, size, 0.0), osg::Vec3(-size, -size, 0.0), osg::Vec3(size, -size, 0.0)});
  updateVerts(xAxisName, {osg::Vec3(-size, 0.0, 0.0), osg::Vec3(size, 0.0, 0.0)});
  updateVerts(yAxisName, {osg::Vec3(0.0, -size, 0.0), osg::Vec3(0.0, size, 0.0)});
  //shouldn't need to update point;
}

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_TRUNK_H
#define SKT_TRUNK_H

#include <osg/MatrixTransform>
#include <osg/Switch>
#include <osg/Group>
#include <osg/AutoTransform>
#include <osg/PositionAttitudeTransform>
#include <osg/Depth>
#include <osg/LineWidth>
#include <osg/Geometry>
#include <osgText/Text>

namespace skt
{
  //! @struct Trunk @brief as in storage trunk. A collection of osg things for visual
  struct Trunk
  {
    osg::Vec4 preHighlightColor = osg::Vec4(1.0, 1.0, 0.0, 1.0); //!< Color used for prehighlighting.
    osg::Vec4 highlightColor = osg::Vec4(1.0, 1.0, 1.0, 1.0); //!< Color used for highlighting.
    osg::Vec4 entityColor = osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f); //!< Color used for entities.
    osg::Vec4 constructionColor = osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f); //!< Color used for entities.
    osg::Vec4 constraintColor = osg::Vec4(1.0, 0.0, 0.0, 1.0); //!< Color used for entities.
    osg::Vec4 planeColor = osg::Vec4(.4, .7, .75, .5); //!< Color used for the visible sketch plane.
    osg::Vec4 xAxisColor = osg::Vec4(1.0, 0.0, 0.0, 1.0); //!< Color used for sketch plane x axis.
    osg::Vec4 yAxisColor = osg::Vec4(0.0, 1.0, 0.0, 1.0); //!< Color used for sketch plane y axis.
    osg::Vec4 originColor = osg::Vec4(0.0, 0.0, 1.0, 1.0); //!< Color used for sketch plane origin point.
    
    float planeDepthValue = 0.04;
    float constraintDepth = 0.03;
    float curveDepthValue = 0.02;
    float pointDepthValue = 0.01;
    float pointSize = 10.0;
    float curveWidth = 5.0;
    float constructionWidthValue = 1.0;
    
    std::string_view pointName = "point";
    std::string_view lineName = "line";
    std::string_view arcName = "arc";
    std::string_view circleName = "circle";
    std::string_view bezierName = "bezier";
    bool isCurveName(std::string_view n) const {return n == lineName || n == arcName || n == circleName || n == bezierName;}
    
    std::string_view planeQuadName = "planeQuad";
    std::string_view planeLinesName = "planeLines";
    std::string_view selectionPlaneName = "selectionPlane";
    std::string_view xAxisName = "xAxis";
    std::string_view yAxisName = "yAxis";
    std::string_view originName = "origin";
    
    osg::ref_ptr<osg::MatrixTransform> transform; //!< Transformation of sketch plane.
    osg::ref_ptr<osg::Switch> theSwitch; //!< Switch to control visibility of entities, constraints etc.
    osg::ref_ptr<osg::Group> planeGroup; //!< Visible plane, axes etc
    osg::ref_ptr<osg::Group> entityGroup; //!< Lines, arcs etc
    osg::ref_ptr<osg::Group> constraintGroup; //!< Constraint visuals.
    osg::ref_ptr<osg::Group> dimensionGroup; //!< Dimension visuals.
    osg::ref_ptr<osg::PositionAttitudeTransform> statusTextTransform; //!< Status text into corner of visible plane.
    osg::ref_ptr<osgText::Text> statusText; //!< Solve info
    osg::ref_ptr<osg::LineWidth> constructionWidth; //!< construction geometry width
    
    Trunk();
    osg::Geometry* buildAddEntityPoint(); //with callback
    osg::Geometry* buildAddEntityCurve(); //with callback
    osg::AutoTransform* buildAddConstraint1(int); //with callback
    osg::Group* buildAddConstraint2(int); //with callback
    void buildPlane();
    void updatePlane(double);
  };
}

#endif //SKT_TRUNK_H

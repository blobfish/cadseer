/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_TYPES_H
#define SKT_TYPES_H

#include <cstdint>
#include <vector>

namespace skt
{
  using SSHandle = uint32_t; //same type as solvespace: Slvs_hParam, Slvs_hEntity, Slvs_hConstraint, Slvs_hGroup
  using SSHandles = std::vector<SSHandle>;
  
  //! @brief The state of user interaction.
  enum class State
  {
    selection = 0 //!< no command running and user can select for future command.
    , drag //!< drag operation is commencing.
    , dynamic //!< user is creating geometry. think: line, arcs etc...
    , error //!< solver has conflicting constraints
  };
}

#endif //SKT_TYPES_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <sstream>
#include <boost/current_function.hpp>

#include <GCE2d_MakeLine.hxx>
#include <Geom2dAPI_InterCurveCurve.hxx>

#include <osg/ComputeBoundsVisitor>
#include <osgGA/GUIEventAdapter>

#include "tools/idtools.h"
#include "application/appapplication.h"
#include "selection/slcmessage.h"
#include "message/msgmessage.h"
#include "library/lbrangulardimension.h"
#include "library/lbrsketchlineardimension.h"
#include "library/lbrdiameterdimension.h"
#include "library/lbrplabel.h"
#include "library/lbrchildnamevisitor.h"
#include "project/serial/generated/prjsrlsktssketch.h"
#include "parameter/prmconstants.h"
#include "parameter/prmparameter.h"
#include "sketch/sktnodemasks.h"
#include "sketch/sktcallback.h"
#include "sketch/sktsolver.h"
#include "sketch/sktmapping.h"
#include "sketch/sktadapter.h"
#include "sketch/sktanglewrench.h"
#include "sketch/sktdynamic.h"
#include "sketch/sktosg.h"
#include "sketch/skttrunk.h"
#include "sketch/sktvisual.h"

using boost::uuids::uuid;

namespace skt
{
  /*! @struct Stow
   * @brief Private data for visual
   */
  struct Visual::Stow
  {
    Solver &solver; //!< Constraint Solver.
    
    bool autoSize = true; //!< @brief Size the visible plane automatically. @ref Sizing
    double size = 1.0; //!< @brief Radius of the visible plane. @ref Sizing
    double lastSize = 0.0; //!< @brief Last size used to update the visible plane.
    double minScaleFactor = 0.0001; //!< @brief minimum scale of size for sketch text relative to size.
    double maxScaleFactor = 0.005; //!< @brief maximum scale of size for sketch text relative to size.
    Context context;
    Trunk trunk;
    
    osg::ref_ptr<osg::Node> preHighlight; //!< object currently prehighlighted or invalid.
    std::vector<osg::ref_ptr<osg::Node>> highlights;  //!< objects currently highlighted or empty.
    
    Map eMap; //!< Entity map.
    Map cMap; //!< Constraint map.
    
    State state = State::selection; //!< Current state
    std::optional<osg::Vec3d> userDragPoint; //!< Used for user dragging. not dynamic geometry!
    osg::ref_ptr<osg::Node> userDragPick; //!< What was prehighlighted when drag started. not dynamic geometry!
    std::set<osg::ref_ptr<osg::Node>> dontSelectNodes;
    std::unique_ptr<Fluid> fluid;
    
    Stow() = delete;
    Stow(Solver &solverIn) : solver(solverIn) {}
    
    void clearDrag()
    {
      userDragPoint = std::nullopt;
      userDragPick.release();
    }
    
    void setDontSelect(const SSHandles &hsIn)
    {
      dontSelectNodes.clear();
      for (auto h : hsIn) if (auto oe = eMap.getORecord(h)) dontSelectNodes.insert(oe->get().node);
    }

    void setConstruction(Map::Record &entity)
    {
      entity.construction = true;
      if (PointCallback *pcb = dynamic_cast<PointCallback*>(entity.node->getUpdateCallback()))
        pcb->setColor(trunk.constructionColor);
      if (CurveCallback *ccb = dynamic_cast<CurveCallback*>(entity.node->getUpdateCallback()))
      {
        ccb->setColor(trunk.constructionColor);
        ccb->setWidth(trunk.constructionWidthValue);
      }
    }
    void clearConstruction(Map::Record &entity)
    {
      entity.construction = false;
      if (PointCallback *pcb = dynamic_cast<PointCallback*>(entity.node->getUpdateCallback()))
        pcb->setColor(trunk.entityColor);
      if (CurveCallback *ccb = dynamic_cast<CurveCallback*>(entity.node->getUpdateCallback()))
      {
        ccb->setColor(trunk.entityColor);
        ccb->setWidth(trunk.curveWidth);
      }
    }
    
    void updatePlaneSize()
    {
      if (!autoSize) return;
      
      //try to use boundingbox compute for a more accurate plane size.
      //boundingbox is in parent's space.
      osg::ComputeBoundsVisitor bv;
      trunk.entityGroup->accept(bv);
      osg::BoundingBox cb = bv.getBoundingBox();
      if (!cb.valid()) return; //lets try and leave the size where it is and see what happens.
        
      //we have to give a value for the radius in order for expandBy to work.
      osg::BoundingSphere bs(osg::Vec3d(), std::numeric_limits<double>::epsilon());
      bs.expandRadiusBy(cb);
      size = bs.radius();
    }
    
    //! @brief Update the visual plane to @ref size.
    void updatePlane()
    {
      if (trunk.planeGroup->getNumChildren() == 0) trunk.buildPlane();
      updatePlaneSize();
      if (std::fabs(size - lastSize) < std::numeric_limits<float>::epsilon()) return;
      lastSize = size;
      trunk.updatePlane(size);
    }
    
    void updateSolver()
    {
      //don't change state if valid before and after.
      //after an error state any dynamic or dragging will be stopped and now allowed to start
      //that happens at call sites.
      if (state == State::error)
      {
        solver.solve(solver.getGroup(), true);
        if (solver.getSys().result == SLVS_RESULT_OKAY) state = State::selection;
      }
      else
      {
        solver.solve(solver.getGroup(), true);
        if (solver.getSys().result != SLVS_RESULT_OKAY) state = State::error;
      }
    }
    
    void updateStatusMessage()
    {
      switch (state)
      {
        case State::selection:
        {
          //analyze selection and augment message?
          app::instance()->messageSlot(msg::buildStatusMessage("Select Entities And/Or Commands"));
          break;
        }
        case State::error:
        {
          app::instance()->messageSlot(msg::buildStatusMessage("Select Constraint To Remove"));
          break;
        }
        case State::drag:
        {
          //analyze context and augment message?
          app::instance()->messageSlot(msg::buildStatusMessage("Drop Entity For Position"));
          break;
        }
        case State::dynamic:
        {
          if (fluid) app::instance()->messageSlot(msg::buildStatusMessage(fluid->getMessage()));
          break;
        }
      }
    }
    
    //! @brief Update the solver status text.
    void updateText()
    {
      std::ostringstream t;
      t << solver.getResultMessage() << ". ";
      if(solver.getSys().result == SLVS_RESULT_OKAY) t << solver.getSys().dof << " DOF";
      else
      {
        auto failed = solver.getFailed();
        for (const auto &h :failed)
        {
          auto oc = solver.findConstraint(h);
          if (!oc) continue;
          //       t << oc.get().type << " ";
          t << h << " ";
        }
      }
      trunk.statusText->setText(t.str());
      
      double corner = size - size * 0.005;
      double scale = size * 0.002;
      
      trunk.statusTextTransform->setPosition(osg::Vec3d(corner, corner, 0.001));
      trunk.statusTextTransform->setScale(osg::Vec3d(scale, scale, scale));
    }
    
    void sendSolverStatus()
    {
      app::instance()->messageSlot(msg::buildStatusMessage(trunk.statusText->getText().createUTF8EncodedString(), 2.0));
    }
    
    /* problem: When we zoom in close with an orthographic camera
     * this intersection fails. Not sure why. I need to see
     * if I can duplicate this behavior in a stand alone test application.
     */
    std::optional<osg::Vec3d> getPlanePoint(const osgUtil::LineSegmentIntersector::Intersections &is)
    {
      std::optional<osg::Vec3d> out;
      for (const auto &i : is)
      {
        osg::Geometry *tg = dynamic_cast<osg::Geometry*>(i.drawable.get());
        if (tg && tg->getName() == trunk.selectionPlaneName)
        {
          assert(i.nodePath.size() > 1);
          osg::Node *noi = i.nodePath.at(i.nodePath.size() - 2);
          osg::AutoTransform *autoScale = dynamic_cast<osg::AutoTransform*>(noi); assert(autoScale);
          out = i.localIntersectionPoint * osg::Matrixd::scale(autoScale->getScale());
          break;
        }
      }
      return out;
    }
    
    /*! @brief Filter current selection for all points.
     * @param includeWork True means: include the work plane origin.
     * @note this includes the workplane origin and both 2d and 3d points.
     */
    SSHandles getSelectedPoints(bool includeWork = true)
    {
      SSHandles out;
      for (const auto &h : highlights)
      {
        if (includeWork && h->getName() == trunk.originName)
        {
          out.push_back(solver.getWPOrigin());
          continue;
        }
        auto e = eMap.getORecord(h);
        if (!e) continue;
        if (solver.isEntityPoint(e->get().handle)) out.push_back(e->get().handle);
      }
      return out;
    }
    
    /*! @brief Filter current selection for all lines.
     * @param includeWork True means: include the work plane axes.
     */
    SSHandles getSelectedLines(bool includeWork = true)
    {
      SSHandles out;
      for (const auto &h : highlights)
      {
        const std::string &n = h->getName();
        if (includeWork && n == trunk.xAxisName) {out.push_back(solver.getXAxis()); continue;}
        if (includeWork && n == trunk.yAxisName) {out.push_back(solver.getYAxis()); continue;}
        auto e = eMap.getORecord(h);
        if (!e) continue;
        if (solver.isEntityLine(e->get().handle)) out.push_back(e->get().handle);
      }
      return out;
    }
    
    //! @brief Filter current selection for all circles and arcs.
    SSHandles getSelectedCircles()
    {
      SSHandles out;
      for (const auto &h : highlights)
      {
        auto e = eMap.getORecord(h);
        if (!e) continue;
        if (solver.isEntityCircular(e->get().handle)) out.push_back(e->get().handle);
      }
      return out;
    }
    
    //! @brief Filter current selection for all arcs.
    SSHandles getSelectedArcs()
    {
      SSHandles out;
      for (const auto &h : highlights)
      {
        auto e = eMap.getORecord(h);
        if (!e) continue;
        if (solver.isEntityArc(e->get().handle)) out.push_back(e->get().handle);
      }
      return out;
    }
    
    SSHandles getSelectedBeziers()
    {
      SSHandles out;
      for (const auto &h : highlights)
      {
        auto e = eMap.getORecord(h);
        if (!e) continue;
        if (solver.isEntityBezier(e->get().handle)) out.push_back(e->get().handle);
      }
      return out;
    }
    
    /*! @brief Get rotation angle between to vectors.
     * @param v1 Start vector. Will be normalized.
     * @param v2 Finish vector. Will be normalized.
     * @return Angle in radians. [0, 2PI]
     */
    double vectorAngle(osg::Vec3d v1, osg::Vec3d v2)
    {
      v1.normalize();
      v2.normalize();
      double angle;
      osg::Vec3d cross = v1 ^ v2;
      double dot = v1 * v2;
      if (std::fabs(cross.z()) < std::numeric_limits<float>::epsilon())
      {
        if (dot > 0.0) angle = osg::PI * 2.0;
        else angle = osg::PI;
      }
      else
      {
        angle = std::acos(dot);
        if (cross.z() < 0.0) angle = (osg::PI * 2.0) - angle;
      }
      return angle;
    }
    
    /*! @brief Get a point along an entity.
     * @param h Is a handle to entity.
     * @param u Is a parameter along curve.
     * @return A point on the curve.
     * @note for an arc: u is clamped to a closed range of 0.0 to 1.0.
     * for a line: u of 0.0 start of line and u of 1.0 will be end of line.
     * However values less than 0.0 and greater than 1.0 can be used.
     */
    osg::Vec3d parameterPoint(SSHandle h, double u)
    {
      auto e = solver.findEntity(h); assert(e);
      
      if (solver.isEntityLine(h))
      {
        osg::Vec3d s = PointAdapter(solver, e->get().point[0]);
        osg::Vec3d f = PointAdapter(solver, e->get().point[1]);
        return s + ((f - s) * u);
      }
      else if (solver.isEntityArc(h))
      {
        u = std::max(u, 0.0);
        u = std::min(1.0, u);
        osg::Vec3d ac = PointAdapter(solver, e->get().point[0]);
        osg::Vec3d as = PointAdapter(solver, e->get().point[1]);
        osg::Vec3d af = PointAdapter(solver, e->get().point[2]);
        osg::Vec3d r1 = as - ac;
        osg::Vec3d r2 = af - ac;
        double angle = vectorAngle(r1, r2);
        angle *= u;
        osg::Quat rot(angle, osg::Vec3d(0.0, 0.0, 1.0));
        return (rot * r1) + ac;
      }
      else if (solver.isEntityBezier(h))
      {
        u = std::max(u, 0.0);
        u = std::min(1.0, u);
        auto project = [&](const osg::Vec3d v0, const osg::Vec3d v1) -> osg::Vec3d
        {
          return (v1 - v0) * u + v0;
        };
        
        std::array<osg::Vec3d, 4> v0 =
        {
          PointAdapter(solver, e->get().point[0])
          , PointAdapter(solver, e->get().point[1])
          , PointAdapter(solver, e->get().point[2])
          , PointAdapter(solver, e->get().point[3])
        };
        std::array<osg::Vec3d, 3> v1 =
        {
          project(v0[0], v0[1])
          , project(v0[1], v0[2])
          , project(v0[2], v0[3])
        };
        std::array<osg::Vec3d, 2> v2 =
        {
          project(v1[0], v1[1])
          , project(v1[1], v1[2])
        };
        return project(v2[0], v2[1]);
        //quit laughing.
      }
      else if (solver.isEntityCircle(h))
      {
        auto distance = solver.findEntity(e->get().distance); assert(distance);
        auto value = solver.getParameterValue(distance->get().param[0]); assert(value);
        osg::Quat rotation(u * 2.0 * osg::PI, osg::Vec3d(0.0, 0.0, 1.0));
        osg::Vec3d projection = rotation * osg::Vec3d(1.0, 0.0, 0.0);
        projection *= *value;
        osg::Vec3d location = PointAdapter(solver, e->get().point[0]);
        location += projection;
        return location;
      }
      else
      {
        assert(0); //unsupported type
        std::cout << "unsupported type " << BOOST_CURRENT_FUNCTION << std::endl;
      }
      
      return osg::Vec3d(0.0, 0.0, 0.0);
    }
    
    /*! @brief Get the center of the bounding sphere of entities.
     * @param handles Array of entity handles.
     * @return Bounding center.
     * @note this is used as a hack to place constraints
     */
    osg::Vec3d boundingCenter(const SSHandles &handles)
    {
      assert(!handles.empty());
      osg::BoundingSphere bs;
      
      for (const auto &h : handles)
      {
        auto r = eMap.getORecord(h);
        if (!r) continue; // might have selected an axis so no assert on r.
        osg::Drawable *d = dynamic_cast<osg::Drawable*>(r->get().node.get()); assert(d);
        if (!bs.valid()) bs = d->getBound();
        else bs.expandBy(d->getBound());
      };
      assert(bs.valid());
      return bs.center();
    }
    
    /*! @brief position and rotate angle dimension to lines
     * @param record is the record from the constraint map.
     */
    void placeAngularDimension(Map::Record &record)
    {
      osg::MatrixTransform *dim = dynamic_cast<osg::MatrixTransform*>(record.node.get()); assert(dim);
      auto oc = solver.findConstraint(record.handle); assert(oc);
      AngleWrench wrench(solver, {oc->get().entityA, oc->get().entityB}, true);
      dim->setMatrix(wrench.getCSys());
      if (!wrench.isValid()) return;
      
      auto *cb = dynamic_cast<lbr::AngularDimensionCallback*>(dim->getUpdateCallback()); assert(cb);
      cb->setAngleDegrees(oc->get().valA);
      cb->setRangeMask1(wrench.getFirstRangeMask());
      cb->setRangeMask2(wrench.getSecondRangeMask());
      cb->setMaxScale(lastSize * maxScaleFactor);
    }
    
    void placeLinearDimension(SSHandle ch)
    {
      auto record = cMap.getORecord(ch); assert(record);
      osg::MatrixTransform *p = dynamic_cast<osg::MatrixTransform*>(record->get().node.get()); assert(p);
      lbr::LinearDimensionCallback *cb = dynamic_cast<lbr::LinearDimensionCallback*>(p->getUpdateCallback()); assert(cb);
      
      //limit as we zoom away, but not as we zoom in.
      cb->setMaxScale(lastSize * maxScaleFactor);
      
      auto oc = solver.findConstraint(record->get().handle); assert(oc);
      osg::Vec3d point1 = PointAdapter(solver, oc->get().ptA);
      std::optional<osg::Vec3d> xaxis;
      
      if (oc->get().type == SLVS_C_PT_PT_DISTANCE)
      {
        osg::Vec3d point2 = PointAdapter(solver, oc->get().ptB);
        xaxis = point2 - point1;
        xaxis->normalize();
      }
      else if (oc->get().type == SLVS_C_PT_LINE_DISTANCE)
      {
        auto ols = solver.findEntity(oc->get().entityA); assert(ols);
        osg::Vec3d lp0 = PointAdapter(solver, ols->get().point[0]);
        osg::Vec3d lp1 = PointAdapter(solver, ols->get().point[1]);
        osg::Vec3d ld = lp1 - lp0;
        ld.normalize();
        osg::Vec3d pp = projectPointLine(lp0, ld, point1);
        xaxis = pp - point1;
        xaxis->normalize();
        
        //update range mask.
        osg::Vec3d tv0 = pp - lp0;
        osg::Vec3d tv1 = pp - lp1;
        double l0 = tv0.length();
        double l1 = tv1.length();
        tv0.normalize();
        tv1.normalize();
        if ((tv0 * tv1) > 0.0) //equal, intersection is off of line.
          cb->setRangeMask2(lbr::RangeMask(-l1, -l0));
        else //opposite, intersection is on line
          cb->setRangeMask2(lbr::RangeMask(-l1, l0));
      }
      if (xaxis)
      {
        if (oc->get().valA < 0.0) xaxis = - *xaxis;
        osg::Quat rotation;
        rotation.makeRotate(osg::Vec3d(1.0, 0.0, 0.0), *xaxis);
        osg::Matrixd transform = osg::Matrixd::rotate(rotation) * osg::Matrixd::translate(point1);
        p->setMatrix(transform);
        cb->setDistance(oc->get().valA);
      }
    }
    
    void placeDiameterDimension(SSHandle ch)
    {
      auto record = cMap.getORecord(ch); assert(record);
      osg::MatrixTransform *p = dynamic_cast<osg::MatrixTransform*>(record->get().node.get()); assert(p);
      lbr::DiameterDimensionCallback *cb = dynamic_cast<lbr::DiameterDimensionCallback*>(p->getUpdateCallback()); assert(cb);
      auto oc = solver.findConstraint(record->get().handle); assert(oc);
      auto oe = solver.findEntity(oc->get().entityA); assert(oe);
      
      cb->setMaxScale(lastSize * maxScaleFactor);
      
      auto setLocation = [&](const osg::Matrixd &r = osg::Matrixd::identity())
      {
        osg::Vec3d point = PointAdapter(solver, oe->get().point[0]);
        point.z() = 0.002;
        p->setMatrix(r * osg::Matrixd::translate(point));
      };
      
      if (solver.isEntityCircle(oc->get().entityA))
      {
        setLocation();
        cb->setDiameter(oc->get().valA);
        //no range mask for a circle.
      }
      else if (solver.isEntityArc(oc->get().entityA))
      {
        cb->setDiameter(oc->get().valA);
        
        osg::Vec3d ac = PointAdapter(solver, oe->get().point[0]);
        osg::Vec3d as = PointAdapter(solver, oe->get().point[1]);
        osg::Vec3d af = PointAdapter(solver, oe->get().point[2]);
        
        double startAngle = vectorAngle(osg::Vec3d(1.0, 0.0, 0.0), as - ac);
        osg::Quat rot(startAngle, osg::Vec3d(0.0, 0.0, 1.0));
        setLocation(osg::Matrixd(rot));
        
        cb->setRangeMask(lbr::RangeMask(0.0, vectorAngle(as - ac, af - ac)));
      }
    }
    
    bool isAlreadySelected(const osg::Node* nIn) const
    {
      if (std::find(highlights.begin(), highlights.end(), nIn) != highlights.end()) return true;
      return false;
    };
    
    void setPreHighlight()
    {
      //data prehighlight already set.
      if (preHighlight.valid())
        if (BaseCallback *cb = getBaseCallback(preHighlight.get()))
          cb->setHighlight(BaseCallback::Highlight::preHighlighted);
    }
    
    void clearPreHighlight()
    {
      if (preHighlight.valid())
      {
        if (BaseCallback *cb = getBaseCallback(preHighlight.get()))
          cb->setHighlight(BaseCallback::Highlight::none);
        preHighlight.release();
      }
    }
    
    void setHighlight(osg::Node *dIn)
    {
      if (BaseCallback *cb = getBaseCallback(dIn))
        cb->setHighlight(BaseCallback::Highlight::highlighted);
    }
    
    void clearHighlight(osg::Node *dIn)
    {
      if (BaseCallback *cb = getBaseCallback(dIn))
        cb->setHighlight(BaseCallback::Highlight::none);
    }
    
    void updateEntities()
    {
      eMap.setAllUnreferenced();
      for (const auto &e : solver.getEntities())
      {
        //understood that entites in group 1 are: constant, invisible and not in map.
        if (e.group == 1) continue;
        auto record = eMap.getORecord(e.h);
        if (!record) record = eMap.records.emplace_back(e.h, gu::createRandomId(), false);
        record->get().referenced = true;
        
        auto buildCurve = [&](std::string_view name)
        {
          record->get().node = trunk.buildAddEntityCurve();
          record->get().node->setUserValue<std::string>("id", gu::idToString(record->get().id));
          record->get().node->setName(std::string(name));
          if (record->get().construction) setConstruction(record->get());
          else clearConstruction(record->get());
        };
        
        //update entity, even new ones.
        switch (e.type)
        {
          case SLVS_E_POINT_IN_2D:
          {
            if (!record->get().node)
            {
              record->get().node = trunk.buildAddEntityPoint();
              record->get().node->setUserValue<std::string>("id", gu::idToString(record->get().id));
              if (record->get().construction) setConstruction(record->get());
              else clearConstruction(record->get());
            }
            osg::Geometry *geometry = dynamic_cast<osg::Geometry*>(record->get().node.get()); // verified in updatePoint
            skt::updatePoint(geometry, PointAdapter(solver, record->get().handle));
            break;
          }
          case SLVS_E_LINE_SEGMENT:
          {
            if (!record->get().node) buildCurve(trunk.lineName);
            osg::Geometry *geometry = dynamic_cast<osg::Geometry*>(record->get().node.get()); //verified in updateLine
            skt::updateLine(geometry, PointAdapter(solver, e.point[0]), PointAdapter(solver, e.point[1]));
            break;
          }
          case SLVS_E_ARC_OF_CIRCLE:
          {
            if (!record->get().node) buildCurve(trunk.arcName);
            osg::Geometry *geometry = dynamic_cast<osg::Geometry*>(record->get().node.get()); //verified in updateArcGeometry
            osg::Vec3d ac = PointAdapter(solver, e.point[0]);
            osg::Vec3d as = PointAdapter(solver, e.point[1]);
            osg::Vec3d af = PointAdapter(solver, e.point[2]);
            updateArcGeometry(geometry, ac, as, af);
            break;
          }
          case SLVS_E_CIRCLE:
          {
            if (!record->get().node) buildCurve(trunk.circleName);
            osg::Geometry *geometry = dynamic_cast<osg::Geometry*>(record->get().node.get()); //verified in updateArcGeometry
            osg::Vec3d center = PointAdapter(solver, e.point[0]);
            double radius = *solver.getParameterValue(solver.findEntity(e.distance)->get().param[0]);
            osg::Vec3d se = center + osg::Vec3d(1.0, 0.0, 0.0) * radius;
            updateArcGeometry(geometry, center, se, se);
            break;
          }
          case SLVS_E_CUBIC:
          {
            if (!record->get().node) buildCurve(trunk.bezierName);
            osg::Geometry *geometry = dynamic_cast<osg::Geometry*>(record->get().node.get()); //verified in updateBezierGeometry
            osg::Vec3d p0 = PointAdapter(solver, e.point[0]);
            osg::Vec3d p1 = PointAdapter(solver, e.point[1]);
            osg::Vec3d p2 = PointAdapter(solver, e.point[2]);
            osg::Vec3d p3 = PointAdapter(solver, e.point[3]);
            updateBezierGeometry(geometry, p0, p1, p2, p3);
            break;
          }
          case SLVS_E_DISTANCE:
          {
            //used for circles. don't need to anything with distances, just absorb so no warning message
            break;
          }
          default:
          {
            std::cout << "unrecognized entity of type: " << e.type << " skt::Visual::Stow::updateEntities" << std::endl;
          }
        }
      }
      //clean up unreferenced.
      for (const auto &e : eMap.records) if (!e.referenced) trunk.entityGroup->removeChild(e.node);
      eMap.removeUnreferenced();
    }
    
    void updateConstraints()
    {
      //create records for any dynamic hidden constraints, so we can set hidden attribute.
      if (fluid)
      {
        for (auto h : fluid->getHiddenConstraints())
        {
          cMap.records.push_back(Map::Record());
          auto &record = cMap.records.back();
          record.handle = h;
          record.hidden = true;
        }
      }
      
      cMap.setAllUnreferenced();
      auto setAutoMinMaxScale = [&](osg::AutoTransform *at)
      {
        assert(at); if (!at) return;
        at->setMinimumScale(lastSize * minScaleFactor);
        at->setMaximumScale(lastSize * maxScaleFactor);
      };
      for (const auto &c : solver.getConstraints())
      {
        auto record = cMap.getORecord(c.h);
        if (!record)
        {
          // add new constraint
          cMap.records.push_back(Map::Record());
          record = cMap.records.back();
          record->get().handle = c.h;
          record->get().id = gu::createRandomId();
        }
        if (record->get().hidden)
        {
          record->get().referenced = true;
          continue;
        }
        
        //build and/or update constraints.
        switch (c.type)
        {
          case SLVS_C_HORIZONTAL: case SLVS_C_VERTICAL: case SLVS_C_AT_MIDPOINT:
          {
            if (!record->get().node) record->get().node = trunk.buildAddConstraint1(c.type);
            osg::AutoTransform *p = dynamic_cast<osg::AutoTransform*>(record->get().node.get()); assert(p);
            p->setPosition(parameterPoint(solver.findConstraint(record->get().handle)->get().entityA, 0.5));
            setAutoMinMaxScale(p);
            break;
          }
          case SLVS_C_POINTS_COINCIDENT: case SLVS_C_PT_ON_LINE: case SLVS_C_PT_ON_CIRCLE: case SLVS_C_WHERE_DRAGGED:
          {
            if (!record->get().node) record->get().node = trunk.buildAddConstraint1(c.type);
            osg::AutoTransform *p = dynamic_cast<osg::AutoTransform*>(record->get().node.get()); assert(p);
            osg::Vec3d point1 = PointAdapter(solver, solver.findConstraint(record->get().handle)->get().ptA);
            p->setPosition(point1);
            setAutoMinMaxScale(p);
            break;
          }
          case SLVS_C_ARC_LINE_TANGENT: case SLVS_C_CURVE_CURVE_TANGENT: case SLVS_C_CUBIC_LINE_TANGENT:
          {
            if (!record->get().node) record->get().node = trunk.buildAddConstraint2(c.type);
            osg::Group *g = record->get().node->asGroup(); assert(g);
            osg::AutoTransform *t0 = g->getChild(0)->asTransform()->asAutoTransform(); assert(t0);
            setAutoMinMaxScale(t0);
            osg::AutoTransform *t1 = g->getChild(1)->asTransform()->asAutoTransform(); assert(t1);
            setAutoMinMaxScale(t1);
            
            auto oc = solver.findConstraint(record->get().handle); assert(oc);
            auto opp = solver.getCoincidentPoints(oc->get().entityA, oc->get().entityB);
            assert(opp); if (!opp) continue; //can't have tangent without coincident.
            
            CurveAdapter ca0(solver, oc->get().entityA);
            double prmA = 0.0;
            if (ca0.startPointHandle == opp->first) prmA = 0.1;
            if (ca0.finishPointHandle == opp->first) prmA = 0.9;
            t0->setPosition(parameterPoint(oc->get().entityA, prmA));
            
            CurveAdapter ca1(solver, oc->get().entityB);
            double prmB = 0.0;
            if (ca1.startPointHandle == opp->second) prmB = 0.1;
            if (ca1.finishPointHandle == opp->second) prmB = 0.9;
            t1->setPosition(parameterPoint(oc->get().entityB, prmB));
            break;
          }
          case SLVS_C_PT_PT_DISTANCE: case SLVS_C_PT_LINE_DISTANCE:
          {
            placeLinearDimension(record->get().handle);
            break;
          }
          case SLVS_C_EQUAL_LENGTH_LINES: case SLVS_C_EQUAL_RADIUS:
          {
            if (!record->get().node) record->get().node = trunk.buildAddConstraint2(c.type);
            auto oc = solver.findConstraint(record->get().handle); assert(oc);
            osg::Group *g = record->get().node->asGroup(); assert(g);
            
            osg::AutoTransform *t0 = g->getChild(0)->asTransform()->asAutoTransform(); assert(t0);
            t0->setPosition(parameterPoint(oc->get().entityA, 0.7));
            setAutoMinMaxScale(t0);
            
            osg::AutoTransform *t1 = g->getChild(1)->asTransform()->asAutoTransform(); assert(t1);
            t1->setPosition(parameterPoint(oc->get().entityB, 0.7));
            setAutoMinMaxScale(t1);
            break;
          }
          case SLVS_C_DIAMETER:
          {
            placeDiameterDimension(record->get().handle);
            break;
          }
          case SLVS_C_SYMMETRIC_HORIZ: case SLVS_C_SYMMETRIC_VERT: case SLVS_C_SYMMETRIC_LINE:
          {
            if (!record->get().node) record->get().node = trunk.buildAddConstraint2(c.type);
            auto oc = solver.findConstraint(record->get().handle); assert(oc);
            osg::Group *g = record->get().node->asGroup(); assert(g);
            
            osg::AutoTransform *t0 = g->getChild(0)->asTransform()->asAutoTransform(); assert(t0);
            t0->setPosition(PointAdapter(solver, oc->get().ptA));
            setAutoMinMaxScale(t0);
            
            osg::AutoTransform *t1 = g->getChild(1)->asTransform()->asAutoTransform(); assert(t1);
            t1->setPosition(PointAdapter(solver, oc->get().ptB));
            setAutoMinMaxScale(t1);
            break;
          }
          case SLVS_C_ANGLE:
          {
            placeAngularDimension(record->get());
            break;
          }
          case SLVS_C_EQUAL_ANGLE:
          {
            if (!record->get().node) record->get().node = trunk.buildAddConstraint2(c.type);
            osg::Group *g = record->get().node->asGroup(); assert(g);
            osg::AutoTransform *t0 = g->getChild(0)->asTransform()->asAutoTransform(); assert(t0);
            osg::AutoTransform *t1 = g->getChild(1)->asTransform()->asAutoTransform(); assert(t1);
            
            auto oc = solver.findConstraint(record->get().handle); assert(oc);
            
            setAutoMinMaxScale(t0);
            setAutoMinMaxScale(t1);
            
            osg::Matrixd tf = osg::Matrixd::identity();
            bool results = trunk.transform->computeWorldToLocalMatrix(tf, nullptr);
            if (results)
            {
              {
                SSHandles ehandles;
                ehandles.push_back(oc->get().entityA);
                ehandles.push_back(oc->get().entityB);
                t0->setPosition(boundingCenter(ehandles) * tf);
              }
              {
                SSHandles ehandles;
                ehandles.push_back(oc->get().entityC);
                ehandles.push_back(oc->get().entityD);
                t1->setPosition(boundingCenter(ehandles) * tf);
              }
            }
            else
              std::cout << "couldn't calculate matrix for SLVS_C_EQUAL_ANGLE, in: " << BOOST_CURRENT_FUNCTION << std::endl;
            break;
          }
          
          case SLVS_C_PARALLEL: case SLVS_C_PERPENDICULAR:
          {
            double cp = (c.type == SLVS_C_PARALLEL) ? 0.4 : 0.6;
            if (!record->get().node) record->get().node = trunk.buildAddConstraint2(c.type);
            auto oc = solver.findConstraint(record->get().handle); assert(oc);
            osg::Group *g = record->get().node->asGroup(); assert(g);
            
            osg::AutoTransform *t0 = g->getChild(0)->asTransform()->asAutoTransform(); assert(t0);
            t0->setPosition(parameterPoint(oc->get().entityA, cp));
            setAutoMinMaxScale(t0);
            
            osg::AutoTransform *t1 = g->getChild(1)->asTransform()->asAutoTransform(); assert(t1);
            t1->setPosition(parameterPoint(oc->get().entityB, cp));
            setAutoMinMaxScale(t1);
            break;
          }
        }
        
        record->get().referenced = true;
      }
      //clean up unreferenced.
      for (const auto &c : cMap.records)
      {
        if (!c.referenced)
        {
          //no side affect from removing nonexistent child.
          trunk.constraintGroup->removeChild(c.node);
          trunk.dimensionGroup->removeChild(c.node);
        }
      }
      cMap.removeUnreferenced();
    }
    
    int canDoTangent(bool commit)
    {
      if (highlights.size() < 2) return 0;
      
      SSHandles lines = getSelectedLines(false);
      SSHandles arcs = getSelectedArcs();
      SSHandles cubics = getSelectedBeziers();
      
      Solver::Chain chain;
      chain.insert(chain.end(), lines.begin(), lines.end());
      chain.insert(chain.end(), arcs.begin(), arcs.end());
      chain.insert(chain.end(), cubics.begin(), cubics.end());
      
      int tangentCount = 0;
      Solver::Chains chains = solver.getConnected(chain);
      for (const auto &ch : chains)
      {
        if (ch.size() != 2) continue; //not supporting closed arc or bezier at this point.
        if (!solver.isEntityConnectable(ch.front()) || !solver.isEntityConnectable(ch.back())) continue;
        if (solver.getTangentConstraint(ch.front(), ch.back())) continue; //already exists, don't add again.
        
        SSHandles tLines;
        SSHandles tArcs;
        SSHandles tCubics;
        if (solver.isEntityType(ch.front(), SLVS_E_LINE_SEGMENT)) tLines.push_back(ch.front());
        if (solver.isEntityType(ch.back(), SLVS_E_LINE_SEGMENT)) tLines.push_back(ch.back());
        if (solver.isEntityType(ch.front(), SLVS_E_ARC_OF_CIRCLE)) tArcs.push_back(ch.front());
        if (solver.isEntityType(ch.back(), SLVS_E_ARC_OF_CIRCLE)) tArcs.push_back(ch.back());
        if (solver.isEntityType(ch.front(), SLVS_E_CUBIC)) tCubics.push_back(ch.front());
        if (solver.isEntityType(ch.back(), SLVS_E_CUBIC)) tCubics.push_back(ch.back());
        
        if (tLines.size() == 1 && tArcs.size() == 1)
        {
          if (commit) {if (solver.addArcLineTangent(tArcs.front(), tLines.front()) != 0) tangentCount++;}
          else tangentCount++;
        }
        else if (tArcs.size() == 2)
        {
          if (commit) {if (solver.addCurveCurveTangent(tArcs.front(), tArcs.back()) != 0) tangentCount++;}
          else tangentCount++;
        }
        else if (tLines.size() == 1 && tCubics.size() == 1)
        {
          if (commit) {if (solver.addCubicLineTangent(tCubics.front(), tLines.front()) != 0) tangentCount++;}
          else tangentCount++;
        }
        else if (tCubics.size() == 1 && tArcs.size() == 1)
        {
          if (commit) {if (solver.addCurveCurveTangent(tCubics.front(), tArcs.front()) != 0) tangentCount++;}
          else tangentCount++;
        }
        else if (tCubics.size() == 2)
        {
          if (commit) {if (solver.addCurveCurveTangent(tCubics.front(), tCubics.back()) != 0) tangentCount++;}
          else tangentCount++;
        }
      }
      return tangentCount;
    }
    
    bool canDoCoincident(bool commit)
    {
      if (highlights.size() != 2) return false;
      SSHandles points = getSelectedPoints();
      SSHandles lines = getSelectedLines();
      SSHandles circles = getSelectedCircles();
      if (points.size() == 2)
      {
        if (commit) solver.addPointsCoincident(points.front(), points.back());
        return true;
      }
      if (points.size() == 1 && lines.size() == 1)
      {
        if (commit) solver.addPointOnLine(points.front(), lines.front());
        return true;
      }
      if (points.size() == 1 && circles.size() == 1)
      {
        if (commit) solver.addPointOnCircle(points.front(), circles.front());
        return true;
      }
      return false;
    }
    
    SSHandle canDoSymmetric(bool commit) //return 0, if can't do.
    {
      SSHandles lines = getSelectedLines();
      SSHandles notAxes;
      SSHandles points = getSelectedPoints(false);
      std::optional<SSHandle> xAxis;
      std::optional<SSHandle> yAxis;
      for (const auto &l : lines)
      {
        if (l == solver.getXAxis()) xAxis = l;
        else if (l == solver.getYAxis()) yAxis = l;
        else notAxes.push_back(l);
      }
      if (highlights.size() == 2 && lines.size() == 2 && notAxes.size() == 1 && xAxis)
        {if (commit) return solver.addSymmetricVertical(notAxes.front()); else return 1;}
      if (highlights.size() == 2 && lines.size() == 2 && notAxes.size() == 1 && yAxis)
        {if (commit) return solver.addSymmetricHorizontal(notAxes.front()); else return 1;}
      if (highlights.size() == 3 && points.size() == 2 && xAxis)
        {if (commit) return solver.addSymmetricVertical(points.front(), points.back()); else return 1;}
      if (highlights.size() == 3 && points.size() == 2 && yAxis)
        {if (commit) return solver.addSymmetricHorizontal(points.front(), points.back()); else return 1;}
      if (highlights.size() == 2 && notAxes.size() == 2)
        {if (commit) return solver.addSymmetricLine(notAxes.front(), notAxes.back()); else return 1;}
      if (highlights.size() == 3 && notAxes.size() == 1 && points.size() == 2)
        {if (commit) return solver.addSymmetricLine(points.front(), points.back(), notAxes.front()); else return 1;}
      return 0;
    }
  };
}

using namespace skt;

Visual::Visual(Solver &sIn) : stow(std::make_unique<Stow>(sIn)){}
Visual::~Visual() = default;

void Visual::setChainOn()
{
  stow->context.chain = true;
  if (stow->fluid) stow->fluid->setContext(stow->context);
}
void Visual::setChainOff()
{
  stow->context.chain = false;
  if (stow->fluid) stow->fluid->setContext(stow->context);
}

bool Visual::isChainOn() {return stow->context.chain;}

void Visual::setAutoCoincident(bool valueIn)
{
  stow->context.autoCoincident = valueIn;
  if (stow->fluid) stow->fluid->setContext(stow->context);
}

void Visual::setAutoTangent(bool valueIn)
{
  stow->context.autoTangent = valueIn;
  if (stow->fluid) stow->fluid->setContext(stow->context);
}

void Visual::update()
{
  stow->updateEntities();
  //going to update plane before constraints.
  //plane update will change 'lastSize' to reflect boundind size of entities.
  //then we can use 'lastSize' to scale constraints.
  stow->updatePlane();
  stow->updateText();
  stow->updateConstraints();
}

void Visual::showPlane()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.planeGroup.get(), true);
}

void Visual::hidePlane()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.planeGroup.get(), false);
}

void Visual::showEntity()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.entityGroup.get(), true);
}

void Visual::hideEntity()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.entityGroup.get(), false);
}

void Visual::showConstraint()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.constraintGroup.get(), true);
}

void Visual::hideConstraint()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.constraintGroup.get(), false);
}

void Visual::showDimensions()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.dimensionGroup.get(), true);
}

void Visual::hideDimensions()
{
  stow->trunk.theSwitch->setChildValue(stow->trunk.dimensionGroup.get(), false);
}

void Visual::showAll()
{
  showPlane();
  showEntity();
  showConstraint();
}

void Visual::hideAll()
{
  hidePlane();
  hideEntity();
  hideConstraint();
}

void Visual::setActiveSketch()
{
  stow->trunk.transform->setNodeMask(stow->trunk.transform->getNodeMask() | ActiveSketch.to_ulong());
}

void Visual::clearActiveSketch()
{
  stow->trunk.transform->setNodeMask(stow->trunk.transform->getNodeMask() & ~ActiveSketch.to_ulong());
}

void Visual::move(const osgUtil::PolytopeIntersector::Intersections &is)
{
  //We don't need to do any intersection testing, when dragging dimensions
  if (stow->state == State::drag && stow->userDragPick->getNodeMask() == Constraint.to_ulong()) return;
  
  //find and sort interesting items.
  //not really paths, just vector of nodes
  osg::NodePath workOriginNodes;
  osg::NodePath workAxisNodes;
  osg::NodePath workPlaneNodes;
  osg::NodePath pointNodes;
  osg::NodePath curveNodes;
  osg::NodePath constraintNodes;
  for (const auto &i : is)
  {
    //true boolean return terminates node depth search.
    auto apply = [&](osg::Node *target) -> bool
    {
      if (stow->isAlreadySelected(target)) return true;
      if (stow->dontSelectNodes.count(target) != 0) return true;
      auto mask = NodeMask(target->getNodeMask()); //can't switch case this.
      if (mask == WorkPlaneOrigin){workOriginNodes.push_back(target); return true;}
      if (mask == WorkPlaneAxis){workAxisNodes.push_back(target); return true;}
      if (mask == WorkPlane){workPlaneNodes.push_back(target); return true;}
      if (mask == Entity)
      {
        if (target->getName() == stow->trunk.pointName) pointNodes.push_back(target);
        else curveNodes.push_back(target);
        return true;
      }
      if (mask == Constraint){constraintNodes.push_back(target); return true;}
      return false;
    };
    for (auto rit = i.nodePath.rbegin(); rit != i.nodePath.rend(); ++rit)
      if (apply(*rit)) break;
    if (stow->state == State::drag || stow->state == State::dynamic) constraintNodes.clear();// no while dragging.
  }
  
  osg::NodePath allNodes;
  allNodes.insert(allNodes.end(), pointNodes.begin(), pointNodes.end());
  allNodes.insert(allNodes.end(), curveNodes.begin(), curveNodes.end());
  allNodes.insert(allNodes.end(), constraintNodes.begin(), constraintNodes.end());
  allNodes.insert(allNodes.end(), workOriginNodes.begin(), workOriginNodes.end());
  allNodes.insert(allNodes.end(), workAxisNodes.begin(), workAxisNodes.end());
  allNodes.insert(allNodes.end(), workPlaneNodes.begin(), workPlaneNodes.end());
  
  if (allNodes.empty()) stow->clearPreHighlight();
  else
  {
    if (stow->preHighlight != allNodes.front())
    {
      stow->clearPreHighlight();
      stow->preHighlight = allNodes.front();
      stow->setPreHighlight();
    }
  }
}

void Visual::move(const osgUtil::LineSegmentIntersector::Intersections &is)
{
  if (is.empty()) return;
  if (stow->state != State::dynamic) return;
  if (!stow->fluid) return;
  std::optional<osg::Vec3d> point = stow->getPlanePoint(is); if (!point) return;
  
  stow->fluid->goDrag(*point);
  stow->solver.dragSet(stow->fluid->getDragParameters());
  stow->updateSolver();
  update();
  //i don't like that I have to set this every move, but curve is created in move so what else?
  stow->setDontSelect(stow->fluid->getDontSelect());
  stow->updateStatusMessage();
}

void Visual::pick(const osgUtil::PolytopeIntersector::Intersections&)
{
  if (stow->state == State::selection || stow->state == State::error)
  {
    if (!stow->preHighlight.valid()) return;
    stow->highlights.push_back(stow->preHighlight);
    stow->setHighlight(stow->highlights.back().get());
    stow->preHighlight.release();
    
    Map::ORecRef oRecord;
    oRecord = stow->eMap.getORecord(stow->highlights.back());
    if (oRecord) //determine if we can chain this type.
    {
      auto h = oRecord->get().handle;
      if (!stow->solver.isEntityConnectable(h)) oRecord = std::nullopt;
    }
    
    if (stow->context.chain && oRecord)
    {
      const Map::Record &record = oRecord->get();
      Solver::Chains chains = stow->solver.getChains();
      std::optional<Solver::Chain> resultChain;
      for (const auto &c : chains)
      {
        for (auto h : c) if (h == record.handle) resultChain = c;
        if (resultChain) break;
      }
      
      if (resultChain)
      {
        for (auto h : *resultChain)
        {
          auto resultRecord = stow->eMap.getORecord(h); assert(resultRecord); if (!resultRecord) continue;
          if (stow->isAlreadySelected(resultRecord->get().node)) continue;
          stow->highlights.push_back(resultRecord->get().node->asDrawable());
          stow->setHighlight(stow->highlights.back().get());
        }
      }
    }
    
    app::instance()->messageSlot(msg::Message(msg::Response | msg::Sketch | msg::Selection | msg::Add, slc::Message()));
  }
}

void Visual::pick(const osgUtil::LineSegmentIntersector::Intersections &is)
{
  if (is.empty()) return;
  if (stow->state != State::dynamic) return;
  if (!stow->fluid) return;
  std::optional<osg::Vec3d> point = stow->getPlanePoint(is); if (!point) return;
  
  SSHandle preHandle = 0;
  if (stow->preHighlight.valid())
  {
    //work plane origin is not in eMap, but we want to use it.
    if (stow->preHighlight->getName() == "origin")
    {
      preHandle = stow->solver.getWPOrigin();
    }
    else
    {
      auto oe = stow->eMap.getORecord(stow->preHighlight.get());
      if (oe) preHandle = oe->get().handle;
    }
  }
  Spot spot(stow->solver, preHandle, *point);
  spot.setContext(stow->context);
  stow->fluid->goPick(spot);
  stow->updateSolver();
  
  //if we have a failed solver lets remove the transient constraints in an effort to keep going.
  while(stow->state == State::error)
  {
    if (!stow->fluid->popTransient()) break;
    stow->updateSolver();
  }
  // stow->update sets state to either selection or error. We don't want selection. we want dynamic here.
  if (stow->state == State::selection) stow->state = State::dynamic;
  update();
  stow->sendSolverStatus();
  
  auto goFinish =[&]()
  {
    stow->fluid->finish();
    stow->fluid.reset(nullptr);
    stow->dontSelectNodes.clear();
    stow->solver.solve(stow->solver.getGroup(), true); //don't call stow version as it may modify status.
    update();
    stow->sendSolverStatus();
    app::instance()->messageSlot(msg::Message(msg::Response | msg::Sketch | msg::Shape | msg::Done));
  };
  if (stow->state == State::error) goFinish();
  else if (stow->fluid->isTerminated()) {stow->state = State::selection; goFinish();}
  stow->updateStatusMessage();
}

void Visual::startDrag(const osgUtil::LineSegmentIntersector::Intersections &is)
{
  if (stow->state == State::error) return;
  if (!stow->preHighlight.valid()) return;
  auto op = stow->getPlanePoint(is); if (!op) return;
  stow->userDragPick = stow->preHighlight;
  stow->userDragPoint = *op;
  stow->clearPreHighlight();
  stow->state = State::drag;
  
  //setup dont select
  auto oe = stow->eMap.getORecord(stow->userDragPick); if (!oe) return;
  if (stow->solver.isEntityPoint2d(oe->get().handle)) stow->setDontSelect(stow->solver.getNexus(oe->get().handle));
  else if (stow->solver.isEntityCurve(oe->get().handle))
  {
    auto ds = stow->solver.getCurvePoints(oe->get().handle);
    ds.push_back(oe->get().handle);
    stow->setDontSelect(ds);
  }
  stow->updateStatusMessage();
}

/*! @brief Response to a mouse drag.
 * 
 * @note I did some profiling to see if I can figure out why 
 * updating feels sluggish when dragging. The time to run solver
 * was similar to freecad, solvespace doesn't show time to solve by default.
 * The time to visual update looked insignificant. 
 * I tried different threading models for the viewer. didn't help.
 * I am convinced that the solver is working efficiently and the 
 * event system queue is being flooded. I tried clearing the osg event
 * queue, but that didn't help. Buffering must happen above. Perhaps
 * X11 for the prototype.
 */
void Visual::drag(const osgUtil::LineSegmentIntersector::Intersections &is)
{
  if (stow->state != State::drag) return;
  if (!stow->userDragPick.valid()) return;
  auto op = stow->getPlanePoint(is); if (!op) return;
  
  //drag entities.
  auto record = stow->eMap.getORecord(stow->userDragPick);
  if (record)
  {
    osg::Vec3d projection = *op - *stow->userDragPoint;
    if (projection.length() < std::numeric_limits<float>::epsilon() * 100.0) return;
    SSHandle ph = record->get().handle;
    auto oe = stow->solver.findEntity(ph); assert(oe);
    std::vector<Slvs_hParam> draggedParameters;
    
    auto movePoint = [&](SSHandle pIn)
    {
      auto oe = stow->solver.findEntity(pIn); assert(oe);
      Point point(stow->solver, *oe);
      point.move(projection);
      auto temp = point.getParameterHandles();
      draggedParameters.insert(draggedParameters.end(), temp.begin(), temp.end());
    };
    
    if (stow->solver.isEntityType(ph, SLVS_E_POINT_IN_2D))
    {
      movePoint(oe->get().h);
    }
    if (stow->solver.isEntityType(ph, SLVS_E_LINE_SEGMENT))
    {
      movePoint(oe->get().point[0]);
      movePoint(oe->get().point[1]);
    }
    if (stow->solver.isEntityType(ph, SLVS_E_ARC_OF_CIRCLE))
    {
      movePoint(oe->get().point[0]);
      movePoint(oe->get().point[1]);
      movePoint(oe->get().point[2]);
    }
    if (stow->solver.isEntityType(ph, SLVS_E_CIRCLE))
    {
      auto oc = stow->solver.findEntity(ph); assert(oc);
      
      osg::Vec3d center = PointAdapter(stow->solver, oc->get().point[0]);
      osg::Vec3d rv = *op - center; //radius vector
      rv.normalize();
      osg::Vec3d pp = projectPointLine(center, rv, *stow->userDragPoint);
      osg::Vec3d prv = *op - pp; // projected radius vector
      double adjustment = prv.length();
      prv.normalize();
      if ((prv - rv).length() > std::numeric_limits<float>::epsilon())
        adjustment *= -1.0;
      
      auto od = stow->solver.findEntity(oc->get().distance); assert(od);
      auto opv = stow->solver.getParameterValue(od->get().param[0]);
      stow->solver.setParameterValue(od->get().param[0], *opv + adjustment);
      draggedParameters.push_back(od->get().param[0]);
    }
    if (stow->solver.isEntityType(ph, SLVS_E_CUBIC))
    {
      movePoint(oe->get().point[0]);
      movePoint(oe->get().point[1]);
      movePoint(oe->get().point[2]);
      movePoint(oe->get().point[3]);
    }
    
    stow->solver.dragSet(draggedParameters);
    stow->updateSolver();
    update();
  }
  else if (auto t = dynamic_cast<osg::MatrixTransform*>(stow->userDragPick.get())) //drag dimensions
  {
    auto position = *op * osg::Matrix::inverse(t->getMatrix());
    if (auto *cb = dynamic_cast<lbr::LinearDimensionCallback*>(t->getUpdateCallback()))
      cb->setTextLocation(position);
    if (auto *cb = dynamic_cast<lbr::AngularDimensionCallback*>(t->getUpdateCallback()))
      cb->setTextLocation(position);
    if (auto *cb = dynamic_cast<lbr::DiameterDimensionCallback*>(t->getUpdateCallback()))
      cb->setTextLocation(position);
    //yeah I know that sucks!!
  }
  
  stow->userDragPoint = *op;
}

//! @brief Response to ending of a mouse drag.
void Visual::finishDrag(const osgUtil::LineSegmentIntersector::Intersections&)
{
  Map::ORecRef oDragRec, oDropRec;
  if
  (
    stow->preHighlight.valid()
    && stow->context.autoCoincident
    && (oDragRec = stow->eMap.getORecord(stow->userDragPick))
    && (oDropRec = stow->eMap.getORecord(stow->preHighlight))
  )
  {
    //now we know to have at least autoCoincident and 2 entities.
    Spot spot(stow->solver, oDropRec->get().handle, osg::Vec3d()); //osg::Vec3d doesn't matter here.
    spot.setContext(stow->context);
    if (spot.goPointsCoincident(stow->solver, oDragRec->get().handle))
      app::instance()->messageSlot(msg::buildStatusMessage("Coincident Added", 2.0));
    auto oDropCurve = stow->solver.findCurveOfEndPoint(oDragRec->get().handle);
    if (oDropCurve && spot.goTangent(stow->solver, *oDropCurve))
      app::instance()->messageSlot(msg::buildStatusMessage("Tangent Added", 2.0));
    stow->updateSolver();
    update();
    stow->sendSolverStatus();
  }
  
  stow->state = State::selection;
  stow->clearDrag();
  stow->dontSelectNodes.clear();
  stow->updateStatusMessage();
}

void Visual::leftDoubleClick(const osgUtil::PolytopeIntersector::Intersections &is)
{
  //note: proper state was verified in selection before calling this function.
  //can't do this without the intersections. I tried.
  //  Prehighlight is cleared and highlights.back() might not be where double click happened.
  //just going to clear all selection. Not getting into keeping the parameter table selection
  // in sync with the visual selection in the 3d view.
  
  lbr::PLabel *label = nullptr;
  auto search = [&]()
  {
    for (const auto &i : is)
      for (auto rit = i.nodePath.rbegin(); rit != i.nodePath.rend(); ++rit)
        if ((*rit)->getName() == "lbr::PLabel")
        {
          label = dynamic_cast<lbr::PLabel*>(*rit);
          return;
        }
  };
  search();
  if (!label) return;
  
  //good to go.
  stow->clearPreHighlight();
  clearSelection();
  slc::Message sm;
  sm.shapeId = label->getParameter()->getId(); //lie: parameter id, not shape id.
  //send message to command view. Weird osg crash without queue.
  app::instance()->queuedMessage(msg::Message(msg::Request | msg::Sketch | msg::Edit | msg::Parameter, sm));
}

//! @brief handle hot keys
bool Visual::handleKey(int key)
{
  /* I messed up when I put the hot keys for the 3d view inside the CamManipulator.
   * There is no easy way to make these hot keys override those, so both behaviors
   * are being triggered for keys used in both. This is very confusing to the user
   * so we will disable these hot keys for now.
   */
  return false;
  
  //don't do anything while dragging.
  if (stow->state == State::drag) return false;
  
  //future: filter hot key for commands:
  //remove
  //remove last conflicting constraint
  if (stow->state == State::error) return false;
  
  
  //command view can evaluate controls because after adding constraints we clear the selection
  //and that message is sent to the command view. FYI.
  using SYM = osgGA::GUIEventAdapter::KeySymbol;
  switch (key)
  {
    case SYM::KEY_C:
    {
      if (stow->state == State::selection)
      {
        addCoincident();
        return true;
      }
      else
      {
        app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Toggle | msg::Auto | msg::Coincident));
        return true;
      }
      break;
    }
    case SYM::KEY_T:
    {
      if (stow->state == State::selection)
      {
        addTangent();
        return true;
      }
      else
      {
        app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Toggle | msg::Auto | msg::Tangent));
        return true;
      }
      break;
    }
    //just send request to command view and let it determine action based upon state?
    case SYM::KEY_P:
    {
      app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Point));
      break;
    }
    case SYM::KEY_L:
    {
      app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Line));
      break;
    }
    case SYM::KEY_A:
    {
      app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Arc));
      break;
    }
    case SYM::KEY_I:
    {
      app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Circle));
      break;
    }
    case SYM::KEY_B:
    {
      app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Bezier));
      break;
    }
    case SYM::KEY_H:
    {
      if (stow->state == State::selection)
      {
        addHorizontal();
        return true;
      }
      //maybe in line state we add the constraint the current dynamic line? TODO
      break;
    }
    case SYM::KEY_V:
    {
      if (stow->state == State::selection)
      {
        addVertical();
        return true;
      }
      //maybe in chain we add constraint the current dynamic line? TODO
      break;
    }
    case SYM::KEY_E:
    {
      if (stow->state == State::selection)
      {
        addEqual();
        return true;
      }
      break;
    }
    case SYM::KEY_R:
    {
      if (stow->state == State::selection)
      {
        app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Remove));
        return true;
      }
      break;
    }
    case SYM::KEY_X:
    {
      if (stow->state != State::selection && stow->state != State::drag)
      {
        app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::Command | msg::Done));
        return true;
      }
      break;
    }
    case SYM::KEY_F:
    {
      app::instance()->messageSlot(msg::Message(msg::Request | msg::Sketch | msg::View | msg::Top));
      return true;
      break;
    }
    default:{return false; break;}
  }
  return false;
}

void Visual::clearSelection()
{
  for (auto d : stow->highlights) stow->clearHighlight(d.get());
  stow->highlights.clear();
  app::instance()->messageSlot(msg::Message(msg::Response | msg::Sketch | msg::Selection | msg::Clear));
}

void Visual::setPreHighlightColor(const osg::Vec4 &cIn) {stow->trunk.preHighlightColor = cIn;}
osg::Vec4 Visual::getPreHighlightColor() {return stow->trunk.preHighlightColor;}
void Visual::setHighlightColor(const osg::Vec4 &cIn) {stow->trunk.highlightColor = cIn;}
osg::Vec4 Visual::getHighlightColor() {return stow->trunk.highlightColor;}

void Visual::addPoint()
{
  if (stow->state == State::error) return;
  stow->state = State::dynamic;
  stow->trunk.statusText->setText("Point");
  clearSelection();
  if (!stow->fluid)
  {
    stow->fluid = std::make_unique<Fluid>(stow->solver);
    stow->fluid->setContext(stow->context);
  }
  stow->fluid->goPoint();
  stow->updateStatusMessage();
}

void Visual::addLine()
{
  if (stow->state == State::error) return;
  stow->state = State::dynamic;
  stow->trunk.statusText->setText("Line");
  clearSelection();
  if (!stow->fluid)
  {
    stow->fluid = std::make_unique<Fluid>(stow->solver);
    stow->fluid->setContext(stow->context);
  }
  stow->fluid->goLine();
  stow->updateStatusMessage();
}

void Visual::addArc()
{
  if (stow->state == State::error) return;
  stow->state = State::dynamic;
  stow->trunk.statusText->setText("Arc");
  clearSelection();
  if (!stow->fluid)
  {
    stow->fluid = std::make_unique<Fluid>(stow->solver);
    stow->fluid->setContext(stow->context);
  }
  stow->fluid->goArc();
  stow->updateStatusMessage();
}

void Visual::addCircle()
{
  if (stow->state == State::error) return;
  stow->state = State::dynamic;
  stow->trunk.statusText->setText("Circle");
  clearSelection();
  if (!stow->fluid)
  {
    stow->fluid = std::make_unique<Fluid>(stow->solver);
    stow->fluid->setContext(stow->context);
  }
  stow->fluid->goCircle();
  stow->updateStatusMessage();
}

void Visual::addCubicBezier()
{
  if (stow->state == State::error) return;
  stow->state = State::dynamic;
  stow->trunk.statusText->setText("Bezier");
  clearSelection();
  if (!stow->fluid)
  {
    stow->fluid = std::make_unique<Fluid>(stow->solver);
    stow->fluid->setContext(stow->context);
  }
  stow->fluid->goBezier();
  stow->updateStatusMessage();
}

bool Visual::canCoincident()
{
  if (stow->state == State::error) return false;
  return stow->canDoCoincident(false);
}

void Visual::addCoincident()
{
  if (stow->state == State::error) return;
  auto results = stow->canDoCoincident(true);
  clearSelection();
  if (!results)
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Coincident Rejected", 2.0));
    return;
  }
  
  stow->updateSolver();
  app::instance()->messageSlot(msg::buildStatusMessage("Coincident Added", 2.0));
  update();
  stow->sendSolverStatus();
}

bool Visual::canHorizontal()
{
  if (stow->state == State::error) return false;
  SSHandles lines = stow->getSelectedLines(false);
  if (stow->highlights.size() != lines.size() || lines.empty()) return false;
  return true;
}

void Visual::addHorizontal()
{
  if (stow->state == State::error) return;
  SSHandles lines = stow->getSelectedLines(false);
  if (stow->highlights.size() != lines.size() || lines.empty())
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Horizontal Rejected", 2.0));
    return;
  }
  
  for (const auto &l : lines)
  {
    stow->solver.addHorizontal(l);
    app::instance()->messageSlot(msg::buildStatusMessage("Horizontal Added", 2.0));
  }
  stow->updateSolver();
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canVertical()
{
  if (stow->state == State::error) return false;
  SSHandles lines = stow->getSelectedLines(false);
  if (stow->highlights.size() != lines.size() || lines.empty()) return false;
  return true;
}

//! @brief Add a vertical constraint to each selected line segment.
void Visual::addVertical()
{
  if (stow->state == State::error) return;
  SSHandles lines = stow->getSelectedLines(false);
  if (stow->highlights.size() != lines.size() || lines.empty())
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Vertical Rejected", 2.0));
    return;
  }
  for (const auto &l : lines)
  {
    stow->solver.addVertical(l);
    app::instance()->messageSlot(msg::buildStatusMessage("Vertical Added", 2.0));
  }
  stow->updateSolver();
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canTangent()
{
  if (stow->state == State::error) return false;
  return stow->canDoTangent(false) != 0;
}

void Visual::addTangent()
{
  if (stow->state == State::error) return;
  auto count = stow->canDoTangent(true);
  if (count > 0)
  {
    stow->updateSolver();
    update();
    stow->sendSolverStatus();
  }
  clearSelection();
  std::string message = std::to_string(count);
  message += " Tangent Constraints Added";
  app::instance()->messageSlot(msg::buildStatusMessage(message, 2.0));
}

bool Visual::canDistance()
{
  if (stow->state == State::error) return false;
  SSHandles points = stow->getSelectedPoints(true);
  SSHandles lines = stow->getSelectedLines(true);
  if (stow->highlights.size() == 2 && points.size() == 2) return true;
  if (stow->highlights.size() == 1 && lines.size() == 1) return true;
  if (stow->highlights.size() == 2 && lines.size() == 1 && points.size() == 1) return true;
  return false;
}

std::optional<std::pair<SSHandle, std::shared_ptr<prm::Parameter>>> Visual::addDistance()
{
  if (stow->state == State::error) return std::nullopt;
  SSHandle dh = 0;
  double length = 0.0;
  
  //we can use both work origin and axes.
  SSHandles points = stow->getSelectedPoints(true);
  SSHandles lines = stow->getSelectedLines(true);
  
  if (stow->highlights.size() == 2 && points.size() == 2)
  {
    osg::Vec3d p1 = PointAdapter(stow->solver, points.front());
    osg::Vec3d p2 = PointAdapter(stow->solver, points.back());
    length = (p2 - p1).length();
    dh = stow->solver.addPointPointDistance(length, points.front(), points.back());
  }
  else if (stow->highlights.size() == 1 && lines.size() == 1)
  {
    auto ols = stow->solver.findEntity(lines.front()); assert(ols);
    
    osg::Vec3d p1 = PointAdapter(stow->solver, ols->get().point[0]);
    osg::Vec3d p2 = PointAdapter(stow->solver, ols->get().point[1]);
    length = (p2 - p1).length();
    dh = stow->solver.addPointPointDistance(length, ols->get().point[0], ols->get().point[1]);
  }
  else if (stow->highlights.size() == 2 && lines.size() == 1 && points.size() == 1)
  {
    osg::Vec3d p = PointAdapter(stow->solver, points.front());
    auto ols = stow->solver.findEntity(lines.front()); assert(ols);
    osg::Vec3d lp = PointAdapter(stow->solver, ols->get().point[0]);
    osg::Vec3d ld = static_cast<osg::Vec3d>(PointAdapter(stow->solver, ols->get().point[1])) - lp;
    ld.normalize();
    length = distancePointLine(lp, ld, p);
    dh = stow->solver.addPointLineDistance(length, points.front(), lines.front());
  }
  else
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Distance Rejected. Unsupported Entity Type Combination", 2.0));
    return std::nullopt;
  }
  stow->updateSolver();
  
  std::shared_ptr<prm::Parameter> parameter = std::make_shared<prm::Parameter>(prm::Names::Distance, length);
  stow->cMap.records.emplace_back(dh, gu::createRandomId(), false);
  connectDistance(dh, parameter.get(), osg::Vec3d(length / 2.0, -0.1, 0.0));
  
  app::instance()->messageSlot(msg::buildStatusMessage("Distance Added", 2.0));
  update();
  stow->sendSolverStatus();
  clearSelection();
  
  return std::make_pair(dh, parameter);
}

void Visual::connect(SSHandle cHandle, prm::Parameter *parameter, const osg::Vec3d &location)
{
  auto c = stow->solver.findConstraint(cHandle);
  if (!c) return;
  if (c->get().type == SLVS_C_PT_PT_DISTANCE || c->get().type == SLVS_C_PT_LINE_DISTANCE) connectDistance(cHandle, parameter, location);
  else if (c->get().type == SLVS_C_DIAMETER) connectDiameter(cHandle, parameter, location);
  else if (c->get().type == SLVS_C_ANGLE) connectAngle(cHandle, parameter, location);
}

void Visual::connectDistance(SSHandle cHandle, prm::Parameter *parameter, const osg::Vec3d &location)
{
  auto oRec = stow->cMap.getORecord(cHandle); assert(oRec); if (!oRec) return;
  
  lbr::PLabel *label = new lbr::PLabel(parameter);
  label->setMatrix(osg::Matrixd::translate(location));
  label->setShowName(false);
  
  auto *ccb = new DimensionCallback(label);
  //plabel handles color
  ccb->setPrehighlightColor(stow->trunk.preHighlightColor);
  ccb->setHighlightColor(stow->trunk.highlightColor);
  ccb->setDefaultDepth(stow->trunk.constraintDepth);
  
  osg::MatrixTransform *ld = lbr::buildLinearDimension(label);
  ld->setNodeMask(Constraint.to_ulong());
  lbr::LinearDimensionCallback *cb = new lbr::LinearDimensionCallback();
  cb->setTextObjectName(label->getName().c_str());
  cb->setDistance(parameter->getDouble());
  ld->setUpdateCallback(cb);
  
  Map::Record &record = oRec->get();
  record.node = ld;
  stow->trunk.dimensionGroup->addChild(record.node);
}

void Visual::connectDiameter(SSHandle cHandle, prm::Parameter *parameter, const osg::Vec3d &location)
{
  auto oRec = stow->cMap.getORecord(cHandle); assert(oRec); if (!oRec) return;
  
  lbr::PLabel *label = new lbr::PLabel(parameter);
  label->setMatrix(osg::Matrixd::translate(location));
  label->setShowName(false);
  
  auto *ccb = new DimensionCallback(label);
  //plabel handles color
  ccb->setPrehighlightColor(stow->trunk.preHighlightColor);
  ccb->setHighlightColor(stow->trunk.highlightColor);
  ccb->setDefaultDepth(stow->trunk.constraintDepth);
  
  osg::MatrixTransform *dd = lbr::buildDiameterDimension(label);
  dd->setNodeMask(Constraint.to_ulong());
  lbr::DiameterDimensionCallback *cb = new lbr::DiameterDimensionCallback(label->getName());
  cb->setDiameter(parameter->getDouble());
  dd->setUpdateCallback(cb);
  
  Map::Record &record = oRec->get();
  record.node = dd;
  stow->trunk.dimensionGroup->addChild(record.node);
}

void Visual::connectAngle(SSHandle cHandle, prm::Parameter *parameter, const osg::Vec3d &location)
{
  auto oRec = stow->cMap.getORecord(cHandle); assert(oRec); if (!oRec) return;
  
  lbr::PLabel *label = new lbr::PLabel(parameter);
  label->setMatrix(osg::Matrixd::translate(location));
  label->setShowName(false);
  
  auto *ccb = new DimensionCallback(label);
  //plabel handles color
  ccb->setPrehighlightColor(stow->trunk.preHighlightColor);
  ccb->setHighlightColor(stow->trunk.highlightColor);
  ccb->setDefaultDepth(stow->trunk.constraintDepth);
  
  osg::MatrixTransform *angularDimension = lbr::buildAngularDimension(label);
  lbr::AngularDimensionCallback *cb = new lbr::AngularDimensionCallback(label->getName());
  cb->setAngleDegrees(parameter->getDouble());
  angularDimension->setUpdateCallback(cb);
  
  Map::Record &record = oRec->get();
  record.node = angularDimension;
  record.node->setNodeMask(Constraint.to_ulong());
  stow->trunk.dimensionGroup->addChild(record.node);
}

bool Visual::canEqual()
{
  if (stow->state == State::error) return false;
  SSHandles lines = stow->getSelectedLines(false);
  SSHandles arcs = stow->getSelectedCircles();
  if (stow->highlights.size() == lines.size() && lines.size() > 1) return true;
  if (stow->highlights.size() == arcs.size() && arcs.size() > 1) return true;
  return false;
}

void Visual::addEqual()
{
  if (stow->state == State::error) return;
  SSHandles lines = stow->getSelectedLines(false);
  SSHandles arcs = stow->getSelectedCircles();
  if (stow->highlights.size() == lines.size() && lines.size() > 1)
  {
    for (std::size_t i = 0; i < lines.size() - 1; ++i)
      stow->solver.addEqualLengthLines(lines.at(i), lines.at(i + 1));
  }
  else if (stow->highlights.size() == arcs.size() && arcs.size() > 1)
  {
    for (std::size_t i = 0; i < arcs.size() - 1; ++i)
      stow->solver.addEqualRadius(arcs.at(i), arcs.at(i + 1));
  }
  else
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Equality Rejected. Unsupported Entity Type Combination", 2.0));
    return;
  }
  stow->updateSolver();
  app::instance()->messageSlot(msg::buildStatusMessage("Equality Added", 2.0));
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canEqualAngle()
{
  if (stow->state == State::error) return false;
  SSHandles lines = stow->getSelectedLines();
  if (stow->highlights.size() != lines.size()) return false;
  if (lines.size() != 3 && lines.size() != 4) return false;
  
  return true;
}

/*! @brief Add a equal angle constraint to the currently selected objects.
 * 
 * @details The user needs to keep selection in a counter clockwise
 * to get desired results. Same as add angle.
 * 
 * @note this is done separate from addEqual because we want addEqual
 * to be able to apply equal length constraints to multiple line segments.
 * EqualAngle can take 3 or 4 segments, so there is no way to
 * differentiate between equal angle or multiple equal lengths from selection.
 */
void Visual::addEqualAngle()
{
  if (stow->state == State::error) return;
  SSHandles lines = stow->getSelectedLines();
  if (stow->highlights.size() != lines.size())
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Equal Angle Rejected. Only Lines Supported", 2.0));
    return;
  }
  if (lines.size() == 3) stow->solver.addEqualAngle(lines.at(0), lines.at(1), lines.at(1), lines.at(2));
  else if (lines.size() == 4) stow->solver.addEqualAngle(lines.at(0), lines.at(1), lines.at(2), lines.at(3));
  else
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Equal Angle Rejected. Only 3 or 4 Lines Accepted", 2.0));
    return;
  }
  stow->updateSolver();
  app::instance()->messageSlot(msg::buildStatusMessage("Equal Angle Added", 2.0));
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canDiameter()
{
  if (stow->state == State::error) return false;
  //can only do one at a time.
  SSHandles circles = stow->getSelectedCircles();
  if (circles.size() != 1 || stow->highlights.size() != circles.size()) return false;
  return true;
}

std::optional<std::pair<SSHandle, std::shared_ptr<prm::Parameter>>> Visual::addDiameter()
{
  if (stow->state == State::error) return std::nullopt;
  SSHandles circles = stow->getSelectedCircles();
  if (circles.size() != 1)
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Diameter Rejected. Only Circles Supported", 2.0));
    return std::nullopt;
  }
  
  SSHandle c = circles.front();
  auto oe = stow->solver.findEntity(c); assert(oe);
  
  std::optional<double> radius;
  osg::Vec3d position;
  if (stow->solver.isEntityType(c, SLVS_E_CIRCLE))
  {
    //circles have a handle to a distance entity.
    auto od = stow->solver.findEntity(oe->get().distance); assert(od);
    radius = stow->solver.getParameterValue(od->get().param[0]);
    position = osg::Vec3d(*radius, 0.0, 0.0);
  }
  else if (stow->solver.isEntityType(c, SLVS_E_ARC_OF_CIRCLE))
  {
    osg::Vec3d ac = PointAdapter(stow->solver, oe->get().point[0]);
    osg::Vec3d as = PointAdapter(stow->solver, oe->get().point[1]);
    radius = (as - ac).length();
    position = stow->parameterPoint(c, 0.5) - ac;
  }
  if (radius && (*radius > std::numeric_limits<float>::epsilon()))
  {
    SSHandle dh = stow->solver.addDiameter(*radius * 2.0, c);
    stow->updateSolver();
    
    std::shared_ptr<prm::Parameter> parameter = std::make_shared<prm::Parameter>
    (prm::Names::Diameter, *radius * 2.0);
    
    stow->cMap.records.emplace_back(dh, gu::createRandomId(), false);
    connectDiameter(dh, parameter.get(), position * 1.5);

    app::instance()->messageSlot(msg::buildStatusMessage("Diameter Added", 2.0));
    update();
    stow->sendSolverStatus();
    clearSelection();
    return std::make_pair(dh, parameter);
  }
  app::instance()->messageSlot(msg::buildStatusMessage("Distance Rejected. Invalid Radius", 2.0));
  return std::nullopt;
}

bool Visual::canSymmetry()
{
  if (stow->state == State::error) return false;
  return stow->canDoSymmetric(false) != 0;
}

void Visual::addSymmetric()
{
  if (stow->state == State::error) return;
  auto nc = stow->canDoSymmetric(true);
  if (nc)
  {
    stow->updateSolver();
    app::instance()->messageSlot(msg::buildStatusMessage("Symmetric Added", 2.0));
    update();
    stow->sendSolverStatus();
    clearSelection();
  }
  else
    app::instance()->messageSlot(msg::buildStatusMessage("Symmetric Rejected", 2.0));
}

bool Visual::canAngle()
{
  if (stow->state == State::error) return false;
  SSHandles lines = stow->getSelectedLines(true);
  if (stow->highlights.size() == 2 && lines.size() == 2) return true;
  return false;
}

std::optional<std::pair<SSHandle, std::shared_ptr<prm::Parameter>>> Visual::addAngle()
{
  if (stow->state == State::error) return std::nullopt;
  SSHandles lines = stow->getSelectedLines(true);
  if (stow->highlights.size() != 2 || lines.size() != 2)
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Invalid Selection", 2.0));
    clearSelection();
    return std::nullopt;
  }
  
  //make sure user hasn't selected both work lines
  auto xAxis = stow->solver.getXAxis();
  auto yAxis = stow->solver.getYAxis();
  auto isAxis = [&](SSHandle l) -> bool {return l == xAxis || l == yAxis;};
  if (isAxis(lines.front()) && isAxis(lines.back()))
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Invalid Selection", 2.0));
    clearSelection();
    return std::nullopt;
  }
  
  AngleWrench wrench(stow->solver, lines, true);
  double angle = wrench.getAngle();
  bool other = wrench.gleanOther();
  if (!wrench.isValid())
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Adding Angle Failed", 2.0));
    clearSelection();
    return std::nullopt;
  }
  
  SSHandle ah = stow->solver.addAngle(osg::RadiansToDegrees(angle), lines.front(), lines.back(), other);
  stow->updateSolver();
  auto parameter = std::make_shared<prm::Parameter> (prm::Names::Angle, osg::RadiansToDegrees(angle));
  parameter->setConstraint(prm::Constraint::buildZeroPositiveHalfAngle());
  auto &cRecord = stow->cMap.records.emplace_back(ah, gu::createRandomId(), false);
  connectAngle(ah, parameter.get(), wrench.getTextLocation());
  
  //we need a separate procedure for dimension creations vs dimension update.
  //when creating we derive sense and solvespace other variable.
  //when updating, the solvespace other has been set and we can't change it.
  osg::MatrixTransform *dim = dynamic_cast<osg::MatrixTransform*>(cRecord.node.get()); assert(dim);
  dim->setMatrix(wrench.getCSys());
  
  auto *cb = dynamic_cast<lbr::AngularDimensionCallback*>(dim->getUpdateCallback()); assert(cb);
  cb->setAngleDegrees(parameter->getDouble());
  cb->setRangeMask1(wrench.getFirstRangeMask());
  cb->setRangeMask2(wrench.getSecondRangeMask());
  cb->setMaxScale(stow->lastSize * stow->maxScaleFactor);
  
  app::instance()->messageSlot(msg::buildStatusMessage("Angle added", 2.0));
  update();
  clearSelection();
  return std::make_pair(ah, parameter);
}

bool Visual::canParallel()
{
  if (stow->state == State::error) return false;
  SSHandles lines = stow->getSelectedLines();
  if (stow->highlights.size() == 2 && lines.size() == 2) return true;
  return false;
}

void Visual::addParallel()
{
  if (stow->state == State::error) return;
  SSHandles lines = stow->getSelectedLines();
  if (lines.size() != 2)
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Parallel Rejected. Only Lines Supported", 2.0));
    return;
  }
  stow->solver.addParallel(lines.front(), lines.back());
  stow->updateSolver();
  app::instance()->messageSlot(msg::buildStatusMessage("Parallel Added", 2.0));
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canPerpendicular()
{
  if (stow->state == State::error) return false;
  SSHandles lines = stow->getSelectedLines();
  if (stow->highlights.size() == 2 && lines.size() == 2) return true;
  return false;
}

void Visual::addPerpendicular()
{
  if (stow->state == State::error) return;
  SSHandles lines = stow->getSelectedLines();
  if (lines.size() != 2)
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Perpendicular Rejected. Only Lines Supported", 2.0));
    return;
  }
  stow->solver.addPerpendicular(lines.front(), lines.back());
  stow->updateSolver();
  app::instance()->messageSlot(msg::buildStatusMessage("Perpendicular Added", 2.0));
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canMidPoint()
{
  if (stow->state == State::error || stow->highlights.size() != 2) return false;
  SSHandles lines = stow->getSelectedLines(false);
  SSHandles points = stow->getSelectedPoints(true);
  if (lines.size() != 1 || points.size() != 1) return false;
  return true;
}

void Visual::addMidpoint()
{
  if (stow->state == State::error) return;
  //using the work axis line as a line makes no sense. That midpoint should be origin, so just use that.
  //using work origin as point does make sense. 
  SSHandles lines = stow->getSelectedLines(false);
  SSHandles points = stow->getSelectedPoints(true);
  
  if (stow->highlights.size() != 2)
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Midpoint Rejected. Need 2 Entities", 2.0));
    return;
  }
  if (lines.size() != 1 || points.size() != 1)
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Midpoint Rejected. Need 1 point and 1 line", 2.0));
    return;
  }
  
  stow->solver.addMidpointLine(points.front(), lines.front());
  stow->updateSolver();
  app::instance()->messageSlot(msg::buildStatusMessage("Midpoint Added", 2.0));
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canWhereDragged()
{
  if (stow->state == State::error) return false;
   SSHandles points = stow->getSelectedPoints(false);
   if (points.empty() || stow->highlights.size() != points.size()) return false;
   return true;
}

void Visual::addWhereDragged()
{
  if (stow->state == State::error) return;
  SSHandles points = stow->getSelectedPoints(false);
  if (points.empty())
  {
    app::instance()->messageSlot(msg::buildStatusMessage("Where Dragged Rejected. Need At Least 1 Point", 2.0));
    return;
  }
  for (const auto &h : points) stow->solver.addWhereDragged(h);
  stow->updateSolver();
  app::instance()->messageSlot(msg::buildStatusMessage("Where Dragged Added", 2.0));
  update();
  stow->sendSolverStatus();
  clearSelection();
}

bool Visual::canRemove()
{
  if (stow->highlights.empty()) return false;
  
  for (const auto &h : stow->highlights)
  {
    auto c = stow->cMap.getORecord(h); if (c) continue;
    auto e = stow->eMap.getORecord(h); if (e) continue;
    //if we make it here, we have found something we can't remove. like a work plane axis.
    return false;
  }
  return true;
}

bool Visual::canRemoveLast()
{
  if (!stow->highlights.empty()) return false;
  if (stow->cMap.records.empty()) return false;
  return true;
}

void Visual::remove()
{
  stow->clearPreHighlight();
  for (const auto &h : stow->highlights)
  {
    if (auto c = stow->cMap.getORecord(h))
    {
      stow->solver.removeConstraint(c->get().handle);
      continue;
    }
    if (auto e = stow->eMap.getORecord(h.get()))
    {
      stow->solver.removeEntity(e->get().handle);
      continue;
    }
  }
  stow->solver.clean();
  stow->updateSolver();
  update();
  stow->sendSolverStatus();
  stow->highlights.clear(); //items gone so no need to unhighlight, just remove.
  app::instance()->messageSlot(msg::buildStatusMessage("Select Command Or Entities"));
  app::instance()->messageSlot(msg::buildStatusMessage("Items Removed", 2.0));
}

void Visual::removeLast()
{
  assert(stow->state == State::selection || stow->state == State::error);
  // command view should prevent launching this command with any selection so we don't worry about clearing it
  
  /*My original plan was to have 2 different behaviors based upon ok state.
   In error state, I would take the highest conflicting constraint and erase it.
   Unfortunately the list of conflicting constraints is not comprehensive and
   we end up deleting a constraint we probably didn't want to. So now we are going
   to ignore the failed list and and just remove the last constraint added.*/
  
  switch (stow->state)
  {
    case State::error:
    {
      /*
      SSHandle highest = 0;
      for (auto f : stow->solver.getFailed()) highest = std::max(highest, f);
      if (highest == 0) stow->solver.removeLastConstraint();
      else stow->solver.removeConstraint(highest);
      */
      stow->solver.removeLastConstraint();
      break;
    }
    case State::selection:
    {
      stow->solver.removeLastConstraint();
      break;
    }
    case State::drag: case State::dynamic:
    {
      assert(0);
      std::cout << "Error: shouldn't be in skt::Visual:removeLast in current state." << std::endl;
      break;
    }
  }
  
  stow->solver.clean();
  stow->updateSolver();
  update();
  stow->sendSolverStatus();
}

void Visual::cancel()
{
  stow->state = State::selection;
  stow->dontSelectNodes.clear();
  if (stow->fluid)
  {
    stow->fluid->cancel();
    stow->fluid.reset(nullptr);
  }
  
  stow->updateSolver();
  update();
  stow->sendSolverStatus();
  stow->clearDrag();
}

bool Visual::canToggleConstruction()
{
  if (stow->state == State::error || stow->highlights.empty()) return false;
  
  //just make sure of just non-work geometry.
  auto pc = stow->getSelectedPoints(false).size();
  auto lc = stow->getSelectedLines(false).size();
  auto cc = stow->getSelectedCircles().size(); //includes arcs
  auto bc = stow->getSelectedBeziers().size();
  if ((pc + lc + cc + bc) == stow->highlights.size()) return true;
  
  return false;
}

void Visual::toggleConstruction()
{
  if (stow->state == State::error) return;
  for (const auto &h : stow->highlights)
  {
    auto e = stow->eMap.getORecord(h.get());
    if (e)
    {
      if (e->get().construction) stow->clearConstruction(e->get());
      else stow->setConstruction(e->get());
    }
  }
  app::instance()->messageSlot(msg::buildStatusMessage("Construction Toggled", 2.0));
  clearSelection();
}

void Visual::setAutoSize(bool value) {stow->autoSize = value;}
bool Visual::getAutoSize() {return stow->autoSize;}

/*! @brief Set the current value for size.
 * @param value New value for size
 * @ref Sizing
 * @note This sets autoSize to false. Call @ref update for
 * display to reflect this change.
 */
void Visual::setSize(double value)
{
  setAutoSize(false);
  stow->size = value;
}

/*! @brief Get the current value of size.
 * @return Current value of size
 * @ref Sizing
 */
double Visual::getSize() {return stow->size;}
State Visual::getState() {return stow->state;}
osg::MatrixTransform* Visual::getTransform() {return stow->trunk.transform.get();}

/*! @brief Project a point onto a line.
 * 
 * @param lp Is a point on the line.
 * @param ld Is a unit vector for the line direction.
 * @param p Is the point to project.
 * @return The projected point.
 */
osg::Vec3d Visual::projectPointLine(const osg::Vec3d &lp, const osg::Vec3d &ld, const osg::Vec3d &p)
{
  osg::Vec3d b(lp - p);
  osg::Vec3d proj(ld * (b * ld));
  return b - proj + p;
}

/*! @brief Get the distance between a point and a line.
 * 
 * @param lp Is a point on the line.
 * @param ld Is a unit vector for the line direction.
 * @param p Is a point.
 * @return The distance.
 * @note This function uses the line sense and returns a distance that
 * can be positive or negative.
 */
double Visual::distancePointLine(const osg::Vec3d &lp, const osg::Vec3d &ld, const osg::Vec3d &p)
{
  osg::Vec3d projectedPoint = projectPointLine(lp, ld, p);
  double distance = (projectedPoint - p).length();
  
  osg::Vec3d aux = p - lp;
  aux.normalize(); //probably don't need.
  osg::Vec3d cross = aux ^ ld;
  if (cross.z() < 0.0)
    distance *= -1.0;
  
  return distance;
}

bool Visual::isConstruction(SSHandle h)
{
  auto ro = stow->eMap.getORecord(h); assert(ro); if (!ro) return false;
  return ro->get().construction;
}

boost::uuids::uuid Visual::getEntityId(SSHandle h)
{
  auto ro = stow->eMap.getORecord(h); assert(ro);
  return ro->get().id;
}

prj::srl::skts::Visual Visual::serialOut() const
{
  prj::srl::skts::Visual out
  (
    stow->autoSize
    , stow->size
  );
  
  //both entitymap and constraintmap are typed as follows.
  typedef ::xsd::cxx::tree::sequence<::prj::srl::skts::VisualMapRecord> SequenceType;
  auto serialMap = [](const Map& mapIn) -> SequenceType
  {
    SequenceType emout;
    for (const auto &e : mapIn.records)
    {
      osg::Vec3d location;
      osg::Group *g = dynamic_cast<osg::Group*>(e.node.get());
      if (g)
      {
        lbr::ChildNameVisitor v("lbr::PLabel");
        g->accept(v);
        if (v.out)
        {
          lbr::PLabel *l = v.castResult<lbr::PLabel>();
          location = l->getMatrix().getTrans();
        }
      }
      emout.push_back
      (
        prj::srl::skts::VisualMapRecord
        (
          e.handle
          , gu::idToString(e.id)
          , prj::srl::spt::Vec3d(location.x(), location.y(), location.z())
          , e.construction
        )
      );
    }
    return emout;
  };
  
  out.entityMap() = serialMap(stow->eMap);
  out.constraintMap() = serialMap(stow->cMap);
  
  return out;
}

void Visual::serialIn(const prj::srl::skts::Visual &sIn)
{
  stow->autoSize = sIn.autoSize();
  stow->size = sIn.size();
  for (const auto &e : sIn.entityMap()) stow->eMap.records.emplace_back(e.handle(), gu::stringToId(e.id()), e.construction());
  for (const auto &c : sIn.constraintMap()) stow->cMap.records.emplace_back(c.handle(), gu::stringToId(c.id()), c.construction());
}

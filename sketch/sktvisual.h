/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2018  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKT_VISUAL_H
#define SKT_VISUAL_H

#include <memory>
#include <utility>
#include <optional>

#include <boost/uuid/uuid.hpp>

#include <osg/ref_ptr>
#include <osgUtil/PolytopeIntersector>
#include <osgUtil/LineSegmentIntersector>

#include "sketch/skttypes.h"

namespace osg
{
  class MatrixTransform;
  class Vec3d;
}
namespace prm{class Parameter;}
namespace prj{namespace srl{namespace skts{class Visual;}}}

namespace skt
{
  struct Solver;
  
  /*! @class Visual
   * @brief Manages visual interface to a sketch.
   * 
   * The sketch handles managing references between parameters, entities and constraints.
   * The visual should update itself to the state of the sketch. For the most
   * part visual will not change the sketch, but there will be cases it has to. i.e. dragging.
   */
  class Visual
  {
  public:
    Visual(Solver&);
    ~Visual();
    
    /** @anchor Visibility
     * @name Visibility
     * Functions used to control Visibility.
     */
    ///@{
    void update();
    void showPlane();
    void hidePlane();
    void showEntity();
    void hideEntity();
    void showConstraint();
    void hideConstraint();
    void showDimensions();
    void hideDimensions();
    void showAll();
    void hideAll();
    void setActiveSketch();
    void clearActiveSketch();
    ///@}
    
    /** @anchor SelectionInteraction
     * @name Selection Interaction
     * These are instigated from the selection event handler.
     */
    ///@{
    void move(const osgUtil::PolytopeIntersector::Intersections&);
    void move(const osgUtil::LineSegmentIntersector::Intersections&);
    void pick(const osgUtil::PolytopeIntersector::Intersections&);
    void pick(const osgUtil::LineSegmentIntersector::Intersections&);
    void startDrag(const osgUtil::LineSegmentIntersector::Intersections&);
    void drag(const osgUtil::LineSegmentIntersector::Intersections&);
    void finishDrag(const osgUtil::LineSegmentIntersector::Intersections&);
    void leftDoubleClick(const osgUtil::PolytopeIntersector::Intersections&);
    bool handleKey(int);
    void clearSelection();
    ///@}
    
    /** @anchor Colors
     * @name Colors
     * Functions that control color.
     */
    ///@{
    void setPreHighlightColor(const osg::Vec4&);
    osg::Vec4 getPreHighlightColor();
    void setHighlightColor(const osg::Vec4&);
    osg::Vec4 getHighlightColor();
    ///@}
    
    /** @anchor ObjectControl
     * @name Object Control
     * Functions for adding and removing.
     */
    ///@{
    void setChainOn();
    void setChainOff();
    bool isChainOn();
    void setAutoCoincident(bool);
    void setAutoTangent(bool);
    void addPoint();
    void addLine();
    void addArc();
    void addCircle();
    void addCubicBezier();
    void addCoincident();
    void addHorizontal();
    void addVertical();
    void addTangent();
    std::optional<std::pair<SSHandle, std::shared_ptr<prm::Parameter>>> addDistance();
    void addEqual();
    void addEqualAngle();
    std::optional<std::pair<SSHandle, std::shared_ptr<prm::Parameter>>> addDiameter();
    void addSymmetric();
    std::optional<std::pair<SSHandle, std::shared_ptr<prm::Parameter>>> addAngle();
    void addParallel();
    void addPerpendicular();
    void addMidpoint();
    void addWhereDragged();
    void remove();
    void removeLast();
    void cancel();
    void toggleConstruction();
    ///@}
    
    /** @anchor SelectionTesting
     * @name Selection Testing 
     * Test viability of command based upon current selection.
     */
    ///@{
    bool canCoincident();
    bool canHorizontal();
    bool canVertical();
    bool canTangent();
    bool canSymmetry();
    bool canParallel();
    bool canPerpendicular();
    bool canEqual();
    bool canEqualAngle();
    bool canMidPoint();
    bool canWhereDragged();
    bool canDistance();
    bool canDiameter();
    bool canAngle();
    bool canToggleConstruction();
    bool canRemove();
    bool canRemoveLast();
    ///@}
    
    /** @anchor Sizing
     * @name Sizing
     * Functions for controlling the visible plane size.
     * When autoSize is true, the visible plane size will be determined
     * when update is called. When autoSize is false, the plane size will
     * be determined by the value of size. @see autoSize size
     */
    ///@{
    void setAutoSize(bool);
    bool getAutoSize();
    void setSize(double);
    double getSize();
    ///@}
    
    /** @anchor VisualMisc
     * @name Misc
     */
    ///@{
    State getState();
    osg::MatrixTransform* getTransform();
    static osg::Vec3d projectPointLine(const osg::Vec3d&, const osg::Vec3d&, const osg::Vec3d&);
    static double distancePointLine(const osg::Vec3d&, const osg::Vec3d&, const osg::Vec3d&);
    bool isConstruction(SSHandle);
    boost::uuids::uuid getEntityId(SSHandle);
    ///@}
    
    /** @anchor ParameterBasedDimensions
     * @name Parameter Based Dimensions
     * Most visuals for constraints are detected and constructed as needed
     * in update. There are a few dimensions that use prm::Parameters
     * and this scheme doesn't work. We need to build these visuals
     * at constraint construction and serialIn. These functions are used
     * for that setup. @see ftr::Sketch::serialRead
     */
    ///@{
    void connect(SSHandle, prm::Parameter*, const osg::Vec3d&);
    void connectDistance(SSHandle, prm::Parameter*, const osg::Vec3d&);
    void connectDiameter(SSHandle, prm::Parameter*, const osg::Vec3d&);
    void connectAngle(SSHandle, prm::Parameter*, const osg::Vec3d&);
    ///@}
    
    /** @anchor VisualSerial
     * @name Serial
     */
    ///@{
    prj::srl::skts::Visual serialOut() const;
    void serialIn(const prj::srl::skts::Visual&);
    ///@}
    
  private:
    struct Stow;
    std::unique_ptr<Stow> stow; //!< Private data. pimpl.
  };
}

#endif // SKT_VISUAL_H

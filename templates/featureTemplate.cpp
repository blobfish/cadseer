/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) %YEAR% Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <osg/Switch>

#include "globalutilities.h"
// #include "annex/annseershape.h"
// #include "library/lbrplabel.h"
// #include "library/lbrplabelgrid.h"
// #include "library/lbripgroup.h"
// #include "parameter/prmconstants.h"
// #include "parameter/prmparameter.h"
// #include "tools/occtools.h"
// #include "tools/featuretools.h"
// #include "tools/idtools.h"
// #include "feature/ftrshapecheck.h"
#include "feature/ftrupdatepayload.h"
// #include "feature/ftrinputtype.h"
// #include "project/serial/generated/prjsrl_FIX_%CLASSNAMELOWERCASE%.h"
#include "feature/ftr%CLASSNAMELOWERCASE%.h"

using boost::uuids::uuid;
using namespace ftr::%CLASSNAME%;
QIcon Feature::icon = QIcon(":/resources/images/%CLASSNAMELOWERCASE%.svg");

struct Feature::Stow
{
  Feature &feature;
  // ann::SeerShape sShape;
  
  // prm::Parameter %CLASSNAMELOWERCASE%Type{QObject::tr("%CLASSNAME% Type"), 0, PrmTags::%CLASSNAMELOWERCASE%Type};
  // prm::Parameter picks{prm::Names::Picks, ftr::Picks(), prm::Tags::Picks};
  // prm::Parameter distance{prm::Names::Distance, 1.0, prm::Tags::Distance};
  // prm::Observer dirtyObserver{std::bind(&Feature::setModelDirty, &feature)};
  // prm::Observer syncObserver{std::bind(&Stow::prmActiveSync, this)};
  
  // osg::ref_ptr<osg::AutoTransform> grid = lbr::PLabelGridCallback::buildGrid(1);
  // osg::ref_ptr<lbr::PLabel> %CLASSNAMELOWERCASE%TypeLabel{new lbr::PLabel(&%CLASSNAMELOWERCASE%Type)};
  // osg::ref_ptr<lbr::PLabel> distanceLabel{new lbr::PLabel(&distance)};
  // osg::ref_ptr<lbr::IPGroup> iLabel{new lbr::IPGroup(&parameter)};
  
  Stow(Feature &fIn) : feature(fIn)
  {
    // feature.annexes.insert(std::make_pair(ann::Type::SeerShape, &sShape));
    
    // QStringList %CLASSNAMELOWERCASE%TypeStrings =
    // {
    //   QObject::tr("Constant")
    //   , QObject::tr("Infer")
    //   , QObject::tr("Picks")
    // };
    // %CLASSNAMELOWERCASE%Type.setEnumeration(%CLASSNAMELOWERCASE%TypeStrings);
    // %CLASSNAMELOWERCASE%Type.connect(dirtyObserver);
    // %CLASSNAMELOWERCASE%Type.connect(syncObserver);
    // feature.parameters.push_back(&%CLASSNAMELOWERCASE%Type);
    
    // picks.connect(dirtyObserver); feature.parameters.push_back(&picks);
    // distance.connect(dirtyObserver); feature.parameters.push_back(&distance);
    
    // feature.overlaySwitch->addChild(grid);
    // grid->addChild(%CLASSNAMELOWERCASE%TypeLabel);
    // grid->addChild(distanceLabel);
    // %CLASSNAMELOWERCASE%TypeLabel->refresh();
    
    // prmActiveSync();
  }
  
  void prmActiveSync()
  {
    // prm::ObserverBlocker dirtyBlocker(dirtyObserver);
    // prm::ObserverBlocker syncBlocker(syncObserver);
  }
};

Feature::Feature()
: Base()
, stow(std::make_unique<Stow>(*this))
{
  name = QObject::tr("%CLASSNAME%");
  mainSwitch->setUserValue<int>(gu::featureTypeAttributeTitle, static_cast<int>(getType()));
}

Feature::~Feature() = default;

void Feature::updateModel(const UpdatePayload &/*pIn*/)
{
  setFailure();
  lastUpdateLog.clear();
//   stow->sShape.reset();
  try
  {
    // prm::ObserverBlocker dirtyBlocker(stow->dirtyObserver);
    // prm::ObserverBlocker syncBlocker(stow->syncObserver);
    //setup new failure state for alter features only.
    // stow->sShape.setOCCTShape(tss.getRootOCCTShape(), getId());
    // stow->sShape.shapeMatch(tss);
    // stow->sShape.uniqueTypeMatch(tss);
    // stow->sShape.outerWireMatch(tss);
    // stow->sShape.derivedMatch();
    // stow->sShape.ensureNoNils(); //just in case
    // stow->sShape.ensureNoDuplicates(); //just in case
    // stow->sShape.ensureEvolve();
    
    if (isSkipped())
    {
      setSuccess();
      throw std::runtime_error("feature is skipped");
    }
    
//     tls::Resolver pr(pIn);
//     if (!pr.resolve(picks.front())) throw std::runtime_error("invalid pick resolution");
    
    //update goes here.
    
//     ShapeCheck check(out);
//     if (!check.isValid()) throw std::runtime_error("shapeCheck failed");

    //set color if this is an alter feature.
    
    setSuccess();
  }
  catch (const Standard_Failure &e)
  {
    std::ostringstream s; s << "OCC Error in " << getTypeString() << " update: " << e.GetMessageString() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (const std::exception &e)
  {
    std::ostringstream s; s << "Standard error in " << getTypeString() << " update: " << e.what() << std::endl;
    lastUpdateLog += s.str();
  }
  catch (...)
  {
    std::ostringstream s; s << "Unknown error in " << getTypeString() << " update." << std::endl;
    lastUpdateLog += s.str();
  }
  setModelClean();
  if (!lastUpdateLog.empty())
    std::cout << std::endl << lastUpdateLog;
}

void Feature::serialWrite(const std::filesystem::path &/*dIn*/)
{
//   prj::srl::FIX::%CLASSNAME% so
//   (
//     Base::serialOut()
//     , stow->sShape.serialOut()
//     , stow->%CLASSNAMELOWERCASE%Type.serialOut()
//     , stow->picks.serialOut()
//     , stow->distance.serialOut()
//     , lbr::PLabelGridCallback::serialOut(stow->grid)
//     , stow->%CLASSNAMELOWERCASE%TypeLabel->serialOut()
//     , stow->distanceLabel->serialOut()
//   );
//   
//   xml_schema::NamespaceInfomap infoMap;
//   std::ofstream stream(buildFilePathName(dIn).string());
//   prj::srl::FIX::%CLASSNAMELOWERCASE%(stream, so, infoMap);
}

// void Feature::serialRead(const prj::srl::FIXME::%CLASSNAME% &so)
// {
//   Base::serialIn(so.base());
//   stow->sShape.serialIn(so.seerShape());
//   stow->%CLASSNAMELOWERCASE%Type.serialIn(so.%CLASSNAMELOWERCASE%Type());
//   stow->picks.serialIn(so.picks());
//   stow->distance.serialIn(so.direction());
//   stow->%CLASSNAMELOWERCASE%TypeLabel->serialIn(so.%CLASSNAMELOWERCASE%TypeLabel());
//   stow->distanceLabel->serialIn(so.distanceLabel());
//   lbr::PLabelGridCallback::serialIn(so.gridLocation(), stow->grid);
// }

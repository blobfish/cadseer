/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2016  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QTextStream>

#include <gp_Pnt.hxx>
#include <gp_Dir.hxx>

#include <osg/Matrixd>

#include "globalutilities.h"
#include "tools/tlsstring.h"
#include "tools/infotools.h"

QTextStream& gu::osgMatrixOut(QTextStream &streamIn, const osg::Matrixd &m)
{
    streamIn << QString::fromStdString(tls::valueString(m));
    return streamIn;
}

QTextStream& gu::osgQuatOut(QTextStream &s, const osg::Quat &qIn)
{
  s << QString::fromStdString(tls::valueString(qIn));
  return s;
}

QTextStream& gu::osgVectorOut(QTextStream &s, const osg::Vec3d &vIn)
{
  s << QString::fromStdString(tls::valueString(vIn));
  return s;
}

QTextStream& gu::gpPntOut(QTextStream &sIn, const gp_Pnt &pIn)
{
  sIn << QString::fromStdString(tls::valueString(gu::toOsg(pIn)));
  return sIn;
}

QTextStream& gu::gpDirOut(QTextStream &sIn, const gp_Dir &dIn)
{
  sIn << QString::fromStdString(tls::valueString(gu::toOsg(dIn)));
  return sIn;
}

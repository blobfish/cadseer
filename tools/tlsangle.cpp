/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <cassert>

#include <TopExp.hxx>
#include <BRep_Tool.hxx>
#include <gp_Dir.hxx>
#include <gp_Ax1.hxx>
#include <Precision.hxx>
#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <Geom_Line.hxx>
#include <GeomAPI_ExtremaCurveCurve.hxx>

#include <osg/AutoTransform>
#include <osgText/Text>

#include "globalutilities.h"
#include "library/lbrplabel.h"
#include "library/lbrangulardimension.h"
#include "tools/tlsstring.h"
#include "tools/tlsosgtools.h"
#include "tools/tlsangle.h"

namespace
{
  struct Segment
  {
    osg::Vec3d head;
    osg::Vec3d tail;
    osg::Vec3d projection(){return head - tail;}
    osg::Vec3d direction(){auto temp = head - tail; temp.normalize(); return temp;}
    gp_Ax1 axis(){return gp_Ax1(gp_Pnt(gu::toOcc(tail).XYZ()), gu::toOcc(projection()));}
    opencascade::handle<Geom_Line> line(){return new Geom_Line(gp_Lin(axis()));}
    std::optional<lbr::RangeMask> makeRange(const osg::Vec3d &origin)
    {
      gp_Ax1 ax(gp_Pnt(gu::toOcc(origin).XYZ()), gu::toOcc(projection()));
      opencascade::handle<Geom_Line> tempLine = new Geom_Line(gp_Lin(ax));
      GeomAPI_ProjectPointOnCurve projector(gu::toOcc(head).XYZ(), tempLine);
      if (projector.NbPoints() < 1) return std::nullopt;
      double headParameter = projector.Parameter(1);
      projector.Perform(gu::toOcc(tail).XYZ());
      if (projector.NbPoints() < 1) return std::nullopt;
      double tailParameter = projector.Parameter(1);
      return lbr::RangeMask(headParameter, tailParameter);
    }
    bool orient(const osg::Vec3d iPoint)
    {
      //we know tail is parameter 0 and head parameter is projection().length().
      //so mid point on line is parameter projection.length() / 2.0;
      auto midParameter = projection().length() / 2.0;
      auto l = line();
      GeomAPI_ProjectPointOnCurve projector(gu::toOcc(iPoint).XYZ(), l);
      if (projector.NbPoints() < 1) return false;
      double iParameter = projector.Parameter(1);
      if (iParameter > midParameter){std::swap(head, tail); return true;}
      return false;
    }
  };
}

using namespace tls::ang;

struct tls::Angle::Stow
{
  std::optional<Segment> seg0;
  std::optional<Segment> seg1;
  State state;
  Mask mask;
  double angle = 0.0; //radians
  OVec helper;
  bool other = false; //false for solve space means the curves are oriented the same.
  osg::Vec3d origin;
  osg::Matrixd system;
  osg::Vec3d textPosition;
  std::optional<lbr::RangeMask> range0;
  std::optional<lbr::RangeMask> range1;
  
  void addSegment(const Segment &segIn)
  {
    if (!seg0) seg0 = segIn;
    else if (!seg1) seg1 = segIn;
    if (seg0 && seg1) state = Ready;
  }
  
  void calcAngle()
  {
    assert(seg0 && seg1);
    gp_Ax1 axisX = seg0->axis();
    gp_Ax1 axisY = seg1->axis();
    
    auto tryCoaxial = [&]() -> bool
    {
      //we can't calculate helper with coaxial, so we rely on it being passed in.
      if (axisX.IsCoaxial(axisY, Precision::Angular(), Precision::Confusion()))
      {
        angle = 0.0;
        mask = Coplanar | Colinear | Parallel;
        return true;
      }
      if (axisX.IsCoaxial(axisY.Reversed(), Precision::Angular(), Precision::Confusion()))
      {
        angle = osg::PI;
        mask = Coplanar | Colinear | Parallel;
        return true;;
      }
      return false;
    };
    if (tryCoaxial()) {state = Ang; return;}
    
    //not coaxial but parallel we know co-planar and 0 or 180 degrees.
    auto calcHelper = [&]() -> bool
    {
      if (helper) return true;
      gp_Pnt p0 = gu::toOcc(seg0->tail).XYZ();
      auto otherLine = seg1->line();
      GeomAPI_ProjectPointOnCurve projector(p0, otherLine);
      if (projector.NbPoints() < 1) return false;
      gp_Pnt p1 = projector.Point(1);
      auto yAxis = gu::toOsg(p1) - gu::toOsg(p0);
      if (yAxis.length() < Precision::Confusion()) {return false;}
      yAxis.normalize();
      helper = seg0->direction() ^ yAxis;
      helper->normalize();
      return true;
    };
    
    if (axisX.Direction().IsEqual(axisY.Direction(), Precision::Angular()))
    {
      angle = 0.0;
      mask = Coplanar | Parallel;
      state = Ang;
      calcHelper();
      return;
    }
    if (axisX.Direction().IsOpposite(axisY.Direction(), Precision::Angular()))
    {
      angle = osg::PI;
      mask = Coplanar | Parallel;
      state = Ang;
      calcHelper();
      return;
    }
    
    //here we have an angle to calculate.
    gp_Dir ref(seg0->axis().Direction().Crossed(seg1->axis().Direction()));
    if (helper) ref = gp_Dir(gu::toOcc(*helper));
    else helper = gu::toOsg(ref);
    angle = seg0->axis().Direction().AngleWithRef(seg1->axis().Direction(), ref);
    state = Ang;
    //range is -PI to PI. fix?
    
    //last thing to determine is coplanar or skew.
    std::optional<bool> isCoplanar;
    auto z = seg0->direction() ^ seg1->direction();
    if (z.length() < Precision::Confusion()) return;
    std::vector<osg::Vec3d> testVectors;
    testVectors.push_back(seg0->head - seg1->head);
    testVectors.push_back(seg0->head - seg1->tail);
    testVectors.push_back(seg0->tail - seg1->head);
    testVectors.push_back(seg0->tail - seg1->tail);
    for (const auto &v : testVectors)
    {
      if (v.length() < Precision::Confusion()) continue;
      auto t = v; t.normalize();
      double dot = z * t;
      if (std::fabs(dot) > Precision::Confusion())
      {
        isCoplanar = false;
        break;
      }
      else
        isCoplanar = true;
    }
    if (!isCoplanar) return;
    if (*isCoplanar) mask |= Coplanar;
    else mask |= Skew;
  }
  
  void calcOrigin()
  {
    if ((mask & Colinear).any())
    {
      auto temp = seg0->head + seg0->tail + seg1->head + seg1->tail;
      origin = temp * 0.25;
      state = Origin;
      return;
    }
    if ((mask & Parallel).any())
    {
      //project 2 points from 2nd segment onto 1st segment. Then average the 2 projected
      //with the 2 from 1st segment.
      auto line = seg0->line();
      GeomAPI_ProjectPointOnCurve projector(gp_Pnt(gu::toOcc(seg1->head).XYZ()), line);
      if (projector.NbPoints() < 1) return;
      osg::Vec3d p0 = gu::toOsg(projector.Point(1));
      projector.Perform(gu::toOcc(seg1->tail).XYZ());
      if (projector.NbPoints() < 1) return;
      osg::Vec3d p1 = gu::toOsg(projector.Point(1));
      auto temp = seg0->head + seg0->tail + p0 + p1;
      origin = temp * 0.25;
      state = Origin;
      return;
    }
    
    auto line0 = seg0->line();
    auto line1 = seg1->line();
    GeomAPI_ExtremaCurveCurve extrema(seg0->line(), seg1->line());
    if (extrema.NbExtrema() < 1) return;
    gp_Pnt p0, p1; //check distance between results and verify co-planar?
    extrema.Points(1, p0, p1);
    
    if ((mask & Coplanar).any())
    {
      origin = gu::toOsg(p0);
      state = Origin;
      return;
    }
    if ((mask & Skew).any())
    {
      //Not sure where we should put this origin.
      // origin = (gu::toOsg(p0) + gu::toOsg(p1)) * 0.5;
      origin = gu::toOsg(p0);
      state = Origin;
      return;
    }
  }
  
  void calcSystem()
  {
    if (!helper) return;
    auto temp = tls::matrixFromAxes({seg0->direction(), std::nullopt, *helper, origin});
    if (!temp) return;
    state = System;
    system = *temp;
  }
  
  void calcTextPosition()
  {
    double distance = 0.0;
    distance = std::max(distance, (seg0->head - origin).length());
    distance = std::max(distance, (seg0->tail - origin).length());
    distance = std::max(distance, (seg1->head - origin).length());
    distance = std::max(distance, (seg1->tail - origin).length());
    if (distance < Precision::Confusion()) return;
    osg::Vec3d temp(distance * 1.1, 0.0, 0.0);
    osg::Quat rotation(angle / 2.0, osg::Vec3d(0.0, 0.0, 1.0));
    textPosition = rotation * temp;
    state = TextPosition;
  }
  
  void calcRangeMasks()
  {
    // we can't just use distance as we negative numbers so we project onto line.
    range0 = seg0->makeRange(origin);
    range1 = seg1->makeRange(origin);
    state = RangeMasks;
  }
};

using namespace tls;

Angle::Angle() : stow(std::make_unique<Stow>()){}
Angle::~Angle() = default;

void Angle::addSegment(const TopoDS_Edge &edgeIn, OVec ovecIn)
{
  if (stow->state != Initial) return; //not designed to be used more than once.
  auto p0 = gu::toOsg(BRep_Tool::Pnt(TopExp::FirstVertex(edgeIn, true)));
  auto p1 = gu::toOsg(BRep_Tool::Pnt(TopExp::LastVertex(edgeIn, true)));
  if (!p0.valid() || !p1.valid()) return;
  if ((p1 - p0).length() < Precision::Confusion()) return;
  if (ovecIn)
  {
    auto dist0 = (*ovecIn - p0).length();
    auto dist1 = (*ovecIn - p1).length();
    if (dist0 < dist1) std::swap(p0, p1);
  }
  stow->addSegment(Segment{p1, p0});
}

void Angle::addSegment(const osg::Vec3d &headIn, const osg::Vec3d &tailIn)
{
  if (stow->state != Initial) return; //not designed to be used more than once.
  if (!headIn.valid() || !tailIn.valid()) return;
  if ((headIn - tailIn).length() < Precision::Confusion()) return;
  stow->addSegment(Segment{headIn, tailIn});
}

void Angle::process(OVec helperZ)
{
  stow->helper = helperZ;
  if (stow->state < Ready) return;
  stow->calcAngle(); if (stow->state < Ang) return;
  stow->calcOrigin(); if (stow->state < Origin) return;
  stow->calcSystem(); if (stow->state < System) return;
  stow->calcTextPosition(); if (stow->state < TextPosition) return;
  stow->calcRangeMasks();
}

void Angle::processSensed(OVec helperZ)
{
  stow->helper = helperZ;
  if (stow->state < Ready) return;
  stow->calcAngle(); if (stow->state < Ang) return;
  stow->calcOrigin(); if (stow->state < Origin) return;
  bool sense0 = stow->seg0->orient(stow->origin);
  bool sense1 = stow->seg1->orient(stow->origin);
  stow->other = sense0 != sense1;
  if (stow->other)
  {
    stow->angle = osg::PI - stow->angle;
    stow->helper = -(*stow->helper);
  }
  stow->calcSystem(); if (stow->state < System) return;
  stow->calcTextPosition(); if (stow->state < TextPosition) return;
  stow->calcRangeMasks();
}

Angle::State Angle::getState() const
{
  return stow->state;
}

ang::Mask Angle::getMask() const
{
  return stow->mask;
}

double Angle::getAngle() const
{
  return stow->angle;
}

osg::Vec3d Angle::getOrigin() const
{
  return stow->origin;
}

bool Angle::getOther() const
{
  return stow->other;
}

osg::Matrixd Angle::getSystem() const
{
  return stow->system;
}

osg::Vec3d Angle::getTextPosition() const
{
  return stow->textPosition;
}

lbr::RangeMask Angle::getFirstRangeMask() const
{
  return *stow->range0;
}

lbr::RangeMask Angle::getLastRangeMask() const
{
  return *stow->range1;
}

osg::MatrixTransform* Angle::buildAngularDimension()
{
  double angleDeg = osg::RadiansToDegrees(stow->angle);
  auto *text = lbr::PLabel::buildText();
  text->setText(tls::prettyDouble(angleDeg, 3));
  
  auto matrixTransform = new osg::MatrixTransform();
  matrixTransform->setName("theTransform");
  matrixTransform->setMatrix(osg::Matrixd::translate(stow->textPosition));
  
  auto *transform = new osg::AutoTransform();
  transform->addChild(text);
  transform->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_SCREEN);
  transform->setAutoScaleToScreen(true);
  matrixTransform->addChild(transform);
  
  auto *dimension = lbr::buildAngularDimension(matrixTransform);
  dimension->setMatrix(stow->system);
  lbr::AngularDimensionCallback *cb = new lbr::AngularDimensionCallback("theTransform");
  cb->setAngleDegrees(angleDeg);
  if (stow->range0) cb->setRangeMask1(*stow->range0);
  if (stow->range1) cb->setRangeMask2(*stow->range1);
  dimension->setUpdateCallback(cb);
  
  return dimension;
}

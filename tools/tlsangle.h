/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2024  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TLS_ANGLE_H
#define TLS_ANGLE_H

#include <memory>
#include <bitset>
#include <optional>

#include <osg/Vec3d>

class TopoDS_Edge;
namespace osg{class Matrixd; class MatrixTransform;}
namespace lbr{struct RangeMask;}

namespace tls
{
  //didn't want to do this!!!!! can't make constant bitsets in class without init in source. Yuck.
  namespace ang
  {
    using Mask = std::bitset<4>;
    constexpr Mask Unknown{0};  //!< Default error state. segments state unknown
    constexpr Mask Coplanar{1}; //!< segments exist on same plane
    constexpr Mask Colinear{2}; //!< segments exist on same line
    constexpr Mask Parallel{4}; //!< segments are parallel 0 or 180 degs
    constexpr Mask Skew{8};     //!< segments have none of the above
  }
  
  
  /*! @struct Angle
   * 
   * @brief Help placing angle dimensions from 2 line segments
   * @details Designed to be only used once. Add 2 segments, process,
   * inspect and throw away. State describes the succession of operations.
   * If any operation fails, processing will stop and user can test
   * state to determine what data can safely be extracted. User can
   * also test mask to see the nature of the defining vectors.
   * 
   * The deal with process and processSensed. We use this inside the command
   * to measure angles. Inside that command the user dictates the direction/sense
   * of the lines/vectors and so we use process there. We also use this inside
   * the sketcher. There the selection mechanics don't allow the user to specify
   * direction/sense, so we have to detect it with processSensed.
   */
  struct Angle
  {
    enum State
    {
      Initial //!< 2 segments not defined. initial state
      , Ready //!< 2 segments ready to process
      , Ang //!< process successfully calculated angle.
      , Origin //!< process successfully calculated origin of dimension.
      , System //!< process successfully calculated system of dimension.
      , TextPosition //!< process successfully calculated text position of dimension.
      , RangeMasks //!< process successfully calculated range masks.
    };
    
    using OVec = std::optional<osg::Vec3d>;
    
    Angle();
    ~Angle();
    void addSegment(const TopoDS_Edge&, OVec = std::nullopt); //!< optional for direction sense.
    void addSegment(const osg::Vec3d&, const osg::Vec3d&); //!< first is head, second is tail.
    void process(OVec = std::nullopt); //!< Optional vec to determine sense
    void processSensed(OVec = std::nullopt); //!< see getOther.
    State getState() const;
    ang::Mask getMask() const;
    
    //always safe to call these but results might be meaningless without proper state after process.
    double getAngle() const; //!< Radians
    osg::Vec3d getOrigin() const;
    bool getOther() const; //!< a solvespace thing.
    osg::Matrixd getSystem() const;
    osg::Vec3d getTextPosition() const; //!< position is in system space.
    lbr::RangeMask getFirstRangeMask() const;
    lbr::RangeMask getLastRangeMask() const;
    osg::MatrixTransform* buildAngularDimension(); //!< static dimension, i.e. measure angle command.
  private:
    struct Stow;
    std::unique_ptr<Stow> stow;
  };
}

#endif //TLS_ANGLE_H

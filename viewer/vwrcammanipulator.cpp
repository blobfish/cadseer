/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>
#include <bitset>
#include <optional>
#include <limits>
#include <queue>

#include "preferences/preferencesXML.h"
#include "preferences/prfmanager.h"
#include "message/msgnode.h"
#include "viewer/vwrstandardviews.h"
#include "viewer/vwrspaceballosgevent.h"
#include "viewer/vwrcammanipulator.h"

namespace
{
  /* develop notes. We are tracking middle button state but we are not doing anything
   * with it right now. Our camera manipulator is added to the view before our
   * selection handler, so if we wanted to do something with ctrl + middle
   * click, I think we can do it without too much head-ache. Thinking about 'fit'
   */
  
  //Keep track of buttons and keys to determine if we should handle events.
  using StateMask = std::bitset<32>;
  static const StateMask ControlKey(StateMask().set(0));
  static const StateMask LeftButton(StateMask().set(1));
  static const StateMask MiddleButton(StateMask().set(2));
  static const StateMask RightButton(StateMask().set(3));
  static const StateMask Rotate(ControlKey | LeftButton);
  static const StateMask Pan(ControlKey | RightButton);
  
  struct ProjectionInfo
  {
    vwr::CamManipulator::CamType camType;
    double left = 0.0;
    double right = 0.0;
    double bottom = 0.0;
    double top = 0.0;
    double zNear = 0.0;
    double zFar = 0.0;
    
    explicit ProjectionInfo(const osg::Matrixd &mIn)
    {
      camType = vwr::CamManipulator::detectCamType(mIn);
      
      switch (camType)
      {
        case vwr::CamManipulator::None: {assert(0); return;}
        case vwr::CamManipulator::Ortho:
        {
          mIn.getOrtho(left, right, bottom, top, zNear, zFar);
          break;
        }
        case vwr::CamManipulator::Perspective:
        {
          mIn.getFrustum(left, right, bottom, top, zNear, zFar);
          break;
        }
      }
    }
    bool isValid() const
    {
      if (camType == vwr::CamManipulator::None) return false;
      if (width() < std::numeric_limits<float>::epsilon()) return false;
      if (height() < std::numeric_limits<float>::epsilon()) return false;
      return true;
    }
    double width() const {return (right - left);}
    double height() const {return (top - bottom);}
    double aspectRatio() const {return width() / height();}
    double fovy() const
    {
      assert(isValid());
      assert(camType == vwr::CamManipulator::Perspective);
      return osg::RadiansToDegrees(atan(top/zNear)-atan(bottom/zNear));
    }

    void project(const osg::Vec2d &upperRight)
    {
      osg::Vec2d ur(right, top);
      ur += upperRight;
      right = ur.x();
      top = ur.y();
      
      osg::Vec2d ll(left, bottom);
      ll += -upperRight;
      left = ll.x();
      bottom = ll.y();
    }
    
    //scales narrowest screen dimension to 2 * halfDim
    void scaleTo(double halfDim)
    {
      double ar = aspectRatio();
      if (ar <= 1.0)
      {
        left = -halfDim;
        right = halfDim;
        bottom = left / ar;
        top = right / ar;
      }
      else
      {
        bottom = -halfDim;
        top = halfDim;
        left = bottom * ar;
        right = top * ar;
      }
    }
    
    //return true if scale happens.
    bool floor(double eps)
    {
      if (std::min(right, top) < eps)
      {
        scaleTo(eps);
        return true;
      }
      return false;
    }
    
    osg::Matrixd toOrtho() const
    {
      return osg::Matrixd::ortho(left, right, bottom, top, zNear, zFar);
    }
  };
  
  [[maybe_unused]] std::ostream& operator<<(std::ostream &stream, const ProjectionInfo &pInfo)
  {
    assert(pInfo.isValid());
    
    std::string typeString = "None";
    if (pInfo.camType == vwr::CamManipulator::CamType::Ortho) typeString = "Ortho";
    if (pInfo.camType == vwr::CamManipulator::CamType::Perspective) typeString = "Perspective";
    
    stream << "Projection Info:" << std::endl
    << "  camera type: " << typeString << std::endl
    << "  left: " << pInfo.left << std::endl
    << "  right: " << pInfo.right << std::endl
    << "  bottom: " << pInfo.bottom << std::endl
    << "  top: " << pInfo.top << std::endl
    << "  near: " << pInfo.zNear << std::endl
    << "  far: " << pInfo.zFar << std::endl
    << "  width: " << pInfo.width() << std::endl
    << "  height: " << pInfo.height() << std::endl
    << "  aspect ratio: " << pInfo.aspectRatio() << std::endl;
    if (pInfo.camType == vwr::CamManipulator::CamType::Perspective)
      stream << "  fovy: " << pInfo.fovy() << std::endl;
    return stream;
  }
  
  //transient data only valid through updateCamera or handle call stack.
  struct Payload
  {
    osg::Matrixd viewMatrix;
    osg::Matrixd projectionMatrix;
    osg::Matrixd windowMatrix;
    
    osg::Matrixd VPW;
    osg::Matrixd WPV;
    
    double windowWidth = 1.0;
    double windowHeight = 1.0;
    
    vwr::CamManipulator::CamType camType;
    
    Payload() = delete;
    Payload(const osg::Matrixd &vmIn, osgGA::GUIActionAdapter &aa)
    : Payload(vmIn, aa.asView()->getCamera())
    {}
    Payload(const osg::Matrixd &vmIn, const osg::Camera *cameraIn)
    : viewMatrix(vmIn)
    {
      assert(cameraIn); if (!cameraIn) return;
      projectionMatrix = cameraIn->getProjectionMatrix();
      windowMatrix = cameraIn->getViewport()->computeWindowMatrix();
      windowWidth = cameraIn->getViewport()->width();
      windowHeight = cameraIn->getViewport()->height();
      compute();
    }
    
    bool isValid() const {return valid;}
    
  private:
    bool valid = false;
    
    void compute()
    {
      VPW = viewMatrix * projectionMatrix * windowMatrix;
      WPV = osg::Matrixd::inverse(VPW);
      camType = vwr::CamManipulator::detectCamType(projectionMatrix);
      validate();
    }
    
    void validate()
    {
      valid = VPW.valid() && WPV.valid() && camType != vwr::CamManipulator::CamType::None;
    }
  };
  
  /* build a plane tangent to a sphere. sphere center is projected in direction, onto sphere.
   * why all this, you ask:
   * Quite a lot of preconditions for valid construction.
   * I wanted to be able to verify these before and avoid a lot of
   * optionals or other error handling techniques.
   */
  struct TangentSpace
  {
    const osg::BoundingSphere &sphere;
    osg::Vec3d direction;
    
    TangentSpace() = delete;
    TangentSpace(const osg::BoundingSphere &bsIn, const osg::Vec3d &dIn)
    : sphere(bsIn)
    , direction(dIn)
    {}
    
    bool isValid()
    {
      return 
      (
        direction.valid()
        && direction.length() > std::numeric_limits<float>::epsilon()
        && sphere.valid()
        && sphere.radius() > std::numeric_limits<float>::epsilon()
      );
    }
    
    //plane normal is pointing toward sphere center.
    osg::Plane build()
    {
      assert(isValid());
      direction.normalize();
      osg::Vec3d planePoint = sphere.center() + direction * sphere.radius();
      osg::Plane out(-direction, planePoint);
      assert(out.valid());
      return out;
    }
  };
  
  /* store for todo operations. created in handle and applied in updateCamera.
   * This follows separation manipulators where data is calculated and stored in event handlers
   * and actually applied in cameraUpdate. See note preceding class declaration.
   */
  struct Operation
  {
    enum Type
    {
      None
      , ScaleView
      , Convert
      , Fit
      , FitBox
    };
    Type type;
    double value = 1.0;
    double halfWidth = 1.0;
    double halfHeight = 1.0;
    vwr::CamManipulator::CamType newCamType;
    
    static Operation scaleView(double vIn)
    {
      Operation out;
      out.type = Type::ScaleView;
      out.value = vIn;
      return out;
    }
    
    static Operation convert(vwr::CamManipulator::CamType tIn)
    {
      Operation out;
      out.type = Convert;
      out.newCamType = tIn;
      return out;
    }
    
    static Operation fit()
    {
      Operation out;
      out.type = Type::Fit;
      return out;
    }
    
    static Operation fitBox(double halfWidthIn, double halfHeightIn)
    {
      Operation out;
      out.type = Type::FitBox;
      out.halfWidth = halfWidthIn;
      out.halfHeight = halfHeightIn;
      return out;
    }
  };
  
}

using namespace vwr;

struct CamManipulator::Stow
{
  CamManipulator &camManipulator;
  CamManipulator::CamType camType = Ortho; // we set ortho by default.
  osg::BoundingSphere camSphere; //we pin ortho camera to bounding sphere.
  double fovy = 30.0; //user adjustable?
  StateMask currentStateMask;
  osg::Vec2f lastScreenPoint;
  double rotationFactor = 0.0;
  double translationFactor = 0.0;
  
  std::queue<Operation> operations;
  std::optional<osg::Matrixd> projectionOut;
  
  osg::Vec3d center;
  osg::Quat rotation;
  double distance = 1.0;
  std::optional<osg::Vec3d> rotationPoint;
  std::optional<osg::Vec3d> zoomPoint;
  
  Stow(CamManipulator &cIn) : camManipulator(cIn)
  {
    setDefaultCamSphere();
    setDefaultFov();
  }
  
  void convertProjectionType(CamType tIn, const Payload &plIn)
  {
    if (camType == tIn) return;
    switch (tIn)
    {
      case CamType::None:
      {
        assert(0);
        return;
        break;
      }
      case CamType::Ortho: // from perspective to ortho
      {
        ProjectionInfo pInfo(plIn.projectionMatrix);
        assert(pInfo.isValid()); if (!pInfo.isValid()) return;
        assert(pInfo.camType == CamType::Perspective); if (pInfo.camType != CamType::Perspective) return;
        projectionOut = osg::Matrixd::ortho(pInfo.left, pInfo.right, pInfo.bottom, pInfo.top, pInfo.zNear, pInfo.zFar);
        projectToBound();
        break;
      }
      case CamType::Perspective: // from ortho to persepective
      {
        ProjectionInfo opi(plIn.projectionMatrix); //old projection info
        assert(opi.isValid()); if (!opi.isValid()) return;
        assert(opi.camType == CamType::Ortho); if (opi.camType != CamType::Ortho) return;
        
        /* Move cam along look vector to get 'equal' view
         * We do this to compensate for the ortho camera being 'pinned' to the bounding sphere.
         */
        double dc = opi.top / std::tan(osg::DegreesToRadians(fovy / 2.0)); //camera distance from the near plane
        double nd = distance - opi.zNear + dc; // new distance.
        double dd = distance - nd; //delta distance.
        double nnp = dc; //new near plane
        double nfp = opi.zFar -dd; //new far plane.
        projectionOut = osg::Matrixd::perspective(fovy, opi.aspectRatio(), nnp, nfp);
        distance = nd;
        break;
      }
    }
    camType = tIn;
  }
  
  void scaleView(double zMovement, const Payload &plIn)
  {
    /* this is for ortho camera. zMovement is what we would have moved the camera in 'z',
     * if it was a perspective camera. We calculate the change in projection height based up
     * the fovy and the length of zMovement. Projection width is then calculated from the height
     * change and the aspect ratio.
     */
    
    ProjectionInfo pInfo(plIn.projectionMatrix);
    assert(pInfo.isValid()); if (!pInfo.isValid()) return;
    assert(pInfo.camType == CamType::Ortho); if (pInfo.camType != CamType::Ortho) return;
    
    double dy = std::tan(osg::DegreesToRadians(fovy / 2.0)) * zMovement;
    double dx = dy * pInfo.aspectRatio();
    osg::Vec2d project(dx, dy);
    pInfo.project(-project);
    pInfo.floor(0.00001);
    
    projectionOut = pInfo.toOrtho();
  }
  
  void fit(const Payload &plIn)
  {
    ProjectionInfo pInfo(plIn.projectionMatrix); assert(pInfo.isValid()); if (!pInfo.isValid()) return;
    if (plIn.camType == Ortho)
    {
      center = camSphere.center();
      pInfo.scaleTo(camSphere.radius());
      projectionOut = pInfo.toOrtho();
      projectToBound();
    }
    else if (plIn.camType == Perspective)
    {
      center = camSphere.center();
      distance = camSphere.radius() / std::sin(osg::DegreesToRadians(pInfo.fovy()) / 2.0);
    }
  }
  
  //assumes camera position is set and this just scales the projection or moves the camera distance.
  void fitBox(const Payload &plIn, double halfWidthIn, double halfHeightIn)
  {
    ProjectionInfo pInfo(plIn.projectionMatrix);
    if (!pInfo.isValid()) return;
    if (halfWidthIn < std::numeric_limits<float>::epsilon() || halfHeightIn < std::numeric_limits<float>::epsilon()) return;
    //assuming symmetric projection and using upper right corner.
    osg::Vec2d ourc(pInfo.right, pInfo.top);
    osg::Vec2d nurc(halfWidthIn, halfHeightIn);
    double xScale = nurc.x() / ourc.x();
    double yScale = nurc.y() / ourc.y();
    double scale = std::max(xScale, yScale);
    double dy = ourc.y() * scale - ourc.y();
    
    if (pInfo.camType == Ortho)
    {
      osg::Vec2d py(0.0, 1.0 * dy);
      osg::Vec2d px(py.y() * pInfo.aspectRatio(), 0.0);
      pInfo.project(px + py);
      if (!pInfo.isValid()) return;
      projectionOut = pInfo.toOrtho();
    }
    else if (pInfo.camType == Perspective)
    {
      double dz = dy / std::tan(osg::DegreesToRadians(fovy / 2.0));
      distance += dz;
    }
  }
  
  //we make narrowest screen dimension equal to 1 complete revolution.
  void setRotationFactor(const osg::Matrixd &projectionMatrix)
  {
    ProjectionInfo pInfo(projectionMatrix);
    if (!pInfo.isValid())
    {
      rotationFactor = 0.0;
      return;
    }
    rotationFactor = std::min(pInfo.width(), pInfo.height()) / (2.0 * osg::PI);
  }
  
  void setSpaceballRotationFactor()
  {
    // 512 is max value from my magellan plus xt. we make that max value 1 full rotation.
    const auto &sensi = prf::manager().rootPtr->input()->spaceball().get();
    rotationFactor = 2.0 * osg::PI / 512.0 * sensi.overallSensitivity().get() * sensi.rotationsSensitivity().get();
  }
  
  void setSpaceballTranslationFactor(const osg::Matrixd &projectionMatrix)
  {
    ProjectionInfo pInfo(projectionMatrix);
    if (!pInfo.isValid())
    {
      translationFactor = 0.0;
      return;
    }
    const auto &sensi = prf::manager().rootPtr->input()->spaceball().get();
    translationFactor = std::min(pInfo.width(), pInfo.height()) / 512.0
      * sensi.overallSensitivity().get() * sensi.translationsSensitivity().get();
  }
  
  void setDefaultCamSphere()
  {
    camSphere = osg::BoundingSphere(osg::Vec3d(0.0, 0.0, 0.0), 15.0);
  }
  
  void setDefaultFov()
  {
    //from osg/View.cpp:30
    double height = osg::DisplaySettings::instance()->getScreenHeight();
    double distance = osg::DisplaySettings::instance()->getScreenDistance();
    fovy = osg::RadiansToDegrees(atan2(height / 2.0, distance) * 2.0);
    fovy = std::clamp(fovy, 1.0, 179.0);
  }
  
  //move camera origin to tangent space of bounding sphere.
  //plane point is the camSphere center projected campSphere radius along the look vector;
  //update center to camSphere radius distance, along look vector from the eye.
  void projectToBound()
  {
    osg::Vec3d lookProjection = rotation * osg::Vec3d(0.0, 0.0, 1.0);
    TangentSpace tSpace(camSphere, lookProjection);
    if (!tSpace.isValid()) return;
    osg::Plane plane = tSpace.build();
    distance = plane.distance(center);
  }
  
  void processOperations(const Payload &pl)
  {
    while (!operations.empty())
    {
      const auto &op = operations.front();
      if (op.type == Operation::Convert) convertProjectionType(op.newCamType, pl);
      else if (op.type == Operation::ScaleView) scaleView(op.value, pl);
      else if (op.type == Operation::Fit) fit(pl);
      else if (op.type == Operation::FitBox) fitBox(pl, op.halfWidth, op.halfHeight);
      operations.pop();
    }
  }
  
  void goLeftMouseButtonPush(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
  {
    currentStateMask |= LeftButton;
    lastScreenPoint = osg::Vec2f(ea.getX(), ea.getY());
    Payload pl(camManipulator.getInverseMatrix(), aa);
    setRotationFactor(pl.projectionMatrix);
  }
  
  void goLeftMouseButtonRelease(const osgGA::GUIEventAdapter&)
  {
    currentStateMask &= ~LeftButton;
  }
  
  void goMiddleMouseButtonPush(const osgGA::GUIEventAdapter&)
  {
    currentStateMask |= MiddleButton;
  }
  
  void goMiddleMouseButtonRelease(const osgGA::GUIEventAdapter&)
  {
    currentStateMask &= ~MiddleButton;
  }
  
  void goRightMouseButtonPush(const osgGA::GUIEventAdapter &ea)
  {
    currentStateMask |= RightButton;
    lastScreenPoint = osg::Vec2f(ea.getX(), ea.getY());
  }
  
  void goRightMouseButtonRelease(const osgGA::GUIEventAdapter&)
  {
    currentStateMask &= ~RightButton;
  }
  
  void goScroll(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa, double directionFactor)
  {
    Payload pl(camManipulator.getInverseMatrix(), aa);
    assert(pl.isValid());
    if (!pl.isValid()) return;
    ProjectionInfo pInfo(pl.projectionMatrix);
    assert(pInfo.isValid());
    if (!pInfo.isValid()) return;
    
    double feelFactor = 0.05; // 10% of view y, we work on half view y.
    double userPref = prf::manager().rootPtr->input().get().mouse().get().wheelZoomFactor().get();
    double scroll = feelFactor * userPref;
    
    /* we use the view y size to calculate how much a scroll will
     * move in z. I tried to use a factor of bounding sphere but we
     * need zoom affect to be relative to the current view size otherwise
     * zoom is to slow away from model and to fast near model.
     */
    osg::Vec3d nearCamera(pl.windowWidth / 2.0, pl.windowHeight / 2.0, 0.0);
    auto worldCamera = nearCamera * pl.WPV;
    
    osg::Vec3d worldNearPoint;
    if (zoomPoint)
    {
      auto nearPoint = *zoomPoint * pl.VPW;
      nearPoint.z() = 0.0;
      worldNearPoint = nearPoint * pl.WPV;
    }
    else
    {
      worldNearPoint = osg::Vec3d(ea.getX(), ea.getY(), 0.0) * pl.WPV;
    }
    
    //base movement off of current clip.
    double dy = pInfo.top * scroll;
    // movement in view XY camera plane.
    auto moveNearPlane = worldNearPoint - worldCamera;
    moveNearPlane *= scroll * directionFactor;
    center += moveNearPlane;
    
    if (pInfo.camType == CamManipulator::Ortho)
    {
      double dz = dy / std::sin(osg::DegreesToRadians(fovy / 2.0)) * directionFactor;
      operations.push(Operation::scaleView(dz));
    }
    else if (pInfo.camType == CamManipulator::Perspective)
    {
      // we have to put a floor on movement or view freezes as we get close to front clipping plane.
      // I have see this behavior in blender at some point in time.
      dy = std::max(0.1, dy);
      double dz = dy / std::sin(osg::DegreesToRadians(fovy / 2.0)) * directionFactor;
      auto tempDistance = distance - dz;
      if (tempDistance <= std::numeric_limits<float>::epsilon())
      {
        //push back the center.
        osg::Vec3d zMove(0.0, 0.0, -dz);
        zMove = rotation * zMove;
        center += zMove;
      }
      else
      {
        distance = tempDistance;
      }
    }
  }
  
  void goDragRotate(const osg::Vec3d &move3d)
  {
    //rotation axis will be normal to drag vector.
    osg::Vec3d camZ = rotation * osg::Vec3d(0.0, 0.0, 1.0);
    osg::Vec3d axis = move3d ^ camZ;
    if (!axis.valid()) return;
    axis.normalize();
    osg::Quat mouseRotation(move3d.length() / rotationFactor, axis);
    
    osg::Vec3d rp = center;
    if (rotationPoint) rp = *rotationPoint;
    
    osg::Matrixd combo = osg::Matrixd::translate(-rp)
      * osg::Matrixd(mouseRotation)
      * osg::Matrixd::translate(rp);
    center = center * combo;
    
    rotation *= mouseRotation;
    if (camType == CamManipulator::CamType::Ortho) projectToBound();
  }
  
  void goDragPan(const osg::Vec3d &move3d)
  {
    center += -move3d;
    //shouldn't need to project ortho cam onto tangent space.
  }
  
  bool handleMouse(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
  {
    //nothing works unless the control is down.
    //we don't have to worry about control key itself, because that is handled by the qt event filter
    if ((currentStateMask & ControlKey).none()) return false;
    
    switch (ea.getEventType())
    {
      case osgGA::GUIEventAdapter::PUSH:
      {
        if (ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON)
        {
          goLeftMouseButtonPush(ea, aa);
          return true;
        }
        else if (ea.getButton() == osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON)
        {
          goMiddleMouseButtonPush(ea);
          return true;
        }
        else if (ea.getButton() == osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON)
        {
          goRightMouseButtonPush(ea);
          return true;
        }
        break;
      }
      case osgGA::GUIEventAdapter::RELEASE:
      {
        if (ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON)
        {
          goLeftMouseButtonRelease(ea);
          return true;
        }
        else if (ea.getButton() == osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON)
        {
          goMiddleMouseButtonRelease(ea);
          return true;
        }
        else if (ea.getButton() == osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON)
        {
          goRightMouseButtonRelease(ea);
          return true;
        }
        break;
      }
      case osgGA::GUIEventAdapter::SCROLL:
      {
        std::optional<double> scaleDirection;
        if (ea.getScrollingMotion() == osgGA::GUIEventAdapter::SCROLL_DOWN) scaleDirection = 1.0;
        else if (ea.getScrollingMotion() == osgGA::GUIEventAdapter::SCROLL_UP) scaleDirection = -1.0;
        if (scaleDirection)
        {
          goScroll(ea, aa, *scaleDirection);
          aa.requestRedraw();
          return true;
        }
        break;
      }
      case osgGA::GUIEventAdapter::DRAG:
      {
        osg::Vec3d movement3d;
        auto assignMovement = [&]() -> bool
        {
          Payload pl(camManipulator.getInverseMatrix(), aa);
          if (!pl.isValid()) return false;
          osg::Vec3d oldPoint(lastScreenPoint.x(), lastScreenPoint.y(), 0.0);
          osg::Vec3d newPoint(ea.getX(), ea.getY(), 0.0);
          oldPoint = oldPoint * pl.WPV;
          newPoint = newPoint * pl.WPV;
          movement3d = newPoint - oldPoint;
          return
          (
            movement3d.valid()
            && movement3d.length() > static_cast<double>(std::numeric_limits<float>::epsilon())
          );
        };
        
        auto post = [&]()
        {
          lastScreenPoint = osg::Vec2f(ea.getX(), ea.getY());
          aa.requestRedraw();
        };
        
        if (currentStateMask == Rotate)
        {
          if (!assignMovement()) return true;
          goDragRotate(movement3d);
          post();
          return true;
        }
        else if (currentStateMask == Pan)
        {
          if (!assignMovement()) return true;
          goDragPan(movement3d);
          post();
          return true;
        }
        break;
      }
      default: {break;}
    }
    
    return false;
  }
  
  /* This has been adapted from previous manipulator implementation. I didn't have a spaceball
   * with me when I did this, so ... bugs. Original implementation used the eye, center, and up scheme.
   * Here I converted to the distance, rotation, center scheme, so more bugs. magic number of 512
   * is the max value of my magellan xt plus. I will make that feel natural, other devices with different
   * range values will have to use preferences to get natural feel.
   */
  bool handleSpaceball(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
  {
    if (ea.getEventType() != osgGA::GUIEventAdapter::USER) return false;
    
    const vwr::SpaceballOSGEvent *event = static_cast<const vwr::SpaceballOSGEvent *>(ea.getUserData());
    if (!event) return false;
    Payload payload(camManipulator.getInverseMatrix(), aa); if (!payload.isValid()) return false;
    
    //rotation
    setSpaceballRotationFactor();
    osg::Quat rx(event->rotationX * rotationFactor, rotation * osg::Vec3d(1.0, 0.0, 0.0));
    osg::Quat ry(event->rotationY * rotationFactor, rotation * osg::Vec3d(0.0, 1.0, 0.0));
    osg::Quat rz(event->rotationZ * rotationFactor, rotation * osg::Vec3d(0.0, 0.0, 1.0));
    osg::Quat rAll = rx * ry * rz;
    
    rotation *= rAll;
    osg::Vec3d rp = center;
    if (rotationPoint) rp = *rotationPoint;
    osg::Matrixd combo = osg::Matrixd::translate(-rp)
    * osg::Matrixd(rAll)
    * osg::Matrixd::translate(rp);
    center = center * combo;
    
    //translation
    setSpaceballTranslationFactor(payload.projectionMatrix);
    osg::Vec3d tx = rotation * osg::Vec3d(event->translationX * translationFactor, 0.0, 0.0);
    osg::Vec3d ty = rotation * osg::Vec3d(0.0, event->translationY * translationFactor, 0.0);
    osg::Vec3d tz = rotation * osg::Vec3d(0.0, 0.0, event->translationZ * translationFactor);
    osg::Vec3d tPlane = rotation * (tx + ty); //tz affects distance.
    
    center += tPlane;
    
    if (camType == CamManipulator::Ortho)
    {
      operations.push(Operation::scaleView(tz.length()));
      projectToBound();
    }
    else if (camType == CamManipulator::Perspective)
    {
      distance -= tz.length();
      double lo = std::numeric_limits<float>::epsilon();
      if (distance >= 0.0) distance = std::clamp(distance, lo, std::numeric_limits<double>::max());
      else distance = std::clamp(distance, std::numeric_limits<double>::lowest(), -lo);
    }
    
    aa.requestRedraw();
    return true;
  }
};

CamManipulator::CamManipulator()
: osgGA::CameraManipulator()
, stow(std::make_unique<Stow>(*this))
{}

CamManipulator::CamManipulator(const CamManipulator& manipIn, const osg::CopyOp& copyOp)
: osg::Object(manipIn, copyOp)
, osg::Callback(manipIn, copyOp)
, osgGA::CameraManipulator(manipIn, copyOp)
, stow(std::make_unique<Stow>(*this))
{}

CamManipulator::~CamManipulator() = default;


void CamManipulator::setByMatrix(const osg::Matrixd &mIn)
{
  stow->center = osg::Vec3d(0.0, 0.0, -stow->distance) * mIn;
  stow->rotation = mIn.getRotate();
}

void CamManipulator::setByInverseMatrix(const osg::Matrixd &mIn)
{
  setByMatrix(osg::Matrixd::inverse(mIn));
}

osg::Matrixd CamManipulator::getMatrix() const
{
  return osg::Matrixd::translate(0.0, 0.0, stow->distance)
    * osg::Matrixd::rotate(stow->rotation)
    * osg::Matrixd::translate(stow->center);
}

osg::Matrixd CamManipulator::getInverseMatrix() const
{
  return osg::Matrixd::translate(-stow->center)
    * osg::Matrixd::rotate(stow->rotation.inverse())
    * osg::Matrixd::translate(0.0, 0.0, -stow->distance);
}

bool CamManipulator::handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
{
  aa.requestContinuousUpdate(false); //don't want animation
  
  if (ea.getEventType() == osgGA::GUIEventAdapter::KEYUP)
  {
    double translationFactor = 1.0;
    auto assignTranslationFactor = [&]() -> bool
    {
      Payload pl(getInverseMatrix(), aa);
      if (!pl.isValid()) return false;
      ProjectionInfo pInfo(pl.projectionMatrix);
      if (!pInfo.isValid()) return false;
      double base = std::min(pInfo.width(), pInfo.height());
      translationFactor = base / 10.0;
      return true;
    };
    auto goRotate = [&](const osg::Vec3d &axisIn)
    {
      osg::Quat localRotation(osg::PI_4 / 4.0, axisIn);
      if (stow->rotationPoint)
      {
        osg::Matrixd combo = osg::Matrixd::translate(-*stow->rotationPoint)
        * osg::Matrixd(localRotation)
        * osg::Matrixd::translate(*stow->rotationPoint);
        stow->center = stow->center * combo;
      }
      stow->rotation *= localRotation;
      if (stow->camType == CamManipulator::CamType::Ortho) stow->projectToBound();
    };
    auto goZoom = [&](double movement)
    {
      if (stow->camType == CamManipulator::Ortho)
        stow->operations.push(Operation::scaleView(movement));
      else if (stow->camType == CamManipulator::Perspective)
      {
        stow->distance -= movement;
        double lo = std::numeric_limits<float>::epsilon();
        if (stow->distance >= 0.0) stow->distance = std::clamp(stow->distance, lo, std::numeric_limits<double>::max());
        else stow->distance = std::clamp(stow->distance, std::numeric_limits<double>::lowest(), -lo);
      }
    };
    bool isShiftDown = ea.getModKeyMask() & osgGA::GUIEventAdapter::KEY_Shift_L
      || ea.getModKeyMask() & osgGA::GUIEventAdapter::KEY_Shift_R;
    
    // boy this feels a little hacky doing this outside the command structure.
    switch (ea.getKey())
    {
      case 'p': {toggleCamType(); return true;}
      case 'h': {fit(); return true;}
      case 't': {setView(StandardViews::top()); return true;}
      case 'f': {setView(StandardViews::front()); return true;}
      case 'r': {setView(StandardViews::right()); return true;}
      case 'b': {setView(StandardViews::bottom()); return true;}
      case 'k': {setView(StandardViews::back()); return true;}
      case 'l': {setView(StandardViews::left()); return true;}
      case 'i': {setView(StandardViews::iso()); return true;}
      case osgGA::GUIEventAdapter::KEY_Left:
      {
        if (isShiftDown)
        {
          auto axis = stow->rotation * osg::Vec3d(0.0, 1.0, 0.0);
          goRotate(axis);
        }
        else
        {
          auto trans = stow->rotation * osg::Vec3d(-1.0, 0.0, 0.0);
          if (!assignTranslationFactor()) return true;
          trans *= translationFactor;
          stow->goDragPan(trans);
        }
        return true;
      }
      case osgGA::GUIEventAdapter::KEY_Right:
      {
        if (isShiftDown)
        {
          auto axis = stow->rotation * osg::Vec3d(0.0, -1.0, 0.0);
          goRotate(axis);
        }
        else
        {
          auto trans = stow->rotation * osg::Vec3d(1.0, 0.0, 0.0);
          if (!assignTranslationFactor()) return true;
          trans *= translationFactor;
          stow->goDragPan(trans);
        }
        return true;
      }
      case osgGA::GUIEventAdapter::KEY_Down:
      {
        if (isShiftDown)
        {
          auto axis = stow->rotation * osg::Vec3d(-1.0, 0.0, 0.0);
          goRotate(axis);
        }
        else
        {
          auto trans = stow->rotation * osg::Vec3d(0.0, -1.0, 0.0);
          if (!assignTranslationFactor()) return true;
          trans *= translationFactor;
          stow->goDragPan(trans);
        }
        return true;
      }
      case osgGA::GUIEventAdapter::KEY_Up:
      {
        if (isShiftDown)
        {
          auto axis = stow->rotation * osg::Vec3d(1.0, 0.0, 0.0);
          goRotate(axis);
        }
        else
        {
          auto trans = stow->rotation * osg::Vec3d(0.0, 1.0, 0.0);
          if (!assignTranslationFactor()) return true;
          trans *= translationFactor;
          stow->goDragPan(trans);
        }
        return true;
      }
      case osgGA::GUIEventAdapter::KEY_Page_Up:
      {
        auto moveCameraZ = stow->camSphere.radius() / 5.0;
        goZoom(moveCameraZ);
        return true;
      }
      case osgGA::GUIEventAdapter::KEY_Page_Down:
      {
        auto moveCameraZ = stow->camSphere.radius() / -5.0;
        goZoom(moveCameraZ);
        return true;
      }
    }
  }
  
  if (stow->handleSpaceball(ea, aa)) return true;

  //base class just returns false, so no need to call it.
  return stow->handleMouse(ea, aa);
}

/* updateCamera runs every frame, so very often. I ran a test comparing matrices between calls.
 * This test was benign, meaning no user interaction or modifications of any kind. Just pure OSG.
 * The camera matrix was equal every call.
 * The window matrix was equal every call.
 * The projection matrix NOT equal. It was being slightly changed frequently, something like every other call.
 */
void CamManipulator::updateCamera(osg::Camera& camera)
{
  stow->camType = detectCamType(camera.getProjectionMatrix());
  camera.setViewMatrix(getInverseMatrix());
  stow->processOperations(Payload(getInverseMatrix(), &camera));
  if (stow->projectionOut)
  {
    camera.setProjectionMatrix(*stow->projectionOut);
    stow->projectionOut = std::nullopt;
  }
}

void CamManipulator::getLookAt(osg::Vec3d &eyeIn, osg::Vec3d &centerIn, osg::Vec3d &upIn) const
{
  centerIn = stow->center;
  eyeIn = centerIn + stow->rotation * osg::Vec3d(0.0, 0.0, stow->distance);
  upIn = stow->rotation * osg::Vec3d(0.0, 1.0, 0.0);
}

void CamManipulator::setLookAt(const osg::Vec3d &eyeIn, const osg::Vec3d &centerIn, const osg::Vec3d &upIn)
{
  stow->center = centerIn;
  stow->distance = (centerIn - eyeIn).length();
  stow->rotation = osg::Matrixd::lookAt(eyeIn, centerIn, upIn).getRotate().inverse();
}

void CamManipulator::setBoundingSphere(const osg::BoundingSphere &sIn)
{
  if (!sIn.valid() || sIn.radius() < std::numeric_limits<float>::epsilon()) stow->setDefaultCamSphere();
  else stow->camSphere = sIn;
  if (stow->camType == Ortho) stow->projectToBound();
}

void CamManipulator::setView(const osg::Quat &rIn)
{
  stow->rotation = rIn;
  stow->center = stow->camSphere.center();
  stow->distance = stow->camSphere.radius();
  if (stow->camType == Ortho) stow->projectToBound();
  fit();
}

//set center to matrix trans and set rotation to matrix rotation. no fit/distance.
void CamManipulator::setView(const osg::Matrixd &mIn)
{
  stow->center = mIn.getTrans();
  stow->rotation = mIn.getRotate();
}

void CamManipulator::fit()
{
  stow->operations.push(Operation::fit());
}

/* more exact fit using projected bounding boxes*/
void CamManipulator::fit(const std::vector<osg::BoundingBox> &boxes, const osg::Camera *camera)
{
  Payload pl(getInverseMatrix(), camera); assert(pl.isValid()); if (!pl.isValid()) return;
  ProjectionInfo pInfo(pl.projectionMatrix); assert(pInfo.isValid()); if (!pInfo.isValid()) return;
  assert(pInfo.camType == stow->camType); if (pInfo.camType != stow->camType) return;
  
  //get clip plane normals. default to orthographic. Normals point in.
  osg::Vec3d leftNormal(1.0, 0.0, 0.0);
  osg::Vec3d rightNormal(-1.0, 0.0, 0.0);
  osg::Vec3d bottomNormal(0.0, 1.0, 0.0);
  osg::Vec3d topNormal(0.0, -1.0, 0.0);
  osg::Vec3d frontNormal(0.0, 0.0, -1.0);
  osg::Vec3d backNormal(0.0, 0.0, 1.0);
  osg::Vec3d lrCenterNormal(1.0, 0.0, 0.0);
  osg::Vec3d tbCenterNormal(0.0, 1.0, 0.0);
  
  //rotate these normals if we have a perspective view.
  if (pInfo.camType == CamType::Perspective)
  {
    double verticalAngle = osg::DegreesToRadians(pInfo.fovy() / 2.0);
    double horizontalAngle = std::atan(std::tan(verticalAngle) * pInfo.aspectRatio());
    leftNormal = osg::Quat(horizontalAngle, osg::Vec3d(0.0, 1.0, 0.0)) * leftNormal;
    rightNormal = osg::Quat(horizontalAngle, osg::Vec3d(0.0, -1.0, 0.0)) * rightNormal;
    bottomNormal = osg::Quat(verticalAngle, osg::Vec3d(-1.0, 0.0, 0.0)) * bottomNormal;
    topNormal = osg::Quat(verticalAngle, osg::Vec3d(1.0, 0.0, 0.0)) * topNormal;
  }
  
  //rotate the normals to view orientation
  leftNormal = stow->rotation * leftNormal;
  rightNormal = stow->rotation * rightNormal;
  bottomNormal = stow->rotation * bottomNormal;
  topNormal = stow->rotation * topNormal;
  frontNormal = stow->rotation * frontNormal;
  backNormal = stow->rotation * backNormal;
  lrCenterNormal = stow->rotation * lrCenterNormal;
  tbCenterNormal = stow->rotation * tbCenterNormal;
  
  //get all bounding box points
  //do we really need individual boxes???? yeah I think so, more accurate.
  std::vector<osg::Vec3> worldBoxPoints;
  osg::BoundingBox worldBox;
  for (const auto &b : boxes)
  {
    for (unsigned int i = 0; i < 8; ++i)
    {
      worldBox.expandBy(b.corner(i));
      worldBoxPoints.push_back(b.corner(i));
    }
  }
  if (!worldBox.valid()) return;
  /* bounding boxes are like bounding sphere as a single point can be added
   * and then the object is valid. We don't want that condition here.
   * This might be a problem. What about box around a linear edge?
   */
  if
    (
      worldBox.xMax() == worldBox.xMin()
      && worldBox.yMax() == worldBox.yMin()
      && worldBox.zMax() == worldBox.zMin()
    )
    return;
  
  //loop through all points and find the minimum plane. All other points will have a distance of >= 0.0.
  auto getBoundaryPlane = [&](const osg::Vec3d &normalIn) -> osg::Plane
  {
    osg::Plane out(normalIn, worldBoxPoints.front());
    for (const auto &p : worldBoxPoints) if (out.distance(p) < 0.0) out = osg::Plane(normalIn, p);
    return out;
  };
  std::vector<osg::Plane> planes;
  planes.push_back(getBoundaryPlane(leftNormal));
  planes.push_back(getBoundaryPlane(rightNormal));
  planes.push_back(getBoundaryPlane(bottomNormal));
  planes.push_back(getBoundaryPlane(topNormal));
  planes.push_back(getBoundaryPlane(frontNormal));
  planes.push_back(getBoundaryPlane(backNormal));
  
  if (pInfo.camType == CamType::Perspective)
  {
    //put point into center of planes. This seems to produce some slop.
    //calling this multiple times makes the result more accurate. not sure why.
    //see osgTest project for iterative hack, if needed.
    auto projectToMiddle = []
    (
      const osg::Vec3d &pointIn
      , const osg::Plane &planeIn0
      , const osg::Plane &planeIn1
      , const osg::Vec3d &midPlaneNormal
    ) ->osg::Vec3d
    {
      osg::Plane midPlane(midPlaneNormal, pointIn);
      //project passed in point to passed in planes.
      auto pp0 = pointIn + planeIn0.getNormal() * -planeIn0.distance(pointIn);
      auto pp1 = pointIn + planeIn1.getNormal() * -planeIn1.distance(pointIn);
      //get the difference in distance between each projected point and mid plane.
      auto offset = (midPlane.distance(pp1) + midPlane.distance(pp0)) / 2.0;
      osg::Vec3d out = pointIn + midPlane.getNormal() * offset;
      return out;
    };
    
    auto getIntersectionPoint = [&]
    (
      const osg::Plane &plane0
      , const osg::Plane &plane1
      , const osg::Vec3d &midNormal
      , double halfFOV
    ) -> osg::Vec3d
    {
      auto wbc = projectToMiddle(worldBox.center(), plane0, plane1, midNormal);
      auto point0 = wbc + plane0.getNormal() * -plane0.distance(wbc);
      auto point1 = wbc + plane1.getNormal() * -plane1.distance(wbc);
      auto midPoint = point0 + ((point1 - point0) * 0.5);
      auto distance = (point1 - midPoint).length() / std::tan(halfFOV);
      auto proj = stow->rotation * osg::Vec3d(0.0, 0.0, distance);
      return midPoint + proj;
    };
    double verticalAngle = osg::DegreesToRadians(pInfo.fovy() / 2.0);
    double horizontalAngle = std::atan(std::tan(verticalAngle) * pInfo.aspectRatio());
    auto lrPoint = getIntersectionPoint(planes.at(0), planes.at(1), lrCenterNormal, horizontalAngle);
    auto btPoint = getIntersectionPoint(planes.at(2), planes.at(3), tbCenterNormal, verticalAngle);
    
    /* now we take those 2 points and transfer them into view space.
     * new eye position will have:
     * a x value from the transformed, left right point.
     * a y value from the transformed, bottom top point.
     * a z value that is the max from both transformed, points.
     * transform that new eye position back into world space.
     */
    osg::Matrixd toView = getInverseMatrix();
    osg::Matrixd fromView = getMatrix();
    lrPoint = lrPoint * toView;
    btPoint = btPoint * toView;
    osg::Vec3d location(lrPoint.x(), btPoint.y(), std::max(lrPoint.z(), btPoint.z()));
    location = location * fromView;
    
    //derive our center and distance values from our new eye position and near and far planes.
    double toNear = -planes.at(4).distance(location);
    double toFar = planes.at(5).distance(location);
    double distancePrime = (toFar - toNear) / 2.0 + toNear;
    osg::Vec3d centerPrime(0.0, 0.0, -distancePrime);
    centerPrime = stow->rotation * centerPrime + location;
    stow->center = centerPrime;
    stow->distance = distancePrime;
  }
  else // we have an ortho cam
  {
    auto project = [&](const osg::Plane &plane, const osg::Vec3d &point) -> osg::Vec3d
    {
      auto d = plane.distance(point);
      return point + plane.getNormal() * -d;
    };
    
    osg::Matrixd toView = getInverseMatrix();
    osg::Matrixd fromView = getMatrix();
    auto leftPoint = project(planes.at(0), worldBox.center()) * toView;
    auto rightPoint = project(planes.at(1), worldBox.center()) * toView;
    auto bottomPoint = project(planes.at(2), worldBox.center()) * toView;
    auto topPoint = project(planes.at(3), worldBox.center()) * toView;
    auto frontPoint = project(planes.at(4), worldBox.center()) * toView;
    auto backPoint = project(planes.at(5), worldBox.center()) * toView;
    osg::Vec3d centerPrime
    (
      ((rightPoint - leftPoint) * 0.5 + leftPoint).x()
      , ((topPoint - bottomPoint) * 0.5 + bottomPoint).y()
      , ((backPoint - frontPoint) * 0.5 + frontPoint).z()
    );
    stow->center = centerPrime * fromView;
    stow->projectToBound();
    double halfWidth = ((rightPoint - leftPoint) * 0.5).length();
    double halfHeight = ((topPoint - bottomPoint) * 0.5).length();
    stow->operations.push(Operation::fitBox(halfWidth, halfHeight));
  }
}

void CamManipulator::setRotationPoint(const osg::Vec3d &pIn)
{
  stow->rotationPoint = pIn;
}

void CamManipulator::clearRotationPoint()
{
  stow->rotationPoint = std::nullopt;
}

void CamManipulator::setZoomPoint(const osg::Vec3d &pIn)
{
  stow->zoomPoint = pIn;
}

void CamManipulator::clearZoomPoint()
{
  stow->zoomPoint = std::nullopt;
}

void CamManipulator::ctrlDown()
{
  stow->currentStateMask = ControlKey; //this resets the state.
}

void CamManipulator::ctrlUp()
{
  stow->currentStateMask.reset();
}

CamManipulator::CamType CamManipulator::getCamType() const
{
  return stow->camType;
}

void CamManipulator::setCamType(CamType camTypeIn)
{
  stow->operations.push(Operation::convert(camTypeIn));
}

void CamManipulator::toggleCamType()
{
  if (stow->camType == CamManipulator::CamType::Ortho)
  {
    setCamType(CamManipulator::CamType::Perspective);
    msg::hub().send(msg::buildStatusMessage("Switched To Perspective Camera", 2.0));
  }
  else if (stow->camType == CamManipulator::CamType::Perspective)
  {
    setCamType(CamManipulator::CamType::Ortho);
    msg::hub().send(msg::buildStatusMessage("Switched To Orthographic Camera", 2.0));
  }
}

CamManipulator::CamType CamManipulator::detectCamType(const osg::Matrixd &mIn)
{
  //these are dummy values
  double l, r, b, t, zn, zf;
  if (mIn.getOrtho(l, r, b, t, zn, zf)) return CamType::Ortho;
  else if (mIn.getFrustum(l, r, b, t, zn, zf)) return CamType::Perspective;
  else return CamType::None;
}

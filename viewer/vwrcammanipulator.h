/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VWR_CAMMANIPULATOR_H
#define VWR_CAMMANIPULATOR_H

#include <memory>

#include <osgGA/CameraManipulator>

/* Manipulators are designed to handle the camera location matrix, but not the projection matrix.
 * This presents problems for cameras with an orthographic projection...you know, like all cad uses.
 * I was thinking I could store a projection matrix in this subclass and update the projection
 * matrix in the 'updateCamera' override function, similar to how the view matrix works.
 * The problem is the camera projection matrix gets manipulated by the openscenegraph framework
 * in other places. Especially the near and far planes, but I would imagine things like resizing
 * window affects the projection matrix also. Long story short, we can't 'drive' the projection
 * matrix from the manipulator. So then I was thinking that we could just store the current
 * projection matrix every frame from updateCamera and in the handle function we could alter that
 * projection matrix if needed. The problem is how do we know that my cached projection matrix is equal to
 * the current projection matrix. Meaning, what has happened to the projection matrix between the updateCamera
 * call and the handle call? I checked and the projection matrix is changed every so slightly
 * all the time by osg, so equality check won't work. Going with: just store an abstract operation in the handle
 * function for anything projection matrix related and apply it in the updateCamera function. That
 * makes sense as that is basically what happens with the view matrix.
 */

namespace vwr
{
  class CamManipulator : public osgGA::CameraManipulator
  {
  public:
    enum CamType
    {
      None //error
      , Ortho
      , Perspective
    };
    
    META_Object(vwr, CamManipulator)
    CamManipulator();
    CamManipulator(const CamManipulator& manipIn,  const osg::CopyOp& copyOp = osg::CopyOp::SHALLOW_COPY);
    ~CamManipulator() override;
    
    //view matrix. overrides from parent class
    void setByMatrix(const osg::Matrixd&) override;
    void setByInverseMatrix(const osg::Matrixd&) override;
    osg::Matrixd getMatrix() const override;
    osg::Matrixd getInverseMatrix() const override;
    
    bool handle(const osgGA::GUIEventAdapter&, osgGA::GUIActionAdapter&) override;
    void updateCamera(osg::Camera&) override;
    
    void getLookAt(osg::Vec3d&, osg::Vec3d&, osg::Vec3d&) const;
    void setLookAt(const osg::Vec3d&, const osg::Vec3d&, const osg::Vec3d&);
    
    //external modifiers
    void setBoundingSphere(const osg::BoundingSphere&);
    void setView(const osg::Quat&);
    void setView(const osg::Matrixd&);
    void fit(); //approx fit of bounding sphere.
    void fit(const std::vector<osg::BoundingBox>&, const osg::Camera*);
    void setRotationPoint(const osg::Vec3d&);
    void clearRotationPoint();
    void setZoomPoint(const osg::Vec3d&);
    void clearZoomPoint();
    
    //for external qt event filter.
    void ctrlDown();
    void ctrlUp();
    
    /* camera types. Camera type is cached every frame from updateCamera.
     * We are not able to convert the default perspective camera to ortho
     * at startup. The conversion in the queue doesn't solve the problem because
     * the projection matrix is not correct in the first frame.
     */
    CamType getCamType() const;
    void setCamType(CamType);
    void toggleCamType(); //toggle back and forth, ortho and perspective.
    
    static CamType detectCamType(const osg::Matrixd&);
  private:
    struct Stow;
    std::unique_ptr<Stow> stow;
  };
}

#endif //VWR_CAMMANIPULATOR_H

/*
 * CadSeer. Parametric Solid Modeling.
 * Copyright (C) 2023  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VWR_STANDARDVIEWS_H
#define VWR_STANDARDVIEWS_H

#include <cassert>

#include <osg/Matrixd>

namespace vwr
{
  namespace StandardViews
  {
    inline osg::Quat top(const osg::Quat &base = osg::Quat()) {return base;}
    inline osg::Quat front(const osg::Quat &base = osg::Quat())
    {
      auto xAxis = base * osg::Vec3d(1.0, 0.0, 0.0);
      return base * osg::Quat(osg::PI_2, xAxis);
    }
    inline osg::Quat right(const osg::Quat &base = osg::Quat())
    {
      auto xAxis = base * osg::Vec3d(1.0, 0.0, 0.0);
      auto zAxis = base * osg::Vec3d(0.0, 0.0, 1.0);
      return base * osg::Quat(osg::PI_2, xAxis) * osg::Quat(osg::PI_2, zAxis);
    }
    inline osg::Quat bottom(const osg::Quat &base = osg::Quat())
    {
      auto xAxis = base * osg::Vec3d(1.0, 0.0, 0.0);
      return base * osg::Quat(osg::PI, xAxis);
    }
    inline osg::Quat back(const osg::Quat &base = osg::Quat())
    {
      auto xAxis = base * osg::Vec3d(1.0, 0.0, 0.0);
      auto zAxis = base * osg::Vec3d(0.0, 0.0, 1.0);
      return base * osg::Quat(osg::PI_2, xAxis) * osg::Quat(osg::PI, zAxis);
    }
    inline osg::Quat left(const osg::Quat &base = osg::Quat())
    {
      auto xAxis = base * osg::Vec3d(1.0, 0.0, 0.0);
      auto zAxis = base * osg::Vec3d(0.0, 0.0, 1.0);
      return base * osg::Quat(osg::PI_2, xAxis) * osg::Quat(-osg::PI_2, zAxis);
    }
    
    inline osg::Quat iso(const osg::Quat &base = osg::Quat())
    {
      auto zAxis = base * osg::Vec3d(0.0, 0.0, 1.0);
      auto one = osg::Quat(osg::PI_4, zAxis);
      auto vec = base * osg::Vec3d(1.0, 1.0, 0.0);
      vec.normalize();
      auto two = osg::Quat(osg::PI_4, vec);
      return one * two;
    }
  };
}

#endif //VWR_STANDARDVIEWS_H
